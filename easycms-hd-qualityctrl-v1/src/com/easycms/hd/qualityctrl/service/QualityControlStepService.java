package com.easycms.hd.qualityctrl.service;

import java.util.List;

import com.easycms.hd.qualityctrl.domain.QualityControlStep;

public interface QualityControlStepService {
	
	public QualityControlStep add(QualityControlStep qualityControlStep);
	public boolean delete(QualityControlStep qualityControlStep);
	public QualityControlStep update(QualityControlStep qualityControlStep);
	public QualityControlStep findById(Integer id);
	public Integer count();
	
	/**
	 * 获取指定质量问题ID的处理步骤数据
	 * @param page
	 * @param userId
	 * @return
	 */
	public List<QualityControlStep> findByQctrlId(Integer qctrlId);
	
	/**
	 * 获取质量问题处理步骤
	 * @param qctrlId 问题ID
	 * @param stepDetails 具体步骤
	 * @return
	 */
	public List<QualityControlStep> findByQctrlIdAndStepDetails(Integer qctrlId,String stepDetails);

}
