package com.easycms.hd.qualityctrl.directive;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.response.qualityctrl.QualityControlPageDataResult;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QualityDirective extends BaseDirective<QualityControl> {

	@Autowired
	private QualityControlService qualityControlService;
	@Autowired
	private UserService userService;
	@Autowired
	private DepartmentService departmentService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return qualityControlService.count();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected QualityControl field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		log.debug("[id] ==> " + id);
		if (id != null) {
			QualityControl qualityProblem = qualityControlService.findById(id);
			return qualityProblem;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<QualityControl> list(Map params, String filter, String order, String sort, boolean pageable,
			Page<QualityControl> pager, Map<String, Object> envParams) {
		QualityControlPageDataResult qcPageResult = new QualityControlPageDataResult();
		Page<QualityControl> qualityControls = new Page<QualityControl>();
		Gson gson = new Gson();
		QualityControl ret = gson.fromJson(filter, new TypeToken<QualityControl>() {}.getType());
		
		String status = FreemarkerTemplateUtility.getStringValueFromParams(params, "status");
		log.debug("status------->"+status);

		Integer loginId = ret.getUserId();
		log.debug("loginId------->"+loginId);
		User loginUser = userService.findUserById(loginId);
		if(loginUser == null){
			return null;
		}
//		Integer deptId = loginUser.getDepartment().getId();
		
		//判断角色，返回对应数据
		if(userService.isRoleOf(loginUser, "QCManager")){
			qualityControls = qualityControlService.findByStatus(pager, status);
		}else if(userService.isRoleOf(loginUser, new String[]{ConstantVar.witness_team_qc1,ConstantVar.witness_team_qc2,ConstantVar.witness_member_qc1,ConstantVar.witness_member_qc2,ConstantVar.qc_member})){//"QC1")){
			qualityControls = qualityControlService.findByQcUser(pager, loginId, status);
		}else if(departmentService.isFromDept(loginUser, departmentService.responsibilityDept())){//是责任部门的人，才查询出来分派

			//当前登录人是否是责任部门人员
			/**
			 * 机通队
				主系统队
				电仪队
				管焊队
			 */
			//班长登录则查看本班组的所有质量问题
			if(loginUser.getDepartment() != null){
				//查出子部门，传部门ｉｄ集合查询
				qualityControls = qualityControlService.findByDept(pager, loginUser.getDepartment().getId(),loginId, status);				
			}else{
				return null;
			}
		}else if(userService.isRoleOf(loginUser, "monitor")||userService.isRoleOf(loginUser,ConstantVar.TEAM)){
			
			//班长登录则查看本班组的所有质量问题
			if(loginUser.getDepartment() != null){
				qualityControls = qualityControlService.findByTeamUser(pager, loginId, status);//.findByTeam(page,deptList,loginId, status);	
			}else{
				return null;
			}
		}else{
			qualityControls = qualityControlService.findByCreateUser(pager, loginId, status);
		}
		
		List<QualityControl> retList = qualityControls.getDatas() ;
		retList.forEach(bn->{
			Integer deptId = bn.getResponsibleDept();
			if(deptId!=null){
				
				bn.setResponsibleDept2(departmentService.findById(deptId).getName());
			}
			Integer teamId = bn.getResponsibleTeam() ;
			if(teamId!=null){
				bn.setResponsibleTeam2(departmentService.findById(teamId).getName());
			}
		});
		
		return retList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<QualityControl> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
