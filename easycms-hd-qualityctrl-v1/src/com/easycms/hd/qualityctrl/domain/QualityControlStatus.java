package com.easycms.hd.qualityctrl.domain;

public enum QualityControlStatus {
	
	/**
	 * 待QC主任指派QC
	 */
	PreQCLeaderAssign,
	
	/**
	 * 待QC指派整改队伍
	 */
	PreQCAssign,
	/**
	 * 待up部门整改
	 */
	PreUpRenovete,
	
	/**
	 * 待整改部门整改
	 */
	PreRenovete,
	
	/**
	 * 待QC核实整改结果
	 */
	PreQCverify,
	
	/**
	 * 问题关闭
	 */
	Closed,
	
	/**
	 * 问题结束
	 */
	Finished

}
