package com.easycms.hd.qualityctrl.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "hd_qc_file")
@Data
public class QualityControlFile implements Serializable {
	private static final long serialVersionUID = 7806598708745604174L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	/**
	 * 问题ID
	 */
	@Column(name="qc_id")
    private Integer qcId;

	/**
	 * 文件名
	 */
	@Column(name="filename")
	private String filename;
	
	/**
	 * 文件路径名
	 */
	@Column(name="filepath")
    private String filepath;

	/**
	 * 文件类型
	 */
	@Column(name="filetype")
	private String filetype;
	
	/**
	 * 上传时间
	 */
	@Column(name="upload_date")
    private Date uploadDate;

}
