package com.easycms.hd.qualityctrl.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "hd_qc_options")
@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControlOptions extends BasicForm implements Serializable {
	private static final long serialVersionUID = -1675495576097313083L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	@Column(name="up_id")
	private Integer up_id;
	
	/**
	 * 区域
	 */
	@Column(name="area")
	private String area;
	
	/**
	 * 房间号
	 */
	@Column(name="room")
	private String room;
	
	/**
	 * 标高
	 */
	@Column(name="level")
	private String level;

	/**
	 * 类型
	 */
	@Column(name="type")
	private String type;

	/**
	 * 类型
	 */
	@Column(name="proj_code")
	private String projCode;

}
