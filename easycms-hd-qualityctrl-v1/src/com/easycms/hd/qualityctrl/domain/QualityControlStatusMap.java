package com.easycms.hd.qualityctrl.domain;

import java.util.HashMap;
import java.util.Map;

public class QualityControlStatusMap {
	
	public static Map<String,String> map = new HashMap<String,String>();
	static {
		map.put("PreQCLeaderAssign", "待分派");
		map.put("PreQCAssign", "待指派");
		map.put("PreRenovete", "整改中");
		map.put("PreUpRenovete", "待整改");
		map.put("PreQCverify", "待审核");
		map.put("Closed", "已关闭");
		map.put("Finished", "已完成");
		map.put("true", "是");//是否质量问题单　
		map.put("false", "否");
		map.put("1", "是");//是否质量问题单　
		map.put("0", "否");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
