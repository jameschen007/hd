package com.easycms.hd.qualityctrl.domain;

public enum QualityControlStepStatus {
	
	/**
	 * 待处理
	 */
	Pending,
	
	/**
	 * 已处理
	 */
	Done

}
