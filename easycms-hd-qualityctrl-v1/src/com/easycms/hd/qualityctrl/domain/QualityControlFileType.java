package com.easycms.hd.qualityctrl.domain;

public enum QualityControlFileType {
	
	/**
	 *问题照片 
	 */
	before,
	
	
	/**
	 *整改照片 
	 */
	after,
	
	
	/**
	 * 再次整改照片
	 */
	again
	
}
