package com.easycms.hd.qualityctrl.mybatis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easycms.hd.qualityctrl.domain.QualityControlDetils;

public interface QualityControlMybatisDao {
	Integer getCount();
	Integer getCountByStatus(@Param("status") String status);
	List<QualityControlDetils> findAll(@Param("pageNumber") Integer pageNumber,@Param("pageSize") Integer pageSize);
	List<QualityControlDetils> findByStatus(@Param("status") String status,@Param("pageNumber") Integer pageNumber,@Param("pageSize") Integer pageSize);
	List<QualityControlDetils> findForReport();
}
