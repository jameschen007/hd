package com.easycms.hd.qualityctrl.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.qualityctrl.domain.QualityControlStep;

public interface QualityControlStepDao extends Repository<QualityControlStep,Integer>,BasicDao<QualityControlStep>{
	
	@Query("FROM QualityControlStep q WHERE q.qcId= ?1 order by q.stepDate asc")
	List<QualityControlStep> findByQctrlId(Integer qcId);
	
	@Query("FROM QualityControlStep q WHERE q.qcId= ?1 AND q.stepDetails = ?2 order by q.stepDate asc")
	List<QualityControlStep> findByQctrlIdAndStep(Integer qcId,String step);

}
