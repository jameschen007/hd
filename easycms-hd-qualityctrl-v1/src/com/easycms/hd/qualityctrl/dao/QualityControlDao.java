package com.easycms.hd.qualityctrl.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.qualityctrl.domain.QualityControl;

public interface QualityControlDao extends Repository<QualityControl,Integer>,BasicDao<QualityControl>{
	
	@Query("FROM QualityControl q ORDER BY q.createDate desc")
	Page<QualityControl> findAll(Pageable page);
	
	@Query("FROM QualityControl q WHERE q.createUser = ?1 order by q.createDate desc")
	Page<QualityControl> findByCreater(Integer createUser, Pageable pageable);
	
	@Query("FROM QualityControl q WHERE q.createUser = ?1 AND q.status = ?2 order by q.createDate desc")
	Page<QualityControl> findByCreateAndStatus(Integer createUser,String status,  Pageable pageable);
	
	@Query("FROM QualityControl q WHERE q.status = ?1 order by q.createDate desc")
	Page<QualityControl> findByStatus(String qcStatus, Pageable pageable);
	
	@Query("FROM QualityControl q WHERE (q.responsibleDept = ?1 OR q.createUser = ?2) order by q.createDate desc")
	Page<QualityControl> findByDeptId(Integer deptId,Integer userId, Pageable pageable);
	
	@Query("FROM QualityControl q WHERE (q.responsibleDept = ?1 OR q.createUser = ?2) AND q.status = ?3 order by q.createDate desc")
	Page<QualityControl> findByDeptIdAndStatus(Integer deptId,Integer userId,String status, Pageable pageable);

	@Query("FROM QualityControl q WHERE (q.responsibleTeam = ?1 OR q.createUser = ?2) order by q.createDate desc")
	Page<QualityControl> findByTeamId(Integer teamId,Integer userId, Pageable pageable);

	@Query("FROM QualityControl q WHERE (q.responsibleTeam in (?1) OR q.createUser = ?2) order by q.createDate desc")
	Page<QualityControl> findByTeamId(List<Integer> teamId,Integer userId, Pageable pageable);

	@Query("FROM QualityControl q WHERE (q.responsibleTeam = ?1 OR q.createUser = ?2) AND q.status = ?3 order by q.createDate desc")
	Page<QualityControl> findByTeamIdAndStatus(Integer teamId,Integer userId,String status, Pageable pageable);
	@Query("FROM QualityControl q WHERE (q.responsibleTeam in (?1) OR q.createUser = ?2) AND q.status = ?3 order by q.createDate desc")
	Page<QualityControl> findByTeamIdAndStatus(List<Integer> teamId,Integer userId,String status, Pageable pageable);

	@Query("FROM QualityControl q WHERE (q.qcUser = ?1 OR q.createUser = ?1) order by q.createDate desc")
	Page<QualityControl> findByQcUserAll(Integer qcUserId, Pageable pageable);
	
	@Query("FROM QualityControl q WHERE (q.qcUser = ?1 OR q.createUser = ?1) AND q.status = ?2 order by q.createDate desc")
	Page<QualityControl> findByQcUserAndStatus(Integer qcUserId,String status, Pageable pageable);

	//责任班查和组长查询
	@Query("FROM QualityControl q WHERE (q.teamUser in (?1) OR q.createUser in (?1)) order by q.createDate desc")
	Page<QualityControl> findByTeamUserAll(List<Integer> qcUserId, Pageable pageable);
	
	@Query("FROM QualityControl q WHERE (q.teamUser in (?1) OR q.createUser in (?1)) AND q.status = ?2 order by q.createDate desc")
	Page<QualityControl> findByTeamUserAndStatus(List<Integer> qcUserId,String status, Pageable pageable);

}
