package com.easycms.hd.qualityctrl.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.qualityctrl.domain.QualityControlOptions;

public interface QualityControlOptionsDao  extends Repository<QualityControlOptions,Integer>,BasicDao<QualityControlOptions>{
	
	
}
