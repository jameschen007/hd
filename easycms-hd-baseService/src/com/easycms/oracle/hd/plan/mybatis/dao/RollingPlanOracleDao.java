package com.easycms.oracle.hd.plan.mybatis.dao;

import java.util.List;

import com.easycms.hd.plan.domain.RollingPlan;

public interface RollingPlanOracleDao {
	Long getCount(RollingPlan rollingPlan);
	List<RollingPlan> findByPage(RollingPlan rollingPlan);
	RollingPlan getById(Integer id);
	RollingPlan getByDrawnoAndWeldno(RollingPlan rollingPlan);
}
