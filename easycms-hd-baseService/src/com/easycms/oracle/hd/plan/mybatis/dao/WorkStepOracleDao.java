package com.easycms.oracle.hd.plan.mybatis.dao;

import java.util.List;

import com.easycms.hd.plan.domain.WorkStep;

public interface WorkStepOracleDao {
	Long getCount(WorkStep workStep);
	List<WorkStep> findByPage(WorkStep workStep);
	WorkStep getById(Integer id);
	List<WorkStep> getByRollingPlanDrawnoAndWeldno(WorkStep workStep);
}
