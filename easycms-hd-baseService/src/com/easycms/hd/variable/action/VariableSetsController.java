package com.easycms.hd.variable.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/variable")
public class VariableSetsController {
	private static final Logger logger = Logger.getLogger(VariableSetsController.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;
	
	@RequestMapping("")
	public String init(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is Variable Sets");
		
		return ForwardUtility.forwardAdminView("/variable/sets_index");
	}
	@RequestMapping(value="/setEntry",method=RequestMethod.GET)
	public String setEntry(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String type = "setEntry" ;
		List<VariableSet> variableSet = variableSetService.findByType(type);
		request.setAttribute("varlist", variableSet);

		VariableSet setEntrySelf = variableSetService.findByKey("setEntrySelf");
		request.setAttribute("setEntrySelf", setEntrySelf);
		
		return ForwardUtility.forwardAdminView("/variable/sets_entry");
	}

	@RequestMapping(value="/variableSet",method=RequestMethod.GET)
	public void variableSetUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String type = (String) request.getParameter("type");
		logger.info("[VariableSetsController] variableSetUI. This is sets " + type);

		String ipFromHead = HttpUtility.getIpFromHead(request);
		logger.info("ipFromHead="+ipFromHead);

//			getRequestURL()-getRequestURI()+getContextPath()
	 		logger.info("获得本机ip="+ContextPath.hostAddress);
			com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test = request.getRequestURL().substring(0,request.getRequestURL().indexOf(request.getContextPath())+request.getContextPath().length())+"/";
			logger.info("selfProjectContextPath_test="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test);
			if( request.getContextPath()==null || "".equals( request.getContextPath().trim())){
				String u = request.getRequestURL().toString();
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test = u.substring(0,u.indexOf("/", 9)+1);
			}
			logger.info("selfProjectContextPath_test="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test);
			if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test.indexOf("localhost")!=-1){
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test = 
						com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test.replace("localhost", ContextPath.hostAddress);
			}
			logger.info("selfProjectContextPath_test="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test);
			if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test.indexOf("127.0.0.1")!=-1){
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test = 
						com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test.replace("127.0.0.1", ContextPath.hostAddress);
			}
			logger.info("selfProjectContextPath_test="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath_test);
		
		
        InputStream in = this.getClass().getResourceAsStream("/api.properties");  
        Properties p = new Properties();
        try {  
            p.load(in);
            com.easycms.common.logic.context.ContextPath.fileBasePath_test = p.getProperty("file_base_path");
            com.easycms.common.logic.context.ContextPath.winfileBasePath_test = p.getProperty("windows_file_base_path");
			if( false == com.easycms.common.logic.context.ContextPath.Linux.equals(com.easycms.common.logic.context.ContextPath.os_name)){
				com.easycms.common.logic.context.ContextPath.fileBasePath_test = com.easycms.common.logic.context.ContextPath.winfileBasePath_test ;
			}
			logger.info("fileBasePath="+com.easycms.common.logic.context.ContextPath.fileBasePath_test);
        } catch (IOException e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        }  
		List<VariableSet> list = new ArrayList<VariableSet>();
		List<VariableSet> variableSet = variableSetService.findByType(type);
		
//		request.setAttribute("varlist", variableSet);

//		JsonResult<List<VariableSet>> result = new JsonResult<List<VariableSet>>();
//		result.setResponseResult(list);
		
		HttpUtility.writeToClient(response, CommonUtility.toJson(variableSet));
		
//		return ForwardUtility.forwardAdminView("/variable/variableSetUI");
	}

	@RequestMapping(value="/variableSet",method=RequestMethod.POST)
	public void variableSet(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] variableSetUI. This is sets planningtable");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		Map<String, String> map = new HashMap<String, String>();
		Map<Integer,VariableSet> beanMap = new HashMap<Integer,VariableSet>();
		Map<String,VariableSet> newbeanMap = new HashMap<String,VariableSet>();
		Enumeration enum2 = request.getParameterNames();        
		try{

			while (enum2.hasMoreElements()) {
				String paramName = (String) enum2.nextElement();
				String paramValue = request.getParameter(paramName);
//				map.put(paramName, paramValue);
				
				
				String [] paramT1 = paramName.split("_");
				if(paramT1==null || paramT1.length<2){
					continue ;
				}
				
				boolean isNew = false ;
				Optional<VariableSet> opt = null ;
				Integer id = null;
				if(paramT1[1].matches("\\d+")){
					id = Integer.valueOf(paramT1[1]);
				}else{
					isNew = true ;
				}

				if(isNew){
					opt = Optional.ofNullable(newbeanMap.get(paramT1[1]));
				}else{
					opt = Optional.ofNullable(beanMap.get(id));
				}
				VariableSet optb = opt.orElseGet(VariableSet::new);

				if(paramT1[0].equals("variableKey")){
					optb.setSetkey(paramValue);
				}

				if(paramT1[0].equals("variableValue")){
					optb.setSetvalue(paramValue);
				}
				
				if(isNew){
					if(newbeanMap.containsKey(paramT1[1]) == false ){
						newbeanMap.put(paramT1[1], optb);
					}
				}else{
					if(beanMap.containsKey(id) == false ){
						beanMap.put(id, optb);
					}
				}
			}

			String type = (String) request.getParameter("type");
			
			newbeanMap.entrySet().stream().forEach(b->{
				VariableSet tmp = b.getValue();
				VariableSet variableSet = new VariableSet();
				variableSet.setSetkey(tmp.getSetkey());
				variableSet.setSetvalue(tmp.getSetvalue());
				variableSet.setStatus("ACTIVE");
				variableSet.setType(type);
				variableSet.setUpdateby(loginUser.getName());
				variableSet.setUpdateon(new Date());

				if(tmp.getSetkey()!=null && !"".equals(tmp.getSetkey())
						&& tmp.getSetvalue()!=null && !"".equals(tmp.getSetvalue())
						){

					variableSetService.add(variableSet);
					
					logger.info("add = "+tmp.getSetkey()+"  "+ tmp.getSetvalue());
				}
			});
			
			beanMap.entrySet().stream().forEach(b->{
				VariableSet tmp = b.getValue();
				VariableSet variableSet = new VariableSet();
				variableSet.setId(b.getKey());
				variableSet.setSetkey(tmp.getSetkey());
				variableSet.setSetvalue(tmp.getSetvalue());
				variableSet.setStatus("ACTIVE");
				variableSet.setType(type);
				variableSet.setUpdateby(loginUser.getName());
				variableSet.setUpdateon(new Date());

				variableSetService.updateById(variableSet);
				
				logger.info(tmp.getSetkey()+"  "+ tmp.getSetvalue());
			});
	     	HttpUtility.writeToClient(response, "true");
        }catch(Exception e){
        	e.printStackTrace();
        }
     	HttpUtility.writeToClient(response, "false");
	}
	
	/** */
	@RequestMapping(value="/{type}",method=RequestMethod.GET)
	public String settingsGet(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) throws Exception {
		logger.info("[VariableSetsController] This is sets " + type);
		
		request.setAttribute(type + "Map", getSet(type));
		
		return ForwardUtility.forwardAdminView("/variable/sets_" + type);
	}

	@RequestMapping(value="/planningtable",method=RequestMethod.POST)
	public String planningtableSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets planningtable");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String rollingPlan = (String) request.getParameter("rollingPlan");
		String workStep = (String) request.getParameter("workStep");
		
		Map<String, String> planningtableMap = new HashMap<String, String>();
		planningtableMap.put("rollingPlan", rollingPlan);
		planningtableMap.put("workStep", workStep);
		
		parser(planningtableMap, "planningtable", loginUser);
		
		request.setAttribute("planningtableMap", planningtableMap);
		
		logger.info("planningtable update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_planningtable");
	}
	
	@RequestMapping(value="/assigntype",method=RequestMethod.POST)
	public String assigntypeSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets assigntype");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String consteam = (String) request.getParameter("consteam");
		String team = (String) request.getParameter("team");
		String endman = (String) request.getParameter("endman");
		Map<String, String> assigntypeMap = new HashMap<String, String>();
		assigntypeMap.put("consteam", consteam);
		assigntypeMap.put("team", team);
		assigntypeMap.put("endman", endman);
		
		parser(assigntypeMap, "assigntype", loginUser);
		
		request.setAttribute("assigntypeMap", assigntypeMap);
		
		logger.info("assigntype update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_assigntype");
	}
	
	@RequestMapping(value="/price",method=RequestMethod.POST)
	public String priceSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets price");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String GCL = (String) request.getParameter("GCL");
		String DZ = (String) request.getParameter("DZ");
		String GS = (String) request.getParameter("GS");
		Map<String, String> priceMap = new HashMap<String, String>();
		priceMap.put("GCL", GCL);
		priceMap.put("DZ", DZ);
		priceMap.put("GS", GS);
		
		parser(priceMap, "price", loginUser);
		
		request.setAttribute("priceMap", priceMap);
		
		logger.info("price update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_price");
	}
	
	@RequestMapping(value="/witnesstype",method=RequestMethod.POST)
	public String witnesstypeSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets witnesstype");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String witnessTeam = (String) request.getParameter("witnessTeam");
		String witnessDepartment = (String) request.getParameter("witnessDepartment");
		String W = (String) request.getParameter("W");
		String H = (String) request.getParameter("H");
		String R = (String) request.getParameter("R");
		Map<String, String> witnesstypeMap = new HashMap<String, String>();
		witnesstypeMap.put("witnessTeam", witnessTeam);
		witnesstypeMap.put("witnessDepartment", witnessDepartment);
		witnesstypeMap.put("W", W);
		witnesstypeMap.put("H", H);
		witnesstypeMap.put("R", R);
		
		parser(witnesstypeMap, "witnesstype", loginUser);
		
		request.setAttribute("witnesstypeMap", witnesstypeMap);
		
		logger.info("witnesstype update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_witnesstype");
	}
	
	@RequestMapping(value="/welddistinguish",method=RequestMethod.POST)
	public String welddistinguishSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets welddistinguish");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String weldHK = (String) request.getParameter("weldHK");
		String weldZJ = (String) request.getParameter("weldZJ");
		String welder = (String) request.getParameter("welder");
		Map<String, String> welddistinguishMap = new HashMap<String, String>();
		welddistinguishMap.put("weldHK", weldHK);
		welddistinguishMap.put("weldZJ", weldZJ);
		welddistinguishMap.put("welder", welder);
		
		parser(welddistinguishMap, "welddistinguish", loginUser);
		
		request.setAttribute("welddistinguishMap", welddistinguishMap);
		
		logger.info("witnesstype update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_welddistinguish");
	}
	
	@RequestMapping(value="/hysteresis",method=RequestMethod.POST)
	public String hysteresisSettingsPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("[VariableSetsController] This is sets hysteresis");
		
		User loginUser = (User) request.getSession().getAttribute("user");
		
		String hysteresis = (String) request.getParameter("hysteresis");
		Map<String, String> hysteresisMap = new HashMap<String, String>();
		hysteresisMap.put("hysteresis", hysteresis);
		
		parser(hysteresisMap, "hysteresis", loginUser);
		
		request.setAttribute("hysteresisMap", hysteresisMap);
		
		logger.info("hysteresis update success");
		
		return ForwardUtility.forwardAdminView("/variable/sets_hysteresis");
	}
	
	private boolean parser(Map<String, String> map, String type, User user){
		Iterator<Entry<String, String>> mapIter = map.entrySet().iterator();
		while(mapIter.hasNext()) {
			Entry<String, String> entry = mapIter.next();
			
			String key = (String)entry.getKey();
			String value = (String)entry.getValue();
			VariableSet variableSet = new VariableSet();
			variableSet.setSetkey(key);
			variableSet.setSetvalue(value);
			variableSet.setStatus("ACTIVE");
			variableSet.setType(type);
			variableSet.setUpdateby(user.getName());
			variableSet.setUpdateon(new Date());
			
			if(!variableSetService.update(variableSet)){
				return false;
			}
		}
		
		return true;
	}
	
	private Map<String, String> getSet(String type){
		Map<String, String> resultMap = new HashMap<String, String>();
		
		List<VariableSet> variableSet = variableSetService.findByType(type);
		for (VariableSet as : variableSet){
			resultMap.put(as.getSetkey(), as.getSetvalue());
		}
		
		return resultMap;
	}
}
