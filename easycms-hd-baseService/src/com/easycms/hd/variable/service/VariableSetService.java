package com.easycms.hd.variable.service;

import java.util.List;
import java.util.Set;

import com.easycms.hd.variable.domain.VariableSet;

public interface VariableSetService{
	VariableSet findById(Integer id);
	List<VariableSet> findByAll();
	VariableSet findByKey(String key, String type);
	VariableSet findByKey(String key);
	List<VariableSet> findByType(String type);
	void empty(String type);
	public boolean updateById(VariableSet variableSet);
	boolean update(VariableSet variableSet);
	VariableSet add(VariableSet variableSet);
	String findValueByKey(String key);
	String findValueByKey(String key, String type);
	Set<String> findKeyByValue(String value, String type);

	Set<String> findSetValueBySetKeyAndType(String key, String type);
}
