package com.easycms.hd.variable.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.variable.domain.VariableSet;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface VariableSetDao extends BasicDao<VariableSet> ,Repository<VariableSet,Integer>{

	@Modifying
	@Query("DELETE FROM VariableSet p WHERE p.id  = ?1")
	int deleteById(Integer id);

	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	VariableSet findById(String id);
	
//	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM VariableSet p WHERE p.setkey=?1 and p.type = ?2")
	VariableSet findByKey(String key, String type);
	
//	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM VariableSet p WHERE p.setkey=?1")
	VariableSet findByKey(String key);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM VariableSet p WHERE p.type = ?1")
	List<VariableSet> findByType(String type);
	
//	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM VariableSet")
	List<VariableSet> findByAll();

	@Modifying
	@Query("DELETE FROM VariableSet p WHERE p.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Modifying
	@Query("UPDATE VariableSet p SET setvalue='' WHERE p.type = ?1")
	int deleteAllValue(String type);

	@Modifying
	@Query("UPDATE VariableSet p SET p.setvalue=?1, p.status=?2, p.updateon=?3, p.updateby=?4 WHERE p.setkey=?5 and p.type=?6")
	int update(String setvalue,String status,Date updateon,String updateby,String setkey, String type);
	@Modifying
	@Query("UPDATE VariableSet p SET p.setvalue=?1, p.status=?2, p.updateon=?3, p.updateby=?4 WHERE p.setkey=?5 and p.type=?6 and p.id=?7")
	int update(String setvalue,String status,Date updateon,String updateby,String setkey, String type,Integer id);
	
	VariableSet findBySetkey(String key);

	@Query("SELECT p.setkey FROM VariableSet p WHERE p.setvalue=?1 and p.type=?2")
	Set<String> findSetkeyBySetvalueAndType(String value, String type);
	@Query("SELECT p.setvalue FROM VariableSet p WHERE p.setkey=?1 and p.type=?2")
	Set<String> findSetValueBySetKeyAndType(String key, String type);
}
