package com.easycms.hd.statistics.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.statistics.dao.QueryDateDao;
import com.easycms.hd.statistics.domain.QueryDate;
import com.easycms.hd.statistics.service.QueryDateService;

@Service("queryDateSetService")
public class QueryDateServiceImpl implements QueryDateService{
	@Autowired
	private QueryDateDao queryDateDao;

	@Override
	public int deleteAll() {
		return queryDateDao.deleteAll();
	}

	@Override
	public QueryDate save(QueryDate queryDate) {
		return queryDateDao.save(queryDate);
	}
}
