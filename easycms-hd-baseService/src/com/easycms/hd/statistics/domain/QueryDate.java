package com.easycms.hd.statistics.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-08 
 * 
 */
@Entity
@Table(name = "query_date")
@Cacheable
public class QueryDate extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = 4977654969275194552L;
	@Id
	@Column(name = "select_date", unique = true, nullable = false)
	private String id;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}