package com.easycms.hd.statistics.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.statistics.domain.TaskStatusDate;
import com.easycms.hd.statistics.form.TaskStatisticsForm;
import com.easycms.hd.statistics.util.StatisticsUtil;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.RoleService;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {
	private static final Logger logger = Logger.getLogger(StatisticsController.class);
	private static String ASSIGN_TYPE = "assigntype";
	private static String CONSTEAM = "consteam";
	private static String END_MAN = "endman";
	
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private VariableSetService variableSetService;

	@RequestMapping(value = "/hyperbola", method = RequestMethod.GET)
	public String hyperbola(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
		TaskStatusDate tsd = new TaskStatusDate();
		tsd.setAfter(StatisticsUtil.getDate(1));
		tsd.setBefore(StatisticsUtil.getDate(-1));
		tsd.setCurrent(StatisticsUtil.getDate(0));
		tsd.setWeek(StatisticsUtil.getWeek());
		tsd.setMonth(StatisticsUtil.getMonth());
		tsd.setYear(StatisticsUtil.getYear());
		
		request.setAttribute("taskStatusDate", tsd);
		
		String returnString = ForwardUtility.forwardAdminView("statistics/task_hyperbola");
		return returnString;
		
	}
	
	@RequestMapping(value = "/hyperbola", method = RequestMethod.POST)
	public String hyperbolaPOST(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
		TaskStatusDate tsd = new TaskStatusDate();
		tsd.setAfter(StatisticsUtil.getDate(1));
		tsd.setBefore(StatisticsUtil.getDate(-1));
		tsd.setCurrent(StatisticsUtil.getDate(0));
		tsd.setWeek(StatisticsUtil.getWeek());
		tsd.setMonth(StatisticsUtil.getMonth());
		tsd.setYear(StatisticsUtil.getYear());
		
		request.setAttribute("taskStatusDate", tsd);
		
		String returnString = ForwardUtility.forwardAdminView("statistics/data/data_html_task_hyperbola");
		return returnString;
		
	}
	
	@RequestMapping(value = "/taskstatus", method = RequestMethod.GET)
	public String taskStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		TaskStatusDate tsd = new TaskStatusDate();
		tsd.setAfter(StatisticsUtil.getDate(1));
		tsd.setBefore(StatisticsUtil.getDate(-1));
		tsd.setCurrent(StatisticsUtil.getDate(0));
		tsd.setWeek(StatisticsUtil.getWeek());
		tsd.setMonth(StatisticsUtil.getMonth());
		tsd.setYear(StatisticsUtil.getYear());
		
		request.setAttribute("taskStatusDate", tsd);
		
		String returnString = ForwardUtility.forwardAdminView("statistics/task_status");
		return returnString;
		
	}
	
	@RequestMapping(value = "/taskteam", method = RequestMethod.GET)
	public String taskTeam(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
		Department department = new Department();
		department.setName(variableSetService.findValueByKey(CONSTEAM, ASSIGN_TYPE));
		form.setFilter(CommonUtility.toJson(department));
		
		String returnString = ForwardUtility.forwardAdminView("statistics/task_team");
		return returnString;
		
	}
	
	@RequestMapping(value = "/taskprice", method = RequestMethod.GET)
	public String taskPrice(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
		Department department = new Department();
		department.setName(variableSetService.findValueByKey(CONSTEAM, ASSIGN_TYPE));
		form.setFilter(CommonUtility.toJson(department));
		
		String returnString = ForwardUtility.forwardAdminView("statistics/task_price");
		return returnString;
		
	}
	
	@RequestMapping(value = "/taskgroup", method = RequestMethod.GET)
	public String taskGroup(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
		String consteam = variableSetService.findValueByKey(CONSTEAM, ASSIGN_TYPE);
		String endMan = variableSetService.findValueByKey(END_MAN, ASSIGN_TYPE);
		form.setCustomRole(endMan);
		
		Department department = departmentService.findByName(consteam);
		if (null != department){
			if (null != department.getChildren() && department.getChildren().size() > 0){
				Department d = department.getChildren().get(0);
				form.setId(d.getId());
			}
		} else {
			form.setId(0);
		}
		
		department = new Department();
		department.setName(consteam);
		form.setFilter(CommonUtility.toJson(department));
		
		String returnString = ForwardUtility.forwardAdminView("statistics/task_group");
		return returnString;
		
	}
	
	@RequestMapping(value = "/taskgroup", method = RequestMethod.POST)
	public String taskGroupDataList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") TaskStatisticsForm form) {
		logger.debug("==> Show dashboard task group data list.");
		String teamId = request.getParameter("teamId");
		form.setId(Integer.parseInt(teamId));
		String endMan = variableSetService.findValueByKey(END_MAN, ASSIGN_TYPE);
		form.setCustomRole(endMan);
		String returnString = ForwardUtility.forwardAdminView("statistics/data/data_html_task_group");
		return returnString;
	}
	
	@RequestMapping(value = "/taskstatus", method = RequestMethod.POST)
	public String taskStatusDataList(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("==> Show dashboard task status data list.");
		
		String date = request.getParameter("date");
		String category = request.getParameter("category");
		
		request.setAttribute("taskDate", date);
		request.setAttribute("category", category);
		
		String returnString = ForwardUtility.forwardAdminView("statistics/data/data_html_task_status");
		return returnString;
	}
}
