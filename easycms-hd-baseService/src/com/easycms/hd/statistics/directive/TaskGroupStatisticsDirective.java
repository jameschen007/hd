package com.easycms.hd.statistics.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.StatisticsRollingPlanService;
import com.easycms.hd.statistics.domain.TaskStatistics;
import com.easycms.hd.variable.service.VariableSetService;

public class TaskGroupStatisticsDirective extends BaseDirective<TaskStatistics>  {
	private static final Logger logger = Logger.getLogger(TaskGroupStatisticsDirective.class);
	private static String WELD_DISTINGUISH = "welddistinguish";
	
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private StatisticsRollingPlanService statisticsRollingPlanService;
	@Autowired
	private VariableSetService variableSetService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected TaskStatistics field(Map params, Map<String, Object> envParams) {
		String id = FreemarkerTemplateUtility.getStringValueFromParams(params, "id");
		String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
		String status = FreemarkerTemplateUtility.getStringValueFromParams(params, "status");
		
		if (null == id || null == status){
			return null;
		}
		
		if (null != type){
			if (type.equals("weldHK")){
				type = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
			} else if (type.equals("weldZJ")){
				type = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);
			} else {
				return null;
			}
		}
		
		logger.debug("[id] ==> " + id + " [status] ==> " + status + " [type] ==> " + type);
		TaskStatistics taskStatistics = new TaskStatistics();
		if (status.equals("complate")){
			taskStatistics.setComplate(statisticsRollingPlanService.getRollingPlanCompleteByConsEndmanAndType(id, type));
		} else if (status.equals("surplus")){
			taskStatistics.setSurplus(statisticsRollingPlanService.getRollingPlanUnCompleteByConsEndmanAndType(id, type));
		} else if (status.equals("total")){
			taskStatistics.setTotal(statisticsRollingPlanService.getRollingPlanTotalByConsEndman(id));
		}
		
		logger.info(taskStatistics);
		return taskStatistics;
	}
	
	@SuppressWarnings({ "rawtypes"})
	@Override
	protected List<TaskStatistics> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<TaskStatistics> pager,
			Map<String, Object> envParams) {
		
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<TaskStatistics> tree(Map params, Map<String, Object> envParams) {
		return null;
	}
}
