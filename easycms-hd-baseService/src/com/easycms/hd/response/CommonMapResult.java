package com.easycms.hd.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class CommonMapResult implements Comparable<CommonMapResult>, Serializable{
	private static final long serialVersionUID = -8829270636239205067L;
	private String key;
	private String value;
	private Integer seq;

	@Override
	public int compareTo(CommonMapResult commonMapResult) {
		return commonMapResult.getSeq().compareTo(this.getSeq());
	}
}
