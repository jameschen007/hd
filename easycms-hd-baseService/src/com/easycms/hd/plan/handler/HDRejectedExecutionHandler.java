package com.easycms.hd.plan.handler;

import java.lang.Thread.State;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

public class HDRejectedExecutionHandler implements RejectedExecutionHandler {
	private static final Logger logger = Logger.getLogger(HDRejectedExecutionHandler.class);
	private static ThreadPoolRejectMonitorThread threadPoolRejectMonitorThread;
	
	@Override
	public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor) {
		logger.error(runnable.toString() + " is rejected");
		ThreadPoolContext.getInstance().addRunnable(runnable);
		threadPoolRejectMonitorThread(executor);
	}
	
	private void threadPoolRejectMonitorThread(ThreadPoolExecutor executor){
		boolean flag = false;
		
		if (null == threadPoolRejectMonitorThread){
			threadPoolRejectMonitorThread = new ThreadPoolRejectMonitorThread(executor);
			threadPoolRejectMonitorThread.start();
			
			if(threadPoolRejectMonitorThread.getState().equals(State.RUNNABLE) || threadPoolRejectMonitorThread.getState().equals(State.TIMED_WAITING)){
				flag = true;
			}
			logger.info("-------> ThreadPoolRejectMonitorThread is not exist, create it and now the start status is [" + flag + "].");
		} else {
			logger.info("ConferenceDeductionEXThread status : " + threadPoolRejectMonitorThread.getState());
			
	        if(!threadPoolRejectMonitorThread.getState().equals(State.RUNNABLE) && !threadPoolRejectMonitorThread.getState().equals(State.TIMED_WAITING)){
	        	threadPoolRejectMonitorThread = new ThreadPoolRejectMonitorThread(executor);
				threadPoolRejectMonitorThread.start();
	        	
	        	if(threadPoolRejectMonitorThread.getState().equals(State.RUNNABLE) || threadPoolRejectMonitorThread.getState().equals(State.TIMED_WAITING)){
					flag = true;
				}
	        	logger.info("-------> ThreadPoolRejectMonitorThread is stop by some accident, now the restart status is [" + flag + "].");
			}
		}
	}
}
