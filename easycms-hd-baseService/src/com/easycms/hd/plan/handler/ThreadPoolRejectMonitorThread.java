package com.easycms.hd.plan.handler;

import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

public class ThreadPoolRejectMonitorThread extends Thread{
	private static final Logger logger = Logger.getLogger(ThreadPoolRejectMonitorThread.class);
	private static ThreadPoolExecutor executor;
	
	@SuppressWarnings("static-access")
	public ThreadPoolRejectMonitorThread(ThreadPoolExecutor executor){
		this.executor = executor;
		logger.debug("Init ThreadPoolRejectMonitorThread with Constructor.");
	}
	
	@Override
	public void run() {
		while(!ThreadPoolContext.getInstance().getQueueIsEmpty()){
    		try {
    			long sleep = 1000;
    			
    			int queueSize = ThreadPoolContext.getInstance().getQueueSize();
    			logger.debug("Current Quese Size: " + queueSize);
    			
    			int addSize = (executor.getMaximumPoolSize() / 2);
    			if (executor.getActiveCount() < addSize){
	    			for (int i = 0; i < addSize ; i++){
	    				Runnable runnable = ThreadPoolContext.getInstance().getRunnable();
	    				if (null != runnable){
	    					executor.execute(runnable);
	    				}
	    			}
    			}
    			
				logger.debug("ThreadPoolRejectMonitorThread will sleep [" + sleep + " ms].");
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
				logger.debug("ThreadPoolRejectMonitorThread is Interrupted.");
			}
		}
	}
}