package com.easycms.hd.plan.handler;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPoolContext {
    private static ThreadPoolContext instance;
    private static Queue<Runnable> runnableQueue;
    
    private ThreadPoolContext(){
    	runnableQueue = new LinkedBlockingQueue<Runnable>();
    }
    
    public static ThreadPoolContext getInstance(){
        if(null == instance){
            instance = new ThreadPoolContext();
        }
        return instance;
    }
    
    public synchronized Queue<Runnable> getAllQueue(){
        return runnableQueue;
    }
    
    public synchronized boolean addRunnable(Runnable runnable){
    	try {
			runnableQueue.offer(runnable);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return false;
    }
    
    public synchronized Runnable getRunnable(){
    	return runnableQueue.poll();
    }
    
    public synchronized int getQueueSize(){
        return runnableQueue.size();
    }
    
    public synchronized boolean getQueueIsEmpty(){
    	return runnableQueue.isEmpty();
    }
}