package com.easycms.hd.plan.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.plan.domain.Isend;
import com.easycms.hd.plan.form.AssignForm;
import com.easycms.hd.plan.form.RollingPlanForm;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/construction")
@Slf4j
public class ConstructionAssignController {
	
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private UserService userService;
	@Autowired
	private JPushService jPushService;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	//队长查看已分派到班长
	@RequestMapping(value = "/captain", method = RequestMethod.GET)
	public String captain(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility.forwardAdminView("/hdService/assign/list_rollingPlan_captain");
	}
	
	@RequestMapping(value = "/captain", method = RequestMethod.POST)
	public String captainDataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form) {
		log.debug("==> Show assign data list.");
		form.setSearchValue(request.getParameter("search[value]"));
		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("captain");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/assign/data/data_json_rollingPlan_captain");
		return returnString;
	}
	
	//班长查看已未分派到组长
	@RequestMapping(value = "/monitor", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		return ForwardUtility.forwardAdminView("/hdService/assign/list_rollingPlan_monitor_unassign");
	}
	
	@RequestMapping(value = "/monitor", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form) {
		log.debug("==> Show assign data list.");
		form.setSearchValue(request.getParameter("search[value]"));
		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("monitor");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/assign/data/data_json_rollingPlan_monitor_unassign");
		return returnString;
	}
	//班长查看已分派到组
	@RequestMapping(value = "/monitor/assigned", method = RequestMethod.GET)
	public String settingsAlready(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility.forwardAdminView("/hdService/assign/list_rollingPlan_monitor_assigned");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "/monitor/assigned", method = RequestMethod.POST)
	public String dataListAlready(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form) {
		log.debug("==> Show assign data list.");
		form.setSearchValue(request.getParameter("search[value]"));
		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("monitorAssigned");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/assign/data/data_json_rollingPlan_monitor_assigned");
		return returnString;
	}

	/**
	 * 分派到施工班组
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/monitor/assign", method = RequestMethod.GET)
	public String assignUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
		List<User> users = userService.findUserByDeptParentIdAndRoleName(ContextUtil.getCurrentLoginUser().getId(),ConstantVar.TEAM);
		request.setAttribute("teams", users);
		String returnString = ForwardUtility.forwardAdminView("/hdService/assign/modal_rollingPlan_monitor_assign_team");
		return returnString;
	}

	/**
	 * 保存分派信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/monitor/assign", method = RequestMethod.POST)
	public void assign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") AssignForm form,
			BindingResult errors) {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		if (null == loginUser){
			return;
		}
		
		Integer[] ids = form.getIds();
		log.debug("==> Start save new assign for : " + CommonUtility.toJson(ids));
		
//		boolean flag = rollingPlanService.assign(ids, form.getTeamId(), Isend.ARRANGED, new Date(), form.getPlanfinishdate());
		try{
			boolean flag = rollingPlanService.assignTeam(form.getIds(), form.getTeamId(), Isend.ARRANGED, new Date(), form.getPlanfinishdate(), loginUser.getId());
			String message = loginUser.getRealname() + " - 将 [" + ids.length + "] 条滚动计划分派给了你";
			if (flag){
				JPushExtra extra = new JPushExtra();
	    		extra.setCategory(JPushCategoryEnums.CONS_ASSIGN.name());
	    		jPushService.pushByUserId(extra, message, JPushCategoryEnums.CONS_ASSIGN.getName(), form.getTeamId());
			}
			
			HttpUtility.writeToClient(response, CommonUtility.toJson(flag));
			
		}catch(Exception e){
			e.printStackTrace();
			HttpUtility.writeToClient(response, "操作异常:"+e.getMessage());
			return ;
		}
		
		log.debug("==> End save new assign.");
		return;
	}


	/**
	 * 修改分派信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/monitor/modify", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
		List<User> users = userService.findUserByDeptParentIdAndRoleName(ContextUtil.getCurrentLoginUser().getId(),ConstantVar.TEAM);
		request.setAttribute("teams", users);
		return ForwardUtility.forwardAdminView("/hdService/assign/modal_rollingPlan_modify_team");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/monitor/modify", method = RequestMethod.POST)
	public void edit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") AssignForm form,
			BindingResult errors) {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		if (null == loginUser){
			return;
		}

        if (form.getIds().length < 1){
        	HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
    		log.debug("==> End save eidt assign.");
    		return;
        }
        boolean f = false ;
		if (null == form.getPlanfinishdate()) {
			f = rollingPlanService.reAssignTeam(form.getIds(), form.getTeamId(), loginUser.getId());
		} else {
			f = rollingPlanService.reAssignTeam(form.getIds(), form.getTeamId(), form.getPlanfinishdate(), loginUser.getId());
		}
        
/*		
		if (flag){
			for (Integer s : rpConsteamSet){
				int amount = PlanUtil.personAmount(rps, "consteam", String.valueOf(s));
				if (amount != 0){
					String alert = loginUser.getRealname() + " - 将你的 [" + amount + "] 条滚动计划进行了改派";
					JPushExtra extra = new JPushExtra();
					extra.setCategory("modifyAssignTeam");
					extra.setRemark("班组信息被改派，将不再属于这用户");
					jPushService.pushByDepartmentId(extra, alert, "改派到班组", s, team);
				}
			}
			String alert = loginUser.getRealname() + " - 将 [" + ids.length + "] 条滚动计划分派给了你";
			JPushExtra extra = new JPushExtra();
			extra.setCategory("assignTeam");
			extra.setData(CommonUtility.toJson(ids).replace("\"", "'"));
			extra.setRemark("data中的数据为滚动计划的ID数组");
			jPushService.pushByDepartmentId(extra, alert, "分派到班组", teamId, team);
		}
*/
		HttpUtility.writeToClient(response, CommonUtility.toJson(f+""));
		log.debug("==> End save eidt assign.");
		return;
	}

	/**
	 * 批量解除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/monitor/release")
	public void monitorRelease(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		if (null == loginUser){
			return;
		}
		
		log.debug("==> To release assign [" + CommonUtility.toJson(form.getIds()) + "]");
		if (form.getIds() != null && form.getIds().length > 0) {
			rollingPlanService.releaseTeam(form.getIds(), loginUser.getId());
		}
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete assign.");
	}
	
	/**
	 * 批量解除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/captain/release")
	public void captainRelease(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
		log.debug("==> To release assign [" + CommonUtility.toJson(form.getIds()) + "]");
		if (form.getIds() != null && form.getIds().length > 0) {
			rollingPlanService.deleteRollingPlan(form.getIds());
		}
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete assign.");
	}
}
