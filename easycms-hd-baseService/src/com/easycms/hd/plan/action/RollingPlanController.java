package com.easycms.hd.plan.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.hd.plan.form.RollingPlanForm;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/baseservice/rollingplan")
@Slf4j
public class RollingPlanController {

	@Autowired
	private ModulesService modulesService;

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String settings(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return ForwardUtility.forwardAdminView("/hdService/rollingPlan/list_rollingPlan");
  }

  /**
   * 数据片段
   * 
   * @param request
   * @param response
   * @param pageNum
   * @return
   */
  @RequestMapping(value = "", method = RequestMethod.POST)
  public String dataList(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") RollingPlanForm form) {
	form.setSearchValue(request.getParameter("search[value]"));
    log.debug("==> Show rollingPlan data list.");
    form.setFilter(CommonUtility.toJson(form));
    String returnString =
        ForwardUtility.forwardAdminView("/hdService/rollingPlan/data/data_json_rollingPlan");
    return returnString;
  }
  
  @RequestMapping(value = "/type", method = RequestMethod.GET)
  public String typeUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return ForwardUtility.forwardAdminView("/hdService/rollingPlan/modal_rollingPlan_type");
  }

  /**
   * 数据片段
   * 
   * @param request
   * @param response
   * @param pageNum
   * @return
   */
  @RequestMapping(value = "/type", method = RequestMethod.POST)
  public void type(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") RollingPlanForm form) {
	  if (null != modulesService.findByType(form.getType())) {
		  request.getSession().setAttribute("type", form.getType());
		  HttpUtility.writeToClient(response, CommonUtility.toJson(true));
	  }
	  HttpUtility.writeToClient(response, CommonUtility.toJson(false));
  }
}
