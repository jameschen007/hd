package com.easycms.hd.plan.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.Isend;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.form.RollingPlanForm;
import com.easycms.hd.plan.form.WorkStepForm;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.hd.plan.service.WorkStepFlagService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/construction/mytask")
public class ConstructionProcessController {
	private static final Logger logger = Logger.getLogger(ConstructionProcessController.class);
	
	private static String WELDDISTINGUISH = "welddistinguish";
	private static String WELDER = "welder";
	
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private RollingPlanTansferService rollingPlanTansferService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private WorkStepFlagService workStepFlagService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public String task(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		request.setAttribute("pageable", "false");
		return ForwardUtility.forwardAdminView("/hdService/task/list_rollingPlan_task");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form) {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		List<String> consendman = new ArrayList<String>();
		
		if (null != loginUser){
			if (!loginUser.isDefaultAdmin()){
				consendman.add(loginUser.getId()+"");
			}
			form.setConsendman(CommonUtility.toJson(consendman));
			
			form.setFilter(CommonUtility.toJson(form));
			form.setCustom("task");
			logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		}
		String returnString = ForwardUtility.forwardAdminView("/hdService/task/data/data_json_rollingPlan_task");
		return returnString;
	}
	
	@RequestMapping(value="/already", method = RequestMethod.GET)
	public String taskAlready(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility.forwardAdminView("/hdService/task/list_rollingPlan_task_already");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "/already", method = RequestMethod.POST)
	public String alreadyDataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form) {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		List<String> consendman = new ArrayList<String>();
		
		if (null != loginUser){
			if (!loginUser.isDefaultAdmin()){
				consendman.add(loginUser.getId()+"");
			}
			
			form.setConsendman(CommonUtility.toJson(consendman));
			
			form.setFilter(CommonUtility.toJson(form));
			form.setCustom("taskAlready");
			logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		}
		String returnString = ForwardUtility.forwardAdminView("/hdService/task/data/data_json_rollingPlan_task_already");
		return returnString;
	}
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String viewWorkStep(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String params = request.getParameter("autoidup");
		String[] datas =	params.split("[?]");
		request.setAttribute("autoidup",datas[0]);
		return ForwardUtility.forwardAdminView("/hdService/task/list_workStep");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public String workStepDataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") WorkStepForm form) {
		logger.debug("==> Show workStep data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/task/data/data_json_workStep");
		
		return returnString;
	}
	
	@RequestMapping(value = "/workstep/result", method = RequestMethod.GET)
	public String workstepResultUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		Integer workStepId = form.getId();
		String operater = "";
		
		WorkStep workStep = workStepService.findById(workStepId);
		if (null != workStep){
			RollingPlan rollingPlan = workStep.getRollingPlan();
			if (null != rollingPlan){
				String operaterId = rollingPlan.getConsendman();
				try {
					Integer id = Integer.parseInt(operaterId);
					
					User user = userService.findUserById(id);
					if (null != user){
						operater = user.getRealname();
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		
		request.setAttribute("operater", operater);
		
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_workstep_result");
	}
	
	@RequestMapping(value = "/workstep/result/edit", method = RequestMethod.GET)
	public String workstepResultEditUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_workstep_result_edit");
	}
	
	@RequestMapping(value = "/workstep/result/edit", method = RequestMethod.POST)
	public void workstepResultEdit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		}
		
		logger.info(CommonUtility.toJson(form));
		
		WorkStep workStep = workStepService.findById(form.getId());
		
		if (null != workStep){
			workStep.setOperater(form.getOperater());
			workStep.setOperatedesc(form.getOperatedesc());
			workStep.setOperatedate(form.getOperatedate());
			workStep.setUpdatedBy(loginUser.getId()+"");
			workStep.setUpdatedOn(new Date());
			
			workStep = workStepService.add(workStep);
			
//			workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId());
			
			RollingPlan rollingPlan = workStep.getRollingPlan();
			if (null != rollingPlan && PlanUtil.lastOneMark(workStep).isLastone()){
				rollingPlan.setQcman(form.getQcman());
				rollingPlan.setQcsign(form.getQcsign());
				rollingPlan.setQcdate(new Date());
				if (loginUser.isDefaultAdmin()){
					rollingPlan.setQcdate(form.getQcdate());
				}
				
				rollingPlan = rollingPlanService.add(rollingPlan);
			}
			
			String welder = variableSetService.findValueByKey(WELDER, WELDDISTINGUISH);
			
			if (null != rollingPlan && welder.equals(workStep.getStepname())){
				rollingPlan.setWelder(Integer.parseInt(form.getOperater()));
				rollingPlan.setWelddate(new Date());
				
				rollingPlanService.add(rollingPlan);
			}
		
			HttpUtility.writeToClient(response, CommonUtility.toJson(null != workStep));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
		}
		return;
	}
	
	@RequestMapping(value = "/workstep/result", method = RequestMethod.POST)
	public void workstepResult(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		}
		
		logger.info(CommonUtility.toJson(form));
		
		WorkStep workStep = workStepService.findById(form.getId());
		
		if (null != workStep){
			if (null != workStep.getOperater()){
				HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
				return;
			}
			workStep.setOperater(form.getOperater());
			workStep.setOperatedesc(form.getOperatedesc());
			workStep.setOperatedate(form.getOperatedate());
			workStep.setUpdatedBy(loginUser.getId()+"");
			workStep.setUpdatedOn(new Date());
			
			workStep.setStepflag(StepFlag.DONE);
			
			workStep = workStepService.add(workStep);

			try{

				workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),form.getDosage());
						
			}catch(Exception e){
				e.printStackTrace();
				HttpUtility.writeToClient(response,"操作异常:"+e.getMessage());
			}
			
			
			RollingPlan rollingPlan = workStep.getRollingPlan();
			if (null != rollingPlan && PlanUtil.lastOneMark(workStep).isLastone()){
				rollingPlan.setQcman(form.getQcman());
				rollingPlan.setQcsign(form.getQcsign());
				rollingPlan.setQcdate(new Date());
				if (loginUser.isDefaultAdmin()){
					rollingPlan.setQcdate(form.getQcdate());
				}
				
				rollingPlanService.add(rollingPlan);
			}
			
			String welder = variableSetService.findValueByKey(WELDER, WELDDISTINGUISH);
			
			if (null != rollingPlan && welder.equals(workStep.getStepname())){
				rollingPlan.setWelder(Integer.parseInt(form.getOperater()));
				rollingPlan.setWelddate(new Date());
				
				rollingPlanService.add(rollingPlan);
			}
			
			HttpUtility.writeToClient(response, CommonUtility.toJson(null != workStep));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
		}
		return;
	}
	
	@RequestMapping(value = "/workstep/result/batch", method = RequestMethod.GET)
	public String workstepResultBatchUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		String operater = "";
		
		RollingPlan rollingPlan = rollingPlanService.findById(form.getId());
		if (null != rollingPlan){
			rollingPlan = rollingPlanTansferService.markBackFill(rollingPlan);
			String operaterId = rollingPlan.getConsendman();
			try {
				Integer id = Integer.parseInt(operaterId);
				
				User user = userService.findUserById(id);
				if (null != user){
					operater = user.getRealname();
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		request.setAttribute("operater", operater);
		
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_workstep_result_batch");
	}
	
	@RequestMapping(value = "/workstep/result/batch", method = RequestMethod.POST)
	public void workstepResultBatch(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		}

		RollingPlan rollingPlan = rollingPlanService.findById(form.getId());
		if (null != rollingPlan){
			rollingPlan = rollingPlanTansferService.markBackFill(rollingPlan);
			for (Integer id : rollingPlan.getBackFillWorkStepId()){
				WorkStep workStep = workStepService.findById(id);
				
				if (null != workStep){
					if (null != workStep.getOperater()){
						HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
						return;
					}
					workStep.setOperater(form.getOperater());
					workStep.setOperatedesc(form.getOperatedesc());
					workStep.setOperatedate(form.getOperatedate());
					workStep.setUpdatedBy(loginUser.getId()+"");
					workStep.setUpdatedOn(new Date());
					
					workStep.setStepflag(StepFlag.DONE);
					
					workStep = workStepService.add(workStep);
					
					try{

						workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),form.getDosage());
								
					}catch(Exception e){
						e.printStackTrace();
						HttpUtility.writeToClient(response, "操作异常:"+e.getMessage());
					}
					
					if (null != rollingPlan && PlanUtil.lastOneMark(workStep).isLastone()){
						rollingPlan.setQcman(form.getQcman());
						rollingPlan.setQcsign(form.getQcsign());
						rollingPlan.setQcdate(new Date());
						if (loginUser.isDefaultAdmin()){
							rollingPlan.setQcdate(form.getQcdate());
						}
						
						rollingPlanService.add(rollingPlan);
					}
					
					String welder = variableSetService.findValueByKey(WELDER, WELDDISTINGUISH);
					
					if (null != rollingPlan && welder.equals(workStep.getStepname())){
						rollingPlan.setWelder(Integer.parseInt(form.getOperater()));
						rollingPlan.setWelddate(new Date());
						
						rollingPlanService.add(rollingPlan);
					}
					
				}
			}
			HttpUtility.writeToClient(response, CommonUtility.toJson("true"));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
		}
		return;
	}
	
	@RequestMapping(value = "/workstep/result/multiple", method = RequestMethod.GET)
	public String workstepResultMultipleUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		String operater = "";
		Integer[] ids = form.getIds();
		
		if (null != ids && ids.length > 0){
			RollingPlan rollingPlan = rollingPlanService.findById(ids[0]);
			if (null != rollingPlan){
				rollingPlan = rollingPlanTansferService.markBackFill(rollingPlan);
				String operaterId = rollingPlan.getConsendman();
				try {
					Integer id = Integer.parseInt(operaterId);
					
					User user = userService.findUserById(id);
					if (null != user){
						operater = user.getRealname();
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		
		request.setAttribute("operater", operater);
		request.setAttribute("multiple", "multiple");
		
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_workstep_result_multiple");
	}
@RequestMapping(value = "/workstep/result/multiple", method = RequestMethod.POST)
	public void workstepResultmultiple(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		}
		
		Integer[] ids = form.getIds();
		
		if (null != ids && ids.length > 0){
			for (Integer id : ids){
				RollingPlan rollingPlan = rollingPlanService.findById(id);
				if (null != rollingPlan){
					rollingPlan = rollingPlanTansferService.markBackFill(rollingPlan);
					for (Integer workStepId : rollingPlan.getBackFillWorkStepId()){
						WorkStep workStep = workStepService.findById(workStepId);
						
						if (null != workStep){
							if (null != workStep.getOperater()){
								HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
								return;
							}
							workStep.setOperater(form.getOperater());
							workStep.setOperatedesc(form.getOperatedesc());
							workStep.setOperatedate(form.getOperatedate());
							workStep.setUpdatedBy(loginUser.getId()+"");
							workStep.setUpdatedOn(new Date());
							
							workStep.setStepflag(StepFlag.DONE);
							
							workStep = workStepService.add(workStep);
							
							try{

								workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),form.getDosage());
								
							}catch(Exception e){
								e.printStackTrace();
								HttpUtility.writeToClient(response, "操作异常:"+e.getMessage());
							}
							
							
							if (null != rollingPlan && PlanUtil.lastOneMark(workStep).isLastone()){
								rollingPlan.setQcman(form.getQcman());
								rollingPlan.setQcsign(form.getQcsign());
								rollingPlan.setQcdate(new Date());
								if (loginUser.isDefaultAdmin()){
									rollingPlan.setQcdate(form.getQcdate());
								}
								
								rollingPlanService.add(rollingPlan);
							}
							
							String welder = variableSetService.findValueByKey(WELDER, WELDDISTINGUISH);
							
							if (null != rollingPlan && welder.equals(workStep.getStepname())){
								rollingPlan.setWelder(Integer.parseInt(form.getOperater()));
								rollingPlan.setWelddate(new Date());
								
								rollingPlanService.add(rollingPlan);
							}
							
						}
					}
				}
			}
			HttpUtility.writeToClient(response, CommonUtility.toJson("true"));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
		}
		return;
	}
	
	@RequestMapping(value = "/rollingplan/result", method = RequestMethod.GET)
	public String rollingplanResultUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		logger.info("Get into modal_task_rollingplan_result");
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_rollingplan_result");
	}
	
	@RequestMapping(value = "/rollingplan/result", method = RequestMethod.POST)
	public void rollingplanResult(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		}
		
		try{

			logger.info(CommonUtility.toJson(form));
			
			RollingPlan rollingPlan = rollingPlanService.findById(form.getId());
			
			if (null != rollingPlan){
				if (null != rollingPlan.getWelder() && Isend.COMPLETE == rollingPlan.getIsend()){
					HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
					return;
				}
				rollingPlan.setWelder(Integer.parseInt(form.getWelder()));
				rollingPlan.setEnddate(form.getEnddate());
				rollingPlan.setRemark(form.getRemark());
				rollingPlan.setUpdatedBy(loginUser.getId());
				rollingPlan.setUpdatedOn(new Date());
				
				Date qcDate = new Date();
//				if (null != form.getQcdate()){
//					qcDate = form.getQcdate();
//				}
				
				if (null == rollingPlan.getWorkSteps() || rollingPlan.getWorkSteps().isEmpty()){
					rollingPlan.setIsend(Isend.COMPLETE);
					rollingPlan.setQcman(form.getQcman());
					rollingPlan.setQcsign(form.getQcsign());
					rollingPlan.setQcdate(qcDate);
					
					executePushToEnpowerServiceImpl.updateEnpowerStatus(rollingPlan, null,form.getDosage());
				} else {
					rollingPlan.setQcman(form.getQcman());
					rollingPlan.setQcsign(form.getQcsign());
					rollingPlan.setQcdate(qcDate);
				}
				
				rollingPlan = rollingPlanService.add(rollingPlan);
				
				HttpUtility.writeToClient(response, CommonUtility.toJson(null != rollingPlan));
			} else {
				HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
			}
		}catch(Exception e){
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"+"操作异常"+e.getMessage()));
		}
		return;
	}
	
	@RequestMapping(value = "/rollingplan/result/edit", method = RequestMethod.GET)
	public String rollingplanResultEditUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		return ForwardUtility.forwardAdminView("/hdService/task/modal_task_rollingplan_result_edit");
	}
	
	@RequestMapping(value = "/rollingplan/result/edit", method = RequestMethod.POST)
	public void rollingplanResultEdit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") RollingPlanForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		if (null == loginUser){
			return ;
		} 
		
		logger.info(CommonUtility.toJson(form));
		
		RollingPlan rollingPlan = rollingPlanService.findById(form.getId());
		
		if (null != rollingPlan){
			rollingPlan.setWelder(Integer.parseInt(form.getWelder()));
			rollingPlan.setEnddate(form.getEnddate());
			rollingPlan.setRemark(form.getRemark());
			rollingPlan.setUpdatedBy(loginUser.getId());
			rollingPlan.setUpdatedOn(new Date());
			
//			if (null != rollingPlan.getWorkSteps() && !rollingPlan.getWorkSteps().isEmpty()){
				rollingPlan.setQcman(form.getQcman());
				rollingPlan.setQcsign(form.getQcsign());
				if (loginUser.isDefaultAdmin()){
					rollingPlan.setQcdate(form.getQcdate());
				}
//			} 
			
			rollingPlan = rollingPlanService.add(rollingPlan);
			
			HttpUtility.writeToClient(response, CommonUtility.toJson(null != rollingPlan));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
		}
		return;
	}
}
