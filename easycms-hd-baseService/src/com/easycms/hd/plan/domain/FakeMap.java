package com.easycms.hd.plan.domain;

import com.easycms.core.form.BasicForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-05-05
 * 
 */

public class FakeMap extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = 4334165446252943396L;
	private String key;
	private String value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}