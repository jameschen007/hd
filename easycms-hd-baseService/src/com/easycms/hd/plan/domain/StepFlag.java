package com.easycms.hd.plan.domain;

public class StepFlag {
	public static String SKIP = "SKIP";
	public static String PREPARE = "PREPARE";
	public static String UNDO = "UNDO";
	public static String PAUSE = "PAUSE";
	public static String DONE = "DONE";
	public static String WITNESS = "WITNESS";
}
