package com.easycms.hd.plan.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Entity
@Table(name = "rolling_plan")
@Data
@EqualsAndHashCode(callSuper=false)
public class RollingPlan extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = -5237548512125055438L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "autoid", unique = true, nullable = false)
	private Integer id;
	
	
/* enpower	计划主键id */
	@Column(name = "enpower_plan_id", length = 50)
	private String enpowerPlanId;
//	质量计划id	qualityplanid
	@Column(name = "qualityplanid", length = 50)
	private String qualityplanid;

	@Column(name = "qualityplanno", length = 50)
	private String qualityplanno;
	
	@Column(name = "workpackageno", length = 50)
	private String workpackageno;
	
	@Column(name = "weldlistno", length = 50)
	private String weldlistno;
	
//	图纸版本号	drawSerial	draw_serial
	@Column(name = "draw_serial", length = 50)
	private String drawSerial;
	
	@Column(name = "drawno", length = 50)
	private String drawno;
	
	@Column(name = "rccm", length = 10)
	private String rccm;
	
	@Column(name = "areano", length = 10)
	private String areano;
	
	@Column(name = "unitno", length = 10)
	private String unitno;
	
	@Column(name = "weldno", length = 20)
	private String weldno;
	
	@Column(name = "speciality", length = 20)
	private String speciality;
	
	@Column(name = "materialtype", length = 20)
	private String materialtype;
	
	@Column(name = "workpoint", precision = 10, scale = 3)
	private Double workpoint;
	
	@Column(name = "qualitynum", precision = 10, scale = 3)
	private Double qualitynum;
	
	@Column(name = "worktime", precision = 10, scale = 3)
	private Double worktime;
	
	@Column(name = "consteam", length = 20)
	private Integer consteam;
	
	@Column(name = "plandate", length = 30)
	private String plandate;
	
	@Column(name = "consdate", length = 0)
	private Date consdate;
	/**
	 * 0初始、1施工、2完成、3退回,4计划，5处理中
	 */
	@Column(name = "isend", nullable = false)
	private Integer isend;
	
	@Column(name = "consendman", length = 20)
	private String consendman;
	
	@Column(name = "welder", length = 20)
	private Integer welder;
	
	@Column(name = "welder2", length = 20)
	private String welder2;
	
	@Column(name = "welddate", length = 0)
	private Date welddate;
	
	@Column(name = "enddate", length = 0)
	private Date enddate;
	
	@Column(name = "assigndate", length = 0)
	private Date assigndate;
	
	@Column(name = "doissuedate", length = 0)
	private Date doissuedate;
	
	@Column(name = "plan_finish_date", length = 0)
	private Date planfinishdate;
	
	@Column(name = "qcsign", nullable = false)
	private Integer qcsign;
	
	@Column(name = "qcman", length = 20)
	private String qcman;
	
	@Column(name = "qcdate", length = 0)
	private Date qcdate;
	
	@Column(name = "remark", length = 200)
	private String remark;
	
	@Column(name = "created_on", length = 0)
	private Date createdOn;
	
	@Column(name = "created_by", length = 40)
	private String createdBy;
	
	@Column(name = "updated_on", length = 0)
	private Date updatedOn;
	
	@Column(name = "updated_by", length = 40)
	private Integer updatedBy;
	
	@Column(name = "technology_ask", length = 1000)
	private String technologyAsk;
	
	@Column(name = "quality_risk_ctl", length = 1000)
	private String qualityRiskCtl;
	
	@Column(name = "security_risk_ctl", length = 1000)
	private String securityRiskCtl;
	
	@Column(name = "experience_feedback", length = 1000)
	private String experienceFeedback;
	
	@Column(name = "work_tool", length = 1000)
	private String workTool;
	
	@Column(name = "rollingplanflag")
	private String rollingplanflag;
	
	@Column(name = "witnessflag")
	private String witnessflag;
	
	@Column(name = "roomno")
	private String roomno;
	
	@Column(name = "systemno")
	private String systemno;
	
	@Column(name = "lineno")
	private String lineno;

	@Column(name = "projectcost")
	private String projectcost;
	@Column(name = "projectcost2")
	private String projectcost2;
/**
 * 在QC1见证回填“实际完成工程量”，两位小数的数字
 */
	@Column(name = "real_projectcost")
	private String realProjectcost;
	
	@Column(name = "specification")
	private String specification;
	
	@Column(name = "corelevel")
	private String corelevel;
	
	@Column(name = "projectunit")
	private String projectunit;
	
	@Column(name = "projectno")
	private String projectno;
	
	@Column(name = "projecttype")
	private String projectType;
	
	@Column(name = "subitem")
	private String subItem;
	
	@Column(name = "itpno")
	private String itpno;
	
	//点数 4位小数
	@Column(name = "points")
	private BigDecimal points;
	
	@Column(name = "planstartdate")
	private Date planstartdate;
	
	@Column(name = "planenddate")
	private Date planenddate;

	@Column(name = "type")
	private String type;
	/**
	 * Enpower专业 名字
	 */
	@Column(name = "enpower_type")
	private String enpowerType;
	
	@Column(name = "spot")
	private String spot;
	
	@Column(name = "conscaptain_date")
	private Date conscaptaindate;

	@Column(name = "conscaptain")
	private Integer conscaptain;
//	施工班组代码
	@Column(name = "consmonitor_code")
	private String consmonitorCode;

	@Column(name = "consmonitor_date")
	private Date consmonitordate;

	@Column(name = "consmonitor")
	private Integer consmonitor;
	@Column(name = "consmonitor_enpower")
	private String consmonitorEnpower;
	
	@Column(name = "consteam_date")
	private Date consteamdate;
	
	@Column(name = "plan_amount")
	private String planamount;
	
	@Column(name = "item_name")
	private String itemname;
	
	@Column(name = "item_no")
	private String itemno;
	
	@Column(name = "project_name")
	private String projectname;
	
	//0正常状态，1删除， 2关闭
	@Column(name = "isdel")
	private int isdel;
	/**
	 * 实际结束时间：最后一个消点的时间
	 */
	@Column(name="real_end_date")
	private Date realEndDate;
	
	@Column(name = "device_name")
	private String deviceName;
	@Column(name = "device_no")
	private String deviceNo;
	@Column(name = "device_type")
	private String deviceType;
	@Column(name = "mark_no")
	private String markNo;
	@Column(name = "unit_total_count")
	private String unitTotalCount;
	@Column(name = "unit_uncomplete_count")
	private String unitUncompleteCount;
	@Column(name = "loop_total_count")
	private String loopTotalCount;
	@Column(name = "loop_uncomplete_count")
	private String loopUncompleteCount;
	@Column(name = "rinse_name")
	private String rinseName;
	@Column(name = "rinse_type")
	private String rinseType;

//	班长名称		
	@Column(name = "consmonitor_name")
	private String consmonitorName;
//	施工队	conscaptainName	conscaptain_name
	@Column(name = "conscaptain_name")
	private String conscaptainName;
	//enpower的关联作业条目和材料信息字段，只做保存即可
	@Column(name = "pmain_id")
	private String pMainId;
	/**
"是否报量FLAG
	 * "(向app推送时一起推)  ：Y/N
	 */
	@Column(name = "is_report")
	private String isReport;
	
	/**
	 * enpower作业条目id
	 * (向app推送时一起推)
	 */
	@Column(name = "work_list_id")
	private String workListId;
	
	/**
	 * 20171110添加同步接口添加以下属性
	 * ITP类型（类别）
	 */
	@Column(name = "ITP_TYPES")
	private String ITP_TYPES;
	/**
	 * ITP模板类型（模板类型）
	 */
	@Column(name = "ITP_TYPE")
	private String ITP_TYPE;
	/**
	 * (质量计划的)专业
	 */
	@Column(name = "SPECIALTY")
	private String SPECIALTY;
	/**
	 * 版本（质量计划版本）
	 */
	@Column(name = "T_REV")
	private String T_REV;
	/**
	 * 区域 
	 */
	@Column(name = "AREA")
	private String AREA;
	/**
	 * 通知单名称（ITP模板名称(中文)）
	 */
	@Column(name = "MQP_NAME")
	private String MQP_NAME;
	/**
	 * 通知单名称（ITP模板名称(中文)）
	 */
	@Column(name = "send_enp")
	private Boolean sendEnp;

	
//--------------------额外属性	
	@Transient
	private boolean stepStart = false;
	@Transient
	private boolean noticePoint = false;
	@Transient
	private boolean wintnessUnCreate = false;
	@Transient
	private String consteamName;
	@Transient
	private String consendmanName;
	@Transient
	private Set<String> noticeType;
	@Transient
	private List<Integer> backFillWorkStepId;
	@Transient
	private boolean allBackFilled = false;
	
	@Transient
	private Integer rowStart;
	@Transient
	private Integer rowEnd;
	@Transient
	private String searchValue;

	
	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "rollingPlan")
	private List<WorkStep> workSteps;
	@Transient
	boolean sorted =false;
	public List<WorkStep> getWorkSteps(){
	    if(workSteps!=null&&!sorted){
	    	Collections.sort(workSteps); 
	    	sorted = true;
	    	
	    }
	    return workSteps;
	}
}