package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>滚动计划附属材料信息表<br>
 * <b>创建时间：</b>2017-10-23 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name = "enpower_equipment")
@Data
@EqualsAndHashCode(callSuper = false)
public class EnpowerEquipment extends BasicForm implements Serializable {
	private static final long serialVersionUID = 7991371355432184827L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	/**
	 * 滚动计划id
	 */
	@Column(name = "rolling_plan_id", nullable = false)
	private Integer rollingPlanId;
	/**
	 * 设备编码
	 */
	@Column(name = "equipment_code", nullable = false)
	private String equipmentCode;
	/**
	 * 管段号
	 */
	@Column(name = "tubulation_no", nullable = false)
	private String tubulationNo;
	/**
	 * 备注
	 */
	@Column(name = "mark", nullable = false)
	private String mark;
	/**
	 * 源id
	 */
	@Column(name = "source_id")
	private String sourceId;
	/**
	 * 图纸号
	 */
	@Column(name = "drawing_no", nullable = false)
	private String drawingNo;
	/**
	 * 当前工序号
	 */
	@Column(name = "current_work_step_no", nullable = false)
	private String currentWorkStepNo;
	/**
	 * 作业名称
	 */
	@Column(name = "work_name", nullable = false)
	private String workName;
	/**
	 * 通知单id
	 */
	@Column(name = "notice_id")
	private String noticeId;

}
