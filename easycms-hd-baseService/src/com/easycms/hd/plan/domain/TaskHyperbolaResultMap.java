package com.easycms.hd.plan.domain;

import java.io.Serializable;
import java.util.List;

public class TaskHyperbolaResultMap implements Serializable{
	private static final long serialVersionUID = -8631242595052361831L;
	private String type;
	private String date;
	private List<TaskHyperbolaResult> result;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<TaskHyperbolaResult> getResult() {
		return result;
	}
	public void setResult(List<TaskHyperbolaResult> result) {
		this.result = result;
	}
}
