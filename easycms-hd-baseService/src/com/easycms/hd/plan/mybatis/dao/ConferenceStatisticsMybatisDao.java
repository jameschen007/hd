package com.easycms.hd.plan.mybatis.dao;

import com.easycms.hd.api.domain.Statistics;

public interface ConferenceStatisticsMybatisDao {
//Conference统计信息	
	Long conferenceSendCount(Statistics statistics);
	Long conferenceReceiveCount(Statistics statistics);
	Long conferenceDraftCount(Statistics statistics);
	
	Long conferenceSendUnStartedCount(Statistics statistics);
	Long conferenceReceiveUnStartedCount(Statistics statistics);
	
	Long conferenceSendProcessingCount(Statistics statistics);
	Long conferenceReceiveProcessingCount(Statistics statistics);
	
	Long conferenceSendExpiredCount(Statistics statistics);
	Long conferenceReceiveExpiredCount(Statistics statistics);
	
	Long conferenceTotalReceiveUnReadFeedback(Statistics statistics);
	Long conferenceEachReceiveUnReadFeedback(Statistics statistics);
	
	Long conferenceTotalSendUnReadFeedback(Statistics statistics);
	Long conferenceEachSendUnReadFeedback(Statistics statistics);
//Notification统计信息	
	Long notificationSendCount(Statistics statistics);
	Long notificationReceiveCount(Statistics statistics);
	Long notificationDraftCount(Statistics statistics);
	
	Long notificationSendUnStartedCount(Statistics statistics);
	Long notificationReceiveUnStartedCount(Statistics statistics);
	
	Long notificationSendProcessingCount(Statistics statistics);
	Long notificationReceiveProcessingCount(Statistics statistics);
	
	Long notificationSendExpiredCount(Statistics statistics);
	Long notificationReceiveExpiredCount(Statistics statistics);
	
	Long notificationTotalReceiveUnReadFeedback(Statistics statistics);
	Long notificationEachReceiveUnReadFeedback(Statistics statistics);
	
	Long notificationTotalSendUnReadFeedback(Statistics statistics);
	Long notificationEachSendUnReadFeedback(Statistics statistics);
}
