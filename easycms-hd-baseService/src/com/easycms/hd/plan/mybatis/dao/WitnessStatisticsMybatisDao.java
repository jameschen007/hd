package com.easycms.hd.plan.mybatis.dao;

import com.easycms.hd.api.domain.Statistics;
import com.easycms.management.user.domain.User;

public interface WitnessStatisticsMybatisDao {
	
	//临时 将所有绩效考核数据进行清理
	void clearKpi1();
	void clearKpi2();
	void clearKpi3();
	void clearKpi4();
	void clearKpi5();
	
	//临时 滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长
	void initRollingPlan1();
	void initRollingPlan2();
	void initRollingPlan3();
	void initRollingPlan4();
	void initRollingPlan5(User user);
	void initRollingPlan6();
	void initRollingPlan7();
	/**初始化滚运计划，按id*/
	void initRollingPlan2del(User user);
	void initRollingPlan2upd(User user);
	
//班长统计信息	
	Long monitorCompletedCount(Statistics statistics);
	Long monitorUnCompletedCount(Statistics statistics);
//组长统计信息	
	Long teamCompletedCount(Statistics statistics);
	Long teamUnCompletedCount(Statistics statistics);
	Long teamLaunchedCount(Statistics statistics);
//QC组员统计信息	
	Long qcMemberCompletedCount(Statistics statistics);
	Long qcMemberUnCompletedCount(Statistics statistics);
//QC2下属统计信息	
	Long qc2MemberUnAssignCount(Statistics statistics);
	Long qc2MemberCompletedCount(Statistics statistics);
	Long qc2MemberUnCompletedCount(Statistics statistics);
//QC1得到见证统计信息
	Long qc1MemberNoticePointCount(Statistics statistics);
//QC2得到见证统计信息
	Long qc2MemberNoticePointCount(Statistics statistics);
//QC2下属得到见证统计信息
	Long qc2MemberDownLineNoticePointCount(Statistics statistics);
}
