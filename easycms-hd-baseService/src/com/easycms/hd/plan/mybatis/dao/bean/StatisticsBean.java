package com.easycms.hd.plan.mybatis.dao.bean;

import lombok.Data;

@Data
public class StatisticsBean {

	
	//总待办数、待办派数、已解决数、未解决数
	
	//总问题数量、待指派
	private Integer all;
	
/**
 * 统计的谁的数量
 */
	private Integer id;
	//待指派的数量
	private Integer assign;
	//已回复（已提参方案）
	private Integer reply;
	//已解决的数量
	private Integer solved;
	//未解决的数量
	private Integer unsolved;
	//未处理
	private Integer pre;
	/**
	 * 已处理
	 * @return
	 */
	private Integer done;
}
