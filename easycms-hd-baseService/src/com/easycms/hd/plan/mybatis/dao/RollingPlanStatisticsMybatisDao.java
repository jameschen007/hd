package com.easycms.hd.plan.mybatis.dao;

import com.easycms.hd.api.domain.Statistics;

public interface RollingPlanStatisticsMybatisDao {
//班长统计信息	
	Long monitorCompletedCount(Statistics statistics);
	Long monitorProgressingCount(Statistics statistics);
	Long monitorUnProgressingCount(Statistics statistics);
	Long monitorPauseCount(Statistics statistics);
	Long monitorUnAssignCount(Statistics statistics);
	/** 队长看班长的未施工：已分给组长了，组长还没有动过的 */
	Long monitorUnProgressing(Statistics statistics);
//班长统计信息	
	Long teamCompletedCount(Statistics statistics);
	Long teamProgressingCount(Statistics statistics);
	Long teamUnProgressingCount(Statistics statistics);
	Long teamPauseCount(Statistics statistics);
}
