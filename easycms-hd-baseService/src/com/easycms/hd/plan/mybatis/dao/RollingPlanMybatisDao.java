package com.easycms.hd.plan.mybatis.dao;

import java.util.List;

import com.easycms.hd.plan.domain.RollingPlan;

public interface RollingPlanMybatisDao {
	Long getCount();
	List<RollingPlan> findAll();
}
