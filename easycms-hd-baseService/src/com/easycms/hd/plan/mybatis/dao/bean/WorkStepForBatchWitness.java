package com.easycms.hd.plan.mybatis.dao.bean;

import lombok.Data;

@Data
public class WorkStepForBatchWitness {
	/**
	 * 工序以逗号分隔
	 */
	private String workStepIds;
	/**
	 * 工序号　enp
	 */
	private String stepIdentifier;

	/**
	 * 工序名称
	 */
	private String stepname;
	
	
	/**
	 * QC1
	 */
	private String noticeQC1;
	/**
	 * QC1
	 */
	private String noticeQC2;
	/**
	 * CZEC QC
	 */
	private String noticeCZECQC;
	/**
	 * CZEC QA
	 */
	private String noticeCZECQA;
	/**
	 * PAEC
	 */
	private String noticePAEC;
}
