package com.easycms.hd.plan.mybatis.dao;

import java.util.List;

import com.easycms.hd.statistics.domain.StatisticsHse3month;

public interface StatisticsHse3monthDao {
	/**
	 * 近90天（三月）总体隐患数量
	 * @return
	 */
	public StatisticsHse3month find();
	/**
	 * 近90天（三月）总体隐患数量 按队部
	 * @return
	 */
	public List<StatisticsHse3month> findWithCaptain();

}
