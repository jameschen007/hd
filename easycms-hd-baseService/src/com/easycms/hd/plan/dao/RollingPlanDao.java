package com.easycms.hd.plan.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.RollingPlan;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface RollingPlanDao extends Repository<RollingPlan,Integer>,BasicDao<RollingPlan> {
	@Modifying
	@Query("DELETE FROM RollingPlan rp WHERE rp.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Query("FROM RollingPlan rp WHERE rp.id IN (?1)")
	List<RollingPlan> findByIds(Integer[] ids);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam=?2 and rp.id IN (?1)")
	List<RollingPlan> findConsteamByRPIds(Integer[] ids, String consteam);
	
	@Query("FROM RollingPlan rp WHERE rp.consendman=?2 and rp.id IN (?1)")
	List<RollingPlan> findConsendmanByRPIds(Integer[] ids, String consendman);
	
	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consteam=?2, rp.isend=?3, rp.assigndate=?4, rp.planfinishdate=?5 WHERE rp.id IN (?1)")
	int assign(Integer[] ids, String consteam, Integer isend, Date assigndate, Date planfinishdate);
	
	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consendman=?2, rp.plandate=?3, rp.isend=?4 WHERE rp.id IN (?1)")
	int assignEndMan(Integer[] ids, String consendman, String plandate, Integer isend);
	
	@Query("FROM RollingPlan rp WHERE rp.id IN (?1)")
	List<RollingPlan> findByIDs(Integer[] ids);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam IN (?1)")
	List<RollingPlan> findByConsteams(String[] ids);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam IN (?1) and rp.isend=?2")
	List<RollingPlan> findByConsteamsAndIsend(String[] ids, Integer isend);
	
	@Query("FROM RollingPlan rp WHERE rp.consendman IN (?1)")
	List<RollingPlan> findByConsEndman(String[] ids);
	
	@Query("FROM RollingPlan rp WHERE rp.drawno=?1 and rp.weldno=?2")
	RollingPlan findByDrawnoAndWeldno(String drawno, String weldno);
//-------------------------------------------------------------[START] 以下为一些完成未完成的各类情况-----------------------------------------------------------------------------
	//-------------------------------Consteam------------------------------------------	
	Page<RollingPlan> findByConsteamIsNull(Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam is null and (rp.drawno like ?1 or rp.weldno like ?1)")
	Page<RollingPlan> findByConsteamIsNull(Pageable pageable, String keyword);
	
	List<RollingPlan> findByConsteamIsNull();
	
	Page<RollingPlan> findByConsteamIsNotNull(Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam is not null and (rp.drawno like ?1 or rp.weldno like ?1)")
	Page<RollingPlan> findByConsteamIsNotNull(Pageable pageable, String keyword);
	
	List<RollingPlan> findByConsteamIsNotNull();
	
	List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull();
	
	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull(Pageable pageable);
	
	List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull();
	
	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull(Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam is not null and rp.consendman is null and (rp.drawno like ?1 or rp.weldno like ?1)")
	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull(Pageable pageable, String keyword);
	
	List<RollingPlan> findByConsteamInAndConsendmanIsNull(List<String> consteams);
	
	List<RollingPlan> findByConsteamInAndConsendmanIsNotNull(List<String> consteams);
	
	Page<RollingPlan> findByConsteamIn(List<String> consteams, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?1) and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsteamIn(List<String> consteams, String keyword, Pageable pageable);
	
	Page<RollingPlan> findByConsteamNotIn(List<String> consteams, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam not in (?1) and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsteamNotIn(Pageable pageable, List<String> consteams, String keyword);
	
	Page<RollingPlan> findByConsteamNotInAndConsendmanIsNull(List<String> consteams, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam not in (?1) and rp.consendman is null and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsteamNotInAndConsendmanIsNull(List<String> consteams, String keyword, Pageable pageable);
//-------------------------------Consendman------------------------------------------		
	Page<RollingPlan> findByConsendmanIsNull(Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consendman is null and (rp.drawno like ?1 or rp.weldno like ?1)")
	Page<RollingPlan> findByConsendmanIsNull(Pageable pageable, String keyword);
	
	Page<RollingPlan> findByConsendmanIsNotNull(Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consendman is not null and (rp.drawno like ?1 or rp.weldno like ?1)")
	Page<RollingPlan> findByConsendmanIsNotNull(Pageable pageable, String keyword);
	
	List<RollingPlan> findByConsendmanIsNull();
	
	List<RollingPlan> findByConsendmanIn(List<String> consendman);
	
	List<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend);
	
	Page<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend, Pageable pageable);
	
	List<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend);
	
	Page<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend, Pageable pageable);
	
	List<RollingPlan> findByConsendmanInAndIsend(List<String> consendman, Integer isend);
	
	Page<RollingPlan> findByConsendmanInAndIsend(List<String> consendman, Integer isend, Pageable pageable);
	
	List<RollingPlan> findByConsendmanInAndIsendNot(List<String> consendman, Integer isend);
	
	Page<RollingPlan> findByConsendmanInAndIsendNot(List<String> consendman, Integer isend, Pageable pageable);
	
	Page<RollingPlan> findByConsteamInAndConsendmanIsNull(List<String> consendman, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?1) and rp.consendman is null and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsteamInAndConsendmanIsNull(List<String> consendman, String keyword, Pageable pageable);
	
	@Query(value="SELECT DISTINCT a from RollingPlan a where a.id not in (select DISTINCT b.rollingPlan.id from WorkStep b where b.stepflag in ('DONE')) and a.consteam in (?1) and a.consendman is not null")
	Page<RollingPlan> findByConsteamInAndConsendmanIsNotNull(List<String> consendman, Pageable pageable);
	
	@Query(value="SELECT DISTINCT a from RollingPlan a where a.id not in (select DISTINCT b.rollingPlan.id from WorkStep b where b.stepflag in ('DONE')) and a.consteam in (?1) and a.consendman is not null and (a.drawno like ?2 or a.weldno like ?2)")
	Page<RollingPlan> findByConsteamInAndConsendmanIsNotNull(List<String> consendman, String keyword, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam IS NOT NULL and rp.consendman IN (?1) and rp.enddate IS NULL and rp.qcdate IS NULL")
	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanInAndWelderIsNull(List<String> consendman, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam IS NOT NULL and rp.consendman IN (?1) and rp.enddate IS NULL and rp.qcdate IS NULL and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanInAndWelderIsNull(List<String> consendman, String keyword, Pageable pageable);
	
	Page<RollingPlan> findByConsendmanInAndWelderIsNotNull(List<String> consendman, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consendman in (?1) and rp.welder is not null and (rp.drawno like ?2 or rp.weldno like ?2)")
	Page<RollingPlan> findByConsendmanInAndWelderIsNotNull(List<String> consendman, String keyword, Pageable pageable);
	
	Page<RollingPlan> findByConsendmanIn(List<String> consendman, Pageable pageable);
	
	Page<RollingPlan> findByAndConsendmanIn(List<String> consendman, Pageable pageable);
	
	Page<RollingPlan> findByConsteamIsNullAndConsendmanNotIn(List<String> consendman, Pageable pageable);
	
	Page<RollingPlan> findByConsendmanNotIn(List<String> consendman, Pageable pageable);
//-------------------------------------------------------------[END] 以上为一些完成未完成的各类情况-------------------------------------------------------------------------------------------------------------
	
	
//------------------------------------------------------[2017-09-11]---------------------------------------------------------------
	@Query("FROM RollingPlan rp WHERE rp.type = ?1 and isdel != 1")
	Page<RollingPlan> findByType(String type, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.type = ?1 and rp.consmonitor in (?2) and rp.consteam is null and isdel != 1")
	Page<RollingPlan> findByPageMonitorUnAssign(String type, List<Integer> monitorId, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.type = ?1 and rp.consmonitor in (?2) and rp.consteam is not null and isdel != 1")	
	Page<RollingPlan> findByPageMonitorAssigned(String type, List<Integer> monitorId, Pageable pageable);

	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consteam=?2, rp.isend=?3, rp.consteamdate=?4, rp.planfinishdate=?5, rp.updatedBy=?6 WHERE rp.id IN (?1) and rp.consteam is null and rp.isend != 2 and isdel != 1")
	int assignTeam(Integer[] ids, Integer consteam, Integer isend, Date consteamdate, Date planfinishdate, Integer operater);

	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.isend = 2 and isdel != 1")
	Page<RollingPlan> findTeamCompletedByPageWithType(String type, List<Integer> teamIds, Pageable pageable);

	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and "
			+ "((rp.witnessflag is null and rp.rollingplanflag = 'COMPLETED') or (rp.rollingplanflag is null and rp.witnessflag = 'LAUNCHED')  or (rp.rollingplanflag = 'COMPLETED' and rp.witnessflag = 'LAUNCHED')) "
			+ "and rp.isend != 2 and isdel != 1")
	Page<RollingPlan> findTeamProgressingByPageWithType(String type, List<Integer> teamIds, Pageable pageable);

	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.rollingplanflag is null and rp.isend != 2 and "
			+ "(rp.witnessflag is null or rp.witnessflag = '') and isdel != 1")
	Page<RollingPlan> findTeamUnProgressByPageWithType(String type, List<Integer> teamIds, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.rollingplanflag = 'PROBLEM' and rp.isend != 2 and isdel != 1")
	Page<RollingPlan> findTeamPausedByPageWithType(String type, List<Integer> teamIds, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.isend != 2 and isdel != 1")
	Page<RollingPlan> findTeamUnCompleteByPageWithType(String type, List<Integer> teamIds, Pageable pageable);

	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.welder=?2, rp.welddate=?3 WHERE rp.id = ?1 and isdel != 1")
	int backFill(Integer id, Integer welder, Date welddate);

	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consteam=?2, rp.planfinishdate=?3, rp.updatedBy=?4 WHERE rp.id IN (?1) and rp.consteam is not null and rp.isend != 2 and isdel != 1")
	int reAssignTeam(Integer[] ids, Integer toUserId, Date planfinishdate, Integer operater);
	
	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consteam=?2, rp.updatedBy=?3 WHERE rp.id IN (?1) and rp.consteam is not null and rp.isend != 2 and isdel != 1")
	int reAssignTeam(Integer[] ids, Integer toUserId, Integer operater);

	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.consteam=null, rp.updatedBy=?2 WHERE rp.id IN (?1) and rp.consteam is not null and rp.isend != 2 and isdel != 1")
	int releaseTeam(Integer[] ids, Integer operater);

	@Query("FROM RollingPlan rp WHERE rp.consmonitor in (?2) and rp.type = ?1 and isdel != 1")
	Page<RollingPlan> findByPageConsmonitor(String type, List<Integer> monitorId, Pageable pageable);

	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.isdel=1 WHERE rp.id IN (?1)")
	int deleteRollingPlan(Integer[] ids);
	
//-------------------------------------------------[2017-10-30为全局关键词搜索而添加的方法]-----------------------------------------------------------
	String searchCondition = " and (rp.qualityplanno like ?3 or rp.weldlistno like ?3 or rp.itemname like ?3 or rp.itemno like ?3 or rp.projectname like ?3 "
			+ " or rp.drawno like ?3 or rp.drawSerial like ?3 or rp.corelevel like ?3 or rp.itpno like ?3 or rp.lineno like ?3 or rp.materialtype like ?3 "
			+ " or rp.points like ?3 or rp.projectcost like ?3 or rp.projectno like ?3 or rp.projectunit like ?3 or rp.roomno like ?3 or rp.specification like ?3 "
			+ " or rp.systemno like ?3 or rp.workpackageno like ?3 or rp.weldno like ?3 or rp.projectType like ?3 or rp.unitno like ?3 or rp.subItem like ?3 "
			+ " or rp.spot like ?3 or rp.planamount like ?3) ";
	
	@Query("FROM RollingPlan rp WHERE rp.type = ?1 and rp.consmonitor in (?2) and rp.consteam is not null and isdel != 1 " + searchCondition)	
	Page<RollingPlan> findByPageMonitorAssignedWithKeyword(String type, List<Integer> monitorId, String keyword, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.type = ?1 and rp.consmonitor in (?2) and rp.consteam is null and isdel != 1 " + searchCondition)
	Page<RollingPlan> findByPageMonitorUnAssignWithKeyword(String type, List<Integer> monitorIds, String keyword,
			Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.isend = 2 and isdel != 1 " + searchCondition)
	Page<RollingPlan> findTeamCompletedByPageWithTypeAndKeyword(String type, List<Integer> teamIds, String keyword, Pageable pageable);

	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and "
			+ "((rp.witnessflag is null and rp.rollingplanflag = 'COMPLETED') or (rp.rollingplanflag is null and rp.witnessflag = 'LAUNCHED') or (rp.rollingplanflag = 'COMPLETED' and rp.witnessflag = 'LAUNCHED')) "
			+ "and rp.isend != 2 and isdel != 1 " + searchCondition)
	Page<RollingPlan> findTeamProgressingByPageWithTypeAndKeyword(String type, List<Integer> teamIds, String keyword, Pageable pageable);

	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.rollingplanflag is null and rp.isend != 2 and "
			+ "(rp.witnessflag is null or rp.witnessflag = '') and isdel != 1 " + searchCondition)
	Page<RollingPlan> findTeamUnProgressByPageWithTypeAndKeyword(String type, List<Integer> teamIds, String keyword, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.rollingplanflag = 'PROBLEM' and rp.isend != 2 and isdel != 1 " + searchCondition)
	Page<RollingPlan> findTeamPausedByPageWithTypeAndKeyword(String type, List<Integer> teamIds, String keyword, Pageable pageable);
	
	@Query("FROM RollingPlan rp WHERE rp.consteam in (?2) and rp.type = ?1 and rp.isend != 2 and isdel != 1 " + searchCondition)
	Page<RollingPlan> findTeamUnCompleteByPageWithTypeAndKeyword(String type, List<Integer> teamIds, String keyword, Pageable pageable);

	@Query("FROM RollingPlan rp WHERE rp.consmonitor in (?2) and rp.type = ?1 and isdel != 1" + searchCondition)
	Page<RollingPlan> findByPageConsmonitorWithKeyword(String type, List<Integer> monitorId, String keyword, Pageable pageable);
	
	@Modifying
	@Query("UPDATE RollingPlan rp SET rp.welder2=?2, rp.welddate=?3 WHERE rp.id = ?1 and isdel != 1")
	int backFillV2(Integer id, String welder, Date welddate);
	
	List<RollingPlan> findByWorkListId(String workListId);

	List<RollingPlan> findByWorkListIdAndWeldno(String workListId,String weldno);
}
