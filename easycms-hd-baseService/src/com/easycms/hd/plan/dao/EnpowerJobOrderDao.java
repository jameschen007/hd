package com.easycms.hd.plan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.EnpowerJobOrder;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface EnpowerJobOrderDao extends Repository<EnpowerJobOrder,Integer>,BasicDao<EnpowerJobOrder> {

	@Query("from EnpowerJobOrder u where u.rollingPlanId=?1")
	List<EnpowerJobOrder> findByRollingPlanId(Integer rollingPlanId);
}
