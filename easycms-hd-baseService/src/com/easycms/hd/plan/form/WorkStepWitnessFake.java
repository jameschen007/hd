package com.easycms.hd.plan.form;

import java.io.Serializable;
import java.util.Date;

public class WorkStepWitnessFake implements Serializable {
	private static final long serialVersionUID = -7723253168304541495L;
	private Integer id;
	private String witness;
	private String witnessName;
	private String witnesser;
	private String witnesserName;
	private String witnessdes;
	private String witnessaddress;
	private Date witnessdate;
	private Integer status;
	private Integer isok;
	private String remark;
	private String triggerName;
	private String noticeType;
	private String noticePoint;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String noticeresultdesc;
	
	public String getNoticeresultdesc() {
		return noticeresultdesc;
	}
	public void setNoticeresultdesc(String noticeresultdesc) {
		this.noticeresultdesc = noticeresultdesc;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getWitness() {
		return witness;
	}
	public void setWitness(String witness) {
		this.witness = witness;
	}
	public String getWitnessdes() {
		return witnessdes;
	}
	public void setWitnessdes(String witnessdes) {
		this.witnessdes = witnessdes;
	}
	public String getWitnessaddress() {
		return witnessaddress;
	}
	public void setWitnessaddress(String witnessaddress) {
		this.witnessaddress = witnessaddress;
	}
	public Date getWitnessdate() {
		return witnessdate;
	}
	public void setWitnessdate(Date witnessdate) {
		this.witnessdate = witnessdate;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTriggerName() {
		return triggerName;
	}
	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getIsok() {
		return isok;
	}
	public void setIsok(Integer isok) {
		this.isok = isok;
	}
	public String getWitnesser() {
		return witnesser;
	}
	public void setWitnesser(String witnesser) {
		this.witnesser = witnesser;
	}
	public String getNoticeType() {
		return noticeType;
	}
	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}
	public String getWitnesserName() {
		return witnesserName;
	}
	public void setWitnesserName(String witnesserName) {
		this.witnesserName = witnesserName;
	}
	public String getNoticePoint() {
		return noticePoint;
	}
	public void setNoticePoint(String noticePoint) {
		this.noticePoint = noticePoint;
	}
	public String getWitnessName() {
		return witnessName;
	}
	public void setWitnessName(String witnessName) {
		this.witnessName = witnessName;
	}
}