package com.easycms.hd.plan.form;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class AssignForm implements Serializable{
	private static final long serialVersionUID = 6598344337491269084L;
	private Integer[] ids;
	private String[] consteams;
	private Integer teamId;
	private String endManId;
	private Integer id;
	private String filter;
	private Date startTime;
	private Date endTime;
	private Date planfinishdate;
	
	private String customRole;
}
