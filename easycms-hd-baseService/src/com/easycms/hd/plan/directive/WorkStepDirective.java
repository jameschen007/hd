package com.easycms.hd.plan.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.management.user.domain.User;

/**
 * 获取工序步骤列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 */
public class WorkStepDirective extends BaseDirective<WorkStep>  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WorkStepDirective.class);
	
	@Autowired
	private WorkStepService workStepService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected WorkStep field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			WorkStep workStep = workStepService.findById(id);
			if (null == workStep.getRollingPlan()){
				return null;
			}

			return PlanUtil.lastOneMark(workStep);
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStep> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<WorkStep> pager,
			Map<String, Object> envParams) {
		String type =  (String) ContextUtil.getSession().getAttribute("type");
		User user =  ContextUtil.getCurrentLoginUser();
		
		String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		
		// 查询列表
		if (null != custom && custom.equals("mylaunch")){
			pager = workStepService.findByPageTeamAll(type, user.getId(), pager);
			logger.info("==> WorkStep length [" + pager.getDatas().size() + "]");
			return pager.getDatas();
		}
		
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStep> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}
}
