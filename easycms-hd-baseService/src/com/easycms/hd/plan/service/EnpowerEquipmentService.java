package com.easycms.hd.plan.service;

import com.easycms.hd.plan.domain.EnpowerEquipment;

public interface EnpowerEquipmentService {

	public boolean update(EnpowerEquipment rollingPlan) ;

	public EnpowerEquipment add(EnpowerEquipment rollingPlan) ;

	public EnpowerEquipment findById(Integer id) ;

	public EnpowerEquipment findByRollingPlanId(Integer id);
 
}
