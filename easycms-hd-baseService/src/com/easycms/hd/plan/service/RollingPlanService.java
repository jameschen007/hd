package com.easycms.hd.plan.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.management.user.domain.User;

public interface RollingPlanService {

	List<RollingPlan> findAll();

	List<RollingPlan> findAll(RollingPlan condition);
/**所有工序都取消的时候　Witnessflag　置为空*/
	public boolean updateWitnessflagNull(RollingPlan rollingPlan);
	/**
	 * 查找分页
	 * 
	 * @param pageable
	 * @return
	 */

	Page<RollingPlan> findPage(Page<RollingPlan> page);

	public Page<RollingPlan> findPage(Page<RollingPlan> page, String condition,
			String key, String[] value);
	
	public Page<RollingPlan> findPageByKeyWords(Page<RollingPlan> page, String condition,
			String key, String[] value, String keyword);

	Page<RollingPlan> findPage(RollingPlan condition, Page<RollingPlan> page);
	Page<RollingPlan> findPage(RollingPlan condition,RollingPlan andOr, Page<RollingPlan> page);

	/**
	 * 根据ID查找
	 * 
	 * @param id
	 * @return
	 */
	RollingPlan findById(Integer id);

	List<RollingPlan> findByIds(Integer[] ids);

	List<RollingPlan> findConsteamByRPIds(Integer[] ids, String consteam);

	List<RollingPlan> findConsendmanByRPIds(Integer[] ids, String consendman);

	boolean delete(Integer id);

	boolean delete(Integer[] ids);

	/**
	 * 更新滚动计划
	 * 
	 * @param rollingPlan
	 * @return
	 */
	boolean update(RollingPlan rollingPlan);

	/**
	 * 添加滚动计划
	 * 
	 * @param rollingPlan
	 * @return
	 */
	RollingPlan add(RollingPlan rollingPlan);

	boolean assign(Integer[] ids, String consteam, Integer isend,
			Date assigndate, Date planfinishdate);

	boolean assignEndMan(Integer[] ids, String consendman, String plandate,
			Integer isend);

	RollingPlan findByDrawnoAndWeldno(String drawno, String weldno);

	List<RollingPlan> findByIDs(Integer[] ids);

	List<RollingPlan> findByConsteams(String[] ids);

	List<RollingPlan> findByConsteamIsNull();

	List<RollingPlan> findByConsteamIsNotNull();

	List<RollingPlan> findByConsendmanIsNull();

	List<RollingPlan> findByConsendmanIn(List<String> consendman);

	List<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend);

	List<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend);

	List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull();

	List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull();

	List<RollingPlan> findByConsteamInAndConsendmanIsNotNull(List<String> ids);

	List<RollingPlan> findByConsteamInAndConsendmanIsNull(List<String> ids);

	List<RollingPlan> findByConsendmanInAndIsend(List<String> ids, Integer isend);

	List<RollingPlan> findByConsendmanInAndIsendNot(List<String> ids,
			Integer isend);

	List<RollingPlan> findByConsteamsAndIsend(String[] ids, Integer isend);

	List<RollingPlan> findByConsEndman(String[] ids);

	Page<RollingPlan> findByConsteamIsNotNull(Page<RollingPlan> page);

	Page<RollingPlan> findByConsteamIsNull(Page<RollingPlan> page);

	Page<RollingPlan> findByConsteamInAndConsendmanIsNotNull(List<String> ids,
			Page<RollingPlan> page);

	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull(
			Page<RollingPlan> page);

	Page<RollingPlan> findByConsteamInAndConsendmanIsNull(List<String> ids,
			Page<RollingPlan> page);

	Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull(
			Page<RollingPlan> page);

	Page<RollingPlan> findByConsendmanInAndIsendNot(List<String> ids,
			Integer isend, Page<RollingPlan> page);

	Page<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend,
			Page<RollingPlan> page);

	Page<RollingPlan> findByConsendmanInAndIsend(List<String> ids,
			Integer isend, Page<RollingPlan> page);

	Page<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend,
			Page<RollingPlan> page);

//------------------------------------------------------[2017-09-11]---------------------------------------------------------------

	/**
	 * 按类型查找所有的滚动计划
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findByPageWithType(String type, Page<RollingPlan> page);

	/**
	 * 班长已分派的滚动计划
	 * @param type
	 * @param monitorId
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findByPageMonitorAssigned(String type, List<Integer> monitorId, Page<RollingPlan> page);
	
	/**
	 * 班长已分派的滚动计划--带全局搜索条件
	 * @param type
	 * @param monitorId
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findByPageMonitorAssignedWithKeyword(String type, List<Integer> monitorId, String keyword, Page<RollingPlan> page);
	

	/**
	 * 班长未分派的滚动计划
	 * @param type
	 * @param monitorId
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findByPageMonitorUnAssign(String type, List<Integer> monitorId, Page<RollingPlan> page);
	
	/**
	 * 班长未分派的滚动计划--带全局搜索条件
	 * @param type
	 * @param monitorId
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findByPageMonitorUnAssignWithKeyword(String type, List<Integer> monitorIds, String keyword,
			Page<RollingPlan> page);

	/**
	 * 班长分派滚动计划到组长
	 * @param ids
	 * @param consteam
	 * @param isend
	 * @param consteamdate
	 * @param planfinishdate
	 * @return
	 */
	boolean assignTeam(Integer[] ids, Integer consteam, Integer isend, Date consteamdate, Date planfinishdate, Integer operater) throws Exception;

	/**
	 * 组长查询已完成的滚动计划
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamCompletedByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page);
	
	/**
	 * 组长查询已完成的滚动计划--带全局搜索条件
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamCompletedByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword, Page<RollingPlan> page);

	/**
	 * 组长查询施工中的滚动计划
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamProgressingByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page);
	
	/**
	 * 组长查询施工中的滚动计划--带全局搜索条件
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamProgressingByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword, Page<RollingPlan> page);
	
	/**
	 * 组长查询未施工的滚动计划
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamUnProgressByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page);
	
	/**
	 * 组长查询未施工的滚动计划--带全局搜索条件
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamUnProgressByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword, Page<RollingPlan> page);
	
	/**
	 * 组长查询停滞中的滚动计划
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamPausedByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page);
	
	/**
	 * 组长查询停滞中的滚动计划--带全局搜索条件
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamPausedByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword, Page<RollingPlan> page);

	/**
	 * 组长查询未完成的滚动计划
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamUnCompleteByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page);
	
	/**
	 * 组长查询未完成的滚动计划--带全局搜索条件
	 * @param teamIds
	 * @param type
	 * @param page
	 * @return
	 */
	Page<RollingPlan> findTeamUnCompleteByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword, Page<RollingPlan> page);
	
	/**
	 * 组长回填滚动计划
	 * @param ids
	 * @param consteam
	 * @param isend
	 * @param consteamdate
	 * @param planfinishdate
	 * @return
	 */
	boolean backFill(Integer id, Integer welder, Date welddate);

	/**
	 * 改派班组信息
	 * @param ids
	 * @param fromUserId
	 * @param toUserId
	 */
	boolean reAssignTeam(Integer[] ids, Integer userId, Date planfinishdate, Integer operater);
	
	/**
	 * 改派班组信息
	 * @param ids
	 * @param fromUserId
	 * @param toUserId
	 */
	boolean reAssignTeam(Integer[] ids, Integer userId, Integer operater);

	/**
	 * 解派班组信息
	 * @param ids
	 * @return
	 */
	boolean releaseTeam(Integer[] ids, Integer operater);

	/**
	 * 班长拿到其组下所有的计划
	 * @param type
	 * @param id
	 * @param pager
	 * @return
	 */
	Page<RollingPlan> findByPageConsmonitor(String type, List<Integer> monitorId, Page<RollingPlan> page);
	Page<RollingPlan> findByPageConsmonitorWithKeyword(String type, List<Integer> monitorId, String keyword, Page<RollingPlan> page);

	/**
	 * 队长标记删除某个滚动计划
	 * @param array
	 * @return
	 */
	boolean deleteRollingPlan(Integer[] ids);

	/**
	 * 获取滚计划的班长
	 * 首先取对象上面的值，
	 * 首先按滚动计划查
	 * 其次按工序查，
	 * 最后使用见证查
	 * @return
	 */
	public User getConsmoniterBy(RollingPlan plan,WorkStep workStep,WorkStepWitness witness,Integer rollingPlanId,Integer workStepId,Integer witnessId);
	/**
	 * 获取滚计划的班长 id
	 * 首先取对象上面的值，
	 * 首先按滚动计划查
	 * 其次按工序查，
	 * 最后使用见证查
	 * @return
	 */
	public Integer getConsmoniterIdBy(RollingPlan plan,WorkStep workStep,WorkStepWitness witness,Integer rollingPlanId,Integer workStepId,Integer witnessId);

	boolean backFillV2(Integer id, String welder, Date welddate);
	public List<RollingPlan> findByWorkListId(String workListId);

	public List<RollingPlan> findByWorkListIdAndWeldno(String workListId,String weldno);

	/**
	 * 根据其中的工序id查询出滚动计划
	 * @param stepForms
	 * @return
	 */
	public List<WitingPlan> findByWorkSteps(Set<WorkStepWitnessItemsForm> stepForms);
}
