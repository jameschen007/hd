package com.easycms.hd.plan.service;

import com.easycms.hd.plan.domain.PushLog;

public interface PushLogService {
	PushLog insert(PushLog pushLog);
}
