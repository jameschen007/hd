package com.easycms.hd.plan.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepenp;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.form.WorkStepWitnessFake;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepTansferService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

@Service("workStepTansferService")
public class WorkStepTansferServiceImpl implements WorkStepTansferService{
	private static String WITNESS_TYPE = "witnesstype";
	private static String WITNESS_TEAM = "witnessTeam";
	
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private VariableSetService variableSetService;
	
	@Override
	public List<WorkStep> transferWorkStep(List<WorkStep> workSteps){
		if (null != workSteps){
			for(WorkStep workStep : workSteps){
				workStep = transferWorkStep(workStep);
			}
		}
		return workSteps;
	}

	@Override
	public WorkStep transferWorkStep(WorkStep workStep) {
		if (null != workStep){
			if (null != workStep.getRollingPlan()){
				RollingPlan rollingPlan = workStep.getRollingPlan();
				
				if (null != rollingPlan.getConsteam() && !"".equals(rollingPlan.getConsteam())){
					Department department = departmentService.findById(rollingPlan.getConsteam());
					
					if (null != department){
						rollingPlan.setConsteamName(department.getName());
					} else {
						rollingPlan.setConsteamName(null);
					}
				}
				
				if (null != rollingPlan.getConsendman() && !"".equals(rollingPlan.getConsendman())){
					User user = userService.findUserById(Integer.parseInt(rollingPlan.getConsendman()));
					
					if (null != user){
						rollingPlan.setConsendmanName(user.getRealname());
					} else {
						rollingPlan.setConsendmanName(null);
					}
				}
				
				List<WorkStep> wsList = rollingPlan.getWorkSteps();
				if (null != wsList && wsList.size() > 0){
					for (WorkStep ws : wsList){
						if (!ws.getStepflag().equals(StepFlag.UNDO) && !ws.getStepflag().equals(StepFlag.PREPARE)){
							rollingPlan.setStepStart(true);
							break;
						}
					}
				}
				
				workStep.setRollingPlan(rollingPlan);
			}
			
			
		}
		return workStep;
	}
	
	@Override
	public WorkStep addWitnessInfo(WorkStep workStep){
		if (null == workStep){
			return null;
		}
		Integer wsId = workStep.getId();
		List<WorkStepWitness> wswList = witnessService.getParentByWorkStepId(wsId);
		List<WorkStepWitness> wswNewList = null;
		if (null != wswList && wswList.size() > 0){
			if (workStep.getStepflag().equals(StepFlag.PREPARE) && (null == workStep.getNoticeresult() || workStep.getNoticeresult().equals("") || Integer.parseInt(workStep.getNoticeresult()) <= 1)){
				workStep.setStepflag(StepFlag.WITNESS);
			}
			workStep.setWitnessInfo(toFake(transferWitness(wswList)));
		} else {
			wswNewList = witnessService.getChilrenByWorkStepId(wsId);
			if (null != wswNewList && wswNewList.size() > 0){
				if (workStep.getStepflag().equals(StepFlag.PREPARE) && (null == workStep.getNoticeresult() || workStep.getNoticeresult().equals("") || Integer.parseInt(workStep.getNoticeresult()) <= 1)){
					workStep.setStepflag(StepFlag.WITNESS);
				}
				workStep.setWitnessInfo(toFake(transferWitness(wswNewList)));
			}
		}
		wswList = witnessService.getChilrenByWorkStepId(wsId);
		workStep.setWitnessesAssign(toFake(transferWitness(wswList)));
		return workStep;
	}
	
	@Override
	public WorkStepenp addWitnessInfo(WorkStepenp workStep){
		return workStep;
	}
	
	private List<WorkStepWitnessFake> toFake(List<WorkStepWitness> wswList){
		List<WorkStepWitnessFake> wswNewList = new ArrayList<WorkStepWitnessFake>();
		for (WorkStepWitness wsw : wswList){
			WorkStepWitnessFake wswFake = new WorkStepWitnessFake();
			try {
				BeanUtils.copyProperties(wswFake, wsw);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			wswNewList.add(wswFake);
		}
		
		return wswNewList;
	}
	
	private List<WorkStepWitness> transferWitness(List<WorkStepWitness> datas){
		String witnessTeam = variableSetService.findValueByKey(WITNESS_TEAM, WITNESS_TYPE);
		
		if (null != datas){
			for(WorkStepWitness workStepWitness : datas){
				if (null != workStepWitness.getWitness() && !"".equals(workStepWitness.getWitness())){
					Department department = departmentService.findById(workStepWitness.getWitness());
					
					if (null != department){
						workStepWitness.setWitnessName(department.getName());
					} else {
						workStepWitness.setWitnessName(null);
					}
					
					Set<User> user = new HashSet<User>();
					if (null != department.getUsers()){
						for (User u : department.getUsers()){
							if(null != u.getRoles()){
								for (Role r : u.getRoles()){
									if (r.getName().equals(witnessTeam)){
										user.add(u);
									}
								}
							}
						}
					}
					List<User> users = new ArrayList<User>(user);
					StringBuffer userString = new StringBuffer();
					if (null != users && !users.isEmpty()){
						for (User u : users){
							userString.append(u.getRealname() + "");
						}
						
						workStepWitness.setWitnessName(department.getName() + " -- " + userString.toString());
					}
				}
				
				if (null != workStepWitness.getWitnesser()){
					User user = userService.findUserById(workStepWitness.getWitnesser());
					
					if (null != user){
						workStepWitness.setWitnesserName(user.getRealname());
					} else {
						workStepWitness.setWitnesserName(null);
					}
				}
			}
		}
		return datas;
	}
}