package com.easycms.hd.plan.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.dao.WitnessDao;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.mybatis.dao.WitnessMybatisDao;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.service.WorkStepWitnessTansferService;

@Service
public class WitnessServiceImpl implements WitnessService{

	@PersistenceContext
	protected EntityManager em;
	@Autowired
	private WitnessDao witnessDao;
	@Autowired
	private WorkStepService workStepService;
	
	@Autowired
	private WitnessMybatisDao witnessMybatisDao;
	
	@Autowired
	private WorkStepWitnessTansferService workStepWitnessTansferService;

	@Override
	public boolean update(WorkStepWitness workStepWitness) {
		int result = witnessDao.update(workStepWitness.getWitness(),workStepWitness.getWitnessdes(),workStepWitness.getWitnessaddress(), workStepWitness.getWitnessdate(), workStepWitness.getId());
		return result > 0;
	}

	@Override
	public WorkStepWitness add(WorkStepWitness workStepWitness) {
		return witnessDao.save(workStepWitness);
	}

	@Override
	@Transactional
	public int batchAdd(List<WorkStepWitness> workStepWitnessList){
		int i = 0;
		try{
			for (i = 0; i < workStepWitnessList.size(); i++) {
				em.persist(workStepWitnessList.get(i));
				if (i % 30 == 0) {
					em.flush();
					em.clear();
				}
			}
			return i ;
		}catch(Exception e){
			i=0;
			e.printStackTrace();
		}
		
		return i ;
	}

	@Override
	public WorkStepWitness findById(Integer id) {
		return witnessDao.findById(id);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != witnessDao.delete(id)){
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Integer[] ids) {
		if (0 != witnessDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<WorkStepWitness> findAll() {
		return witnessDao.findAll();
	}
	
	@Override
	public List<WorkStepWitness> findAll_of_witnesser(Integer id) {
		return witnessDao.findAll_of_witnesser(""+id);
	}
	
	@Override
	public List<WorkStepWitness> findByWitnesserComplate(String witnesserId) {
		return witnessDao.findByWitnesserAndIsokNot(witnesserId, 0);
	}
	
	@Override
	public Page<WorkStepWitness> findByWitnesserComplate(String witnesserId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByWitnesserAndIsokNot(pageable, witnesserId, 0);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByWitnesserUnComplate(String witnesserId) {
		return witnessDao.findByWitnesserAndIsok(witnesserId, 0);
	}
	
	@Override
	public Page<WorkStepWitness> findByWitnesserUnComplate(String witnesserId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByWitnesserAndIsok(witnesserId, 0, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<WorkStepWitness> findAll(WorkStepWitness condition) {
		return witnessDao.findAll(QueryUtil.queryConditions(condition));
	}
	
	@Override
	public Page<WorkStepWitness> findPage(Page<WorkStepWitness> page, final String condition, final String[] value) {
		
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = null;
		
	    if(condition.equals("equal")){
    		if (null == value){
    			springPage = witnessDao.findDistinctByWitnessIsNull(pageable);
    		} else {
    			springPage = witnessDao.findDistinctByWitnessIn(Arrays.asList(value), pageable); 
    		}
	    } else if(condition.equals("notequal")){
    		if (null == value){
    			springPage = witnessDao.findDistinctByWitnessIsNotNull(pageable); 
    		} else {
    			springPage = witnessDao.findByWitnessNotIn(Arrays.asList(value), pageable); 
    		}
	    } else if(condition.equals("assigned")){
	    	if (null == value){
	    		springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNull(pageable); 
	    	} else {
	    		springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(Arrays.asList(value), pageable); 
	    	}
	    } 
		
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findPageByKeyWords(Page<WorkStepWitness> page, final String condition, final String[] value, String keyword) {
		
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = null;
		
		if(condition.equals("equal")){
			if (null == value){
				springPage = witnessDao.findDistinctByWitnessIsNull(pageable, keyword);
			} else {
				springPage = witnessDao.findDistinctByWitnessIn(Arrays.asList(value), keyword, pageable); 
			}
		} else if(condition.equals("notequal")){
			if (null == value){
				springPage = witnessDao.findDistinctByWitnessIsNotNull(pageable, keyword); 
			} else {
				springPage = witnessDao.findByWitnessNotIn(Arrays.asList(value), keyword, pageable); 
			}
		} else if(condition.equals("assigned")){
			if (null == value){
				springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNull(keyword, pageable); 
			} else {
				springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(Arrays.asList(value), keyword, pageable); 
			}
		} 
		
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findPage(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findAll(null, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<WorkStepWitness> findPage(WorkStepWitness condition,Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<WorkStepWitness> getByIds(Integer[] ids) {
		return witnessDao.getByIds(ids);
	}

	@Override
	public boolean deleteByWorkStep(Integer id) {
		if (0 != witnessDao.deleteByWorkStep(id)){
			return true;
		}
		return false;
	}
	
//	@Override
//	public boolean expireByWorkStep_XX(Integer id) {
//		if (0 != witnessDao.expireByWorkStep(id)){
//			return true;
//		}
//		return false;
//	}

	@Override
	public List<WorkStepWitness> findByWitness(String[] ids) {
		return witnessDao.findByWitness(ids);
	}
	
	@Override
	public List<WorkStepWitness> findByWitnesserId(String witnesserId) {
		return witnessDao.findByWitnesserId(witnesserId);
	}
	
	@Override
	public List<WorkStepWitness> findByChildrenIsNullAndWitnessIn(List<String> witnessId) {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNullAndWitnessIn(witnessId);
	}
	
	@Override
	public Page<WorkStepWitness> findByChildrenIsNullAndWitnessIn(List<String> witnessId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNullAndWitnessIn(witnessId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByChildrenIsNotNullAndWitnessIn(List<String> witnessId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(witnessId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByChildrenIsNotNullAndWitnessIn(List<String> witnessId) {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(witnessId);
	}
	
	@Override
	public Page<WorkStepWitness> findByCompleteWitness(List<String> witnessId, Page<WorkStepWitness> page, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByCompleteWitness(witnessId, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByCompleteWitness(List<String> witnessId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByCompleteWitness(witnessId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByCompleteWitness(List<String> witnessId) {
		return witnessDao.findByCompleteWitness(witnessId);
	}
	
	@Override
	public List<WorkStepWitness> findByUnCompleteWitness(List<String> witnessId) {
		return witnessDao.findByUnCompleteWitness(witnessId);
	}
	
	@Override
	public Page<WorkStepWitness> findByUnCompleteWitness(List<String> witnessId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByUnCompleteWitness(witnessId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByPageWitnesserUnComplate(Page<WorkStepWitness> page, String witnesserId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		Integer[] isoks = {0, 1};
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByPageWitnesserAndIsok(pageable, witnesserId, isoks);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByPageWitnesserUnComplate(Page<WorkStepWitness> page, String witnesserId, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		Integer[] isoks = {0, 1};
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByPageWitnesserAndIsok(pageable, witnesserId, isoks, keyword);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByPageWitnesserComplate(Page<WorkStepWitness> page, String witnesserId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		Integer isoks = 0;
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByWitnesserAndIsokNot(pageable, witnesserId, isoks);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByPageWitnesserComplate(Page<WorkStepWitness> page, String witnesserId, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		Integer isoks = 0;
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByWitnesserAndIsokNot(pageable, witnesserId, isoks, keyword);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

//---------------------------for admin------------------------------		
	@Override
	public Page<WorkStepWitness> findByChildrenIsNull(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNull(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByChildrenIsNull() {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNull();
	}
	
	@Override
	public List<WorkStepWitness> findByChildrenIsNullAndWitnessIsNotNull() {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNullAndWitnessIsNotNull();
	}
	
	@Override
	public Page<WorkStepWitness> findByChildrenIsNotNull(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNull(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findByChildrenIsNullAndWitnessIsNotNull(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIsNotNull(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByChildrenIsNotNull() {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNotNull();
	}
	@Override
	public List<WorkStepWitness> findByChildrenIsNotNullAndWitnessIsNotNull() {
		return witnessDao.findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIsNotNull();
	}
	
	@Override
	public Page<WorkStepWitness> findByComplete(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByComplete(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByComplete() {
		return witnessDao.findByComplete();
	}
	@Override
	public List<WorkStepWitness> findByComplete_of_witnesser(Integer id) {
		return witnessDao.findByComplete_of_witnesser(""+id);
	}
	
	@Override
	public List<WorkStepWitness> findByComplete_() {
		return witnessDao.findByComplete_();
	}
	
	@Override
	public Page<WorkStepWitness> findByUnComplete(Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findByUnComplete(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findByUnComplete() {
		return witnessDao.findByUnComplete();
	}
	
	@Override
	public List<WorkStepWitness> findByUnComplete_() {
		return witnessDao.findByUnComplete_();
	}
	@Override
	public List<WorkStepWitness> findByUnComplete_of_witnesser(Integer id) {
		return witnessDao.findByUnComplete_of_witnesser(""+id);
	}
//---------------------------------------------------------	
	@Override
	public Page<WorkStepWitness> findMyLaunchWitness(String userId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMyLaunchWitnessByPage(userId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<WorkStepWitness> findMyLaunchWitness(String userId) {
		return witnessDao.findMyLaunchWitness(userId);
	}

	@Override
	public Page<WorkStepWitness> findPageByWorkStepId(Page<WorkStepWitness> page, final Integer workStepId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		final WorkStep workStep = new WorkStep();
		workStep.setId(workStepId);
		
		Specification<WorkStepWitness> spec = new Specification<WorkStepWitness>() {  
			@Override
			public Predicate toPredicate(Root<WorkStepWitness> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
		          
			    if(workStepId != 0){  
			        list.add(cb.equal(root.get("workStep").as(WorkStep.class), workStep));  
			    }  
			    
			    Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));  
			}  
		}; 
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findAll(spec, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMyLaunchWitnessByPage(userId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId, String keyword, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMyLaunchWitnessByPage(userId, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		
		return page;
	}

	@Override
	public List<WorkStepWitness> getByWorkStepId(Integer id) {
		return witnessDao.getByWorkStepId(id);
	}
	
	@Override
	public List<WorkStepWitness> getActiveByWorkStepId(Integer id) {
		return witnessDao.getActiveByWorkStepId(id);
	}
	
	@Override
	public List<WorkStepWitness> getParentByWorkStepId(Integer id) {
		return witnessDao.getParentByWorkStepId(id);
	}
	
	@Override
	public List<WorkStepWitness> getChilrenByWorkStepId(Integer id) {
		return witnessDao.getChilrenByWorkStepId(id);
	}

	@Override
	public int countByWitnessToAssign(List<String> witness) {
		return witnessDao.countByWitnessToAssign(witness);
	}
	
	@Override
	public int countByWitnessToAssign(List<String> witness, String categoryFilter) {
		return witnessDao.countByWitnessToAssign(witness, categoryFilter);
	}

	@Override
	public int countdMyReciveWitness(String userId) {
		return witnessDao.countdMyReciveWitness(userId);
	}
	
	@Override
	public int countdMyReciveWitness(String userId, String categoryFilter) {
		return witnessDao.countdMyReciveWitness(userId, categoryFilter);
	}
	
	@Override
	public int countMyWitnessAssigned(List<String> witness) {
		return witnessDao.countMyWitnessAssigned(witness);
	}
	
	@Override
	public int countMyCreateWitness(String userId) {
		return witnessDao.countMyCreateWitness(userId);
	}
	
	@Override
	public int countCompleteWitness(List<String> witness) {
		return witnessDao.countCompleteWitness(witness);
	}
	
	@Override
	public int countCompleteWitness(List<String> witness, String categoryFilter) {
		return witnessDao.countCompleteWitness(witness, categoryFilter);
	}
	
	@Override
	public int countWitnesserAndIsokNot(String userId) {
		return witnessDao.countWitnesserAndIsokNot(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_QC1(String userId) {
		return witnessDao.countWitnesserAndIsokNot_QC1(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_QC2(String userId) {
		return witnessDao.countWitnesserAndIsokNot_QC2(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_A(String userId) {
		return witnessDao.countWitnesserAndIsokNot_A(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_B(String userId) {
		return witnessDao.countWitnesserAndIsokNot_B(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_C(String userId) {
		return witnessDao.countWitnesserAndIsokNot_C(userId);
	}

	@Override
	public int countWitnesserAndIsokNot_D(String userId) {
		return witnessDao.countWitnesserAndIsokNot_D(userId);
	}
	
//---------------------------------------------[2017-09-16为适应中核五公司而做的添加]---------------------------------------------------------------------------
	
	@Override
	public Page<WorkStepWitness> findMonitorPendingWitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMonitorPendingWitnessByPage(userId, rollingPlanType, noticePointType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
//人员名称转换暂时放一下		
//		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findMemberPendingQC2WitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMemberPendingQC2WitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
//人员名称转换暂时放一下		
//		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		
		if (null != page.getDatas() && !page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setChildren(witnessDao.findMemberPendingQC2WitnessAdvance(userId, rollingPlanType, data.getWorkStep().getId()));
			});
		}
		
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findMyLaunchedWitnessByPage(Integer userId, String rollingPlanType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMyLaunchedWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
//人员名称转换暂时放一下		
//		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public boolean assignToWitnessTeam(Integer[] ids, Integer teamId) {
		return witnessDao.assignToWitnessTeam(ids, teamId) > 0;
	}

	@Override
	public Page<WorkStepWitness> findQCTeamUnassignWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamUnassignWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findQCTeamAssignedWitnessByPage(Integer userId, String rollingPlanType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamAssignedWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public boolean assignToWitnessMember(Integer[] ids, Integer teamId, Integer memberId) {
		return witnessDao.assignToWitnessMember(ids, teamId, memberId) > 0;
	}
	
	@Override
	public boolean assignToWitnessMemberQC2(Integer memberId, Integer workStepId) {
		return witnessDao.assignToWitnessMemberQC2(memberId, workStepId) > 0;
	}
	
	@Override
	public boolean assignToWbWitnessMemberFqQC1(Integer memberId, Integer workStepId,Integer witnessteam) {
		return witnessDao.assignToWbWitnessMemberFqQC1(memberId, workStepId,witnessteam) > 0;
	}

	@Override
	public Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberUnCompleteWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findQCMemberCompleteWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberCompleteWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberUnQulifiedWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQCMemberQualifiedWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberQualifiedWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public WorkStepWitness findByWitnessIdAndRollingPlanType(Integer witnessId, String rollingPlanType) {
		WorkStepWitness workStepWitness = witnessDao.findByWitnessIdAndRollingPlanType(witnessId, rollingPlanType);
		
		return workStepWitness;
	}

	@Override
	public Page<WorkStepWitness> findTeamUnCompleteWitnessByPage(Integer teamId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findTeamUnCompleteWitnessByPage(teamId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findTeamCompleteWitnessByPage(Integer teamId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findTeamCompleteWitnessByPage(teamId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQCTeamUnHandledWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamUnHandledWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;	
	}

	@Override
	public Page<WorkStepWitness> findQCTeamHandledWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamHandledWitnessByPage(memberId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}

	@Override
	public List<WorkStepWitness> findMemberPendingQC2WitnessAdvance(Integer userId,
			String rollingPlanType, Integer workStepId) {
		// TODO Auto-generated method stub
		return witnessDao.findMemberPendingQC2WitnessAdvance(userId, rollingPlanType, workStepId);
	}
	
	@Override
	public Page<WorkStepWitness> findMemberPendingQC2WitnessAdvanceByPage(Integer userId,
			String rollingPlanType, Integer workStepId, String noticePointType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMemberPendingQC2WitnessAdvanceByPage(userId, rollingPlanType, workStepId, noticePointType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}

	/**适用于　QC1ReplaceQC2*/
	@Override
	public Page<WorkStepWitness> findMemberUnassignQC2WitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMemberUnassignQC2WitnessByPage(userId, rollingPlanType, noticePointType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}

	@Override
	public boolean assignToWitnesserQC2(Integer[] ids, Integer qc2Id, Integer memberId) {
		return witnessDao.assignToWitnesserQC2(ids, qc2Id, memberId) > 0;
	}

	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberUnCompleteWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberCompleteWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public WorkStepWitness findQC2SingleItem(Integer userId, String rollingPlanType, Integer workStepId) {
		return witnessDao.findQC2SingleItem(userId, rollingPlanType, workStepId);
	}
	@Override
	public WorkStepWitness findQC1SingleItem(Integer userId, String rollingPlanType, Integer workStepId) {
		return witnessDao.findQC1SingleItem(userId, rollingPlanType, workStepId);
	}

	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberUnQulifiedWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberQulifiedWitnessByPage(userId, rollingPlanType, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
//----------------------------------
	
	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	@Override
	public Page<WorkStepWitness> findQC2MemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint) {
		WorkStepWitness workStepWitness = new WorkStepWitness();
		workStepWitness.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		workStepWitness.setPageSize(page.getPagesize());
		workStepWitness.setUserId(userId);
		workStepWitness.setWitnessteam(witnessteam);
		workStepWitness.setWitness(witness);
		workStepWitness.setRollingPlanType(rollingPlanType);

		if(noticePoint==null){
			workStepWitness.setNoticePoint("QC2");
		}else{
			workStepWitness.setNoticePoint(noticePoint);
		}
		
		if (CommonUtility.isNonEmpty(keyword)) {
			workStepWitness.setKeyword(keyword);
		}

		int count = 0;
		List<WorkStepWitness> result = null;
		result = witnessMybatisDao.findQC2MemberUnCompleteWitnessByPage(workStepWitness);
		count = witnessMybatisDao.findQC2MemberUnCompleteWitnessCount(workStepWitness);
		page.execute(count, page.getPageNum(), result);
		
		if (!page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setWorkStep(workStepService.findById(data.getStepno()));
			});
		}
		
		return page;
	}
	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	@Override
	public Page<WorkStepWitness> findQC2MemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint) {
		WorkStepWitness workStepWitness = new WorkStepWitness();
		workStepWitness.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		workStepWitness.setPageSize(page.getPagesize());
		workStepWitness.setUserId(userId);
		workStepWitness.setWitnessteam(witnessteam);
		workStepWitness.setWitness(witness);
		workStepWitness.setRollingPlanType(rollingPlanType);
		
		if(noticePoint==null){
			workStepWitness.setNoticePoint("QC2");
		}else{
			workStepWitness.setNoticePoint(noticePoint);
		}

		int count = 0;
		List<WorkStepWitness> result = null;
		result = witnessMybatisDao.findQC2MemberUnQulifiedWitnessByPage(workStepWitness);
		count = witnessMybatisDao.findQC2MemberUnQulifiedWitnessCount(workStepWitness);
		page.execute(count, page.getPageNum(), result);
		
		if (!page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setWorkStep(workStepService.findById(data.getStepno()));
			});
		}
		
		return page;
	}
	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	@Override
	public Page<WorkStepWitness> findQC2MemberQulifiedWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint) {
		WorkStepWitness workStepWitness = new WorkStepWitness();
		workStepWitness.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		workStepWitness.setPageSize(page.getPagesize());
		workStepWitness.setUserId(userId);
		workStepWitness.setWitnessteam(witnessteam);
		workStepWitness.setWitness(witness);
		workStepWitness.setRollingPlanType(rollingPlanType);

		if(noticePoint==null){
			workStepWitness.setNoticePoint("QC2");
		}else{
			workStepWitness.setNoticePoint(noticePoint);
		}
		
		int count = 0;
		List<WorkStepWitness> result = null;
		result = witnessMybatisDao.findQC2MemberQulifiedWitnessByPage(workStepWitness);
		count = witnessMybatisDao.findQC2MemberQulifiedWitnessCount(workStepWitness);
		page.execute(count, page.getPageNum(), result);
		
		if (!page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setWorkStep(workStepService.findById(data.getStepno()));
			});
		}
		
		return page;
	}
	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	@Override
	public Page<WorkStepWitness> findQC2TeamUnHandledWitnessByPage(Integer userId, String rollingPlanType, String keyword, Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint) {
		WorkStepWitness workStepWitness = new WorkStepWitness();
		workStepWitness.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		workStepWitness.setPageSize(page.getPagesize());
		workStepWitness.setUserId(userId);
		workStepWitness.setWitnessteam(witnessteam);
		workStepWitness.setWitness(witness);
		workStepWitness.setRollingPlanType(rollingPlanType);
		if(noticePoint==null){
			workStepWitness.setNoticePoint("QC2");
		}else{
			workStepWitness.setNoticePoint(noticePoint);
		}
		
		int count = 0;
		List<WorkStepWitness> result = null;
		result = witnessMybatisDao.findQC2TeamUnHandledWitnessByPage(workStepWitness);
		count = witnessMybatisDao.findQC2TeamUnHandledWitnessCount(workStepWitness);
		page.execute(count, page.getPageNum(), result);
		
		if (!page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setWorkStep(workStepService.findById(data.getStepno()));
			});
		}
		
		return page;
	}

	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	@Override
	public Page<WorkStepWitness> findQC2TeamHandledWitnessByPage(Integer userId, String rollingPlanType, String keyword, Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint) {
		WorkStepWitness workStepWitness = new WorkStepWitness();
		workStepWitness.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		workStepWitness.setPageSize(page.getPagesize());
		workStepWitness.setUserId(userId);
		workStepWitness.setWitnessteam(witnessteam);
		workStepWitness.setWitness(witness);
		workStepWitness.setRollingPlanType(rollingPlanType);
		
		if(noticePoint==null){
			workStepWitness.setNoticePoint("QC2");
		}else{
			workStepWitness.setNoticePoint(noticePoint);
		}
		
		int count = 0;
		List<WorkStepWitness> result = null;
		result = witnessMybatisDao.findQC2TeamHandledWitnessByPage(workStepWitness);
		count = witnessMybatisDao.findQC2TeamHandledWitnessCount(workStepWitness);
		page.execute(count, page.getPageNum(), result);
		
		if (!page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(data -> {
				data.setWorkStep(workStepService.findById(data.getStepno()));
			});
		}
		
		return page;
	}
//-------------------------------------------
//----------------------[2017-10-30见证列表分页信息带全局搜索条件]--------------------	
	
	@Override
	public Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberUnCompleteWitnessByPageWithKeyword(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findTeamUnCompleteWitnessByPageWithKeyword(Integer teamId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findTeamUnCompleteWitnessByPageWithKeyword(teamId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberUnCompleteWitnessByPageWithKeyword(userId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	

	@Override
	public Page<WorkStepWitness> findQCMemberCompleteWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberCompleteWitnessByPageWithKeyword(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	
	@Override
	public Page<WorkStepWitness> findTeamCompleteWitnessByPageWithKeyword(Integer teamId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findTeamCompleteWitnessByPageWithKeyword(teamId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}
	

	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberCompleteWitnessByPageWithKeyword(userId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}


	@Override
	public Page<WorkStepWitness> findQCTeamUnassignWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamUnassignWitnessByPageWithKeyword(userId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	/**适用于　QC1ReplaceQC2*/
	@Override
	public Page<WorkStepWitness> findMemberUnassignQC2WitnessByPageWithKeyword(Integer userId, String rollingPlanType, String noticePointType, String keyword, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMemberUnassignQC2WitnessByPageWithKeyword(userId, rollingPlanType, noticePointType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findMonitorPendingWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String noticePointType, String keyword, Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findMonitorPendingWitnessByPageWithKeyword(userId, rollingPlanType, noticePointType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
//人员名称转换暂时放一下		
//		page.setDatas(workStepWitnessTansferService.transferChildren(page.getDatas()));
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberUnQulifiedWitnessByPageWithKeyword(userId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	@Override
	public Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberUnQulifiedWitnessByPageWithKeyword(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	@Override
	public Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQC2DownLineMemberQulifiedWitnessByPageWithKeyword(userId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}

	@Override
	public Page<WorkStepWitness> findQCMemberQualifiedWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCMemberQualifiedWitnessByPageWithKeyword(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		
		return page;
	}
	
	@Override
	public Page<WorkStepWitness> findQCTeamUnHandledWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamUnHandledWitnessByPageWithKeyworde(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;	
	}
	

	@Override
	public Page<WorkStepWitness> findQCTeamHandledWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepWitness> springPage = witnessDao.findQCTeamHandledWitnessByPageWithKeyword(memberId, rollingPlanType, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		page.setDatas(page.getDatas());
		return page;
	}
}
