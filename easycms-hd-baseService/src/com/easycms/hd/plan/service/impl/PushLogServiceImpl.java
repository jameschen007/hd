package com.easycms.hd.plan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.dao.PushLogDao;
import com.easycms.hd.plan.domain.PushLog;
import com.easycms.hd.plan.service.PushLogService;

@Service("pushLogService")
public class PushLogServiceImpl implements PushLogService {

	@Autowired
	private PushLogDao pushLogDao;
	
	@Override
	public PushLog insert(PushLog pushLog) {
		if (null != pushLog) {
			return pushLogDao.save(pushLog);
		}
		return null;
	}

}
