package com.easycms.hd.plan.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushAliasPrefix;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.jpush.JPushSendUtil;
import com.easycms.common.jpush.JPushUtil;
import com.easycms.hd.plan.domain.PushLog;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.PushLogService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.domain.UserDevice;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserDeviceService;
import com.easycms.management.user.service.UserService;

@Service("jPushService")
public class JPushServiceImpl implements JPushService {
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserDeviceService userDeviceService;
	@Autowired
	private PushLogService pushLogService;
	
	@Override
	public void pushByDepartmentId(JPushExtra extra, String alert, String title, Integer departmentId, String roleName) {
		Department department = departmentService.findById(departmentId);
		if (null != department){
			List<User> users = department.getUsers();
			for (User u : users){
				for (Role r : u.getRoles()){
					if (r.getName().equals(roleName)){
						List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(u.getId());
						
						extra.setUserId(u.getId()+"");
						if (null != deviceList && !deviceList.isEmpty()){
							for (UserDevice ud : deviceList){
								JPushSendUtil.sendPushWithAliasAndExtra(alert, ud.getDeviceid() + "_" + u.getId(), title, JPushUtil.transBean2Map(extra));
								savePushLog(extra, alert, title, u.getId(), ud.getDeviceid());
							}
						} else {
							JPushSendUtil.sendPushWithAliasAndExtra(alert, JPushAliasPrefix.USER_PREFIX + u.getId(), title, JPushUtil.transBean2Map(extra));
							savePushLog(extra, alert, title, u.getId(), JPushAliasPrefix.USER_PREFIX);
						}
					}
				}
			}
		}
		
	}

	@Override
	public void pushByUserId(JPushExtra extra, String alert, String title, Integer userId) {
		User user = userService.findUserById(userId);
		
		if (null != user){
			List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(userId);
			
			extra.setUserId(user.getId()+"");
			if (null != deviceList && !deviceList.isEmpty()){
				for (UserDevice ud : deviceList){
					JPushSendUtil.sendPushWithAliasAndExtra(alert, ud.getDeviceid() + "_" + user.getId(), title, JPushUtil.transBean2Map(extra));
					savePushLog(extra, alert, title, userId, ud.getDeviceid());
				}
			} else {
				JPushSendUtil.sendPushWithAliasAndExtra(alert, JPushAliasPrefix.USER_PREFIX + user.getId(), title, JPushUtil.transBean2Map(extra));
				savePushLog(extra, alert, title, userId, JPushAliasPrefix.USER_PREFIX);
			}
		}
	}
	
	@Override
	public void pushByUserId(JPushExtra extra, String alert, String title, List<Integer> userIdList) {
		for (Integer userId : userIdList){
			User user = userService.findUserById(userId);
			
			if (null != user){
				List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(userId);
				
				extra.setUserId(user.getId()+"");
				if (null != deviceList && !deviceList.isEmpty()){
					for (UserDevice ud : deviceList){
						JPushSendUtil.sendPushWithAliasAndExtra(alert, ud.getDeviceid() + "_" + user.getId(), title, JPushUtil.transBean2Map(extra));
						savePushLog(extra, alert, title, userId, ud.getDeviceid());
					}
				} else {
					JPushSendUtil.sendPushWithAliasAndExtra(alert, JPushAliasPrefix.USER_PREFIX + user.getId(), title, JPushUtil.transBean2Map(extra));
					savePushLog(extra, alert, title, userId, JPushAliasPrefix.USER_PREFIX);
				}
			}
		}
	}
	
	@Override
	public void pushByUser(JPushExtra extra, String alert, String title, List<User> userList) {
		for (User user : userList){
			user = userService.findUserById(user.getId());
			
			if (null != user){
				List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(user.getId());
				
				extra.setUserId(user.getId()+"");
				if (null != deviceList && !deviceList.isEmpty()){
					for (UserDevice ud : deviceList){
						JPushSendUtil.sendPushWithAliasAndExtra(alert, ud.getDeviceid() + "_" + user.getId(), title, JPushUtil.transBean2Map(extra));
						savePushLog(extra, alert, title, user.getId(), ud.getDeviceid());
					}
				} else {
					JPushSendUtil.sendPushWithAliasAndExtra(alert, JPushAliasPrefix.USER_PREFIX + user.getId(), title, JPushUtil.transBean2Map(extra));
					savePushLog(extra, alert, title, user.getId(), JPushAliasPrefix.USER_PREFIX);
				}
			}
		}
	}
	
	private void savePushLog(JPushExtra extra, String message, String title, Integer userId, String device) {
		PushLog pushLog = new PushLog();
		if (null != extra) {
			pushLog.setExtraCategory(extra.getCategory());
			pushLog.setExtraData(extra.getData());
			pushLog.setExtraRemark(extra.getRemark());
			pushLog.setExtraUserId(extra.getUserId());
		}
		pushLog.setMessage(message);
		pushLog.setTitle(title);
		pushLog.setUserId(userId);
		pushLog.setSendOn(new Date());
		pushLog.setDevice(device);
		
		pushLogService.insert(pushLog);
	}
}
