package com.easycms.hd.plan.service;

import java.util.Date;
import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.RollingPlan;

public interface StatisticsRollingPlanService {

	List<RollingPlan> findAll();

	List<RollingPlan> findAll(RollingPlan condition);

	/**
	 * 查找分页
	 * 
	 * @param pageable
	 * @return
	 */

	Page<RollingPlan> findPage(Page<RollingPlan> page);

	Page<RollingPlan> findPage(RollingPlan condition, Page<RollingPlan> page);

	Page<RollingPlan> findPageByStatus(Page<RollingPlan> page, Integer status,
			String type, String date, String category, Integer hysteresis);

	Page<RollingPlan> findPageByStatusClasz(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids);
	
	Page<RollingPlan> findPageByStatusGroup(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids);

	Page<RollingPlan> findPageByStatus(Page<RollingPlan> page, Integer status,
		String type, String date, String category, Integer hysteresis,
		String keyword);
	
	Page<RollingPlan> findPageByStatusGroup(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids, String keyword);

	Page<RollingPlan> findPageByStatusClasz(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids, String keyword);

	/**
	 * 根据ID查找
	 * 
	 * @param id
	 * @return
	 */
	RollingPlan findById(Integer id);

	boolean delete(Integer id);

	/**
	 * 更新滚动计划
	 * 
	 * @param rollingPlan
	 * @return
	 */
	boolean update(RollingPlan rollingPlan);

	/**
	 * 添加滚动计划
	 * 
	 * @param rollingPlan
	 * @return
	 */
	RollingPlan add(RollingPlan rollingPlan);


	List<Object[]> getRollingPlanTaskHyperbola(String month, String type);

	List<Object[]> getRollingPlanTaskHyperbolaByDate(String startTime,
			String endTime, String type);

	// -------------------------------------------task
	// Team---------------------------------------------------

	Long getRollingPlanSurplus(String id, String type);

	Long getRollingPlanComplate(String id, String type);

	Long getRollingPlanTotal(String id, String type);

	// -------------------------------------------task
	// Group---------------------------------------------------
	Long getRollingPlanCompleteByConsEndmanAndType(String id, String type);

	Long getRollingPlanUnCompleteByConsEndmanAndType(String id, String type);

	Long getRollingPlanTotalByConsEndman(String id);
	
	Long getRollingPlanTotalByConsEndman(String id, String type);

	// ------------------------------------------task
	// status---------------------------------------------
	Long getRollingPlanStatusDayStatus0(String type, String date);

	Long getRollingPlanStatusDayStatus1(String type, String date);

	Long getRollingPlanStatusDayStatus2(String type, String date);

	Long getRollingPlanStatusDayStatus6(String type, Date date, Integer diff);

	Long getRollingPlanStatusDayStatus4(String type, String date);

	Long getRollingPlanStatusDayStatus5(String type, String date);

	Long getRollingPlanStatusDayStatus3(String type, String date);

	// ---------------------------Week
	Long getRollingPlanStatusWeekStatus0(String type, Date date);

	Long getRollingPlanStatusWeekStatus1(String type, Date date);

	Long getRollingPlanStatusWeekStatus2(String type, Date date);

	Long getRollingPlanStatusWeekStatus3(String type, Date date);

	Long getRollingPlanStatusWeekStatus6(String type, Date date, Integer diff);

	Long getRollingPlanStatusWeekStatus4(String type, Date date);

	Long getRollingPlanStatusWeekStatus5(String type, Date date);

	// ---------------------------Month
	Long getRollingPlanStatusMonthStatus0(String type, String date);

	Long getRollingPlanStatusMonthStatus1(String type, String date);

	Long getRollingPlanStatusMonthStatus2(String type, String date);

	Long getRollingPlanStatusMonthStatus3(String type, String date);

	Long getRollingPlanStatusMonthStatus6(String type, Date date, Integer diff);

	Long getRollingPlanStatusMonthStatus4(String type, String date);

	Long getRollingPlanStatusMonthStatus5(String type, String date);

	// ---------------------------Year
	Long getRollingPlanStatusYearStatus0(String type, String date);

	Long getRollingPlanStatusYearStatus1(String type, String date);

	Long getRollingPlanStatusYearStatus2(String type, String date);

	Long getRollingPlanStatusYearStatus3(String type, String date);

	Long getRollingPlanStatusYearStatus6(String type, Date date, Integer diff);

	Long getRollingPlanStatusYearStatus4(String type, String date);

	Long getRollingPlanStatusYearStatus5(String type, String date);

	// by user department id
	List<Object[]> getRollingPlanTaskHyperbolaByGroup(String month,
			String weldHK, String[] id);

	Long getRollingPlanStatusDayStatus0(String type, String date, String[] ids);

	Long getRollingPlanStatusAllStatus0(String type, Date date, String[] ids);

	Long getRollingPlanStatusDayStatus1(String type, String date, String[] ids);

	Long getRollingPlanStatusDayStatus2(String type, String date, String[] ids);

	Long getRollingPlanStatusDayStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusDayStatus4(String type, String date, String[] ids);

	Long getRollingPlanStatusDayStatus5(String type, String date, String[] ids);

	Long getRollingPlanStatusWeekStatus0(String type, Date date, String[] ids);

	Long getRollingPlanStatusWeekStatus1(String type, Date date, String[] ids);

	Long getRollingPlanStatusWeekStatus2(String type, Date date, String[] ids);

	Long getRollingPlanStatusWeekStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusWeekStatus4(String type, Date date, String[] ids);

	Long getRollingPlanStatusWeekStatus5(String type, Date date, String[] ids);

	Long getRollingPlanStatusMonthStatus0(String type, String date, String[] ids);

	Long getRollingPlanStatusMonthStatus1(String type, String date, String[] ids);

	Long getRollingPlanStatusMonthStatus2(String type, String date, String[] ids);

	Long getRollingPlanStatusMonthStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusMonthStatus4(String type, String date, String[] ids);

	Long getRollingPlanStatusMonthStatus5(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus0(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus1(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus2(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusYearStatus4(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus5(String type, String date, String[] ids);

	Long getRollingPlanStatusDayStatus3(String type, String date, String[] ids);

	Long getRollingPlanStatusWeekStatus3(String type, Date date, String[] ids);

	Long getRollingPlanStatusMonthStatus3(String type, String date, String[] ids);

	Long getRollingPlanStatusYearStatus3(String type, String date, String[] ids);

	Long getRollingPlanStatusAllStatus5(String type, Date date, String[] ids);

	Long getRollingPlanStatusAllStatus0(String type, Date date);

	Long getRollingPlanStatusAllStatus5(String type, Date date);

	Long getRollingPlanStatusAllStatus0ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusDayStatus1ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusDayStatus2ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusDayStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusDayStatus4ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusAllStatus5ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusDayStatus3ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusWeekStatus1ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusWeekStatus2ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusWeekStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusWeekStatus4ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusWeekStatus3ByGroup(String type, Date date,
			String[] ids);

	Long getRollingPlanStatusMonthStatus1ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusMonthStatus2ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusMonthStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusMonthStatus4ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusMonthStatus3ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusYearStatus1ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusYearStatus2ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusYearStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

	Long getRollingPlanStatusYearStatus4ByGroup(String type, String date,
			String[] ids);

	Long getRollingPlanStatusYearStatus3ByGroup(String type, String date,
			String[] ids);

	List<Object[]> getRollingPlanTaskHyperbola(String month, String weldHK,
			String[] ids);

	List<Object[]> getRollingPlanTaskHyperbolaByDuration(String fromDate,
			String toDate, String weldHK, String[] ids);

	List<Object[]> getRollingPlanTaskHyperbolaByGroupAndDuration(
			String fromDate, String toDate, String weldHK, String[] ids);
	
	
	
	//--------------------------------------------------------根班长统计-----------------------------------------------------------------------
	
	//根据班长查询未分配滚动计划统计
	Long getRollingPlanStatusAllStatus0ByClasz(String type, String[] ids);
	
	//根据班长按天查询计划中滚动计划统计
	Long getRollingPlanStatusDayStatus4ByClasz(String type, String date, String[] ids);
	//根据班长按周查询计划中滚动计划统计
	Long getRollingPlanStatusWeekStatus4ByClasz(String type, Date date, String[] ids);
	//根据班长按月查询计划中滚动计划统计
	Long getRollingPlanStatusMonthStatus4ByClasz(String type, String date, String[] ids);
	//根据班长按年查询计划中滚动计划统计
	Long getRollingPlanStatusYearStatus4ByClasz(String type, String date, String[] ids);
	  
	//-----------------------------------------------计划员统计----------------------------------------------
	//根据计划员统计未分配
   Long getRollingPlanStatusAllStatus0ByPlanner(String type);
}
