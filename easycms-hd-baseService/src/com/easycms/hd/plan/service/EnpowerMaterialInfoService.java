package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.hd.plan.domain.EnpowerMaterialInfo;

public interface EnpowerMaterialInfoService {

	public boolean update(EnpowerMaterialInfo rollingPlan) ;
	public boolean updateActualDosage(EnpowerMaterialInfo rollingPlan) ;

	public EnpowerMaterialInfo add(EnpowerMaterialInfo rollingPlan) ;


	public EnpowerMaterialInfo findById(Integer id) ;

	public List<EnpowerMaterialInfo> findByRollingPlanId(Integer id);
 
}
