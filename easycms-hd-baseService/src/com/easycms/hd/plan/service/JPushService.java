package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.common.jpush.JPushExtra;
import com.easycms.management.user.domain.User;

public interface JPushService {
	void pushByDepartmentId(JPushExtra extra, String alert, String title, Integer departmentId, String roleName);
	
	void pushByUserId(JPushExtra extra, String alert, String title, Integer userId);
	
	void pushByUserId(JPushExtra extra, String alert, String title, List<Integer> userIdList);
	
	void pushByUser(JPushExtra extra, String alert, String title, List<User> userList);
}
