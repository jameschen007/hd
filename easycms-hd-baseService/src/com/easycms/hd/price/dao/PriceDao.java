package com.easycms.hd.price.dao;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.price.domain.Price;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-08 
 * 
 */
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface PriceDao extends Repository<Price,Integer>,BasicDao<Price> {
	@Modifying
	@Query("UPDATE Price p SET p.speciality=?1,p.pricetype=?2,p.unitprice=?3,p.remark=?4 WHERE p.id=?5")
	int update(String speciality,String pricetype,Double unitprice,String remark,Integer id);
	
	@Modifying
	@Query("DELETE FROM Price p WHERE p.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM Price p WHERE p.speciality=?1 and p.pricetype=?2")
	Price checkPricetypeExist(String speciality, String pricetype);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM Price p WHERE p.speciality=?1 and p.pricetype=?2 and p.id!=?3")
	Price checkPricetypeExistWithId(String speciality, String pricetype, Integer id);
}
