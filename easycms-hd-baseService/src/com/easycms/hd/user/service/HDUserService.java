package com.easycms.hd.user.service;

import java.util.List;

import com.easycms.management.user.domain.User;

public interface HDUserService {
  /**
   * 根据userID 判断该用户是否是施工组组长
   * @param userId 用户主键id
   * @return
   */
  public boolean isGroup(Integer userId);
  
  /**
   * 根据用户id判断用户是否是施工班班长
   * @param userId 用户主键id
   * @return 
   */
  public boolean isClasz(Integer userId);

  /**
   * 获取班长下面的组长
   * @param user
   * @return
   */
  public List<User> getChildren(User user);
  
  
  
}
