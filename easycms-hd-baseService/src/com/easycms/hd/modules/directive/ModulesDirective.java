package com.easycms.hd.modules.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;

/**
 * 获取模块列表指令 
 */
public class ModulesDirective extends BaseDirective<Modules>  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ModulesDirective.class);
	@Autowired
	private ModulesService modulesService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected Modules field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		String status = FreemarkerTemplateUtility.getStringValueFromParams(params, "status");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			Modules modules = modulesService.findById(id);
			if (null == modules){
				return null;
			}

			return modules;
		} else if (CommonUtility.isNonEmpty(status)) {
			Modules modules = modulesService.findByType(status);
			if (null == modules){
				return null;
			}

			return modules;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<Modules> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Modules> pager,
			Map<String, Object> envParams) {
		List<Modules> datas = modulesService.findByStatus("ACTIVE");
		logger.info("==> Modules length [" + datas.size() + "]");
		return datas;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<Modules> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}
}
