package com.easycms.hd.dashboard.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.api.response.statistics.RollingPlanStatisticsResult;
import com.easycms.hd.api.response.statistics.StatisticsMainResult;
import com.easycms.hd.api.response.statistics.StatisticsReportResult;
import com.easycms.hd.api.response.statistics.WitnessStatisticsResult;
import com.easycms.hd.api.service.StatisticsApiService;
import com.easycms.hd.statistics.domain.TaskStatusDate;
import com.easycms.hd.statistics.form.TaskStatisticsForm;
import com.easycms.hd.statistics.util.StatisticsUtil;
import com.easycms.hd.user.service.HDUserService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/management")
public class DashboardController {
  private static final Logger logger = Logger.getLogger(DashboardController.class);
  private static String ASSIGN_TYPE = "assigntype";
  @Autowired
  private VariableSetService variableSetService;
  @Autowired
  private UserService userService;
  @Autowired
  private HDUserService hdUserService;
  @Autowired
  private StatisticsApiService statisticsApiService;


  @RequestMapping("/index")
  public String view(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
/*    Role role = new Role();
    String consteam = variableSetService.findValueByKey("consteam", ASSIGN_TYPE);
    role.setName(consteam);
    form.setFilter(CommonUtility.toJson(role));

    TaskStatusDate tsd = new TaskStatusDate();
    tsd.setAfter(StatisticsUtil.getDate(1));
    tsd.setBefore(StatisticsUtil.getDate(-1));
    tsd.setCurrent(StatisticsUtil.getDate(0));
    tsd.setWeek(StatisticsUtil.getWeek());
    tsd.setMonth(StatisticsUtil.getMonth());
    tsd.setYear(StatisticsUtil.getYear());

    request.setAttribute("taskStatusDate", tsd);
    String loginUserRole = null;

    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());
    if (hdUserService.isClasz(user.getId())) {
      loginUserRole = "clasz";
      request.setAttribute("claszUser", user);
    } else if (hdUserService.isGroup(user.getId())) {
      loginUserRole = "group";
    }

    List<User> users = hdUserService.getChildren(user);
    request.setAttribute("users", users);
    request.setAttribute("loginUserRole", loginUserRole);
    
    String type = (String)ContextUtil.getSession().getAttribute("type");
    
    StatisticsMainResult<RollingPlanStatisticsResult> rollingPlanMainResult = statisticsApiService.getRollingPlanStatisticsResult(user, type);
    
    List<StatisticsReportResult<RollingPlanStatisticsResult>> results = null;
    if (null != rollingPlanMainResult) {
    	if (null != rollingPlanMainResult.getCaptain() && !rollingPlanMainResult.getCaptain().isEmpty()) {
    		results = rollingPlanMainResult.getCaptain();
    	} else if (null != rollingPlanMainResult.getMonitor() && !rollingPlanMainResult.getMonitor().isEmpty()) {
    		results = rollingPlanMainResult.getMonitor();
    	} else if (null != rollingPlanMainResult.getWitness_team() && !rollingPlanMainResult.getWitness_team().isEmpty()) {
    		results = rollingPlanMainResult.getWitness_team();
    	}
    }
	
    StatisticsMainResult<WitnessStatisticsResult> witnessMainResult = statisticsApiService.getWitnessStatisticsResult(user,type, null);
    
    List<StatisticsReportResult<WitnessStatisticsResult>> witnessResults = null;
    if (null != witnessMainResult) {
    	if (null != witnessMainResult.getCaptain() && !witnessMainResult.getCaptain().isEmpty()) {
    		witnessResults = witnessMainResult.getCaptain();
    	} else if (null != witnessMainResult.getMonitor() && !witnessMainResult.getMonitor().isEmpty()) {
    		witnessResults = witnessMainResult.getMonitor();
    	} else if (null != witnessMainResult.getWitness_team() && !witnessMainResult.getWitness_team().isEmpty()) {
    		witnessResults = witnessMainResult.getWitness_team();
    	}
    }
    
    request.setAttribute("rollingplan", results);
    request.setAttribute("witness", witnessResults);
    */
    String returnString = ForwardUtility.forwardAdminView("index");
    return returnString;

  }

  @RequestMapping("/dashboard")
  public String dashboard(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") TaskStatisticsForm form) throws Exception {
    Role role = new Role();
    String consteam = variableSetService.findValueByKey("consteam", ASSIGN_TYPE);
    role.setName(consteam);
    form.setFilter(CommonUtility.toJson(role));

    TaskStatusDate tsd = new TaskStatusDate();
    tsd.setAfter(StatisticsUtil.getDate(1));
    tsd.setBefore(StatisticsUtil.getDate(-1));
    tsd.setCurrent(StatisticsUtil.getDate(0));
    tsd.setWeek(StatisticsUtil.getWeek());
    tsd.setMonth(StatisticsUtil.getMonth());
    tsd.setYear(StatisticsUtil.getYear());

    request.setAttribute("taskStatusDate", tsd);


    String loginUserRole = null;

    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());

    if (hdUserService.isClasz(user.getId())) {
      loginUserRole = "clasz";
      request.setAttribute("claszUser", user);
    } else if (hdUserService.isGroup(user.getId())) {
      loginUserRole = "group";
    }
    List<User> users = hdUserService.getChildren(user);
    request.setAttribute("users", users);
    request.setAttribute("loginUserRole", loginUserRole);

    String returnString = ForwardUtility.forwardAdminView("index/dashboard");
    return returnString;

  }

  @RequestMapping(value = "/dashboard/taskstatus", method = RequestMethod.POST)
  public String taskStatusDataList(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("==> Show dashboard task status data list.");
    String date = request.getParameter("date");
    String category = request.getParameter("category");
    String id = request.getParameter("id");

    request.setAttribute("taskDate", date);
    request.setAttribute("category", category);
    request.setAttribute("id", id);


    String loginUserRole = null;
    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());
  
    if (hdUserService.isClasz(user.getId())) {
      loginUserRole = "clasz";
      request.setAttribute("claszUser", user);
    } else if (hdUserService.isGroup(user.getId())) {
      loginUserRole = "group";
    }
    request.setAttribute("loginUserRole", loginUserRole);
    
    String returnString = ForwardUtility.forwardAdminView("/index/data/data_html_task_status");
    return returnString;
  }

  @RequestMapping(value = "/dashboard/finishedhyperbola", method = RequestMethod.POST)
  public String finishedHyperbola(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("==> Show dashboard finishedhyperbola.");

    String fromDate = request.getParameter("fromDate");
    String toDate = request.getParameter("toDate");
    String category = request.getParameter("category");
    TaskStatusDate tsd = new TaskStatusDate();
    tsd.setAfter(StatisticsUtil.getDate(1));
    tsd.setBefore(StatisticsUtil.getDate(-1));
    tsd.setCurrent(StatisticsUtil.getDate(0));
    tsd.setWeek(StatisticsUtil.getWeek());
    tsd.setMonth(StatisticsUtil.getMonth());
    tsd.setYear(StatisticsUtil.getYear());

    request.setAttribute("taskStatusDate", tsd);
    request.setAttribute("startTime", fromDate);
    request.setAttribute("endTime", toDate);
    request.setAttribute("category", category);

    String returnString = ForwardUtility.forwardAdminView("/index/data/data_html_count_hyperbola");
    return returnString;
  }
}
