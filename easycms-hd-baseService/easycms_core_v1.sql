/*
Navicat MySQL Data Transfer

Source Server         : CaaSApp_Localhost
Source Server Version : 50087
Source Host           : localhost:3306
Source Database       : easycms_core_v1

Target Server Type    : MYSQL
Target Server Version : 50087
File Encoding         : 65001

Date: 2015-07-22 19:08:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ACT_EVT_LOG`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_EVT_LOG`;
CREATE TABLE `ACT_EVT_LOG` (
  `LOG_NR_` bigint(20) NOT NULL auto_increment,
  `TYPE_` varchar(64) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `TIME_STAMP_` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `USER_ID_` varchar(255) collate utf8_bin default NULL,
  `DATA_` longblob,
  `LOCK_OWNER_` varchar(255) collate utf8_bin default NULL,
  `LOCK_TIME_` timestamp NULL default NULL,
  `IS_PROCESSED_` tinyint(4) default '0',
  PRIMARY KEY  (`LOG_NR_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_EVT_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_GE_BYTEARRAY`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_GE_BYTEARRAY`;
CREATE TABLE `ACT_GE_BYTEARRAY` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `DEPLOYMENT_ID_` varchar(64) collate utf8_bin default NULL,
  `BYTES_` longblob,
  `GENERATED_` tinyint(4) default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_GE_BYTEARRAY
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_GE_PROPERTY`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_GE_PROPERTY`;
CREATE TABLE `ACT_GE_PROPERTY` (
  `NAME_` varchar(64) collate utf8_bin NOT NULL default '',
  `VALUE_` varchar(300) collate utf8_bin default NULL,
  `REV_` int(11) default NULL,
  PRIMARY KEY  (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_GE_PROPERTY
-- ----------------------------
INSERT INTO `ACT_GE_PROPERTY` VALUES ('next.dbid', '2501', '2');
INSERT INTO `ACT_GE_PROPERTY` VALUES ('schema.history', 'create(5.16)', '1');
INSERT INTO `ACT_GE_PROPERTY` VALUES ('schema.version', '5.16', '1');

-- ----------------------------
-- Table structure for `ACT_HI_ACTINST`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_ACTINST`;
CREATE TABLE `ACT_HI_ACTINST` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) collate utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `CALL_PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `ACT_NAME_` varchar(255) collate utf8_bin default NULL,
  `ACT_TYPE_` varchar(255) collate utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) collate utf8_bin default NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime default NULL,
  `DURATION_` bigint(20) default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_ACTINST
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_ATTACHMENT`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_ATTACHMENT`;
CREATE TABLE `ACT_HI_ATTACHMENT` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `USER_ID_` varchar(255) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `DESCRIPTION_` varchar(4000) collate utf8_bin default NULL,
  `TYPE_` varchar(255) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `URL_` varchar(4000) collate utf8_bin default NULL,
  `CONTENT_ID_` varchar(64) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_ATTACHMENT
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_COMMENT`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_COMMENT`;
CREATE TABLE `ACT_HI_COMMENT` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `TYPE_` varchar(255) collate utf8_bin default NULL,
  `TIME_` datetime NOT NULL,
  `USER_ID_` varchar(255) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `ACTION_` varchar(255) collate utf8_bin default NULL,
  `MESSAGE_` varchar(4000) collate utf8_bin default NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_COMMENT
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_DETAIL`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_DETAIL`;
CREATE TABLE `ACT_HI_DETAIL` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `TYPE_` varchar(255) collate utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `ACT_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) collate utf8_bin default NULL,
  `REV_` int(11) default NULL,
  `TIME_` datetime NOT NULL,
  `BYTEARRAY_ID_` varchar(64) collate utf8_bin default NULL,
  `DOUBLE_` double default NULL,
  `LONG_` bigint(20) default NULL,
  `TEXT_` varchar(4000) collate utf8_bin default NULL,
  `TEXT2_` varchar(4000) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`),
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_DETAIL
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_IDENTITYLINK`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_IDENTITYLINK`;
CREATE TABLE `ACT_HI_IDENTITYLINK` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `GROUP_ID_` varchar(255) collate utf8_bin default NULL,
  `TYPE_` varchar(255) collate utf8_bin default NULL,
  `USER_ID_` varchar(255) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_IDENTITYLINK
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_PROCINST`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_PROCINST`;
CREATE TABLE `ACT_HI_PROCINST` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin NOT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime default NULL,
  `DURATION_` bigint(20) default NULL,
  `START_USER_ID_` varchar(255) collate utf8_bin default NULL,
  `START_ACT_ID_` varchar(255) collate utf8_bin default NULL,
  `END_ACT_ID_` varchar(255) collate utf8_bin default NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) collate utf8_bin default NULL,
  `DELETE_REASON_` varchar(4000) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  `NAME_` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_PROCINST
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_TASKINST`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_TASKINST`;
CREATE TABLE `ACT_HI_TASKINST` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `TASK_DEF_KEY_` varchar(255) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `PARENT_TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `DESCRIPTION_` varchar(4000) collate utf8_bin default NULL,
  `OWNER_` varchar(255) collate utf8_bin default NULL,
  `ASSIGNEE_` varchar(255) collate utf8_bin default NULL,
  `START_TIME_` datetime NOT NULL,
  `CLAIM_TIME_` datetime default NULL,
  `END_TIME_` datetime default NULL,
  `DURATION_` bigint(20) default NULL,
  `DELETE_REASON_` varchar(4000) collate utf8_bin default NULL,
  `PRIORITY_` int(11) default NULL,
  `DUE_DATE_` datetime default NULL,
  `FORM_KEY_` varchar(255) collate utf8_bin default NULL,
  `CATEGORY_` varchar(255) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_TASKINST
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_HI_VARINST`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_VARINST`;
CREATE TABLE `ACT_HI_VARINST` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) collate utf8_bin default NULL,
  `REV_` int(11) default NULL,
  `BYTEARRAY_ID_` varchar(64) collate utf8_bin default NULL,
  `DOUBLE_` double default NULL,
  `LONG_` bigint(20) default NULL,
  `TEXT_` varchar(4000) collate utf8_bin default NULL,
  `TEXT2_` varchar(4000) collate utf8_bin default NULL,
  `CREATE_TIME_` datetime default NULL,
  `LAST_UPDATED_TIME_` datetime default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_HI_VARINST
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_ID_GROUP`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_GROUP`;
CREATE TABLE `ACT_ID_GROUP` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `TYPE_` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_ID_GROUP
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_ID_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_INFO`;
CREATE TABLE `ACT_ID_INFO` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `USER_ID_` varchar(64) collate utf8_bin default NULL,
  `TYPE_` varchar(64) collate utf8_bin default NULL,
  `KEY_` varchar(255) collate utf8_bin default NULL,
  `VALUE_` varchar(255) collate utf8_bin default NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_ID_INFO
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_ID_MEMBERSHIP`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_MEMBERSHIP`;
CREATE TABLE `ACT_ID_MEMBERSHIP` (
  `USER_ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `GROUP_ID_` varchar(64) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_ID_MEMBERSHIP
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_ID_USER`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_USER`;
CREATE TABLE `ACT_ID_USER` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `FIRST_` varchar(255) collate utf8_bin default NULL,
  `LAST_` varchar(255) collate utf8_bin default NULL,
  `EMAIL_` varchar(255) collate utf8_bin default NULL,
  `PWD_` varchar(255) collate utf8_bin default NULL,
  `PICTURE_ID_` varchar(64) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_ID_USER
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RE_DEPLOYMENT`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_DEPLOYMENT`;
CREATE TABLE `ACT_RE_DEPLOYMENT` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `CATEGORY_` varchar(255) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  `DEPLOY_TIME_` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RE_DEPLOYMENT
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RE_MODEL`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_MODEL`;
CREATE TABLE `ACT_RE_MODEL` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `KEY_` varchar(255) collate utf8_bin default NULL,
  `CATEGORY_` varchar(255) collate utf8_bin default NULL,
  `CREATE_TIME_` timestamp NULL default NULL,
  `LAST_UPDATE_TIME_` timestamp NULL default NULL,
  `VERSION_` int(11) default NULL,
  `META_INFO_` varchar(4000) collate utf8_bin default NULL,
  `DEPLOYMENT_ID_` varchar(64) collate utf8_bin default NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) collate utf8_bin default NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`),
  KEY `ACT_FK_MODEL_SOURCE` (`EDITOR_SOURCE_VALUE_ID_`),
  KEY `ACT_FK_MODEL_SOURCE_EXTRA` (`EDITOR_SOURCE_EXTRA_VALUE_ID_`),
  KEY `ACT_FK_MODEL_DEPLOYMENT` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RE_MODEL
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RE_PROCDEF`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_PROCDEF`;
CREATE TABLE `ACT_RE_PROCDEF` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `CATEGORY_` varchar(255) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `KEY_` varchar(255) collate utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) collate utf8_bin default NULL,
  `RESOURCE_NAME_` varchar(4000) collate utf8_bin default NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) collate utf8_bin default NULL,
  `DESCRIPTION_` varchar(4000) collate utf8_bin default NULL,
  `HAS_START_FORM_KEY_` tinyint(4) default NULL,
  `SUSPENSION_STATE_` int(11) default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RE_PROCDEF
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_EVENT_SUBSCR`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_EVENT_SUBSCR`;
CREATE TABLE `ACT_RU_EVENT_SUBSCR` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `EVENT_TYPE_` varchar(255) collate utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) collate utf8_bin default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `ACTIVITY_ID_` varchar(64) collate utf8_bin default NULL,
  `CONFIGURATION_` varchar(255) collate utf8_bin default NULL,
  `CREATED_` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_EVENT_SUBSCR_CONFIG_` (`CONFIGURATION_`),
  KEY `ACT_FK_EVENT_EXEC` (`EXECUTION_ID_`),
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_EVENT_SUBSCR
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_EXECUTION`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_EXECUTION`;
CREATE TABLE `ACT_RU_EXECUTION` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `BUSINESS_KEY_` varchar(255) collate utf8_bin default NULL,
  `PARENT_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `SUPER_EXEC_` varchar(64) collate utf8_bin default NULL,
  `ACT_ID_` varchar(255) collate utf8_bin default NULL,
  `IS_ACTIVE_` tinyint(4) default NULL,
  `IS_CONCURRENT_` tinyint(4) default NULL,
  `IS_SCOPE_` tinyint(4) default NULL,
  `IS_EVENT_SCOPE_` tinyint(4) default NULL,
  `SUSPENSION_STATE_` int(11) default NULL,
  `CACHED_ENT_STATE_` int(11) default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  `NAME_` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  KEY `ACT_FK_EXE_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_EXECUTION
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_IDENTITYLINK`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_IDENTITYLINK`;
CREATE TABLE `ACT_RU_IDENTITYLINK` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `GROUP_ID_` varchar(255) collate utf8_bin default NULL,
  `TYPE_` varchar(255) collate utf8_bin default NULL,
  `USER_ID_` varchar(255) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_IDX_ATHRZ_PROCEDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  KEY `ACT_FK_IDL_PROCINST` (`PROC_INST_ID_`),
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_IDENTITYLINK
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_JOB`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_JOB`;
CREATE TABLE `ACT_RU_JOB` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `TYPE_` varchar(255) collate utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp NULL default NULL,
  `LOCK_OWNER_` varchar(255) collate utf8_bin default NULL,
  `EXCLUSIVE_` tinyint(1) default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `RETRIES_` int(11) default NULL,
  `EXCEPTION_STACK_ID_` varchar(64) collate utf8_bin default NULL,
  `EXCEPTION_MSG_` varchar(4000) collate utf8_bin default NULL,
  `DUEDATE_` timestamp NULL default NULL,
  `REPEAT_` varchar(255) collate utf8_bin default NULL,
  `HANDLER_TYPE_` varchar(255) collate utf8_bin default NULL,
  `HANDLER_CFG_` varchar(4000) collate utf8_bin default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  PRIMARY KEY  (`ID_`),
  KEY `ACT_FK_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_JOB
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_TASK`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_TASK`;
CREATE TABLE `ACT_RU_TASK` (
  `ID_` varchar(64) collate utf8_bin NOT NULL default '',
  `REV_` int(11) default NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_DEF_ID_` varchar(64) collate utf8_bin default NULL,
  `NAME_` varchar(255) collate utf8_bin default NULL,
  `PARENT_TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `DESCRIPTION_` varchar(4000) collate utf8_bin default NULL,
  `TASK_DEF_KEY_` varchar(255) collate utf8_bin default NULL,
  `OWNER_` varchar(255) collate utf8_bin default NULL,
  `ASSIGNEE_` varchar(255) collate utf8_bin default NULL,
  `DELEGATION_` varchar(64) collate utf8_bin default NULL,
  `PRIORITY_` int(11) default NULL,
  `CREATE_TIME_` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `DUE_DATE_` datetime default NULL,
  `CATEGORY_` varchar(255) collate utf8_bin default NULL,
  `SUSPENSION_STATE_` int(11) default NULL,
  `TENANT_ID_` varchar(255) collate utf8_bin default '',
  `FORM_KEY_` varchar(255) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_TASK
-- ----------------------------

-- ----------------------------
-- Table structure for `ACT_RU_VARIABLE`
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_VARIABLE`;
CREATE TABLE `ACT_RU_VARIABLE` (
  `ID_` varchar(64) collate utf8_bin NOT NULL,
  `REV_` int(11) default NULL,
  `TYPE_` varchar(255) collate utf8_bin NOT NULL,
  `NAME_` varchar(255) collate utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) collate utf8_bin default NULL,
  `PROC_INST_ID_` varchar(64) collate utf8_bin default NULL,
  `TASK_ID_` varchar(64) collate utf8_bin default NULL,
  `BYTEARRAY_ID_` varchar(64) collate utf8_bin default NULL,
  `DOUBLE_` double default NULL,
  `LONG_` bigint(20) default NULL,
  `TEXT_` varchar(4000) collate utf8_bin default NULL,
  `TEXT2_` varchar(4000) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID_`),
  KEY `ACT_IDX_VARIABLE_TASK_ID` (`TASK_ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of ACT_RU_VARIABLE
-- ----------------------------

-- ----------------------------
-- Table structure for `ec_department`
-- ----------------------------
DROP TABLE IF EXISTS `ec_department`;
CREATE TABLE `ec_department` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `department_name` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '部门名称',
  `parent_id` int(11) default NULL COMMENT '父级部门ID',
  `top_role_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `department_fk` (`parent_id`),
  KEY `fk_role` (`top_role_id`),
  CONSTRAINT `ec_department_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `ec_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role` FOREIGN KEY (`top_role_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_department
-- ----------------------------
INSERT INTO `ec_department` VALUES ('81', '经理部', null, '130');
INSERT INTO `ec_department` VALUES ('82', '工程部', '81', '132');
INSERT INTO `ec_department` VALUES ('83', '技术部', '81', '133');
INSERT INTO `ec_department` VALUES ('84', '管道队', '81', '136');
INSERT INTO `ec_department` VALUES ('85', '技术管理室', '83', '134');
INSERT INTO `ec_department` VALUES ('86', '施工管理室', '84', '136');
INSERT INTO `ec_department` VALUES ('89', '质检部', '81', '145');
INSERT INTO `ec_department` VALUES ('91', 'A见证组', '89', '141');
INSERT INTO `ec_department` VALUES ('92', 'B见证组', '89', '141');
INSERT INTO `ec_department` VALUES ('93', '毛国强班', '86', '137');
INSERT INTO `ec_department` VALUES ('94', '张孝林班', '86', '137');
INSERT INTO `ec_department` VALUES ('95', '李和平班', '86', '137');

-- ----------------------------
-- Table structure for `ec_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ec_menu`;
CREATE TABLE `ec_menu` (
  `id` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '序号UID',
  `menu_name` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '菜单名',
  `seq` int(255) NOT NULL default '0' COMMENT '队列序号',
  `icon_path` varchar(255) collate utf8_unicode_ci default NULL COMMENT '图标路径',
  `style` varchar(255) collate utf8_unicode_ci default NULL COMMENT '菜单样式',
  `privilege_id` int(255) default NULL,
  `parent_id` varchar(255) collate utf8_unicode_ci default NULL COMMENT '父级ID',
  PRIMARY KEY  (`id`),
  KEY `m_p_fk` (`privilege_id`),
  KEY `m_m_fk` (`parent_id`),
  CONSTRAINT `ec_menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `ec_menu` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `ec_menu_ibfk_2` FOREIGN KEY (`privilege_id`) REFERENCES `ec_privilege` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_menu
-- ----------------------------
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c2d20000', '菜单列表', '10', '', '', '1913', '4028820b4de2263e014de22707420000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c32a0001', '添加菜单', '10', '', '', '1914', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c35d0002', '修改菜单', '9', '', '', '1915', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c39f0003', '删除菜单', '0', '', '', '1916', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22707420000', '菜单管理', '0', '', 'icon-list-alt', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22937af0001', '结构设置', '9', '', 'icon-cogs', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22960f90002', '人员列表', '10', '', '', '1918', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22984c40003', '添加人员', '0', '', '', '1919', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229a3440004', '修改用户', '0', '', null, '1921', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229d3790005', '查看人员', '0', '', '', '1920', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229efbd0006', '删除人员', '0', '', '', '1922', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22bb0d70007', '职务列表', '9', '', '', '1929', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22bdbc60008', '添加职务', '0', '', '', '1930', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d141a0009', '修改职务', '0', '', '', '1931', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d59ab000a', '分配角色', '0', '', null, '1923', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d87ea000b', '重置密码', '0', '', null, '1924', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22dc365000c', '删除职务', '0', '', '', '1932', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e1e36000d', '施工班组列表', '0', '', '', '1925', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e441a000e', '添加部门', '0', '', null, '1926', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e6c7a000f', '修改部门', '0', '', null, '1927', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e8abf0010', '删除部门', '0', '', null, '1928', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22ec9f70011', '权限列表', '0', '', null, '1933', '4028820b4de2263e014de22707420000');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22eee690012', '添加权限', '0', '', null, '1934', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f12ea0013', '修改权限', '0', '', null, '1935', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f383c0014', '删除权限', '0', '', null, '1936', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f5f790015', '设置', '10', '', 'icon-cog', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f8af60016', '系统设置', '0', '', null, '1939', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('402882234de73907014de74441bd0000', '分派到班组', '0', '', null, '1964', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de57aaada0000', '问题跟踪', '4', '', 'icon-retweet', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de581cd4a0003', '需要处理的问题', '112', '', null, '2000', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de582626b0004', '需要确认的问题', '122', '', '', '2010', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de582bad30005', '未解决问题', '11', '', '', '2004', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de58316230006', '已解决问题', '111', '', null, '2002', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64782700000', '关注的问题', '1', '', null, '2013', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64883a90001', '发起的问题', '1', '', '', '2007', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64942450002', '基础设置', '8', '', 'icon-wrench', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6497ec80003', '施工管理', '7', '', 'icon-tasks', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de649c12f0004', '工序见证', '5', '', 'icon-eye-open', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de649effa0005', '统计信息', '3', '', 'icon-bar-chart', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64ed1880007', '单价管理', '0', '', '', '1941', '4028824c4de60d34014de64942450002');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64f3de40008', '材质类型', '0', '', null, '1945', '4028824c4de60d34014de64942450002');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65227c60009', '分派到班组', '0', '', null, '1963', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65291fe000a', '分派到组长', '0', '', null, '1968', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6542bb3000b', '我的任务', '0', '', null, '1973', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6566e51000c', '班组任务统计', '0', '', null, '1983', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6588d1c000d', '任务状态统计', '0', '', null, '1985', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de658f3e7000e', '任务完成统计', '0', '', null, '1986', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de659497a000f', '任务价格统计', '0', '', null, '1987', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65a25cc0010', '各组任务统计', '0', '', null, '1984', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de65feaf10000', '分派见证', '0', '', null, '2016', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66090c40001', '已分派见证', '0', '', null, '2018', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66104700002', '分派到见证人', '0', '', null, '2017', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de661859f0003', '改派', '0', '', null, '2019', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de661c5bb0004', '解除分派', '0', '', null, '2020', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66225930005', '未完成见证', '0', '', null, '2021', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de662c1f50006', '见证结果', '0', '', '', '2022', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66350530007', '已完成见证', '0', '', null, '2023', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de6638f740008', '修改见证结果', '0', '', '', '2024', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de663f5060009', '我发起的见证', '0', '', null, '2025', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de664be85000a', '我的见证', '0', '', null, '2026', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c2d0a10000', '问题确认', '0', '', null, '2009', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c43db50001', '发起的问题明细', '0', '', '', '2035', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c56efb0002', '处理问题', '0', '', null, '1996', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c60ea70003', '删除问题', '0', '', null, '2006', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c65eef0004', '已解决问题明细', '0', '', null, '2001', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c6a5730005', '未解决问题明细', '0', '', null, '2003', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c701250006', '发起问题明细', '0', '', null, '2005', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c77ce10007', '关注的问题明细', '0', '', null, '2011', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c7d6f20008', '创建问题', '0', '', null, '1997', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c8b5a30009', '解决人', '0', '', null, '1998', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c94e21000a', '增加材质类型', '0', '', null, '1946', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c98972000b', '删除材质类型', '0', '', null, '1948', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c9d005000c', '查看材质类型', '0', '', null, '1947', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca2511000d', '更新材质类型', '0', '', null, '1949', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca5922000e', '添加单价', '0', '', null, '1942', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca89ee000f', '修改单价', '0', '', null, '1943', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cab21b0010', '删除单价', '0', '', null, '1944', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cc55b00011', '滚动计划', '0', '', null, '1950', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ccbd460012', '上传滚动计划', '0', '', null, '1951', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ccf0070013', '添加滚动计划', '0', '', null, '1952', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cd2dfd0014', '修改滚动计划', '0', '', null, '1953', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cd69d60015', '删除滚动计划', '0', '', '', '1954', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ce45390016', '查看工序步骤', '0', '', null, '1955', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ce848d0017', '添加工序步骤', '0', '', null, '1956', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ceb7e80018', '修改工序步骤', '0', '', null, '1957', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cef43f0019', '查看工序步骤明细', '0', '', null, '1958', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cf2ed9001a', '删除工序', '0', '', null, '1959', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cf5cce001b', '导入工序', '0', '', null, '1960', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d035ba001c', '见证人名词', '0', '', null, '1993', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d09202001d', '计划表名词', '0', '', null, '1989', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d0c32f001e', '单价名词', '0', '', null, '1992', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d101ff001f', '焊口/支架名词', '0', '', '', '1990', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d153780020', '滞后时间设置', '0', '', null, '1994', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d19c710021', '问题配置', '0', '', null, '1961', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d1f4d10022', '分派班组名词', '0', '', null, '1991', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d5c8df0023', '问题明细', '0', '', null, '1995', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d6451b0024', '删除关注人', '0', '', null, '2012', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d737b50025', '分派至组长', '0', '', '', '1969', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d8864b0026', '已分派到组长', '0', '', null, '1970', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d8d06f0027', '已分派到班组', '0', '', null, '1965', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d9a8700028', '解除分派班组', '0', '', null, '1967', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6da80a90029', '解除分派组长', '0', '', null, '1972', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6daed4b002a', '改派班组', '0', '', null, '1966', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6db20ef002b', '改派组长', '0', '', null, '1971', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6dd2ac2002c', '进入任务', '0', '', null, '1974', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6de1bf7002d', '发起见证', '0', '', null, '1975', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6de6b01002e', '见证状态', '0', '', null, '1976', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6debd0e002f', '回填信息', '0', '', null, '1977', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6df05a70030', '修改回填信息', '0', '', null, '1978', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6df7aa20031', '完成信息', '0', '', null, '1979', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6dfc6400032', '修改完成信息', '0', '', null, '1980', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e00b010033', '我完成的任务', '0', '', null, '1981', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e060cd0034', '上传文件', '0', '', null, '1999', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e0d9e60035', '上传计划文件', '0', '', null, '2031', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e17f6c0036', '上传图标', '0', '', null, '1937', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('402882f74e3811d0014e38191d5a0000', '下载滚动计划模板', '0', '', null, '2034', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c71cb060000', '我的见证结果', '0', '', null, '2038', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c7214090001', '修改我的见证结果', '0', '', null, '2039', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c7271970002', '分派完成情况', '0', '', null, '2040', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('402882f74e5cb6a3014e5cb79ecc0000', '我完成的见证', '0', '', null, '2041', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('402882f74e73208d014e732246ae0000', '工作流', '0', '', null, null, null);
INSERT INTO `ec_menu` VALUES ('402882f74e73208d014e7322c0af0001', '流程文件', '0', '', null, '2030', '402882f74e73208d014e732246ae0000');
INSERT INTO `ec_menu` VALUES ('402882f74e73208d014e732301ba0002', '发布', '0', '', null, '2033', '402882f74e73208d014e7322c0af0001');
INSERT INTO `ec_menu` VALUES ('402882f74e73208d014e7323832f0003', '删除', '0', '', null, '2032', '402882f74e73208d014e7322c0af0001');

-- ----------------------------
-- Table structure for `ec_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `ec_privilege`;
CREATE TABLE `ec_privilege` (
  `id` int(11) NOT NULL auto_increment COMMENT '默认ID',
  `privilege_name` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '权限名称',
  `uri` varchar(255) collate utf8_unicode_ci default NULL COMMENT '权限URI',
  `menu_id` varchar(255) collate utf8_unicode_ci default NULL,
  `create_time` datetime default NULL,
  `update_time` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `p_m_fk` (`menu_id`),
  CONSTRAINT `ec_privilege_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `ec_menu` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2042 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_privilege
-- ----------------------------
INSERT INTO `ec_privilege` VALUES ('1913', '菜单列表', '/management/menu', '4028820b4de223b4014de223c2d20000', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1914', '添加菜单', '/management/menu/add', '4028820b4de223b4014de223c32a0001', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1915', '修改菜单', '/management/menu/edit', '4028820b4de223b4014de223c35d0002', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1916', '删除菜单', '/management/menu/delete', '4028820b4de223b4014de223c39f0003', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1918', '用户管理', '/management/user', '4028820b4de2263e014de22960f90002', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1919', '添加用户', '/management/user/add', '4028820b4de2263e014de22984c40003', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1920', '查看用户', '/management/user/show', '4028820b4de2263e014de229d3790005', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1921', '修改用户', '/management/user/edit', '4028820b4de2263e014de229a3440004', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1922', '删除用户', '/management/user/delete', '4028820b4de2263e014de229efbd0006', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1923', '分配角色', '/management/user/role', '4028820b4de2263e014de22d59ab000a', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1924', '重置密码', '/management/user/password', '4028820b4de2263e014de22d87ea000b', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1925', '部门管理', '/management/department', '4028820b4de2263e014de22e1e36000d', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1926', '新建部门', '/management/department/add', '4028820b4de2263e014de22e441a000e', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1927', '编辑部门', '/management/department/edit', '4028820b4de2263e014de22e6c7a000f', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1928', '删除部门', '/management/department/delete', '4028820b4de2263e014de22e8abf0010', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1929', '角色管理', '/management/role', '4028820b4de2263e014de22bb0d70007', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1930', '添加角色', '/management/role/add', '4028820b4de2263e014de22bdbc60008', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1931', '修改角色', '/management/role/edit', '4028820b4de2263e014de22d141a0009', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1932', '删除角色', '/management/role/delete', '4028820b4de2263e014de22dc365000c', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1933', '权限管理', '/management/privilege', '4028820b4de2263e014de22ec9f70011', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1934', '添加权限', '/management/privilege/add', '4028820b4de2263e014de22eee690012', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1935', '修改权限', '/management/privilege/edit', '4028820b4de2263e014de22f12ea0013', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1936', '删除权限', '/management/privilege/delete', '4028820b4de2263e014de22f383c0014', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1937', '上传图标', '/management/privilege/upload', '4028824c4de6705f014de6e17f6c0036', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1939', '系统设置', '/management/settings/website', '4028820b4de2263e014de22f8af60016', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1941', '单价管理', '/baseservice/price', '4028824c4de60d34014de64ed1880007', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1942', '添加单价', '/baseservice/price/add', '4028824c4de6705f014de6ca5922000e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1943', '修改单价', '/baseservice/price/edit', '4028824c4de6705f014de6ca89ee000f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1944', '删除单价', '/baseservice/price/delete', '4028824c4de6705f014de6cab21b0010', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1945', '材质类型', '/baseservice/material_type', '4028824c4de60d34014de64f3de40008', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1946', '增加材质类型', '/baseservice/material_type/add', '4028824c4de6705f014de6c94e21000a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1947', '查看材质类型', '/baseservice/material_type/detail', '4028824c4de6705f014de6c9d005000c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1948', '删除材质类型', '/baseservice/material_type/delete', '4028824c4de6705f014de6c98972000b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1949', '更新材质类型', '/baseservice/material_type/edit', '4028824c4de6705f014de6ca2511000d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1950', '滚动计划', '/baseservice/rollingplan', '4028824c4de6705f014de6cc55b00011', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1951', '上传滚动计划', '/baseservice/rollingplan/import', '4028824c4de6705f014de6ccbd460012', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1952', '添加滚动计划', '/baseservice/rollingplan/add', '4028824c4de6705f014de6ccf0070013', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1953', '修改滚动计划', '/baseservice/rollingplan/edit', '4028824c4de6705f014de6cd2dfd0014', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1954', '删除滚动计划', '/baseservice/rollingplan/delete', '4028824c4de6705f014de6cd69d60015', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1955', '查看工序步骤', '/baseservice/rollingplan/workstep', '4028824c4de6705f014de6ce45390016', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1956', '添加工序步骤', '/baseservice/rollingplan/workstep/add', '4028824c4de6705f014de6ce848d0017', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1957', '修改工序步骤', '/baseservice/rollingplan/workstep/edit', '4028824c4de6705f014de6ceb7e80018', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1958', '查看工序步骤明细', '/baseservice/rollingplan/workstep/detail', '4028824c4de6705f014de6cef43f0019', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1959', '删除工序步骤', '/baseservice/rollingplan/workstep/delete', '4028824c4de6705f014de6cf2ed9001a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1960', '导入工序', '/baseservice/rollingplan/workstep/import', '4028824c4de6705f014de6cf5cce001b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1961', '问题配置', '/baseservice/setting/problem', '4028824c4de6705f014de6d19c710021', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1963', '分派到班组', '/construction/team', '4028824c4de60d34014de65227c60009', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1964', '分派到班组', '/construction/team/assign', '402882234de73907014de74441bd0000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1965', '已分派班组', '/construction/team/already', '4028824c4de6705f014de6d8d06f0027', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1966', '改派', '/construction/team/modify', '4028824c4de6705f014de6daed4b002a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1967', '解除分派', '/construction/team/release', '4028824c4de6705f014de6d9a8700028', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1968', '分派到组长', '/construction/endman', '4028824c4de60d34014de65291fe000a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1969', '分派到组长', '/construction/endman/assign', '4028824c4de6705f014de6d737b50025', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1970', '已分派组长', '/construction/endman/already', '4028824c4de6705f014de6d8864b0026', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1971', '改派', '/construction/endman/modify', '4028824c4de6705f014de6db20ef002b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1972', '解除分派', '/construction/endman/release', '4028824c4de6705f014de6da80a90029', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1973', '我的任务', '/construction/mytask', '4028824c4de60d34014de6542bb3000b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1974', '进入任务', '/construction/mytask/view', '4028824c4de6705f014de6dd2ac2002c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1975', '发起见证', '/construction/mytask/witness', '4028824c4de6705f014de6de1bf7002d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1976', '见证状态', '/construction/mytask/witness/view', '4028824c4de6705f014de6de6b01002e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1977', '回填信息', '/construction/mytask/workstep/result', '4028824c4de6705f014de6debd0e002f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1978', '修改回填信息', '/construction/mytask/workstep/result/edit', '4028824c4de6705f014de6df05a70030', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1979', '完成信息', '/construction/mytask/rollingplan/result', '4028824c4de6705f014de6df7aa20031', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1980', '修改完成信息', '/construction/mytask/rollingplan/result/edit', '4028824c4de6705f014de6dfc6400032', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1981', '我完成的任务', '/construction/mytask/already', '4028824c4de6705f014de6e00b010033', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1983', '班组任务统计', '/statistics/taskteam', '4028824c4de60d34014de6566e51000c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1984', '各组任务统计', '/statistics/taskgroup', '4028824c4de60d34014de65a25cc0010', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1985', '任务状态统计', '/statistics/taskstatus', '4028824c4de60d34014de6588d1c000d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1986', '任务完成统计', '/statistics/hyperbola', '4028824c4de60d34014de658f3e7000e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1987', '任务价格统计', '/statistics/taskprice', '4028824c4de60d34014de659497a000f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1989', '计划表名词', '/variable/planningtable', '4028824c4de6705f014de6d09202001d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1990', '焊口/支架名词', '/variable/welddistinguish', '4028824c4de6705f014de6d101ff001f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1991', '分派班组名词', '/variable/assigntype', '4028824c4de6705f014de6d1f4d10022', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1992', '单价名词', '/variable/price', '4028824c4de6705f014de6d0c32f001e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1993', '见证人名词', '/variable/witnesstype', '4028824c4de6705f014de6d035ba001c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1994', '滞后时间设置', '/variable/hysteresis', '4028824c4de6705f014de6d153780020', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1995', '问题明细', '/hd/workstep/problem/detail', '4028824c4de6705f014de6d5c8df0023', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1996', '处理问题', '/hd/workstep/problem/handle', '4028824c4de6705f014de6c56efb0002', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1997', '创建问题', '/hd/workstep/problem/add', '4028824c4de6705f014de6c7d6f20008', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1998', '解决人', '/hd/workstep/problem/solvers', '4028824c4de6705f014de6c8b5a30009', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1999', '上传文件', '/hd/workstep/problem/upload', '4028824c4de6705f014de6e060cd0034', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2000', '需要处理的问题', '/hd/workstep/problem', '4028824c4de578a7014de581cd4a0003', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2001', '已解决问题明细', '/hd/workstep/problem/solved/detail', '4028824c4de6705f014de6c65eef0004', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2002', '已解决问题', '/hd/workstep/problem/solved', '4028824c4de578a7014de58316230006', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2003', '未解决问题明细', '/hd/workstep/problem/unsolved/detail', '4028824c4de6705f014de6c6a5730005', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2004', '未解决问题', '/hd/workstep/problem/unsolved', '4028824c4de578a7014de582bad30005', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2005', '发起的问题明细', '/hd/workstep/problem/created/detail', '4028824c4de6705f014de6c701250006', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2006', '删除问题', '/hd/workstep/problem/created/delete', '4028824c4de6705f014de6c60ea70003', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2007', '发起的问题', '/hd/workstep/problem/created', '4028824c4de60d34014de64883a90001', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2008', '发起的问题明细', '/hd/workstep/problem/toconfirm/detail', '4028824c4de6705f014de6c43db50001', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2009', '问题确认', '/hd/workstep/problem/solveconfirm', '4028824c4de6705f014de6c2d0a10000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2010', '需要确认的问题', '/hd/workstep/problem/toconfirm', '4028824c4de578a7014de582626b0004', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2011', '关注的问题明细', '/hd/workstep/problem/toconcerned/detail', '4028824c4de6705f014de6c77ce10007', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2012', '删除关注人', '/hd/workstep/problem/concernman/delete', '4028824c4de6705f014de6d6451b0024', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2013', '关注的问题', '/hd/workstep/problem/toconcerned', '4028824c4de60d34014de64782700000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2016', '分派见证', '/witness/witnesser', '4028824c4de65ea6014de65feaf10000', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2017', '分派到见证人', '/witness/witnesser/assign', '4028824c4de65ea6014de66104700002', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2018', '已分派见证', '/witness/witnesser/already', '4028824c4de65ea6014de66090c40001', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2019', '改派', '/witness/witnesser/modify', '4028824c4de65ea6014de661859f0003', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2020', '解除分派', '/witness/witnesser/release', '4028824c4de65ea6014de661c5bb0004', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2021', '未完成见证', '/witness/witnesser/uncomplete', '4028824c4de65ea6014de66225930005', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2022', '见证结果', '/witness/witnesser/result', '4028824c4de65ea6014de662c1f50006', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2023', '已完成见证', '/witness/witnesser/complete', '4028824c4de65ea6014de66350530007', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2024', '修改见证结果', '/witness/witnesser/result/edit', '4028824c4de65ea6014de6638f740008', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2025', '我发起的见证', '/witness/mylaunch', '4028824c4de65ea6014de663f5060009', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2026', '我的见证', '/witness/myevent', '4028824c4de65ea6014de664be85000a', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2027', '见证结果', '/witness/myevent/result', '4028824c4de65ea6014de662c1f50006', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2028', '修改见证结果', '/witness/myevent/result/edit', '4028824c4de65ea6014de6638f740008', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2030', '流程文件', '/workflow/zips', '402882f74e73208d014e7322c0af0001', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2031', '上传文件', '/workflow/zips/upload', '4028824c4de6705f014de6e0d9e60035', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2032', '删除', '/workflow/zips/delete', '402882f74e73208d014e7323832f0003', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2033', '发布', '/workflow/zips/deploy', '402882f74e73208d014e732301ba0002', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2034', '下载滚动计划模板', '/baseservice/rollingplan/download', '402882f74e3811d0014e38191d5a0000', '2015-06-28 10:10:08', null);
INSERT INTO `ec_privilege` VALUES ('2035', '确认的问题明细', '/hd/workstep/problem/confirm/detail', '4028824c4de6705f014de6c43db50001', '2015-07-03 13:35:01', null);
INSERT INTO `ec_privilege` VALUES ('2036', '滚动计划问题', '/hd/workstep/problem/rollingplan', null, '2015-07-04 17:32:28', null);
INSERT INTO `ec_privilege` VALUES ('2037', '滚动计划问题明细', '/hd/workstep/problem/rollingplan/detail', null, '2015-07-04 17:32:28', null);
INSERT INTO `ec_privilege` VALUES ('2038', '我的见证结果', '/witness/myevent/launch', '402882f74e5c6fc1014e5c71cb060000', '2015-07-05 10:25:35', null);
INSERT INTO `ec_privilege` VALUES ('2039', '修改我的见证结果', '/witness/myevent/launch/edit', '402882f74e5c6fc1014e5c7214090001', '2015-07-05 10:25:35', null);
INSERT INTO `ec_privilege` VALUES ('2040', '分派完成情况', '/witness/witnesser/assign/view', '402882f74e5c6fc1014e5c7271970002', '2015-07-05 12:18:29', null);
INSERT INTO `ec_privilege` VALUES ('2041', '我完成的见证', '/witness/myevent/already', '402882f74e5cb6a3014e5cb79ecc0000', '2015-07-05 13:35:54', null);

-- ----------------------------
-- Table structure for `ec_role`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role`;
CREATE TABLE `ec_role` (
  `id` int(11) NOT NULL auto_increment,
  `role_name` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '角色名',
  `parent_id` int(11) default NULL,
  `level` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `ec_parent_fk` (`parent_id`),
  CONSTRAINT `ec_parent_fk` FOREIGN KEY (`parent_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role
-- ----------------------------
INSERT INTO `ec_role` VALUES ('130', '总经理', null, '1');
INSERT INTO `ec_role` VALUES ('131', '技术经理', '130', '2');
INSERT INTO `ec_role` VALUES ('132', '工程部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('133', '技术部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('134', '管理室主任', '133', '4');
INSERT INTO `ec_role` VALUES ('135', '工程师', '134', '5');
INSERT INTO `ec_role` VALUES ('136', '队长', '131', '3');
INSERT INTO `ec_role` VALUES ('137', '班长', '136', '4');
INSERT INTO `ec_role` VALUES ('138', '组长', '137', '5');
INSERT INTO `ec_role` VALUES ('139', '计划部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('140', '计划员', '139', '4');
INSERT INTO `ec_role` VALUES ('141', '见证组组长', '145', '4');
INSERT INTO `ec_role` VALUES ('142', 'W级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('143', 'H级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('144', 'R级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('145', '质检部部长', '131', '3');

-- ----------------------------
-- Table structure for `ec_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role_menu`;
CREATE TABLE `ec_role_menu` (
  `role_id` int(255) NOT NULL,
  `menu_id` varchar(255) collate utf8_unicode_ci default NULL,
  KEY `r_middle_fk` (`role_id`),
  KEY `m_middle_fk` (`menu_id`),
  CONSTRAINT `ec_role_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `ec_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_role_menu_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role_menu
-- ----------------------------
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('118', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('119', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('120', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('130', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('131', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('132', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('133', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('134', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('135', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('136', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('142', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('142', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('143', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('143', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('146', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('146', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('139', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('140', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('147', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('148', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('149', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('145', '402882f74e3811d0014e38191d5a0000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('145', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('145', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('144', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('144', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('144', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('144', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('150', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('150', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('150', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('137', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('138', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('138', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('138', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de6588d1c000d');
INSERT INTO `ec_role_menu` VALUES ('138', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('141', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('141', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('141', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('141', '402882f74e5c6fc1014e5c7271970002');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('141', '4028824c4de65ea6014de66104700002');

-- ----------------------------
-- Table structure for `ec_role_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role_privilege`;
CREATE TABLE `ec_role_privilege` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `privilege_id` int(11) NOT NULL COMMENT '权限ID',
  PRIMARY KEY  (`role_id`,`privilege_id`),
  KEY `fk_role_privilege` (`role_id`),
  KEY `fk_privilege_role` (`privilege_id`),
  CONSTRAINT `ec_role_privilege_ibfk_1` FOREIGN KEY (`privilege_id`) REFERENCES `ec_privilege` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_role_privilege_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role_privilege
-- ----------------------------

-- ----------------------------
-- Table structure for `ec_site`
-- ----------------------------
DROP TABLE IF EXISTS `ec_site`;
CREATE TABLE `ec_site` (
  `id` int(11) NOT NULL auto_increment COMMENT '默认ID',
  `site_name` varchar(255) collate utf8_unicode_ci default NULL COMMENT '网站名称',
  `site_domain` varchar(100) collate utf8_unicode_ci default NULL COMMENT '域名',
  `site_key` varchar(255) collate utf8_unicode_ci default NULL COMMENT '关键字',
  `site_describe` varchar(255) collate utf8_unicode_ci default NULL COMMENT '网站描述',
  `site_tpl_name` varchar(50) collate utf8_unicode_ci default NULL COMMENT '模板名称',
  `upload_max_size` int(11) default '0' COMMENT '上传文件大小',
  `upload_tpl_dir` varchar(255) collate utf8_unicode_ci default NULL COMMENT '模板上传目录',
  `tpl_file_type` varchar(50) collate utf8_unicode_ci default NULL COMMENT '模板上传类型',
  `upload_res_dir` varchar(255) collate utf8_unicode_ci default NULL COMMENT '资源上传目录',
  `is_local_site` tinyint(1) NOT NULL default '0' COMMENT '是否本地站点',
  `app_path` varchar(255) collate utf8_unicode_ci default NULL COMMENT '站点绝对路径',
  `i18n_path` varchar(255) collate utf8_unicode_ci default NULL COMMENT '国际化目录',
  `pagesize_list` varchar(100) collate utf8_unicode_ci default NULL,
  `pagesize` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_site
-- ----------------------------
INSERT INTO `ec_site` VALUES ('9', 'Test System', 'asdas', 'asda', 'sdsad', 'easycms', null, null, null, null, '1', '/', null, '[20,50,100]', '20');

-- ----------------------------
-- Table structure for `ec_user`
-- ----------------------------
DROP TABLE IF EXISTS `ec_user`;
CREATE TABLE `ec_user` (
  `id` int(11) NOT NULL auto_increment COMMENT '默认ID',
  `username` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '用户名唯一',
  `password` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '密码',
  `real_name` varchar(100) collate utf8_unicode_ci default NULL COMMENT '真是姓名',
  `email` varchar(50) collate utf8_unicode_ci default NULL COMMENT 'email',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) collate utf8_unicode_ci NOT NULL default '127.0.0.1' COMMENT '注册IP',
  `current_login_time` datetime default NULL COMMENT '当前登录时间',
  `current_login_ip` varchar(50) collate utf8_unicode_ci default '127.0.0.1' COMMENT '当前登录IP',
  `last_login_time` datetime default NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(50) collate utf8_unicode_ci default '127.0.0.1' COMMENT '上次登录IP',
  `login_times` int(11) default NULL COMMENT '登录次数',
  `is_default_admin` tinyint(1) NOT NULL default '0' COMMENT '是否默认管理员',
  `is_del` tinyint(1) default '0',
  `device` varchar(100) collate utf8_unicode_ci default NULL,
  `parent_id` int(100) default NULL,
  `department_id` int(100) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_parent` (`parent_id`),
  KEY `fk_department` (`department_id`),
  CONSTRAINT `fk_department` FOREIGN KEY (`department_id`) REFERENCES `ec_department` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_parent` FOREIGN KEY (`parent_id`) REFERENCES `ec_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_user
-- ----------------------------
INSERT INTO `ec_user` VALUES ('8', 'admin', '594afe5e1f8ac83827ca43434f7dae1a', '邓洁芃', '', '2014-01-01 18:20:59', '127.0.0.1', '2015-07-22 17:27:01', '192.168.2.163', '2015-07-22 16:43:21', '192.168.2.163', '1459', '1', '0', 'zzzzzz', null, null);
INSERT INTO `ec_user` VALUES ('89', 'zhangxiaolin', '594afe5e1f8ac83827ca43434f7dae1a', '张孝林', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-07-22 15:27:38', '192.168.2.163', '2015-06-14 22:41:42', '192.168.2.119', '77', '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('91', 'liheping', '594afe5e1f8ac83827ca43434f7dae1a', '李和平', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-14 21:40:08', '192.168.2.119', '2015-06-04 22:23:48', '192.168.2.119', '24', '0', '0', null, null, '95');
INSERT INTO `ec_user` VALUES ('92', 'maoguoqiang', '594afe5e1f8ac83827ca43434f7dae1a', '毛国强', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-05-29 15:56:41', '192.168.2.119', null, '127.0.0.1', '1', '0', '0', null, null, '93');
INSERT INTO `ec_user` VALUES ('95', 'jianli', '594afe5e1f8ac83827ca43434f7dae1a', '监理', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-07-22 17:18:29', '192.168.2.163', null, '127.0.0.1', '1', '0', '0', null, null, '91');
INSERT INTO `ec_user` VALUES ('101', 'gaobo', '594afe5e1f8ac83827ca43434f7dae1a', '高博', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '92');
INSERT INTO `ec_user` VALUES ('106', 'maosan', '594afe5e1f8ac83827ca43434f7dae1a', '毛三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '93');
INSERT INTO `ec_user` VALUES ('107', 'maoer', '594afe5e1f8ac83827ca43434f7dae1a', '毛二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '93');
INSERT INTO `ec_user` VALUES ('108', 'maoyi', '594afe5e1f8ac83827ca43434f7dae1a', '毛一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '93');
INSERT INTO `ec_user` VALUES ('109', 'lisan', '594afe5e1f8ac83827ca43434f7dae1a', '李三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '95');
INSERT INTO `ec_user` VALUES ('110', 'lier', '594afe5e1f8ac83827ca43434f7dae1a', '李二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '95');
INSERT INTO `ec_user` VALUES ('111', 'liyi', '594afe5e1f8ac83827ca43434f7dae1a', '李一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '95');
INSERT INTO `ec_user` VALUES ('112', 'zhangsan', '594afe5e1f8ac83827ca43434f7dae1a', '张三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('113', 'zhanger', '594afe5e1f8ac83827ca43434f7dae1a', '张二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('114', 'zhangyi', '594afe5e1f8ac83827ca43434f7dae1a', '张一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('116', 'zhoujuan', '594afe5e1f8ac83827ca43434f7dae1a', '周娟', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '91');
INSERT INTO `ec_user` VALUES ('118', 'yangkaifeng', '594afe5e1f8ac83827ca43434f7dae1a', '杨凯峰', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-14 17:33:01', '192.168.2.119', '2015-06-11 20:33:23', '192.168.2.119', '11', '0', '0', null, null, '91');
INSERT INTO `ec_user` VALUES ('131', 'banzhang', '594afe5e1f8ac83827ca43434f7dae1a', '班长', '', '2015-06-07 16:31:04', '192.168.2.119', '2015-06-14 21:06:11', '192.168.2.119', '2015-06-14 20:31:07', '192.168.2.119', '51', '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('132', 'zuzhang', '594afe5e1f8ac83827ca43434f7dae1a', '施工组长', '', '2015-06-07 16:33:59', '192.168.2.119', '2015-07-22 17:09:41', '192.168.2.163', '2015-07-22 16:24:15', '192.168.2.163', '83', '0', '0', null, null, '94');
INSERT INTO `ec_user` VALUES ('136', 'jhy', '594afe5e1f8ac83827ca43434f7dae1a', '计划员', 'jhy@hd.com', '2015-06-07 20:02:30', '192.168.2.119', '2015-06-14 19:34:24', '192.168.2.119', '2015-06-14 17:31:36', '192.168.2.119', '32', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('138', 'lindong1', '594afe5e1f8ac83827ca43434f7dae1a', '林动', '', '2015-06-09 17:53:27', '192.168.2.119', '2015-06-14 21:59:40', '192.168.2.119', '2015-06-14 21:44:46', '192.168.2.119', '17', '0', '0', null, null, '91');
INSERT INTO `ec_user` VALUES ('139', 'xxx', '594afe5e1f8ac83827ca43434f7dae1a', 'xxx', null, '2015-07-22 14:19:00', '127.0.0.1', '2015-07-22 14:19:04', '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);

-- ----------------------------
-- Table structure for `ec_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `ec_user_role`;
CREATE TABLE `ec_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY  (`user_id`,`role_id`),
  KEY `fk_ec_user_role` (`user_id`),
  KEY `fk_ec_role_user` (`role_id`),
  CONSTRAINT `ec_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ec_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_user_role
-- ----------------------------
INSERT INTO `ec_user_role` VALUES ('89', '137');
INSERT INTO `ec_user_role` VALUES ('91', '137');
INSERT INTO `ec_user_role` VALUES ('92', '137');
INSERT INTO `ec_user_role` VALUES ('95', '141');
INSERT INTO `ec_user_role` VALUES ('101', '141');
INSERT INTO `ec_user_role` VALUES ('106', '138');
INSERT INTO `ec_user_role` VALUES ('107', '138');
INSERT INTO `ec_user_role` VALUES ('108', '138');
INSERT INTO `ec_user_role` VALUES ('109', '138');
INSERT INTO `ec_user_role` VALUES ('110', '138');
INSERT INTO `ec_user_role` VALUES ('111', '138');
INSERT INTO `ec_user_role` VALUES ('112', '138');
INSERT INTO `ec_user_role` VALUES ('113', '138');
INSERT INTO `ec_user_role` VALUES ('114', '138');
INSERT INTO `ec_user_role` VALUES ('116', '142');
INSERT INTO `ec_user_role` VALUES ('118', '142');
INSERT INTO `ec_user_role` VALUES ('131', '137');
INSERT INTO `ec_user_role` VALUES ('132', '138');
INSERT INTO `ec_user_role` VALUES ('136', '140');
INSERT INTO `ec_user_role` VALUES ('138', '141');
INSERT INTO `ec_user_role` VALUES ('149', '129');
INSERT INTO `ec_user_role` VALUES ('153', '132');
INSERT INTO `ec_user_role` VALUES ('155', '134');
INSERT INTO `ec_user_role` VALUES ('160', '120');
INSERT INTO `ec_user_role` VALUES ('161', '118');
INSERT INTO `ec_user_role` VALUES ('162', '128');
INSERT INTO `ec_user_role` VALUES ('163', '133');
INSERT INTO `ec_user_role` VALUES ('164', '130');
INSERT INTO `ec_user_role` VALUES ('165', '131');
INSERT INTO `ec_user_role` VALUES ('166', '146');
INSERT INTO `ec_user_role` VALUES ('167', '141');
INSERT INTO `ec_user_role` VALUES ('168', '138');
INSERT INTO `ec_user_role` VALUES ('168', '139');
INSERT INTO `ec_user_role` VALUES ('168', '140');
INSERT INTO `ec_user_role` VALUES ('169', '138');
INSERT INTO `ec_user_role` VALUES ('170', '137');
INSERT INTO `ec_user_role` VALUES ('171', '129');
INSERT INTO `ec_user_role` VALUES ('172', '119');
INSERT INTO `ec_user_role` VALUES ('173', '142');
INSERT INTO `ec_user_role` VALUES ('174', '145');
INSERT INTO `ec_user_role` VALUES ('175', '143');
INSERT INTO `ec_user_role` VALUES ('176', '143');
INSERT INTO `ec_user_role` VALUES ('177', '125');
INSERT INTO `ec_user_role` VALUES ('178', '122');
INSERT INTO `ec_user_role` VALUES ('179', '121');
INSERT INTO `ec_user_role` VALUES ('180', '127');
INSERT INTO `ec_user_role` VALUES ('181', '124');
INSERT INTO `ec_user_role` VALUES ('182', '126');
INSERT INTO `ec_user_role` VALUES ('196', '144');
INSERT INTO `ec_user_role` VALUES ('197', '147');
INSERT INTO `ec_user_role` VALUES ('199', '149');

-- ----------------------------
-- Table structure for `ec_workflow_trace_task`
-- ----------------------------
DROP TABLE IF EXISTS `ec_workflow_trace_task`;
CREATE TABLE `ec_workflow_trace_task` (
  `trace_id` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT '跟踪ID',
  `pid` varchar(100) collate utf8_unicode_ci default NULL COMMENT '流程实例ID',
  `pdid` varchar(100) collate utf8_unicode_ci default NULL COMMENT '流程定义ID',
  `deploy_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '发布人ID',
  `current_handle_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '当前处理人id',
  `pre_handle_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '前处理人ID',
  `task_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '节点ID',
  `pre_task_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '前节点ID',
  `start_date` date default NULL COMMENT '开始日期',
  `end_date` date default NULL COMMENT '结束日期',
  `handle_date` date default NULL COMMENT '处理时间',
  `duration` int(11) default NULL COMMENT '持续时间',
  `task_status` varchar(255) collate utf8_unicode_ci default NULL COMMENT '任务状态',
  PRIMARY KEY  (`trace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_workflow_trace_task
-- ----------------------------

-- ----------------------------
-- Table structure for `material_type`
-- ----------------------------
DROP TABLE IF EXISTS `material_type`;
CREATE TABLE `material_type` (
  `id` int(11) NOT NULL auto_increment,
  `materialtypeid` varchar(20) NOT NULL default '',
  `materialtypename` varchar(20) default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of material_type
-- ----------------------------
INSERT INTO `material_type` VALUES ('1', 'CS', '碳钢', '2015-04-10 08:41:56', 'admin', '2015-04-10 01:15:27', 'admin');
INSERT INTO `material_type` VALUES ('2', 'SS', '不锈钢', null, null, '2015-06-03 22:21:46', 'admin');

-- ----------------------------
-- Table structure for `MONTH_DAY`
-- ----------------------------
DROP TABLE IF EXISTS `MONTH_DAY`;
CREATE TABLE `MONTH_DAY` (
  `DAY_NUM` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of MONTH_DAY
-- ----------------------------
INSERT INTO `MONTH_DAY` VALUES ('1');
INSERT INTO `MONTH_DAY` VALUES ('2');
INSERT INTO `MONTH_DAY` VALUES ('3');
INSERT INTO `MONTH_DAY` VALUES ('4');
INSERT INTO `MONTH_DAY` VALUES ('5');
INSERT INTO `MONTH_DAY` VALUES ('6');
INSERT INTO `MONTH_DAY` VALUES ('7');
INSERT INTO `MONTH_DAY` VALUES ('8');
INSERT INTO `MONTH_DAY` VALUES ('9');
INSERT INTO `MONTH_DAY` VALUES ('10');
INSERT INTO `MONTH_DAY` VALUES ('11');
INSERT INTO `MONTH_DAY` VALUES ('12');
INSERT INTO `MONTH_DAY` VALUES ('13');
INSERT INTO `MONTH_DAY` VALUES ('14');
INSERT INTO `MONTH_DAY` VALUES ('15');
INSERT INTO `MONTH_DAY` VALUES ('16');
INSERT INTO `MONTH_DAY` VALUES ('17');
INSERT INTO `MONTH_DAY` VALUES ('18');
INSERT INTO `MONTH_DAY` VALUES ('19');
INSERT INTO `MONTH_DAY` VALUES ('20');
INSERT INTO `MONTH_DAY` VALUES ('21');
INSERT INTO `MONTH_DAY` VALUES ('22');
INSERT INTO `MONTH_DAY` VALUES ('23');
INSERT INTO `MONTH_DAY` VALUES ('24');
INSERT INTO `MONTH_DAY` VALUES ('25');
INSERT INTO `MONTH_DAY` VALUES ('26');
INSERT INTO `MONTH_DAY` VALUES ('27');
INSERT INTO `MONTH_DAY` VALUES ('28');
INSERT INTO `MONTH_DAY` VALUES ('29');
INSERT INTO `MONTH_DAY` VALUES ('30');
INSERT INTO `MONTH_DAY` VALUES ('31');

-- ----------------------------
-- Table structure for `price`
-- ----------------------------
DROP TABLE IF EXISTS `price`;
CREATE TABLE `price` (
  `autoid` int(11) NOT NULL auto_increment,
  `speciality` varchar(20) default NULL,
  `pricetype` varchar(20) default NULL,
  `unitprice` decimal(10,2) default NULL,
  `remark` varchar(50) default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) default NULL,
  PRIMARY KEY  (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of price
-- ----------------------------
INSERT INTO `price` VALUES ('37', 'GDZJ', 'GS', '2.00', '', '2015-06-07 16:34:22', 'admin', null, null);
INSERT INTO `price` VALUES ('38', 'GDHK', 'GCL', '20.00', '', '2015-06-07 16:34:37', 'admin', null, null);
INSERT INTO `price` VALUES ('39', 'GDHK', 'DZ', '15.00', '', '2015-06-07 16:34:47', 'admin', null, null);
INSERT INTO `price` VALUES ('40', 'GDHK', 'GS', '10.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);
INSERT INTO `price` VALUES ('41', 'GDZJ', 'GCL', '4.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);
INSERT INTO `price` VALUES ('42', 'GDZJ', 'DZ', '3.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);

-- ----------------------------
-- Table structure for `problem_concernman`
-- ----------------------------
DROP TABLE IF EXISTS `problem_concernman`;
CREATE TABLE `problem_concernman` (
  `id` int(11) NOT NULL auto_increment,
  `concernmanid` int(11) NOT NULL,
  `problemid` int(11) NOT NULL,
  `concermanname` varchar(40) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of problem_concernman
-- ----------------------------
INSERT INTO `problem_concernman` VALUES ('253', '131', '172', '班长');
INSERT INTO `problem_concernman` VALUES ('254', '179', '172', '宋国强');
INSERT INTO `problem_concernman` VALUES ('255', '131', '173', '班长');
INSERT INTO `problem_concernman` VALUES ('256', '179', '173', '宋国强');
INSERT INTO `problem_concernman` VALUES ('257', '131', '174', '班长');
INSERT INTO `problem_concernman` VALUES ('258', '179', '174', '宋国强');
INSERT INTO `problem_concernman` VALUES ('259', '131', '175', '班长');
INSERT INTO `problem_concernman` VALUES ('260', '179', '175', '宋国强');
INSERT INTO `problem_concernman` VALUES ('261', '131', '176', '班长');
INSERT INTO `problem_concernman` VALUES ('262', '179', '176', '宋国强');
INSERT INTO `problem_concernman` VALUES ('263', '131', '177', '班长');
INSERT INTO `problem_concernman` VALUES ('264', '179', '177', '宋国强');
INSERT INTO `problem_concernman` VALUES ('265', '131', '178', '班长');
INSERT INTO `problem_concernman` VALUES ('266', '179', '178', '宋国强');
INSERT INTO `problem_concernman` VALUES ('267', '131', '179', '班长');
INSERT INTO `problem_concernman` VALUES ('268', '179', '179', '宋国强');
INSERT INTO `problem_concernman` VALUES ('269', '131', '180', '班长');
INSERT INTO `problem_concernman` VALUES ('270', '179', '180', '宋国强');
INSERT INTO `problem_concernman` VALUES ('271', '131', '181', '班长');
INSERT INTO `problem_concernman` VALUES ('272', '179', '181', '宋国强');
INSERT INTO `problem_concernman` VALUES ('273', '131', '182', '班长');
INSERT INTO `problem_concernman` VALUES ('274', '179', '182', '宋国强');
INSERT INTO `problem_concernman` VALUES ('275', '131', '183', '班长');
INSERT INTO `problem_concernman` VALUES ('276', '179', '183', '宋国强');
INSERT INTO `problem_concernman` VALUES ('277', '131', '184', '班长');
INSERT INTO `problem_concernman` VALUES ('278', '179', '184', '宋国强');
INSERT INTO `problem_concernman` VALUES ('279', '131', '185', '班长');
INSERT INTO `problem_concernman` VALUES ('280', '179', '185', '宋国强');
INSERT INTO `problem_concernman` VALUES ('281', '131', '186', '班长');
INSERT INTO `problem_concernman` VALUES ('282', '179', '186', '宋国强');
INSERT INTO `problem_concernman` VALUES ('283', '131', '187', '班长');
INSERT INTO `problem_concernman` VALUES ('284', '179', '187', '宋国强');
INSERT INTO `problem_concernman` VALUES ('285', '131', '188', '班长');
INSERT INTO `problem_concernman` VALUES ('286', '179', '188', '宋国强');
INSERT INTO `problem_concernman` VALUES ('287', '131', '189', '班长');
INSERT INTO `problem_concernman` VALUES ('288', '179', '189', '宋国强');
INSERT INTO `problem_concernman` VALUES ('289', '179', '190', '宋国强');
INSERT INTO `problem_concernman` VALUES ('290', '179', '191', '宋国强');
INSERT INTO `problem_concernman` VALUES ('291', '179', '192', '宋国强');
INSERT INTO `problem_concernman` VALUES ('292', '179', '193', '宋国强');
INSERT INTO `problem_concernman` VALUES ('293', '179', '194', '宋国强');
INSERT INTO `problem_concernman` VALUES ('294', '179', '195', '宋国强');
INSERT INTO `problem_concernman` VALUES ('295', '179', '196', '宋国强');
INSERT INTO `problem_concernman` VALUES ('296', '179', '197', '宋国强');
INSERT INTO `problem_concernman` VALUES ('297', '179', '198', '宋国强');
INSERT INTO `problem_concernman` VALUES ('298', '161', '198', '沈晖');
INSERT INTO `problem_concernman` VALUES ('299', '172', '198', '林冬总');
INSERT INTO `problem_concernman` VALUES ('300', '160', '198', '林冬');
INSERT INTO `problem_concernman` VALUES ('301', '179', '199', '宋国强');
INSERT INTO `problem_concernman` VALUES ('302', '179', '200', '宋国强');
INSERT INTO `problem_concernman` VALUES ('303', '161', '200', '沈晖');
INSERT INTO `problem_concernman` VALUES ('304', '172', '200', '林冬总');
INSERT INTO `problem_concernman` VALUES ('305', '160', '200', '林冬');
INSERT INTO `problem_concernman` VALUES ('306', '179', '201', '宋国强');
INSERT INTO `problem_concernman` VALUES ('307', '173', '201', '江庆华');
INSERT INTO `problem_concernman` VALUES ('308', '89', '201', '张孝林');
INSERT INTO `problem_concernman` VALUES ('309', '175', '201', '胡习萍');
INSERT INTO `problem_concernman` VALUES ('310', '176', '201', '樊阳');
INSERT INTO `problem_concernman` VALUES ('311', '174', '201', '胡菊芳');
INSERT INTO `problem_concernman` VALUES ('312', '179', '202', '宋国强');
INSERT INTO `problem_concernman` VALUES ('313', '173', '202', '江庆华');
INSERT INTO `problem_concernman` VALUES ('314', '89', '202', '张孝林');
INSERT INTO `problem_concernman` VALUES ('315', '175', '202', '胡习萍');
INSERT INTO `problem_concernman` VALUES ('316', '176', '202', '樊阳');
INSERT INTO `problem_concernman` VALUES ('317', '174', '202', '胡菊芳');
INSERT INTO `problem_concernman` VALUES ('318', '179', '203', '宋国强');
INSERT INTO `problem_concernman` VALUES ('319', '179', '204', '宋国强');
INSERT INTO `problem_concernman` VALUES ('320', '179', '205', '宋国强');
INSERT INTO `problem_concernman` VALUES ('321', '179', '206', '宋国强');
INSERT INTO `problem_concernman` VALUES ('322', '173', '206', '江庆华');
INSERT INTO `problem_concernman` VALUES ('323', '89', '206', '张孝林');
INSERT INTO `problem_concernman` VALUES ('324', '175', '206', '胡习萍');
INSERT INTO `problem_concernman` VALUES ('325', '176', '206', '樊阳');
INSERT INTO `problem_concernman` VALUES ('326', '174', '206', '胡菊芳');
INSERT INTO `problem_concernman` VALUES ('327', '179', '207', '宋国强');
INSERT INTO `problem_concernman` VALUES ('328', '179', '208', '宋国强');
INSERT INTO `problem_concernman` VALUES ('329', '179', '209', '宋国强');
INSERT INTO `problem_concernman` VALUES ('330', '173', '209', '江庆华');
INSERT INTO `problem_concernman` VALUES ('331', '89', '209', '张孝林');
INSERT INTO `problem_concernman` VALUES ('332', '175', '209', '胡习萍');
INSERT INTO `problem_concernman` VALUES ('333', '176', '209', '樊阳');
INSERT INTO `problem_concernman` VALUES ('334', '174', '209', '胡菊芳');

-- ----------------------------
-- Table structure for `problem_trigger_duration`
-- ----------------------------
DROP TABLE IF EXISTS `problem_trigger_duration`;
CREATE TABLE `problem_trigger_duration` (
  `id` int(11) NOT NULL auto_increment,
  `duration` int(11) default NULL,
  `name` varchar(40) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of problem_trigger_duration
-- ----------------------------
INSERT INTO `problem_trigger_duration` VALUES ('2', '2', 'change_solver');

-- ----------------------------
-- Table structure for `QRTZ_BLOB_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_CALENDARS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_CRON_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_FIRED_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) default NULL,
  `JOB_GROUP` varchar(200) default NULL,
  `IS_NONCONCURRENT` varchar(1) default NULL,
  `REQUESTS_RECOVERY` varchar(1) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_JOB_DETAILS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('scheduler', 'jobDetail', 'DEFAULT', null, 'com.easycms.quartz.service.MyQuartzJobBean', '1', '1', '1', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);

-- ----------------------------
-- Table structure for `QRTZ_LOCKS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('scheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `QRTZ_PAUSED_TRIGGER_GRPS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_SCHEDULER_STATE`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_SIMPLE_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '0133ed9c-9574-434e-ac6a-bc73c4baccd6', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '025ca52f-daea-4c14-a673-4da033ea5182', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '0d988f22-0da9-41bd-a50f-8555e4e82625', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '0ecbb7ec-2a40-4728-a1d0-f38a70bea6c1', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '1c3f1568-f63e-46b3-bbed-58f6a2f30677', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '1ea39392-cd74-4bdb-81e9-68e619c27b0a', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '1f5769cb-9c43-47ac-b355-8d277a0ef63e', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '2d9435f0-bf2f-4da8-8bdb-f99b811b9979', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '3958db06-3136-4ccc-912a-df6383616e99', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '3b46892d-93be-4193-add3-6a0b2a86a603', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '471bb1a5-2055-471e-ba69-6f004e75aca8', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '5608c519-67ba-4562-8985-4bc08e76c65d', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '5d1cea01-c614-4efd-be9e-8026fa5ab81e', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '5f0e8a83-8c2e-4d83-bb88-f45ce5e88091', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '604a06ec-87c9-4777-9bb2-d760f521cb1a', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '6150bae0-bb6c-4428-9ba7-27b0a29c6d8a', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '61ad0a3c-173c-4fcc-bc67-7680d13da951', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '6c26b6fc-535a-4e81-b239-5dc325fca0e4', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '6d412381-df59-4c2d-8716-952eb9ec11e5', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '750230f5-d5bb-420a-83ba-60ee46bd7e8d', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '82d6970e-ead1-4653-9ca5-f2f62763f65e', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '82ff2cf6-ca53-4d42-8a3b-bc383444618b', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '86566729-257e-4b4f-9c96-8935e807c508', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '895932c3-899d-4ba2-92a6-292a9689c5e7', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '8ac1b36b-7442-4222-babf-169d3c9a8ea5', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '8e641014-382e-4aac-9435-2f9444c09eeb', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', '906715fd-4737-4b91-8dac-6a7f0079855a', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'a91bc4ae-1b74-420d-834e-f73e9c91b551', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'abe5eff2-e5aa-4993-9d91-508fddee74bf', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'ac0e108b-8d8a-4222-a7f0-e8b0c8fc3aaf', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'b119b678-4bff-4c56-ba05-6bc8b8b90ca7', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'b1f0a09f-84b8-4626-ba48-3e4db66128ae', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'b4105b96-6ee7-4f41-93d6-0179ee6e4a3a', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'b74e727a-ada8-4b81-8aac-a9cf4ae33646', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'bc3fb13d-bd43-4988-87ac-9a243eb10f18', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'bd861bec-21aa-422d-a90d-1c75ebe4bb03', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'c8c4fbfd-4fce-45ec-b83f-947c34d51572', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'ca143266-d7c6-4c26-8809-61f0667ecb65', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd0956a27-d2fa-4955-8943-c9be39ea3a44', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd4848dec-43a2-4fc0-af6a-b00eeca5904f', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd6c113c9-2e30-462c-97ac-577de110f4da', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd84a5a49-2161-424f-843d-471ab0e90131', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd8563222-6af7-46d1-bbb4-4dccbd8cc908', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'd91d78a8-ffd3-409d-8a50-b7c46338fbc9', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e0089681-564a-4ecf-96ce-a5d9e77556fb', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e33a5437-525a-4fc5-b7a0-f531ff35bac1', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e6063cd4-4ddd-42ee-9183-a95b9bfd74b4', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e70eabf8-5869-4241-a2d6-f4b424df3184', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e755213c-4a88-4bc8-abd4-dd73bed6862e', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e7ea524a-9b8e-42f6-ac2d-753081f184c6', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'e9b37972-fe09-4ec9-85e7-63936d72c07c', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'edf02047-1491-4345-9e11-0fa8ff035bf8', 'DEFAULT', '0', '0', '0');
INSERT INTO `QRTZ_SIMPLE_TRIGGERS` VALUES ('scheduler', 'f4d9448b-f8d0-4ef1-b8e0-baa71576b13e', 'DEFAULT', '0', '0', '0');

-- ----------------------------
-- Table structure for `QRTZ_SIMPROP_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) default NULL,
  `STR_PROP_2` varchar(512) default NULL,
  `STR_PROP_3` varchar(512) default NULL,
  `INT_PROP_1` int(11) default NULL,
  `INT_PROP_2` int(11) default NULL,
  `LONG_PROP_1` bigint(20) default NULL,
  `LONG_PROP_2` bigint(20) default NULL,
  `DEC_PROP_1` decimal(13,4) default NULL,
  `DEC_PROP_2` decimal(13,4) default NULL,
  `BOOL_PROP_1` varchar(1) default NULL,
  `BOOL_PROP_2` varchar(1) default NULL,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for `QRTZ_TRIGGERS`
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) default NULL,
  `NEXT_FIRE_TIME` bigint(13) default NULL,
  `PREV_FIRE_TIME` bigint(13) default NULL,
  `PRIORITY` int(11) default NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) default NULL,
  `CALENDAR_NAME` varchar(200) default NULL,
  `MISFIRE_INSTR` smallint(2) default NULL,
  `JOB_DATA` blob,
  PRIMARY KEY  (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '0133ed9c-9574-434e-ac6a-bc73c4baccd6', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436594406137', '-1', '5', 'WAITING', 'SIMPLE', '1436594406137', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400086C65616465726964737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000008374000970726F626C656D69647371007E000C000000A8787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '025ca52f-daea-4c14-a673-4da033ea5182', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436762088776', '-1', '5', 'WAITING', 'SIMPLE', '1436762088776', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A44BF078000000007371007E001E000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF000071007E002071007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002071007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003571007E003A71007E003F71007E004471007E004971007E004E787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001B77080000014E763D4196787400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E762D447878000000007371007E001E000000857074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005471007E002F7070707371007E002A00FFFFFFFF000171007E006271007E005471007E0031707371007E000C000000077704000000077371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0054787371007E000C0000000777040000000771007E006571007E006A71007E006F71007E007471007E007971007E007E71007E0083787874000970726F626C656D69647371007E001E000000B0787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '0d988f22-0da9-41bd-a50f-8555e4e82625', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437318828932', '-1', '5', 'WAITING', 'SIMPLE', '1437318828932', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000CA7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '0ecbb7ec-2a40-4728-a1d0-f38a70bea6c1', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436763168647', '-1', '5', 'WAITING', 'SIMPLE', '1436763168647', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF000071007E002071007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002071007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003571007E003A71007E003F71007E004471007E004971007E004E787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E76437BF878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007371007E001E000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF000071007E005871007E005471007E002F7070707371007E002A00FFFFFFFF000171007E005871007E005471007E0031707371007E000C000000077704000000077371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0054787371007E000C0000000777040000000771007E006471007E006971007E006E71007E007371007E007871007E007D71007E0082787874000970726F626C656D69647371007E001E000000B6787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '1c3f1568-f63e-46b3-bbed-58f6a2f30677', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437641999243', '-1', '5', 'WAITING', 'SIMPLE', '1437641999243', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014EAA6270D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014EAA511B5878000000007371007E001C000000B77074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CF7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '1ea39392-cd74-4bdb-81e9-68e619c27b0a', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437298870108', '-1', '5', 'WAITING', 'SIMPLE', '1437298870108', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E962F73687800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E962AE75878000000007371007E001C000000A97074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C67874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '1f5769cb-9c43-47ac-b355-8d277a0ef63e', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437661131854', '-1', '5', 'WAITING', 'SIMPLE', '1437661131854', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014EABD28B387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014EABB556A878000000007371007E001C000000BD7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000027704000000027372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014EAAB18308780000000070740010373939303336316535306562333335627371007E001C0000009370707071007E0014787371007E000A0000000277040000000271007E003371007E0038787874000970726F626C656D69647371007E001C000000D17874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '2d9435f0-bf2f-4da8-8bdb-f99b811b9979', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437014111025', '-1', '5', 'WAITING', 'SIMPLE', '1437014111025', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000BA7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '3958db06-3136-4ccc-912a-df6383616e99', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436762954986', '-1', '5', 'WAITING', 'SIMPLE', '1436762954986', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF000071007E002071007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002071007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003571007E003A71007E003F71007E004471007E004971007E004E787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E76437BF878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007371007E001E000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF000071007E005871007E005471007E002F7070707371007E002A00FFFFFFFF000171007E005871007E005471007E0031707371007E000C000000077704000000077371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0054787371007E000C0000000777040000000771007E006471007E006971007E006E71007E007371007E007871007E007D71007E0082787874000970726F626C656D69647371007E001E000000B3787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '3b46892d-93be-4193-add3-6a0b2a86a603', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436763071651', '-1', '5', 'WAITING', 'SIMPLE', '1436763071651', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E76437BF878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007371007E001E000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000B4787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '471bb1a5-2055-471e-ba69-6f004e75aca8', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437013907121', '-1', '5', 'WAITING', 'SIMPLE', '1437013907121', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E853434E078000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E7A31E88078000000007371007E001C000000917074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000BA7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '5608c519-67ba-4562-8985-4bc08e76c65d', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437029078198', '-1', '5', 'WAITING', 'SIMPLE', '1437029078198', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E85FC8DF078000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E8569B43078000000007371007E001C000000987074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000BC7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '5d1cea01-c614-4efd-be9e-8026fa5ab81e', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436756431806', '-1', '5', 'WAITING', 'SIMPLE', '1436756431806', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A44BF078000000007371007E001C000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001977080000014E75E73F99787400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E75E32FA078000000007371007E001C000000817074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF00007371007E001C000000B371007E005271007E002D7070707371007E002800FFFFFFFF000171007E006071007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006371007E006871007E006D71007E007271007E007771007E007C71007E0081787874000970726F626C656D69647371007E001C000000AC7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '5f0e8a83-8c2e-4d83-bb88-f45ce5e88091', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437637080565', '-1', '5', 'WAITING', 'SIMPLE', '1437637080565', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000CE7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '604a06ec-87c9-4777-9bb2-d760f521cb1a', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437318234409', '-1', '5', 'WAITING', 'SIMPLE', '1437318234409', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000C97874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '6150bae0-bb6c-4428-9ba7-27b0a29c6d8a', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436506961405', '-1', '5', 'WAITING', 'SIMPLE', '1436506961405', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400066C656164657273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000B4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000D4C000B6C6173744C6F67696E497071007E000B4C000D6C6173744C6F67696E54696D6571007E000C4C000A6C6F67696E54696D657371007E000D4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000B4C000870617373776F726471007E000B4C00087265616C6E616D6571007E000B4C000A7265676973746572497071007E000B4C000C726567697374657254696D6571007E000C4C0005726F6C657371007E000E4C000A7573657244657669636571007E000E7872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000B5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000B4C000666696C74657271007E000B4C0009666F726D617474657271007E000B4C000466756E6371007E000B5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000B4C00047061676571007E000B4C000971756572795479706571007E000B4C0004726F777371007E000B4C0004736F727471007E000B4C0009737461727454696D6571007E000B787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001A0000000074000D3139322E3136382E322E3131397371007E001677080000014E57A44BF078000000007371007E001A000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001677080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E000E7872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000B4C001273657373696F6E466163746F72795575696471007E000B4C000E73746F726564536E617073686F7471007E0028787000FFFFFFFF000071007E001C71007E001274002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002600FFFFFFFF000171007E001C71007E0012740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E7573657244657669636570737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A657870000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000B4C0009637265617465644F6E71007E000C4C000664657669636571007E000B4C0008646576696365696471007E000B4C0002696471007E000D4C000672656D61726B71007E000B4C000975706461746564427971007E000B4C0009757064617465644F6E71007E000C4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E000F7070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014DCD488700780000000070740010646634666261646636616265323531367371007E001A0000001170707071007E00127371007E00307070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001A0000001270707071007E00127371007E00307070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001A0000001470707071007E00127371007E00307070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001A0000001970707071007E00127371007E00307070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001A0000002070707071007E00127371007E00307070707071007E001371007E001470707070707070740008436F72655F4150497371007E001677080000014E021B6290780000000070740010653039336264646362636535353534397371007E001A0000002570707071007E0012787371007E002E0000000677040000000671007E003271007E003771007E003C71007E004171007E004671007E004B7874000770726F626C656D73720025636F6D2E65617379636D732E68642E70726F626C656D2E646F6D61696E2E50726F626C656D8304E2CE69395332020016490007636F6E6669726D49000F63757272656E74736F6C7665726964490002696449000469734F6B4900056C6576656C49000B6D6574686F646D616E6964490009776F727374657069644C0009636F6E6365726D616E71007E000B4C000D636F6E6365726D616E6E616D6571007E000B4C00086372656174654F6E71007E000C4C000963726561746564427971007E000B4C000D63757272656E74736F6C76657271007E000B4C0008646573637269626571007E000B4C0006647261776E6F71007E000B4C000C7175657374696F6E6E616D6571007E000B4C000672656D61726B71007E000B4C000D726F6C6C696E67506C616E496471007E000D4C0009736F6C76656461746571007E000C4C000B736F6C76656D6574686F6471007E000B4C0008757064617465427971007E000B4C00087570646174654F6E71007E000C4C000677656C646E6F71007E000B78700000000000000083000000A70000000000000000000000000000000070707371007E001777080000014E6C2FF9D47874000331373871007E0023740003646464740015303730363044592D4A505330312D4A50442D3030337400076775616E7A6875707371007E001A0000019C707070707400024D317874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '61ad0a3c-173c-4fcc-bc67-7680d13da951', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437120160318', '-1', '5', 'WAITING', 'SIMPLE', '1437120160318', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000C37874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '6c26b6fc-535a-4e81-b239-5dc325fca0e4', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437399393226', '-1', '5', 'WAITING', 'SIMPLE', '1437399393226', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E9C250A307800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E976C96E878000000007371007E001C000000B47074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CC7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '6d412381-df59-4c2d-8716-952eb9ec11e5', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437298869841', '-1', '5', 'WAITING', 'SIMPLE', '1437298869841', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E962F73687800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E962AE75878000000007371007E001C000000A97074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C57874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '750230f5-d5bb-420a-83ba-60ee46bd7e8d', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437318169220', '-1', '5', 'WAITING', 'SIMPLE', '1437318169220', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E3131397372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E976246B078740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001B0000000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737871007E001877080000014E975C184078000000007371007E001B000000B17074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E002077080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF00007371007E001B000000B371007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E002D71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E002077080000014E97624648780000000070740010633435643634363166306437366636347371007E001B0000008C70707071007E0014787371007E000A0000000177040000000171007E0034787874000970726F626C656D69647371007E001B000000C97874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '82d6970e-ead1-4653-9ca5-f2f62763f65e', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437117392191', '-1', '5', 'WAITING', 'SIMPLE', '1437117392191', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B68DC307800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B67CAC078000000007371007E001C000000A07074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF00007371007E001C000000B371007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E002D71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003471007E003971007E003E71007E004371007E004871007E004D71007E0052787874000970726F626C656D69647371007E001C000000C27874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '82ff2cf6-ca53-4d42-8a3b-bc383444618b', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437115758741', '-1', '5', 'WAITING', 'SIMPLE', '1437115758741', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B2255807800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B1F6B6878000000007371007E001C0000009D7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000BF7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '86566729-257e-4b4f-9c96-8935e807c508', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437029123935', '-1', '5', 'WAITING', 'SIMPLE', '1437029123935', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E85FC8DF078000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E8569B43078000000007371007E001C000000987074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000BD7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '895932c3-899d-4ba2-92a6-292a9689c5e7', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436790663063', '-1', '5', 'WAITING', 'SIMPLE', '1436790663063', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E77F1EF4878000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E77F108D078000000007371007E001C0000008E7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000B97874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '8ac1b36b-7442-4222-babf-169d3c9a8ea5', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436619821944', '-1', '5', 'WAITING', 'SIMPLE', '1436619821944', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400066C6561646572737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000008374000770726F626C656D73720025636F6D2E65617379636D732E68642E70726F626C656D2E646F6D61696E2E50726F626C656D8304E2CE69395332020016490007636F6E6669726D49000F63757272656E74736F6C7665726964490002696449000469734F6B4900056C6576656C49000B6D6574686F646D616E6964490009776F727374657069644C0009636F6E6365726D616E7400124C6A6176612F6C616E672F537472696E673B4C000D636F6E6365726D616E6E616D6571007E000F4C00086372656174654F6E7400104C6A6176612F7574696C2F446174653B4C000963726561746564427971007E000F4C000D63757272656E74736F6C76657271007E000F4C0008646573637269626571007E000F4C0006647261776E6F71007E000F4C000C7175657374696F6E6E616D6571007E000F4C000672656D61726B71007E000F4C000D726F6C6C696E67506C616E49647400134C6A6176612F6C616E672F496E74656765723B4C0009736F6C76656461746571007E00104C000B736F6C76656D6574686F6471007E000F4C0008757064617465427971007E000F4C00087570646174654F6E71007E00104C000677656C646E6F71007E000F787000000000000000B3000000A90000000000000000000000000000000070740010E78FADE995BF7CE5AE8BE59BBDE5BCBA7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E72EA199A78740003313738740009E5AE8BE59BBDE5BCBA740003636363740015303730363044592D4A505330312D4A50442D303033740003676667707371007E000A000001DB707070707400034D31367874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '8e641014-382e-4aac-9435-2f9444c09eeb', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437665506122', '-1', '5', 'WAITING', 'SIMPLE', '1437665506122', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000D17874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', '906715fd-4737-4b91-8dac-6a7f0079855a', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437029354349', '-1', '5', 'WAITING', 'SIMPLE', '1437029354349', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000BC7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'a91bc4ae-1b74-420d-834e-f73e9c91b551', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437315648266', '-1', '5', 'WAITING', 'SIMPLE', '1437315648266', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E96CC53907800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E9643A74878000000007371007E001C000000AC7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C87874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'abe5eff2-e5aa-4993-9d91-508fddee74bf', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436765884577', '-1', '5', 'WAITING', 'SIMPLE', '1436765884577', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF00007371007E001C0000008371007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E002D71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003471007E003971007E003E71007E004371007E004871007E004D787371007E000C7070707071007E001571007E001670707070707070000074000F303A303A303A303A303A303A303A317371007E001877080000014E76437BF878000000007400007371007E001C000000B371007E001F74000F303A303A303A303A303A303A303A317371007E001877080000014E763D423878000000007371007E001C000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005771007E005371007E002E7070707371007E002800FFFFFFFF000171007E005771007E005371007E0030707371007E000A000000077704000000077371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0053787371007E000A0000000777040000000771007E006371007E006871007E006D71007E007271007E007771007E007C71007E0081787874000970726F626C656D69647371007E001C000000B77874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'ac0e108b-8d8a-4222-a7f0-e8b0c8fc3aaf', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436751925098', '-1', '5', 'WAITING', 'SIMPLE', '1436751925098', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A44BF078000000007371007E001C000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF00007371007E001C0000008371007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E002D71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003471007E003971007E003E71007E004371007E004871007E004D787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E75A2561078000000007400007371007E001C000000B371007E001F74000F303A303A303A303A303A303A303A317371007E001877080000014E759AC0A878000000007371007E001C0000007C7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005771007E005371007E002E7070707371007E002800FFFFFFFF000171007E005771007E005371007E0030707371007E000A000000077704000000077371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00537371007E00327070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0053787371007E000A0000000777040000000771007E006371007E006871007E006D71007E007271007E007771007E007C71007E0081787874000970726F626C656D69647371007E001C000000AB7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'b119b678-4bff-4c56-ba05-6bc8b8b90ca7', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436758716004', '-1', '5', 'WAITING', 'SIMPLE', '1436758716004', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A44BF078000000007371007E001E000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000D3139322E3136382E322E3131397371007E001A77080000014E75E882E878000000007400007371007E001E000000B371007E002174000D3139322E3136382E322E3131397371007E001A77080000014E75E73EB078000000007371007E001E000000827074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000AD787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'b1f0a09f-84b8-4626-ba48-3e4db66128ae', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437318737780', '-1', '5', 'WAITING', 'SIMPLE', '1437318737780', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E97640B687800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E9762464878000000007371007E001C000000B27074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CA7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'b4105b96-6ee7-4f41-93d6-0179ee6e4a3a', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437119178460', '-1', '5', 'WAITING', 'SIMPLE', '1437119178460', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B85E5C87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E8B7E738878000000007371007E001E000000A27074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF000071007E002071007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002071007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0016787371007E000C0000000777040000000771007E003571007E003A71007E003F71007E004471007E004971007E004E71007E0053787874000970726F626C656D69647371007E001E000000C3787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'b74e727a-ada8-4b81-8aac-a9cf4ae33646', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437137078439', '-1', '5', 'WAITING', 'SIMPLE', '1437137078439', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8C977CD87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B8B6BD878000000007371007E001C000000A47074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C47874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'bc3fb13d-bd43-4988-87ac-9a243eb10f18', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436762398360', '-1', '5', 'WAITING', 'SIMPLE', '1436762398360', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E762D447878000000007371007E001E000000857074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000B1787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'bd861bec-21aa-422d-a90d-1c75ebe4bb03', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437114559531', '-1', '5', 'WAITING', 'SIMPLE', '1437114559531', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B2255807800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B1F6B6878000000007371007E001C0000009D7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000BE7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'c8c4fbfd-4fce-45ec-b83f-947c34d51572', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437015134745', '-1', '5', 'WAITING', 'SIMPLE', '1437015134745', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001977080000014E855323A5787400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E85417F3878000000007371007E001C000000937074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF00007371007E001C000000B371007E005271007E002D7070707371007E002800FFFFFFFF000171007E006071007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006371007E006871007E006D71007E007271007E007771007E007C71007E0081787874000970726F626C656D69647371007E001C000000BB7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'ca143266-d7c6-4c26-8809-61f0667ecb65', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436760911203', '-1', '5', 'WAITING', 'SIMPLE', '1436760911203', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A44BF078000000007371007E001E000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E760BA31878000000007400007371007E001E000000B371007E002174000D3139322E3136382E322E3131397371007E001A77080000014E75E882E878000000007371007E001E000000837074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000AE787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd0956a27-d2fa-4955-8943-c9be39ea3a44', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437636840581', '-1', '5', 'WAITING', 'SIMPLE', '1437636840581', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014EAA511B587800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E9C3C205878000000007371007E001C000000B67074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CE7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd4848dec-43a2-4fc0-af6a-b00eeca5904f', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437115806576', '-1', '5', 'WAITING', 'SIMPLE', '1437115806576', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B2255807800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B1F6B6878000000007371007E001C0000009D7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C07874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd6c113c9-2e30-462c-97ac-577de110f4da', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436766570108', '-1', '5', 'WAITING', 'SIMPLE', '1436766570108', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A457A878000000007371007E001C000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E767FE0E878000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E7678EBA878000000007371007E001C000000887074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000B87874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd84a5a49-2161-424f-843d-471ab0e90131', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436761959289', '-1', '5', 'WAITING', 'SIMPLE', '1436761959289', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A44BF078000000007371007E001E000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF000071007E002071007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002071007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003571007E003A71007E003F71007E004471007E004971007E004E787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E762D447878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E760BA31878000000007371007E001E000000847074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF000071007E005871007E005471007E002F7070707371007E002A00FFFFFFFF000171007E005871007E005471007E0031707371007E000C000000077704000000077371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00547371007E00337070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0054787371007E000C0000000777040000000771007E006471007E006971007E006E71007E007371007E007871007E007D71007E0082787874000970726F626C656D69647371007E001E000000AF787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd8563222-6af7-46d1-bbb4-4dccbd8cc908', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436762896926', '-1', '5', 'WAITING', 'SIMPLE', '1436762896926', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E76437BF878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007371007E001E000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000B2787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'd91d78a8-ffd3-409d-8a50-b7c46338fbc9', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437657355070', '-1', '5', 'WAITING', 'SIMPLE', '1437657355070', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014EAB6AD0887800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014EAADCEC5078000000007371007E001C000000BA7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000027704000000027372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014EAAB18308780000000070740010373939303336316535306562333335627371007E001C0000009370707071007E0014787371007E000A0000000277040000000271007E003371007E0038787874000970726F626C656D69647371007E001C000000D07874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e0089681-564a-4ecf-96ce-a5d9e77556fb', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437399310411', '-1', '5', 'WAITING', 'SIMPLE', '1437399310411', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E9C250A307800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E976C96E878000000007371007E001C000000B47074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CB7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e33a5437-525a-4fc5-b7a0-f531ff35bac1', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437117370293', '-1', '5', 'WAITING', 'SIMPLE', '1437117370293', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E8B68DC307800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E8B67CAC078000000007371007E001C000000A07074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C17874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e6063cd4-4ddd-42ee-9183-a95b9bfd74b4', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437299454724', '-1', '5', 'WAITING', 'SIMPLE', '1437299454724', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000C67874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e70eabf8-5869-4241-a2d6-f4b424df3184', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436763072497', '-1', '5', 'WAITING', 'SIMPLE', '1436763072497', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C7665725365727669636574000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000F4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E00114C000B6C6173744C6F67696E497071007E000F4C000D6C6173744C6F67696E54696D6571007E00104C000A6C6F67696E54696D657371007E00114C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000F4C000870617373776F726471007E000F4C00087265616C6E616D6571007E000F4C000A7265676973746572497071007E000F4C000C726567697374657254696D6571007E00104C0005726F6C657371007E00124C000A7573657244657669636571007E00127872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000F5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000F4C000666696C74657271007E000F4C0009666F726D617474657271007E000F4C000466756E6371007E000F5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000F4C00047061676571007E000F4C000971756572795479706571007E000F4C0004726F777371007E000F4C0004736F727471007E000F4C0009737461727454696D6571007E000F787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000F303A303A303A303A303A303A303A31737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E764139D87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001E0000000074000D3139322E3136382E322E3131397371007E001A77080000014E57A457A878000000007371007E001E000000977074000862616E7A68616E677400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001A77080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00127872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000F4C001273657373696F6E466163746F72795575696471007E000F4C000E73746F726564536E617073686F7471007E002C787000FFFFFFFF00007371007E001E0000008371007E001674002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002A00FFFFFFFF000171007E002F71007E0016740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000C000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000F4C0009637265617465644F6E71007E00104C000664657669636571007E000F4C0008646576696365696471007E000F4C0002696471007E00114C000672656D61726B71007E000F4C000975706461746564427971007E000F4C0009757064617465644F6E71007E00104C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00137070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD488700780000000070740010646634666261646636616265323531367371007E001E0000001170707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001E0000001270707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001E0000001470707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001E0000001970707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001E0000002070707071007E00167371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E021B6290780000000070740010653039336264646362636535353534397371007E001E0000002570707071007E0016787371007E000C0000000677040000000671007E003671007E003B71007E004071007E004571007E004A71007E004F787371007E000E7070707071007E001771007E001870707070707070000074000F303A303A303A303A303A303A303A317371007E001A77080000014E76437BF878000000007400007371007E001E000000B371007E002174000F303A303A303A303A303A303A303A317371007E001A77080000014E763D423878000000007371007E001E000000867074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001A77080000014E2E64D16078000000007371007E002A00FFFFFFFF00007371007E001E000000B371007E005571007E00307070707371007E002A00FFFFFFFF000171007E006371007E005571007E0032707371007E000C000000077704000000077371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001E0000003470707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001E0000003570707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E301311E8780000000070740010633435643634363166306437366636347371007E001E0000003770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001E0000003C70707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001E0000007270707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001E0000007770707071007E00557371007E00347070707071007E001771007E001870707070707070740008436F72655F4150497371007E001A77080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001E0000007870707071007E0055787371007E000C0000000777040000000771007E006671007E006B71007E007071007E007571007E007A71007E007F71007E0084787874000970726F626C656D69647371007E001E000000B5787800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e755213c-4a88-4bc8-abd4-dd73bed6862e', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437315833124', '-1', '5', 'WAITING', 'SIMPLE', '1437315833124', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000C87874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e7ea524a-9b8e-42f6-ac2d-753081f184c6', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1436717473852', '-1', '5', 'WAITING', 'SIMPLE', '1436717473852', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000277040000000273720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000174000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E57A457A87800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000837371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E57A44BF078000000007371007E001C000000967074000962616E7A68616E67797400203539346166653565316638616338333832376361343334333466376461653161740006E78FADE995BF74000D3139322E3136382E322E3131397371007E001877080000014DCD25014078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000067704000000067372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD488700780000000070740010646634666261646636616265323531367371007E001C0000001170707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCD56042078000000007074000F3931396436653265636437343736367371007E001C0000001270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DCE397E80780000000070740010633863326263646538333030633533637371007E001C0000001470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DD38F6A48780000000070740010363764663333326631643237333036617371007E001C0000001970707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014DF80DD670780000000070740010336632313130643230663137396464647371007E001C0000002070707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E021B6290780000000070740010653039336264646362636535353534397371007E001C0000002570707071007E0014787371007E000A0000000677040000000671007E003371007E003871007E003D71007E004271007E004771007E004C787371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E732E96F078000000007400007371007E001C000000B371007E001F74000D3139322E3136382E322E3131397371007E001877080000014E732DAC9078000000007371007E001C000000787074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007371007E002800FFFFFFFF000071007E005671007E005271007E002D7070707371007E002800FFFFFFFF000171007E005671007E005271007E002F707371007E000A000000077704000000077371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00527371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F507800000000707400103263396365353237303165353165636471007E005970707071007E0052787371007E000A0000000777040000000771007E006271007E006771007E006C71007E007171007E007671007E007B71007E0080787874000970726F626C656D69647371007E001C000000AA7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'e9b37972-fe09-4ec9-85e7-63936d72c07c', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437299268879', '-1', '5', 'WAITING', 'SIMPLE', '1437299268879', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000377040000000373720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E331F9E387800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000A17371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E32A9261878000000007371007E001C00000005707400077368656E6875697400203063373363356139313337346463336131396636623766363862336236616632740006E6B288E6999674000D3139322E3136382E322E3131397371007E001877080000014E2477B5F078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000071007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E757365724465766963657070707371007E000C7070707071007E001571007E001670707070707070000070707400007371007E001C000000AC71007E001F707070707400086C696E646F6E677A7400203063373363356139313337346463336131396636623766363862336236616632740009E69E97E586ACE680BB74000D3139322E3136382E322E3131397371007E001877080000014E2E612BC878000000007371007E002800FFFFFFFF000071007E003271007E003071007E002D7070707371007E002800FFFFFFFF000071007E003271007E003071007E002F7070707371007E000C7070707071007E001571007E001670707070707070000074000D3139322E3136382E322E3131397371007E001877080000014E3391866078000000007400007371007E001C000000A071007E001F74000D3139322E3136382E322E3131397371007E001877080000014E33847E7078000000007371007E001C00000006707400076C696E646F6E677400203063373363356139313337346463336131396636623766363862336236616632740006E69E97E586AC74000D3139322E3136382E322E3131397371007E001877080000014E24778EE078000000007371007E002800FFFFFFFF000071007E003E71007E003A71007E002D7070707371007E002800FFFFFFFF000071007E003E71007E003A71007E002F7070707874000970726F626C656D69647371007E001C000000C67874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'edf02047-1491-4345-9e11-0fa8ff035bf8', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437472971107', '-1', '5', 'WAITING', 'SIMPLE', '1437472971107', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E9C3C20587800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E9C250A3078000000007371007E001C000000B57074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000017704000000017372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E97624648780000000070740010633435643634363166306437366636347371007E001C0000008C70707071007E0014787371007E000A0000000177040000000171007E0033787874000970726F626C656D69647371007E001C000000CD7874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);
INSERT INTO `QRTZ_TRIGGERS` VALUES ('scheduler', 'f4d9448b-f8d0-4ef1-b8e0-baa71576b13e', 'DEFAULT', 'jobDetail', 'DEFAULT', null, '1437315561718', '-1', '5', 'WAITING', 'SIMPLE', '1437315561718', '0', null, '-1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000A6A6F62446174614D61707371007E00053F4000000000000C770800000010000000027400076C656164657273737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A6578700000000177040000000173720027636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E5573657224EAFDE8DF4B6D230200125A000C64656661756C7441646D696E5A0005697344656C4C000E63757272656E744C6F67696E49707400124C6A6176612F6C616E672F537472696E673B4C001063757272656E744C6F67696E54696D657400104C6A6176612F7574696C2F446174653B4C0005656D61696C71007E000D4C000269647400134C6A6176612F6C616E672F496E74656765723B4C0012697344656661756C7441646D696E466C616771007E000F4C000B6C6173744C6F67696E497071007E000D4C000D6C6173744C6F67696E54696D6571007E000E4C000A6C6F67696E54696D657371007E000F4C00056D656E75737400104C6A6176612F7574696C2F4C6973743B4C00046E616D6571007E000D4C000870617373776F726471007E000D4C00087265616C6E616D6571007E000D4C000A7265676973746572497071007E000D4C000C726567697374657254696D6571007E000E4C0005726F6C657371007E00104C000A7573657244657669636571007E00107872001F636F6D2E65617379636D732E636F72652E666F726D2E4261736963466F726DDDE391474CEFE3EB02000D4C0006637573746F6D71007E000D5B000A646174654669656C64737400135B4C6A6176612F6C616E672F537472696E673B4C0007656E6454696D6571007E000D4C000666696C74657271007E000D4C0009666F726D617474657271007E000D4C000466756E6371007E000D5B00036964737400145B4C6A6176612F6C616E672F496E74656765723B4C00056F7264657271007E000D4C00047061676571007E000D4C000971756572795479706571007E000D4C0004726F777371007E000D4C0004736F727471007E000D4C0009737461727454696D6571007E000D787070707070740013797979792D4D4D2D64642048483A6D6D3A737374000070707070707070000074000D3139322E3136382E322E313139737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000014E96CC53907800000000740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000B37371007E001C0000000074000D3139322E3136382E322E3131397371007E001877080000014E9643A74878000000007371007E001C000000AC7074000C736F6E6767756F7169616E677400203063373363356139313337346463336131396636623766363862336236616632740009E5AE8BE59BBDE5BCBA74000D3139322E3136382E322E3131397371007E001877080000014E2E64D16078000000007372002F6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E50657273697374656E74426167464A645C192E1EC40200014C000362616771007E00107872003E6F72672E68696265726E6174652E636F6C6C656374696F6E2E696E7465726E616C2E416273747261637450657273697374656E74436F6C6C656374696F6E844A7E7706AE8D0B0200095A001B616C6C6F774C6F61644F7574736964655472616E73616374696F6E49000A63616368656453697A655A000564697274795A000B696E697469616C697A65644C00036B65797400164C6A6176612F696F2F53657269616C697A61626C653B4C00056F776E65727400124C6A6176612F6C616E672F4F626A6563743B4C0004726F6C6571007E000D4C001273657373696F6E466163746F72795575696471007E000D4C000E73746F726564536E617073686F7471007E002A787000FFFFFFFF000071007E001E71007E001474002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E726F6C65737070707371007E002800FFFFFFFF000171007E001E71007E0014740032636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E557365722E75736572446576696365707371007E000A000000077704000000077372002D636F6D2E65617379636D732E6D616E6167656D656E742E757365722E646F6D61696E2E55736572446576696365CA4FF725EA1F08200200094C000963726561746564427971007E000D4C0009637265617465644F6E71007E000E4C000664657669636571007E000D4C0008646576696365696471007E000D4C0002696471007E000F4C000672656D61726B71007E000D4C000975706461746564427971007E000D4C0009757064617465644F6E71007E000E4C0004757365727400294C636F6D2F65617379636D732F6D616E6167656D656E742F757365722F646F6D61696E2F557365723B7871007E00117070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A2D68780000000070740010373939303336316535306562333335627371007E001C0000003470707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E2F0A7B88780000000070740010653039336264646362636535353534397371007E001C0000003570707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E301311E8780000000070740010633435643634363166306437366636347371007E001C0000003770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E32A63FE8780000000070740010666434383930386163633837656565337371007E001C0000003C70707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E4A42AB00780000000070740010363764663333326631643237333036617371007E001C0000007270707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57961B2078000000007074000F3931396436653265636437343736367371007E001C0000007770707071007E00147371007E00317070707071007E001571007E001670707070707070740008436F72655F4150497371007E001877080000014E57AB8F50780000000070740010326339636535323730316535316563647371007E001C0000007870707071007E0014787371007E000A0000000777040000000771007E003371007E003871007E003D71007E004271007E004771007E004C71007E0051787874000970726F626C656D69647371007E001C000000C77874000A6A6F625365727669636574001E70726F626C656D4175746F4368616E6765536F6C766572536572766963657800);

-- ----------------------------
-- Table structure for `query_date`
-- ----------------------------
DROP TABLE IF EXISTS `query_date`;
CREATE TABLE `query_date` (
  `select_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`select_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of query_date
-- ----------------------------
INSERT INTO `query_date` VALUES ('2015-07-01 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-02 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-03 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-04 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-05 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-06 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-07 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-08 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-09 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-10 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-11 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-12 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-13 00:00:00');

-- ----------------------------
-- Table structure for `rd_source`
-- ----------------------------
DROP TABLE IF EXISTS `rd_source`;
CREATE TABLE `rd_source` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `source` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '来源',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_source
-- ----------------------------
INSERT INTO `rd_source` VALUES ('1', '党政网（省委宣传部）');
INSERT INTO `rd_source` VALUES ('2', '四川省机要局（涉密文件）');
INSERT INTO `rd_source` VALUES ('3', '上级文件（市委、市政府）');
INSERT INTO `rd_source` VALUES ('4', '下级文件（区级宣传部）');
INSERT INTO `rd_source` VALUES ('5', '平级文件（市教育局、市公安局……）');

-- ----------------------------
-- Table structure for `rd_type`
-- ----------------------------
DROP TABLE IF EXISTS `rd_type`;
CREATE TABLE `rd_type` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `type` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '类型',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_type
-- ----------------------------
INSERT INTO `rd_type` VALUES ('1', '通知');
INSERT INTO `rd_type` VALUES ('2', '公函');
INSERT INTO `rd_type` VALUES ('3', '通报');
INSERT INTO `rd_type` VALUES ('4', '公告');

-- ----------------------------
-- Table structure for `rd_workflow_file`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_file`;
CREATE TABLE `rd_workflow_file` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `filename` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '文件名',
  `file_number` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '文号',
  `create_date` datetime NOT NULL COMMENT '签发日期',
  `file_source_id` int(11) NOT NULL COMMENT '文件来源ID',
  `file_type_id` int(11) NOT NULL COMMENT '文件类型ID',
  `description` text collate utf8_unicode_ci COMMENT '任务描述',
  `rd_task_id` int(11) default NULL COMMENT '任务ID',
  PRIMARY KEY  (`id`),
  KEY `file_source_fk` (`file_source_id`),
  KEY `file_type_fk` (`file_type_id`),
  KEY `file_task_fk` (`rd_task_id`),
  CONSTRAINT `file_source_fk` FOREIGN KEY (`file_source_id`) REFERENCES `rd_source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `file_task_fk` FOREIGN KEY (`rd_task_id`) REFERENCES `rd_workflow_trace_task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `file_type_fk` FOREIGN KEY (`file_type_id`) REFERENCES `rd_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_file
-- ----------------------------

-- ----------------------------
-- Table structure for `rd_workflow_flow_zip`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_flow_zip`;
CREATE TABLE `rd_workflow_flow_zip` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `filename` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '文件名',
  `realname` varchar(255) collate utf8_unicode_ci NOT NULL COMMENT '真实文件名',
  `path` varchar(500) collate utf8_unicode_ci NOT NULL COMMENT '相对路径',
  `real_path` varchar(500) collate utf8_unicode_ci NOT NULL COMMENT '真实相对路径',
  `upload_date` datetime NOT NULL COMMENT '上传时间',
  `version` int(11) NOT NULL COMMENT '版本',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `deployment_id` varchar(100) collate utf8_unicode_ci default NULL COMMENT '发布ID',
  `deployment_name` varchar(255) collate utf8_unicode_ci default NULL COMMENT '发布名称',
  `deployment_date` datetime default NULL COMMENT '发布时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_flow_zip
-- ----------------------------
INSERT INTO `rd_workflow_flow_zip` VALUES ('8', 'problemProcess', 'problemProcess.zip', '/WEB-INF/workflow/problemProcess.zip', '/WEB-INF/workflow/problemProcess.zip', '2015-07-09 22:07:00', '1', '1', '1', 'problemProcess', '2015-07-09 22:07:09');

-- ----------------------------
-- Table structure for `rd_workflow_handled_task`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_handled_task`;
CREATE TABLE `rd_workflow_handled_task` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `task_id` int(11) NOT NULL COMMENT '跟踪任务ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_handled_task
-- ----------------------------

-- ----------------------------
-- Table structure for `rd_workflow_trace_task`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_trace_task`;
CREATE TABLE `rd_workflow_trace_task` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `rd_file_id` int(11) NOT NULL COMMENT '执行文件ID',
  `deploy_user_id` int(11) NOT NULL COMMENT '发布人ID',
  `assignee_id` int(11) default NULL COMMENT '当前处理人',
  `handle_date` datetime default NULL COMMENT '处理时间',
  `end_date` datetime default NULL COMMENT '结束时间',
  `start_date` datetime default NULL COMMENT '开始时间',
  `task_name` varchar(255) collate utf8_unicode_ci default NULL COMMENT '节点名称',
  `task_id` varchar(255) collate utf8_unicode_ci default NULL COMMENT '节点ID',
  `process_instance_id` varchar(255) collate utf8_unicode_ci default NULL COMMENT '流程定义ID',
  `finish_task_id` varchar(255) collate utf8_unicode_ci default NULL COMMENT '完成任务ID',
  `dispatch_user_id` int(11) default NULL COMMENT '分配人ID',
  `handle_message` text collate utf8_unicode_ci COMMENT '处理意见',
  `audit_message` text collate utf8_unicode_ci COMMENT '审核意见',
  `table_name` varchar(500) collate utf8_unicode_ci default NULL COMMENT '表单名',
  `status` int(11) default NULL COMMENT '任务状态',
  `duration` int(11) default NULL COMMENT '持续时间',
  `complete` int(11) default NULL COMMENT '任务完成状态',
  `department_id` int(11) default NULL COMMENT '部门ID',
  PRIMARY KEY  (`id`),
  KEY `rd_file_id` (`rd_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_trace_task
-- ----------------------------

-- ----------------------------
-- Table structure for `rolling_plan`
-- ----------------------------
DROP TABLE IF EXISTS `rolling_plan`;
CREATE TABLE `rolling_plan` (
  `autoid` int(11) NOT NULL auto_increment,
  `qualityplanno` varchar(50) collate utf8_unicode_ci default NULL,
  `rollingplanflag` varchar(10) collate utf8_unicode_ci default NULL,
  `weldlistno` varchar(50) collate utf8_unicode_ci default NULL,
  `drawno` varchar(50) collate utf8_unicode_ci default NULL,
  `rccm` varchar(10) collate utf8_unicode_ci default NULL,
  `areano` varchar(10) collate utf8_unicode_ci default NULL,
  `unitno` varchar(10) collate utf8_unicode_ci default NULL,
  `weldno` varchar(20) collate utf8_unicode_ci default NULL,
  `speciality` varchar(20) collate utf8_unicode_ci default NULL,
  `materialtype` varchar(20) collate utf8_unicode_ci default NULL,
  `workpoint` decimal(10,3) default NULL,
  `qualitynum` decimal(10,3) default NULL,
  `worktime` decimal(10,3) default NULL,
  `consteam` varchar(20) collate utf8_unicode_ci default NULL,
  `plandate` varchar(50) collate utf8_unicode_ci default NULL,
  `consdate` datetime default NULL,
  `isend` int(11) NOT NULL,
  `consendman` varchar(20) collate utf8_unicode_ci default NULL,
  `welder` varchar(20) collate utf8_unicode_ci default NULL,
  `assigndate` datetime default NULL,
  `doissuedate` datetime default NULL,
  `plan_finish_date` datetime default NULL,
  `enddate` datetime default NULL,
  `qcsign` int(11) NOT NULL,
  `qcman` varchar(20) collate utf8_unicode_ci default NULL,
  `qcdate` datetime default NULL,
  `remark` varchar(200) collate utf8_unicode_ci default NULL,
  `technology_ask` varchar(1000) collate utf8_unicode_ci default NULL,
  `quality_risk_ctl` varchar(1000) collate utf8_unicode_ci default NULL,
  `security_risk_ctl` varchar(1000) collate utf8_unicode_ci default NULL,
  `experience_feedback` varchar(1000) collate utf8_unicode_ci default NULL,
  `work_tool` varchar(1000) collate utf8_unicode_ci default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=691 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rolling_plan
-- ----------------------------
INSERT INTO `rolling_plan` VALUES ('680', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M1', 'GDHK', 'CS', '2.500', '0.325', null, '94', '2015-07-22至2015-07-22', '2015-03-15 00:00:00', '1', '132', null, '2015-07-22 11:40:03', null, '2015-07-22 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:08', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('681', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M10', 'GDHK', 'CS', '4.000', '0.780', null, '94', '2015-07-22至2015-07-22', '2015-03-15 00:00:00', '1', '114', null, '2015-07-22 14:42:03', null, '2015-07-22 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('682', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00399', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M11', 'GDHK', 'CS', '4.000', '10.000', null, '93', '2015-07-22至2015-07-23', '2015-03-15 00:00:00', '1', '106', null, '2015-07-22 15:15:19', null, '2015-07-22 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('683', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00029', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M12', 'GDHK', 'CS', '4.000', '20.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('684', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00034', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M13', 'GDHK', 'CS', '4.000', '0.400', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('685', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001A', 'GDZJ', 'CS', '0.000', '100.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('686', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001B', 'GDZJ', 'CS', '0.000', '120.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('687', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001C', 'GDZJ', 'CS', '0.000', '100.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('688', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001D', 'GDZJ', 'CS', '0.000', '130.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('689', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001E', 'GDZJ', 'CS', '0.000', '20.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('690', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001F', 'GDZJ', 'CS', '0.000', '200.000', null, null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);

-- ----------------------------
-- Table structure for `rolling_plan_question`
-- ----------------------------
DROP TABLE IF EXISTS `rolling_plan_question`;
CREATE TABLE `rolling_plan_question` (
  `autoid` int(11) NOT NULL auto_increment,
  `autoidup` int(11) NOT NULL,
  `weldno` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drawno` varchar(100) collate utf8_unicode_ci default NULL,
  `questiondes` varchar(10000) collate utf8_unicode_ci default NULL,
  `questionname` varchar(200) collate utf8_unicode_ci default NULL,
  `isok` int(11) NOT NULL,
  `queslevel` int(11) NOT NULL,
  `solvemethod` varchar(10000) collate utf8_unicode_ci default NULL,
  `remark` varchar(50) collate utf8_unicode_ci default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  `methodmanid` int(11) default NULL,
  `confirm` int(11) default NULL,
  `concernman` varchar(100) collate utf8_unicode_ci default NULL,
  `solvedate` datetime default NULL,
  `currentsolver` varchar(40) collate utf8_unicode_ci default NULL,
  `currentsolverid` int(11) default NULL,
  `concernmanname` varchar(100) collate utf8_unicode_ci default NULL,
  `rolling_plan_id` int(11) default NULL,
  `solver_id` int(11) default NULL,
  PRIMARY KEY  (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rolling_plan_question
-- ----------------------------
INSERT INTO `rolling_plan_question` VALUES ('208', '0', 'M10', '07060DY-JPS01-JPD-003', 'fgg', 'gvgg', '1', '0', '我能解决哈的', null, '2015-07-20 21:15:54', '178', '2015-07-20 21:16:47', '179', '179', '1', null, '2015-07-20 21:16:47', '宋国强', '179', '宋国强', '663', '297');
INSERT INTO `rolling_plan_question` VALUES ('209', '0', 'M10', '07060DY-JPS01-JPD-003', 'vvv', 'hbg', '0', '1', '我能解决', null, '2015-07-20 22:18:51', '178', '2015-07-20 23:31:45', '179', '179', '0', null, '2015-07-20 22:19:09', '江庆华', '179', '宋国强|江庆华|张孝林|胡习萍|樊阳|胡菊芳', '663', '299');

-- ----------------------------
-- Table structure for `solve_question_person`
-- ----------------------------
DROP TABLE IF EXISTS `solve_question_person`;
CREATE TABLE `solve_question_person` (
  `autoid` int(11) NOT NULL auto_increment,
  `problemid` int(11) NOT NULL,
  `stepno` int(11) NOT NULL,
  `okmanid` varchar(20) collate utf8_unicode_ci default NULL,
  `okdes` varchar(10000) collate utf8_unicode_ci default NULL,
  `okdate` datetime default NULL,
  `isok` int(11) NOT NULL,
  `ison` int(11) default NULL,
  `oklevel` int(11) default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  `okmanname` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of solve_question_person
-- ----------------------------
INSERT INTO `solve_question_person` VALUES ('297', '208', '0', '179', null, null, '0', '0', null, '2015-07-20 21:15:54', 'xiongzhanwen', null, null, '宋国强');
INSERT INTO `solve_question_person` VALUES ('298', '209', '0', '179', 'banzhang ', null, '0', '0', null, '2015-07-20 22:18:51', 'xiongzhanwen', null, null, '宋国强');
INSERT INTO `solve_question_person` VALUES ('299', '209', '0', '173', null, null, '0', '0', null, '2015-07-20 23:31:46', 'songguoqiang', null, null, '江庆华');

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `autoid` int(11) NOT NULL auto_increment,
  `userid` varchar(20) collate utf8_unicode_ci default NULL,
  `username` varchar(20) collate utf8_unicode_ci default NULL,
  `userpassword` varchar(20) collate utf8_unicode_ci default NULL,
  `userdepartment` varchar(20) collate utf8_unicode_ci default NULL,
  `userrole` varchar(50) collate utf8_unicode_ci default NULL,
  `upleader` varchar(20) collate utf8_unicode_ci default NULL,
  `rolelevel` int(11) default NULL,
  `online` int(11) default NULL,
  `isenable` int(11) default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`autoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of staff
-- ----------------------------

-- ----------------------------
-- Table structure for `Test`
-- ----------------------------
DROP TABLE IF EXISTS `Test`;
CREATE TABLE `Test` (
  `a` char(1) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Test
-- ----------------------------

-- ----------------------------
-- Table structure for `user_device`
-- ----------------------------
DROP TABLE IF EXISTS `user_device`;
CREATE TABLE `user_device` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `device` varchar(50) default NULL,
  `deviceid` varchar(80) NOT NULL,
  `remark` varchar(100) default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) default NULL,
  PRIMARY KEY  (`id`),
  KEY `ud_user_id_fk` (`userid`),
  CONSTRAINT `ud_user_id_fk` FOREIGN KEY (`userid`) REFERENCES `ec_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_device
-- ----------------------------
INSERT INTO `user_device` VALUES ('138', '178', null, 'c45d6461f0d76f64', null, '2015-07-16 22:52:33', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('139', '178', null, '919d6e2ecd74766', null, '2015-07-16 22:52:40', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('140', '179', null, 'c45d6461f0d76f64', null, '2015-07-16 23:01:17', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('141', '173', null, 'c45d6461f0d76f64', null, '2015-07-16 23:14:17', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('142', '170', null, 'c45d6461f0d76f64', null, '2015-07-17 10:13:00', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('143', '95', null, 'c45d6461f0d76f64', null, '2015-07-17 11:34:51', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('144', '95', null, '919d6e2ecd74766', null, '2015-07-17 21:00:18', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('145', '174', null, 'c45d6461f0d76f64', null, '2015-07-17 21:17:37', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('146', '178', null, '7990361e50eb335b', null, '2015-07-20 16:56:29', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('147', '179', null, '7990361e50eb335b', null, '2015-07-20 17:00:37', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('148', '170', null, '7990361e50eb335b', null, '2015-07-20 17:05:49', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('149', '168', null, '7990361e50eb335b', null, '2015-07-20 17:06:55', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('150', '179', null, '919d6e2ecd74766', null, '2015-07-20 23:18:21', 'Core_API', null, null);

-- ----------------------------
-- Table structure for `variable_set`
-- ----------------------------
DROP TABLE IF EXISTS `variable_set`;
CREATE TABLE `variable_set` (
  `id` int(11) NOT NULL auto_increment,
  `setkey` varchar(50) collate utf8_unicode_ci NOT NULL,
  `type` varchar(50) collate utf8_unicode_ci NOT NULL,
  `setvalue` varchar(500) collate utf8_unicode_ci default NULL,
  `status` varchar(10) collate utf8_unicode_ci default NULL,
  `createon` datetime default NULL,
  `createby` varchar(50) collate utf8_unicode_ci default NULL,
  `updateon` datetime default NULL,
  `updateby` varchar(50) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of variable_set
-- ----------------------------
INSERT INTO `variable_set` VALUES ('1', 'consteam', 'assigntype', '施工管理室', 'ACTIVE', null, null, '2015-07-22 10:45:00', 'admin');
INSERT INTO `variable_set` VALUES ('2', 'rollingPlan', 'planningtable', '双日滚动计划表', 'ACTIVE', null, null, '2015-06-01 17:28:40', 'admin');
INSERT INTO `variable_set` VALUES ('3', 'workStep', 'planningtable', '工序步骤表', 'ACTIVE', null, null, '2015-06-01 17:28:40', 'admin');
INSERT INTO `variable_set` VALUES ('4', 'GCL', 'price', '工程量', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('5', 'DZ', 'price', '点值', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('6', 'W', 'witnesstype', 'W级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('7', 'H', 'witnesstype', 'H级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('8', 'R', 'witnesstype', 'R级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('9', 'witnessTeam', 'witnesstype', '见证组组长', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('10', 'weldZJ', 'welddistinguish', 'GDZJ', 'ACTIVE', null, null, '2015-06-14 22:37:38', 'admin');
INSERT INTO `variable_set` VALUES ('11', 'weldHK', 'welddistinguish', 'GDHK', 'ACTIVE', null, null, '2015-06-14 22:37:38', 'admin');
INSERT INTO `variable_set` VALUES ('12', 'hysteresis', 'hysteresis', '3', 'ACTIVE', null, null, '2015-06-14 22:38:56', 'admin');
INSERT INTO `variable_set` VALUES ('13', 'changeSolver', 'problem', '3', 'ACTIVE', null, null, '2015-05-30 13:30:40', 'admin');
INSERT INTO `variable_set` VALUES ('14', 'GS', 'price', '工时', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('15', 'team', 'assigntype', '班长', 'ACTIVE', null, null, '2015-07-22 14:50:34', 'admin');
INSERT INTO `variable_set` VALUES ('16', 'endman', 'assigntype', '组长', 'ACTIVE', null, null, '2015-07-22 14:50:37', 'admin');
INSERT INTO `variable_set` VALUES ('17', 'witnessDepartment', 'witnesstype', '质检部', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');

-- ----------------------------
-- Table structure for `work_step`
-- ----------------------------
DROP TABLE IF EXISTS `work_step`;
CREATE TABLE `work_step` (
  `autoid` int(11) NOT NULL auto_increment,
  `autoidup` int(11) NOT NULL,
  `stepno` int(11) NOT NULL,
  `stepflag` varchar(10) collate utf8_unicode_ci default NULL,
  `stepname` varchar(50) collate utf8_unicode_ci default NULL,
  `operater` varchar(50) collate utf8_unicode_ci default NULL,
  `operatedesc` varchar(1000) collate utf8_unicode_ci default NULL,
  `operatedate` datetime default NULL,
  `noticeainfo` varchar(100) collate utf8_unicode_ci default NULL,
  `noticeresult` varchar(50) collate utf8_unicode_ci default NULL,
  `noticeresultdesc` varchar(100) collate utf8_unicode_ci default NULL,
  `noticeaqc1` char(1) collate utf8_unicode_ci default NULL,
  `witnesseraqc1` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdateaqc1` datetime default NULL,
  `noticeaqc2` char(1) collate utf8_unicode_ci default NULL,
  `witnesseraqc2` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdateaqc2` datetime default NULL,
  `noticeaqa` char(1) collate utf8_unicode_ci default NULL,
  `witnesseraqa` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdateaqa` datetime default NULL,
  `noticeb` varchar(50) collate utf8_unicode_ci default NULL,
  `witnesserb` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdateb` datetime default NULL,
  `noticec` varchar(50) collate utf8_unicode_ci default NULL,
  `witnesserc` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdatec` datetime default NULL,
  `noticed` varchar(50) collate utf8_unicode_ci default NULL,
  `witnesserd` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdated` datetime default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`autoid`),
  KEY `work_step_rp_fk` (`autoidup`),
  CONSTRAINT `ws_rp_id_fk` FOREIGN KEY (`autoidup`) REFERENCES `rolling_plan` (`autoid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3326 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of work_step
-- ----------------------------
INSERT INTO `work_step` VALUES ('3296', '680', '1', 'DONE', '坡口加工', 'fdsa', '合格', '2015-07-22 15:39:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', '2015-07-22 15:39:08', 'zuzhang');
INSERT INTO `work_step` VALUES ('3297', '680', '2', 'PREPARE', '内部清洁度检查', '脉动', '工序已完成。', '2015-07-23 16:46:23', null, null, null, 'W', '116', null, null, null, null, 'W', '118', null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', '2015-07-22 16:47:30', '132');
INSERT INTO `work_step` VALUES ('3298', '680', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, 'W', null, null, 'W', null, null, 'W', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3299', '680', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3300', '680', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, 'H', null, null, 'W', null, null, 'H', null, null, 'H', null, null, 'H', null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3301', '680', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3302', '681', '1', 'PREPARE', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3303', '681', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3304', '681', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, 'W', null, null, 'W', null, null, 'W', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3305', '681', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3306', '681', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, 'H', null, null, 'W', null, null, 'H', null, null, 'H', null, null, 'H', null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3307', '681', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3308', '682', '1', 'PREPARE', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3309', '682', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3310', '682', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, 'W', null, null, 'W', null, null, 'W', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3311', '682', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3312', '682', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, 'H', null, null, 'W', null, null, 'H', null, null, 'H', null, null, 'H', null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3313', '682', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3314', '683', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3315', '683', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3316', '683', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, 'W', null, null, 'W', null, null, 'W', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3317', '683', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3318', '683', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, 'H', null, null, 'W', null, null, 'H', null, null, 'H', null, null, 'H', null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3319', '683', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3320', '684', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3321', '684', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3322', '684', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, 'W', null, null, 'W', null, null, 'W', null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3323', '684', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3324', '684', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, 'H', null, null, 'W', null, null, 'H', null, null, 'H', null, null, 'H', null, null, '2015-07-22 10:40:09', 'admin', null, null);
INSERT INTO `work_step` VALUES ('3325', '684', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, '2015-07-22 10:40:09', 'admin', null, null);

-- ----------------------------
-- Table structure for `work_step_witness`
-- ----------------------------
DROP TABLE IF EXISTS `work_step_witness`;
CREATE TABLE `work_step_witness` (
  `id` int(11) NOT NULL auto_increment COMMENT '默认ID',
  `parent_id` int(11) default NULL,
  `stepno` int(11) NOT NULL COMMENT '工序步骤ID',
  `witness` int(11) default NULL COMMENT '见证人ID',
  `witnesser` varchar(50) collate utf8_unicode_ci default NULL,
  `witnessdes` varchar(10000) collate utf8_unicode_ci default NULL COMMENT '工序完成说明',
  `witnessdate` datetime default NULL,
  `noticeresultdesc` varchar(1000) collate utf8_unicode_ci default NULL,
  `witnessaddress` varchar(200) collate utf8_unicode_ci default NULL,
  `status` int(11) default NULL COMMENT '当前见证状态',
  `isok` int(11) default NULL COMMENT '当前见证结果',
  `notice_point` varchar(10) collate utf8_unicode_ci default NULL,
  `notice_type` char(1) collate utf8_unicode_ci default NULL,
  `triggerName` varchar(50) collate utf8_unicode_ci default NULL,
  `remark` varchar(50) collate utf8_unicode_ci default NULL COMMENT '备注',
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`),
  KEY `wsw_id_fk` (`parent_id`),
  KEY `wsw_ws_id_fk` (`stepno`),
  CONSTRAINT `wsw_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `work_step_witness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wsw_ws_id_fk` FOREIGN KEY (`stepno`) REFERENCES `work_step` (`autoid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of work_step_witness
-- ----------------------------
INSERT INTO `work_step_witness` VALUES ('300', null, '3297', '91', null, '带上纸牌。', '2015-07-23 16:47:07', null, '茶水间', '1', '0', null, null, null, null, '2015-07-22 16:47:30', '132', null, null);
INSERT INTO `work_step_witness` VALUES ('301', '300', '3297', null, '118', null, null, null, null, '0', '0', 'A_QA', 'W', null, null, '2015-07-22 17:34:23', '95', null, null);
INSERT INTO `work_step_witness` VALUES ('302', '300', '3297', null, '116', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-07-22 17:34:23', '95', null, null);

-- ----------------------------
-- Table structure for `worker_team`
-- ----------------------------
DROP TABLE IF EXISTS `worker_team`;
CREATE TABLE `worker_team` (
  `id` varchar(20) collate utf8_unicode_ci NOT NULL default '',
  `consteamid` varchar(20) collate utf8_unicode_ci default NULL,
  `worker` varchar(20) collate utf8_unicode_ci default NULL,
  `workname` varchar(20) collate utf8_unicode_ci default NULL,
  `created_on` datetime default NULL,
  `created_by` varchar(40) collate utf8_unicode_ci default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of worker_team
-- ----------------------------

-- ----------------------------
-- Table structure for `workstep_problem_file`
-- ----------------------------
DROP TABLE IF EXISTS `workstep_problem_file`;
CREATE TABLE `workstep_problem_file` (
  `id` int(11) NOT NULL auto_increment,
  `problemid` int(11) NOT NULL,
  `filepath` varchar(100) collate utf8_unicode_ci NOT NULL,
  `uploadtime` datetime NOT NULL,
  `filename` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of workstep_problem_file
-- ----------------------------

-- ----------------------------
-- View structure for `shigongliang`
-- ----------------------------
DROP VIEW IF EXISTS `shigongliang`;
CREATE ALGORITHM=UNDEFINED DEFINER=`easycms_v2`@`%` SQL SECURITY DEFINER VIEW `shigongliang` AS select count(0) AS `total`,`easycms_core_v1`.`rolling_plan`.`consendman` AS `consendman` from `rolling_plan` where (`easycms_core_v1`.`rolling_plan`.`isend` = 1) group by `easycms_core_v1`.`rolling_plan`.`consendman` ;

-- ----------------------------
-- View structure for `shigongwancheng`
-- ----------------------------
DROP VIEW IF EXISTS `shigongwancheng`;
CREATE ALGORITHM=UNDEFINED DEFINER=`easycms_v2`@`%` SQL SECURITY DEFINER VIEW `shigongwancheng` AS select count(0) AS `total`,`easycms_core_v1`.`rolling_plan`.`consendman` AS `consendman` from `rolling_plan` where (`easycms_core_v1`.`rolling_plan`.`isend` = 2) group by `easycms_core_v1`.`rolling_plan`.`consendman` ;
