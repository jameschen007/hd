package com.easycms.hd.conference.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "notification_feedback_mark")
@Data
public class NotificationFeedbackMark {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "notificationid")
	private Integer notificationid;
	@Column(name = "userid")
	private Integer userId;
	@Column(name = "mark_time")
	private Date markTime;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	
}
