package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "expire_notice_files")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class ExpireNoticeFiles extends BasicForm implements Serializable {
	private static final long serialVersionUID = -4647617585942876366L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "notice_id")
	private Integer noticeId;
	@Column(name = "no")
	private String no;
	@Column(name = "number")
	private Integer number;
	@Column(name = "version")
	private String version;
	@Column(name = "receiver")
	private String receiver;
	@Column(name = "status")
	private String status;
	@Column(name = "filename")
	private String filename;
	@Column(name = "filepath")
	private String filepath;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private String createBy;
}
