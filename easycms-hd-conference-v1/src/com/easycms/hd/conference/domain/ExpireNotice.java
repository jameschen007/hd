package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "expire_notice")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class ExpireNotice extends BasicForm implements Serializable {
	private static final long serialVersionUID = -6730329633126063014L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "title")
	private String title;
	@Column(name = "no")
	private String no;
	@Column(name = "content")
	private String content;
	@Column(name = "department")
	private String department;
	@Column(name = "status")
	private String status;
	@Column(name = "approvel_time")
	private Date approvelTime;
	@Column(name = "write_time")
	private Date writeTime;
	@Column(name = "publish_time")
	private Date publishTime;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private String createBy;
	
	@JsonIgnore
	@ManyToMany(targetEntity=com.easycms.management.user.domain.User.class,fetch=FetchType.EAGER,cascade={CascadeType.REMOVE, CascadeType.REFRESH})
	@JoinTable(
			name="expire_notice_user",
			joinColumns=@JoinColumn(name="notice_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="user_id",referencedColumnName="id")
			)
	private List<User> participants;
	
	@Transient
	private List<ExpireNoticeFiles> expireNoticeFiles;
}
