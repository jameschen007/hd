package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * 借阅人
 * @author ul-webdev
 *
 */
@Entity
@Table(name = "expire_notice_reader")
@Data
@EqualsAndHashCode(callSuper=false)
public class ExpireNoticeReader implements Serializable {
	private static final long serialVersionUID = -4647617585942876366L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "canc_id")
	private String cancId;// 文件id
	@Column(name = "bor_num")
	private String borNum;// 份数
	@Column(name = "borrower")
	private String borrower;// 文件使用人
	@Column(name = "loginname")
	private String loginname;// 文件借阅人
	@Column(name = "login_id")
	private String loginId;// 文件借阅人id
	@Column(name = "org_id")
	private String orgId;// 借阅人所属部门id
	@Column(name = "org_name")
	private String orgName;// 借阅人所属部门
	@Column(name = "status")
	private String status;// 状态　N未还 Y已还

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "file_id", nullable = false)
	private ExpireNoticeFile file;
}
