package com.easycms.hd.conference.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.conference.dao.EnpowerExpireNoticeDao;
import com.easycms.hd.conference.dao.ExpireNoticeDao;
import com.easycms.hd.conference.domain.ConferenceReceive;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.mybatis.dao.ExpireNoticeMybatisDao;
import com.easycms.hd.conference.service.ExpireNoticeService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;

@Service("expireNoticeService")
public class ExpireNoticeServiceImpl implements ExpireNoticeService {
	
	private static Logger logger = Logger.getLogger(ExpireNoticeServiceImpl.class);

	@Autowired
	private ExpireNoticeDao expireNoticeDao;
	
	@Autowired
	private ExpireNoticeMybatisDao expireNoticeMybatisDao;
	@Autowired
	private EnpowerExpireNoticeDao enpowerExpireNoticeDao;

	@Override
	public ExpireNotice add(ExpireNotice expireNotice) {
		return expireNoticeDao.save(expireNotice);
	}

	@Override
	public Page<ExpireNotice> findByPage(Integer userId, Page<ExpireNotice> page) {
		ConferenceReceive conferenceReceive = new ConferenceReceive();
		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		conferenceReceive.setPageSize(page.getPagesize());
		conferenceReceive.setUserId(userId);

		int count = 0;
		List<Integer> result = null;
		result = expireNoticeMybatisDao.findExpireNoticeReceive(conferenceReceive);
		count = expireNoticeMybatisDao.findExpireNoticeReceiveCount(conferenceReceive);
		
		if (null != result && !result.isEmpty()) {
			List<ExpireNotice> expireNotices = result.stream().map(id -> {
				ExpireNotice expireNotice = expireNoticeDao.findById(id);
				return expireNotice;
			}).collect(Collectors.toList());
			
			page.execute(count, page.getPageNum(), expireNotices);
		}
		
		return page;
	}
	

	public Page<EnpowerExpireNotice> findByPage(EnpowerExpireNotice condition,Map<String,Object> additionalCondition, Page<EnpowerExpireNotice> page){

		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		logger.debug("==> Find all user by condition.");
		org.springframework.data.domain.Page<EnpowerExpireNotice> springPage = null;
		springPage = enpowerExpireNoticeDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	
	}

	@Override
	public ExpireNotice findById(Integer id) {
		return expireNoticeDao.findById(id);
	}
}
