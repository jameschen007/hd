package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ExpireNoticeFilesDao;
import com.easycms.hd.conference.domain.ExpireNoticeFiles;
import com.easycms.hd.conference.service.ExpireNoticeFilesService;

@Service("expireNoticeFilesService")
public class ExpireNoticeFilesServiceImpl implements ExpireNoticeFilesService {

	@Autowired
	private ExpireNoticeFilesDao expireNoticeFilesDao;

	@Override
	public List<ExpireNoticeFiles> findByNoticeId(Integer noticeId) {
		return expireNoticeFilesDao.findByNoticeId(noticeId);
	}

	@Override
	public ExpireNoticeFiles add(ExpireNoticeFiles expireNoticeFiles) {
		return expireNoticeFilesDao.save(expireNoticeFiles);
	}

}
