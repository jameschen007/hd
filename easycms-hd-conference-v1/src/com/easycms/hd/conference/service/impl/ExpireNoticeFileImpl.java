package com.easycms.hd.conference.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ExpireNoticeFileDao;
import com.easycms.hd.conference.domain.ExpireNoticeFile;

@Service("expireNoticeFileImpl")
public class ExpireNoticeFileImpl{

	@Autowired
	private ExpireNoticeFileDao ExpireNoticeFileDao;
	
	public ExpireNoticeFile add(ExpireNoticeFile expireNotice) {
		return ExpireNoticeFileDao.save(expireNotice);
	}
	public ExpireNoticeFile findById(Integer id) {
		return ExpireNoticeFileDao.findById(id);
	}
}
