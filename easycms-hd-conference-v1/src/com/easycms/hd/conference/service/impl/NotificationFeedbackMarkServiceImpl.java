package com.easycms.hd.conference.service.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.NotificationFeedbackMarkDao;
import com.easycms.hd.conference.domain.NotificationFeedbackMark;
import com.easycms.hd.conference.service.NotificationFeedbackMarkService;

@Service("notificationFeedbackMarkService")
public class NotificationFeedbackMarkServiceImpl implements NotificationFeedbackMarkService{

	@Autowired
	private NotificationFeedbackMarkDao notificationFeedbackMarkDao;
	
	@Override
	public NotificationFeedbackMark add(NotificationFeedbackMark mark) {
		return notificationFeedbackMarkDao.save(mark);
	}
	@Override
	public NotificationFeedbackMark findByNotificationIdAndUserId(Integer notificationId, Integer userId) {
		return notificationFeedbackMarkDao.findByNotificationIdAndUserId(notificationId, userId);
	}
	
	@Override
	public int markRead(Integer markId, Date markTime) {
		return notificationFeedbackMarkDao.markRead(markId, markTime);
	}
	@Override
	public boolean batchRemove(Integer[] ids) {
		return notificationFeedbackMarkDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveNotification(Integer[] ids) {
		return notificationFeedbackMarkDao.batchRemoveNotification(ids) > 0;
	}

}
