package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.NotificationConfirmDao;
import com.easycms.hd.conference.domain.NotificationConfirm;
import com.easycms.hd.conference.service.NotificationConfirmService;

@Service("notificationConfirmService")
public class NotificationConfirmServiceImpl implements NotificationConfirmService{

    @Autowired
    private NotificationConfirmDao notificationConfirmDao;
    
    @Override
    public NotificationConfirm add(NotificationConfirm confirm) {
        return notificationConfirmDao.save(confirm);
    }
    
    @Override
    public List<NotificationConfirm> findByNotificationId(Integer id, Integer userid) {
        return notificationConfirmDao.findByNotificationId(id, userid);
    }

}
