package com.easycms.hd.conference.service;

import java.util.Map;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.conference.domain.ExpireNotice;

public interface ExpireNoticeService {
	ExpireNotice add(ExpireNotice expireNotice);

	Page<ExpireNotice> findByPage(Integer userId, Page<ExpireNotice> page);
	public Page<EnpowerExpireNotice> findByPage(EnpowerExpireNotice condition,Map<String,Object> additionalCondition, Page<EnpowerExpireNotice> page);

	ExpireNotice findById(Integer id);
}
