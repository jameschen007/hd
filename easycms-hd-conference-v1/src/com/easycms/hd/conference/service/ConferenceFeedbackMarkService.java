package com.easycms.hd.conference.service;

import java.util.Date;

import com.easycms.hd.conference.domain.ConferenceFeedbackMark;

public interface ConferenceFeedbackMarkService {

	ConferenceFeedbackMark add(ConferenceFeedbackMark mark);

	ConferenceFeedbackMark findByConferenceIdAndUserId(Integer conferenceId, Integer userId);

	int markRead(Integer markId, Date markTime);
	
	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveConference(Integer[] ids);
}
