package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.NotificationFeedback;

public interface NotificationFeedbackService {
	NotificationFeedback add(NotificationFeedback feedback);
	
	List<NotificationFeedback> findByNotificationId(Integer id);
	
	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveNotification(Integer[] ids);
}
