package com.easycms.hd.conference.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.domain.NotificationFile;
import com.easycms.hd.conference.service.NotificationFileService;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-11-09 
 */
@Slf4j
public class NotificationDirective extends BaseDirective<Notification>  {
	
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private NotificationFileService notificationFileService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Notification field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			Notification notification = notificationService.findById(id);
			List<NotificationFile> notificationFiles = notificationFileService.findFilesByNotificationId(notification.getId());
			if (null != notificationFiles) {
				notification.setNotificationFiles(notificationFiles);
			}
			return notification;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<Notification> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Notification> page,
			Map<String, Object> envParams) {
		User user =  ContextUtil.getCurrentLoginUser();
		String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
		Notification condition = null;
		
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Notification.class, filter,"yyyy-MM-dd");
		}else{
			condition = new Notification();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		
		if (!CommonUtility.isNonEmpty(type)) {
			type = ConferenceApiRequestType.SEND.name();
		}

		if (type.equals(ConferenceApiRequestType.DRAFT.name())) {
			page = notificationService.findByTypeAndCreater(type, user.getId(), page);
		} else if (type.equals(ConferenceApiRequestType.SEND.name())) {
			page = notificationService.findNotificationSend(user.getId(), page);
		} else if (type.equals(ConferenceApiRequestType.RECEIVE.name())){
			page = notificationService.findNotificationReceive(user.getId(), page);
		}
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<Notification> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
