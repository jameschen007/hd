package com.easycms.hd.conference.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.domain.ConferenceFile;
import com.easycms.hd.conference.service.ConferenceFileService;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-11-09 
 */
@Slf4j
public class ConferenceDirective extends BaseDirective<Conference>  {
	
	@Autowired
	private ConferenceService conferenceService;
	@Autowired
	private ConferenceFileService conferenceFileService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Conference field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			Conference conference = conferenceService.findById(id);
			if (null != conference) {
				List<ConferenceFile> conferenceFiles = conferenceFileService.findFilesByConferenceId(conference.getId());
				if (null != conferenceFiles) {
					conference.setConferenceFiles(conferenceFiles);
				}
			}
			return conference;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<Conference> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Conference> page,
			Map<String, Object> envParams) {
		User user =  ContextUtil.getCurrentLoginUser();
		String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
		Conference condition = null;
		
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Conference.class, filter,"yyyy-MM-dd");
		}else{
			condition = new Conference();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		if (!CommonUtility.isNonEmpty(type)) {
			type = ConferenceApiRequestType.SEND.name();
		}
		
		if (type.equals(ConferenceApiRequestType.DRAFT.name())) {
			page = conferenceService.findByTypeAndCreater(type, user.getId(), page);
		} else if (type.equals(ConferenceApiRequestType.SEND.name())) {
			page = conferenceService.findConferenceSend(user.getId(), page);
		} else if (type.equals(ConferenceApiRequestType.RECEIVE.name())) {
			page = conferenceService.findConferenceReceive(user.getId(), page);
		}
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<Conference> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
