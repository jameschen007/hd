package com.easycms.hd.conference.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.domain.ExpireNoticeFiles;
import com.easycms.hd.conference.service.ExpireNoticeFilesService;
import com.easycms.hd.conference.service.ExpireNoticeService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-11-09 
 */
@Slf4j
public class ExpireNoticeDirective extends BaseDirective<ExpireNotice>  {
	
	@Autowired
	private ExpireNoticeService expireNoticeService;
	@Autowired
	private ExpireNoticeFilesService expireNoticeFilesService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected ExpireNotice field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			List<ExpireNoticeFiles> expireNoticeFiles = expireNoticeFilesService.findByNoticeId(id);
			
			ExpireNotice expireNotice = expireNoticeService.findById(id);
			expireNotice.setExpireNoticeFiles(expireNoticeFiles);
			return expireNotice;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExpireNotice> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<ExpireNotice> page,
			Map<String, Object> envParams) {
		User user =  ContextUtil.getCurrentLoginUser();
		page = expireNoticeService.findByPage(user.getId(), page);
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExpireNotice> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
