package com.easycms.hd.conference.mybatis.dao;

import java.util.List;

import com.easycms.hd.conference.domain.ConferenceReceive;

public interface ExpireNoticeMybatisDao {
//ExpireNotice
	List<Integer> findExpireNoticeReceive(ConferenceReceive conference);
	Integer findExpireNoticeReceiveCount(ConferenceReceive conference);
}
