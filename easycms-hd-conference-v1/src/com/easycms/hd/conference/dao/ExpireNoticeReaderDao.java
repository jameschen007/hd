package com.easycms.hd.conference.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.domain.ExpireNoticeReader;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExpireNoticeReaderDao extends Repository<ExpireNoticeReader,Integer>,BasicDao<ExpireNoticeReader>{
	@Query("FROM ExpireNoticeReader")
	Page<ExpireNotice> findByPage(Pageable pageable);
	

	@Modifying
	@Query("UPDATE ExpireNoticeReader u SET u.status = ?1,u.loginname=null,u.borrower=null WHERE u.cancId=?2 and u.loginId=?3")
	int updateById(String status,String enpowerId,String enpLoginId);
	@Modifying
	@Query("UPDATE ExpireNoticeReader u SET u.borNum=?1 WHERE u.cancId=?2 and u.loginId=?3")
	int updateBorNumById(String borNum,String enpowerId,String enpLoginId);
	
}
