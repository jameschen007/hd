package com.easycms.hd.conference.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.Conference;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ConferenceDao extends Repository<Conference,Integer>,BasicDao<Conference>{
	@Query("FROM Conference c WHERE c.type = ?1 and c.createBy = ?2 order by c.createOn desc")
	Page<Conference> findByTypeAndCreater(String type, Integer creater, Pageable pageable);

	@Modifying
	@Query("DELETE FROM Conference c WHERE c.id IN (?1)")
	int deleteByIds(Integer[] ids);
	@Modifying
	@Query("update Conference n set status=?2 WHERE n.id IN (?1)")
	int updateById(Integer id,String statu);
}
