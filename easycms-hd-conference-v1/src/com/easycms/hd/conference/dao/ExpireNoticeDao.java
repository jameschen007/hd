package com.easycms.hd.conference.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ExpireNotice;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExpireNoticeDao extends Repository<ExpireNotice,Integer>,BasicDao<ExpireNotice>{
	@Query("FROM ExpireNotice en ORDER BY en.createOn desc")
	Page<ExpireNotice> findByPage(Pageable pageable);
}
