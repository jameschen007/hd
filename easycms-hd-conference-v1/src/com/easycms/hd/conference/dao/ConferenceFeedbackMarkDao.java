package com.easycms.hd.conference.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ConferenceFeedbackMark;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ConferenceFeedbackMarkDao extends Repository<ConferenceFeedbackMark,Integer>,BasicDao<ConferenceFeedbackMark>{
	@Modifying
	@Query("UPDATE ConferenceFeedbackMark cfm SET cfm.markTime = ?2 where cfm.id = ?1") 
	int markRead(Integer markId, Date markTime);

	@Query("FROM ConferenceFeedbackMark cfm where cfm.conferenceid = ?1 AND cfm.userId = ?2")
	ConferenceFeedbackMark findByConferenceIdAndUserId(Integer conferenceId, Integer userId);

	@Modifying
	@Query("DELETE FROM ConferenceFeedbackMark cfm where cfm.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM ConferenceFeedbackMark cfm where cfm.conferenceid IN (?1)")
	int batchRemoveConference(Integer[] ids);
}
