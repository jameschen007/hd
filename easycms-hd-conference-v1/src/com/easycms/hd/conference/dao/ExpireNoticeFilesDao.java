package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ExpireNoticeFiles;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExpireNoticeFilesDao extends Repository<ExpireNoticeFiles,Integer>,BasicDao<ExpireNoticeFiles>{
	@Query("From ExpireNoticeFiles enf where enf.noticeId = ?1")
	List<ExpireNoticeFiles> findByNoticeId(Integer id);
}
