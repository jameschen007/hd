package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ConferenceFile;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ConferenceFileDao extends Repository<ConferenceFile,Integer>,BasicDao<ConferenceFile>{
	@Query("From ConferenceFile wf WHERE wf.conferenceid = ?1")
	List<ConferenceFile> findByConferenceId(Integer id);

	@Modifying
	@Query("DELETE FROM ConferenceFile wf WHERE wf.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM ConferenceFile wf WHERE wf.conferenceid IN (?1)")
	int batchRemoveConference(Integer[] ids);

	@Query("From ConferenceFile wf WHERE wf.conferenceid IN (?1)")
	List<ConferenceFile> findFilesByConferenceIds(Integer[] ids);

	@Modifying
	@Query("UPDATE ConferenceFile wf SET wf.conferenceid = ?2 WHERE wf.id IN (?1)")
	int batchUpdateConference(Integer[] ids, Integer conferenceId);
}
