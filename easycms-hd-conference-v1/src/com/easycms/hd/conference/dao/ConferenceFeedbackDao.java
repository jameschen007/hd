package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ConferenceFeedback;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ConferenceFeedbackDao extends Repository<ConferenceFeedback,Integer>,BasicDao<ConferenceFeedback>{
	@Query("From ConferenceFeedback cf where cf.conferenceid = ?1 order by cf.createOn")
	List<ConferenceFeedback> findByConferenceId(Integer id);

	@Modifying
	@Query("DELETE FROM ConferenceFeedback cf where cf.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM ConferenceFeedback cf where cf.conferenceid IN (?1)")
	int batchRemoveConference(Integer[] ids);
}
