package com.easycms.hd.conference.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.request.NotificationRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.NotificationApiService;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.domain.NotificationFile;
import com.easycms.hd.conference.enums.ConferenceCategory;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.hd.conference.enums.ConferenceType;
import com.easycms.hd.conference.service.NotificationFileService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/baseservice/notification")
@Slf4j
public class NotificationController {
	
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private NotificationApiService notificationApiService;
	@Autowired
	private NotificationFileService notificationFileService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_notification_path']}")
	private String fileNotificationPath;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") Notification form) throws Exception {
		String func = form.getFunc();
		log.debug("[func] = " + func + " : " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/notification/list_notification");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") Notification form) {
		log.debug("==> Show notification data list.");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/notification/data/data_json_notification");
		return returnString;
	}

	/**
	 * 新增信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		Department department = departmentService.findByName("K2/K3核电项目部");
		if (null != department) {
			List<Department> departments = department.getChildren();
			if (null != departments && !departments.isEmpty()) {
				for (Department d : departments) {
					List<User> users = new ArrayList<User>();
					users.addAll(departmentUser(d));
					d.setUsers(users);
				}
			}
			
			request.setAttribute("department", departments);
		}
		
		request.setAttribute("alarmTime", ConferenceAlarmTypsEnums.getList());
		request.setAttribute("category", ConferenceCategory.getList());
		request.setAttribute("type", ConferenceType.getList());
		request.setAttribute("currentTime", new Date());
		
		String returnString = ForwardUtility.forwardAdminView("/notification/page_notification_add");
		return returnString;
	}
	
	private List<User> departmentUser(Department department){
		if (null != department) {
			List<User> users = new ArrayList<User>();
			users.addAll(department.getUsers());
			List<Department> departments = department.getChildren();
			if (null != departments && !departments.isEmpty()) {
				for (Department d : departments) {
					users.addAll(departmentUser(d));
				}
			}
			
			return users;
		}
		return null;
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<String> add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") NotificationRequestForm form,
			BindingResult errors) {
		log.debug("==> Start save new notification.");
		JsonResult<String> result = new JsonResult<String>();
		log.info(CommonUtility.toJson(form));

		if (null != form) {
			HttpSession session = request.getSession();
			User loginUser = (User) session.getAttribute("user");
			
			JsonResult<Integer> notificationResult = notificationApiService.insert(form, loginUser, ConferenceSource.WEB);
			if (null != notificationResult && null != notificationResult.getResponseResult()) {
				if (null != form.getFileIds() && !form.getFileIds().isEmpty()) {
					notificationFileService.batchUpdateNotification(form.getFileIds().toArray(new Integer[] {}), notificationResult.getResponseResult());
				}
			} else {
				result.setCode("-1000");
		    	result.setMessage(notificationResult.getMessage());
		    	return result;
			}
			log.debug("==> End save new notification.");
			result.setCode("1000");
	    	result.setMessage("保存通知成功。");
	    } else {
	    	result.setCode("-1000");
	    	result.setMessage("保存通知失败。");
	    }
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/add/upload", method = RequestMethod.POST)
	public JsonResult<String> notificationFileUpload(HttpServletRequest request, HttpServletResponse response){
		String filePath = basePath + fileNotificationPath;
		JsonResult<String> result = new JsonResult<String>();
		
		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
        if (fileInfos != null && fileInfos.size() > 0) {
        	List<Integer> fileIds = new ArrayList<Integer>();
            for (FileInfo fileInfo : fileInfos) {
            	NotificationFile file = new NotificationFile();
                file.setPath(fileInfo.getNewFilename());
                file.setTime(new Date());
                String fileName = fileInfo.getFilename();
                if (fileName.length() > 90) {
                	fileName = fileName.substring(0, 90);
                }
                file.setFileName(fileName);
                log.debug("upload file success : " + file.toString());
                notificationFileService.add(file);
                fileIds.add(file.getId());
            }
            result.setResponseResult(fileIds.stream().map(s -> s+"").reduce((s1, s2) -> s1 + "," + s2).get());
            return result;
        }
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/file/{id}", method = RequestMethod.POST)
	public JsonResult<String> notificationFileRemove(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Integer id){
		String filePath = basePath + fileNotificationPath;
		JsonResult<String> result = new JsonResult<String>();
		
		if (id != null) {
			NotificationFile notificationFile = notificationFileService.findById(id);
			if (null != notificationFile) {
				FileUtils.remove(filePath + notificationFile.getPath());
				Integer[] ids = new Integer[] {id};
				notificationFileService.batchRemove(ids);
				result.setMessage("文件删除成功");
			} else {
				result.setCode("-1000");
				result.setMessage("文件没有找到，请重新刷新页面。");
			}
		} else {
			result.setCode("-1000");
			result.setMessage("ID不能为空，请重新刷新页面。");
		}
		
		return result;
	}

	/**
	 * 修改通知
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") Notification form) {
		Department department = departmentService.findByName("K2/K3核电项目部");
		if (null != department) {
			List<Department> departments = department.getChildren();
			if (null != departments && !departments.isEmpty()) {
				for (Department d : departments) {
					List<User> users = new ArrayList<User>();
					users.addAll(departmentUser(d));
					d.setUsers(users);
				}
			}
			
			request.setAttribute("department", departments);
		}
		
		request.setAttribute("alarmTime", ConferenceAlarmTypsEnums.getList());
		request.setAttribute("category", ConferenceCategory.getList());
		request.setAttribute("type", ConferenceType.getList());
		request.setAttribute("currentTime", new Date());
		
		return ForwardUtility.forwardAdminView("/notification/page_notification_edit");
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") Notification form) {
		log.debug("==> Start delete notification.");

		Integer[] ids = form.getIds();
		log.debug("==> To delete notification [" + CommonUtility.toJson(ids) + "]");
		if (ids != null && ids.length > 0) {
			notificationApiService.batchDelete(form.getIds());
		}
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete notification.");
	}
	/**
	 * 查看会议信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") Notification form) {
		Department department = departmentService.findByName("K2/K3核电项目部");
		if (null != department) {
			List<Department> departments = department.getChildren();
			if (null != departments && !departments.isEmpty()) {
				for (Department d : departments) {
					List<User> users = new ArrayList<User>();
					users.addAll(departmentUser(d));
					d.setUsers(users);
				}
			}
			
			request.setAttribute("department", departments);
		}
		
		request.setAttribute("alarmTime", ConferenceAlarmTypsEnums.getList());
		request.setAttribute("category", ConferenceCategory.getList());
		request.setAttribute("type", ConferenceType.getList());
		request.setAttribute("currentTime", new Date());
		
		String returnString = ForwardUtility.forwardAdminView("/notification/modal_notification_view");
		return returnString;
	}
}
