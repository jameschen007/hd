package com.easycms.hd.kpi.underlying.mybatis.dao;

import java.util.List;

import com.easycms.kpi.underlying.domain.KpiLibDetails;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;

public interface KpiLibDetailsMybatisDao {
	Long getCount();
	List<KpiLibDetails> findAll();
	
	List<KpiSummarySheet> findCurrent();

}
