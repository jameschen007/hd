package com.easycms.kpi.underlying.dao;
import com.easycms.basic.BasicDao;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.domain.KpiUserBase;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface KpiMonthlyEntryDao extends BasicDao<KpiMonthlyEntry> ,Repository<KpiMonthlyEntry,Integer>{

	@Query(value="from KpiMonthlyEntry u where u.monthlyMain.id=?1 and u.lib.id=?2 and u.isDel=0")
	KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer mainId,Integer libId);

	@Query(value="from KpiMonthlyEntry u where u.owner.id=?1 and u.monthlyMain.id=?2 and u.lib.id=?3 and u.isDel=0")
public KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer userId,Integer mainId,Integer libId);

	@Query(value="from KpiMonthlyEntry u where  u.monthlyMain.id=?1 and u.isDel=0")
	public List<KpiMonthlyEntry> findByMainId(Integer mainId);

	@Query(value="from KpiMonthlyEntry u where u.monthlyMain.id=?1 and u.lib.kpiType=?2 and u.isDel=0")
    List<KpiMonthlyEntry> findByMainId(Integer mainId,String kpiType);
	@Query(value="from KpiMonthlyEntry u where u.monthlyMain.id=?1 and u.stakeholder.id=?2 and u.isDel=0")
    List<KpiMonthlyEntry> findByMainIdAndStak(Integer mainId,Integer stakId);
	
	@Modifying
	@Query(value="delete from KpiMonthlyEntry u where u.id=?1")
	int deleteById(Integer id);
	/**
	 * 根据用户 、月份、和指标库id查询
	 * @param userId
	 * @param mainId
	 * @param libId
	 * @return
	 */
	@Query(value="from KpiMonthlyEntry u where u.owner.id=?1 and u.monthlyMain.id=?2 and u.lib.id=?3 and u.isDel=0")
	public KpiMonthlyEntry findKpiMonthlyEntryByUserIdMainId(Integer userId,Integer mainId,Integer libId);

	@Modifying
	@Query(value="delete from KpiMonthlyEntry u where u.monthlyMain.id=?1 and u.lib.id=?2")
	int deleteByMainIdAndLibId(Integer mainId,Integer libId);
	
	@Modifying
	@Query(value="delete from KpiMonthlyEntry u where u.monthlyMain.id=?1")
	 public int deleteByMainId(Integer mainId);
	
}