package com.easycms.kpi.underlying.dao;

import com.easycms.basic.BasicDao;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiLib;
import jodd.jtx.meta.Transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-24 下午07:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation= Propagation.REQUIRED)
public interface KpiLibDao extends BasicDao<KpiLib>,Repository<KpiLib,Integer> {


    @Query(value = "select a.id,kpi_name,b.real_name kpi_type from kpi_lib a,ec_user b where a.stakeholder=b.id and a.is_del=0 and b.is_del=0",nativeQuery = true)
    Page<KpiLib> findPageSql(KpiLib condition, Page<KpiLib> page);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @Modifying
    @Query("delete from KpiLib u WHERE u.id  IN (?1) ")
//    @Query("UPDATE KpiLib u SET u.isDel = 1 WHERE u.id  IN (?1) ")
    int deleteByIds(Integer[] ids);
    /**
     * 删除
     * @param id
     * @return
     */
    @Transaction
    @Modifying
    @Query("delete from KpiLib u WHERE u.id  IN (?1) ")
    int deleteById(Integer id);


    @Transaction
    @Modifying
    @Query("from KpiLib u WHERE u.user.id IN (?1) and isDel=0 and u.useType=?2 ")
    public List<KpiLib> findByUserIdUseType(Integer userId,String useType);
    
    
}