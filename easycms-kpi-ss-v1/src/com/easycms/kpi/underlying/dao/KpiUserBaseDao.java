package com.easycms.kpi.underlying.dao;
import com.easycms.basic.BasicDao;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.QueryHint;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-16 下午10:01:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface KpiUserBaseDao extends BasicDao<KpiUserBase> ,Repository<KpiUserBase,Integer>{

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("FROM KpiUserBase p WHERE p.user.id=?1 and p.isDel = 0")
    KpiUserBase findByUserId(Integer userId);
}