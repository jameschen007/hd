package com.easycms.kpi.underlying.form;

import com.easycms.core.form.BasicForm;

import java.io.Serializable;
import java.util.Date;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午05:18:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiHandleExchangeForm extends BasicForm implements Serializable{
	
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;

    /** 默认ID */
    private Integer id;
    /** 考核主表id */
    private Integer monthlyMainId;
    /** 处理用户id */
    private Integer handleUserId;
    /** 状态：N待处理,Y已处理 */
    private String status;
    /** 环节 */
    private String segment;
    /** 处理时间 */
    private Date handleTime;
    /** 删除状态:1为已删除，0为有效 */
    private boolean isDel;


    /**获取  默认ID */
    public Integer getId() {
        return id;
    }

    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    /**获取  考核主表id */
    public Integer getMonthlyMainId() {
        return monthlyMainId;
    }

    /**设置  考核主表id */
    public void setMonthlyMainId(Integer monthlyMainId) {
        this.monthlyMainId = monthlyMainId;
    }


    /**获取  处理用户id */
    public Integer getHandleUserId() {
        return handleUserId;
    }

    /**设置  处理用户id */
    public void setHandleUserId(Integer handleUserId) {
        this.handleUserId = handleUserId;
    }


    /**获取  状态：N待处理,Y已处理 */
    public String getStatus() {
        return status;
    }

    /**设置  状态：N待处理,Y已处理 */
    public void setStatus(String status) {
        this.status = status;
    }


    /**获取  环节 */
    public String getSegment() {
        return segment;
    }

    /**设置  环节 */
    public void setSegment(String segment) {
        this.segment = segment;
    }


    /**获取  处理时间 */
    public Date getHandleTime() {
        return handleTime;
    }

    /**设置  处理时间 */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }


    /**获取  删除状态:1为已删除，0为有效 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态:1为已删除，0为有效 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }
}
