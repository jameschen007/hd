package com.easycms.kpi.underlying.form;

import java.io.Serializable;
import java.util.Date;

import com.easycms.core.form.NoUserIdBasicForm;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiSummarySheetForm extends NoUserIdBasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;
    private Integer loginId;

    /** 用户id */
    private Integer userId;
    /** 月份 */
    private String month;
    /** 部门 */
    private String departmentName;
    /** 姓名 */
    private String realName;
    /** 自评分 */
    private Integer selfScoring;
    /** 干系人评分 */
    private Integer stakeholderScoring;
    /** 直接上级评分 */
    private Integer directSuperiorScoring;
    /** 总经理评分 */
    private Integer generalManagerScoring;
    /** 综合得分 */
    private Integer compositeScoring;
    /** 删除状态 */
    private boolean isDel;
    /** 创建时间 */
    private Date createTime;


    /**获取  用户id */
    public Integer getUserId() {
        return userId;
    }

    /**设置  用户id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    /**获取  月份 */
    public String getMonth() {
        return month;
    }

    /**设置  月份 */
    public void setMonth(String month) {
        this.month = month;
    }


    /**获取  部门 */
    public String getDepartmentName() {
        return departmentName;
    }

    /**设置  部门 */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }


    /**获取  姓名 */
    public String getRealName() {
        return realName;
    }

    /**设置  姓名 */
    public void setRealName(String realName) {
        this.realName = realName;
    }


    /**获取  自评分 */
    public Integer getSelfScoring() {
        return selfScoring;
    }

    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }


    /**获取  干系人评分 */
    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }

    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }


    /**获取  直接上级评分 */
    public Integer getDirectSuperiorScoring() {
        return directSuperiorScoring;
    }

    /**设置  直接上级评分 */
    public void setDirectSuperiorScoring(Integer directSuperiorScoring) {
        this.directSuperiorScoring = directSuperiorScoring;
    }


    /**获取  总经理评分 */
    public Integer getGeneralManagerScoring() {
        return generalManagerScoring;
    }

    /**设置  总经理评分 */
    public void setGeneralManagerScoring(Integer generalManagerScoring) {
        this.generalManagerScoring = generalManagerScoring;
    }


    /**获取  综合得分 */
    public Integer getCompositeScoring() {
        return compositeScoring;
    }

    /**设置  综合得分 */
    public void setCompositeScoring(Integer compositeScoring) {
        this.compositeScoring = compositeScoring;
    }


    /**获取  删除状态 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }


    /**获取  创建时间 */
    public Date getCreateTime() {
        return createTime;
    }

    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}

}
