package com.easycms.kpi.underlying.form;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;

import java.io.Serializable;
import java.util.Date;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-20 下午05:15:10<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiUserBaseForm extends BasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;

    /** 默认ID */
    private Integer id;
    /** 用户id */
    private User user;
    /** 人员类别atThe：：部门正、副职，director：：室主任/主管，employees：：普通员工， */
    private String personnelCategory;
    /** 干系人_用户id */
    private User stakeholder;
    /** 直接上级_用户id */
    private User immediateSuperior;
    /** 备注 */
    private String mark;
    /** 删除状态 */
    private boolean isDel;
    /** 创建时间 */
    private Date createTime;


    /**获取  默认ID */
    public Integer getId() {
        return id;
    }

    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    /**获取  用户id */
    public User getUser() {
        return user;
    }

    /**设置  用户id */
    public void setUser(User user) {
        this.user = user;
    }


    /**获取  人员类别atThe：：部门正、副职，director：：室主任/主管，employees：：普通员工， */
    public String getPersonnelCategory() {
        return personnelCategory;
    }

    /**设置  人员类别atThe：：部门正、副职，director：：室主任/主管，employees：：普通员工， */
    public void setPersonnelCategory(String personnelCategory) {
        this.personnelCategory = personnelCategory;
    }


    /**获取  干系人_用户id */
    public User getStakeholder() {
        return stakeholder;
    }

    /**设置  干系人_用户id */
    public void setStakeholder(User stakeholder) {
        this.stakeholder = stakeholder;
    }


    /**获取  直接上级_用户id */
    public User getImmediateSuperior() {
        return immediateSuperior;
    }

    /**设置  直接上级_用户id */
    public void setImmediateSuperior(User immediateSuperior) {
        this.immediateSuperior = immediateSuperior;
    }

    /**获取  备注 */
    public String getMark() {
        return mark;
    }

    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }


    /**获取  删除状态 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }


    /**获取  创建时间 */
    public Date getCreateTime() {
        return createTime;
    }

    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }
}