package com.easycms.kpi.underlying.form;

import java.io.Serializable;
import java.util.Date;

import com.easycms.core.form.BasicForm;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiMonthlyEntryForm extends BasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;

    /** 默认ID */
    private Integer id;
    /** 被考核人 */
    private Integer ownerId;
    /** 指标库id */
    private KpiLib lib;
    /** 权重 */
    private Integer weight;
    /** 完成时间 */
    private Date finishTime;
    /** 自评分 */
    private Integer selfScoring;
    /** 干系人 */
    private Integer stakeholderId;
    /** 干系人评分 */
    private Integer stakeholderScoring;
    /** 状态 */
    private String status;
    /** 备注 */
    private String mark;
    /** 自评理由 */
    private String selfReason;
    /** 干系人打分打分理由 */
    private String stakeholderScoringReason;
    /** 一票否决理由 */
    private String oneTicketVetoReason;
    /** 删除状态 */
    private boolean isDel;
    /** 创建时间 */
    private Date createTime;
    /** 主表id */
    private KpiMonthlyMain monthlyMainId;


    /**获取  默认ID */
    public Integer getId() {
        return id;
    }

    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    /**获取  被考核人 */
    public Integer getOwnerId() {
        return ownerId;
    }

    /**设置  被考核人 */
    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }


    /**获取  指标库id */
    public KpiLib getLib() {
        return lib;
    }

    /**设置  指标库id */
    public void setLib(KpiLib lib) {
        this.lib = lib;
    }


    /**获取  权重 */
    public Integer getWeight() {
        return weight;
    }

    /**设置  权重 */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }


    /**获取  完成时间 */
    public Date getFinishTime() {
        return finishTime;
    }

    /**设置  完成时间 */
    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }


    /**获取  自评分 */
    public Integer getSelfScoring() {
        return selfScoring;
    }

    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }


    /**获取  干系人 */
    public Integer getStakeholderId() {
        return stakeholderId;
    }

    /**设置  干系人 */
    public void setStakeholderId(Integer stakeholderId) {
        this.stakeholderId = stakeholderId;
    }


    /**获取  干系人评分 */
    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }

    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }


    /**获取  状态 */
    public String getStatus() {
        return status;
    }

    /**设置  状态 */
    public void setStatus(String status) {
        this.status = status;
    }


    /**获取  备注 */
    public String getMark() {
        return mark;
    }

    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }


    /**获取  自评理由 */
    public String getSelfReason() {
        return selfReason;
    }

    /**设置  自评理由 */
    public void setSelfReason(String selfReason) {
        this.selfReason = selfReason;
    }


    /**获取  干系人打分打分理由 */
    public String getStakeholderScoringReason() {
        return stakeholderScoringReason;
    }

    /**设置  干系人打分打分理由 */
    public void setStakeholderScoringReason(String stakeholderScoringReason) {
        this.stakeholderScoringReason = stakeholderScoringReason;
    }


    /**获取  一票否决理由 */
    public String getOneTicketVetoReason() {
        return oneTicketVetoReason;
    }

    /**设置  一票否决理由 */
    public void setOneTicketVetoReason(String oneTicketVetoReason) {
        this.oneTicketVetoReason = oneTicketVetoReason;
    }


    /**获取  删除状态 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }


    /**获取  创建时间 */
    public Date getCreateTime() {
        return createTime;
    }

    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    /**获取  主表id */
    public KpiMonthlyMain getMonthlyMainId() {
        return monthlyMainId;
    }

    /**设置  主表id */
    public void setMonthlyMainId(KpiMonthlyMain monthlyMainId) {
        this.monthlyMainId = monthlyMainId;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }
}
