package com.easycms.kpi.underlying.form;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.easycms.core.form.BasicForm;
import com.easycms.core.form.NoUserIdBasicForm;

import lombok.Data;
@Data
public class MyLibRealLibForm extends NoUserIdBasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;
    private Integer userId;
    private Integer libId;
    private Integer weight;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date finishDate;
}
