package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiUserBase;

import java.util.List;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-16 下午10:01:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiUserBaseService{

    /**
     * 查找所有
     * @return
     */
    List<KpiUserBase> findAll();

    List<KpiUserBase> findAll(KpiUserBase condition);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiUserBase> findPage(Specifications<KpiUserBase> sepc,Pageable pageable);


    Page<KpiUserBase> findPage(Page<KpiUserBase> page);

    Page<KpiUserBase> findPage(KpiUserBase condition,Page<KpiUserBase> page);

    /**
     *
     * @return
     */
    KpiUserBase userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiUserBase findKpiUserBase(String username);

    public KpiUserBase findKpiUserBaseByUserId(Integer id);
    public KpiUserBase findById(Integer id);
    /**
     * 添加用户
     * @param kpiUserBase
     * @return
     */
    KpiUserBase add(KpiUserBase kpiUserBase);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiUserBase
     * @return
     */
    KpiUserBase update(KpiUserBase kpiUserBase);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiUserBase> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiUserBase> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiUserBase> findKpiUserBaseByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiUserBase 发起问题的kpiUserBase
     * @return
     */
    List<KpiUserBase> getSolvers(KpiUserBase kpiUserBase);

}