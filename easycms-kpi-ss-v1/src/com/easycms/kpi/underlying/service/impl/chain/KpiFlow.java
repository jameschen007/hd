package com.easycms.kpi.underlying.service.impl.chain;

import java.util.HashMap;
import java.util.Map;

public class KpiFlow {
	public static KpiFlowChain ch_atThe = new KpiFlowChain();
	//部门正、副职：自评后至干系人、分管领导、总经理
	 static {
//	ch_atThe.addNode("6.generalScore2");
	ch_atThe.addNode("5.generalScore");
	ch_atThe.addNode("4.chargeLeaderScore");//由上级领导评分4.parentScore改为 由分管领导评分 chargeLeaderScore
	ch_atThe.addNode("3.stakeholderScore");
	ch_atThe.addNode("2.selfScore");
	ch_atThe.addNode("1.parentOk");
	}
	
	//director：：室主任/主管
	public static KpiFlowChain ch_director = new KpiFlowChain();
	static {
	ch_director.addNode("4.departmentManagerScore");//由上级领导评分4.parentScore改为 由部门经理评分 departmentManagerScore
	ch_director.addNode("3.stakeholderScore");
	ch_director.addNode("2.selfScore");
	ch_director.addNode("1.parentOk");

	}
	//employees：：普通员工
	public static KpiFlowChain ch_employees = new KpiFlowChain();
	static {

	ch_employees.addNode("3.parentScore");
	ch_employees.addNode("2.selfScore");
	ch_employees.addNode("1.parentOk");
	}
	
	public static Map<String,KpiFlowChain> flowMap =null ;
	public static Map<String,KpiFlowChain> getFlowMap(){
		if(flowMap==null){
			flowMap = new HashMap<String,KpiFlowChain>();
			flowMap.put("atThe", ch_atThe);
			flowMap.put("director", ch_director);
			flowMap.put("employees", ch_employees);
		}
		return flowMap; 
	}

	public static void main(String[] args) {
		//atThe：：部门正、副职
		KpiFlowChain ch_atThe = new KpiFlowChain();
		ch_atThe.addNode("6.generalScore2");
		ch_atThe.addNode("5.generalScore");
		ch_atThe.addNode("4.chargeLeaderScore");
		ch_atThe.addNode("3.stakeholderScore");
		ch_atThe.addNode("2.selfScore");
		ch_atThe.addNode("1.parentOk");
		
		//director：：室主任/主管
		KpiFlowChain ch_director = new KpiFlowChain();
		ch_director.addNode("4.departmentManagerScore");
		ch_director.addNode("3.stakeholderScore");
		ch_director.addNode("2.selfScore");
		ch_director.addNode("1.parentOk");
		
		//employees：：普通员工
		KpiFlowChain ch_employees = new KpiFlowChain();

		ch_employees.addNode("3.parentScore");
		ch_employees.addNode("2.selfScore");
		ch_employees.addNode("1.parentOk");

//		System.out.println(ch_employees.getSize());
//		System.out.println(ch_employees.searchNode("1.parentOk").current);
//		System.out.println(ch_employees.searchNode("1.parentOk").nextNode.current);
//		System.out.println(ch_employees.searchNode("2.selfScore").current);
//		System.out.println(ch_employees.searchNode("2.selfScore").nextNode.current);
//		System.out.println(ch_employees.searchNode("3.parentScore").current);
//		System.out.println(ch_employees.searchNode("3.parentScore").nextNode==null);
		
		KpiFlowChainNode flowNode= ch_employees.getHead();
		for (int i=0;i<ch_employees.getSize();i++){
			System.out.println(flowNode.current);
			if(ch_employees.searchNode(flowNode.current).nextNode!=null){
				flowNode = ch_employees.searchNode(flowNode.current).nextNode;
			}
		}
		
		
		
	}
	
}
