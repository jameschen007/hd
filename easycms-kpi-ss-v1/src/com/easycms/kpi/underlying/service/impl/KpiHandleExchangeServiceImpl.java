package com.easycms.kpi.underlying.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.dao.KpiHandleExchangeDao;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.service.PrivilegeService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午05:18:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("kpiHandleExchangeService")
public class KpiHandleExchangeServiceImpl implements KpiHandleExchangeService {

    private static final Logger logger = Logger.getLogger(KpiHandleExchangeServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiHandleExchangeDao kpiHandleExchangeDao;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private Authenticator<KpiHandleExchange> authemtocator;



    @Override
    public KpiHandleExchange userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiHandleExchange findKpiHandleExchange(String username) {
        // TODO Auto-generated method stub
//		return kpiHandleExchangeDao.findByName(username);
        return null;
    }

    @Override
    public List<KpiHandleExchange> findByMainId(Integer mainId){
    	return kpiHandleExchangeDao.findByMainId(mainId);
    }
    @Override
    public KpiHandleExchange findKpiHandleExchangeById(Integer id) {
        return kpiHandleExchangeDao.findById(id);
    }

//	@Override
//	public Page<KpiHandleExchange> findPage(Specifications<KpiHandleExchange> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiHandleExchangeDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiHandleExchangeService#add(com.easycms.core.user.domain.KpiHandleExchange)
     */
    @Override
    public KpiHandleExchange add(KpiHandleExchange kpiHandleExchange) {
        return kpiHandleExchangeDao.save(kpiHandleExchange);
    }
    

    @Override
    public boolean addAll(List<KpiHandleExchange> kpiHandleExchanges){
    	boolean f = false ;
    	if(kpiHandleExchanges!=null && kpiHandleExchanges.size()>0){
    		try{
    			
    			for(KpiHandleExchange b:kpiHandleExchanges){
    				logger.info("prepare save KpiHandleExchange ...");
    				kpiHandleExchangeDao.save(b);
    				logger.info("prepare save KpiHandleExchange successful");
    			}
    			f = true ;
    		}catch(Exception e){

				logger.error("prepare save KpiHandleExchange error");
    			e.printStackTrace();
    			f = false ;
    		}
    		
    	}else{
    		return true ;
    	}
    	return f;
    }
    @Override
    public boolean addOrUpdateAll(List<KpiHandleExchange> kpiHandleExchanges){
    	boolean f = false ;
    	if(kpiHandleExchanges!=null && kpiHandleExchanges.size()>0){
    		try{
    			List<KpiHandleExchange> dbBeans = kpiHandleExchangeDao.findByMainId(kpiHandleExchanges.get(0).getMonthlyMain().getId());
    			if(dbBeans==null || dbBeans.size()==0){
    				addAll(kpiHandleExchanges);
    			}
				for(KpiHandleExchange dbBean:dbBeans){
        			for(KpiHandleExchange b:kpiHandleExchanges){
        				
    					if(dbBean.getMonthlyMain().getId().intValue()==b.getMonthlyMain().getId().intValue()
    							&& dbBean.getHandleUserId().intValue()==b.getHandleUserId().intValue()
    							&& dbBean.getSegment().equals(b.getSegment())
    							&& dbBean.getSegmentStatus().equals(b.getSegmentStatus())){
    	    				
    	    				dbBean.setVisitStatus(b.getVisitStatus());
    						kpiHandleExchangeDao.save(dbBean);
    					}
        			}
    			}

    			f = true ;
    		}catch(Exception e){

				logger.error("prepare save KpiHandleExchange error");
    			e.printStackTrace();
    			f = false ;
    		}
    		
    	}else{
    		return true ;
    	}
    	return f;
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiHandleExchangeDao.deleteById(id) > 0;

    }

    @Override
    public KpiHandleExchange update(KpiHandleExchange kpiHandleExchange) {
        // TODO Auto-generated method stub
        return kpiHandleExchangeDao.save(kpiHandleExchange);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
            KpiHandleExchange kpiHandleExchange = this.findKpiHandleExchangeById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiHandleExchangeDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        // TODO Auto-generated method stub

        return true ;
    }

    @Override
    public List<KpiHandleExchange> findAll() {
        // TODO Auto-generated method stub
        return kpiHandleExchangeDao.findAll();
    }


    @Override
    public List<KpiHandleExchange> findAll(KpiHandleExchange condition) {
        // TODO Auto-generated method stub
        return kpiHandleExchangeDao.findAll(QueryUtil.queryConditions(condition));
    }

    @Override
    public Page<KpiHandleExchange> findPage(Page<KpiHandleExchange> page) {
//        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//
//        org.springframework.data.domain.Page<KpiHandleExchange> springPage = kpiHandleExchangeDao.findAll(null,pageable);
//        page.execute(Integer.valueOf(Long.toString(springPage
//                .getTotalElements())), page.getPageNum(), springPage.getContent());
//        return page;
        return null ;

    }
    @Override
    public Page<KpiHandleExchange> findPage(KpiHandleExchange condition,Page<KpiHandleExchange> page) {
//        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//        logger.debug("==> Find all kpiHandleExchange by condition.");
//        org.springframework.data.domain.Page<KpiHandleExchange> springPage = kpiHandleExchangeDao.findAll(QueryUtil.queryConditions(condition),pageable);
//        page.execute(Integer.valueOf(Long.toString(springPage
//                .getTotalElements())), page.getPageNum(), springPage.getContent());
//        return page;
        return null ;

    }

    @Override
    public Integer count() {
        // TODO Auto-generated method stub
        return kpiHandleExchangeDao.count();
    }

    @Override
    public List<KpiHandleExchange> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiHandleExchange> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiHandleExchange> findKpiHandleExchangeByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiHandleExchange> getSolvers(KpiHandleExchange kpiHandleExchange) {
        return null;
    }

}
