package com.easycms.kpi.underlying.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.dao.KpiLibDao;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.service.KpiLibService;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-24 下午07:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("kpiLibService")
public class KpiLibServiceImpl implements KpiLibService {

    private static final Logger logger = Logger.getLogger(KpiLibServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiLibDao kpiLibDao;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private Authenticator<KpiLib> authemtocator;



    @Override
    public KpiLib userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiLib findKpiLib(String username) {
        // TODO Auto-generated method stub
//		return kpiLibDao.findByName(username);
        return null;
    }

    @Override
    public KpiLib findKpiLibById(Integer id) {
        return kpiLibDao.findById(id);
    }
    

    public List<KpiLib> findByUserIdUseType(Integer userId,String useType){
    	return kpiLibDao.findByUserIdUseType(userId,useType);
    }

//	@Override
//	public Page<KpiLib> findPage(Specifications<KpiLib> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiLibDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiLibService#add(com.easycms.core.user.domain.KpiLib)
     */
    @Override
    public KpiLib add(KpiLib kpiLib) {
        return kpiLibDao.save(kpiLib);
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiLibDao.deleteById(id) > 0;

    }

    @Override
    public KpiLib update(KpiLib kpiLib) {
    	
        List<KpiLib> libs = kpiLibDao.findByUserIdUseType(kpiLib.getUser().getId(), "mine");
//修改干系人时，需要修改对应的已选的指标库的对应干系人？？
            	for(KpiLib l:libs){
            		if(kpiLib.getKpiName().equals(l.getKpiName())){
                		l.setStakeholder(kpiLib.getStakeholder());
                		kpiLibDao.save(l);
            		}
            	}


        // TODO Auto-generated method stub
        return kpiLibDao.save(kpiLib);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
            KpiLib kpiLib = this.findKpiLibById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiLibDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        try {
            for (Integer id : ids) {
                logger.info("即将删除掉指标库id="+id);
                kpiLibDao.deleteById(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true ;
    }

    @Override
    public List<KpiLib> findAll() {
        // TODO Auto-generated method stub
        return kpiLibDao.findAll();
    }

    @Override
    public List<KpiLib> findAll(final KpiLib condition) {
        Specification spe = new Specification<KpiLib> () {

			@Override
			public Predicate toPredicate(Root<KpiLib> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				List<Predicate> predicate = new ArrayList<Predicate>(); 
				logger.debug("kpiType="+condition.getKpiType());
				logger.debug("useType="+condition.getUseType());
				logger.debug("kpiType="+condition.getKpiType());
				logger.debug("user.id="+condition.getKpiType());

				if (condition.getId()!=null){
                    predicate.add(cb.equal(root.get("id").as(String.class), condition.getId()));
                }
				if (condition.getKpiName()!=null){
                    predicate.add(cb.like(root.get("kpiName").as(String.class), "%" +condition.getKpiName()+"%" ));
                }
				if (condition.getKpiType()!=null && !"".equals(condition.getKpiType())){
                    predicate.add(cb.equal(root.get("kpiType").as(String.class), condition.getKpiType()));
                }
				if(condition.getUseType()!=null && !"".equals(condition.getUseType())){
					predicate.add(cb.equal(root.get("useType").as(String.class),condition.getUseType()));
				}
				predicate.add(cb.equal(root.get("isDel").as(boolean.class),false));
				//两张表关联查询
                Join<KpiLib,User> userJoin = root.join(root.getModel().getSingularAttribute("user",User.class),JoinType.LEFT);
//              
                //如果是通用指标
                predicate.add(cb.equal(userJoin.get("id").as(Integer.class), condition.getUser().getId() ));
                Predicate[] pre = new Predicate[predicate.size()];
                return query.where(predicate.toArray(pre)).getRestriction();
			}
        	
        };
        return kpiLibDao.findAll(spe);
    }

    @Override
    public Page<KpiLib> findPage(Page<KpiLib> page) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());


        org.springframework.data.domain.Page<KpiLib> springPage = kpiLibDao.findAll(null,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;
    }
    @Override
    public Page<KpiLib> findPage(final KpiLib condition,Page<KpiLib> page) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

        logger.debug("==> Find all kpiLib by condition.");

        Specification spe = new Specification<KpiLib> () {

			@Override
			public Predicate toPredicate(Root<KpiLib> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				List<Predicate> predicate = new ArrayList<Predicate>(); 
				logger.debug("kpiType="+condition.getKpiType());
				logger.debug("useType="+condition.getUseType());
				logger.debug("kpiType="+condition.getKpiType());
				logger.debug("user.id="+condition.getKpiType());

				if (condition.getId()!=null){
                    predicate.add(cb.equal(root.get("id").as(String.class), condition.getId()));
                }
				if (condition.getKpiName()!=null){
                    predicate.add(cb.like(root.get("kpiName").as(String.class), "%" +condition.getKpiName()+"%" ));
                }
				if (condition.getKpiType()!=null && !"".equals(condition.getKpiType())){
                    predicate.add(cb.equal(root.get("kpiType").as(String.class), condition.getKpiType()));
                }
				if(condition.getUseType()!=null && !"".equals(condition.getUseType())){
					predicate.add(cb.equal(root.get("useType").as(String.class),condition.getUseType()));
				}
				predicate.add(cb.equal(root.get("isDel").as(boolean.class),false));
				//两张表关联查询
                Join<KpiLib,User> userJoin = root.join(root.getModel().getSingularAttribute("user",User.class),JoinType.LEFT);
//              
                //如果是通用指标 通用指标的系统实现与岗位指标完全相同，只是名字之差。
//                if("general".equals(condition.getKpiType()) && (condition.getUser()==null || condition.getUser().getId()==null)){
//                    predicate.add(cb.equal(userJoin.get("id").as(Integer.class), 0 ));
//                }else{
                    predicate.add(cb.equal(userJoin.get("id").as(Integer.class), condition.getUser().getId() ));
//                }
                Predicate[] pre = new Predicate[predicate.size()];
                return query.where(predicate.toArray(pre)).getRestriction();
			}
        	
        };
        //QueryUtil.queryConditions(condition)
        
        
        org.springframework.data.domain.Page<KpiLib> springPage = kpiLibDao.findAll(spe,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;
    }

    @Override
    public Integer count() {
        // TODO Auto-generated method stub
        return kpiLibDao.count();
    }

    @Override
    public List<KpiLib> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiLib> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiLib> findKpiLibByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiLib> getSolvers(KpiLib kpiLib) {
        return null;
    }
    

//    public Specification getCondi(){
//    	Specification me = new Specification<KpiLib> () {
//
//			@Override
//			public Predicate toPredicate(Root<KpiLib> r, CriteriaQuery<?> query, CriteriaBuilder cb) {
//				Join<KpiLib,KpiLib> join = r.jo
//				Path<String> exp4 = r
//				return null;
//			}
//		};
//    	return me ;
//    }

}