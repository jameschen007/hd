package com.easycms.kpi.underlying.service.impl.chain;

public class KpiFlowChain {
	 private KpiFlowChainNode head; // 头节点
	 private int size; // 链表的实体（即节点的数量）
	/**
	* 获得链表中节点数量
	* 
	* @return 链表中节点数
	*/
	public int getSize() {
		return this.size;
	}

	/**
	* 添加节点的一般方法
	* 
	* @param p
	*/
	public void addNode(String p) {
		if (head != null) { // 如果有头节点，则添加新节点作为头节点
			head = new KpiFlowChainNode( p, head);
			size++;
		}else{
			// 如果没有头节点，则添加对象作为头节点
			head = new KpiFlowChainNode(p, null);
			size++;
		}
	}
	public KpiFlowChainNode searchNode(String segment) {
		KpiFlowChainNode p = null;
		for (KpiFlowChainNode pcn = head; pcn != null; pcn = pcn.nextNode) {
			if (pcn.current.equals(segment)) {
				p = pcn;
			}
		}
		return p;
	}
	
	public KpiFlowChainNode getHead(){
		return head;
	}
}
