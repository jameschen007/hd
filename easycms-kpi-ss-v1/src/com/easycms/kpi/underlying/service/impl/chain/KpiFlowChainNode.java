package com.easycms.kpi.underlying.service.impl.chain;

public class KpiFlowChainNode {
	public String current;
	public KpiFlowChainNode nextNode;
	
	// 获取下一个实体
	public KpiFlowChainNode getNextNode(){
		return nextNode;
	}
	public KpiFlowChainNode (String segment,KpiFlowChainNode ep){
		current = segment;
		nextNode = ep ;
	}
	
}
