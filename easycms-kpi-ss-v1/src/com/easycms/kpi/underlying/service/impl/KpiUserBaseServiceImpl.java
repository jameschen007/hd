package com.easycms.kpi.underlying.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.dao.KpiUserBaseDao;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.service.PrivilegeService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-16 下午10:01:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("kpiUserBaseService")
public class KpiUserBaseServiceImpl implements KpiUserBaseService {

    private static final Logger logger = Logger.getLogger(KpiUserBaseServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiUserBaseDao kpiUserBaseDao;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private Authenticator<KpiUserBase> authemtocator;



    @Override
    public KpiUserBase userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiUserBase findKpiUserBase(String username) {
        // TODO Auto-generated method stub
//		return kpiUserBaseDao.findByName(username);
        return null;
    }

    @Override
    public KpiUserBase findKpiUserBaseByUserId(Integer id) {
        return kpiUserBaseDao.findByUserId(id);
    }
    @Override
    public KpiUserBase findById(Integer id) {
        return kpiUserBaseDao.findById(id);
    }

//	@Override
//	public Page<KpiUserBase> findPage(Specifications<KpiUserBase> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiUserBaseDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiUserBaseService#add(com.easycms.core.user.domain.KpiUserBase)
     */
    @Override
    public KpiUserBase add(KpiUserBase kpiUserBase) {
    	
    	KpiUserBase old = kpiUserBaseDao.findByUserId(kpiUserBase.getUser().getId());
    	if(old!=null){
    		old.setPersonnelCategory(kpiUserBase.getPersonnelCategory());
            return kpiUserBaseDao.save(old);
    	}else{
            return kpiUserBaseDao.save(kpiUserBase);
    	}
    	
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiUserBaseDao.deleteById(id) > 0;

    }

    @Override
    public KpiUserBase update(KpiUserBase kpiUserBase) {
        // TODO Auto-generated method stub
        return kpiUserBaseDao.save(kpiUserBase);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
//            KpiUserBase kpiUserBase = this.findKpiUserBaseById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiUserBaseDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        // TODO Auto-generated method stub

        return true ;
    }

    @Override
    public List<KpiUserBase> findAll() {
        // TODO Auto-generated method stub
        return kpiUserBaseDao.findAll();
    }


    @Override
    public List<KpiUserBase> findAll(KpiUserBase condition) {
        // TODO Auto-generated method stub
        return kpiUserBaseDao.findAll(QueryUtil.queryConditions(condition));
    }

    @Override
    public Page<KpiUserBase> findPage(Page<KpiUserBase> page) {
        PageRequest pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());


        org.springframework.data.domain.Page<KpiUserBase> springPage = kpiUserBaseDao.findAll(null,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;
    }
    @Override
    public Page<KpiUserBase> findPage(KpiUserBase condition,Page<KpiUserBase> page) {
        PageRequest pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

        logger.debug("==> Find all kpiUserBase by condition.");
        org.springframework.data.domain.Page<KpiUserBase> springPage = kpiUserBaseDao.findAll(QueryUtil.queryConditions(condition),pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;
    }

    @Override
    public Integer count() {
        // TODO Auto-generated method stub
        return kpiUserBaseDao.count();
    }

    @Override
    public List<KpiUserBase> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiUserBase> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiUserBase> findKpiUserBaseByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiUserBase> getSolvers(KpiUserBase kpiUserBase) {
        return null;
    }

}