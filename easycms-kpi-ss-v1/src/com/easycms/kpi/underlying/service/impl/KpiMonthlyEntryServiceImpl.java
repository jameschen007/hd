package com.easycms.kpi.underlying.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.dao.KpiLibDao;
import com.easycms.kpi.underlying.dao.KpiMonthlyEntryDao;
import com.easycms.kpi.underlying.dao.KpiMonthlyMainDao;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.service.KpiMonthlyEntryService;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.UserService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:22<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("kpiMonthlyEntryService")
public class KpiMonthlyEntryServiceImpl implements KpiMonthlyEntryService {

    private static final Logger logger = Logger.getLogger(KpiMonthlyEntryServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiMonthlyEntryDao kpiMonthlyEntryDao;
    @Autowired
    private KpiMonthlyMainDao kpiMonthlyMainDao;
    @Autowired
    private KpiLibDao kpiLibDao;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private UserService userService;
    @Autowired
    private Authenticator<KpiMonthlyEntry> authemtocator;

@Override
    public List<KpiMonthlyEntry> findByMainId(Integer mainId){
	//因为原设计干系人复制在本表中。现在真实的需求是换人立即换人操作，并不让原干系人做完，故在此添加一个修改数据的判断方法。
	
	List<KpiMonthlyEntry> entrys =this.kpiMonthlyEntryDao.findByMainId(mainId) ;
	
	if(entrys!=null && entrys.size()>0){
		for(KpiMonthlyEntry en:entrys){
			if(en.getStakeholder()!=null && en.getLib()!=null && en.getLib().getStakeholder()!=null 
					&& en.getStakeholder().getId().intValue()!=en.getLib().getStakeholder().getId().intValue()){
				en.setStakeholder(en.getLib().getStakeholder());
				kpiMonthlyEntryDao.save(en);
			}else if(en.getStakeholder()==null && en.getLib()!=null && en.getLib().getStakeholder()!=null){//详情指标评分项中没有干系人时，将异常数据处理。
				en.setStakeholder(en.getLib().getStakeholder());
				kpiMonthlyEntryDao.save(en);
			}
		}
	}
	
	
		return entrys ;
    }

@Override
	public
    List<KpiMonthlyEntry> findByMainId(Integer mainId,String kpiType){
		return this.kpiMonthlyEntryDao.findByMainId(mainId,kpiType);
    }
@Override
public
List<KpiMonthlyEntry> findByMainIdAndStak(Integer mainId,Integer stakId){
	return this.kpiMonthlyEntryDao.findByMainIdAndStak(mainId,stakId);
}


@Override
	public KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer mainId,Integer libId){
		return this.kpiMonthlyEntryDao.findKpiMonthlyEntryByUserIdMonth(mainId, libId);
	}
@Override
public KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer userId,Integer mainId,Integer libId){
	return this.kpiMonthlyEntryDao.findKpiMonthlyEntryByUserIdMonth(userId,mainId, libId);
}

	/**
	 * 如果没有,则进行添加后返回
	 * @param entry
	 * @return
	 */
    @Override
	public KpiMonthlyEntry findOrAdd(KpiMonthlyEntry entry){

    	KpiMonthlyEntry mai = null ;
		KpiMonthlyEntry mains = kpiMonthlyEntryDao.findKpiMonthlyEntryByUserIdMonth(entry.getOwner().getId(), entry.getMonthlyMain().getId(), entry.getLib().getId());
		if(mains!=null){
			mai = mains;
		}else{
			this.add(entry);
			return entry;
		}
		
		return mai ;
    	
    	
	}
    @Override
	public List<KpiMonthlyEntry> findOrAdd(List<KpiMonthlyEntry> entrys,boolean isaddNewLlib){
    	List<KpiMonthlyEntry> returnList = new ArrayList<KpiMonthlyEntry>();
    	
    	if(entrys!=null && entrys.size()>0){
    		for(KpiMonthlyEntry entry:entrys){
    	    	KpiMonthlyEntry mai = null ;
    			KpiMonthlyEntry mains = kpiMonthlyEntryDao.findKpiMonthlyEntryByUserIdMonth(entry.getOwner().getId(), entry.getMonthlyMain().getId(), entry.getLib().getId());
    			if(mains!=null ){
    				returnList.add(mains);
    			}else{
    				mains = new KpiMonthlyEntry();
    				mains.setMonthlyMain(entry.getMonthlyMain());
    				mains.setLib(entry.getLib());
    				mains.setOwner(entry.getOwner());
    				mains.setStakeholder(entry.getStakeholder());
    				 if(isaddNewLlib){
    	    			this.add(mains);
    				 }
    				returnList.add(mains);
    			}
    		}
    	}
		
		return returnList ;
	}
    

    @Override
	public boolean updateAll(List<KpiMonthlyEntry> entrys){
    	try{
        	if(entrys!=null && entrys.size()>0){
        		for(KpiMonthlyEntry entry:entrys){
        			kpiMonthlyEntryDao.save(entry);
        		}
        		return true ;
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return false ;
    }

    @Override
    public KpiMonthlyEntry userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiMonthlyEntry findKpiMonthlyEntry(String username) {
        // TODO Auto-generated method stub
//		return kpiMonthlyEntryDao.findByName(username);
        return null;
    }

    @Override
    public KpiMonthlyEntry findKpiMonthlyEntryById(Integer id) {
        return kpiMonthlyEntryDao.findById(id);
    }

//	@Override
//	public Page<KpiMonthlyEntry> findPage(Specifications<KpiMonthlyEntry> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiMonthlyEntryDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiMonthlyEntryService#add(com.easycms.core.user.domain.KpiMonthlyEntry)
     */
    @Override
    public KpiMonthlyEntry add(KpiMonthlyEntry kpiMonthlyEntry) {
        return kpiMonthlyEntryDao.save(kpiMonthlyEntry);
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiMonthlyEntryDao.deleteById(id) > 0;

    }

    @Override
    public KpiMonthlyEntry update(KpiMonthlyEntry kpiMonthlyEntry) {
        return kpiMonthlyEntryDao.save(kpiMonthlyEntry);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
            KpiMonthlyEntry kpiMonthlyEntry = this.findKpiMonthlyEntryById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiMonthlyEntryDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        //删除子表 和 指标表的内容
    	if(ids!=null && ids.length>0){
    		for(int id:ids){
    			KpiMonthlyEntry entry = kpiMonthlyEntryDao.findById(id);
    			KpiMonthlyMain main = entry.getMonthlyMain();
    			//只有在指标规划时可以修改
    	    	if(FlowName.selfShowInit.getCode().equals(main.getCurrentStatus())){
        			kpiMonthlyEntryDao.deleteById(id);
        			
        			//同时修改　添加指标中的　指标
        			KpiLib lib = entry.getLib();
        			lib.setDel(true);
        			kpiLibDao.save(lib);
        			
    			}
    		}
            return true ;
    	}

        return false ;
    }

    @Override
    public List<KpiMonthlyEntry> findAll() {
        // TODO Auto-generated method stub
        return kpiMonthlyEntryDao.findAll();
    }


    @Override
    public List<KpiMonthlyEntry> findAll(KpiMonthlyEntry condition) {
        // TODO Auto-generated method stub
        return kpiMonthlyEntryDao.findAll(QueryUtil.queryConditions(condition));
    }

    @Override
    public Page<KpiMonthlyEntry> findPage(Page<KpiMonthlyEntry> page) {
//        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//
//        org.springframework.data.domain.Page<KpiMonthlyEntry> springPage = kpiMonthlyEntryDao.findAll(null,pageable);
//        page.execute(Integer.valueOf(Long.toString(springPage
//                .getTotalElements())), page.getPageNum(), springPage.getContent());
//        return page;
        return null ;

    }
    @Override
    public Page<KpiMonthlyEntry> findPage(KpiMonthlyEntry condition,Page<KpiMonthlyEntry> page) {
//        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//        logger.debug("==> Find all kpiMonthlyEntry by condition.");
//        org.springframework.data.domain.Page<KpiMonthlyEntry> springPage = kpiMonthlyEntryDao.findAll(QueryUtil.queryConditions(condition),pageable);
//        page.execute(Integer.valueOf(Long.toString(springPage
//                .getTotalElements())), page.getPageNum(), springPage.getContent());
//        return page;
        return null ;

    }

    @Override
    public Integer count() {
        // TODO Auto-generated method stub
        return kpiMonthlyEntryDao.count();
    }

	/**
	 * 根据用户 、主表id、和指标库id,实时更新指标规划
	 * @param userId
	 * @param mainId
	 * @param libId
	 * @return
	 */
    @Override
    public boolean realLib(Integer userId,Integer mainId,Integer libId,Integer weight,Date finishDate){
    	boolean f = false ;
    	try{
    		if(weight!=null || finishDate!=null){
    			KpiMonthlyEntry bean = kpiMonthlyEntryDao.findKpiMonthlyEntryByUserIdMonth( userId,  mainId,  libId);
    			
    			//如果没有，则添加
    			if(bean==null){
    				bean = new KpiMonthlyEntry();
    				User own = userService.findUserById(userId);
    				bean.setOwner(own);
    				
    				KpiMonthlyMain main = kpiMonthlyMainDao.findById(mainId);
    				bean.setMonthlyMain(main);
    				KpiLib lib = kpiLibDao.findById(libId);
    				bean.setLib(lib);
    				bean.setStakeholder(lib.getStakeholder());
    			}
    			
    			if(weight!=null){
        			bean.setWeight(weight);
    			}
    		
    			if( finishDate!=null){
//         		   if(finishTimeStr==null || "".equals(finishTimeStr)||!finishTimeStr.matches("\\d{4}-\\d{2}-\\d{2}")){
        			bean.setFinishTime(finishDate);
    			}
        		kpiMonthlyEntryDao.save(bean);
    			f=true;
    		}
    		
    	}catch(Exception e){
    		
    		e.printStackTrace();
    	}
    	return f ;
 
    }
    

    public boolean deleteByMainIdAndLibId(Integer mainId,Integer libId){
    	boolean f = false ;
    	try{
        	kpiMonthlyEntryDao.deleteByMainIdAndLibId(mainId,libId);
        }catch(Exception e){
    		
    		e.printStackTrace();
    	}
    	return f ;
    }

    @Override
    public List<KpiMonthlyEntry> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiMonthlyEntry> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiMonthlyEntry> findKpiMonthlyEntryByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiMonthlyEntry> getSolvers(KpiMonthlyEntry kpiMonthlyEntry) {
        return null;
    }

}
