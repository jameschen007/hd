package com.easycms.kpi.underlying.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.poi.excel.ExcelColumn;
import com.easycms.common.poi.excel.ExcelHead;
import com.easycms.common.poi.excel.ExcelHelper;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.hd.kpi.underlying.mybatis.dao.KpiLibDetailsMybatisDao;
import com.easycms.hd.qualityctrl.domain.QualityControlDetils;
import com.easycms.kpi.underlying.dao.KpiHandleExchangeDao;
import com.easycms.kpi.underlying.dao.KpiMonthlyEntryDao;
import com.easycms.kpi.underlying.dao.KpiMonthlyMainDao;
import com.easycms.kpi.underlying.dao.KpiSummarySheetDao;
import com.easycms.kpi.underlying.directive.KpiSummarySheetDirective;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.service.PrivilegeService;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.StringModel;
import lombok.extern.slf4j.Slf4j;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Slf4j
@Service("kpiSummarySheetService")
public class KpiSummarySheetServiceImpl implements KpiSummarySheetService {

    private static final Logger logger = Logger.getLogger(KpiSummarySheetServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiSummarySheetDao kpiSummarySheetDao;
    @Autowired
    private KpiSummarySheetDirective directive;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private Authenticator<KpiSummarySheet> authemtocator;
    @Autowired
    private KpiLibDetailsMybatisDao kpiLibDetailsMybatisDao;
    @Autowired
    private KpiHandleExchangeDao kpiHandleExchangeDao;
    @Autowired
    private KpiMonthlyMainDao kpiMonthlyMainDao;
    @Autowired
    private KpiMonthlyEntryDao kpiMonthlyEntryDao;

	private StringModel stringModel;


    @Override
    public KpiSummarySheet userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiSummarySheet findKpiSummarySheet(String username) {
        // TODO Auto-generated method stub
//		return kpiSummarySheetDao.findByName(username);
        return null;
    }

    @Override
    public KpiSummarySheet findKpiSummarySheetById(Integer id) {
        return kpiSummarySheetDao.findById(id);
    }

//	@Override
//	public Page<KpiSummarySheet> findPage(Specifications<KpiSummarySheet> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiSummarySheetDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiSummarySheetService#add(com.easycms.core.user.domain.KpiSummarySheet)
     */
    @Override
    public KpiSummarySheet add(KpiSummarySheet kpiSummarySheet) {
        return kpiSummarySheetDao.save(kpiSummarySheet);
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiSummarySheetDao.deleteById(id) > 0;

    }

    @Override
    public KpiSummarySheet update(KpiSummarySheet kpiSummarySheet) {
        // TODO Auto-generated method stub
        return kpiSummarySheetDao.save(kpiSummarySheet);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
            KpiSummarySheet kpiSummarySheet = this.findKpiSummarySheetById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiSummarySheetDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        // TODO Auto-generated method stub

        return true ;
    }

    @Override
    public List<KpiSummarySheet> findAll() {
        // TODO Auto-generated method stub
        return kpiSummarySheetDao.findAll();
    }


    @Override
    public List<KpiSummarySheet> findAll(KpiSummarySheet condition) {
        // TODO Auto-generated method stub
        return kpiSummarySheetDao.findAll(QueryUtil.queryConditions(condition));
    }

    @Override
    public Page<KpiSummarySheet> findPage(Page<KpiSummarySheet> page) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
        org.springframework.data.domain.Page<KpiSummarySheet> springPage = kpiSummarySheetDao.findAll(null,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;

    }
    @Override
    public Page<KpiSummarySheet> findPage(KpiSummarySheet condition,Page<KpiSummarySheet> page) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

        logger.debug("==> Find all kpiSummarySheet by condition.");
        org.springframework.data.domain.Page<KpiSummarySheet> springPage = kpiSummarySheetDao.findAll(QueryUtil.queryConditions(condition),pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;
    }

    @Override
    public Integer count() {
        // TODO Auto-generated method stub
        return kpiSummarySheetDao.count();
    }

    @Override
    public List<KpiSummarySheet> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiSummarySheet> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiSummarySheet> findKpiSummarySheetByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiSummarySheet> getSolvers(KpiSummarySheet kpiSummarySheet) {
        return null;
    }

	@Override
	public List<KpiSummarySheet> getCurrentSheet() {
		
		return kpiLibDetailsMybatisDao.findCurrent();
	}

/**
 * 删除某一个绩效考核的所有记录
 */
	@Override
	public boolean deleteByMainId(Integer mainId){
		boolean f = false ;
		try{

//			TRUNCATE table 	kpi_handle_exchange ;-- 流程表
			kpiHandleExchangeDao.deleteByMainId(mainId);
//			truncate table kpi_monthly_entry ; -- 子表
			kpiMonthlyEntryDao.deleteByMainId(mainId);
//			truncate table kpi_monthly_main ; -- z主表
			kpiMonthlyMainDao.deleteByMainId(mainId);
			f= true ;
		}catch(Exception e){
			e.printStackTrace();
		}
		return
		
		f; 
    }
	@Override
	public boolean deleteHisByMainId(Integer mainId){
		boolean f = false ;
		try{
			KpiSummarySheet his = kpiSummarySheetDao.findById(mainId);
			his.setDel(true);
			kpiSummarySheetDao.save(his);
			f= true ;
		}catch(Exception e){
			e.printStackTrace();
		}
		return
		
		f; 
    }

/**导出历史绩效考核*/

	@Override
	public void statisticalReport(HttpServletRequest request, HttpServletResponse response){
		boolean status = false;
		try{
			@SuppressWarnings("rawtypes")
			Map map = new HashMap();
			String searchvalue= request.getParameter("search[value]");
			if(CommonUtility.isNonEmpty(searchvalue)){
				
			map.put("searchValue", searchvalue);
			}
			List<KpiSummarySheet> qualityControl = directive.list(map, null, "month", null, false, null, null);
			List<KpiSummarySheet> qualityControls = qualityControl.stream().map(mapper ->{
				return mapper;
			}).collect(Collectors.toList());
			
			//获取模板文件
			String tempFlieName = "KpiTemplate.xls";
	    	InputStream in = this.getClass().getClassLoader().getResourceAsStream(tempFlieName);
			
			//设置Excel每列显示内容
			List<ExcelColumn> columns = new ArrayList<ExcelColumn>();
			columns.add(new ExcelColumn(0,"month","时间"));
			columns.add(new ExcelColumn(1,"departmentName","部门"));
			columns.add(new ExcelColumn(2,"realName","姓名"));
			columns.add(new ExcelColumn(3,"selfScoring","自评分"));
			columns.add(new ExcelColumn(4,"stakeholderScoring","干系人评分"));
			columns.add(new ExcelColumn(5,"directSuperiorScoring","直接上级评分"));
			columns.add(new ExcelColumn(6,"generalManagerScoring","总经理评分"));
			columns.add(new ExcelColumn(7,"compositeScoring","综合得分"));
			
			ExcelHead head = new ExcelHead();
			head.setColumns(columns);
			head.setColumnCount(columns.size());
			head.setRowCount(2);
			if(qualityControls==null || qualityControls.size()==0){
				WebUtility.writeToClient(response, "没有查询到数据，请修改或删除搜索框中内容");
				return  ;
			}

			//写入数据
			ExcelHelper.getInstanse().exportExcelFile(head, in, response.getOutputStream(), qualityControls);
			status = true;
		}catch(Exception e){
			log.error("数据写入文件出错！"+e.getMessage());
			e.printStackTrace();
		}
	}
}
