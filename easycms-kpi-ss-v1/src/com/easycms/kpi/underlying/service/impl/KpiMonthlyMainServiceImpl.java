package com.easycms.kpi.underlying.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.action.KpiCycleHandle;
import com.easycms.kpi.underlying.dao.KpiMonthlyMainDao;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
import com.easycms.kpi.underlying.service.KpiMonthlyEntryService;
import com.easycms.kpi.underlying.service.KpiMonthlyMainService;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlow;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlowChain;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlowChainNode;
import com.easycms.kpi.underlying.service.impl.sys.PersonnelCategory;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:51<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("kpiMonthlyMainService")
@Transactional
public class KpiMonthlyMainServiceImpl implements KpiMonthlyMainService {

    private static final Logger logger = Logger.getLogger(KpiMonthlyMainServiceImpl.class);

    private static final String disturb = "easycms.com";
    @Autowired
    private KpiMonthlyMainDao kpiMonthlyMainDao;
    @Autowired
    private PrivilegeService privilegeService;
    @Autowired
    private WebsiteService websiteService;
    @Autowired
    private Authenticator<KpiMonthlyMain> authemtocator;
    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;
    @Autowired
    private KpiUserBaseService kpiUserBaseService;
    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;
    @Autowired
    private KpiMonthlyEntryService kpiMonthlyEntryService;
    
    
    
    @Autowired
    private UserDao userDao;
    

	/**
	 * 如果没有,则进行添加后返回
	 * @param main
	 * @return
	 */
    @Override
	public KpiMonthlyMain findOrAdd(KpiMonthlyMain main){
		KpiMonthlyMain mai = null ;
		KpiMonthlyMain mains = findKpiMonthlyMainByUserIdMonth(main.getOwner().getId(),main.getMonth());
		if(mains!=null ){
			mai = mains;
		} else {
//如果没有,则进行添加 新的主表记录，初始化考核周期为当月的月初第一天至月未最后一天，状态为待规划
			String needNewMonthMain = main.getMonth();

			String thisMonthc = "2017年08月";
			if (needNewMonthMain != null && !"".equals(needNewMonthMain)) {
				thisMonthc = needNewMonthMain.substring(0, 4) + "年" + needNewMonthMain.substring(4) + "月";// 从详情页中切换月份，到这里来新增这个月的主信息。
			}

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, Integer.valueOf(needNewMonthMain.substring(0, 4)) - 1);
			calendar.set(Calendar.MONTH, Integer.valueOf(needNewMonthMain.substring(4)) - 1);
			int maxd = calendar.getActualMaximum(Calendar.DATE);

			thisMonthc = thisMonthc + "1日~" + thisMonthc + maxd + "日";
			logger.info("添加时考核日期:" + thisMonthc);
			logger.info("main:" + main);

			main.setKpiCycle(thisMonthc);
			main.setKpiStatus(FlowName.selfShowInit.getMsg());
			main.setCurrentStatus(FlowName.selfShowInit.getCode());
			main.setKpiType("正在进行");
			main.setDel(false);

			mai = this.add(main);
		}
		
		return mai ;
	}


    @Override
    public KpiMonthlyMain findKpiMonthlyMainByUserIdMonth(Integer userId,String month){
    	return kpiMonthlyMainDao.findKpiMonthlyMainByUserIdMonth(userId, month);
    }
    
    @Override
    public KpiMonthlyMain userLogin(String username, String password) {
        return authemtocator.executeAuth(username, password, true);
    }

    @Override
    public KpiMonthlyMain findKpiMonthlyMain(String username) {
        // TODO Auto-generated method stub
//		return kpiMonthlyMainDao.findByName(username);
        return null;
    }

    @Override
    public KpiMonthlyMain findKpiMonthlyMainById(Integer id) {
        return kpiMonthlyMainDao.findById(id);
    }

//	@Override
//	public Page<KpiMonthlyMain> findPage(Specifications<KpiMonthlyMain> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return kpiMonthlyMainDao.findAll(sepc,pageable);
//	}

    /* (non-Javadoc)
     * @see com.easycms.core.user.service.KpiMonthlyMainService#add(com.easycms.core.user.domain.KpiMonthlyMain)
     */
    @Override
    public KpiMonthlyMain add(KpiMonthlyMain kpiMonthlyMain) {
        return kpiMonthlyMainDao.save(kpiMonthlyMain);
    }

    @Override
    public boolean deleteByName(String name) {
        return false;
    }

    @Override
    public boolean deleteByNames(String[] names) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return kpiMonthlyMainDao.deleteById(id) > 0;

    }

    @Override
    public KpiMonthlyMain update(KpiMonthlyMain kpiMonthlyMain) {
        return kpiMonthlyMainDao.save(kpiMonthlyMain);
    }

    @Override
    public boolean updatePassword(Integer id,String oldPassword,String newPassword) {
        if(id == null) {
            return false;
        }

        if(CommonUtility.isNonEmpty(oldPassword)){
            oldPassword = CommonUtility.MD5Digest(disturb, oldPassword);
        }else{
            KpiMonthlyMain kpiMonthlyMain = this.findKpiMonthlyMainById(id);
        }

        newPassword = CommonUtility.MD5Digest(disturb, newPassword);
        int row = 0;//kpiMonthlyMainDao.updatePassword(id, oldPassword, newPassword);
        logger.debug("[row] = " + row);
        return row > 0;
    }

    @Override
    public boolean userExist(String username) {return true;
    }

    @Override
    public boolean userExistExceptWithId(String username, Integer id) {
        return false;
    }

    @Override
    public boolean delete(Integer[] ids) {
        return true ;
    }

    @Override
    public List<KpiMonthlyMain> findAll() {
        return kpiMonthlyMainDao.findAll();
    }


    @Override
    public List<KpiMonthlyMain> findAll(KpiMonthlyMain condition) {
        return kpiMonthlyMainDao.findAll(QueryUtil.queryConditions(condition));
    }

    @Override
    public Page<KpiMonthlyMain> findPage(Page<KpiMonthlyMain> page) {
//        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//
//        org.springframework.data.domain.Page<KpiMonthlyMain> springPage = kpiMonthlyMainDao.findAll(null,pageable);
//        page.execute(Integer.valueOf(Long.toString(springPage
//                .getTotalElements())), page.getPageNum(), springPage.getContent());
//        return page;
        return null ;

    }
    /**
     * getIds()[0] 表是 处理人
     * @param condition
     * @param page
     * @return
     */
    @Override
    public Page<KpiMonthlyMain> findPage(final KpiMonthlyMain condition,Page<KpiMonthlyMain> page) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//TODO list pleanse 
        logger.debug("==> Find all kpiMonthlyMain by condition.");
        logger.debug("condition.getOwner().getId()="+condition.getOwner().getId());
        logger.debug("condition.getOwner().getName()="+condition.getOwner().getName());
        logger.debug("condition.getCurrentStatus()="+condition.getCurrentStatus());
        
        Specification<KpiMonthlyMain> spc = new Specification<KpiMonthlyMain>(){

			@Override
			public Predicate toPredicate(Root<KpiMonthlyMain> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				Path<KpiMonthlyMain> path= root.get("id");
				
				//创建子查询并声明返回类型是String类型  
				Subquery<KpiMonthlyMain> queryTwo=query.subquery(KpiMonthlyMain.class);
				//声明子查询的对象  
				Root<KpiHandleExchange> rootTwo = ((Root<KpiHandleExchange>)queryTwo.from(KpiHandleExchange.class));
				queryTwo.select(rootTwo.<KpiMonthlyMain>get("monthlyMain"));

				//查环节待处理人
				//TODO condition.owner.id 页面存在这里哈
				//     condition.owner.name 存环节 preTwo=cb.and(preTwo,);
				Predicate predicates = cb.conjunction();
				Predicate preTwo = cb.equal(rootTwo.get("handleUserId"), condition.getOwner().getId());
				if(CommonUtility.isNonEmpty(condition.getOwner().getName())){
					String segment = condition.getOwner().getName();
					if(segment.indexOf(",")!=-1){
						In<String> in = cb.in(rootTwo.get("segment").as(String.class));
						for(String x:segment.split(",")){
							in.value(x);
						}
						preTwo=cb.and(preTwo,in);
					}else{
						preTwo = cb.and(preTwo,cb.equal(rootTwo.get("segment").as(String.class), condition.getOwner().getName()));
					}
				}
				
				
				preTwo = cb.and(preTwo,cb.equal(rootTwo.get("segmentStatus").as(String.class), "pending"));
				preTwo = cb.and(preTwo,cb.equal(rootTwo.get("visitStatus").as(String.class), "Y"));
				preTwo = cb.and(preTwo,cb.equal(rootTwo.get("isDel").as(boolean.class), false));
				queryTwo.where(preTwo);

				predicates = cb.and(predicates,cb.in(path).value(queryTwo));

				if(CommonUtility.isNonEmpty(condition.getCurrentStatus())){
					predicates = cb.and(predicates,cb.equal(root.get("currentStatus").as(String.class), condition.getCurrentStatus()));
				}
				return query.where(predicates).getRestriction();
				
			}
        	
        };
        
        
        org.springframework.data.domain.Page<KpiMonthlyMain> springPage = kpiMonthlyMainDao.findAll(spc,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;

    }
    /**
     * 根据用户和开始时间查当 开始时间月的主信息id
     * @param userId
     * @param startDate
     * @return
     */
    public Integer getMainIdByUserIdAndStartDate(Integer userId,String startDate){

		KpiCycleHandle ff = new KpiCycleHandle();
		String newMonth= null;
		if(startDate!=null){
			newMonth = ff.pauseMonthyyyyMM(startDate);
		}

		KpiMonthlyMain mainCheck = kpiMonthlyMainDao.findKpiMonthlyMainByUserIdMonth(userId,newMonth);
		if(mainCheck!=null){
			return mainCheck.getId();
		}
		return null;
    }

	/**
	 * 修改考核周期字符串
	 * @param mainId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
    @Override
	public String updateMonth(Integer userId,Integer mainId,String startDate,String endDate){

    	String result = "";
		try{

			KpiCycleHandle ff = new KpiCycleHandle();
			
			KpiMonthlyMain main = kpiMonthlyMainDao.findById(mainId);
			
			String newMonth= null;
			if(startDate!=null){
				newMonth = ff.pauseMonthyyyyMM(startDate);
			}else if(endDate!=null){
				newMonth = ff.pauseMonthyyyyMM(endDate);
			}
//在开始时间中进行验证是否重复
			if(startDate!=null){
				KpiMonthlyMain mainCheck = kpiMonthlyMainDao.findKpiMonthlyMainByUserIdMonth(userId,newMonth);
				if(mainCheck!=null){
					//找到已经存在月份的记录了，就跳转到这个月份去吧。
					if(FlowName.selfShowInit.getCode().equals(mainCheck.getCurrentStatus())){//如果还在规划状态，修改一下考核周期
						mainCheck.setMonth(newMonth);
						mainCheck.setKpiCycle(ff.handle(main.getKpiCycle(),startDate, endDate));
						kpiMonthlyMainDao.save(main);
					}
					
					result = "你所选择的月份已经存在规划记录！"+mainCheck.getKpiCycle();
					result = "thisMonthExist="+mainCheck.getId();
					return result ;
				}
			}
			//在结束时间中验证是否   选择的开始时间和结束时间的月份是不相同的
			if(endDate!=null){
				//
				String oldStart = ff.pauseMonthyyyyMM(ff.getcycleSE(main.getKpiCycle())[0]);
				
				if(!oldStart.equals(ff.pauseMonthyyyyMM(endDate))){
					return "你所选择的结束日期与开始结果的月份不一致错误！";
				}
				
			}
			
			main.setMonth(newMonth);
			
			main.setKpiCycle(ff.handle(main.getKpiCycle(),startDate, endDate));
			
			kpiMonthlyMainDao.save(main);

			if(endDate!=null){
				result = "thisMonthExist="+main.getId();
				return result ;
			}
			return result ;
		}catch(Exception e){
			e.printStackTrace();
		}

		return result ;
	}

    @Override
    public Integer count() {
        return kpiMonthlyMainDao.count();
    }

    /**
     * 环节处理公共方法
     * @return
     */
    public boolean commonHandleLink(HttpServletRequest request){
//      审核指标确定
//      保存评分
//      这４个功能都是按mainId,linkName


//      String sId = request.getParameter("sId");//mainId
      String custom = request.getParameter("custom");//linkName
  	Object sIds = request.getParameter("sId");
  	logger.info("sIds="+sIds);
  	logger.info("custom="+custom);
  	Integer mainId = Integer.valueOf(String.valueOf(sIds));
      KpiMonthlyMain main = findKpiMonthlyMainById(mainId);
      Integer hisId = main.getOwner().getId();
      KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId) ;
      User he = main.getOwner();
      User me = getMe(request);
  		//查询基本信息
  		request.setAttribute("ownBase", userBase);
  		request.setAttribute("main", main);
  		
  		String parentOpinion = request.getParameter("opinion");//领导审核指标
  		
		// 查询流程
		List<KpiHandleExchange> dbFlows = kpiHandleExchangeService.findByMainId(mainId);
      
		List<KpiHandleExchange> nextspendings = new ArrayList<KpiHandleExchange>();//下一待办

		KpiHandleExchange thispending = null;//当前待办
		KpiHandleExchange thisdone = null;//当前已办
		
		KpiFlowChain flow = KpiFlow.getFlowMap().get(userBase.getPersonnelCategory());
		KpiFlowChainNode flowNode = flow.getHead();
		boolean isTailLink = false;
		/*当前处理 最后人吗*/
		boolean isFinally = false ;
		String thisSegment="",nextSegment="",nextSegmentMsg="";
		
		if (dbFlows != null && dbFlows.size() > 0) {
			//一次循环
			for (KpiHandleExchange dbFlow : dbFlows) {
				if ("Y".equals(dbFlow.getVisitStatus()) && me.getId().intValue()==dbFlow.getHandleUserId().intValue()) {//多个干系人处理
//					thispendings.add(dbFlow);
					thisSegment = dbFlow.getSegment();
					thispending = dbFlow;
					logger.info("当前处理环节:" + thispending.getSegment());
//当前处理都完毕了吗?
					isFinally = verifyIsFinall(dbFlows,thisSegment);
					// 循环下一个环节
					if (flow.searchNode(thispending.getSegment()).nextNode != null) {
						flowNode = flow.searchNode(thispending.getSegment()).nextNode;
						nextSegment= flowNode.current ;
						if(isFinally){
							for(FlowName n:FlowName.values()){//下一个环节待办提示语
								if(n.getCode().equals(nextSegment)){
									nextSegmentMsg = n.getMsg();
								}
							}
						}
						logger.info("下一处理环节:" + flowNode.current);
					}else{
						isTailLink= true ;
						logger.info("你是最后一个处理环节:" + thispending.getSegment());
					}
				}
			}

			if(parentReturnHandle(parentOpinion,dbFlows,FlowName.selfShowInit.getCode(),FlowName.selfShowInit.getMsg(),thispending,main)){
	  			return true;//领导审核指标 不通过处理
	  		}
			//二次循环 下一
			if(isFinally){
				for (KpiHandleExchange dbFlow : dbFlows) {
					if ("done".equals(dbFlow.getSegmentStatus()) && thisSegment.equals(dbFlow.getSegment())) {
						thisdone = dbFlow;
					}else if((!isTailLink)&&"pending".equals(dbFlow.getSegmentStatus()) && nextSegment.equals(dbFlow.getSegment())){
						//下一步的所有人通知到位
						nextspendings.add(dbFlow);
					}
				}
			}
		}
		try{
			int selfScores = 0 ;
			int stakeholderScores = 0 ;

			//当前已办 
			Optional<KpiHandleExchange> updThisDone = Optional.ofNullable(thisdone); //如果是干系人评分，前面评分的人，流程表中没有记录，
			KpiHandleExchange updThisToDone = updThisDone.orElse(
					dbFlows.stream().filter(e->"Y".equals(e.getVisitStatus()) && me.getId().intValue()==e.getHandleUserId().intValue()).findAny().get()
					);
			updThisToDone.setHandleTime(Calendar.getInstance().getTime());
			//当前待办 数据逻辑处理
			thispending.setVisitStatus("N");
			kpiHandleExchangeService.update(thispending);
			kpiHandleExchangeService.update(updThisToDone);//当前已办 
			
			if(isFinally){

				//主表状态修改
				if(!isTailLink){
					main.setCurrentStatus(nextSegment);
					main.setKpiStatus(nextSegmentMsg);
					main.setKpiType("正在进行");
					//上一步最后一人时 , 下一待办 数据逻辑处理  下一步的所有人通知到位
					if(isFinally){
						for(KpiHandleExchange bn:nextspendings){
							bn.setVisitStatus("Y");
							kpiHandleExchangeService.update(bn);
						}
					}
				}else{
					
					BigDecimal compositeScore = new BigDecimal(0) ;
					int generalManagerScore = 0 ;
					int parentScore = 0;
					if(main.getGeneralManagerScoring()!=null){
						generalManagerScore = main.getGeneralManagerScoring();
					}
					if(main.getDirectSuperiorScoring()!=null){
						parentScore = main.getDirectSuperiorScoring();
					}

					List<KpiMonthlyEntry> entrys = kpiMonthlyEntryService.findByMainId(mainId);
					for(KpiMonthlyEntry entry:entrys){
						selfScores += entry.getSelfScoring();
//							20171020zengmao修改,普通员工直接上级逐条评分
//							if(false == PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())){
							stakeholderScores += entry.getStakeholderScoring();
//							}
					}
//（二）得分计算规则
					if(PersonnelCategory.atThe.toString().equals(userBase.getPersonnelCategory())){
//部门正、副职月度绩效综合得分=自评分×5%+干系人评分×70%+分管领导评分×15%+总经理评分×10%
						compositeScore = (new BigDecimal(selfScores*5+stakeholderScores*70+parentScore*15+generalManagerScore*10)).divide(new BigDecimal(100.0)) ;
					}else if(PersonnelCategory.director.toString().equals(userBase.getPersonnelCategory())){
//室主任/主管月度绩效综合得分=自评分×5%+干系人评分×70%+部门经理评分×25%
						compositeScore = (new BigDecimal(selfScores*5 + stakeholderScores*70 + parentScore*25)).divide(new BigDecimal(100.0)) ;
					}else if(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())){
//普通员工月度绩效综合得分=自评分×20%+直接上级评分×80%
						compositeScore = (new BigDecimal(selfScores*20 + stakeholderScores*80)).divide(new BigDecimal(100.0)) ;
					}
					compositeScore.setScale(2);

					main.setStakeholderScoring(stakeholderScores);
					main.setSelfScoring(selfScores);
					main.setCompositeScoring(compositeScore);
//
//所有考核完成后，系统根据以上计算方法进行每个人的月度绩效考核分数计算
//（三）考核系数确定规则（该部分不在系统里计算）
					
					main.setCurrentStatus("kpiOver");
					main.setKpiStatus("已完成考核");
					main.setKpiType("已完成");
					

					KpiSummarySheet summary = new KpiSummarySheet();
					summary.setUserId(main.getOwner().getId());
					summary.setMonth(main.getMonth());
					summary.setDepartmentName(main.getOwner().getDepartment().getName());
					summary.setRealName(main.getOwner().getRealname());
					summary.setSelfScoring(main.getSelfScoring());
					if(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())){
						summary.setStakeholderScoring(null);
						summary.setDirectSuperiorScoring(main.getStakeholderScoring());
					}else{
						summary.setStakeholderScoring(main.getStakeholderScoring());
						summary.setDirectSuperiorScoring(main.getDirectSuperiorScoring());					
					}
					summary.setGeneralManagerScoring(main.getGeneralManagerScoring());
					summary.setCompositeScoring(main.getCompositeScoring());
					summary.setMonthlyMainId(main.getId());
					summary.setCreateTime(Calendar.getInstance().getTime());
					kpiSummarySheetService.add(summary);
				}
				update(main);
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
//			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw e ;
		}
      
			
      
//    主id和环节编码　做为入参执行动作：
//    １　当前环节的pending记录关闭状态，状态设置为N
//    ２　当前环境的done记录处理时间设置为当前时间
//    ３　下一环节　？？？的
//    　　　下一环节的pending打开访问状态
//
//    ４　下一环节的人员来打开页面时，
//    	将下一环节的pending记录处理时间。
//    总的来说：pending 状态先Y(上一环节的处理人done时:自已的pending变为N,自已的done记时间，下一环节的pending打开)

//      return ForwardUtility.forwardAdminView("/kpi/underlying/page_kpi_hislib_show");
  
		return true ;
     
    }

    @Override
    public List<KpiMonthlyMain> findByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiMonthlyMain> findParentByDepartmentIdAndRoleId(Integer departmentId, Integer roleId) {
        return null;
    }

    @Override
    public List<KpiMonthlyMain> findKpiMonthlyMainByParentId(Integer userId) {
        return null;
    }

    @Override
    public List<KpiMonthlyMain> getSolvers(KpiMonthlyMain kpiMonthlyMain) {
        return null;
    }

    //领导审核指标 不通过处理
    private boolean parentReturnHandle(String parentOpinion, List<KpiHandleExchange> dbFlows, String code, String msg,KpiHandleExchange thispending,KpiMonthlyMain main) {
		logger.info(parentOpinion);
    	if("N".equals(parentOpinion)){
			//领导环节关闭
			thispending.setVisitStatus("N");
			kpiHandleExchangeService.update(thispending);
			//上一环节打开 指标确认没有
			for(KpiHandleExchange previous:dbFlows){
				if(code.equals(previous.getSegment())){
					previous.setVisitStatus("Y");
		            kpiHandleExchangeService.update(previous);
				}
			}
			
			//主表状态修改
        	main.setKpiStatus(msg);
        	main.setCurrentStatus(code);
        	main.setKpiType("正在进行");
            update(main);
			return true ;
		}
		return false;
	}
    

	private User getMe(HttpServletRequest request){
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        Integer hisId = loginUser.getId();
        User he = userDao.findById(hisId);
        return he ;
    }
	/**
     * 是否本环节最后一人处理
     */
    private boolean verifyIsFinall(List<KpiHandleExchange> dbFlows, String thisSegment) {
		boolean isFinallyPerson = false ;
		long count=0;
		//20180306　出现直接领导和干系人同时待办的数据，原没有使用thisSegment的判断，现在添加上这个判断。
		count = dbFlows.stream().filter(bn->{
			return "Y".equals(bn.getVisitStatus()) && "pending".equals(bn.getSegmentStatus())
					&& thisSegment.equals(bn.getSegment());
			
		}).count();
		
		if(count==1){
			isFinallyPerson= true ;
		}
		return isFinallyPerson;
	}
}