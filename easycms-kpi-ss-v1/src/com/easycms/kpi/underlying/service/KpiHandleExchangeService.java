package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.domain.KpiLib;

import java.util.List;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午05:18:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiHandleExchangeService{

    /**
     * 查找所有
     * @return
     */
    List<KpiHandleExchange> findAll();

    List<KpiHandleExchange> findAll(KpiHandleExchange condition);
    List<KpiHandleExchange> findByMainId(Integer mainId);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiHandleExchange> findPage(Specifications<KpiHandleExchange> sepc,Pageable pageable);


    Page<KpiHandleExchange> findPage(Page<KpiHandleExchange> page);

    Page<KpiHandleExchange> findPage(KpiHandleExchange condition,Page<KpiHandleExchange> page);

    /**
     *
     * @return
     */
    KpiHandleExchange userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiHandleExchange findKpiHandleExchange(String username);

    /**
     * 根据ID查找用户
     * @param id
     * @return
     */
    KpiHandleExchange findKpiHandleExchangeById(Integer id);

    /**
     * 添加用户
     * @param kpiHandleExchange
     * @return
     */
    KpiHandleExchange add(KpiHandleExchange kpiHandleExchange);
    boolean addAll(List<KpiHandleExchange> kpiHandleExchanges);
    boolean addOrUpdateAll(List<KpiHandleExchange> kpiHandleExchanges);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiHandleExchange
     * @return
     */
    KpiHandleExchange update(KpiHandleExchange kpiHandleExchange);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiHandleExchange> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiHandleExchange> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiHandleExchange> findKpiHandleExchangeByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiHandleExchange 发起问题的kpiHandleExchange
     * @return
     */
    List<KpiHandleExchange> getSolvers(KpiHandleExchange kpiHandleExchange);

}