package com.easycms.kpi.underlying.domain;

import com.easycms.core.form.BasicForm;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Date;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午05:18:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_handle_exchange")
//@Cacheable
public class KpiHandleExchange extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;
    /** 考核主表id */
    @JsonIgnore
    @ManyToOne(targetEntity=KpiMonthlyMain.class,fetch=FetchType.LAZY)
    @JoinColumn(name="monthly_main_id")
    private KpiMonthlyMain monthlyMain;
    /** 处理用户id */
    @Column(name="handle_user_id")
    private Integer handleUserId;
    /** 环节 */
    @Column(name="segment")
    private String segment;
    /** 环节状态：pending待处理,done已处理*/
    @Column(name="segment_status")
    private String segmentStatus;
    /**访问状态：N待处理,Y已处理*/
    @Column(name="visit_status")
    private String visitStatus;
    /** 处理时间 */
    @Column(name="handle_time")
    private Date handleTime;
    /** 删除状态:1为已删除，0为有效 */
    @Column(name="is_del")
    private boolean isDel;

    public Integer getId() {
        return id;
    }
    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    public KpiMonthlyMain getMonthlyMain() {
		return monthlyMain;
	}
	public void setMonthlyMain(KpiMonthlyMain monthlyMain) {
		this.monthlyMain = monthlyMain;
	}
	public Integer getHandleUserId() {
        return handleUserId;
    }
    /**设置  处理用户id */
    public void setHandleUserId(Integer handleUserId) {
        this.handleUserId = handleUserId;
    }


    public void setDel(boolean isDel) {
		this.isDel = isDel;
	}
	public String getSegmentStatus() {
		return segmentStatus;
	}
	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}
	public String getVisitStatus() {
		return visitStatus;
	}
	public void setVisitStatus(String visitStatus) {
		this.visitStatus = visitStatus;
	}
	public String getSegment() {
        return segment;
    }
    /**设置  环节 */
    public void setSegment(String segment) {
        this.segment = segment;
    }

    public Date getHandleTime() {
        return handleTime;
    }
    /**设置  处理时间 */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public boolean getIsDel() {
        return isDel;
    }
    /**设置  删除状态:1为已删除，0为有效 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }

}