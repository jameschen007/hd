package com.easycms.kpi.underlying.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-20 下午05:15:10<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_user_base")
//@Cacheable
public class KpiUserBase extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;

    /** 用户id */
    @JoinColumn(name = "user_id")
    @ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch = FetchType.LAZY)
    private User user;
    /** 人员类别atThe：：部门正、副职，director：：室主任/主管，employees：：普通员工， */
    @Column(name="personnel_category")
    private String personnelCategory;
    /** 干系人_用户id */
    @JsonIgnore
    @JoinColumn(name = "stakeholder_id")
    @ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch = FetchType.LAZY)
    private User stakeholder;
    /** 直接上级_用户id */
    @JsonIgnore
    @JoinColumn(name = "immediate_superior_id")
    @ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch = FetchType.LAZY)
    private User immediateSuperior;

    /** 备注 */
    @Column(name="mark")
    private String mark;
    /** 删除状态 */
    @Column(name="is_del")
    private boolean isDel;
    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;

    public Integer getId() {
        return id;
    }
    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }
    /**设置  用户id */
    public void setUser(User user) {
        this.user = user;
    }

    public String getPersonnelCategory() {
        return personnelCategory;
    }
    /**设置  人员类别atThe：：部门正、副职，director：：室主任/主管，employees：：普通员工， */
    public void setPersonnelCategory(String personnelCategory) {
        this.personnelCategory = personnelCategory;
    }

    public User getStakeholder() {
        return stakeholder;
    }
    /**设置  干系人_用户id */
    public void setStakeholder(User stakeholder) {
        this.stakeholder = stakeholder;
    }

    public User getImmediateSuperior() {
        return immediateSuperior;
    }
    /**设置  直接上级_用户id */
    public void setImmediateSuperior(User immediateSuperior) {
        this.immediateSuperior = immediateSuperior;
    }

    public String getMark() {
        return mark;
    }
    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }

    public boolean getIsDel() {
        return isDel;
    }
    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }
    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}