package com.easycms.kpi.underlying.domain;

import com.easycms.core.form.BasicForm;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>所有人员指标库，多表查询		kpi_lib<br>
 * <b>创建时间：</b>2017-08-23 上午11:11:51<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiUsersLibList extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;

    /** 用户id */
    private Integer userId;
    /** 用户名 */
    private String realName;
    /** 人员类别 */
    private String personnelCategory;
    /** 直接上级 */
    private String parentName;
    /** 所属部门 */
    private String departmentName;
    /** 岗位 */
    private String postName;
    /** 干系人 */
    private String stakeholder;
    /** 指标类型 */
    private String kpiType;
    /** 指标名称 */
    private String kpiName;

    public Integer getUserId() {
        return userId;
    }
    /**设置  用户id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRealName() {
        return realName;
    }
    /**设置  用户名 */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPersonnelCategory() {
        return personnelCategory;
    }
    /**设置  人员类别 */
    public void setPersonnelCategory(String personnelCategory) {
        this.personnelCategory = personnelCategory;
    }

    public String getParentName() {
        return parentName;
    }
    /**设置  直接上级 */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }
    /**设置  所属部门 */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getPostName() {
        return postName;
    }
    /**设置  岗位 */
    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getStakeholder() {
        return stakeholder;
    }
    /**设置  干系人 */
    public void setStakeholder(String stakeholder) {
        this.stakeholder = stakeholder;
    }

    public String getKpiType() {
        return kpiType;
    }
    /**设置  指标类型 */
    public void setKpiType(String kpiType) {
        this.kpiType = kpiType;
    }

    public String getKpiName() {
        return kpiName;
    }
    /**设置  指标名称 */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

}
