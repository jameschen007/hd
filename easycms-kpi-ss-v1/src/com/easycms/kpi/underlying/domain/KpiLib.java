package com.easycms.kpi.underlying.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-20 下午05:15:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_lib")
//@Cacheable
public class KpiLib extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;
    /** 指标名称 */
    @Column(name="kpi_name")
    private String kpiName;
    /** 用户id */
    @JsonIgnore
    @ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    /** 干系人_用户id */
    @JsonIgnore
    @ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "stakeholder")
    private User stakeholder;
    /** 指标类型:general 通用指标，purpose 岗位指标 */
    @Column(name="kpi_type")
    private String kpiType;
    /** 使用类型:import 导入类型，mine 我的类型 */
    @Column(name="use_type")
    private String useType;
    /** 删除状态 */
    @Column(name="is_del")
    private boolean isDel;
    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;

    public Integer getId() {
        return id;
    }
    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }

    public String getKpiName() {
        return kpiName;
    }
    /**设置  指标名称 */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public User getUser() {
        return user;
    }
    /**设置  用户id */
    public void setUser(User userId) {
        this.user = userId;
    }

    public User getStakeholder() {
        return stakeholder;
    }
    /**设置  干系人_用户id */
    public void setStakeholder(User stakeholder) {
        this.stakeholder = stakeholder;
    }

    public String getKpiType() {
        return kpiType;
    }
    /**设置  指标类型:general 通用指标，purpose 岗位指标 */
    public void setKpiType(String kpiType) {
        this.kpiType = kpiType;
    }

    public String getUseType() {
        return useType;
    }
    /**设置  使用类型:import 导入类型，mine 我的类型 */
    public void setUseType(String useType) {
        this.useType = useType;
    }

    public boolean getDel() {
        return isDel;
    }
    /**设置  删除状态 */
    public void setDel(boolean isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }
    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}