package com.easycms.kpi.underlying.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.kpi.underlying.form.KpiHandleExchangeForm;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午07:36:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/handle/exchange")
public class KpiHandleExchangeController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiHandleExchangeController.class);

    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;

    /**
     * 核电绩效流转状态表列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String list(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiHandleExchangeForm kpiHandleExchangeForm) {
        logger.debug("==> Start show KpiHandleExchange list.");
        return ForwardUtility.forwardAdminView("/performanceAppraisal/list_handle_exchange");
    }

    /**
     * 核电绩效流转状态表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String dataList(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiHandleExchangeForm kpiHandleExchangeForm) {
        kpiHandleExchangeForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiHandleExchange data list.");
        kpiHandleExchangeForm.setFilter(CommonUtility.toJson(kpiHandleExchangeForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiHandleExchangeForm));
        String returnString = ForwardUtility
                .forwardAdminView("/performanceAppraisal/data/data_handle_exchange");
        return returnString;
    }

    /**
     * 新增-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addUI(HttpServletRequest request, HttpServletResponse response) {

        String returnString = ForwardUtility
                .forwardAdminView("/user-group/modal_handle_exchange_add");
        return returnString;
    }

    /**
     * 保存新增-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(HttpServletRequest request, HttpServletResponse response,
                    @ModelAttribute("kpiHandleExchangeForm") @Valid KpiHandleExchangeForm kpiHandleExchangeForm,
                    BindingResult errors) {
        try {
//			registryExecutor.execute(kpiHandleExchangeForm, errors, request, response);
        } catch (Exception e) {
            logger.error(e);
        }

    }

    /**
     * 查看-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String show(@RequestParam String id, HttpServletRequest request,
                       HttpServletResponse response) {
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - start");
        }

        request.setAttribute("id", id);
        String returnString = ForwardUtility
                .forwardAdminView("/template/frames/module_users/kpiHandleExchange/handle_exchange_show");
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - end");
        }
        return returnString;
    }

    /**
     * 修改-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUI(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") KpiHandleExchangeForm form) {
        return ForwardUtility.forwardAdminView("/user-group/modal_handle_exchange_edit");
    }

    /**
     * 保存修改-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") KpiHandleExchangeForm form) {
        if (logger.isDebugEnabled()) {
            logger.debug("==> Start save edit handle_exchange.");
        }

        // #########################################################
        // 校验提交信息
        // #########################################################
        if (form.getId() == null) {
            HttpUtility.writeToClient(response, "false");
            logger.debug("==> Save edit handle_exchange failed.");
            logger.debug("==> End save edit handle_exchange.");
            return;
        }

        // #########################################################
        // 校验通过
        // #########################################################
        Integer id = form.getId();
//		KpiHandleExchange user = userService.findKpiHandleExchangeById(id);
//		user.setName(form.getName());
//		user.setRealname(form.getRealname());
//		user.setEmail(form.getEmail());
//		userService.update(user);
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End save edit handle_exchange.");
        return;
    }

    /**
     * 批量删除-核电绩效流转状态表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/delete")
    public void batchDelete(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiHandleExchangeForm form) {
        logger.debug("==> Start delete handle_exchange.");

        Integer[] ids = form.getIds();
        logger.debug("==> To delete handle_exchange [" + CommonUtility.toJson(ids) + "]");
        if (ids != null && ids.length > 0) {
//			userService.delete(ids);
        }
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End delete handle_exchange.");
    }
}