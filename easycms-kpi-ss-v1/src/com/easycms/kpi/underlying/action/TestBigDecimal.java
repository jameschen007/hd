package com.easycms.kpi.underlying.action;

import java.math.BigDecimal;

import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.service.impl.sys.PersonnelCategory;

public class TestBigDecimal {

	public static void main(String[] args) {
		 KpiUserBase userBase = new KpiUserBase();
		 userBase.setPersonnelCategory(PersonnelCategory.atThe.toString());

		BigDecimal compositeScore = new BigDecimal(0) ;
		Integer selfScores = 89;
		Integer stakeholderScores = 87;
		Integer parentScore = 96;
		Integer generalManagerScore = 77;

		if(PersonnelCategory.atThe.toString().equals(userBase.getPersonnelCategory())){
//部门正、副职月度绩效综合得分=自评分×5%+干系人评分×70%+分管领导评分×15%+总经理评分×10%
			compositeScore = (new BigDecimal(selfScores*5+stakeholderScores*70+parentScore*15+generalManagerScore*10)).divide(new BigDecimal(100.0)) ;
		}else if(PersonnelCategory.director.toString().equals(userBase.getPersonnelCategory())){
//室主任/主管月度绩效综合得分=自评分×5%+干系人评分×70%+部门经理评分×25%
			compositeScore = (new BigDecimal(selfScores*5 + stakeholderScores*70 + parentScore*25)).divide(new BigDecimal(100.0)) ;
		}else if(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())){
//普通员工月度绩效综合得分=自评分×20%+直接上级评分×80%
			compositeScore = (new BigDecimal(selfScores*20 + stakeholderScores*80)).divide(new BigDecimal(100.0)) ;
		}
		compositeScore.setScale(2);
							System.out.println(compositeScore);
	}

}
