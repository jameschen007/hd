package com.easycms.kpi.underlying.action;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.form.KpiUserBaseForm;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-16 下午11:37:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/base")
public class KpiUserBaseController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiUserBaseController.class);

    @Autowired
    private KpiUserBaseService kpiUserBaseService;

    @Autowired
    private UserService userService;
    /**
     * 用户绩效基础信息列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String list(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiUserBaseForm kpiUserBaseForm) {
        logger.debug("==> Start show KpiUserBase list.");
        return ForwardUtility.forwardAdminView("/performanceAppraisal/list_kpi_base");
    }

    /**
     * 用户绩效基础信息-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String dataList(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiUserBaseForm kpiUserBaseForm) {
        kpiUserBaseForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiUserBase data list.");
        kpiUserBaseForm.setFilter(CommonUtility.toJson(kpiUserBaseForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiUserBaseForm));
        String returnString = ForwardUtility
                .forwardAdminView("/performanceAppraisal/data/data_kpi_base");
        return returnString;
    }

    /**
     * 新增-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addUI(HttpServletRequest request, HttpServletResponse response) {

        String returnString = ForwardUtility
                .forwardAdminView("/user-group/modal_kpi_base_add");
        return returnString;
    }

    /**
     * 保存新增-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(HttpServletRequest request, HttpServletResponse response,
                    @ModelAttribute("kpiUserBaseForm") @Valid KpiUserBaseForm kpiUserBaseForm,
                    BindingResult errors) {
        //新增绩效　　类别／岗位信息
        try {

            User he = userService.findUserById(kpiUserBaseForm.getUser().getId());

            KpiUserBase base = new KpiUserBase();
            base.setUser(he);
            base.setPersonnelCategory(kpiUserBaseForm.getPersonnelCategory());
            base.setImmediateSuperior(he.getParent());
            //删除了，不用在基本信息中存岗位了。
            kpiUserBaseService.add(base);
            //准备页面数据
            request.setAttribute("userBase", base);
            request.setAttribute("sId", he.getId());
            HttpUtility.writeToClient(response, "true");
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
        }
        HttpUtility.writeToClient(response, "false");
    }

    /**
     * 查看-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String show(@RequestParam String id, HttpServletRequest request,
                       HttpServletResponse response) {
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - start");
        }

        request.setAttribute("id", id);
        String returnString = ForwardUtility
                .forwardAdminView("/template/frames/module_users/kpiUserBase/kpi_base_show");
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - end");
        }
        return returnString;
    }

    /**
     * 修改-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUI(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") KpiUserBaseForm form) {
        return ForwardUtility.forwardAdminView("/user-group/modal_kpi_base_edit");
    }

    /**
     * 保存修改-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") KpiUserBaseForm form) {
        if (logger.isDebugEnabled()) {
            logger.debug("==> Start save edit kpi_base.");
        }

        // #########################################################
        // 校验提交信息
        // #########################################################
        if (form.getId() == null) {
            HttpUtility.writeToClient(response, "false");
            logger.debug("==> Save edit kpi_base failed.");
            logger.debug("==> End save edit kpi_base.");
            return;
        }

        // #########################################################
        // 校验通过
        // #########################################################
        Integer id = form.getId();
//		KpiUserBase user = userService.findKpiUserBaseById(id);
//		user.setName(form.getName());
//		user.setRealname(form.getRealname());
//		user.setEmail(form.getEmail());
//		userService.update(user);
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End save edit kpi_base.");
        return;
    }

    /**
     * 批量删除-用户绩效基础信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/delete")
    public void batchDelete(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiUserBaseForm form) {
        logger.debug("==> Start delete kpi_base.");

        Integer[] ids = form.getIds();
        logger.debug("==> To delete kpi_base [" + CommonUtility.toJson(ids) + "]");
        if (ids != null && ids.length > 0) {
//			userService.delete(ids);
        }
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End delete kpi_base.");
    }
}