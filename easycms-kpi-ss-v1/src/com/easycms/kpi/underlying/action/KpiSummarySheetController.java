package com.easycms.kpi.underlying.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.form.KpiSummarySheetForm;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/summary/sheet")
public class KpiSummarySheetController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiSummarySheetController.class);

    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;

    /**
     * 核电人员月度绩效结果汇总表列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String list(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiSummarySheetForm kpiSummarySheetForm) {
    	

    	Calendar c = Calendar.getInstance();

    	List<Integer> yyyy = new  ArrayList<Integer>();
    	List<Integer> mm = new  ArrayList<Integer>();
    	
    	int currentYYYY = c.get(Calendar.YEAR);
    	for(int i=-2;i<1;i++){
    		yyyy.add(currentYYYY+i);
    	}
    	request.setAttribute("yyyys", yyyy);
    	for(int i=1;i<13;i++){
    		mm.add(i);
    	}
    	request.setAttribute("mms", mm);
    	
    	
        logger.debug("==> Start show KpiSummarySheet list.");
        return ForwardUtility.forwardAdminView("/kpi/score/list_kpi_summarySheet");
    }

    /**
     * 核电人员月度绩效结果汇总表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String dataList(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
        
        String idStr = WebUtility.getUserIdFromSession(request);
        if(CommonUtility.isNonEmpty(idStr)){
        	Integer loginId = Integer.parseInt(idStr);        	
        	kpiSummarySheetForm.setLoginId(loginId);
        }

        logger.debug("==> Show KpiSummarySheet data list.");
        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/data/data_kpi_summarySheet");
        return returnString;
    }


    /**
     * 查看-核电人员月度绩效结果汇总表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String show(@RequestParam String id, HttpServletRequest request,
                       HttpServletResponse response) {
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - start");
        }

        request.setAttribute("id", id);
        String returnString = ForwardUtility
                .forwardAdminView("/template/frames/module_users/kpiSummarySheet/summary_sheet_show");
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - end");
        }
        return returnString;
    }
    
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public String dataListCurrent(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiSummarySheet data list.");
        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));
        
        List<KpiSummarySheet> kpiCurrentSheet =  kpiSummarySheetService.getCurrentSheet();
        request.setAttribute("kpiCurrentSheetList", kpiCurrentSheet);

        
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
        return returnString;
    }

/**
 * 管理员删除绩效考核
 * @param request
 * @param response
 * @param mainId
 * @return
 */
    @RequestMapping(value = "/deleteByMainId", method = RequestMethod.GET)
    public void dataListCurrent(HttpServletRequest request,
                           HttpServletResponse response, 
                           @RequestParam Integer mainId) {
    	
    	boolean f = kpiSummarySheetService.deleteByMainId(mainId);
    	  HttpUtility.writeToClient(response, f+"");
    
    }
    /**
     * 管理员删除历史绩效考核
     * @param request
     * @param response
     * @param mainId
     * @return
     */
        @RequestMapping(value = "/deleteHistoryByMainId", method = RequestMethod.GET)
        public void deleteHistory(HttpServletRequest request,
                               HttpServletResponse response, 
                               @RequestParam Integer mainId) {
        	
        	boolean f = kpiSummarySheetService.deleteHisByMainId(mainId);
        	  HttpUtility.writeToClient(response, f+"");
        
        }

    	@RequestMapping(value = "/report", method = {RequestMethod.POST,RequestMethod.GET})
    	public void statisticalReportPost(HttpServletRequest request, HttpServletResponse response){
    		String mimetype = "application/vnd.ms-excel;charset=utf-8";
        	response.setContentType(mimetype);
        	String filename = "绩效考核汇总";
        	filename += ".xls";
        	String headerValue = "attachment;";
        	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
        	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
        	response.setHeader("Content-Disposition", headerValue);
        	kpiSummarySheetService.statisticalReport(request, response);
    	}
}