package com.easycms.kpi.underlying.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.kpi.underlying.form.KpiMonthlyEntryForm;
import com.easycms.kpi.underlying.form.KpiMonthlyMainForm;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi")
public class KpiPendingController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiPendingController.class);

    @Autowired
    private UserService userService;

    /**
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/verify/pending", method = RequestMethod.GET)
    public String verifypending(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyEntryForm kpiMonthlyEntryForm) {
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        
        request.setAttribute("pendinglist", "verify");
        
        logger.debug("==> Start show KpiMonthlyEntry list.");
        return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_pending");
    }
    /**
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/score/pending", method = RequestMethod.GET)
    public String scorepending(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyEntryForm kpiMonthlyEntryForm) {
        request.setAttribute("pendinglist", "score");
        return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_pending");
    }
    
    public KpiMonthlyMainForm commonLogic(HttpServletRequest request, HttpServletResponse response
            ,KpiMonthlyMainForm kpiMonthlyMainForm,String inCustom) {
    	boolean f = false ;
    	kpiMonthlyMainForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiMonthlyEntry data list.");

        User u = new User();
        User he = getHe(request);
        String custom = request.getParameter("custom");
        if(inCustom!=null && !"".equals(inCustom)){
        	custom = inCustom ;
        }
        u.setId(he.getId());
        u.setName(custom);
        kpiMonthlyMainForm.setOwner(u);
        kpiMonthlyMainForm.setFilter(CommonUtility.toJson(kpiMonthlyMainForm));

//        待审考核列表
//
//        待评考核列表
//        这两个列表都是直接根据当前登录用户取出，查询出待审列表出来。
//        主要是需要查询主表并且子查询子表过滤当前登录人，并且查询访问打开状态的记录
        
        return kpiMonthlyMainForm;        
    }

    /**
     * 核电员工月度考核子表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "/verify/pending", method = RequestMethod.POST)
    public String dataList1(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiMonthlyMainForm kpiMonthlyMainForm) {
    	String segment = FlowName.parentOk.getCode();
    	kpiMonthlyMainForm = commonLogic(request,response,kpiMonthlyMainForm,segment);
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiMonthlyMainForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/data/data_kpi_pending");
        return returnString;
    }
    /**
     * 核电员工月度考核子表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "/score/pending", method = RequestMethod.POST)
    public String dataList2(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiMonthlyMainForm kpiMonthlyMainForm) {
    	String segment = "";
    	String not = FlowName.parentOk.getCode() ;
    	String not2 = FlowName.selfScore.getCode() ;
    	
    	for(FlowName e:FlowName.values()){
    		if(not.equals(e.getCode())||not2.equals(e.getCode())){
    		}else{
    			segment=segment+","+e.getCode();
    		}
    	}
    	//查询待评分环节
    	kpiMonthlyMainForm = commonLogic(request,response,kpiMonthlyMainForm,segment.substring(1));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiMonthlyMainForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/data/data_kpi_pending");
        return returnString;
    }

    private User getHe(HttpServletRequest request){
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        Integer hisId = loginUser.getId();
        User he = userService.findUserById(hisId);
        return he ;
    }
}