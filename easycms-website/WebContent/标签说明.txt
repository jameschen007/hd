标签说明:

自定义标签可以继承com.easycms.basic.BaseDirective<T>，实现其对应的方法即可，再在/WEB-INF/conf/directive/directive.xml里面配置就可以用
-------------------------------------------------------------------
1.[@ec_user;param/]:
-------------------------------------------------------------------
说明：
	用户标签
参数:
  - queryType:查询类型,4种可选，list/field/tree/count，默认值:field
  - id:序号，在queryType为field下生效
  - name:用户名，在queryType为field下生效
  - sort:排序规则
  - order:升序/降序
  - pageable:是否分页,0为不分页，1为分页
  - pageNum:页数
  - pagesize：每页条数
  - search:查询条件，JSON字符串
变量：
	param:用户自定义变量名来接受返回的值
	
-------------------------------------------------------------------
2.[@ec_role;param/]:
-------------------------------------------------------------------
说明：
	角色标签
参数:
  - queryType:查询类型,默认值:query,可选，count/search
  - id:序号，在queryType为query下生效
  - name:角色名
  - sort:排序规则
  - order:升序/降序
  - pageable:是否分页,0为不分页，1为分页
  - pageNum:页数
  - pagesize：每页条数
  - search:查询条件，JSON字符串,
变量：
	param:用户自定义变量名来接受返回的值
	
-------------------------------------------------------------------
3.[@ec_privilege;param/]:
-------------------------------------------------------------------
说明：
	权限标签
参数:
  - queryType:查询类型,默认值:query,可选，count/search/tree
  - id:序号，在queryType为query下生效
  - parentId:父级序号，在queryType为query下生效
  - name:权限名
  - uri:权限的URI
  - sort:排序规则
  - order:升序/降序
  - pageable:是否分页,0为不分页，1为分页
  - pageNum:页数
  - pagesize：每页条数
  - search:查询条件，JSON字符串,
变量：
	param:用户自定义变量名来接受返回的值
-------------------------------------------------------------------
4.[@ec_privilege_check/]:
-------------------------------------------------------------------
说明：
	权限检查标签，如果此权限合法则显示标签对里的内容，并且可以通过"AUTH_MENU"变量来使用此经过验证的权限
参数:
  - name:权限名
  - uri:权限的URI
	


-------------------------------------------------------------------
使用:
-------------------------------------------------------------------
1.当queryType为query：
	ID有值时，会查询出该ID下对应的用户信息，返回用户对象User,优先级最高
	username有值时，会查询该username的信息，返回用户对象User，优先级低于ID
	sort和order一般成对使用，用来排序
2.queryType为count：
	会返回所有个数
3.queryType为tree：
	若是具有父子级关系的数据，会返回树形结构的数据；
4.queryType为search：
	需要search参数，search参数为JSON字符串，如果继承BasicForm会有set/getSearch方法，需要在controller内写入form.setSearch(CommonUtility.toJson(form));
	实例代码：
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") UserForm form) {
		form.setSearch(CommonUtility.toJson(form));
		....
		return null;
	}
	
	在页面上使用form.search放入标签的search字段，实例如下：
	[@ec_user 
		queryType = "${form.queryType!}" 
		search = "${form.search!}"
		order = "${form.order!'desc'}"
		sort = "${form.sort!}"
	;list]
	[/@ec_user]
	