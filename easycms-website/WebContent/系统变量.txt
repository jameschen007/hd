1.当前站点变量
${LOCAL_WEBSITE}
属性:
 - sitename:站点名称
 - domain:站点域名
 - keywords:关键字
 - describe:描述
 - siteTplName:使用模板名称
 - uploadMaxSize:上传文件最大值
 - uploadTplDir:上传文件目录
 - tplFileType:上传文件类型
 - uploadResDir:资源上传目录
 - pagesizeList:分页大小列表
 
2.当前URI
${URI}
3.上下文路径
${path}
4.时间戳
${timestamp}
5.当前菜单路径
${menuLink}
6.父菜单路径
${topLink}
7.当前权限路径
${current_uri}