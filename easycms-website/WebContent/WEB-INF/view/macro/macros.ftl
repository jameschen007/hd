


[#-- 当前位置--]
[#macro breadcrumbs separator="&gt;"]
[#compress]
[@ec_location;list]
<div id="breadcrumbs">
<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i><a href="${path}/management/index">首页</a>
            <span class="divider"><i class="icon-angle-right"></i></span>
        </li>

[#if list?? && (list?size>0)]
	[#list list as p]
        <li >${p.name!}</li>
		[#if p_index != (list?size-1)]
            <span class="divider"><i class="icon-angle-right"></i></span>
		[/#if]
	[/#list]
[/#if]
</ul>
</div>
[/@ec_location]
[/#compress]
[/#macro]


[#-- 当前位置--]
[#macro location separator="&gt;"]
[#compress]
[@ec_location;list]
[#if list?? && (list?size>0)]
[#-- onclick="openTab('${p.name}','${path}${p.uri!}','${p.icon!}');" --]
首页&nbsp;${separator}&nbsp;
[#list list as p]
<a href="javascript:void(0);"  >${p.name!}</a>
[#if p_index != (list?size-1)]
&nbsp;${separator}&nbsp;
[/#if]
[/#list]
[/#if]
[/@ec_location]
[/#compress]
[/#macro]

[#--分页宏命令--]
[#macro ec_page_pannel pagesize="10" pageNum=1 start=1 end=1 pageCount=1 totalCounts=1 url="#" params=""]
[#if params ==""]
	[#assign p = "" /]
[#else]
	[#assign p = "&${params?replace(' ','')}" /]
[/#if]
[#if (totalCounts?number > 0)]
<script type="text/javascript">
<!--
function selectPage(e) {
	var url = "${url}?pageNum=1${p}";
	url = url + "&pagesize=" + $(e).val();
	refreshData(url);
}
//-->
</script>
<div class="EC-page-pannel">
      <div class="EC-page-info">
                    <span>当前第<em>${pageNum}</em>页</span>
                    <span>共<em>${pageCount}</em>页,<em>${totalCounts}</em>条记录</span>
                    <span>每页显示<em>
			<select onchange="selectPage(this);">
				[#if pagesize == "10"]
				<option value="10" selected="selected">10</option>
				[#else]
				<option value="10">10</option>
				[/#if]
				
				[#if pagesize == "20"]
				<option value="20" selected="selected">20</option>
				[#else]
				<option value="20">20</option>
				[/#if]
				
				[#if pagesize == "30"]
				<option value="30" selected="selected">30</option>
				[#else]>
				<option value="30">30</option>
				[/#if]
				
				[#if pagesize == "60"]
				<option value="60" selected="selected">60</option>
				[#else]>
				<option value="60">60</option>
				[/#if]
				
				[#if pagesize == "${totalCounts}"]
				<option value="${totalCounts}" selected="selected">全部</option>
				[#else]
				<option value="${totalCounts}">全部</option>
				[/#if]
			</select>
			</em>条记录</span>
      </div>
	<ul class="EC-page-list">
    	[#if (pageNum?number > 1)]
		<li><a href="javascript:void(0);" onclick="refreshData('${url}?pagesize=${pagesize}&pageNum=1${p}');"><<</i></a></li>
		<li><a href="javascript:void(0);" onclick="refreshData('${url}?pagesize=${pagesize}&pageNum=${pageNum?number - 1}${p}');"><</i></a></li>
		[/#if]
	[#list (start?number)..(end?number) as num]
		[#if (pageNum?number == num)]
		<li><a href="javascript:void(0);" class="EC-page-on">${num}</a></li>
		[#else]
		<li><a href="javascript:void(0);" onclick="refreshData('${url}?pagesize=${pagesize}&pageNum=${num}${p}');">${num}</a></li>
		[/#if]
	[/#list]
		[#if (pageNum?number < pageCount?number)]
		<li><a href="javascript:void(0);" onclick="refreshData('${url}?pagesize=${pagesize}&pageNum=${pageNum?number + 1}${p}');">></a></li>
		<li><a href="javascript:void(0);" onclick="refreshData('${url}?pagesize=${pagesize}&pageNum=${pageCount}${p}');">>></a></li>
		[/#if]
	</ul>
</div>
[/#if]
[/#macro]

[#-- 编辑宏命令 --]
[#macro ec_editable type='js']
[#switch type]
	[#case 'js']
	[#nested]
	[#break]
	
	[#case 'css']
	[#nested]
	[#break]
	
	[#case 'txt']
	[#nested]
	[#break]
	
	[#case 'html']
	[#nested]
	[#break]
	
	[#case 'shtml']
	[#nested]
	[#break]
	
	[#case 'xml']
	[#nested]
	[#break]
	
	[#case 'jsp']
	[#nested]
	[#break]
	
	[#case 'properties']
	[#nested]
	[#break]
[/#switch]
[/#macro]

[#-- 判断图片宏命令 --]
[#macro ec_pic type='jpg']
[#switch type]
	[#case 'jpg']
	[#nested]
	[#break]
	
	[#case 'jpeg']
	[#nested]
	[#break]
	
	[#case 'gif']
	[#nested]
	[#break]
	
	[#case 'png']
	[#nested]
	[#break]
	
	[#case 'bmp']
	[#nested]
	[#break]
[/#switch]
[/#macro]

[#-- 根据文件类型显示小图标宏命令 --]
[#macro ec_icon type='unknown']
[#switch type]
	[#case 'asp']
	asp
	[#break]
	
	[#case 'aspx']
	aspx
	[#break]
	
	[#case 'bmp']
	bmp
	[#break]
	
	[#case 'css']
	css
	[#break]
	
	[#case 'db']
	db
	[#break]
	
	[#case 'doc']
	doc
	[#break]
	
	[#case 'docx']
	doc
	[#break]
	
	[#case 'exe']
	exe
	[#break]
	
	[#case 'flv']
	flv
	[#break]
	
	[#case 'folder']
	folder
	[#break]
	
	[#case 'gif']
	gif
	[#break]
	
	[#case 'html']
	html
	[#break]

	[#case 'jpg']
	jpg
	[#break]
	
	[#case 'jpeg']
	jpeg
	[#break]
	
	[#case 'js']
	js
	[#break]
	
	[#case 'jsp']
	jsp
	[#break]
	
	[#case 'mov']
	mov
	[#break]
	
	[#case 'mp3']
	mp3
	[#break]
	
	[#case 'mp4']
	mp4
	[#break]
	
	[#case 'none']
	none
	[#break]
	
	[#case 'pdf']
	pdf
	[#break]
	
	[#case 'php']
	php
	[#break]
	
	[#case 'png']
	png
	[#break]
	
	[#case 'rar']
	rar
	[#break]
	
	[#case 'rm']
	rm
	[#break]
	
	[#case 'shtml']
	shtml
	[#break]
	
	[#case 'sql']
	sql
	[#break]
	
	[#case 'swf']
	swf
	[#break]
	
	[#case 'txt']
	txt
	[#break]
	
	[#case 'unknow']
	unknow
	[#break]
	
	[#case 'wmp']
	wmp
	[#break]
	
	[#case 'wmv']
	wmv
	[#break]
	
	[#case 'xls']
	xls
	[#break]
	
	[#case 'xlsx']
	xls
	[#break]
	
	[#case 'zip']
	zip
	[#break]
	
	[#default]
	unknown
[/#switch]
[/#macro]

[#-- 截取文字长度 --]
[#macro ec_text_max length=50 value="" suffix="......"][#if value?length <= length ]${value}[#else]${value?substring(0,length)}${suffix}[/#if][/#macro]

[#-- 计算文件大小,若不足1K按B计算，若大于1024KB按MB算，若大于1024MB按G算，若大于1024G按T算 --]
[#macro ec_size value=""]
[#if value?? ]
	[#assign num = value?number /]

	[#-- 若不足1KB(1024)则显示B --]
	[#if num < (1024)]${num} B
	[#-- 若小于1MB(1024*1024)则显示KB --]
	[#elseif num < (1024*1024)]
		[#assign val = num/(1024) /]${val} KB
	[#-- 若大于1MB(1024*1024)小于1G(1024*1024*1024)则显示MB --]
	[#elseif (num > (1024*1024)) && (num < (1024*1024*1024))]	
		[#assign val = num/(1024*1024) /]${val} MB
	[/#if]
[/#if]
[/#macro]

[#-- 选择框树形节点 --]
[#macro ec_select_tree nodes selectId attr="" level=0 ]
[#if nodes?? && (nodes?size > 0)]
 [#list nodes as node]
		[#assign on='']
		[#if selectId?is_number]
			[#if selectId?? && (node.id == selectId)]
				[#assign on='selected="selected"']
			[/#if]
		[#else]
			[#list selectId?split(",") as sid]
			[#if selectId?? && (node.id?string == sid)]
				[#assign on='selected="selected"']
			[/#if]
			[/#list]
		[/#if]
		[#if attr?? && attr != "" && (node[attr])?? ]
			[#if node[attr]?is_boolean]
				[#assign dataAttr='data-${attr}="${node[attr]?string}"'/]
			[#else]
				[#assign dataAttr='data-${attr}="${node[attr]?string}"'/]
			[/#if]
		[/#if]
		<option value="${node.id!}" ${on!} ${dataAttr!}>[#if level>0][#list 0..level as index]&nbsp;&nbsp;[/#list]┣[/#if]${node["name"]!}</option>
		[@ec_select_tree nodes=node.children! level=level+1 selectId=selectId attr=attr /]
 [/#list]
[/#if]
[/#macro]

[#-- 图标显示 --]
[#macro icon filename=""]
[#compress]
[#if filename?? && filename !=""]
	<i class='EC-menu-icon-layout'><img src='${path}/icons/${filename}'></i>
[#else]
	<i class='EC-menu-icon-layout'></i>
[/#if]
[/#compress]
[/#macro]

[#-- 状态文字 --]
[#macro rd_status code=0 success="已发布" failed="未发布"]
[#compress]
[#if code == 0]
	<font color='#ff0000'>${failed}</font>
[#else]
	<font color='#6AD317'>${success}</font>
[/#if]
[/#compress]
[/#macro]

[#-- 签收人 --]
[#macro rd_assignee assignee="" complete=0]
[#compress]
[#if assignee?? && (""!=assignee)]
	${assignee}
[#elseif complete == 0]
	<font color='#ff0000;'>等待签收</font>
[#else]
	-
[/#if]
[/#compress]
[/#macro]

[#-- 上传组件 --]
[#macro upload 
	url="" 
	buttonText="选择文件" 
	buttonClass="easyui-linkbutton" 
	fileSizeLimit="5MB"
	fileTypeExts="*.*"
	queueID="uploadQueue"
	removeCompleted="true"
	multi="false"
	width="100"
	height="25"
	lineHeight="25"
	checkExisting=""
	method="post"
	callback=""
	modal = "Y"
]
	[#if modal == "Y"]
	<div class="modal fade " tabindex="-1" role="dialog" data-backdrop="static" id="uploadbox-${timestamp!}" style="display: none;">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	        <div class="widget-header">
				<h5 class="bigger lighter">上传文件</h5>
				<div class="widget-toolbar">
					<a href="#" data-dismiss="modal"><i class="icon-remove"></i></a>
				</div>
			</div>
	            <div class="widget-body">
				<div class="widget-main padding-18">
					上传的文件将在这里显示
					<div id="queue-${timestamp!}"></div>
				</div>
				<div class="widget-toolbox">
					<div class="btn-toolbar">
						<div class="btn-group">
						<a href="javascript:void(0);" id="moduleUpload-${timestamp!}" class="easyui-linkbutton" ><i class="icon-cloud-upload"></i>上传文件</a>
						</div>
					</div>
				</div>
				
			</div>
	        </div>
	    </div>
	</div>
	<button class="btn btn-small btn-success" onclick="modal.open({id:'uploadbox-${timestamp!}'});" >
		<i class="icon-plus"></i>${buttonText!}
	</button>
	[#elseif modal == "N"]
	<a href="javascript:void(0);" id="moduleUpload-${timestamp!}" ><i class="icon-cloud-upload"></i>上传文件</a>
	[/#if]
<script type="text/javascript">
$(function(){
		$("#moduleUpload-${timestamp!}").uploadify({
			//基本设置
			swf:"${path}/resources/js/uploadify/uploadify.swf"
			,modal:false
			,buttonClass:'${buttonClass!}'
			,buttonImage:''
			,buttonText:'${buttonText!}'
			,fileSizeLimit:'${fileSizeLimit}'	//文件大小限制
			,fileTypeExts: '${fileTypeExts}'	//文件类型,格式为"*.*",若有多个格式以";"分割。例如:"*.jpg;*.gif"
			,queueID:'${queueID}' //显示队列的DIV
			,removeCompleted: ${removeCompleted}	//上传成功后的文件，是否在队列中自动删除
			,preventCaching:true
			,auto:true
			,multi:${multi}
			,width:${width}
			,height:${height}
			,lineHeight:${lineHeight}
			,removeTimeout : 1
			//服务器地址和方式设置
//			,checkExisting:"${checkExisting}" //检查上传文件是否存，触发的url，返回1/0
			,uploader: "${url}"
			,method:"${method}"
//			,formData: formData
			,onUploadError: function(file, errorCode, errorMsg, errorString) {
				//若为-280为用户取消上传
				if(errorCode == "-280") {
					bootbox.alert("[" + file.name + "] 取消上传");
				}else{
					bootbox.alert('文件: <font color="#ff0000">[' + file.name + ']</font> 上传失败,' + 
							'<font color="#ff0000">错误原因:</br> '
							+ "错误码 :" + errorCode + "</br>"
							+ "错误消息: " + errorMsg + "</br>"
							+ "错误描述 : " + errorString + "</br></font>");
				}
				
			}
			//上传成功后触发的事件
			,onUploadSuccess: function(file,newFilename,response) {
				if(newFilename!="" && response) {
					//$.messager.alert("[" + file.name + "] 上传成功","上传成功","info");
					${callback!}
				}else {
					//错误的参数
					bootbox.alert("[" + file.name + "] 上传失败");
				}
			}
		});
	})
</script>
[/#macro]


[#-- 问题解决人组织关系 --]
[#macro relationship selectId]
	[@ec_relationship;map]
		[#if map?exists]
            [#list map?keys as key]
            	[#if key == 'leadership']
	    			[#assign nodes = map[key]]
	    			[#if nodes?? && nodes?size>0]
    					[#list nodes as node]
							[#assign on='']
							[#if selectId?? && (node.key == selectId)]
								[#assign on='selected="selected"']
							[/#if]
							<option value="${node.key!}" ${on!}>${node.value!}</option>
						 [/#list]
	    			[/#if]
            	[/#if]
            	[#if key == 'colleague']
	    			[#assign nodes = map[key]]
	    			[#if nodes?? && nodes?size>0]
    					[#list nodes as node]
							[#assign on='']
							[#if selectId?? && (node.key == selectId)]
								[#assign on='selected="selected"']
							[/#if]
							<option value="${node.key!}" ${on!}>${node.value!}</option>
						 [/#list]
	    			[/#if]
            	[/#if]
            	[#if key == 'subordinate']
	    			[#assign nodes = map[key]]
	    			[#if nodes?? && nodes?size>0]
    					[#list nodes as node]
							[#assign on='']
							[#if selectId?? && (node.key == selectId)]
								[#assign on='selected="selected"']
							[/#if]
							<option value="${node.key!}" ${on!}>${node.value!}</option>
						 [/#list]
	    			[/#if]
            	[/#if]
            [/#list]
		[/#if]		
	[/@ec_relationship]
[/#macro]

[#-- 富文本编辑器 --]
[#macro full_text content='' imageUrl='' ]
<br clear="all"/>
	<div id="editor" style="height:300px;">${content!}</div>
    <script type="text/javascript">
        var editor = new wangEditor('editor');
        
        editor.config.menus = [
	        'source',
	        '|',
	        'bold',
	        'underline',
	        'italic',
	        'strikethrough',
	        'eraser',
	        'forecolor',
	        'bgcolor',
	        '|',
	        'quote',
	        'fontfamily',
	        'fontsize',
	        'head',
	        'unorderlist',
	        'orderlist',
	        'alignleft',
	        'aligncenter',
	        'alignright',
	        '|',
	        'link',
	        'unlink',
	        '|',
	        'undo',
	        'redo',
	        'fullscreen'
	    ];
        editor.create();
    </script>
[/#macro]

[#-- 权限树形节点 --]
[#macro ec_department_tree nodes selected=[] title="部门管理" name="authority" attr="" level=0 ]
<link rel="stylesheet" href="${path!}/resources/plugins/dtree/dtree.css" type="text/css" />
<script type="text/javascript" src="${path!}/resources/plugins/dtree/dtree.js"></script>

<div class="dtree">
    <p><a href="javascript:d.closeAll();">展开全部</a> | <a href="javascript: d.openAll();">收起全部</a></p>
    <div id="dtree-content">
    </div>

    <script type="text/javascript">
        d = new dTree('d', "${path!}/resources/plugins/dtree");
        [#assign checked='false']
        d.add('root','dtree_root','${title!}');
        [#if nodes?? && (nodes?size > 0)]
            [#list nodes as node]
                d.add('${node.id!}','root','xxxxxx','${node.id!}','${node.name!}', '${checked!}');
                [#if node.users?? && node.users?size > 0]
                	[#list node.users as user]

		                [#if selected?? && (selected?size > 0)]
		                    [#list selected as select]
		                        [#if user.id == select.id]
		                            [#assign checked='true']
		                            [#break]
		                        [/#if]
		                    [/#list]
		                [/#if]
		                
		                d.add('${user.id!}','${node.id!}','${name!}','${user.id!}','${user.realname!}', '${checked!}');
                	[/#list]
                [/#if]
            [/#list]
        [/#if]
        document.getElementById("dtree-content").innerHTML = d;

        d.openAll();
    </script>

</div>
[/#macro]

[#-- 时区换算 --]
[#macro apptime apptimezoneoffset=5 date="2018-02-12 03:00:00" ]
	[@ec_appzonetime apptimezoneoffset="${apptimezoneoffset!}" date="${date}"]
   ${apptimezoneoffset}
   [/@ec_appzonetime]
[/#macro]