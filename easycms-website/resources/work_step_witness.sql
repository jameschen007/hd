/*
Navicat MySQL Data Transfer

Source Server         : CaaSApp_Localhost
Source Server Version : 50087
Source Host           : localhost:3306
Source Database       : easycms_core_v1

Target Server Type    : MYSQL
Target Server Version : 50087
File Encoding         : 65001

Date: 2015-04-27 13:41:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ec_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `work_step_witness`;
CREATE TABLE `work_step_witness` (
  `id` int(11) NOT NULL auto_increment COMMENT '默认ID',
  `stepno` int(11) NOT NULL COMMENT '工序步骤ID',
  `witness` int(11) NOT NULL COMMENT '见证人ID',
  `witnessdes` varchar(10000) default NULL COMMENT '工序完成说明',
  `status` int(11) NOT NULL COMMENT '当前见证状态',
  `isok` int(11) NOT NULL COMMENT '当前见证结果',
  `remark` varchar(50) default NULL COMMENT '备注',
  `created_on` datetime default NULL,
  `created_by` varchar(40) default NULL,
  `updated_on` datetime default NULL,
  `updated_by` varchar(40) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1480 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
