/*
Navicat MySQL Data Transfer

Source Server         : HD
Source Server Version : 50613
Source Host           : 10.0.1.103:3306
Source Database       : easycms_v2

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2015-09-22 15:48:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ec_department`
-- ----------------------------
DROP TABLE IF EXISTS `ec_department`;
CREATE TABLE `ec_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `department_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '部门名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级部门ID',
  `top_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_fk` (`parent_id`),
  KEY `fk_role` (`top_role_id`),
  CONSTRAINT `ec_department_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `ec_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role` FOREIGN KEY (`top_role_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_department
-- ----------------------------
INSERT INTO `ec_department` VALUES ('104', '经理部', null, '151');
INSERT INTO `ec_department` VALUES ('105', '技术厂长', '104', '152');
INSERT INTO `ec_department` VALUES ('106', '质量厂长', '104', '153');
INSERT INTO `ec_department` VALUES ('107', '施工管理室', '105', '155');
INSERT INTO `ec_department` VALUES ('108', '管道队队长', '105', '154');
INSERT INTO `ec_department` VALUES ('109', '宋国强班', '107', '156');
INSERT INTO `ec_department` VALUES ('112', '技术部管道组', '105', '158');
INSERT INTO `ec_department` VALUES ('113', '技术部焊接组', '105', '159');
INSERT INTO `ec_department` VALUES ('114', '生产部', '105', '160');
INSERT INTO `ec_department` VALUES ('115', '物项组', '105', '162');
INSERT INTO `ec_department` VALUES ('116', '质检部', '106', '163');
INSERT INTO `ec_department` VALUES ('117', '见证组组长', '116', '164');
INSERT INTO `ec_department` VALUES ('118', 'W级见证员', '117', '165');
INSERT INTO `ec_department` VALUES ('119', 'H级见证员', '117', '166');
INSERT INTO `ec_department` VALUES ('120', 'R级见证员', '117', '167');
INSERT INTO `ec_department` VALUES ('121', '驻场代表', '105', '168');

-- ----------------------------
-- Table structure for `ec_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ec_menu`;
CREATE TABLE `ec_menu` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '序号UID',
  `menu_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单名',
  `seq` int(255) NOT NULL DEFAULT '0' COMMENT '队列序号',
  `icon_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标路径',
  `style` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '菜单样式',
  `privilege_id` int(255) DEFAULT NULL,
  `parent_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '父级ID',
  PRIMARY KEY (`id`),
  KEY `m_p_fk` (`privilege_id`),
  KEY `m_m_fk` (`parent_id`),
  CONSTRAINT `ec_menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `ec_menu` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `ec_menu_ibfk_2` FOREIGN KEY (`privilege_id`) REFERENCES `ec_privilege` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_menu
-- ----------------------------
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c2d20000', '菜单列表', '10', '', '', '1913', '4028820b4de2263e014de22707420000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c32a0001', '添加菜单', '10', '', '', '1914', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c35d0002', '修改菜单', '9', '', '', '1915', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de223b4014de223c39f0003', '删除菜单', '0', '', '', '1916', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22707420000', '菜单管理', '0', '', 'icon-list-alt', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22937af0001', '结构设置', '9', '', 'icon-cogs', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22960f90002', '人员列表', '10', '', '', '1918', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22984c40003', '添加人员', '0', '', '', '1919', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229a3440004', '修改用户', '0', '', null, '1921', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229d3790005', '查看人员', '0', '', '', '1920', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de229efbd0006', '删除人员', '0', '', '', '1922', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22bb0d70007', '职务列表', '9', '', '', '1929', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22bdbc60008', '添加职务', '0', '', '', '1930', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d141a0009', '修改职务', '0', '', '', '1931', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d59ab000a', '分配角色', '0', '', null, '1923', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22d87ea000b', '重置密码', '0', '', null, '1924', '4028820b4de2263e014de22960f90002');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22dc365000c', '删除职务', '0', '', '', '1932', '4028820b4de2263e014de22bb0d70007');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e1e36000d', '施工班组列表', '0', '', '', '1925', '4028820b4de2263e014de22937af0001');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e441a000e', '添加部门', '0', '', null, '1926', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e6c7a000f', '修改部门', '0', '', null, '1927', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22e8abf0010', '删除部门', '0', '', null, '1928', '4028820b4de2263e014de22e1e36000d');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22ec9f70011', '权限列表', '0', '', null, '1933', '4028820b4de2263e014de22707420000');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22eee690012', '添加权限', '0', '', null, '1934', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f12ea0013', '修改权限', '0', '', null, '1935', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f383c0014', '删除权限', '0', '', null, '1936', '4028820b4de2263e014de22ec9f70011');
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f5f790015', '设置', '10', '', 'icon-cog', null, null);
INSERT INTO `ec_menu` VALUES ('4028820b4de2263e014de22f8af60016', '系统设置', '0', '', null, '1939', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('402882234de73907014de74441bd0000', '分派到班组', '0', '', null, '1964', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_menu` VALUES ('402882234ff3ff4c014ff40105180000', '发起见证', '0', '', null, '2044', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de57aaada0000', '问题跟踪', '4', '', 'icon-retweet', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de581cd4a0003', '需要处理的问题', '112', '', null, '2000', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de582626b0004', '需要确认的问题', '122', '', '', '2010', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de582bad30005', '未解决问题', '11', '', '', '2004', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de578a7014de58316230006', '已解决问题', '111', '', null, '2002', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64782700000', '关注的问题', '1', '', null, '2013', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64883a90001', '发起的问题', '1', '', '', '2007', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64942450002', '基础设置', '8', '', 'icon-wrench', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6497ec80003', '施工管理', '7', '', 'icon-tasks', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de649c12f0004', '工序见证', '5', '', 'icon-eye-open', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de649effa0005', '统计信息', '3', '', 'icon-bar-chart', null, null);
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64ed1880007', '单价管理', '0', '', '', '1941', '4028824c4de60d34014de64942450002');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de64f3de40008', '材质类型', '0', '', null, '1945', '4028824c4de60d34014de64942450002');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65227c60009', '分派到班组', '0', '', null, '1963', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65291fe000a', '分派到组长', '0', '', null, '1968', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6542bb3000b', '我的任务', '0', '', null, '1973', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de6566e51000c', '班组任务统计', '0', '', null, '1983', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de658f3e7000e', '任务完成统计', '0', '', null, '1986', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de659497a000f', '任务价格统计', '0', '', null, '1987', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de60d34014de65a25cc0010', '各组任务统计', '0', '', null, '1984', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de65feaf10000', '分派见证', '0', '', null, '2016', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66090c40001', '已分派见证', '0', '', null, '2018', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66104700002', '分派到见证人', '0', '', null, '2017', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de661859f0003', '改派', '0', '', null, '2019', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de661c5bb0004', '解除分派', '0', '', null, '2020', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66225930005', '未完成见证', '0', '', null, '2021', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de662c1f50006', '见证结果', '0', '', '', '2022', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de66350530007', '已完成见证', '0', '', null, '2023', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de6638f740008', '修改见证结果', '0', '', '', '2024', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de663f5060009', '我发起的见证', '0', '', null, '2025', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de65ea6014de664be85000a', '我的见证', '0', '', null, '2026', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c2d0a10000', '问题确认', '0', '', null, '2009', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c43db50001', '发起的问题明细', '0', '', '', '2035', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c56efb0002', '处理问题', '0', '', null, '1996', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c60ea70003', '删除问题', '0', '', null, '2006', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c65eef0004', '已解决问题明细', '0', '', null, '2001', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c6a5730005', '未解决问题明细', '0', '', null, '2003', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c701250006', '发起问题明细', '0', '', null, '2005', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c77ce10007', '关注的问题明细', '0', '', null, '2011', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c7d6f20008', '创建问题', '0', '', null, '1997', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c8b5a30009', '解决人', '0', '', null, '1998', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c94e21000a', '增加材质类型', '0', '', null, '1946', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c98972000b', '删除材质类型', '0', '', null, '1948', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6c9d005000c', '查看材质类型', '0', '', null, '1947', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca2511000d', '更新材质类型', '0', '', null, '1949', '4028824c4de60d34014de64f3de40008');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca5922000e', '添加单价', '0', '', null, '1942', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ca89ee000f', '修改单价', '0', '', null, '1943', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cab21b0010', '删除单价', '0', '', null, '1944', '4028824c4de60d34014de64ed1880007');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cc55b00011', '滚动计划', '0', '', null, '1950', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ccbd460012', '上传滚动计划', '0', '', null, '1951', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ccf0070013', '添加滚动计划', '0', '', null, '1952', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cd2dfd0014', '修改滚动计划', '0', '', null, '1953', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cd69d60015', '删除滚动计划', '0', '', '', '1954', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ce45390016', '查看工序步骤', '0', '', null, '1955', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ce848d0017', '添加工序步骤', '0', '', null, '1956', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6ceb7e80018', '修改工序步骤', '0', '', null, '1957', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cef43f0019', '查看工序步骤明细', '0', '', null, '1958', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cf2ed9001a', '删除工序', '0', '', null, '1959', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6cf5cce001b', '导入工序', '0', '', null, '1960', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d035ba001c', '见证人名词', '0', '', null, '1993', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d09202001d', '计划表名词', '0', '', null, '1989', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d0c32f001e', '单价名词', '0', '', null, '1992', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d101ff001f', '焊口/支架名词', '0', '', '', '1990', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d153780020', '滞后时间设置', '0', '', null, '1994', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d19c710021', '问题配置', '0', '', null, '1961', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d1f4d10022', '分派班组名词', '0', '', null, '1991', '4028820b4de2263e014de22f5f790015');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d5c8df0023', '问题明细', '0', '', null, '1995', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d6451b0024', '删除关注人', '0', '', null, '2012', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d737b50025', '分派至组长', '0', '', '', '1969', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d8864b0026', '已分派到组长', '0', '', null, '1970', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d8d06f0027', '已分派到班组', '0', '', null, '1965', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6d9a8700028', '解除分派班组', '0', '', null, '1967', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6da80a90029', '解除分派组长', '0', '', null, '1972', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6daed4b002a', '改派班组', '0', '', null, '1966', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6db20ef002b', '改派组长', '0', '', null, '1971', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6dd2ac2002c', '进入任务', '0', '', null, '1974', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6de1bf7002d', '发起见证', '0', '', null, '1975', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6de6b01002e', '见证状态', '0', '', null, '1976', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6debd0e002f', '回填信息', '0', '', null, '1977', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6df05a70030', '修改回填信息', '0', '', null, '1978', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6df7aa20031', '完成信息', '0', '', null, '1979', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6dfc6400032', '修改完成信息', '0', '', null, '1980', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e00b010033', '我完成的任务', '0', '', null, '1981', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e060cd0034', '上传文件', '0', '', null, '1999', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e0d9e60035', '上传计划文件', '0', '', null, '2031', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('4028824c4de6705f014de6e17f6c0036', '上传图标', '0', '', null, '1937', '4028820b4de223b4014de223c2d20000');
INSERT INTO `ec_menu` VALUES ('402882f74e3811d0014e38191d5a0000', '下载滚动计划模板', '0', '', null, '2034', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c71cb060000', '我的见证结果', '0', '', null, '2038', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c7214090001', '修改我的见证结果', '0', '', null, '2039', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_menu` VALUES ('402882f74e5c6fc1014e5c7271970002', '分派完成情况', '0', '', null, '2040', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_menu` VALUES ('402882f74e5cb6a3014e5cb79ecc0000', '我完成的见证', '0', '', null, '2041', '4028824c4de60d34014de649c12f0004');

-- ----------------------------
-- Table structure for `ec_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `ec_privilege`;
CREATE TABLE `ec_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `privilege_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '权限名称',
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限URI',
  `menu_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_m_fk` (`menu_id`),
  CONSTRAINT `ec_privilege_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `ec_menu` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2045 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_privilege
-- ----------------------------
INSERT INTO `ec_privilege` VALUES ('1913', '菜单列表', '/management/menu', '4028820b4de223b4014de223c2d20000', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1914', '添加菜单', '/management/menu/add', '4028820b4de223b4014de223c32a0001', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1915', '修改菜单', '/management/menu/edit', '4028820b4de223b4014de223c35d0002', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1916', '删除菜单', '/management/menu/delete', '4028820b4de223b4014de223c39f0003', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1918', '用户管理', '/management/user', '4028820b4de2263e014de22960f90002', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1919', '添加用户', '/management/user/add', '4028820b4de2263e014de22984c40003', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1920', '查看用户', '/management/user/show', '4028820b4de2263e014de229d3790005', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1921', '修改用户', '/management/user/edit', '4028820b4de2263e014de229a3440004', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1922', '删除用户', '/management/user/delete', '4028820b4de2263e014de229efbd0006', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1923', '分配角色', '/management/user/role', '4028820b4de2263e014de22d59ab000a', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1924', '重置密码', '/management/user/password', '4028820b4de2263e014de22d87ea000b', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1925', '部门管理', '/management/department', '4028820b4de2263e014de22e1e36000d', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1926', '新建部门', '/management/department/add', '4028820b4de2263e014de22e441a000e', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1927', '编辑部门', '/management/department/edit', '4028820b4de2263e014de22e6c7a000f', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1928', '删除部门', '/management/department/delete', '4028820b4de2263e014de22e8abf0010', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1929', '角色管理', '/management/role', '4028820b4de2263e014de22bb0d70007', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1930', '添加角色', '/management/role/add', '4028820b4de2263e014de22bdbc60008', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1931', '修改角色', '/management/role/edit', '4028820b4de2263e014de22d141a0009', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1932', '删除角色', '/management/role/delete', '4028820b4de2263e014de22dc365000c', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1933', '权限管理', '/management/privilege', '4028820b4de2263e014de22ec9f70011', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1934', '添加权限', '/management/privilege/add', '4028820b4de2263e014de22eee690012', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1935', '修改权限', '/management/privilege/edit', '4028820b4de2263e014de22f12ea0013', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1936', '删除权限', '/management/privilege/delete', '4028820b4de2263e014de22f383c0014', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1937', '上传图标', '/management/privilege/upload', '4028824c4de6705f014de6e17f6c0036', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1939', '系统设置', '/management/settings/website', '4028820b4de2263e014de22f8af60016', '2015-06-11 18:13:35', null);
INSERT INTO `ec_privilege` VALUES ('1941', '单价管理', '/baseservice/price', '4028824c4de60d34014de64ed1880007', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1942', '添加单价', '/baseservice/price/add', '4028824c4de6705f014de6ca5922000e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1943', '修改单价', '/baseservice/price/edit', '4028824c4de6705f014de6ca89ee000f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1944', '删除单价', '/baseservice/price/delete', '4028824c4de6705f014de6cab21b0010', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1945', '材质类型', '/baseservice/material_type', '4028824c4de60d34014de64f3de40008', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1946', '增加材质类型', '/baseservice/material_type/add', '4028824c4de6705f014de6c94e21000a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1947', '查看材质类型', '/baseservice/material_type/detail', '4028824c4de6705f014de6c9d005000c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1948', '删除材质类型', '/baseservice/material_type/delete', '4028824c4de6705f014de6c98972000b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1949', '更新材质类型', '/baseservice/material_type/edit', '4028824c4de6705f014de6ca2511000d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1950', '滚动计划', '/baseservice/rollingplan', '4028824c4de6705f014de6cc55b00011', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1951', '上传滚动计划', '/baseservice/rollingplan/import', '4028824c4de6705f014de6ccbd460012', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1952', '添加滚动计划', '/baseservice/rollingplan/add', '4028824c4de6705f014de6ccf0070013', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1953', '修改滚动计划', '/baseservice/rollingplan/edit', '4028824c4de6705f014de6cd2dfd0014', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1954', '删除滚动计划', '/baseservice/rollingplan/delete', '4028824c4de6705f014de6cd69d60015', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1955', '查看工序步骤', '/baseservice/rollingplan/workstep', '4028824c4de6705f014de6ce45390016', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1956', '添加工序步骤', '/baseservice/rollingplan/workstep/add', '4028824c4de6705f014de6ce848d0017', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1957', '修改工序步骤', '/baseservice/rollingplan/workstep/edit', '4028824c4de6705f014de6ceb7e80018', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1958', '查看工序步骤明细', '/baseservice/rollingplan/workstep/detail', '4028824c4de6705f014de6cef43f0019', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1959', '删除工序步骤', '/baseservice/rollingplan/workstep/delete', '4028824c4de6705f014de6cf2ed9001a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1960', '导入工序', '/baseservice/rollingplan/workstep/import', '4028824c4de6705f014de6cf5cce001b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1961', '问题配置', '/baseservice/setting/problem', '4028824c4de6705f014de6d19c710021', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1963', '分派到班组', '/construction/team', '4028824c4de60d34014de65227c60009', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1964', '分派到班组', '/construction/team/assign', '402882234de73907014de74441bd0000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1965', '已分派班组', '/construction/team/already', '4028824c4de6705f014de6d8d06f0027', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1966', '改派', '/construction/team/modify', '4028824c4de6705f014de6daed4b002a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1967', '解除分派', '/construction/team/release', '4028824c4de6705f014de6d9a8700028', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1968', '分派到组长', '/construction/endman', '4028824c4de60d34014de65291fe000a', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1969', '分派到组长', '/construction/endman/assign', '4028824c4de6705f014de6d737b50025', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1970', '已分派组长', '/construction/endman/already', '4028824c4de6705f014de6d8864b0026', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1971', '改派', '/construction/endman/modify', '4028824c4de6705f014de6db20ef002b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1972', '解除分派', '/construction/endman/release', '4028824c4de6705f014de6da80a90029', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1973', '我的任务', '/construction/mytask', '4028824c4de60d34014de6542bb3000b', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1974', '进入任务', '/construction/mytask/view', '4028824c4de6705f014de6dd2ac2002c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1975', '发起见证', '/construction/mytask/witness', '4028824c4de6705f014de6de1bf7002d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1976', '见证状态', '/construction/mytask/witness/view', '4028824c4de6705f014de6de6b01002e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1977', '回填信息', '/construction/mytask/workstep/result', '4028824c4de6705f014de6debd0e002f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1978', '修改回填信息', '/construction/mytask/workstep/result/edit', '4028824c4de6705f014de6df05a70030', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1979', '完成信息', '/construction/mytask/rollingplan/result', '4028824c4de6705f014de6df7aa20031', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1980', '修改完成信息', '/construction/mytask/rollingplan/result/edit', '4028824c4de6705f014de6dfc6400032', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1981', '我完成的任务', '/construction/mytask/already', '4028824c4de6705f014de6e00b010033', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1983', '班组任务统计', '/statistics/taskteam', '4028824c4de60d34014de6566e51000c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1984', '各组任务统计', '/statistics/taskgroup', '4028824c4de60d34014de65a25cc0010', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1985', '任务状态统计', '/statistics/taskstatus', null, '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1986', '任务完成统计', '/statistics/hyperbola', '4028824c4de60d34014de658f3e7000e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1987', '任务价格统计', '/statistics/taskprice', '4028824c4de60d34014de659497a000f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1989', '计划表名词', '/variable/planningtable', '4028824c4de6705f014de6d09202001d', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1990', '焊口/支架名词', '/variable/welddistinguish', '4028824c4de6705f014de6d101ff001f', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1991', '分派班组名词', '/variable/assigntype', '4028824c4de6705f014de6d1f4d10022', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1992', '单价名词', '/variable/price', '4028824c4de6705f014de6d0c32f001e', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1993', '见证人名词', '/variable/witnesstype', '4028824c4de6705f014de6d035ba001c', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1994', '滞后时间设置', '/variable/hysteresis', '4028824c4de6705f014de6d153780020', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1995', '问题明细', '/hd/workstep/problem/detail', '4028824c4de6705f014de6d5c8df0023', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1996', '处理问题', '/hd/workstep/problem/handle', '4028824c4de6705f014de6c56efb0002', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1997', '创建问题', '/hd/workstep/problem/add', '4028824c4de6705f014de6c7d6f20008', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1998', '解决人', '/hd/workstep/problem/solvers', '4028824c4de6705f014de6c8b5a30009', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('1999', '上传文件', '/hd/workstep/problem/upload', '4028824c4de6705f014de6e060cd0034', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2000', '需要处理的问题', '/hd/workstep/problem', '4028824c4de578a7014de581cd4a0003', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2001', '已解决问题明细', '/hd/workstep/problem/solved/detail', '4028824c4de6705f014de6c65eef0004', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2002', '已解决问题', '/hd/workstep/problem/solved', '4028824c4de578a7014de58316230006', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2003', '未解决问题明细', '/hd/workstep/problem/unsolved/detail', '4028824c4de6705f014de6c6a5730005', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2004', '未解决问题', '/hd/workstep/problem/unsolved', '4028824c4de578a7014de582bad30005', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2005', '发起的问题明细', '/hd/workstep/problem/created/detail', '4028824c4de6705f014de6c701250006', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2006', '删除问题', '/hd/workstep/problem/created/delete', '4028824c4de6705f014de6c60ea70003', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2007', '发起的问题', '/hd/workstep/problem/created', '4028824c4de60d34014de64883a90001', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2008', '发起的问题明细', '/hd/workstep/problem/toconfirm/detail', '4028824c4de6705f014de6c43db50001', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2009', '问题确认', '/hd/workstep/problem/solveconfirm', '4028824c4de6705f014de6c2d0a10000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2010', '需要确认的问题', '/hd/workstep/problem/toconfirm', '4028824c4de578a7014de582626b0004', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2011', '关注的问题明细', '/hd/workstep/problem/toconcerned/detail', '4028824c4de6705f014de6c77ce10007', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2012', '删除关注人', '/hd/workstep/problem/concernman/delete', '4028824c4de6705f014de6d6451b0024', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2013', '关注的问题', '/hd/workstep/problem/toconcerned', '4028824c4de60d34014de64782700000', '2015-06-12 09:53:26', null);
INSERT INTO `ec_privilege` VALUES ('2016', '分派见证', '/witness/witnesser', '4028824c4de65ea6014de65feaf10000', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2017', '分派到见证人', '/witness/witnesser/assign', '4028824c4de65ea6014de66104700002', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2018', '已分派见证', '/witness/witnesser/already', '4028824c4de65ea6014de66090c40001', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2019', '改派', '/witness/witnesser/modify', '4028824c4de65ea6014de661859f0003', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2020', '解除分派', '/witness/witnesser/release', '4028824c4de65ea6014de661c5bb0004', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2021', '未完成见证', '/witness/witnesser/uncomplete', '4028824c4de65ea6014de66225930005', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2022', '见证结果', '/witness/witnesser/result', '4028824c4de65ea6014de662c1f50006', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2023', '已完成见证', '/witness/witnesser/complete', '4028824c4de65ea6014de66350530007', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2024', '修改见证结果', '/witness/witnesser/result/edit', '4028824c4de65ea6014de6638f740008', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2025', '我发起的见证', '/witness/mylaunch', '4028824c4de65ea6014de663f5060009', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2026', '我的见证', '/witness/myevent', '4028824c4de65ea6014de664be85000a', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2027', '见证结果', '/witness/myevent/result', '4028824c4de65ea6014de662c1f50006', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2028', '修改见证结果', '/witness/myevent/result/edit', '4028824c4de65ea6014de6638f740008', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2030', '流程文件', '/workflow/zips', null, '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2031', '上传文件', '/workflow/zips/upload', '4028824c4de6705f014de6e0d9e60035', '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2032', '删除', '/workflow/zips/delete', null, '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2033', '发布', '/workflow/zips/deploy', null, '2015-06-12 14:04:40', null);
INSERT INTO `ec_privilege` VALUES ('2034', '下载滚动计划模板', '/baseservice/rollingplan/download', '402882f74e3811d0014e38191d5a0000', '2015-06-28 10:10:08', null);
INSERT INTO `ec_privilege` VALUES ('2035', '确认的问题明细', '/hd/workstep/problem/confirm/detail', '4028824c4de6705f014de6c43db50001', '2015-07-03 13:35:01', null);
INSERT INTO `ec_privilege` VALUES ('2036', '滚动计划问题', '/hd/workstep/problem/rollingplan', null, '2015-07-04 17:32:28', null);
INSERT INTO `ec_privilege` VALUES ('2037', '滚动计划问题明细', '/hd/workstep/problem/rollingplan/detail', null, '2015-07-04 17:32:28', null);
INSERT INTO `ec_privilege` VALUES ('2038', '我的见证结果', '/witness/myevent/launch', '402882f74e5c6fc1014e5c71cb060000', '2015-07-05 10:25:35', null);
INSERT INTO `ec_privilege` VALUES ('2039', '修改我的见证结果', '/witness/myevent/launch/edit', '402882f74e5c6fc1014e5c7214090001', '2015-07-05 10:25:35', null);
INSERT INTO `ec_privilege` VALUES ('2040', '分派完成情况', '/witness/witnesser/assign/view', '402882f74e5c6fc1014e5c7271970002', '2015-07-05 12:18:29', null);
INSERT INTO `ec_privilege` VALUES ('2041', '我完成的见证', '/witness/myevent/already', '402882f74e5cb6a3014e5cb79ecc0000', '2015-07-05 13:35:54', null);
INSERT INTO `ec_privilege` VALUES ('2042', '需确认的问题明细', '/hd/workstep/problem/toconfirm/detail', null, '2015-07-24 09:42:53', null);
INSERT INTO `ec_privilege` VALUES ('2043', '修改见证', '/construction/mytask/witness/edit', null, '2015-08-14 11:02:01', null);
INSERT INTO `ec_privilege` VALUES ('2044', '发起见证', '/construction/mytask/batch_witness', '402882234ff3ff4c014ff40105180000', '2015-09-22 15:28:50', null);

-- ----------------------------
-- Table structure for `ec_role`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role`;
CREATE TABLE `ec_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名',
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `coexist` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ec_parent_fk` (`parent_id`),
  CONSTRAINT `ec_parent_fk` FOREIGN KEY (`parent_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role
-- ----------------------------
INSERT INTO `ec_role` VALUES ('151', '厂长', null, '1', null);
INSERT INTO `ec_role` VALUES ('152', '技术厂长', '151', '2', null);
INSERT INTO `ec_role` VALUES ('153', '质量厂长', '151', '2', null);
INSERT INTO `ec_role` VALUES ('154', '管道队队长', '152', '3', null);
INSERT INTO `ec_role` VALUES ('155', '施工管理室', '152', '3', null);
INSERT INTO `ec_role` VALUES ('156', '班长', '155', '4', null);
INSERT INTO `ec_role` VALUES ('157', '组长', '156', '4', null);
INSERT INTO `ec_role` VALUES ('158', '管道组组长', '152', '3', null);
INSERT INTO `ec_role` VALUES ('159', '焊接组组长', '152', '3', null);
INSERT INTO `ec_role` VALUES ('160', '生产部主任', '152', '3', null);
INSERT INTO `ec_role` VALUES ('161', '生产部主任助理', '160', '4', null);
INSERT INTO `ec_role` VALUES ('162', '物项组组长', '152', '3', null);
INSERT INTO `ec_role` VALUES ('163', '质量部主任', '153', '3', null);
INSERT INTO `ec_role` VALUES ('164', '见证组组长', '163', '4', null);
INSERT INTO `ec_role` VALUES ('165', 'W级见证员', '164', '5', '1');
INSERT INTO `ec_role` VALUES ('166', 'H级见证员', '164', '5', '1');
INSERT INTO `ec_role` VALUES ('167', 'R级见证员', '164', '5', '1');
INSERT INTO `ec_role` VALUES ('168', '管道预制代表', '152', '3', null);

-- ----------------------------
-- Table structure for `ec_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role_menu`;
CREATE TABLE `ec_role_menu` (
  `role_id` int(255) NOT NULL,
  `menu_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `r_middle_fk` (`role_id`),
  KEY `m_middle_fk` (`menu_id`),
  CONSTRAINT `ec_role_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `ec_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_role_menu_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role_menu
-- ----------------------------
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('118', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('118', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('119', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('119', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('120', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('120', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('122', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('124', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('126', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('127', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('128', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('129', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('121', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('125', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('151', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('152', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('153', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('154', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de6542bb3000b');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6e060cd0034');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6dfc6400032');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6df7aa20031');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6df05a70030');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6debd0e002f');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6de6b01002e');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6de1bf7002d');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6dd2ac2002c');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c8b5a30009');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c7d6f20008');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('157', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('158', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('159', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('160', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('162', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('163', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('163', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('163', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('163', '402882f74e5c6fc1014e5c7271970002');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('163', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('155', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('161', '402882f74e3811d0014e38191d5a0000');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('161', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('161', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('168', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de6497ec80003');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6e00b010033');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d8d06f0027');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6daed4b002a');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d9a8700028');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d8864b0026');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6db20ef002b');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6da80a90029');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cc55b00011');
INSERT INTO `ec_role_menu` VALUES ('156', '402882f74e3811d0014e38191d5a0000');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6e0d9e60035');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cf5cce001b');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cf2ed9001a');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cef43f0019');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6ceb7e80018');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6ce848d0017');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6ce45390016');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cd69d60015');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6cd2dfd0014');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6ccf0070013');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6ccbd460012');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de65291fe000a');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d737b50025');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de65227c60009');
INSERT INTO `ec_role_menu` VALUES ('156', '402882234de73907014de74441bd0000');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('156', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('164', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('164', '402882f74e5c6fc1014e5c7271970002');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de64883a90001');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6d6451b0024');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c701250006');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c60ea70003');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('164', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('166', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('166', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('166', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de663f5060009');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de66350530007');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de6638f740008');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de66225930005');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de662c1f50006');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de66090c40001');
INSERT INTO `ec_role_menu` VALUES ('166', '402882f74e5c6fc1014e5c7271970002');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de661c5bb0004');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de661859f0003');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de65feaf10000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de65ea6014de66104700002');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de578a7014de57aaada0000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de578a7014de582626b0004');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c43db50001');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c2d0a10000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de578a7014de581cd4a0003');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c56efb0002');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de578a7014de58316230006');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c65eef0004');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de578a7014de582bad30005');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c6a5730005');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de64782700000');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6d5c8df0023');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de6705f014de6c77ce10007');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('166', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('167', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('167', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('167', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('167', '4028824c4de60d34014de6566e51000c');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de649c12f0004');
INSERT INTO `ec_role_menu` VALUES ('165', '402882f74e5cb6a3014e5cb79ecc0000');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de65ea6014de664be85000a');
INSERT INTO `ec_role_menu` VALUES ('165', '402882f74e5c6fc1014e5c7214090001');
INSERT INTO `ec_role_menu` VALUES ('165', '402882f74e5c6fc1014e5c71cb060000');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de649effa0005');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de65a25cc0010');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de659497a000f');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de658f3e7000e');
INSERT INTO `ec_role_menu` VALUES ('165', '4028824c4de60d34014de6566e51000c');

-- ----------------------------
-- Table structure for `ec_role_privilege`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role_privilege`;
CREATE TABLE `ec_role_privilege` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `privilege_id` int(11) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`role_id`,`privilege_id`),
  KEY `fk_role_privilege` (`role_id`),
  KEY `fk_privilege_role` (`privilege_id`),
  CONSTRAINT `ec_role_privilege_ibfk_1` FOREIGN KEY (`privilege_id`) REFERENCES `ec_privilege` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_role_privilege_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role_privilege
-- ----------------------------

-- ----------------------------
-- Table structure for `ec_site`
-- ----------------------------
DROP TABLE IF EXISTS `ec_site`;
CREATE TABLE `ec_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站名称',
  `site_domain` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '域名',
  `site_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键字',
  `site_describe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站描述',
  `site_tpl_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板名称',
  `upload_max_size` int(11) DEFAULT '0' COMMENT '上传文件大小',
  `upload_tpl_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板上传目录',
  `tpl_file_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板上传类型',
  `upload_res_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源上传目录',
  `is_local_site` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否本地站点',
  `app_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '站点绝对路径',
  `i18n_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '国际化目录',
  `pagesize_list` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pagesize` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_site
-- ----------------------------
INSERT INTO `ec_site` VALUES ('9', '核岛安装实时动态监管系统', 'asdas', 'asda', 'sdsad', 'easycms', null, null, null, null, '1', '/', null, '[20,50,100]', '20');

-- ----------------------------
-- Table structure for `ec_user`
-- ----------------------------
DROP TABLE IF EXISTS `ec_user`;
CREATE TABLE `ec_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名唯一',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `real_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '真是姓名',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `current_login_time` datetime DEFAULT NULL COMMENT '当前登录时间',
  `current_login_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT '127.0.0.1' COMMENT '当前登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT '127.0.0.1' COMMENT '上次登录IP',
  `login_times` int(11) DEFAULT NULL COMMENT '登录次数',
  `is_default_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认管理员',
  `is_del` tinyint(1) DEFAULT '0',
  `device` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(100) DEFAULT NULL,
  `department_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_parent` (`parent_id`),
  KEY `fk_department` (`department_id`),
  CONSTRAINT `fk_department` FOREIGN KEY (`department_id`) REFERENCES `ec_department` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_parent` FOREIGN KEY (`parent_id`) REFERENCES `ec_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_user
-- ----------------------------
INSERT INTO `ec_user` VALUES ('8', 'admin', '594afe5e1f8ac83827ca43434f7dae1a', '邓洁芃', '', '2014-01-01 18:20:59', '127.0.0.1', '2015-09-22 15:41:06', '192.168.2.163', '2015-09-22 15:29:17', '192.168.2.163', '1593', '1', '0', 'zzzzzz', null, null);
INSERT INTO `ec_user` VALUES ('164', 'liukun', '0c73c5a91374dc3a19f6b7f68b3b6af2', '刘坤', '', '2015-08-18 14:57:56', '192.168.8.1', null, null, null, null, null, '0', '0', null, null, '104');
INSERT INTO `ec_user` VALUES ('165', 'yanghongfang', '0c73c5a91374dc3a19f6b7f68b3b6af2', '杨红芳', '', '2015-08-18 14:58:12', '192.168.8.1', '2015-08-18 16:10:45', '192.168.8.1', null, null, '1', '0', '0', null, '164', '105');
INSERT INTO `ec_user` VALUES ('166', 'guizuqin', '0c73c5a91374dc3a19f6b7f68b3b6af2', '桂祖琴', '', '2015-08-18 14:58:27', '192.168.8.1', null, null, null, null, null, '0', '0', null, '164', '106');
INSERT INTO `ec_user` VALUES ('167', 'liuxue', '0c73c5a91374dc3a19f6b7f68b3b6af2', '刘学', '', '2015-08-18 14:58:42', '192.168.8.1', null, null, null, null, null, '0', '0', null, '165', '108');
INSERT INTO `ec_user` VALUES ('168', 'zhangxiaolin', '0c73c5a91374dc3a19f6b7f68b3b6af2', '张孝林', '', '2015-08-18 14:58:55', '192.168.8.1', '2015-08-18 16:24:37', '192.168.8.1', '2015-08-18 16:11:05', '192.168.8.1', '2', '0', '0', null, '165', '121');
INSERT INTO `ec_user` VALUES ('169', 'songguoqiang', '0c73c5a91374dc3a19f6b7f68b3b6af2', '宋国强', '', '2015-08-18 14:59:09', '192.168.8.1', '2015-09-10 16:29:55', '192.168.8.1', '2015-09-10 16:04:05', '42.81.35.192', '22', '0', '0', null, '182', '109');
INSERT INTO `ec_user` VALUES ('170', 'limin', '0c73c5a91374dc3a19f6b7f68b3b6af2', '李敏', '', '2015-08-18 14:59:22', '192.168.8.1', '2015-09-21 14:30:37', '192.168.2.119', '2015-09-10 16:52:09', '192.168.8.1', '17', '0', '0', null, '169', '109');
INSERT INTO `ec_user` VALUES ('171', 'xiongzhanwen', '0c73c5a91374dc3a19f6b7f68b3b6af2', '熊占文', '', '2015-08-18 14:59:34', '192.168.8.1', '2015-09-10 16:54:06', '211.138.116.12', '2015-09-10 16:18:50', '211.138.116.12', '12', '0', '0', null, '169', '109');
INSERT INTO `ec_user` VALUES ('172', 'jiangqiuju', '0c73c5a91374dc3a19f6b7f68b3b6af2', '蒋秋菊', '', '2015-08-18 14:59:45', '192.168.8.1', '2015-09-02 13:16:11', '218.75.56.242', '2015-08-24 10:31:48', '218.75.56.242', '2', '0', '0', null, '165', '112');
INSERT INTO `ec_user` VALUES ('173', 'wangshifeng', '0c73c5a91374dc3a19f6b7f68b3b6af2', '王世峰', '', '2015-08-18 14:59:57', '192.168.8.1', '2015-09-01 08:34:28', '218.75.56.242', null, null, '1', '0', '0', null, '165', '113');
INSERT INTO `ec_user` VALUES ('174', 'hekuilin', '0c73c5a91374dc3a19f6b7f68b3b6af2', '何奎林', '', '2015-08-18 15:00:18', '192.168.8.1', '2015-08-31 15:08:20', '218.75.56.242', null, null, '1', '0', '0', null, '165', '114');
INSERT INTO `ec_user` VALUES ('175', 'chaifeng', '0c73c5a91374dc3a19f6b7f68b3b6af2', '柴峰', '', '2015-08-18 15:00:36', '192.168.8.1', '2015-09-10 16:17:02', '192.168.8.1', '2015-09-08 08:55:01', '218.75.56.242', '29', '0', '0', null, '174', '114');
INSERT INTO `ec_user` VALUES ('176', 'xukai', '0c73c5a91374dc3a19f6b7f68b3b6af2', '许恺', '', '2015-08-18 15:00:52', '192.168.8.1', '2015-09-02 13:42:24', '218.75.56.242', '2015-09-01 11:41:39', '218.75.56.242', '4', '0', '0', null, '165', '115');
INSERT INTO `ec_user` VALUES ('177', 'zhanglei', '0c73c5a91374dc3a19f6b7f68b3b6af2', '张蕾', '', '2015-08-18 15:01:04', '192.168.8.1', '2015-09-10 15:49:07', '218.75.56.242', '2015-09-10 15:16:33', '218.75.56.242', '3', '0', '0', null, '166', '116');
INSERT INTO `ec_user` VALUES ('178', 'changxia', '0c73c5a91374dc3a19f6b7f68b3b6af2', '常夏', '', '2015-08-18 15:01:19', '192.168.8.1', '2015-09-21 14:28:40', '192.168.2.119', '2015-09-21 14:13:54', '192.168.2.119', '33', '0', '0', null, '177', '117');
INSERT INTO `ec_user` VALUES ('179', 'cairui', '0c73c5a91374dc3a19f6b7f68b3b6af2', '蔡瑞', '', '2015-08-18 15:01:34', '192.168.8.1', '2015-08-28 11:23:07', '218.75.56.242', '2015-08-21 11:39:38', '218.75.56.242', '2', '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('180', 'zhangxiaofeng', '0c73c5a91374dc3a19f6b7f68b3b6af2', '张小峰', '', '2015-08-18 15:01:50', '192.168.8.1', '2015-09-21 14:09:31', '192.168.2.119', '2015-09-21 14:06:00', '192.168.2.119', '18', '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('181', 'lixuancheng', '0c73c5a91374dc3a19f6b7f68b3b6af2', '李宣澄', '', '2015-08-18 15:02:13', '192.168.8.1', null, null, null, null, null, '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('182', 'songguoqiang1', '0c73c5a91374dc3a19f6b7f68b3b6af2', '宋国强主任', '', '2015-08-19 16:16:22', '192.168.8.1', '2015-09-10 15:55:14', '42.81.35.192', '2015-09-10 15:53:32', '42.81.35.192', '5', '0', '0', null, '165', '107');
INSERT INTO `ec_user` VALUES ('183', 'zouliyan', '0c73c5a91374dc3a19f6b7f68b3b6af2', '邹丽燕', '', '2015-08-20 19:40:43', '101.70.52.8', '2015-08-21 13:13:09', '218.75.56.242', null, null, '1', '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('184', 'liufen', '0c73c5a91374dc3a19f6b7f68b3b6af2', '刘芬', '', '2015-08-20 19:40:56', '101.70.52.8', null, null, null, null, null, '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('185', 'yangjiawei', '0c73c5a91374dc3a19f6b7f68b3b6af2', '杨佳伟', '', '2015-08-20 19:41:12', '101.70.52.8', null, null, null, null, null, '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('186', 'yanxuepeng', '0c73c5a91374dc3a19f6b7f68b3b6af2', '闫雪鹏', '', '2015-08-20 19:41:28', '101.70.52.8', null, null, null, null, null, '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('187', 'mawei', '0c73c5a91374dc3a19f6b7f68b3b6af2', '马伟', '', '2015-08-20 19:41:42', '101.70.52.8', null, null, null, null, null, '0', '0', null, '178', '117');
INSERT INTO `ec_user` VALUES ('188', 'jiaxiqing', '0c73c5a91374dc3a19f6b7f68b3b6af2', '贾喜庆', '', '2015-08-20 19:42:02', '101.70.52.8', '2015-08-21 19:57:13', '123.155.227.249', null, null, '1', '0', '0', null, '178', '117');

-- ----------------------------
-- Table structure for `ec_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `ec_user_role`;
CREATE TABLE `ec_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_ec_user_role` (`user_id`),
  KEY `fk_ec_role_user` (`role_id`),
  CONSTRAINT `ec_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `ec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ec_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ec_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_user_role
-- ----------------------------
INSERT INTO `ec_user_role` VALUES ('164', '151');
INSERT INTO `ec_user_role` VALUES ('165', '152');
INSERT INTO `ec_user_role` VALUES ('166', '153');
INSERT INTO `ec_user_role` VALUES ('167', '154');
INSERT INTO `ec_user_role` VALUES ('168', '168');
INSERT INTO `ec_user_role` VALUES ('169', '156');
INSERT INTO `ec_user_role` VALUES ('170', '157');
INSERT INTO `ec_user_role` VALUES ('171', '157');
INSERT INTO `ec_user_role` VALUES ('172', '158');
INSERT INTO `ec_user_role` VALUES ('173', '159');
INSERT INTO `ec_user_role` VALUES ('174', '160');
INSERT INTO `ec_user_role` VALUES ('175', '161');
INSERT INTO `ec_user_role` VALUES ('176', '162');
INSERT INTO `ec_user_role` VALUES ('177', '163');
INSERT INTO `ec_user_role` VALUES ('178', '164');
INSERT INTO `ec_user_role` VALUES ('179', '165');
INSERT INTO `ec_user_role` VALUES ('179', '166');
INSERT INTO `ec_user_role` VALUES ('179', '167');
INSERT INTO `ec_user_role` VALUES ('180', '165');
INSERT INTO `ec_user_role` VALUES ('180', '166');
INSERT INTO `ec_user_role` VALUES ('180', '167');
INSERT INTO `ec_user_role` VALUES ('181', '165');
INSERT INTO `ec_user_role` VALUES ('181', '166');
INSERT INTO `ec_user_role` VALUES ('181', '167');
INSERT INTO `ec_user_role` VALUES ('182', '155');
INSERT INTO `ec_user_role` VALUES ('183', '165');
INSERT INTO `ec_user_role` VALUES ('183', '166');
INSERT INTO `ec_user_role` VALUES ('183', '167');
INSERT INTO `ec_user_role` VALUES ('184', '165');
INSERT INTO `ec_user_role` VALUES ('185', '165');
INSERT INTO `ec_user_role` VALUES ('186', '165');
INSERT INTO `ec_user_role` VALUES ('187', '165');
INSERT INTO `ec_user_role` VALUES ('188', '165');

-- ----------------------------
-- Table structure for `ec_workflow_trace_task`
-- ----------------------------
DROP TABLE IF EXISTS `ec_workflow_trace_task`;
CREATE TABLE `ec_workflow_trace_task` (
  `trace_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '跟踪ID',
  `pid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '流程实例ID',
  `pdid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '流程定义ID',
  `deploy_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发布人ID',
  `current_handle_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '当前处理人id',
  `pre_handle_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '前处理人ID',
  `task_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节点ID',
  `pre_task_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '前节点ID',
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `handle_date` date DEFAULT NULL COMMENT '处理时间',
  `duration` int(11) DEFAULT NULL COMMENT '持续时间',
  `task_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务状态',
  PRIMARY KEY (`trace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_workflow_trace_task
-- ----------------------------

-- ----------------------------
-- Table structure for `material_type`
-- ----------------------------
DROP TABLE IF EXISTS `material_type`;
CREATE TABLE `material_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materialtypeid` varchar(20) NOT NULL DEFAULT '',
  `materialtypename` varchar(20) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of material_type
-- ----------------------------
INSERT INTO `material_type` VALUES ('1', 'CS', '碳钢', '2015-04-10 08:41:56', 'admin', '2015-04-10 01:15:27', 'admin');
INSERT INTO `material_type` VALUES ('2', 'SS', '不锈钢', null, null, '2015-06-03 22:21:46', 'admin');

-- ----------------------------
-- Table structure for `MONTH_DAY`
-- ----------------------------
DROP TABLE IF EXISTS `MONTH_DAY`;
CREATE TABLE `MONTH_DAY` (
  `DAY_NUM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of MONTH_DAY
-- ----------------------------
INSERT INTO `MONTH_DAY` VALUES ('1');
INSERT INTO `MONTH_DAY` VALUES ('2');
INSERT INTO `MONTH_DAY` VALUES ('3');
INSERT INTO `MONTH_DAY` VALUES ('4');
INSERT INTO `MONTH_DAY` VALUES ('5');
INSERT INTO `MONTH_DAY` VALUES ('6');
INSERT INTO `MONTH_DAY` VALUES ('7');
INSERT INTO `MONTH_DAY` VALUES ('8');
INSERT INTO `MONTH_DAY` VALUES ('9');
INSERT INTO `MONTH_DAY` VALUES ('10');
INSERT INTO `MONTH_DAY` VALUES ('11');
INSERT INTO `MONTH_DAY` VALUES ('12');
INSERT INTO `MONTH_DAY` VALUES ('13');
INSERT INTO `MONTH_DAY` VALUES ('14');
INSERT INTO `MONTH_DAY` VALUES ('15');
INSERT INTO `MONTH_DAY` VALUES ('16');
INSERT INTO `MONTH_DAY` VALUES ('17');
INSERT INTO `MONTH_DAY` VALUES ('18');
INSERT INTO `MONTH_DAY` VALUES ('19');
INSERT INTO `MONTH_DAY` VALUES ('20');
INSERT INTO `MONTH_DAY` VALUES ('21');
INSERT INTO `MONTH_DAY` VALUES ('22');
INSERT INTO `MONTH_DAY` VALUES ('23');
INSERT INTO `MONTH_DAY` VALUES ('24');
INSERT INTO `MONTH_DAY` VALUES ('25');
INSERT INTO `MONTH_DAY` VALUES ('26');
INSERT INTO `MONTH_DAY` VALUES ('27');
INSERT INTO `MONTH_DAY` VALUES ('28');
INSERT INTO `MONTH_DAY` VALUES ('29');
INSERT INTO `MONTH_DAY` VALUES ('30');
INSERT INTO `MONTH_DAY` VALUES ('31');

-- ----------------------------
-- Table structure for `price`
-- ----------------------------
DROP TABLE IF EXISTS `price`;
CREATE TABLE `price` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `speciality` varchar(20) DEFAULT NULL,
  `pricetype` varchar(20) DEFAULT NULL,
  `unitprice` decimal(10,2) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of price
-- ----------------------------
INSERT INTO `price` VALUES ('37', 'GDZJ', 'GS', '2.00', '', '2015-06-07 16:34:22', 'admin', null, null);
INSERT INTO `price` VALUES ('38', 'GDHK', 'GCL', '20.00', '', '2015-06-07 16:34:37', 'admin', null, null);
INSERT INTO `price` VALUES ('39', 'GDHK', 'DZ', '15.00', '', '2015-06-07 16:34:47', 'admin', null, null);
INSERT INTO `price` VALUES ('40', 'GDHK', 'GS', '10.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);
INSERT INTO `price` VALUES ('41', 'GDZJ', 'GCL', '4.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);
INSERT INTO `price` VALUES ('42', 'GDZJ', 'DZ', '3.00', '--由导入文件生成--', '2015-06-15 21:12:22', 'jhy', null, null);

-- ----------------------------
-- Table structure for `problem_concernman`
-- ----------------------------
DROP TABLE IF EXISTS `problem_concernman`;
CREATE TABLE `problem_concernman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concernmanid` int(11) NOT NULL,
  `problemid` int(11) NOT NULL,
  `concermanname` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=373 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of problem_concernman
-- ----------------------------
INSERT INTO `problem_concernman` VALUES ('367', '165', '229', '杨红芳');
INSERT INTO `problem_concernman` VALUES ('368', '182', '229', '宋国强');
INSERT INTO `problem_concernman` VALUES ('369', '182', '230', '宋国强主任');
INSERT INTO `problem_concernman` VALUES ('370', '165', '230', '杨红芳');
INSERT INTO `problem_concernman` VALUES ('371', '182', '231', '宋国强主任');
INSERT INTO `problem_concernman` VALUES ('372', '182', '232', '宋国强主任');

-- ----------------------------
-- Table structure for `problem_trigger_duration`
-- ----------------------------
DROP TABLE IF EXISTS `problem_trigger_duration`;
CREATE TABLE `problem_trigger_duration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duration` int(11) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of problem_trigger_duration
-- ----------------------------
INSERT INTO `problem_trigger_duration` VALUES ('2', '2', 'change_solver');

-- ----------------------------
-- Table structure for `query_date`
-- ----------------------------
DROP TABLE IF EXISTS `query_date`;
CREATE TABLE `query_date` (
  `select_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`select_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of query_date
-- ----------------------------
INSERT INTO `query_date` VALUES ('2015-07-01 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-02 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-03 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-04 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-05 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-06 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-07 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-08 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-09 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-10 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-11 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-12 00:00:00');
INSERT INTO `query_date` VALUES ('2015-07-13 00:00:00');

-- ----------------------------
-- Table structure for `rd_source`
-- ----------------------------
DROP TABLE IF EXISTS `rd_source`;
CREATE TABLE `rd_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '来源',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_source
-- ----------------------------
INSERT INTO `rd_source` VALUES ('1', '党政网（省委宣传部）');
INSERT INTO `rd_source` VALUES ('2', '四川省机要局（涉密文件）');
INSERT INTO `rd_source` VALUES ('3', '上级文件（市委、市政府）');
INSERT INTO `rd_source` VALUES ('4', '下级文件（区级宣传部）');
INSERT INTO `rd_source` VALUES ('5', '平级文件（市教育局、市公安局……）');

-- ----------------------------
-- Table structure for `rd_type`
-- ----------------------------
DROP TABLE IF EXISTS `rd_type`;
CREATE TABLE `rd_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_type
-- ----------------------------
INSERT INTO `rd_type` VALUES ('1', '通知');
INSERT INTO `rd_type` VALUES ('2', '公函');
INSERT INTO `rd_type` VALUES ('3', '通报');
INSERT INTO `rd_type` VALUES ('4', '公告');

-- ----------------------------
-- Table structure for `rd_workflow_file`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_file`;
CREATE TABLE `rd_workflow_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文件名',
  `file_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文号',
  `create_date` datetime NOT NULL COMMENT '签发日期',
  `file_source_id` int(11) NOT NULL COMMENT '文件来源ID',
  `file_type_id` int(11) NOT NULL COMMENT '文件类型ID',
  `description` text COLLATE utf8_unicode_ci COMMENT '任务描述',
  `rd_task_id` int(11) DEFAULT NULL COMMENT '任务ID',
  PRIMARY KEY (`id`),
  KEY `file_source_fk` (`file_source_id`),
  KEY `file_type_fk` (`file_type_id`),
  KEY `file_task_fk` (`rd_task_id`),
  CONSTRAINT `file_source_fk` FOREIGN KEY (`file_source_id`) REFERENCES `rd_source` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `file_task_fk` FOREIGN KEY (`rd_task_id`) REFERENCES `rd_workflow_trace_task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `file_type_fk` FOREIGN KEY (`file_type_id`) REFERENCES `rd_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_file
-- ----------------------------

-- ----------------------------
-- Table structure for `rd_workflow_flow_zip`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_flow_zip`;
CREATE TABLE `rd_workflow_flow_zip` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文件名',
  `realname` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '真实文件名',
  `path` varchar(500) COLLATE utf8_unicode_ci NOT NULL COMMENT '相对路径',
  `real_path` varchar(500) COLLATE utf8_unicode_ci NOT NULL COMMENT '真实相对路径',
  `upload_date` datetime NOT NULL COMMENT '上传时间',
  `version` int(11) NOT NULL COMMENT '版本',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `deployment_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发布ID',
  `deployment_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发布名称',
  `deployment_date` datetime DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_flow_zip
-- ----------------------------
INSERT INTO `rd_workflow_flow_zip` VALUES ('9', 'problemProcess', 'problemProcess.zip', '/WEB-INF/workflow/problemProcess.zip', '/WEB-INF/workflow/problemProcess.zip', '2015-07-23 22:35:25', '1', '1', '1', 'problemProcess', '2015-07-23 22:35:32');

-- ----------------------------
-- Table structure for `rd_workflow_handled_task`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_handled_task`;
CREATE TABLE `rd_workflow_handled_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `task_id` int(11) NOT NULL COMMENT '跟踪任务ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_handled_task
-- ----------------------------

-- ----------------------------
-- Table structure for `rd_workflow_trace_task`
-- ----------------------------
DROP TABLE IF EXISTS `rd_workflow_trace_task`;
CREATE TABLE `rd_workflow_trace_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `rd_file_id` int(11) NOT NULL COMMENT '执行文件ID',
  `deploy_user_id` int(11) NOT NULL COMMENT '发布人ID',
  `assignee_id` int(11) DEFAULT NULL COMMENT '当前处理人',
  `handle_date` datetime DEFAULT NULL COMMENT '处理时间',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `start_date` datetime DEFAULT NULL COMMENT '开始时间',
  `task_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节点名称',
  `task_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节点ID',
  `process_instance_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '流程定义ID',
  `finish_task_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '完成任务ID',
  `dispatch_user_id` int(11) DEFAULT NULL COMMENT '分配人ID',
  `handle_message` text COLLATE utf8_unicode_ci COMMENT '处理意见',
  `audit_message` text COLLATE utf8_unicode_ci COMMENT '审核意见',
  `table_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '表单名',
  `status` int(11) DEFAULT NULL COMMENT '任务状态',
  `duration` int(11) DEFAULT NULL COMMENT '持续时间',
  `complete` int(11) DEFAULT NULL COMMENT '任务完成状态',
  `department_id` int(11) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`),
  KEY `rd_file_id` (`rd_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rd_workflow_trace_task
-- ----------------------------

-- ----------------------------
-- Table structure for `rolling_plan`
-- ----------------------------
DROP TABLE IF EXISTS `rolling_plan`;
CREATE TABLE `rolling_plan` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `qualityplanno` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rollingplanflag` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weldlistno` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `drawno` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rccm` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `areano` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unitno` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weldno` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `speciality` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `materialtype` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workpoint` decimal(10,3) DEFAULT NULL,
  `qualitynum` decimal(10,3) DEFAULT NULL,
  `worktime` decimal(10,3) DEFAULT NULL,
  `consteam` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plandate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consdate` datetime DEFAULT NULL,
  `isend` int(11) NOT NULL,
  `consendman` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `welder` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `welddate` datetime DEFAULT NULL,
  `assigndate` datetime DEFAULT NULL,
  `doissuedate` datetime DEFAULT NULL,
  `plan_finish_date` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `qcsign` int(11) NOT NULL,
  `qcman` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qcdate` datetime DEFAULT NULL,
  `remark` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `technology_ask` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality_risk_ctl` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_risk_ctl` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `experience_feedback` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_tool` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=915 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rolling_plan
-- ----------------------------
INSERT INTO `rolling_plan` VALUES ('739', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3323.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0242', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '2', '170', null, null, '2015-08-24 10:35:36', null, '2015-08-25 00:00:00', '2015-09-10 15:35:28', '1', '张小峰', '2015-09-10 15:35:28', null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('741', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '2236.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0245', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '2', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', '2015-09-10 15:46:42', '1', 'limin', '2015-09-10 15:46:42', null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('742', 'BQP-DFMS-31UJA0821-30FAK40-0001', 'PROBLEM', '3369.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0269', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', '2015-09-10 16:13:58', '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('743', 'BQP-DFMS-31UJA0821-30FAK40-0001', 'PROBLEM', '3373.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0276', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', '2015-09-10 16:17:41', '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('744', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3374.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0278', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('745', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3375.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0281', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('746', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3384.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0280', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('747', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3385.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0284', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('748', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3386.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0285', 'GDHK', 'SS', null, '159.000', null, '109', '2015-09-01至2015-09-01', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('749', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3196.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0165', 'GDHK', 'SS', null, '273.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('750', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3213.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0182', 'GDHK', 'SS', null, '273.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('751', 'BQP-DFMS-31UJA0821-30FAK40-0001', null, '3214.0', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'QA2', '30FAK40', '3', 'M0183', 'GDHK', 'SS', null, '273.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('752', 'BQP-DFMS-PJA0821-D-30KBA69-0001', null, '4325.0', 'LYG-3-PD22-31-1UJA0821-DG-0018-S', 'QA2', '30KBA69', '3', 'M0244', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('753', 'BQP-DFMS-PJA0821-D-30KBA69-0001', null, '4326.0', 'LYG-3-PD22-31-1UJA0821-DG-0018-S', 'QA2', '30KBA69', '3', 'M0243', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('754', 'BQP-DFMS-PJA0821-D-30KBA69-0001', null, '4327.0', 'LYG-3-PD22-31-1UJA0821-DG-0018-S', 'QA2', '30KBA69', '3', 'M0241', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('755', 'BQP-DFMS-PJA0821-D-30KBA69-0001', null, '4336.0', 'LYG-3-PD22-31-1UJA0821-DG-0018-S', 'QA2', '30KBA69', '3', 'M0240', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('756', 'BQP-DFMS-31UJA0821-30KBA10-0001', null, '4471.0', 'LYG-3-PD22-31-1UJA0821-DG-0019-S', 'QA2', '30KBA10', '3', 'M0055', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('757', 'BQP-DFMS-31UJA0821-30KBA10-0001', null, '4472.0', 'LYG-3-PD22-31-1UJA0821-DG-0019-S', 'QA2', '30KBA10', '3', 'M0056', 'GDHK', 'SS', null, '133.000', null, '109', '2015-09-02至2015-09-03', '2015-08-20 00:00:00', '1', '170', null, null, '2015-08-24 10:35:54', null, '2015-08-25 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('759', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56098.0', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0028', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('760', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56197', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0058', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('761', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56200', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0075', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('762', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56202', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0077', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('763', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56203', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0078', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('764', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56222', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0080', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('765', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56227', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0085', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('766', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56228.0', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0087', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('767', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56076', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0089', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('768', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56789', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0090', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('769', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56456', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0095', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('770', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56790', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0098', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('771', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '69803', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0100', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('772', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56794', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0101', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('773', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56814', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0104', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('774', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56815', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0105', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('775', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56348', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0110', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('776', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56351', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0112', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('777', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56355', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0114', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('778', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56356', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0115', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:38', null, '2015-09-18 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('779', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56367', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0118', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('780', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '69806', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0119', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('781', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56459', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0122', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('782', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56460', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0123', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('783', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56470', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0125', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('784', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56473', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0127', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('785', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56481', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0133', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('786', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56482', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0134', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('787', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56484', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0136', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('788', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56499', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0138', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('789', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56338', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0152', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('790', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56399', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0157', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('791', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56406', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0159', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('792', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56417', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0164', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('793', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56418', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0165', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('794', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56373', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0197', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('795', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56375', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0199', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('796', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56380', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0201', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('797', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56389', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0202', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('798', 'BQP-DFMS-31UCB0152-30SGA06-0001', null, '56390', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0203', 'GDHK', 'CS', null, '108.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 08:59:54', null, '2015-09-11 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('799', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56641', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0237', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('800', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56644', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0239', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('801', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56700', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0241', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-14至2015-09-18', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('802', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56698', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0243', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('803', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56695', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0245', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('804', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56694', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0246', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('805', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56692', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0248', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('806', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56691', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0249', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('807', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56690', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0250', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('808', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56688', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0252', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('809', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56687', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0253', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('810', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56683', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0255', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('811', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56681', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0256', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('812', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56706', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0295', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('813', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56712', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0299', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('814', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56713', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0300', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('815', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56716', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0303', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('816', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56717', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0304', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('817', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56754', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0306', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('818', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56765', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0308', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-16至2015-09-20', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:08', null, '2015-09-20 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('819', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54316', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0396', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('820', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54421', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0406', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('821', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54429', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0407', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('822', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54470', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0412', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('823', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54479', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0413', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('824', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56820', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0537', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('825', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56845', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0542', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('826', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56847', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0544', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('827', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54208', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0546', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('828', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56849', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0548', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('829', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56850', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0549', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('830', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56852', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0551', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('831', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56861', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0552', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('832', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56862', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0553', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('833', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56863', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0554', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('834', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56866', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0557', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('835', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56871', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0560', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('836', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56869', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0561', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('837', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54285', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0616', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('838', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54298', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0619', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-10至2015-09-12', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:21', null, '2015-09-10 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('839', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54300', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0620', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('840', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54315', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0622', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('841', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54325', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0624', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('842', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54338', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0627', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('843', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56874', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0629', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('844', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '70827', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0740', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('845', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54908', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0741', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('846', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54918', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0742', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('847', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54924', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0744', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('848', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54982', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0746', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('849', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54984', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0748', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('850', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '70889', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0751', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('851', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55041', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0752', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('852', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54580', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0781', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('853', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54582', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0782', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('854', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54592', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0784', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('855', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54593', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0785', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('856', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54619', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0786', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('857', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54624', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0788', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('858', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54533', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0794', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-07至2015-09-09', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:32', null, '2015-09-09 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('859', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '54549', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0796', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('860', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55615', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0943', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('861', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55600', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0947', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('862', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55598', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0948', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('863', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55591', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0951', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('864', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55586', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0953', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('865', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55584', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0954', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('866', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55552', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0956', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('867', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55539', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0958', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('868', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55535', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0959', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('869', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55514', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0963', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('870', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55513', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0964', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('871', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55512', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0965', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('872', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55509', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0968', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('873', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55502', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0969', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('874', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55499', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0971', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('875', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55498', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0972', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('876', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55494', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0975', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('877', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55493', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0976', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('878', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55472', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0979', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-11至2015-09-15', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:49', null, '2015-09-15 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('879', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '55471', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0980', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('880', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55467', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0997', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('881', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55466', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M0998', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('882', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55429', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1000', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('883', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55419', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1002', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('884', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55416', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1003', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('885', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55413', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1004', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('886', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55402', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1006', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('887', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55401', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1007', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('888', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55367', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1010', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('889', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55347', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1015', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('890', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55344', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1017', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('891', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55341', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1019', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('892', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55331', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1020', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('893', 'BQP-DFMS-31UCB0152-30SGA10-0001', null, '55327', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1021', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('894', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56312', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1258', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('895', 'BQP-DFMS-31UCB0152-30SGA50-0001', null, '56308', 'LYG-3-PD22-31-1UCB0152-DG-0004-S', 'QA3', 'SGA', '3', 'M1260', 'GDHK', 'CS', null, '89.000', null, '109', '2015-09-12至2015-09-16', '2015-09-01 00:00:00', '1', '171', null, null, '2015-09-07 09:00:58', null, '2015-09-16 00:00:00', null, '0', null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `rolling_plan` VALUES ('896', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M1', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('897', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M10', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('898', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001F', 'GDZJ', 'CS', '200.000', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('899', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M2', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('900', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M3', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('901', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M4', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('902', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M5', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('903', 'NZPT-TQP-0DY-JPD-P-20519', null, null, '07060DY-JPS01-JPD-003', 'R1', 'RX', '1', 'R002.001x', 'GDZJ', 'CS', '200.000', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('904', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M6', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('905', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M7', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('906', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M8', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('907', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M9', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('908', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M16', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('909', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M11', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('910', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M12', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('911', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M13', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('912', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00518', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M14', 'GDHK', 'CS', '0.325', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('913', 'NZPT-TQP-0DY-JPD-P-20519', null, 'HJKZ-TQP-20519-00398', '07060DY-JPS01-JPD-003', 'NA', 'DY', '0', 'M15', 'GDHK', 'CS', '0.780', '1.000', '1.000', null, null, '2015-03-15 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `rolling_plan` VALUES ('914', 'BQP-DFMS-31UKA0021-30KBC84-0001', null, '23786', 'LYG-3-PD22-31-1UKA0021-DG-0024-S', 'QA3', '30KBC84', '3', 'M0264', 'GDHK', 'CS', null, '219.000', null, null, null, '2015-09-17 00:00:00', '0', null, null, null, null, null, null, null, '0', null, null, null, null, null, null, null, null, '2015-09-17 10:47:12', 'admin', null, null);

-- ----------------------------
-- Table structure for `rolling_plan_question`
-- ----------------------------
DROP TABLE IF EXISTS `rolling_plan_question`;
CREATE TABLE `rolling_plan_question` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `autoidup` int(11) NOT NULL,
  `weldno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `drawno` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `questiondes` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `questionname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isok` int(11) NOT NULL,
  `queslevel` int(11) NOT NULL,
  `solvemethod` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `methodmanid` int(11) DEFAULT NULL,
  `confirm` int(11) DEFAULT NULL,
  `concernman` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `solvedate` datetime DEFAULT NULL,
  `currentsolver` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentsolverid` int(11) DEFAULT NULL,
  `concernmanname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rolling_plan_id` int(11) DEFAULT NULL,
  `solver_id` int(11) DEFAULT NULL,
  `created_by_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rolling_plan_question
-- ----------------------------
INSERT INTO `rolling_plan_question` VALUES ('230', '0', 'M0244', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', 'ggh', 'ggd', '0', '2', null, null, '2015-09-10 15:47:50', '170', '2015-09-10 15:55:43', null, '0', '0', null, null, '蒋秋菊', '169', '宋国强主任|杨红芳', '740', '349', null);
INSERT INTO `rolling_plan_question` VALUES ('231', '0', 'M0269', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', '弯头壁厚为9毫米，管子壁厚为5毫米无法组对。', '壁厚不一样，无法组对', '0', '0', null, null, '2015-09-10 16:13:58', '170', null, null, '0', '0', null, null, '宋国强', '169', '宋国强主任', '742', '350', null);
INSERT INTO `rolling_plan_question` VALUES ('232', '0', 'M0276', 'LYG-3-PD22-31-1UJA0821-DG-0008-S', '管子内部有缺陷。请确定是否照用。', '管子内部缺陷', '0', '0', null, null, '2015-09-10 16:17:41', '170', null, null, '0', '0', null, null, '宋国强', '169', '宋国强主任', '743', '351', null);

-- ----------------------------
-- Table structure for `solve_question_person`
-- ----------------------------
DROP TABLE IF EXISTS `solve_question_person`;
CREATE TABLE `solve_question_person` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `problemid` int(11) NOT NULL,
  `stepno` int(11) NOT NULL,
  `okmanid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okdes` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okdate` datetime DEFAULT NULL,
  `isok` int(11) NOT NULL,
  `ison` int(11) DEFAULT NULL,
  `oklevel` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okmanname` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of solve_question_person
-- ----------------------------
INSERT INTO `solve_question_person` VALUES ('347', '230', '0', '169', '技术问题', null, '0', '0', null, '2015-09-10 15:47:50', 'limin', null, null, '宋国强');
INSERT INTO `solve_question_person` VALUES ('348', '230', '0', '182', '请处理', null, '0', '0', null, '2015-09-10 15:55:00', 'songguoqiang', null, null, '宋国强主任');
INSERT INTO `solve_question_person` VALUES ('349', '230', '0', '172', null, null, '0', '0', null, '2015-09-10 15:55:43', 'songguoqiang1', null, null, '蒋秋菊');
INSERT INTO `solve_question_person` VALUES ('350', '231', '0', '169', null, null, '0', '0', null, '2015-09-10 16:13:58', 'limin', null, null, '宋国强');
INSERT INTO `solve_question_person` VALUES ('351', '232', '0', '169', null, null, '0', '0', null, '2015-09-10 16:17:41', 'limin', null, null, '宋国强');

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userpassword` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userdepartment` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userrole` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upleader` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rolelevel` int(11) DEFAULT NULL,
  `online` int(11) DEFAULT NULL,
  `isenable` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of staff
-- ----------------------------

-- ----------------------------
-- Table structure for `Test`
-- ----------------------------
DROP TABLE IF EXISTS `Test`;
CREATE TABLE `Test` (
  `a` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Test
-- ----------------------------

-- ----------------------------
-- Table structure for `user_device`
-- ----------------------------
DROP TABLE IF EXISTS `user_device`;
CREATE TABLE `user_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `device` varchar(50) DEFAULT NULL,
  `deviceid` varchar(80) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ud_user_id_fk` (`userid`),
  CONSTRAINT `ud_user_id_fk` FOREIGN KEY (`userid`) REFERENCES `ec_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_device
-- ----------------------------
INSERT INTO `user_device` VALUES ('138', '178', null, 'c45d6461f0d76f64', null, '2015-07-16 22:52:33', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('139', '178', null, '919d6e2ecd74766', null, '2015-07-16 22:52:40', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('140', '179', null, 'c45d6461f0d76f64', null, '2015-07-16 23:01:17', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('141', '173', null, 'c45d6461f0d76f64', null, '2015-07-16 23:14:17', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('142', '170', null, 'c45d6461f0d76f64', null, '2015-07-17 10:13:00', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('145', '174', null, 'c45d6461f0d76f64', null, '2015-07-17 21:17:37', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('157', '8', null, '919d6e2ecd74766', null, '2015-07-23 23:07:35', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('158', '8', null, 'c45d6461f0d76f64', null, '2015-07-23 23:08:11', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('199', '171', null, '7990361e50eb335b', null, '2015-08-18 16:08:37', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('200', '169', null, '7990361e50eb335b', null, '2015-08-18 16:09:26', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('201', '165', null, '7990361e50eb335b', null, '2015-08-18 16:10:45', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('202', '168', null, '7990361e50eb335b', null, '2015-08-18 16:11:05', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('203', '175', null, '7990361e50eb335b', null, '2015-08-18 16:23:48', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('204', '175', null, 'c45d6461f0d76f64', null, '2015-08-19 10:44:01', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('205', '169', null, 'fd48908acc87eee3', null, '2015-08-19 15:04:09', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('206', '182', null, '7990361e50eb335b', null, '2015-08-19 16:18:26', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('207', '188', null, 'b75a1f40bfb2cc10', null, '2015-08-21 19:57:13', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('208', '182', null, 'fd48908acc87eee3', null, '2015-08-24 09:58:29', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('209', '172', null, 'd1e2d190a33e1f14', null, '2015-08-24 10:31:48', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('210', '171', null, '3ec23742cbcc588c', null, '2015-08-26 13:31:55', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('211', '170', null, 'ddfc47bc14ce893d', null, '2015-08-26 13:34:44', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('212', '171', null, '36ef460b41086852', null, '2015-08-26 14:43:04', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('213', '171', null, 'c45d6461f0d76f64', null, '2015-08-26 16:38:26', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('214', '178', null, 'fd48908acc87eee3', null, '2015-08-28 10:58:24', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('215', '179', null, 'fd48908acc87eee3', null, '2015-08-28 11:23:07', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('216', '178', null, '7990361e50eb335b', null, '2015-08-28 11:35:10', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('217', '170', null, '7990361e50eb335b', null, '2015-08-28 11:36:45', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('218', '170', null, 'fd48908acc87eee3', null, '2015-08-28 13:07:17', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('219', '178', null, 'c8c2bcde8300c53c', null, '2015-08-28 16:33:24', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('220', '173', null, 'd5f68f53168ea1b7', null, '2015-09-01 08:34:28', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('221', '178', null, 'ddfc47bc14ce893d', null, '2015-09-01 09:09:43', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('222', '176', null, '98d28e42576a94ea', null, '2015-09-01 11:41:39', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('223', '171', null, '919d6e2ecd74766', null, '2015-09-01 20:58:38', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('224', '170', null, '919d6e2ecd74766', null, '2015-09-01 21:01:46', 'Core_API', null, null);
INSERT INTO `user_device` VALUES ('225', '180', null, '7990361e50eb335b', null, '2015-09-10 15:19:07', 'Core_API', null, null);

-- ----------------------------
-- Table structure for `variable_set`
-- ----------------------------
DROP TABLE IF EXISTS `variable_set`;
CREATE TABLE `variable_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setkey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `setvalue` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createon` datetime DEFAULT NULL,
  `createby` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updateon` datetime DEFAULT NULL,
  `updateby` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of variable_set
-- ----------------------------
INSERT INTO `variable_set` VALUES ('1', 'consteam', 'assigntype', '施工管理室', 'ACTIVE', null, null, '2015-08-18 15:56:52', 'admin');
INSERT INTO `variable_set` VALUES ('2', 'rollingPlan', 'planningtable', '双日滚动计划表', 'ACTIVE', null, null, '2015-06-01 17:28:40', 'admin');
INSERT INTO `variable_set` VALUES ('3', 'workStep', 'planningtable', '工序步骤表', 'ACTIVE', null, null, '2015-06-01 17:28:40', 'admin');
INSERT INTO `variable_set` VALUES ('4', 'GCL', 'price', '工程量', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('5', 'DZ', 'price', '点值', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('6', 'W', 'witnesstype', 'W级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('7', 'H', 'witnesstype', 'H级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('8', 'R', 'witnesstype', 'R级见证员', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('9', 'witnessTeam', 'witnesstype', '见证组组长', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('10', 'weldZJ', 'welddistinguish', 'GDZJ', 'ACTIVE', null, null, '2015-06-14 22:37:38', 'admin');
INSERT INTO `variable_set` VALUES ('11', 'weldHK', 'welddistinguish', 'GDHK', 'ACTIVE', null, null, '2015-06-14 22:37:38', 'admin');
INSERT INTO `variable_set` VALUES ('12', 'hysteresis', 'hysteresis', '1', 'ACTIVE', null, null, '2015-06-14 22:38:56', 'admin');
INSERT INTO `variable_set` VALUES ('13', 'changeSolver', 'problem', '3', 'ACTIVE', null, null, '2015-05-30 13:30:40', 'admin');
INSERT INTO `variable_set` VALUES ('14', 'GS', 'price', '工时', 'ACTIVE', null, null, '2015-06-07 17:21:19', 'admin');
INSERT INTO `variable_set` VALUES ('15', 'team', 'assigntype', '班长', 'ACTIVE', null, null, '2015-08-18 15:56:52', 'admin');
INSERT INTO `variable_set` VALUES ('16', 'endman', 'assigntype', '组长', 'ACTIVE', null, null, '2015-08-18 15:56:52', 'admin');
INSERT INTO `variable_set` VALUES ('17', 'witnessDepartment', 'witnesstype', '质检部', 'ACTIVE', null, null, '2015-07-22 17:27:17', 'admin');
INSERT INTO `variable_set` VALUES ('18', 'welder', 'welddistinguish', '焊接', 'ACTIVE', null, null, '2015-07-24 16:40:55', 'admin');

-- ----------------------------
-- Table structure for `work_step`
-- ----------------------------
DROP TABLE IF EXISTS `work_step`;
CREATE TABLE `work_step` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `autoidup` int(11) NOT NULL,
  `stepno` int(11) NOT NULL,
  `stepflag` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stepname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operater` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operatedesc` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operatedate` datetime DEFAULT NULL,
  `noticeainfo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noticeresult` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noticeresultdesc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noticeaqc1` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesseraqc1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdateaqc1` datetime DEFAULT NULL,
  `noticeaqc2` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesseraqc2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdateaqc2` datetime DEFAULT NULL,
  `noticeaqa` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesseraqa` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdateaqa` datetime DEFAULT NULL,
  `noticeb` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesserb` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdateb` datetime DEFAULT NULL,
  `noticec` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesserc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdatec` datetime DEFAULT NULL,
  `noticed` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnesserd` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdated` datetime DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoid`),
  KEY `work_step_rp_fk` (`autoidup`),
  CONSTRAINT `ws_rp_id_fk` FOREIGN KEY (`autoidup`) REFERENCES `rolling_plan` (`autoid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4545 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of work_step
-- ----------------------------
INSERT INTO `work_step` VALUES ('3518', '739', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:45:37', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:29:35', '170');
INSERT INTO `work_step` VALUES ('3519', '739', '2', 'DONE', '准备坡口及标识', '李敏', '合格', '2015-09-10 15:50:03', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:33:59', '170');
INSERT INTO `work_step` VALUES ('3520', '739', '3', 'DONE', '检查内部清洁度', '李敏', '合格', '2015-09-10 15:50:20', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:34:16', '170');
INSERT INTO `work_step` VALUES ('3521', '739', '4', 'DONE', '组对部件及检查', '李敏', '合格', '2015-09-10 15:50:34', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:34:30', '170');
INSERT INTO `work_step` VALUES ('3522', '739', '5', 'DONE', '焊接组装件', '李敏', '合格', '2015-09-10 15:50:49', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:34:44', '170');
INSERT INTO `work_step` VALUES ('3523', '739', '6', 'DONE', '焊缝目检、检查标记符合性', '李敏', '合格', '2015-09-10 15:51:32', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:35:28', '170');
INSERT INTO `work_step` VALUES ('3524', '741', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:46:26', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:30:24', '170');
INSERT INTO `work_step` VALUES ('3525', '741', '2', 'DONE', '准备坡口及标识', 'jj', '合格', '2015-09-10 15:53:47', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:45:18', '170');
INSERT INTO `work_step` VALUES ('3526', '741', '3', 'DONE', '检查内部清洁度', 'limin', '合格', '2015-09-10 15:54:38', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:46:08', '170');
INSERT INTO `work_step` VALUES ('3527', '741', '4', 'DONE', '组对部件及检查', 'limin', '合格', '2015-09-10 15:54:45', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:46:16', '170');
INSERT INTO `work_step` VALUES ('3528', '741', '5', 'DONE', '焊接组装件', 'limin', '合格', '2015-09-10 15:54:53', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:46:24', '170');
INSERT INTO `work_step` VALUES ('3529', '741', '6', 'DONE', '焊缝目检、检查标记符合性', 'limin', '合格', '2015-09-10 15:55:12', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:46:42', '170');
INSERT INTO `work_step` VALUES ('3530', '742', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:47:13', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:31:11', '170');
INSERT INTO `work_step` VALUES ('3531', '742', '2', 'DONE', '准备坡口及标识', '李敏', '合格', '2015-09-10 16:26:22', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 16:10:00', '170');
INSERT INTO `work_step` VALUES ('3532', '742', '3', 'DONE', '检查内部清洁度', '李敏', '合格', '2015-09-10 16:26:38', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 16:10:17', '170');
INSERT INTO `work_step` VALUES ('3533', '742', '4', 'PREPARE', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-07 11:37:00', '180');
INSERT INTO `work_step` VALUES ('3534', '742', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-07 11:37:08', '180');
INSERT INTO `work_step` VALUES ('3535', '742', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-07 11:37:13', '180');
INSERT INTO `work_step` VALUES ('3536', '743', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:47:41', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 15:31:38', '170');
INSERT INTO `work_step` VALUES ('3537', '743', '2', 'DONE', '准备坡口及标识', '李敏', '合格', '2015-09-10 16:31:18', null, '3', '', 'W', '179', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 16:14:54', '170');
INSERT INTO `work_step` VALUES ('3538', '743', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, '3', 'ddd', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 16:05:56', '8');
INSERT INTO `work_step` VALUES ('3539', '743', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:25', 'chaifeng', '2015-09-10 16:06:31', '8');
INSERT INTO `work_step` VALUES ('3540', '743', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-07 11:36:56', '180');
INSERT INTO `work_step` VALUES ('3541', '743', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-07 11:36:35', '180');
INSERT INTO `work_step` VALUES ('3542', '744', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3543', '744', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3544', '744', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3545', '744', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3546', '744', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3547', '744', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3548', '745', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3549', '745', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3550', '745', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3551', '745', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3552', '745', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3553', '745', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3554', '746', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:48:45', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:42', '170');
INSERT INTO `work_step` VALUES ('3555', '746', '2', 'PREPARE', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3556', '746', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3557', '746', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3558', '746', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3559', '746', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3560', '747', '1', 'DONE', '按长度下料', '李敏', '合格', '2015-09-10 15:49:13', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:33:10', '170');
INSERT INTO `work_step` VALUES ('3561', '747', '2', 'PREPARE', '准备坡口及标识', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:35', '180');
INSERT INTO `work_step` VALUES ('3562', '747', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:43', '180');
INSERT INTO `work_step` VALUES ('3563', '747', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:28:07', '180');
INSERT INTO `work_step` VALUES ('3564', '747', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:28:16', '180');
INSERT INTO `work_step` VALUES ('3565', '747', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:28:23', '180');
INSERT INTO `work_step` VALUES ('3566', '748', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3567', '748', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3568', '748', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3569', '748', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3570', '748', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3571', '748', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3572', '749', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:40', '180');
INSERT INTO `work_step` VALUES ('3573', '749', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:46', '180');
INSERT INTO `work_step` VALUES ('3574', '749', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:51', '180');
INSERT INTO `work_step` VALUES ('3575', '749', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:55', '180');
INSERT INTO `work_step` VALUES ('3576', '749', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:00', '180');
INSERT INTO `work_step` VALUES ('3577', '749', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:05', '180');
INSERT INTO `work_step` VALUES ('3578', '750', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:18', '180');
INSERT INTO `work_step` VALUES ('3579', '750', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:09', '180');
INSERT INTO `work_step` VALUES ('3580', '750', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:05', '180');
INSERT INTO `work_step` VALUES ('3581', '750', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:30:00', '180');
INSERT INTO `work_step` VALUES ('3582', '750', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:29:54', '180');
INSERT INTO `work_step` VALUES ('3583', '750', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:10', '180');
INSERT INTO `work_step` VALUES ('3584', '751', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:14', '180');
INSERT INTO `work_step` VALUES ('3585', '751', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:18', '180');
INSERT INTO `work_step` VALUES ('3586', '751', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:23', '180');
INSERT INTO `work_step` VALUES ('3587', '751', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:23', '180');
INSERT INTO `work_step` VALUES ('3588', '751', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:19', '180');
INSERT INTO `work_step` VALUES ('3589', '751', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:16', '180');
INSERT INTO `work_step` VALUES ('3590', '752', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:13', '180');
INSERT INTO `work_step` VALUES ('3591', '752', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:11', '180');
INSERT INTO `work_step` VALUES ('3592', '752', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:05', '180');
INSERT INTO `work_step` VALUES ('3593', '752', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:32:02', '180');
INSERT INTO `work_step` VALUES ('3594', '752', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:59', '180');
INSERT INTO `work_step` VALUES ('3595', '752', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:53', '180');
INSERT INTO `work_step` VALUES ('3596', '753', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:56', '180');
INSERT INTO `work_step` VALUES ('3597', '753', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:47', '180');
INSERT INTO `work_step` VALUES ('3598', '753', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:50', '180');
INSERT INTO `work_step` VALUES ('3599', '753', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:42', '180');
INSERT INTO `work_step` VALUES ('3600', '753', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:36', '180');
INSERT INTO `work_step` VALUES ('3601', '753', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:31:32', '180');
INSERT INTO `work_step` VALUES ('3602', '754', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:51', '180');
INSERT INTO `work_step` VALUES ('3603', '754', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:57', '180');
INSERT INTO `work_step` VALUES ('3604', '754', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:04', '180');
INSERT INTO `work_step` VALUES ('3605', '754', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:10', '180');
INSERT INTO `work_step` VALUES ('3606', '754', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:17', '180');
INSERT INTO `work_step` VALUES ('3607', '754', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:25:23', '180');
INSERT INTO `work_step` VALUES ('3608', '755', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:09', '180');
INSERT INTO `work_step` VALUES ('3609', '755', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:15', '180');
INSERT INTO `work_step` VALUES ('3610', '755', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:22', '180');
INSERT INTO `work_step` VALUES ('3611', '755', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:30', '180');
INSERT INTO `work_step` VALUES ('3612', '755', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:37', '180');
INSERT INTO `work_step` VALUES ('3613', '755', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:24:43', '180');
INSERT INTO `work_step` VALUES ('3614', '756', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:26', 'chaifeng', '2015-09-10 15:23:17', '180');
INSERT INTO `work_step` VALUES ('3615', '756', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:23:27', '180');
INSERT INTO `work_step` VALUES ('3616', '756', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:23:33', '180');
INSERT INTO `work_step` VALUES ('3617', '756', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:23:39', '180');
INSERT INTO `work_step` VALUES ('3618', '756', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:23:48', '180');
INSERT INTO `work_step` VALUES ('3619', '756', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:23:55', '180');
INSERT INTO `work_step` VALUES ('3620', '757', '1', 'PREPARE', '按长度下料', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-07 11:29:40', '180');
INSERT INTO `work_step` VALUES ('3621', '757', '2', 'UNDO', '准备坡口及标识', null, null, null, null, '3', '', 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-07 11:29:35', '180');
INSERT INTO `work_step` VALUES ('3622', '757', '3', 'UNDO', '检查内部清洁度', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-07 11:29:30', '180');
INSERT INTO `work_step` VALUES ('3623', '757', '4', 'UNDO', '组对部件及检查', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-07 11:29:24', '180');
INSERT INTO `work_step` VALUES ('3624', '757', '5', 'UNDO', '焊接组装件', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-07 11:29:14', '180');
INSERT INTO `work_step` VALUES ('3625', '757', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', '180', null, 'W', '183', null, null, null, null, 'W', '186', null, null, null, null, null, null, null, '2015-08-24 08:58:27', 'chaifeng', '2015-09-10 15:24:02', '180');
INSERT INTO `work_step` VALUES ('3632', '759', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3633', '759', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3634', '759', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3635', '759', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3636', '759', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3637', '759', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3638', '760', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:55:17', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-07 14:47:03', '171');
INSERT INTO `work_step` VALUES ('3639', '760', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:50:55', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-10 16:41:59', '171');
INSERT INTO `work_step` VALUES ('3640', '760', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3641', '760', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3642', '760', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3643', '760', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3644', '761', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:36:21', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-07 14:28:15', '171');
INSERT INTO `work_step` VALUES ('3645', '761', '2', 'PREPARE', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3646', '761', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3647', '761', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3648', '761', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3649', '761', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3650', '762', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3651', '762', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3652', '762', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3653', '762', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3654', '762', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3655', '762', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3656', '763', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3657', '763', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3658', '763', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3659', '763', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3660', '763', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3661', '763', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3662', '764', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3663', '764', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3664', '764', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3665', '764', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3666', '764', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3667', '764', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3668', '765', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3669', '765', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3670', '765', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3671', '765', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3672', '765', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3673', '765', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3674', '766', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3675', '766', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3676', '766', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3677', '766', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3678', '766', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3679', '766', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3680', '767', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3681', '767', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3682', '767', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3683', '767', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3684', '767', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3685', '767', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3686', '768', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:56:19', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-07 14:48:04', '171');
INSERT INTO `work_step` VALUES ('3687', '768', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:49:58', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-10 16:41:03', '171');
INSERT INTO `work_step` VALUES ('3688', '768', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3689', '768', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3690', '768', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3691', '768', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3692', '769', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3693', '769', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3694', '769', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3695', '769', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3696', '769', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3697', '769', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3698', '770', '1', 'DONE', '按长度下料', '熊占文', '点口', '2015-09-07 09:24:52', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-07 09:16:24', '171');
INSERT INTO `work_step` VALUES ('3699', '770', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:52:23', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', '2015-09-10 16:43:26', '171');
INSERT INTO `work_step` VALUES ('3700', '770', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3701', '770', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3702', '770', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3703', '770', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3704', '771', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3705', '771', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3706', '771', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3707', '771', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3708', '771', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3709', '771', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3710', '772', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3711', '772', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3712', '772', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3713', '772', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:26', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3714', '772', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3715', '772', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3716', '773', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3717', '773', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3718', '773', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3719', '773', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3720', '773', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3721', '773', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3722', '774', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3723', '774', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3724', '774', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3725', '774', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3726', '774', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3727', '774', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3728', '775', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3729', '775', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3730', '775', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3731', '775', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3732', '775', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3733', '775', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3734', '776', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3735', '776', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3736', '776', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3737', '776', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3738', '776', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3739', '776', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3740', '777', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3741', '777', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3742', '777', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3743', '777', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3744', '777', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3745', '777', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3746', '778', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3747', '778', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3748', '778', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3749', '778', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3750', '778', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3751', '778', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3752', '779', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3753', '779', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3754', '779', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3755', '779', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3756', '779', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3757', '779', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3758', '780', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3759', '780', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3760', '780', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3761', '780', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3762', '780', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3763', '780', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3764', '781', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3765', '781', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3766', '781', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3767', '781', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3768', '781', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3769', '781', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3770', '782', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3771', '782', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3772', '782', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3773', '782', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3774', '782', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3775', '782', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3776', '783', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:54:04', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', '2015-09-07 14:45:50', '171');
INSERT INTO `work_step` VALUES ('3777', '783', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:51:49', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', '2015-09-10 16:42:52', '171');
INSERT INTO `work_step` VALUES ('3778', '783', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3779', '783', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3780', '783', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3781', '783', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3782', '784', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3783', '784', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3784', '784', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3785', '784', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3786', '784', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3787', '784', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3788', '785', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3789', '785', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3790', '785', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3791', '785', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3792', '785', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3793', '785', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3794', '786', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3795', '786', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3796', '786', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3797', '786', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3798', '786', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3799', '786', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3800', '787', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3801', '787', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3802', '787', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3803', '787', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3804', '787', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3805', '787', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3806', '788', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3807', '788', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3808', '788', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:27', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3809', '788', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3810', '788', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3811', '788', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3812', '789', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3813', '789', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3814', '789', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3815', '789', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3816', '789', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3817', '789', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3818', '790', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3819', '790', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3820', '790', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3821', '790', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3822', '790', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3823', '790', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3824', '791', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3825', '791', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3826', '791', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3827', '791', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3828', '791', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3829', '791', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3830', '792', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3831', '792', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3832', '792', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3833', '792', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3834', '792', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3835', '792', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3836', '793', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:50:36', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', '2015-09-07 14:42:23', '171');
INSERT INTO `work_step` VALUES ('3837', '793', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:53:59', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', '2015-09-10 16:45:01', '171');
INSERT INTO `work_step` VALUES ('3838', '793', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3839', '793', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3840', '793', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3841', '793', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3842', '794', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3843', '794', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3844', '794', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3845', '794', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3846', '794', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3847', '794', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3848', '795', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3849', '795', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3850', '795', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3851', '795', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3852', '795', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3853', '795', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3854', '796', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:51:58', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', '2015-09-07 14:43:45', '171');
INSERT INTO `work_step` VALUES ('3855', '796', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:53:19', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', '2015-09-10 16:44:22', '171');
INSERT INTO `work_step` VALUES ('3856', '796', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3857', '796', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3858', '796', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3859', '796', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3860', '797', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3861', '797', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3862', '797', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3863', '797', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3864', '797', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3865', '797', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3866', '798', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3867', '798', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3868', '798', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3869', '798', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3870', '798', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3871', '798', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3872', '799', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3873', '799', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3874', '799', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3875', '799', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3876', '799', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3877', '799', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3878', '800', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3879', '800', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3880', '800', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3881', '800', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3882', '800', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3883', '800', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3884', '801', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3885', '801', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3886', '801', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3887', '801', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3888', '801', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3889', '801', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3890', '802', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3891', '802', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3892', '802', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3893', '802', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3894', '802', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3895', '802', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3896', '803', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3897', '803', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:28', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3898', '803', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3899', '803', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3900', '803', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3901', '803', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3902', '804', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3903', '804', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3904', '804', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3905', '804', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3906', '804', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3907', '804', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3908', '805', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3909', '805', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3910', '805', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3911', '805', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3912', '805', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3913', '805', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3914', '806', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3915', '806', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3916', '806', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3917', '806', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3918', '806', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3919', '806', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3920', '807', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3921', '807', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3922', '807', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3923', '807', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3924', '807', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3925', '807', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3926', '808', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3927', '808', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3928', '808', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3929', '808', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3930', '808', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3931', '808', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3932', '809', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3933', '809', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3934', '809', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3935', '809', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3936', '809', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3937', '809', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3938', '810', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:39:10', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', '2015-09-07 14:31:04', '171');
INSERT INTO `work_step` VALUES ('3939', '810', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:55:30', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', '2015-09-10 16:46:32', '171');
INSERT INTO `work_step` VALUES ('3940', '810', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3941', '810', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3942', '810', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3943', '810', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3944', '811', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:59:49', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', '2015-09-07 14:51:33', '171');
INSERT INTO `work_step` VALUES ('3945', '811', '2', 'PREPARE', '准备坡口及标识', null, null, null, null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', '2015-09-10 15:30:36', '180');
INSERT INTO `work_step` VALUES ('3946', '811', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3947', '811', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3948', '811', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3949', '811', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3950', '812', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3951', '812', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3952', '812', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3953', '812', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3954', '812', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3955', '812', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3956', '813', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3957', '813', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3958', '813', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3959', '813', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3960', '813', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3961', '813', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3962', '814', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3963', '814', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3964', '814', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3965', '814', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3966', '814', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3967', '814', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3968', '815', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3969', '815', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3970', '815', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:29', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3971', '815', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3972', '815', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3973', '815', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3974', '816', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3975', '816', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3976', '816', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3977', '816', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3978', '816', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3979', '816', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3980', '817', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3981', '817', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3982', '817', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3983', '817', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3984', '817', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3985', '817', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3986', '818', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3987', '818', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3988', '818', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3989', '818', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3990', '818', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3991', '818', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3992', '819', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3993', '819', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3994', '819', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3995', '819', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3996', '819', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3997', '819', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3998', '820', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('3999', '820', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4000', '820', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4001', '820', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4002', '820', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4003', '820', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4004', '821', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4005', '821', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4006', '821', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4007', '821', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4008', '821', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4009', '821', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4010', '822', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4011', '822', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4012', '822', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4013', '822', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4014', '822', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4015', '822', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4016', '823', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4017', '823', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4018', '823', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4019', '823', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4020', '823', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4021', '823', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4022', '824', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4023', '824', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4024', '824', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4025', '824', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4026', '824', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4027', '824', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4028', '825', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4029', '825', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4030', '825', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4031', '825', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4032', '825', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4033', '825', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4034', '826', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4035', '826', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4036', '826', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4037', '826', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4038', '826', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:30', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4039', '826', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4040', '827', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4041', '827', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4042', '827', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4043', '827', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4044', '827', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4045', '827', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4046', '828', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4047', '828', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4048', '828', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4049', '828', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4050', '828', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4051', '828', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4052', '829', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4053', '829', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4054', '829', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4055', '829', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4056', '829', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4057', '829', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4058', '830', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4059', '830', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4060', '830', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4061', '830', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4062', '830', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4063', '830', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4064', '831', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4065', '831', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4066', '831', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4067', '831', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4068', '831', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4069', '831', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4070', '832', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4071', '832', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4072', '832', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4073', '832', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4074', '832', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4075', '832', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4076', '833', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4077', '833', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4078', '833', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4079', '833', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4080', '833', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4081', '833', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4082', '834', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4083', '834', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4084', '834', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4085', '834', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4086', '834', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4087', '834', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4088', '835', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4089', '835', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4090', '835', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4091', '835', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4092', '835', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4093', '835', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4094', '836', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4095', '836', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:31', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4096', '836', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4097', '836', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4098', '836', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4099', '836', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4100', '837', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4101', '837', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4102', '837', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4103', '837', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4104', '837', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4105', '837', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4106', '838', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4107', '838', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4108', '838', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4109', '838', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4110', '838', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4111', '838', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4112', '839', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4113', '839', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4114', '839', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4115', '839', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4116', '839', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4117', '839', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4118', '840', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4119', '840', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4120', '840', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4121', '840', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4122', '840', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4123', '840', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4124', '841', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4125', '841', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4126', '841', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4127', '841', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4128', '841', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4129', '841', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4130', '842', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4131', '842', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4132', '842', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4133', '842', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4134', '842', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4135', '842', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4136', '843', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4137', '843', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4138', '843', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4139', '843', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4140', '843', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4141', '843', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4142', '844', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4143', '844', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4144', '844', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4145', '844', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4146', '844', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4147', '844', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4148', '845', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4149', '845', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4150', '845', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4151', '845', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4152', '845', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4153', '845', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:32', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4154', '846', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4155', '846', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4156', '846', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4157', '846', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4158', '846', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4159', '846', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4160', '847', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4161', '847', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4162', '847', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4163', '847', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4164', '847', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4165', '847', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4166', '848', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4167', '848', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4168', '848', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4169', '848', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4170', '848', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4171', '848', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4172', '849', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:46:35', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', '2015-09-07 14:38:25', '171');
INSERT INTO `work_step` VALUES ('4173', '849', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:56:28', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', '2015-09-10 16:47:29', '171');
INSERT INTO `work_step` VALUES ('4174', '849', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4175', '849', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4176', '849', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4177', '849', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4178', '850', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:41:58', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', '2015-09-07 14:33:50', '171');
INSERT INTO `work_step` VALUES ('4179', '850', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:56:03', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', '2015-09-10 16:47:05', '171');
INSERT INTO `work_step` VALUES ('4180', '850', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4181', '850', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4182', '850', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4183', '850', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4184', '851', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4185', '851', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4186', '851', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4187', '851', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4188', '851', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4189', '851', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4190', '852', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4191', '852', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4192', '852', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4193', '852', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4194', '852', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4195', '852', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4196', '853', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4197', '853', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4198', '853', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4199', '853', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4200', '853', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4201', '853', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4202', '854', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4203', '854', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4204', '854', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4205', '854', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4206', '854', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4207', '854', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4208', '855', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:33', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4209', '855', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4210', '855', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4211', '855', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4212', '855', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4213', '855', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4214', '856', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4215', '856', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4216', '856', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4217', '856', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4218', '856', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4219', '856', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4220', '857', '1', 'DONE', '按长度下料', '熊占文', '坡口加工及标识', '2015-09-07 14:49:03', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', '2015-09-07 14:40:52', '171');
INSERT INTO `work_step` VALUES ('4221', '857', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:54:51', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', '2015-09-10 16:45:53', '171');
INSERT INTO `work_step` VALUES ('4222', '857', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4223', '857', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4224', '857', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4225', '857', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4226', '858', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4227', '858', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4228', '858', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4229', '858', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4230', '858', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4231', '858', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4232', '859', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4233', '859', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4234', '859', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4235', '859', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4236', '859', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4237', '859', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4238', '860', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4239', '860', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4240', '860', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4241', '860', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4242', '860', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4243', '860', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4244', '861', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4245', '861', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4246', '861', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4247', '861', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4248', '861', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4249', '861', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4250', '862', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4251', '862', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4252', '862', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4253', '862', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4254', '862', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4255', '862', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4256', '863', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:58:23', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', '2015-09-07 14:50:07', '171');
INSERT INTO `work_step` VALUES ('4257', '863', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:47:53', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:34', 'chaifeng', '2015-09-10 16:38:59', '171');
INSERT INTO `work_step` VALUES ('4258', '863', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4259', '863', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4260', '863', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4261', '863', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4262', '864', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4263', '864', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4264', '864', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4265', '864', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4266', '864', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4267', '864', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4268', '865', '1', 'DONE', '按长度下料', '熊占文', '合格', '2015-09-07 14:57:27', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', '2015-09-07 14:49:12', '171');
INSERT INTO `work_step` VALUES ('4269', '865', '2', 'DONE', '准备坡口及标识', '熊占文', '合格', '2015-09-10 16:48:40', null, '3', '', 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', '2015-09-10 16:39:45', '171');
INSERT INTO `work_step` VALUES ('4270', '865', '3', 'PREPARE', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4271', '865', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4272', '865', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', '180', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4273', '865', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, '3', '', 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', '2015-09-11 13:37:34', '180');
INSERT INTO `work_step` VALUES ('4274', '866', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4275', '866', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4276', '866', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4277', '866', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4278', '866', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4279', '866', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4280', '867', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4281', '867', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4282', '867', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4283', '867', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4284', '867', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4285', '867', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4286', '868', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4287', '868', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4288', '868', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4289', '868', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4290', '868', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4291', '868', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4292', '869', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4293', '869', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4294', '869', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4295', '869', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4296', '869', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4297', '869', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4298', '870', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4299', '870', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4300', '870', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4301', '870', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4302', '870', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4303', '870', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:35', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4304', '871', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4305', '871', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4306', '871', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4307', '871', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4308', '871', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4309', '871', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4310', '872', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4311', '872', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4312', '872', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4313', '872', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4314', '872', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4315', '872', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4316', '873', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4317', '873', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4318', '873', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4319', '873', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4320', '873', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4321', '873', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4322', '874', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4323', '874', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4324', '874', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4325', '874', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4326', '874', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4327', '874', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4328', '875', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4329', '875', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4330', '875', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4331', '875', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4332', '875', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4333', '875', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4334', '876', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4335', '876', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4336', '876', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4337', '876', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4338', '876', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4339', '876', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4340', '877', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4341', '877', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4342', '877', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4343', '877', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4344', '877', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4345', '877', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4346', '878', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:36', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4347', '878', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4348', '878', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4349', '878', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4350', '878', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4351', '878', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4352', '879', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4353', '879', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4354', '879', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4355', '879', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4356', '879', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4357', '879', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4358', '880', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4359', '880', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4360', '880', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4361', '880', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4362', '880', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4363', '880', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4364', '881', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4365', '881', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4366', '881', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4367', '881', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4368', '881', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4369', '881', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4370', '882', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4371', '882', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4372', '882', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4373', '882', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4374', '882', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4375', '882', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4376', '883', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4377', '883', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4378', '883', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4379', '883', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4380', '883', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4381', '883', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4382', '884', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4383', '884', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4384', '884', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4385', '884', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4386', '884', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4387', '884', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4388', '885', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4389', '885', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4390', '885', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4391', '885', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4392', '885', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4393', '885', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:37', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4394', '886', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4395', '886', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4396', '886', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4397', '886', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4398', '886', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4399', '886', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4400', '887', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4401', '887', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4402', '887', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4403', '887', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4404', '887', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4405', '887', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4406', '888', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4407', '888', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4408', '888', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4409', '888', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4410', '888', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4411', '888', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4412', '889', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4413', '889', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4414', '889', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4415', '889', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4416', '889', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4417', '889', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4418', '890', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4419', '890', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4420', '890', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4421', '890', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4422', '890', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4423', '890', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4424', '891', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4425', '891', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4426', '891', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4427', '891', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4428', '891', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4429', '891', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4430', '892', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4431', '892', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4432', '892', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4433', '892', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4434', '892', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4435', '892', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4436', '893', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4437', '893', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:38', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4438', '893', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4439', '893', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4440', '893', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4441', '893', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4442', '894', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4443', '894', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4444', '894', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4445', '894', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4446', '894', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4447', '894', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4448', '895', '1', 'PREPARE', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4449', '895', '2', 'UNDO', '准备坡口及标识', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4450', '895', '3', 'UNDO', '检查内部清洁度', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4451', '895', '4', 'UNDO', '组对部件及检查', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4452', '895', '5', 'UNDO', '焊接组装件', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4453', '895', '6', 'UNDO', '焊缝目检、检查标记符合性', null, null, null, null, null, null, 'W', null, null, 'W', null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, '2015-09-07 08:58:39', 'chaifeng', null, null);
INSERT INTO `work_step` VALUES ('4454', '896', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4455', '896', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4456', '896', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4457', '896', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4458', '896', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4459', '896', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4460', '897', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:56', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4461', '897', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4462', '897', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4463', '897', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4464', '897', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4465', '897', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4466', '899', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4467', '899', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4468', '899', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4469', '899', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4470', '899', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4471', '899', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4472', '900', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4473', '900', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4474', '900', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4475', '900', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4476', '900', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4477', '900', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4478', '901', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4479', '901', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:57', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4480', '901', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4481', '901', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4482', '901', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4483', '901', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4484', '902', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4485', '902', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4486', '902', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4487', '902', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4488', '902', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4489', '902', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4490', '904', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4491', '904', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4492', '904', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4493', '904', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4494', '904', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4495', '904', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4496', '905', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4497', '905', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4498', '905', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:58', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4499', '905', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4500', '905', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4501', '905', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4502', '906', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4503', '906', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4504', '906', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4505', '906', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4506', '906', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4507', '906', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4508', '907', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4509', '907', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4510', '907', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4511', '907', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4512', '907', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4513', '907', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4514', '909', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4515', '909', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4516', '909', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:21:59', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4517', '909', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4518', '909', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4519', '909', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4520', '910', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4521', '910', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4522', '910', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4523', '910', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4524', '910', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4525', '910', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4526', '911', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4527', '911', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4528', '911', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4529', '911', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4530', '911', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4531', '911', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4532', '912', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4533', '912', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4534', '912', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:00', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4535', '912', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4536', '912', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4537', '912', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4538', '913', '1', 'UNDO', '坡口加工', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4539', '913', '2', 'UNDO', '内部清洁度检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4540', '913', '3', 'UNDO', '组对', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4541', '913', '4', 'UNDO', '焊缝标识、标记', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4542', '913', '5', 'UNDO', '焊接', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4543', '913', '6', 'UNDO', '焊缝外观检查', null, null, null, null, null, null, 'W', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:22:01', 'admin', null, null);
INSERT INTO `work_step` VALUES ('4544', '914', '1', 'UNDO', '按长度下料', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2015-09-17 10:47:12', 'admin', null, null);

-- ----------------------------
-- Table structure for `work_step_witness`
-- ----------------------------
DROP TABLE IF EXISTS `work_step_witness`;
CREATE TABLE `work_step_witness` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `parent_id` int(11) DEFAULT NULL,
  `stepno` int(11) NOT NULL COMMENT '工序步骤ID',
  `witness` int(11) DEFAULT NULL COMMENT '见证人ID',
  `witnesser` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessdes` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '工序完成说明',
  `witnessdate` datetime DEFAULT NULL,
  `noticeresultdesc` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessaddress` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '当前见证状态',
  `isok` int(11) DEFAULT NULL COMMENT '当前见证结果',
  `notice_point` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notice_type` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `triggerName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wsw_id_fk` (`parent_id`),
  KEY `wsw_ws_id_fk` (`stepno`),
  CONSTRAINT `wsw_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `work_step_witness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wsw_ws_id_fk` FOREIGN KEY (`stepno`) REFERENCES `work_step` (`autoid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=535 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of work_step_witness
-- ----------------------------
INSERT INTO `work_step_witness` VALUES ('360', null, '3518', '117', null, null, '2015-08-27 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:44:18', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('361', null, '3519', '117', null, null, '2015-08-27 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:45:11', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('362', null, '3520', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:45:27', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('363', null, '3521', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:45:46', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('364', null, '3522', '117', null, null, '2015-08-26 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:46:11', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('365', null, '3523', '117', null, null, '2015-08-27 12:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:46:24', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('366', null, '3524', '117', null, null, '2015-08-27 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:47:19', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('367', null, '3525', '117', null, null, '2015-08-27 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:47:32', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('368', null, '3526', '117', null, null, '2015-08-27 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:47:47', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('369', null, '3527', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:48:06', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('370', null, '3528', '117', null, null, '2015-08-27 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:48:25', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('371', null, '3529', '117', null, null, '2015-08-27 12:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:48:46', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('372', null, '3530', '117', null, null, '2015-08-27 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:49:39', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('373', null, '3531', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:50:01', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('374', null, '3532', '117', null, null, '2015-08-27 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:50:16', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('375', null, '3533', '117', null, null, '2015-08-27 12:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:50:58', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('376', null, '3534', '117', null, null, '2015-08-27 13:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:51:19', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('377', null, '3535', '117', null, null, '2015-08-27 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:52:01', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('378', null, '3536', '117', null, null, '2015-08-27 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 13:55:38', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('379', null, '3538', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 14:17:01', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('380', null, '3539', '117', null, null, '2015-08-27 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 14:17:13', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('381', null, '3537', '117', null, null, '2015-08-27 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 14:17:26', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('382', null, '3540', '117', null, null, '2015-08-27 12:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 14:17:51', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('383', null, '3541', '117', null, null, '2015-08-27 12:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-26 14:18:04', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('384', '360', '3518', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:03:45', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('385', '381', '3537', null, '179', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:19:20', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('386', '361', '3519', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:36', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('387', '362', '3520', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:39', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('388', '363', '3521', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:44', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('389', '364', '3522', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:48', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('390', '365', '3523', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:51', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('391', '366', '3524', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:54', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('392', '367', '3525', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:33:56', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('393', '368', '3526', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:17', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('394', '369', '3527', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:20', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('395', '370', '3528', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:23', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('396', '371', '3529', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:25', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('397', '372', '3530', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:28', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('398', '373', '3531', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:30', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('399', '374', '3532', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:36', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('400', '375', '3533', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:39', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('401', '376', '3534', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:43', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('402', '377', '3535', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:45', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('403', '378', '3536', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:49', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('404', '379', '3538', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:52', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('405', '380', '3539', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:54', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('406', '382', '3540', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:34:57', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('407', '383', '3541', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-08-28 11:35:00', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('408', null, '3554', '117', null, null, '2015-08-28 10:00:00', null, 'test', '1', '0', null, null, null, null, '2015-08-28 11:44:49', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('409', null, '3614', '117', null, null, '2015-08-29 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:44:31', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('410', null, '3615', '117', null, null, '2015-08-29 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:44:46', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('411', null, '3616', '117', null, null, '2015-08-29 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:45:02', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('412', null, '3617', '117', null, null, '2015-08-29 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:45:17', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('413', null, '3618', '117', null, null, '2015-08-29 13:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:45:31', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('414', null, '3619', '117', null, null, '2015-08-29 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:45:48', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('415', null, '3620', '117', null, null, '2015-08-29 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-08-28 13:52:40', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('416', null, '3621', '117', null, null, '2015-08-29 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-08-28 13:52:53', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('417', null, '3622', '117', null, null, '2015-08-29 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-08-28 13:53:04', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('418', null, '3623', '117', null, null, '2015-08-29 11:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-08-28 13:53:16', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('419', null, '3624', '117', null, null, '2015-08-29 13:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-08-28 13:53:30', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('420', null, '3625', '117', null, null, '2015-08-29 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:53:41', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('421', null, '3608', '117', null, null, '2015-08-29 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:54:41', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('422', null, '3609', '117', null, null, '2015-08-29 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:54:57', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('423', null, '3610', '117', null, null, '2015-08-29 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:55:10', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('424', null, '3611', '117', null, null, '2015-08-29 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:55:29', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('425', null, '3612', '117', null, null, '2015-08-29 13:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:55:42', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('426', null, '3613', '117', null, null, '2015-08-29 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:55:54', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('427', null, '3602', '117', null, null, '2015-08-29 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:56:30', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('428', null, '3603', '117', null, null, '2015-08-29 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:56:40', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('429', null, '3604', '117', null, null, '2015-08-29 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:56:51', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('430', null, '3605', '117', null, null, '2015-08-29 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:57:02', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('431', null, '3606', '117', null, null, '2015-08-29 13:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:57:15', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('432', null, '3607', '117', null, null, '2015-08-29 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-08-28 13:57:51', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('433', null, '3560', '117', null, null, '2015-09-05 08:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:10:48', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('434', null, '3561', '117', null, null, '2015-09-05 09:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:11:02', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('435', null, '3562', '117', null, null, '2015-09-05 10:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:11:15', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('436', null, '3563', '117', null, null, '2015-09-05 11:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:11:29', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('437', null, '3564', '117', null, null, '2015-09-05 13:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:11:40', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('438', null, '3565', '117', null, null, '2015-09-05 14:00:00', null, '管道车间', '1', '0', null, null, null, null, '2015-09-02 11:11:51', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('439', null, '3939', '117', null, null, '2015-09-11 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:33:02', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('440', null, '4179', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:36:48', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('441', null, '4173', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:39:43', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('442', null, '4221', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:41:39', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('443', null, '3837', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:43:22', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('444', null, '3855', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:44:24', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('445', null, '3699', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:45:24', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('446', null, '3777', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:46:27', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('447', null, '3639', '117', null, null, '2015-09-10 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:47:37', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('448', null, '3687', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:48:43', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('449', null, '4269', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:49:46', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('450', null, '4257', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-07 14:50:53', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('451', null, '3945', '117', null, null, '2015-09-10 10:00:00', null, '几天假他还会', '1', '0', null, null, null, null, '2015-09-07 14:52:09', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('452', '451', '3945', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:19', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('453', '450', '4257', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:23', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('454', '449', '4269', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:26', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('455', '448', '3687', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:29', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('456', '447', '3639', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:33', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('457', '446', '3777', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:54', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('458', '445', '3699', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:56:57', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('459', '444', '3855', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:57:01', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('460', '443', '3837', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:05', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('461', '442', '4221', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:07', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('462', '441', '4173', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:10', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('463', '440', '4179', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:14', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('464', '439', '3939', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:17', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('465', '438', '3565', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:20', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('466', '437', '3564', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:24', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('467', '436', '3563', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:27', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('468', '435', '3562', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:29', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('469', '434', '3561', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:33', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('470', '433', '3560', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:36', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('471', '432', '3607', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:38', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('472', '431', '3606', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:47', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('473', '430', '3605', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:51', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('474', '429', '3604', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:53', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('475', '428', '3603', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:56', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('476', '427', '3602', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:58:59', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('477', '426', '3613', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:02', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('478', '425', '3612', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:05', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('479', '424', '3611', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:07', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('480', '423', '3610', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:11', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('481', '421', '3608', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:19', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('482', '422', '3609', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:21', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('483', '420', '3625', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:34', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('484', '420', '3625', null, '183', null, null, null, null, '0', '0', 'A_QC2', 'W', null, null, '2015-09-07 14:59:34', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('485', '420', '3625', null, '186', null, null, null, null, '0', '0', 'B', 'W', null, null, '2015-09-07 14:59:34', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('486', '414', '3619', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:37', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('487', '413', '3618', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:41', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('488', '412', '3617', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:43', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('489', '411', '3616', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:46', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('490', '410', '3615', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:48', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('491', '409', '3614', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:51', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('492', '408', '3554', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-07 14:59:56', '178', null, null);
INSERT INTO `work_step_witness` VALUES ('493', null, '3572', '117', null, null, '2015-09-09 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:15:22', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('494', null, '3573', '117', null, null, '2015-09-09 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:15:34', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('495', null, '3574', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:15:47', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('496', null, '3575', '117', null, null, '2015-09-09 11:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:16:01', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('497', null, '3576', '117', null, null, '2015-09-09 12:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:16:13', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('498', null, '3577', '117', null, null, '2015-09-09 14:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:16:31', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('499', null, '3578', '117', null, null, '2015-09-09 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:17:29', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('500', null, '3579', '117', null, null, '2015-09-09 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:17:39', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('501', null, '3580', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:17:58', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('502', null, '3581', '117', null, null, '2015-09-09 11:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:18:08', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('503', null, '3582', '117', null, null, '2015-09-09 12:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:18:19', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('504', null, '3583', '117', null, null, '2015-09-09 14:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:18:30', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('505', null, '3584', '117', null, null, '2015-09-09 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:00', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('506', null, '3585', '117', null, null, '2015-09-09 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:10', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('507', null, '3586', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:18', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('508', null, '3587', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:28', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('509', null, '3588', '117', null, null, '2015-09-09 12:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:46', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('510', null, '3589', '117', null, null, '2015-09-09 13:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:19:58', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('511', null, '3590', '117', null, null, '2015-09-09 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:20:30', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('512', null, '3591', '117', null, null, '2015-09-09 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:20:40', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('513', null, '3592', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:20:52', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('514', null, '3593', '117', null, null, '2015-09-09 11:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:21:00', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('515', null, '3594', '117', null, null, '2015-09-09 12:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:21:10', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('516', null, '3595', '117', null, null, '2015-09-09 13:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:21:24', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('517', null, '3596', '117', null, null, '2015-09-09 08:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:22:15', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('518', null, '3597', '117', null, null, '2015-09-09 09:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:22:24', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('519', null, '3598', '117', null, null, '2015-09-09 10:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:22:33', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('520', null, '3599', '117', null, null, '2015-09-09 11:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:22:43', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('521', null, '3600', '117', null, null, '2015-09-09 12:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:22:58', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('522', null, '3601', '117', null, null, '2015-09-09 14:00:00', null, '管道车间', '0', '0', null, null, null, null, '2015-09-08 09:23:10', '170', null, null);
INSERT INTO `work_step_witness` VALUES ('523', null, '4258', '117', null, null, '2015-09-10 08:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 16:38:19', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('524', null, '3688', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 16:57:07', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('525', null, '3689', '117', null, null, '2015-09-08 09:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 16:57:23', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('526', null, '3690', '117', null, null, '2015-09-08 10:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 16:57:43', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('527', null, '4259', '117', null, null, '2015-09-08 09:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 16:59:45', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('528', null, '4260', '117', null, null, '2015-09-08 10:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 17:00:05', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('529', null, '4261', '117', null, null, '2015-09-08 11:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 17:00:25', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('530', null, '4270', '117', null, null, '2015-09-08 08:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 17:00:55', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('531', null, '4271', '117', null, null, '2015-09-08 11:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 17:01:13', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('532', null, '4272', '117', null, null, '2015-09-08 12:00:00', null, '碳钢车间', '1', '0', null, null, null, null, '2015-09-10 17:01:29', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('533', null, '4273', '117', null, null, '2015-09-08 13:00:00', null, '碳钢车间', '0', '0', null, null, null, null, '2015-09-10 17:01:48', '171', null, null);
INSERT INTO `work_step_witness` VALUES ('534', '532', '4272', null, '180', null, null, null, null, '0', '0', 'A_QC1', 'W', null, null, '2015-09-21 14:14:10', '178', null, null);

-- ----------------------------
-- Table structure for `worker_team`
-- ----------------------------
DROP TABLE IF EXISTS `worker_team`;
CREATE TABLE `worker_team` (
  `id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `consteamid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `worker` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of worker_team
-- ----------------------------

-- ----------------------------
-- Table structure for `workstep_problem_file`
-- ----------------------------
DROP TABLE IF EXISTS `workstep_problem_file`;
CREATE TABLE `workstep_problem_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `problemid` int(11) NOT NULL,
  `filepath` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `uploadtime` datetime NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of workstep_problem_file
-- ----------------------------
INSERT INTO `workstep_problem_file` VALUES ('90', '231', '_201509101613581441872838015.jpg', '2015-09-10 16:13:58', '20150618_132325');
INSERT INTO `workstep_problem_file` VALUES ('91', '231', '_201509101613581441872838015.jpg', '2015-09-10 16:13:58', '20150618_132301');
INSERT INTO `workstep_problem_file` VALUES ('92', '232', '_201509101617401441873060859.jpg', '2015-09-10 16:17:41', '20150717_100349');

-- ----------------------------
-- View structure for `shigongliang`
-- ----------------------------
DROP VIEW IF EXISTS `shigongliang`;
CREATE ALGORITHM=UNDEFINED DEFINER=`easycms_v2`@`%` SQL SECURITY DEFINER VIEW `shigongliang` AS select count(0) AS `total`,`rolling_plan`.`consendman` AS `consendman` from `rolling_plan` where (`rolling_plan`.`isend` = 1) group by `rolling_plan`.`consendman` ;

-- ----------------------------
-- View structure for `shigongwancheng`
-- ----------------------------
DROP VIEW IF EXISTS `shigongwancheng`;
CREATE ALGORITHM=UNDEFINED DEFINER=`easycms_v2`@`%` SQL SECURITY DEFINER VIEW `shigongwancheng` AS select count(0) AS `total`,`rolling_plan`.`consendman` AS `consendman` from `rolling_plan` where (`rolling_plan`.`isend` = 2) group by `rolling_plan`.`consendman` ;
