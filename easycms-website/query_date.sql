/*
Navicat MySQL Data Transfer

Source Server         : HD
Source Server Version : 50613
Source Host           : 10.0.1.103:3306
Source Database       : easycms_v2

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2015-06-26 16:26:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `query_date`
-- ----------------------------
DROP TABLE IF EXISTS `query_date`;
CREATE TABLE `query_date` (
  `select_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`select_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

