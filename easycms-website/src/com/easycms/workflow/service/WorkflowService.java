package com.easycms.workflow.service;

import java.util.List;

public interface WorkflowService{
	
	/**
	 * 根据角色ID查找用户ID
	 * @param roleId
	 * @return
	 */
	List<String> findUserId(Integer roleId);
	
	/**
	 * 查找部门下所有用户ID
	 * @param departmentId
	 * @return
	 */
	List<String> findUserIds(Integer departmentId);
	
	/**
	 * 查找部门下对应角色的所有用户ID
	 * @param departmentId
	 * @param roleId
	 * @return
	 */
	List<String> findUserIds(Integer departmentId,Integer roleId);
}
