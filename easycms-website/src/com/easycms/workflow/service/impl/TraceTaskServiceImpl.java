package com.easycms.workflow.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.workflow.dao.TraceTaskDao;
import com.easycms.workflow.domain.TraceTask;
import com.easycms.workflow.service.TraceTaskService;
import com.easycms.workflow.util.WorkflowHelper;

@Service("traceTaskService")
public class TraceTaskServiceImpl implements TraceTaskService{

	@Autowired
	private TraceTaskDao traceTaskDao;

	@Override
	public TraceTask addTraceTask(TraceTask traceTask) {
		return traceTaskDao.save(traceTask);
	}

	@Override
	public TraceTask getByTaskId(String taskId) {
		// TODO Auto-generated method stub
		return traceTaskDao.findByTaskId(taskId);
	}

	@Override
	public TraceTask updateTraceTask(TraceTask traceTask) {
		// TODO Auto-generated method stub
		return traceTaskDao.save(traceTask);
	}

	@Override
	public List<TraceTask> getAllByCurrentHanldeId(String currentHandleId) {
		// TODO Auto-generated method stub
		return traceTaskDao.findByCurrentHandleId(currentHandleId);
	}

	@Override
	public Page<TraceTask> getAllByCurrentHanldeId(String currentHandleId,
			Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setCurrentHandleId(currentHandleId);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<TraceTask> getFinishByCurrentHanldeId(String currentHandleId) {
		// TODO Auto-generated method stub
		return traceTaskDao.findByCurrentHandleIdAndTaskStatus(currentHandleId, WorkflowHelper.TASK_STATUS_FINISH);
	}

	@Override
	public Page<TraceTask> getFinishByCurrentHanldeId(String currentHandleId,
			Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setCurrentHandleId(currentHandleId);
		condition.setTaskStatus(WorkflowHelper.TASK_STATUS_FINISH);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<TraceTask> getProcessingByCurrentHanldeId(String currentHandleId) {
		return traceTaskDao.findByCurrentHandleIdAndTaskStatus(currentHandleId, WorkflowHelper.TASK_STATUS_PROCESSING);
	}

	@Override
	public Page<TraceTask> getProcessingByCurrentHanldeId(
			String currentHandleId, Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setCurrentHandleId(currentHandleId);
		condition.setTaskStatus(WorkflowHelper.TASK_STATUS_PROCESSING);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<TraceTask> getAllByDeployId(String deployId) {
		// TODO Auto-generated method stub
		return traceTaskDao.findByDepolyId(deployId);
	}

	@Override
	public Page<TraceTask> getAllByDeployId(String deployId,
			Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setDeployId(deployId);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<TraceTask> getFinishByDeployId(String deployId) {
		// TODO Auto-generated method stub
		return traceTaskDao.findByDepolyIdAndTaskStatus(deployId, WorkflowHelper.TASK_STATUS_FINISH);
	}

	@Override
	public Page<TraceTask> getFinishByDeployId(String deployId,
			Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setDeployId(deployId);
		condition.setTaskStatus(WorkflowHelper.TASK_STATUS_FINISH);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<TraceTask> getProcessingByDeployId(String deployId) {
		return traceTaskDao.findByDepolyIdAndTaskStatus(deployId, WorkflowHelper.TASK_STATUS_PROCESSING);
	}

	@Override
	public Page<TraceTask> getProcessingByDeployId(String deployId,
			Page<TraceTask> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		TraceTask condition = new TraceTask();
		condition.setDeployId(deployId);
		condition.setTaskStatus(WorkflowHelper.TASK_STATUS_PROCESSING);
		
		org.springframework.data.domain.Page<TraceTask> springPage = traceTaskDao.findByPage(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public boolean delete(String[] processInstanceIds) {
		return traceTaskDao.deleteAllByProcessInstanceIds(processInstanceIds) > 0;
	}

}
