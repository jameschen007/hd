package com.easycms.workflow.exception;


public class StartProcessInstanceError extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7043472125107236501L;

	public StartProcessInstanceError() {
		super("Start ProcessInstance error");
	}
	
	public StartProcessInstanceError(String msg) {
		super(msg);
	}
	
}
