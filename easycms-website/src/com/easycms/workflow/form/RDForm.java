package com.easycms.workflow.form;

import java.io.Serializable;

import com.easycms.core.util.Page;

public class RDForm extends Page implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1677689130385357159L;
	private Integer[] id;	//ID
	private String source;	//来源
	private Integer sourceId;	//来源ID
	private String query;	//查询类型
	private String type;	//类型
	private Integer typeId;	//类型ID
	private String description;	//任务描述
	private String filename;	//文件名
	private String fileNumber;	//文件号
	private Integer departmentId;	//部门ID
	private String[] deploymentId;	//部署ID
	private String proceeDefKey;	//流程定义key
	private String taskType;	//任务类型
	private String[] roleId;	//角色ID
	private String userId;	//人员ID
	private String taskId;	//任务ID
	private String taskName;	//任务ID
	private String handleMessage;	//处理意见
	private String auditMessage;	//审核意见
	private Integer audit;	//审核，1为通过，0不通过
	private String tableName;	//表单名
	
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public Integer[] getId() {
		return id;
	}
	public void setId(Integer[] id) {
		this.id = id;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getSourceId() {
		return sourceId;
	}
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	
	public String[] getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String[] deploymentId) {
		this.deploymentId = deploymentId;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getProceeDefKey() {
		return proceeDefKey;
	}
	public void setProceeDefKey(String proceeDefKey) {
		this.proceeDefKey = proceeDefKey;
	}
	public String[] getRoleId() {
		return roleId;
	}
	public void setRoleId(String[] roleId) {
		this.roleId = roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getHandleMessage() {
		return handleMessage;
	}
	public void setHandleMessage(String handleMessage) {
		this.handleMessage = handleMessage;
	}
	public Integer getAudit() {
		return audit;
	}
	public void setAudit(Integer audit) {
		this.audit = audit;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getAuditMessage() {
		return auditMessage;
	}
	public void setAuditMessage(String auditMessage) {
		this.auditMessage = auditMessage;
	}
	
}
