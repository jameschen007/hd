package com.easycms.quartz.service;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("schedulerService")
public class SchedulerServiceImpl implements SchedulerService {
	private static final String NULLSTRING = null;
	private static final Date NULLDATE = null;

	@Autowired
	private Scheduler scheduler;
	@Autowired
	private JobDetail jobDetail;

	//------------------------------------------CronExpression----------------------------------------------
	
	@Override
	public void schedule(String cronExpression) {
		schedule(NULLSTRING, cronExpression);
	}

	@Override
	public void schedule(String name, String cronExpression) {
		schedule(name, NULLSTRING, cronExpression);
	}

	@Override
	public void schedule(String name, String group, String cronExpression) {
		try {
			schedule(name, group, new CronExpression(cronExpression));
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public void schedule(CronExpression cronExpression) {
		schedule(NULLSTRING, cronExpression);
	}

	@Override
	public void schedule(String name, CronExpression cronExpression) {
		schedule(name, NULLSTRING, cronExpression);
	}

	@Override
	public void schedule(String name, String group, CronExpression cronExpression) {

		if (isValidExpression(cronExpression)) {

			if (name == null || name.trim().equals("")) {
				name = UUID.randomUUID().toString();
			}

			CronTriggerImpl trigger = new CronTriggerImpl();
			trigger.setCronExpression(cronExpression);

			TriggerKey triggerKey = new TriggerKey(name, group);

			trigger.setJobName(jobDetail.getKey().getName());
			trigger.setKey(triggerKey);

			try {
				scheduler.addJob(jobDetail, true);
				if (scheduler.checkExists(triggerKey)) {
					scheduler.rescheduleJob(triggerKey, trigger);
				} else {
					scheduler.scheduleJob(trigger);
				}
			} catch (SchedulerException e) {
				throw new IllegalArgumentException(e);
			}
		}
	}
	
	//---------------------------------------User Defined-------------------------------------------------

	@Override
	public String schedule(Date startTime, Map<String, Object> jobDataMap) {
		return schedule(startTime, NULLDATE, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, String group, Map<String, Object> jobDataMap) {
		return schedule(startTime, NULLDATE, group, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, NULLDATE, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, String group, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, NULLDATE, group, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, Map<String, Object> jobDataMap) {
		return schedule(startTime, endTime, 0, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, String group, Map<String, Object> jobDataMap) {
		return schedule(startTime, endTime, 0, group, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, endTime, 0, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, String group, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, endTime, 0, group, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, int repeatCount, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, NULLDATE, 0, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, int repeatCount, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, endTime, 0, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, int repeatCount, String group, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, endTime, 0, group, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, int repeatCount, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, endTime, 0, 0L, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, int repeatCount, String group, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, endTime, 0, 0L, group, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, int repeatCount, long repeatInterval, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, NULLDATE, repeatCount, repeatInterval, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, int repeatCount, long repeatInterval, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, endTime, repeatCount, repeatInterval, jobDataMap);
	}

	@Override
	public String schedule(Date startTime, Date endTime, int repeatCount, long repeatInterval, String group, Map<String, Object> jobDataMap) {
		return schedule(null, startTime, endTime, repeatCount, repeatInterval, group, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, int repeatCount, long repeatInterval, Map<String, Object> jobDataMap) {
		return schedule(name, startTime, endTime, repeatCount, repeatInterval, NULLSTRING, jobDataMap);
	}

	@Override
	public String schedule(String name, Date startTime, Date endTime, int repeatCount, long repeatInterval, String group, Map<String, Object> jobDataMap) {
		if (this.isValidExpression(startTime)) {

			if (name == null || name.trim().equals("")) {
				name = UUID.randomUUID().toString();
			}

			TriggerKey triggerKey = new TriggerKey(name, group);

			SimpleTriggerImpl trigger = new SimpleTriggerImpl();
			trigger.getJobDataMap().putAll(jobDataMap);
			trigger.setKey(triggerKey);
			trigger.setJobName(jobDetail.getKey().getName());

			trigger.setStartTime(startTime);
			trigger.setEndTime(endTime);
			trigger.setRepeatCount(repeatCount);
			trigger.setRepeatInterval(repeatInterval);
			trigger.setMisfireInstruction(Trigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY);
			
			try {
				scheduler.addJob(jobDetail, true);
				if (scheduler.checkExists(triggerKey)) {
					scheduler.rescheduleJob(triggerKey, trigger);
				} else {
					scheduler.scheduleJob(trigger);
				}
			} catch (SchedulerException e) {
				throw new IllegalArgumentException(e);
			}
			
		}
		return name;
	}

	@Override
	public void pauseTrigger(String triggerName) {
		pauseTrigger(triggerName, NULLSTRING);
	}

	@Override
	public void pauseTrigger(String triggerName, String group) {
		try {
			scheduler.pauseTrigger(new TriggerKey(triggerName, group));// 停止触发器
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void resumeTrigger(String triggerName) {
		resumeTrigger(triggerName, NULLSTRING);
	}

	@Override
	public void resumeTrigger(String triggerName, String group) {
		try {
			scheduler.resumeTrigger(new TriggerKey(triggerName, group));// 重启触发器
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean removeTrigger(String triggerName) {
		return removeTrigger(triggerName, NULLSTRING);
	}

	@Override
	public boolean removeTrigger(String triggerName, String group) {
		TriggerKey triggerKey = new TriggerKey(triggerName, group);
		try {
			scheduler.pauseTrigger(triggerKey);// 停止触发器
			return scheduler.unscheduleJob(triggerKey);// 移除触发器
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	private boolean isValidExpression(final CronExpression cronExpression) {

		CronTriggerImpl trigger = new CronTriggerImpl();
		trigger.setCronExpression(cronExpression);

		Date date = trigger.computeFirstFireTime(null);

		return date != null && date.after(new Date());
	}

	private boolean isValidExpression(final Date startTime) {

		SimpleTriggerImpl trigger = new SimpleTriggerImpl();
		trigger.setStartTime(startTime);

		Date date = trigger.computeFirstFireTime(null);

		return date != null && date.after(new Date());
	}
}
