package com.easycms.quartz.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.easycms.quartz.exception.QuartzException;

public class ScheduleStartListener implements ServletContextListener {
	private static final Logger logger = Logger .getLogger(ScheduleStartListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		WebApplicationContext rwp = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
	    final Scheduler scheduler= (Scheduler)rwp.getBean("scheduler");
		
		try {
			recovery(scheduler);
		} catch (Exception e) {
			throw new QuartzException(
					" ScheduleStartListener contextInitialized ERROR!!", e);
		}
	}

	public void recovery(Scheduler scheduler) {
		try {
			logger.info("ScheduleStartListener try to recovery all Scheduler.");

			scheduler.start();

		} catch (Exception e) {
			throw new QuartzException(
					"ScheduleStartListener  recovery() Error!", e);
		}
	}
}
