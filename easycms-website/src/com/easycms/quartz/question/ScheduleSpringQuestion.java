package com.easycms.quartz.question;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.easycms.hd.question.service.RollingQuestionService;
@Component
public class ScheduleSpringQuestion {
	
	private static Logger logger = Logger.getLogger(ScheduleSpringQuestion.class);
	@Autowired
	private RollingQuestionService rollingQuestionService ;
	/**
	 * 对作业问题的超时状态变更
	 * 1 班长提出的问题，超过（3天+延时小时）时间，技术工程师的状态还是处于待处理状态的，
	 * 2 将技术工程师的记录状态标识为超时，且将作业问题主状态标识为unsolved,主记录的超时状态标识加上
	 * 
	 */
//    @Scheduled(cron ="0 0 0/1 * * ?")
    public void updateRollingQuestionTimeoutStatus(){
    	logger.info("开始定时计算作业问题超时时间，更改超时状态和作业状态...");
    	boolean f = rollingQuestionService.timeoutCompute();
    	logger.info("结束定时计算作业问题超时时间，更改超时状态和作业状态。结果："+(f?"操作成功！":"操作失败"));
        
    }
}
