package com.easycms.quartz.exception;

public class QuartzException extends RuntimeException {
	private static final long serialVersionUID = 6970052235693703042L;

	public QuartzException(String message, Throwable cause) {
		super(message, cause);
	}
}

