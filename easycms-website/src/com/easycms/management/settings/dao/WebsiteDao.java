package com.easycms.management.settings.dao;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.settings.domain.Website;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface WebsiteDao extends Repository<Website,Integer>,BasicDao<Website> {
	
	/**
	 * 查找默认
	 * @return
	 */
	@Query("select w From Website w where w.isLocalSite = 1")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Website findLocal();
	
	/**
	 * 清空
	 */
	void deleteAll();
	
	/**
	 * 更新
	 * @param sitename
	 * @param domain
	 * @param keywords
	 * @param description
	 * @param root
	 * @param id
	 * @return
	 */
	@Modifying
	@Query("UPDATE Website w SET w.sitename=?1,w.domain=?2,w.keywords=?3,w.describe=?4,w.root=?5,w.pagesizeList=?6,w.defaultPagesize = ?7 WHERE w.id=?8")
	int update(String sitename,String domain,String keywords,String description,String root,String pagesizeList,String pagesize,Integer id);
}
