package com.easycms.management.settings.service;

import com.easycms.management.settings.domain.Website;

public interface WebsiteService{
	
	
	/**
	 * 根据ID查找
	 * @param id
	 * @return
	 */
	Website findById(Integer id);
	
	/**
	 * 查找默认站点
	 * @return
	 */
	Website findLocalSite();
	
	/**
	 * 清空所有配置
	 */
	void empty();
	
	/**
	 * 更新站点信息
	 * @param website
	 * @return
	 */
	boolean update(Website website);
	
	/**
	 * 添加站点信息
	 * @param website
	 * @return
	 */
	Website add(Website website);
}
