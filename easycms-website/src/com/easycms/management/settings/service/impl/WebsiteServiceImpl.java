package com.easycms.management.settings.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.management.settings.dao.WebsiteDao;
import com.easycms.management.settings.domain.Website;
import com.easycms.management.settings.service.WebsiteService;

@Service("websiteService")
public class WebsiteServiceImpl implements WebsiteService {

	@Autowired
	private WebsiteDao websiteDao;

	@Override
	public Website findLocalSite() {
		// TODO Auto-generated method stub
		return websiteDao.findLocal();
	}

	@Override
	public void empty() {
		websiteDao.deleteAll();
	}

	@Override
	public boolean update(Website website) {
		// TODO Auto-generated method stub
		int result = websiteDao.update(website.getSitename(),website.getDomain(),website.getKeywords(),website.getDescribe(),website.getRoot(),website.getPagesizeList(),website.getDefaultPagesize(),website.getId());
		return result > 0;
	}

	@Override
	public Website add(Website website) {
		// TODO Auto-generated method stub
		return websiteDao.save(website);
	}

	@Override
	public Website findById(Integer id) {
		// TODO Auto-generated method stub
		return websiteDao.findById(id);
	}
	
}
