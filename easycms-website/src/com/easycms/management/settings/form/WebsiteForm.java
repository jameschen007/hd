
package com.easycms.management.settings.form;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class WebsiteForm {
	private Integer id;
	private String sitename;
	private String domain = "";
	private String keywords = "";
	private String describe = "";
	private String maxSize;
	private String templUploadDir = "";
	private String templUploadType = "";
	private String resUploadDir = "";
	private String root = "";
	private String pagesizeList = "";
	private String defaultPagesize = "";
	
	
	public String getDefaultPagesize() {
		return defaultPagesize;
	}
	public void setDefaultPagesize(String defaultPagesize) {
		this.defaultPagesize = defaultPagesize;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(String maxSize) {
		this.maxSize = maxSize;
	}
	public String getTemplUploadDir() {
		return templUploadDir;
	}
	public void setTemplUploadDir(String templUploadDir) {
		this.templUploadDir = templUploadDir;
	}
	public String getTemplUploadType() {
		return templUploadType;
	}
	public void setTemplUploadType(String templUploadType) {
		this.templUploadType = templUploadType;
	}
	public String getResUploadDir() {
		return resUploadDir;
	}
	public void setResUploadDir(String resUploadDir) {
		this.resUploadDir = resUploadDir;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPagesizeList() {
		return pagesizeList;
	}
	public void setPagesizeList(String pagesizeList) {
		this.pagesizeList = pagesizeList;
	}
	
}
