package com.easycms.management.settings.directive;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.management.settings.domain.Database;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

/**
 * 数据库指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class DatabaseDirective implements TemplateDirectiveModel {
	private static Logger log = Logger.getLogger(DatabaseDirective.class);
	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if(loopVars.length < 1) {
			throw new RuntimeException("Loop variable is required.");
		}
		
		String dirStr = ContextUtil.getRealPath("backup");
		
		log.info("==> Database dir is " + dirStr);
		
		File dir = new File(dirStr);
		
		List<Database> databases = new ArrayList<Database>();
		//遍历所有.sql结尾的文件
		if(dir.exists()) {
			File[] files = dir.listFiles(new FileFilter() {
				
				@Override
				public boolean accept(File subFile) {
					if(subFile == null || !subFile.exists()) return false;
					
					String name = subFile.getName();
					if(!CommonUtility.isNonEmpty(name)) return false;
					
					if(name.lastIndexOf(".sql") == -1) return false;
					return true;
				}
			});
			
			for(File file : files) {
				Database db = new Database();
				db.setName(file.getName());
				db.setSize(String.valueOf(file.length()));
				db.setLastModifyTime(CommonUtility.convertTimestampToCalendar(file.lastModified(), TimeZone.getDefault()));
				databases.add(db);
			}
		}
		
		log.info("[databases length] = " + databases.size());
		loopVars[0] = DEFAULT_WRAPER.wrap(databases);
		body.render(env.getOut());
	}

}
