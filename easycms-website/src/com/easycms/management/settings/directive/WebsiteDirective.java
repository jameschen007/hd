package com.easycms.management.settings.directive;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.management.settings.domain.Website;
import com.easycms.management.settings.service.WebsiteService;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

/**
 * 获取所有站点列表指令 
 * 
 * @author jiepeng
 * 
 */
public class WebsiteDirective implements TemplateDirectiveModel {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(WebsiteDirective.class);

	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();
	@Autowired
	private WebsiteService websiteService;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if(loopVars.length < 1) {
			throw new RuntimeException("Loop variable is required.");
		}
		// #########################################################
		// 若有站点ID信息，则查询出站点信息
		// #########################################################
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		if(id != null) {
			logger.debug("[id] ==> " + id);
			Website website = null;
			//若ID为0则查找当前站点信息
			if(0 == id) {
				website = websiteService.findLocalSite();
			}else{
				website = websiteService.findById(id);
			}
			logger.debug("[website] = " + CommonUtility.toJson(website));
			loopVars[0] = DEFAULT_WRAPER.wrap(website);
			body.render(env.getOut());
			return;
		}
		
		// #########################################################
		// 若无站点ID信息，则查询出站点列表
		// #########################################################
//		List<Website> datas = websiteService.findAll();
//		logger.debug("[wesites length] ==> " + datas.size());
//		loopVars[0] = DEFAULT_WRAPER.wrap(datas);
//		body.render(env.getOut());

	}

}
