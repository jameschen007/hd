package com.easycms.management.settings.action;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.CacheManager;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.settings.form.DBForm;
import com.easycms.management.settings.util.DBBackup;

@Controller
@RequestMapping("/management/settings/db")
public class ManagementDBController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementDBController.class);
	
	/**
	 * 数据库信息列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.debug("==> Show database page.");
		String returnString = ForwardUtility.forwardAdminView("/settings/list_database");
		return returnString;
	}
	/**
	 * 数据列表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String data(HttpServletRequest request,
			HttpServletResponse response,@ModelAttribute("easyuiForm") DBForm easyuiForm) throws Exception {
		logger.debug("==> Show database data list.");
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(easyuiForm));
		return ForwardUtility.forwardAdminView("/settings/data/data_json_database");
	}
	
	/**
	 * 恢复数据
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/recover")
	public void recover(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DBForm form) throws Exception {
		logger.info("==> Start to recover database with [" + form.getNames() +"]");
		
		if(form.isNamesEmpty()) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		String dir = ContextUtil.getRealPath("backup");
		String path = CommonUtility.assemblePath(dir, form.getNames()[0]);
		boolean result  = DBBackup.getInstance().recover(path);
		
		logger.info("==> Recover " + (result?"success":"failed"));
		
		//若恢复成功,清除所有缓存
		if(result) {
			logger.info("==> Clear all cache");
			CacheManager.getInstance().clearAll();
		}

		HttpUtility.writeToClient(response, CommonUtility.toJson(result));
	}
	
	/**
	 * 备份数据库
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/backup")
	public void backup(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("==> Start to backup database.");
		String dir = ContextUtil.getRealPath("backup");
		boolean result = DBBackup.getInstance().backup(dir);
		
		logger.info("==> Backup " + (result?"success":"failed"));
		HttpUtility.writeToClient(response, CommonUtility.toJson(result));
	}
	
	/**
	 * 删除数据库文件
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/delete")
	public void delete(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DBForm form) throws Exception {
		logger.info("==> Start to delete database.");

		if(form.isNamesEmpty()) {
			logger.info("==> Name is empty.");
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		String[] dbNames = form.getNames();
		logger.info("==> Database name are [" + CommonUtility.toJson(dbNames) + "]");
		
		boolean result = false;
		if(dbNames != null && dbNames.length > 0) {
			String dir = ContextUtil.getRealPath("backup");
			int length = dbNames.length;
			for(int i=0;i<length;i++) {
				String path = CommonUtility.assemblePath(dir, dbNames[i]);
				File file = new File(path);
				if(!file.delete()) {
					break;
				}else if(i == length-1){
					result = true;
				}
			}
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(result));
	}
	
	/**
	 * 下载文件
	 * @param request
	 * @param response
	 * @param databaseName
	 * @throws Exception
	 */
	@RequestMapping("/download")
	public void download(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DBForm form) throws Exception {
		logger.debug("==> To download database");
		if(form.isNamesEmpty()) {
			return;
		}
		logger.debug("==> DB name is [" + form.getNames()[0] + "]");
		String dir = ContextUtil.getRealPath("backup");
		String path = CommonUtility.assemblePath(dir, form.getNames()[0]);
		
		UploadUtil.download(path, "utf-8", response);
	}
}
