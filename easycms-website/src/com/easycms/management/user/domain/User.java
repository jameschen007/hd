package com.easycms.management.user.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.hd.mobile.domain.Modules;

@Entity
@Table(name="ec_user")
//@Cacheable
public class User extends BasicForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2660217706536201507L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;

	@Column(name="username")
	private String name;
	
	@Column(name="real_name")
	private String realname;

	@Column(name="password")
	private String password;

	@Column(name="md5")
	private String md5;

	private String email;
	
	@Column(name="register_time")
	private Date registerTime;
	
	@Column(name="register_ip")
	private String registerIp;
	
	@Column(name="current_login_time")
	private Date currentLoginTime;
	
	@Column(name="current_login_ip")
	private String currentLoginIp;
	
	@Column(name="last_login_time")
	private Date lastLoginTime;
	
	@Column(name="last_login_ip")
	private String lastLoginIp;
	
	@Column(name="login_times")
	private Integer loginTimes;
	
	@Column(name="is_default_admin")
	private boolean defaultAdmin;
	
	@Transient
	private Integer isDefaultAdminFlag = 0;
	
	@Column(name="is_del")
	private boolean isDel;
	
	@Column(name="enpower_id")
	private String enpowerId;
	/*
	 * 所在项目
	 */
	@Column(name="mine_project")
	private String mineProject;

	/**
	 * 下一次需要更新密码时间
	 */
	@Column(name="update_pwd_time")
	private Date updatePwdTime;
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch=FetchType.LAZY)
	@JoinColumn(name="parent_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private User parent;
	
	@JsonIgnore
	@ManyToMany(targetEntity=com.easycms.management.user.domain.Role.class,fetch=FetchType.EAGER,cascade={CascadeType.REMOVE, CascadeType.REFRESH})
	@JoinTable(
			name="ec_user_role",
			joinColumns=@JoinColumn(name="user_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="role_id",referencedColumnName="id")
			)
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<Role> roles;
	
	@JsonIgnore
	@ManyToMany(targetEntity=Modules.class,fetch=FetchType.LAZY,cascade={CascadeType.REMOVE})
	@JoinTable(
			name="ec_user_module",
			joinColumns=@JoinColumn(name="user_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="module_id",referencedColumnName="id")
			)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<Modules> modules;
	/**
	 * wangzhi:20171109发现，此userDevice使用EAGER时，User.getRoles()本只有一条记录的，出现多条相同记录，原因是生成的sql关联查询user_device表，
	 * user_device表中存在多条记录。
	 */
	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "user")
	private List<UserDevice> userDevice;
	
	@JsonIgnore
	@Transient
	private List<Menu> menus;
	
	@JsonIgnore
	@Transient
	private Set<String> roleType;
	
	@JsonIgnore
	@ManyToOne(targetEntity=com.easycms.management.user.domain.Department.class,fetch=FetchType.LAZY)
	@JoinColumn(name="department_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//	@NotFound(action=NotFoundAction.IGNORE)
	private Department department;
	
	
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setIsDefaultAdminFlag(Integer isDefaultAdminFlag) {
		this.isDefaultAdminFlag = isDefaultAdminFlag;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	public String getRegisterIp() {
		return registerIp;
	}
	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLastLoginIp() {
		return lastLoginIp;
	}
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	public Integer getLoginTimes() {
		return loginTimes;
	}
	public void setLoginTimes(Integer loginTimes) {
		this.loginTimes = loginTimes;
	}
	
	@Transactional
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public Date getCurrentLoginTime() {
		return currentLoginTime;
	}
	public void setCurrentLoginTime(Date currentLoginTime) {
		this.currentLoginTime = currentLoginTime;
	}
	public String getCurrentLoginIp() {
		return currentLoginIp;
	}
	public void setCurrentLoginIp(String currentLoginIp) {
		this.currentLoginIp = currentLoginIp;
	}
	
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	public boolean isDefaultAdmin() {
		return defaultAdmin;
	}
	public void setDefaultAdmin(boolean defaultAdmin) {
		this.defaultAdmin = defaultAdmin;
	}
	public Integer getIsDefaultAdminFlag() {
		if(this.defaultAdmin) this.isDefaultAdminFlag = 1;
		return isDefaultAdminFlag;
	}
	public boolean isDel() {
		return isDel;
	}
	public void setDel(boolean isDel) {
		this.isDel = isDel;
	}
	public List<UserDevice> getUserDevice() {
		return userDevice;
	}
	public void setUserDevice(List<UserDevice> userDevice) {
		this.userDevice = userDevice;
	}
	
	
	
	public User getParent() {
		return parent;
	}
	public void setParent(User parent) {
		this.parent = parent;
	}

	public String getRoleIds() {
		String roleIds = null;
		if(this.getRoles() != null && this.getRoles().size() != 0) {
			StringBuffer sb = new StringBuffer();
			for(Role r : this.getRoles()) {
				sb.append(r.getId().toString() + ",");
			}
			roleIds = sb.substring(0, sb.length() - 1);
		}
		return roleIds;
	}
	public List<Modules> getModules() {
		return modules;
	}
	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}
	public String getEnpowerId() {
		return enpowerId;
	}
	public void setEnpowerId(String enpowerId) {
		this.enpowerId = enpowerId;
	}
	
	@JsonIgnore
	@Transient
	private boolean cons = false;

	public boolean isCons() {
		try{

			if (null != this.roles && !this.roles.isEmpty()) {
				boolean consFlag = false;
				
				List<Privilege> privileges = getAllPrivilege();
				if (null != privileges && !privileges.isEmpty()) {
					Long count = privileges.stream().filter(privilege -> {
						if (null != privilege && CommonUtility.isNonEmpty(privilege.getUri())) {
							return (privilege.getUri().startsWith("/construction") || 
									privilege.getUri().startsWith("/witness") ||
									privilege.getUri().startsWith("/hd")
									);
						}
						return false;
					}).count();
					
					if (count > 0L) {
						consFlag = true;
					}
				}
				
				return consFlag;
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false ;
		}
		
		return false;
	}
	
	private List<Privilege> getAllPrivilege(){
		if (null != this.roles && !this.roles.isEmpty()) {
			List<Privilege> privileges = new ArrayList<Privilege>();
			
			for (Role role : this.roles) {
				List<Menu> menus = role.getMenus();
				
				if (null != menus && !menus.isEmpty()) {
					for (Menu menu : menus) {
						privileges.addAll(getAllPrivilege(menu));
					}
				}
			}
			
			return privileges;
		}
		
		return null;
	}
	
	private List<Privilege> getAllPrivilege(Menu menu){
		if (null != menu) {
			List<Privilege> privileges = new ArrayList<Privilege>();
			privileges.add(menu.getPrivilege());
			if (null != menu.getChildren() && !menu.getChildren().isEmpty()) {
				for (Menu m : menu.getChildren()) {
					privileges.addAll(getAllPrivilege(m));
				}
			}
			
			return privileges;
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof User) {
        	User u = (User) obj;
            return (id.equals(u.getId()));
        }
        return super.equals(obj);
    }
	
	@Override 
    public int hashCode() {
        return id.hashCode();
    }
	public Set<String> getRoleType() {
		return roleType;
	}
	public void setRoleType(Set<String> roleType) {
		this.roleType = roleType;
	}
	public String getMineProject() {
		return mineProject;
	}
	public void setMineProject(String mineProject) {
		this.mineProject = mineProject;
	}

	public Date getUpdatePwdTime() {
		return updatePwdTime;
	}

	public void setUpdatePwdTime(Date updatePwdTime) {
		this.updatePwdTime = updatePwdTime;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public void setCons(boolean cons) {
		this.cons = cons;
	}
}
