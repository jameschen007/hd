package com.easycms.management.user.domain;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name="ec_privilege")
@Cacheable
public class Privilege extends BasicForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5386363271091316067L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="privilege_name")
	private String name;
	private String uri;
	
	@OneToOne(targetEntity = com.easycms.management.user.domain.Menu.class,fetch=FetchType.LAZY)
	@JoinColumn(name="menu_id")
	private Menu menu;
	
	@Column(name="create_time")
	private Date createTime;
	
	@Column(name="update_time")
	private Date updateTime;
	@Column(name = "is_del")
	private boolean isDel;
	
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isDel() {
		return isDel;
	}

	public void setDel(boolean del) {
		isDel = del;
	}
}
