package com.easycms.management.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.form.RoleForm;
import com.easycms.management.user.form.validator.RoleFormValidator;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.MenuService;
import com.easycms.management.user.service.RoleService;

@Controller
@RequestMapping("/management/role")
public class ManagementRoleController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementRoleController.class);

	@Autowired
	private MenuService menuService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private DepartmentService departmentService;

	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show role list.");
		}
		String returnString = ForwardUtility.forwardAdminView("/user-group/list_role");
		
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") RoleForm form) {
		form.setSearchValue(request.getParameter("search[value]"));
		logger.debug("==> Show role data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/user-group/data/data_json_role");
		return returnString;
	}
	
	/**
	 * 添加角色UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") RoleForm form) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show add new role UI.");
		}
		return ForwardUtility.forwardAdminView("/user-group/modal_role_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("roleForm") @Valid RoleForm roleForm,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start add role.");
			logger.debug("[roleForm] ==> " + CommonUtility.toJson(roleForm));
		}

		// 校验新增表单信息
		// System.out.println(WebUtility.toJson(userForm));
		RoleFormValidator validator = new RoleFormValidator();
		validator.validate(roleForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End add role.");
			}
			return;
		}
		// 校验通过，保存新增
		Role role = new Role();
		role.setName(roleForm.getName());
		String[] menuIds = roleForm.getMenuId();
		if(menuIds != null && menuIds.length > 0) {
			logger.debug("==> MenuIds length = " + menuIds.length);
			List<Menu> menus = new ArrayList<Menu>();
			for (String menuId : menuIds) {
				if(CommonUtility.isNonEmpty(menuId)) {
					Menu menu = menuService.findById(menuId);
					menus.add(menu);
				}
			}
			role.setMenus(menus);
		}
		
		if (roleForm.getParentId() != null) {
			Role parent = roleService.findRole(roleForm.getParentId());
			role.setParent(parent);
			role.setLevel(parent.getLevel() + 1);
		}else{
			role.setLevel(1);
		}
		
		if(roleForm.getCoexist() != null && roleForm.getCoexist() == 1) {
			role.setCoexist(true);
		}
		
		role = roleService.add(role);
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		logger.debug("==> End add role.");
		return;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") BasicForm form) {
		logger.debug("==> Start delete role.");

		Integer[] ids = form.getIds();
		logger.debug("==> To delete role ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			roleService.delete(ids);
		}
		
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete role.");
	}
	
	/**
	 * 修改角色
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") RoleForm form) {
		return ForwardUtility.forwardAdminView("/user-group/modal_role_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid RoleForm roleForm,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start edit role.");
			logger.debug("[roleForm] ==> " + CommonUtility.toJson(roleForm));
		}

		// 校验表单信息
//		 System.out.println(WebUtility.toJson(roleForm));
		RoleFormValidator validator = new RoleFormValidator();
		validator.validate(roleForm, errors);
		if (errors.hasErrors() || id == null) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End add role.");
			}
			return;
		}
		// 校验通过，保存
		Role role = new Role();
		role.setId(id);
		role.setName(roleForm.getName());
		String[] menuIds = roleForm.getMenuId();
		if(menuIds != null && menuIds.length > 0) {
			List<Menu> menus = new ArrayList<Menu>();
			for (String menuId : menuIds) {
				if(CommonUtility.isNonEmpty(menuId)) {
					Menu menu = menuService.findById(menuId);
					menus.add(menu);
				}
			}
			role.setMenus(menus);
		}
		
		if(roleForm.getParentId() != null) {
			Role parent = roleService.findRole(roleForm.getParentId());
			role.setParent(parent);
			role.setLevel(parent.getLevel() + 1);
		}else{
			role.setLevel(1);
		}
		
		if(roleForm.getCoexist() != null && roleForm.getCoexist() == 1) {
			role.setCoexist(true);
		}
		
		role = roleService.update(role);
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		logger.debug("==> End edit role.");
		return;
	}

}
