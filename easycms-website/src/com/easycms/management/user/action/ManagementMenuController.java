package com.easycms.management.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.optional.jsp.JspNameMangler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.SortEnum;
import com.easycms.core.form.FormHelper;
import com.easycms.core.response.JsonResponse;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.MenuForm;
import com.easycms.management.user.form.validator.MenuFormValidator;
import com.easycms.management.user.service.MenuService;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.RoleService;


@Controller
@RequestMapping("/management/menu")
public class ManagementMenuController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementMenuController.class);

	@Autowired
	private MenuService menuService;
	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private RoleService roleService;
	

	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") MenuForm form) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show privilege list.");
		}
		if(form.isValidExist()) {
			boolean avalible = false;
			String name = form.getName();
			String id = form.getId();
			Integer privilegeId = form.getPrivilegeId();
			logger.debug("[name] ==" + name);
			logger.debug("[id] ==" + id);
			logger.debug("[privilegeId] ==" + privilegeId);
			if(id == null) {
				avalible = !menuService.exist(name);
			}else{
				avalible = !menuService.exist(name,id);
			}
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;
		}
		String returnString = ForwardUtility.forwardAdminView("/user-group/list_menu");
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") MenuForm form) {
		logger.debug("==> Show menu data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/user-group/data/data_html_menu");
		return returnString;
	}
	
	/**
	 * 添加权限UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show add new menu UI.");
		}
		return ForwardUtility.forwardAdminView("/user-group/modal_menu_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid MenuForm form,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start add privilege.");
		}
		
		// 校验新增表单信息
		MenuFormValidator validator = new MenuFormValidator();
		validator.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End add menu.");
			}
			return ;
		}
		// 校验通过，保存新增
		Menu menu = new Menu();
		menu.setName(form.getName());
		menu.setIconPath(form.getIconPath());
		menu.setSeq(form.getSeq());
		
		Privilege p = privilegeService.findById(form.getPrivilegeId());
		menu.setPrivilege(p);
		
		Menu parent = menuService.findById(form.getParentId());
		menu.setParent(parent);
		
		menu = menuService.save(menu);
		HttpUtility.writeToClient(response, CommonUtility.toJson(menu.getId()));
		logger.debug("==> End add privilege.");
		return ;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("==> Start delete privilege.");
		String[] ids = request.getParameterValues("ids");
		logger.debug("==> To delete menus ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			menuService.deleteAll(ids);
		}
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete privilege.");
	}
	
	/**
	 * 修改菜单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") MenuForm form) {
		return ForwardUtility.forwardAdminView("/user-group/modal_menu_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid MenuForm form,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start edit menu.");
		}
		
		// 校验新增表单信息
		MenuFormValidator validator = new MenuFormValidator();
		validator.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End edit menu.");
			}
			return;
		}
		// 校验表单信息
		// 校验通过，保存
		// 校验通过，保存更新
		
		Menu menu = menuService.findById(form.getId());
		menu.setName(form.getName());
		menu.setSeq(form.getSeq());
		
		Privilege p = privilegeService.findById(form.getPrivilegeId());
		menu.setPrivilege(p);
		menu.setStyle(form.getStyle());
		menu.setIconPath(form.getIconPath());
		Menu parent = menuService.findById(form.getParentId());
		menu.setParent(parent);
		menu = menuService.save(menu);
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		logger.debug("==> End edit privilege.");
		return;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void upload(HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {

		// ####################################################
		// 添加
		// #########################################################
		List<FileInfo> fileList = UploadUtil.upload(ContextUtil.getRealPath("/icons"),
				"utf-8", request);
		HttpUtility.writeToClient(response,fileList.get(0).getNewFilename());
	}
	
	/**
	 * 刷新菜单
	 * 无需权限
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/refresh")
	public void refresh(HttpServletRequest request,
			HttpServletResponse response)
					throws Exception {
		// ####################################################
		// 刷新菜单
		// #########################################################
		
		User user = (User) request.getSession().getAttribute("user");
		
		if(user == null) {
			JsonResponse res = new JsonResponse("-1002", "user session is expire");
			HttpUtility.writeToClient(response, CommonUtility.toJson(res));
			return;
		}
		
		//如果是默认管理员登录，则拥有所有菜单
		List<Menu> menus = new ArrayList<Menu>();
		if(user.isDefaultAdmin()) {
			menus = menuService.getTree(SortEnum.desc);
		}else{
			//将重复的权限去掉
			List<Role> userRoles = user.getRoles();
			if(userRoles == null || userRoles.size() == 0) {
				JsonResponse res = new JsonResponse();
				HttpUtility.writeToClient(response, CommonUtility.toJson(res));
				return;
			}
			
			List<Role> roles = new ArrayList<Role>();
			for(Role r: userRoles) {
				Role newRole = roleService.findRole(r.getId());
				if(newRole != null) {
					roles.add(newRole);
				}
			}
			menus = menuService.getTree(roles,SortEnum.desc);
		}
		user.setMenus(menus);
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		JsonResponse res = new JsonResponse();
		HttpUtility.writeToClient(response, CommonUtility.toJson(res));
	}
}
