package com.easycms.management.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.form.PrivilegeForm;
import com.easycms.management.user.form.validator.PrivilegeFormValidator;
import com.easycms.management.user.service.PrivilegeService;


@Controller
@RequestMapping("/management/privilege")
public class ManagementPrivilegeController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementPrivilegeController.class);

	@Autowired
	private PrivilegeService privilegeService;
	

	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") PrivilegeForm privilegeForm) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show privilege list.");
		}
		if(privilegeForm.isValidExist()) {
			boolean avalible = false;
			String name = privilegeForm.getName();
			Integer id = privilegeForm.getId();
			String uri = privilegeForm.getUri();
			logger.debug("[name] ==" + name);
			logger.debug("[id] ==" + id);
			logger.debug("[uri] ==" + uri);
			if(id == null) {
				if(CommonUtility.isNonEmpty(name)) {
					avalible = !privilegeService.existName(name);
				}
				
				if(CommonUtility.isNonEmpty(uri)) {
					avalible = !privilegeService.existName(uri);
				}
			}else{
				if(CommonUtility.isNonEmpty(name)) {
					avalible = !privilegeService.existByNameAndId(name,id);
				}
				
				if(CommonUtility.isNonEmpty(uri)) {
					avalible = !privilegeService.existByUriAndId(uri,id);
				}
			}
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;
		}
		String returnString = ForwardUtility.forwardAdminView("/user-group/list_privilege");
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") PrivilegeForm form) {
		logger.debug("==> Show privilege data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/user-group/data/data_json_privilege");
		return returnString;
	}
	
	/**
	 * 添加权限UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Show add new privilege UI.");
		}
		return ForwardUtility.forwardAdminView("/user-group/modal_privilege_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("privilegeForm") @Valid PrivilegeForm privilegeForm,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start add privilege.");
		}
		
		// 校验新增表单信息
		PrivilegeFormValidator validator = new PrivilegeFormValidator();
		validator.validate(privilegeForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End add privilege.");
			}
			return ;
		}
		// 校验通过，保存新增
		Privilege privilege = new Privilege();
		privilege.setName(privilegeForm.getName());
		privilege.setUri(privilegeForm.getUri());
		privilege = privilegeService.save(privilege);
		HttpUtility.writeToClient(response, CommonUtility.toJson(privilege.getId()));
		logger.debug("==> End add privilege.");
		return ;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") BasicForm form) {
		logger.debug("==> Start delete privilege.");

		Integer[] ids = form.getIds();
		logger.debug("==> To delete privilege ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			privilegeService.deleteAll(ids);
		}
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete privilege.");
	}
	
	/**
	 * 修改权限
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") PrivilegeForm privilegeForm) {
		return ForwardUtility.forwardAdminView("/user-group/modal_privilege_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid PrivilegeForm privilegeForm,
			BindingResult errors) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start edit privilege.");
		}
		
		// 校验新增表单信息
		PrivilegeFormValidator validator = new PrivilegeFormValidator();
		validator.validate(privilegeForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			if (logger.isDebugEnabled()) {
				logger.debug("==> End add privilege.");
			}
			return;
		}
		// 校验表单信息
		// 校验通过，保存
		// 校验通过，保存新增
		Privilege privilege = privilegeService.findById(id);
		privilege.setId(id);
		privilege.setName(privilegeForm.getName());
		privilege.setUri(privilegeForm.getUri());
		privilege = privilegeService.save(privilege);
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		logger.debug("==> End edit privilege.");
		return;
	}
}
