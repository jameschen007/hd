package com.easycms.management.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.form.DepartmentForm;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;

@Controller
@RequestMapping("/management/department")
public class ManagementDepartmentController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementDepartmentController.class);

	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private RoleService roleService;

	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") DepartmentForm form) {
		String func = form.getFunc();
		if("findTopRole".equals(func)) {
			request.setAttribute("departmentId", request.getParameter("departmentId"));
			return ForwardUtility
					.forwardAdminView("/user-group/data/data_html_department_role_list");
		}
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/list_department");
		return returnString;
	}

	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DepartmentForm form) {
		form.setSearchValue(request.getParameter("search[value]"));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		form.setFilter(CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/data/data_json_department");
		return returnString;
	}

	/**
	 * 添加UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		return ForwardUtility
				.forwardAdminView("/user-group/modal_department_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid DepartmentForm form,
			BindingResult errors) {

		// 校验新增表单信息
		logger.debug(CommonUtility.toJson(form));
		form.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			return;
		}
		// 校验通过，保存新增
		Department d = new Department();
		d.setName(form.getName());
		if (form.getParentId() != null) {
			Department parent = departmentService.findById(form.getParentId());
			d.setParent(parent);
		}

		if(form.getTopRoleId() != null) {
			d.setTopRole(roleService.findRole(form.getTopRoleId()));
		}
		
		departmentService.add(d);

		HttpUtility.writeToClient(response,
				CommonUtility.toJson(d.getId() != null));
	}

	/**
	 * 修改角色
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DepartmentForm form) {
		return ForwardUtility
				.forwardAdminView("/user-group/modal_department_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid DepartmentForm form,
			BindingResult errors) {
		// 校验新增表单信息
		logger.debug(CommonUtility.toJson(form));
		form.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			return;
		}
		// 校验通过，保存
		Department d = departmentService.findById(form.getId());
		d.setName(form.getName());
		
		if(form.getParentId() != null) {
			Department parent = departmentService.findById(form.getParentId());
			d.setParent(parent);
		}

		if(form.getTopRoleId() != null) {
			d.setTopRole(roleService.findRole(form.getTopRoleId()));
		}
		
		departmentService.update(d);

		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		return;
	}

	@RequestMapping(value = "/delete")
	public void delete(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DepartmentForm form) throws Exception {
		Integer[] ids = form.getIds();
		logger.debug("==> To delete [" + CommonUtility.toJson(ids) + "]");
		Boolean val = false;
		if (ids != null && ids.length > 0) {
			val = departmentService.deleteAll(ids);
		}
		HttpUtility.writeToClient(response, val.toString());
	}
	/**
	 * 部门树
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/tree", method = RequestMethod.POST)
	public void tree(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid DepartmentForm form){
			
	}
	/**
	 * 部门树
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	public String treeUI(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") DepartmentForm form) {
		return ForwardUtility.forwardAdminView("/user-group/tree_dept");
	}
}
