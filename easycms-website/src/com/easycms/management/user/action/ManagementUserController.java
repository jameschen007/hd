package com.easycms.management.user.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.strategy.register.RegistryExecutor;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.enpower.EnpowerUserApiController;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.UserForm;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/management/user")
public class ManagementUserController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementUserController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private RegistryExecutor<UserForm> registryExecutor;
	@Autowired
	private EnpowerUserApiController enpowerUserApiController;//手工同步Enpower人员方法使用
	

	/**
	 * 用户模块 - 用户列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") @Valid UserForm userForm) {
		String func = userForm.getFunc();
		logger.debug("[func] = " + func);
		// #########################################################
		// 校验用户是否存在
		// #########################################################
		if ("exist".equalsIgnoreCase(func)) {
			logger.debug("==> Valid user is exist or not.");
			boolean avalible = false;
			avalible = !userService.userExist(userForm.getName());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;

		}
		// #########################################################
		// 修改用户username
		// #########################################################
		if ("edit".equalsIgnoreCase(func)) {
			logger.debug("==> Valid username is exist or not except with id.");
			boolean avalible = false;
			avalible = !userService.userExistExceptWithId(userForm.getName(),userForm.getId());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;
			
		}
		logger.debug("==> Start show user list.");
		return ForwardUtility.forwardAdminView("/user-group/list_user");
	}

	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") UserForm form) {
		form.setSearchValue(request.getParameter("search[value]"));
		logger.debug("==> Show user data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/data/data_json_user");
		return returnString;
	}
	/**
	 * 分配角色UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/role", method = RequestMethod.GET)
	public String roleUI(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid UserForm userForm) {
		String func = userForm.getFunc();
		if("roleDirect".equals(func)){//为了直接修改岗位，主要用于APP设置　班长、组长、队长使用。
			//岗位列表录request
			List<Role> roles = roleService.findAll2();
			request.setAttribute("roles", roles);
			
			Integer pUserId =0;//直接领导id
			User pid = userService.findUserById(userForm.getId());
			if(pid!=null){
				User pid2 = pid.getParent();
				if(pid2!=null){
					pUserId = pid2.getId();
				}
			}
			
			request.setAttribute("hisroles", pid.getRoles());

			request.setAttribute("parentUserId", pUserId);
			request.setAttribute("users", userService.findAll());
			String returnString = ForwardUtility
					.forwardAdminView("/user-group/modal_user_role_direct");
			return returnString;
		}
		
		//查找部门的顶级职务
		if("findDepartmentTopRole".equals(func)) {
			request.setAttribute("departmentId", userForm.getDepartmentId());
			String returnString = ForwardUtility
					.forwardAdminView("/user-group/data/data_html_user_role_list");
			return returnString;
		}
		//查找直接领导
		if("findSupUser".equals(func)) {
//			Integer[] roleIds = userForm.getRoleId();
//			List<User> parentList = new ArrayList<User>();
//			
//			if(roleIds != null && roleIds.length > 0) {
//				for(Integer id : roleIds) {
//					List<User> users = userService.findParentByDepartmentIdAndRoleId(userForm.getDepartmentId(), id);
//					parentList.addAll(users);
//				}
//			}
//			request.setAttribute("parentList", parentList);
			String returnString = ForwardUtility
					.forwardAdminView("/user-group/data/data_html_user_parent");
			return returnString;
		}
		
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/modal_user_role");
		return returnString;
	}

	/**
	 * 保存分配
	 * 
	 * @param request
	 * @param response
	 * @param userForm
	 * @param errors
	 */
	@RequestMapping(value = "/role", method = RequestMethod.POST)
	public void role(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid UserForm userForm,
			BindingResult errors) {

		String func = userForm.getFunc();

		if("roleDirect".equals(func)){//为了直接修改岗位，主要用于APP设置　班长、组长、队长使用。

			Integer id = userForm.getId();
			User user = userService.findUserById(id);
			// 校验通过，保存新增
			Integer[] roleIds = userForm.getRoleId();
			logger.debug("[roleId] = " + CommonUtility.toJson(roleIds));
			List<Role> roleList = new ArrayList<Role>();
			if (roleIds != null) {
				for (Integer roleId : roleIds) {
					Role role = new Role();
					role.setId(roleId);
					roleList.add(role);
				}
			}
			user.setRoles(roleList);
			//直接上级修改
			Integer parentId = userForm.getParentId();
			if(parentId != null) {
				User parent = userService.findUserById(parentId);
				user.setParent(parent);
			}else{
				user.setParent(null);
			}

			//保存部门
			Integer departmentId = userForm.getDepartmentId();
			if(departmentId != null) {
				Department department = departmentService.findById(departmentId);
				user.setDepartment(department);
			}else{
				user.setDepartment(null);
			}
			user = userService.update(user);
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
			return;
		}
		logger.debug("==> Save user role.");

		Integer id = userForm.getId();
		logger.debug("[id] =" + id);
		if (id == null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}

		User user = userService.findUserById(id);

		//保存部门
		Integer departmentId = userForm.getDepartmentId();
		if(departmentId != null) {
			Department department = departmentService.findById(departmentId);
			user.setDepartment(department);
		}else{
			user.setDepartment(null);
		}
		
		//保存直接领导
		Integer parentId = userForm.getParentId();
		if(parentId != null) {
			User parent = userService.findUserById(parentId);
			user.setParent(parent);
		}else{
			user.setParent(null);
		}
		
		// 校验通过，保存新增
		Integer[] roleIds = userForm.getRoleId();
		logger.debug("[roleId] = " + CommonUtility.toJson(roleIds));
		List<Role> roleList = new ArrayList<Role>();
		if (roleIds != null) {
			for (Integer roleId : roleIds) {
				Role role = new Role();
				role.setId(roleId);
				roleList.add(role);
			}
		}
		user.setRoles(roleList);
		user = userService.update(user);
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		return;
	}
	
	/**
	 * 选择上级领导UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/parent", method = RequestMethod.GET)
	public String parentUI(HttpServletRequest request,
			HttpServletResponse response) {
		String[] ids = request.getParameterValues("roleId");
		List<User> parentList = new ArrayList<User>();
				
		if(ids != null && ids.length > 0) {
			for(String id : ids) {
				Role r = roleService.findRole(Integer.parseInt(id));
				if(r != null) {
					List<User> users = r.getUsers();
					parentList.addAll(users);
				}
			}
		}
		request.setAttribute("parentList", parentList);
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/data/data_html_user_parent");
		return returnString;
	}*/
	
	/**
	 * 修改密码UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/password", method = RequestMethod.GET)
	public String pwdUI(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid UserForm userForm) {
		String returnString = ForwardUtility
				.forwardAdminView("/user-group/modal_user_password");
		return returnString;
	}
	
	/**
	 * 保存修改密码
	 * 
	 * @param request
	 * @param response
	 * @param userForm
	 * @param errors
	 */
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	public void password(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid UserForm userForm,
			BindingResult errors) {
		logger.debug("==> Update user password.");
		
		Integer id = userForm.getId();
		logger.debug("[id] = " + id);
		String password = userForm.getPassword();
		logger.debug("[password] = " + password);
		if (id == null 
				|| !CommonUtility.isNonEmpty(password)) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}

		User e = userService.findUserById(id);
		// 校验通过，保存新增
		boolean success = userService.updatePassword(id,null, password,false);//原来的结构页面已加密了。
		userService.updatePasswordMd5(id,e.getMd5(), password);
		HttpUtility.writeToClient(response, CommonUtility.toJson(success));
		return;
	}

	/**
	 * 新增用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {

		String returnString = ForwardUtility
				.forwardAdminView("/user-group/modal_user_add");
		return returnString;
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("userForm") @Valid UserForm userForm,
			BindingResult errors) {
		try {
			registryExecutor.execute(userForm, errors, request, response);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		
	}

	/**
	 * 查看用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String show(@RequestParam String id, HttpServletRequest request,
			HttpServletResponse response) {
		if (logger.isDebugEnabled()) {
			logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - start");
		}

		request.setAttribute("id", id);
		String returnString = ForwardUtility
				.forwardAdminView("/template/frames/module_users/user/user_show");
		if (logger.isDebugEnabled()) {
			logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - end");
		}
		return returnString;
	}

	/**
	 * 修改用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") UserForm form) {
		return ForwardUtility.forwardAdminView("/user-group/modal_user_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") UserForm form) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start save eidt user.");
		}

		// #########################################################
		// 校验提交信息
		// #########################################################
		if (form.getId() == null) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save eidt user failed.");
			logger.debug("==> End save eidt user.");
			return;
		}

		// #########################################################
		// 校验通过
		// #########################################################
		Integer id = form.getId();
		User user = userService.findUserById(id);
		user.setName(form.getName());
		user.setRealname(form.getRealname());
		user.setEmail(form.getEmail());
		userService.update(user);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save eidt user.");
		return;
	}

	/**
	 * 批量删除
	 * 
	 * @param request
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") UserForm form) {
		logger.debug("==> Start delete user.");

		Integer[] ids = form.getIds();
		logger.debug("==> To delete user [" + CommonUtility.toJson(ids) + "]");
		if (ids != null && ids.length > 0) {
			userService.delete(ids);
		}
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete user.");
	}
	

	/**
	 * 手动同步Enpower人员数据过来APP数据库
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getEnpowerUsers", method = {RequestMethod.POST,RequestMethod.GET})
	public void getEnpowerUsers(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("userForm") @Valid UserForm userForm,
			BindingResult errors) {
		try {
			enpowerUserApiController.enposerUser(request, response, "enpower", "H9d2a0P0p01", "251402f3307cb8f7e7bf53671e210144", "");
		} catch (Exception e) {
			logger.error(e);
		}
		
	}
}
