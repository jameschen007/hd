package com.easycms.management.user.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.Menu;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface MenuDao extends BasicDao<Menu>,Repository<Menu,Integer> {
	
	/**
	 * 删除所有
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Menu")
	int deleteAll();
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Menu o WHERE o.id  IN (?1) and isDel=0")
	int deleteByIds(String[] ids);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Menu findByName(String name);
	
	@Query("FROM Menu m WHERE m.privilege.uri = ?1 and isDel=0")
	Menu findByURI(String uri);
	
	/**
	 * 得到最顶层的父级权限
	 * @return
	 */
	@Query("FROM Menu m WHERE m.parent IS NULL and isDel=0 ORDER BY m.seq desc")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<Menu> findTop();
	
	/**
	 * 得到子级菜单
	 * @param parentId
	 * @return
	 */
	@Query("FROM Menu m WHERE m.parent.id =?1 and isDel=0 ORDER BY m.seq desc")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<Menu> findChildren(String parentId);
	
	@Query("select count(id) FROM Menu m WHERE m.id =?1 and isDel=0")
	int exists(String id);
	
	/**
	 * 新增时判断名称是否存在
	 * @param name
	 * @return
	 */
	@Query("select count(id) FROM Menu m WHERE m.name =?1 and isDel=0")
	int exist(String name);
	/**
	 * 修改时判断名称是否存在
	 * @param name
	 * @return
	 */
	@Query("select count(id) FROM Menu m WHERE m.name =?1 and m.id!=?2 and isDel=0")
	int exist(String name,String id);
}
