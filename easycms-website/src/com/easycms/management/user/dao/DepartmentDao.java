package com.easycms.management.user.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.Department;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface DepartmentDao extends BasicDao<Department>,Repository<Department,Integer> {
	/**
	 * 得到最顶层的父级权限
	 * @return
	 */
	@Query("FROM Department d WHERE d.parent IS NULL and isDel=0")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<Department> findTop();
	
	/**
	 * 得到子级菜单
	 * @param parentId
	 * @return
	 */
	@Query("FROM Department d WHERE d.parent.id =?1 and isDel=0")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="false") })
	List<Department> findChildren(Integer parentId);
	
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	boolean exists(Integer id);
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Department d WHERE d.id  IN (?1) and isDel=0")
	int deleteByIds(Integer[] ids);
	
	/**
	 * 删除所有
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Department")
	int deleteAll();
	
	Department findByName(String name);

	@Query("FROM Department d WHERE d.enpowerId =?1")
	Department findByEnpowerId(String enpowerId);
	@Query("from Department d where d.isDel=0")
	public List<Department> findAll();
}
