package com.easycms.management.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.UserDevice;
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface UserDeviceDao extends BasicDao<UserDevice> ,Repository<UserDevice,Integer>{
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM UserDevice ud WHERE ud.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Query("FROM UserDevice ud WHERE ud.user.id=?1 and ud.deviceid=?2")
	UserDevice getDeviceByDeviceIdAndUserId(Integer userId, String deviceId);
	
	@Query("FROM UserDevice ud WHERE ud.user.id=?1 ")
	List<UserDevice> getDeviceByUserId(Integer userId);
}
