package com.easycms.management.user.dao;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.domain.UserPwd;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Set;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface UserPwdDao extends BasicDao<UserPwd> ,Repository<UserPwd,Integer>{

    @Query(value = "select * from ec_user_pwd where user_id=?1 order by create_time desc limit 5",nativeQuery = true)
    List<UserPwd> last5(Integer userId);
}
