package com.easycms.management.user.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.SortEnum;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.management.user.dao.MenuDao;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.service.MenuService;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.RoleService;

@Service("menuService")
public class MenuServiceImpl implements MenuService {
	@Autowired
	private MenuDao menuDao;

	@Autowired
	private RoleService roleService;
	@Autowired
	private PrivilegeService privilegeService;

	@Override
	public Menu save(Menu m){
		Menu menu = menuDao.save(m);
		if(menu.getPrivilege() != null) {
			privilegeService.updateMenu(menu.getPrivilege().getId(), menu.getId());
		}
		return menu;
	}

	@Override
	public int delete(String id){
		// TODO Auto-generated method stub
		return menuDao.deleteById(id);
	}

	@Override
	public void deleteAll(){
		menuDao.deleteAll();
	}

	@Override
	public void deleteAll(String[] ids){
		// TODO Auto-generated method stub
		menuDao.deleteByIds(ids);
	}

	@Override
	public Menu findById(String id){
		// TODO Auto-generated method stub
		return menuDao.findById(id);
	}

	@Override
	public Menu findByName(String name){
		// TODO Auto-generated method stub
		return menuDao.findByName(name);
	}

	@Override
	public List<Menu> findAll(Menu condition){
		// TODO Auto-generated method stub
		return menuDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public List<Menu> findAll(){
		// TODO Auto-generated method stub
		return menuDao.findAll();
	}

	@Override
	public void updateMenus(List<Menu> list){
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exist(String name){
		// TODO Auto-generated method stub
		return menuDao.exist(name) > 0;
	}

	@Override
	public boolean exist(String name, String id){
		// TODO Auto-generated method stub
		return menuDao.exist(name, id) > 0;
	}

	@Override
	public Page<Menu> findPage(Menu condition, Page<Menu> page){
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<Menu> springPage = menuDao
				.findAll(QueryUtil.queryConditions(condition), pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<Menu> findTop(){
		return menuDao.findTop();
	}

	@Override
	public List<Menu> findChildren(String parentId){
		// TODO Auto-generated method stub
		return menuDao.findChildren(parentId);
	}

	@Override
	public List<Menu> getTree(SortEnum sort){
		List<Menu> elements = menuDao.findAll();
		List<Menu> roots = new ArrayList<Menu>();
		for(Menu m : elements) {
			if(m.getParent() == null) {
				roots.add(m);
			}
		}
		
		switch (sort) {
		case asc:
			Collections.sort(roots);
			break;
		case desc:
			Collections.sort(roots);
			Collections.reverse(roots);
			break;
		default:
			Collections.sort(roots);
			Collections.reverse(roots);
			break;
		}
		getMenuTree(roots,elements,sort);
		return roots;
	}
	
	private void getMenuTree(List<Menu> roots,Collection<Menu> elements,SortEnum sort){
		for(Menu root : roots) {
			List<Menu> children = new ArrayList<Menu>();
			for(Menu child : elements) {
				if(child.getParent() != null 
						&& CommonUtility.isNonEmpty(child.getParent().getId())
						&& root.getId().equals(child.getParent().getId())) {
					children.add(child);	
				}
			}
			
			if(children.size() > 0) {
				switch (sort) {
				case asc:
					Collections.sort(children);
					break;
				case desc:
					Collections.sort(children);
					Collections.reverse(children);
					break;
				default:
					Collections.sort(children);
					Collections.reverse(children);
					break;
				}
				getMenuTree(children,elements,sort);
			}
			
			root.setChildren(children);
		}
	}

	@Override
	public List<Menu> getTree(List<Role> roles, SortEnum sort){
		if(roles == null){
			return null;
		}
		
		List<Menu> container = new ArrayList<Menu>();
		List<Menu> roots = new ArrayList<Menu>();
		if(roles != null) {
			for(Role r : roles) {
				List<Menu> list = r.getMenus();
				container.removeAll(list);
				container.addAll(list);
			}
			
			for(Menu m : container) {
				if(m.getParent() == null) {
					roots.add(m);
				}
			}
			
			switch (sort) {
			case asc:
				Collections.sort(roots);
				break;
			case desc:
				Collections.sort(roots);
				Collections.reverse(roots);
				break;
			default:
				Collections.sort(roots);
				Collections.reverse(roots);
				break;
			}
			getMenuTree(roots,container,sort);
			
		}
		
		return new ArrayList<Menu>(roots);
	}

	@Override
	public Menu findByUri(String uri){
		return menuDao.findByURI(uri);
	}
}
