package com.easycms.management.user.service.impl;


import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.hd.api.response.AuthorizationResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.plan.mybatis.dao.RollingPlanStatisticsMybatisDao;
import com.easycms.management.user.dao.UserPwdDao;
import com.easycms.management.user.domain.UserPwd;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.util.Page;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.kpi.underlying.dao.KpiHandleExchangeDao;
import com.easycms.kpi.underlying.dao.KpiUserBaseDao;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	private static final String disturb = "easycms.com";
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserPwdDao userPwdDao;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private Authenticator<User> authemtocator;
	@Autowired
	private VariableSetDao variableSetDao;
	@Autowired
	private KpiUserBaseDao kpiUserBaseDao;
    @Autowired
    private KpiHandleExchangeDao kpiHandleExchangeDao;
	
	
	@Override
	public User userLogin(String username, String password, boolean isSession) {
		return authemtocator.executeAuth(username, password, isSession);
	}
	@Override
	public
	User userLoginMd5(String username,String md5, boolean isSession){
		return authemtocator.executeAuthMd5(username, md5, isSession);
	}

	@Override
	public User findUser(String username) {
		// TODO Auto-generated method stub
		return userDao.findByName(username);
	}

	@Override
	public User findByRealame(String username){
		return userDao.findByRealame(username);
	}
	
	
	@Override
	public User findByNameIsDel1(String username) {
		return userDao.findByNameIsDel1(username);
	}
	
	@Override
	public User findUserById(Integer id) {
		return userDao.findById(id);
	}
	
	@Override
	public User findUserEnpowerId(String enpowerId) {
		return userDao.findByEnpowerId(enpowerId);
	}

//	@Override
//	public Page<User> findPage(Specifications<User> sepc,Pageable pageable) {
//		// TODO Auto-generated method stub
//		return userDao.findAll(sepc,pageable);
//	}

	/* (non-Javadoc)
	 * @see com.easycms.core.user.service.UserService#add(com.easycms.core.user.domain.User)
	 */
	@Override
	public User add(User user) {
		// TODO Auto-generated method stub
		String password = user.getPassword();
		password = CommonUtility.MD5Digest(disturb, password);
		user.setPassword(password);
		/*20180410
因为已经存在了账号　zhangzl  张照龙 导致的？
如果再有这种情况　，程序　把原来的账号进行删除
		 */
		User old = userDao.findByName(user.getName());

		if(old!=null){
            if(CommonUtility.isNonEmpty(old.getMd5())){

                user.setMd5(old.getMd5());
            }
			userDao.deleteDbById(old.getId());
		}else{
		    user.setMd5("0d26fb6a123d9b07bb24b764f7206989");
        }



		return userDao.save(user);
	}

    public static void main(String[] args) {
        System.out.println(33);
    }

	@Override
	public boolean delete(Integer id) {
		return userDao.deleteById(id) > 0;
		
	}

	@Override
	public User update(User user) {

//		parentOk("1.parentOk","待领导审核"),
//		parentScore("3.parentScore","待领导评分"),
//		stakeholderScore("3.stakeholderScore","待干系人评分"),
//		parentScore4("4.parentScore","待领导评分"),
//		departmentManagerScore("4.departmentManagerScore","待部门经理评分"),
//		chargeLeaderScore("4.chargeLeaderScore","待分管领导评分"),
//		generalScore("5.generalScore","待总经理评分"),
		
		//修改流程状态表中的处理人。本人的不修改，只修改协作人
		User old = userDao.findById(user.getId());
		if(old!=null){

//			kpiHandleExchangeDao.updateHandleIdByOldId(user., old.getId());
			
			// 修改上级时，需要将对应的绩效考核的基础信息中的直接领导修改了
			KpiUserBase base = 	kpiUserBaseDao.findByUserId(user.getId());
			if(base!=null){
				base.setImmediateSuperior(user.getParent());
				kpiUserBaseDao.save(base);
			}
			if (CommonUtility.isNonEmpty(old.getMd5())) {
				user.setMd5(old.getMd5());
			}
			return userDao.save(user);
		}
		return old ;
	}

	@Override
	public boolean updatePassword(Integer id,String oldPassword,String newPassword,boolean isMd5) {
		if(id == null) {
			return false;
		}
		
		if(CommonUtility.isNonEmpty(oldPassword)){
			oldPassword = DigestUtils.md5Hex(oldPassword);
		}else{
			User user = this.findUserById(id);
			oldPassword = user.getPassword();
		}
		if(isMd5){

			newPassword = DigestUtils.md5Hex(newPassword);
		}
		int row = userDao.updatePassword(id, oldPassword, newPassword);
		logger.debug("[row] = " + row);
		return row > 0;
	}

	@Override
	public JsonResult<AuthorizationResult> updatePasswordMd5(Integer id, String oldMd5, String newMd5) {

		Function<String,JsonResult<AuthorizationResult>> failSupplier=(msg)->{
			JsonResult<AuthorizationResult> result = new JsonResult<AuthorizationResult>();
			result.setCode(ExceptionCode.E3);
			if (msg == null) {
				msg = "修改失败！";
			}
			result.setMessage(msg);
			return result;
		};
		Supplier<JsonResult<AuthorizationResult>> okSupplier=()->{
			JsonResult<AuthorizationResult> result = new JsonResult<AuthorizationResult>();
			result.setMessage("修改成功！");
			return result;
		};
		if(id == null) {
			return failSupplier.apply(null);
		}
		User user = this.findUserById(id);
		if (user == null) {

			return failSupplier.apply(null);
		}
		if(CommonUtility.isNonEmpty(oldMd5)){
			if(!oldMd5.equals(user.getMd5())){
				return failSupplier.apply("修改的密码应确保与最近５次使用过的密码不重复。");
			}

		}else{
			oldMd5 = user.getPassword();
		}

		//修改的密码应确保与最近５次使用过的密码不重复。
		List<UserPwd> userPwds = userPwdDao.last5(id);
		if (userPwds != null && userPwds.stream().anyMatch(b -> newMd5.equals(b.getMd5()))) {
			return failSupplier.apply("修改的密码应确保与最近５次使用过的密码不重复。");
		}

		user.setMd5(newMd5);
		userDao.save(user);
		logger.debug("id = " + id+" oldMd5 = " + oldMd5+" newMd5 = " + newMd5);
		
		try{
//			２增加历史密码表，记录历史，并且有时间。
//３每次修改密码时，记录上修改时间，更新用户表中提醒修改密码的时间
			UserPwd t = new UserPwd();
			t.setUserId(id);
			t.setMd5(newMd5);
			Calendar instance = Calendar.getInstance();
			instance.add(Calendar.MONTH, 6);
			Date halfYearLater = instance.getTime();
			user.setUpdatePwdTime(halfYearLater);
			this.userPwdDao.save(t);
			return okSupplier.get();
		}catch(Exception e){
			return failSupplier.apply(null);
		}
	}

	@Override
	public boolean userExist(String username) {
		return userDao.exsit(username) != null;
	}

	@Override
	public boolean delete(Integer[] ids) {
		// TODO Auto-generated method stub
		return userDao.deleteByIds(ids) > 0;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}
	
	@Override
	public List<User> findAll(User condition) {
		// TODO Auto-generated method stub
		return userDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public Page<User> findPage(Page<User> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		
		org.springframework.data.domain.Page<User> springPage = userDao.findAll(null,pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<User> findPage(User condition,Page<User> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		logger.debug("==> Find all user by condition.");
		org.springframework.data.domain.Page<User> springPage = null;
		if(condition!=null && condition.getDepartment()!=null){
			List<Class<?>> list = new ArrayList<Class<?>>();
			list.add(Department.class);
			springPage = userDao.findAll(QueryUtil.queryConditions(condition,list),pageable);
		}else{
			springPage = userDao.findAll(QueryUtil.queryConditions(condition),pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Integer count() {
		// TODO Auto-generated method stub
		return userDao.count();
	}

	@Override
	public boolean userExistExceptWithId(String username, Integer id) {
		return userDao.exsitExceptWithId(username, id) != null;
	}

	@Override
	public boolean deleteByName(String name) {
		return userDao.deleteByName(name)> 0;
	}

	@Override
	public boolean deleteByNames(String[] names) {
		return userDao.deleteByNames(names) > 0;
	}

	@Override
	public List<User> findByDepartmentIdAndRoleId(Integer departmentId,
			Integer roleId) {
		List<User> tempUsers = userDao.getByDepartmentId(departmentId);
		List<User> users = new ArrayList<User>();
		for(User u : tempUsers) {
			List<Role> roles = u.getRoles();
			for(Role r : roles) {
				if(roleId.intValue() == r.getId().intValue()) {
					users.add(u);
					break;
				}
			}
		}
		return users;
	}

	@Override
	public List<User> findParentByDepartmentIdAndRoleId(Integer departmentId,
			Integer roleId) {
		List<User> users = new ArrayList<User>();
		boolean belong = false;
		Department department = departmentService.findById(departmentId);
		Role role = roleService.findRole(roleId);
		Role parentRole = role.getParent();
		
		if(parentRole == null) {
			return null;
		}
		
		Role topRole = department.getTopRole();
		if(topRole == null) {
			return null;
		}
		
		List<Role> container = new ArrayList<Role>();
		getAllRoles(container, topRole);
		
		//迭代查找该父级职务是否在本部门中
		for(Role r : container) {
			if(parentRole.getId().intValue() == r.getId().intValue()) {
				belong = true;
				break;
			}
		}
		
		//若属于本部门，取出该职务下的用户
		//否则取该部门的上级部门的顶级职务 20170911绩效考核需要修改为查出上级部门下的所有人员
		if(belong) {
			List<User> tempUsers = parentRole.getUsers();
			injectUserByDepartment(departmentId, users, tempUsers);
		}else{
			//20170911绩效考核需要修改为查出上级部门下的所有人员
			Department parentDepartment = department.getParent();
//			Role tr = parentDepartment.getTopRole();
//			injectUserByDepartment(parentDepartment.getId(), users, tr.getUsers());
			users = this.findUserByDepartmentId(parentDepartment.getId());
		}
		return users;
	}

	private void injectUserByDepartment(Integer departmentId, List<User> users,
			List<User> tempUsers) {
		for(User u : tempUsers) {
			if(u.getDepartment() != null &&
					departmentId.intValue() == u.getDepartment().getId().intValue()) {
				users.add(u);
			}
		}
	}
	
	private void getAllRoles(List<Role> container,Role topRole) {
		container.add(topRole);
		List<Role> roles =  topRole.getChildren();
		if(roles == null || roles.size() == 0) {
			return;
		}
		for(Role r : roles) {
			getAllRoles(container, r);
		}
	}

  @Override
  public List<User> findUserByParentId(Integer userId) {
    return userDao.findUserByParentId(userId);
  }
  @Override
  public List<User> findUserByDeptParentId(Integer userId){
	  return userDao.findUserByDeptParentId(userId);
  }
  /**
   * 根据人员id 查人员的部门的下级部门下的人员，并且角色是指定的角色
   */
  @Override
  public List<User> findUserByDeptParentIdAndRoleName(Integer userId,String roleNameKey){
	  VariableSet varset = variableSetDao.findByKey(roleNameKey, "roleType");
	  String roleName = varset.getSetvalue();
	  System.out.println("roleName="+roleName);
	  return userDao.findUserByDeptParentIdAndRoleName(userId,roleName);

  }
	/**
	 * 根据部门id集合和，角色查询人员
	 * @param departments
	 * @param roleNameKey
	 * @return
	 */
  @Override
	public List<User> findUserByDepartmentAndRoleName(List<Department> departments,String roleNameKey){
	  VariableSet varset = variableSetDao.findByKey(roleNameKey, "roleType");
	  String roleName = varset.getSetvalue();
	  System.out.println("roleName="+roleName);
	  if(departments==null){
		  return null;
	  }
	  List<Integer> list = new ArrayList<Integer>();
	  for(Department d:departments){
		  list.add(d.getId());
	  }

	  List<String> list2 = new ArrayList<String>();
	  list2.add(roleName);
	  return userDao.findUserByDepartmentIdAndRoleName(list, list2);
	}
  @Override
	public List<User> findUserByDepartmentAndRoleNames(List<Department> departments,String[] roleNameKeys){
	  logger.info(roleNameKeys);
	  List<String> list2 = new ArrayList<String>();
	  for(String k:roleNameKeys){
		  VariableSet varset = variableSetDao.findByKey(k, "roleType");
		  String roleName = varset.getSetvalue();
		  list2.add(roleName);
	  }
	  if(departments==null){
		  return null;
	  }
	  List<Integer> list = new ArrayList<Integer>();
	  for(Department d:departments){
		  list.add(d.getId());
	  }
	  if(list.size()==0){
		  return new ArrayList<User>();
	  }
	  return userDao.findUserByDepartmentIdAndRoleName(list, list2);
	}
  @Override
  public List<User> findUserByDepartmentId(final Integer departmentId) {

      Specification<User> sepc = new Specification<User> () {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<User,Department> join = root.join(root.getModel().getSingularAttribute("department",Department.class),JoinType.LEFT);
				Predicate predicate = cb.equal(join.get("id").as(Integer.class), departmentId);
				return query.where(predicate).getRestriction();
			}
      };
	  
	  
	  return userDao.findAll(sepc);
  }
  @Override
  public List<User> findUserByDepartmentIds(List<Integer> departmentIds) {
	  if(departmentIds!=null && !departmentIds.isEmpty()){

	      Specification<User> sepc = new Specification<User> () {
				@Override
				public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					Join<User,Department> join = root.join(root.getModel().getSingularAttribute("department",Department.class),JoinType.LEFT);
					
					In<Integer> in = cb.in(join.get("id").as(Integer.class));
					departmentIds.stream().forEach(id->{
						in.value(id);
					});
					
					Predicate predicate = in;
					return query.where(predicate).getRestriction();
				}
	      };
		  return userDao.findAll(sepc);
	  }

	  
	  return null;
  }

  @Override
  public List<User> getSolvers(User user){
	  /*
	   * 改前的逻辑 是：1 根据用户查 直接上级 2 查出直接上级的下级，除本人外，3 查本人的下级，4 将查出的结果返回
	   * 现根据这个逻辑，修改为查上级部门下的人员（与原需求可能增多）查本部门的人员和下属部门的人员
	   */
      
    List<User> solvers = new ArrayList<User>();
//    User leader = user.getParent();
//    
//    if(leader != null){
//      solvers.add(leader);
//    }
    if(user.getDepartment().getParent()!=null){
        List<User> leaders = userDao.getByDepartmentId(user.getDepartment().getParent().getId());
        solvers.addAll(leaders);
    }

//    List<User> siblings = userDao.findUserByParentId(leader.getId());
    //直接领导查询下级，通过部门上下级查询
    List<User> siblings = userDao.getByDepartmentId(user.getDepartment().getId());
    if(siblings != null && siblings.size() > 0){
      for (User user2 : siblings) {
        if(user2.getId() != user.getId()){
          solvers.add(user2);
        }
      }
    }

//    List<User> children = userDao.findUserByParentId(user.getId());
    //直接领导查询下级，通过部门上下级查询
    List<User> children = userDao.findUserByDeptParentId(user.getId());
    if(children != null && children.size() > 0){
      solvers.addAll(children);
    }
    
      return solvers;
  }


	/**
	 * 根据User判断 是否是 技术部门的。technologyManagement
	 */
	@Override
	public boolean isDepartmentOf(User user, String technologyManagement) {

		if(user==null || user.getDepartment()==null || user.getDepartment().getName()==null){
			return false ;
		}
		Set<String> roleNameList0 = variableSetDao.findSetkeyBySetvalueAndType(user.getDepartment().getName(),
				"departmentType");
		if (roleNameList0.contains(technologyManagement)) {
			// 如果是技术管理中心的
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 根据User判断 是否是某个角色的,如 班长 monitor。
	 */
	@Override
	public boolean isRoleOf(User user, String monitor) {
		List<Role> roles = user.getRoles();
		if (null != roles) {
			for (Role role : roles) {
				Set<String> roleNameList = variableSetDao.findSetkeyBySetvalueAndType(role.getName(), "roleType");
				if (roleNameList.contains(monitor)) {
					// 如果是班长
					return true;
				} else if (roleNameList.contains("team")) {
					// 如果是组长
					return false;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isRoleOf(User user, String[] monitor) {
		List<Role> roles = user.getRoles();
		if (null != roles) {
			for (Role role : roles) {
				Set<String> roleNameList = variableSetDao.findSetkeyBySetvalueAndType(role.getName(), "roleType");
				for(String monit:monitor){
					if (roleNameList.contains(monit)) {
						// 如果是班长
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 根据字符标示的变量到变量表中取出相应的部门id，查询该部门中的所有人员。
	 * 
	 * @param variableType
	 * @return
	 */
	@Override
	public List<User> findUserByDepartmentVariable(String variableType, String variableKey) {
		if(CommonUtility.isNonEmpty(variableType)){
			variableType = "departmentId";
		}
		if(CommonUtility.isNonEmpty(variableKey)){
			variableKey = "technologyManagement";
		}
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);

		List<User> users = new ArrayList<User>();
		for (String val : set) {
			users.addAll(findUserByDepartmentId(Integer.valueOf(val)));
		}
		
		return users;
	}

	/**
	 * 查询作业问题待处理人列表,根据变量表配置和模块查询
	 * 
	 * */
	@Override
	public List<User> findUserByDepartmentVariableAndModule(String variableType, String variableKey,String moduleType) {

		//variableKey = solver   variableType=roleType 查出
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);
		Set<Integer> keys = new TreeSet<Integer>();
		for(String s:set){
			if(s!=null && s.matches("\\d+")){
				keys.add(Integer.valueOf(s));
			}
		}
		
		List<String> moduleTypeList = new ArrayList<String>();
		moduleTypeList.add(moduleType);
		
		return userDao.findDeptmentAndModule(keys, moduleTypeList);
	}

	/**
	 * 查询作业问题待处理人列表
	 * 
	 * */
	@Override
	public List<User> findUserByDepartmentVariable(String variableType, String variableKey,String moduleType) {

		//variableKey = solver   variableType=roleType 查出
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);
		
		List<String> moduleTypeList = new ArrayList<String>();
		moduleTypeList.add(moduleType);
		
		return userDao.findRoleAndModule(set,moduleTypeList);
		
//		//查出有配置的这个值 做为下一步的查询条件
//		if(set!=null && set.size()>0){
//		      Specification<User> sepc = new Specification<User> () {
//					@Override
//					public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
//
//						List<Predicate> predicate = new ArrayList<Predicate>(); 
//						Path<User> rolepath = root.get("id");
//						Path<User> modulepath = root.get("id");
//						Predicate predicates1 = null ;
//						Subquery<User> roleQuery = query.subquery(User.class);
//						Root<Role> rootRole = ((Root<Role>)roleQuery.from(Role.class));
//						roleQuery.select(rootRole.<User>get("users").get("id"));
//						predicates1 = cb.conjunction();
//						
//							//查角色名称	
//						In<String> in = cb.in(rootRole.get("name").as(String.class));
//						for(String x:set){
//							in.value(x);
//						}
//						Predicate predicates1Two= in ;
//						roleQuery.where(predicates1Two);
//						
//						predicates1 = cb.and(predicates1,cb.in(rolepath).value(roleQuery));
//						
//						查询 滚动计划类型
//						Subquery<User> moduleQuery = query.subquery(User.class);
//						Root<Modules> rootModule = ((Root<Modules>)moduleQuery.from(Modules.class));
//						moduleQuery.select(rootModule.<User>get("users2").get("id"));
//						Predicate predicates2 = cb.conjunction();
//						Predicate predicates2Two=cb.equal(rootModule.get("type").as(String.class), moduleType);
//						moduleQuery.where(predicates2Two);
//						
//						predicates2 = cb.and(predicates2,cb.in(modulepath).value(moduleQuery));
//
//						predicate.add(predicates1);
////						predicate.add(predicates2);
//		                Predicate[] pre = new Predicate[predicate.size()];
//		                return query.where(predicate.toArray(pre)).getRestriction();
//					}
//		      };
//			  return userDao.findAll(sepc);
//		}
//	    return null ;
	}

	/** 
	 * 查询部门下的某角色人员，根据变量表配置角色名和部门ID查询
	 */
	@Override
	public List<User> findUserByRoleAndDepartmentVariable(String roleVariable, String departmentVariable) {
		Set<String> roleName = variableSetDao.findSetValueBySetKeyAndType(roleVariable, "roleType");
		Set<String> deptId = variableSetDao.findSetValueBySetKeyAndType(departmentVariable, "departmentId");
		List<Integer> deptIds = new ArrayList<Integer>();
		List<String> roleNames = new ArrayList<String>();
		
		for(String s:deptId){
			if(s!=null && s.matches("\\d+")){
				deptIds.add(Integer.valueOf(s));
			}
		}

		for(String s:roleName){
			roleNames.add(s);	
		}
		
		return userDao.findUserByDepartmentIdAndRoleName(deptIds,roleNames);
	}

	@Override
	public String findRealnameById(Integer id) {
		String realname = "";
		User u = userDao.findById(id);
		if(u!=null){
			realname= u.getRealname();
		}
		return realname;
	}
	/**
	 * 根据部门名称和人员名称查人员
	 * @param deptName
	 * @param realName
	 * @return
	 */
	@Override
	public
	User findDeptmentAndUser(String deptName,String realName){
		return userDao.findDeptmentAndUser(deptName,realName);
	}
}
