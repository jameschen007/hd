package com.easycms.management.user.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Privilege;

public interface PrivilegeService{
	
	/**
	 * 保存权限
	 * @param p
	 * @return
	 */
	Privilege save(Privilege p);
	
	/**
	 * 删除
	 * @param id
	 */
	int delete(Integer id);
	
	/**
	 * 删除所有
	 */
	void deleteAll();
	
	/**
	 * 删除所以
	 * @param ids
	 */
	void deleteAll(Integer[] ids);
	
	Privilege findById(Integer id);
	
	Privilege findByUri(String uri);
	
	Privilege findByName(String name);
	
	boolean updateMenu(Integer id,String menuId);
	
	/**
	 * 查找所有
	 * @return
	 */
	List<Privilege> findAll(Privilege condition);
	
	List<Privilege> findAll();
	
	/**
	 * 修改时判断名称是否重复
	 * @param name
	 * @return
	 */
	boolean existByNameAndId(String name, int id);
	
	boolean existByUriAndId(String uri, int id);
	
	boolean existName(String name);
	
	boolean existUri(String uri);

	Page<Privilege> findPage(Privilege condition, Page<Privilege> page);
	
	void addIfNotExist(List<Privilege> list);
	
	List<Privilege> findFreePrivileges();
}
