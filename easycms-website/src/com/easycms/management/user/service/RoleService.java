package com.easycms.management.user.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Role;


public interface RoleService {
	
	/**
	 * 保存
	 * @param role
	 * @return
	 */
	public Role add(Role role);
	
	/**
	 * 更新
	 * @param role
	 * @return
	 */
	public Role update(Role role);
	
	/**
	 * 删除
	 * @param id
	 */
	public void delete(Integer id);

	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Integer[] ids);
	
	
	/**
	 * 查找
	 * @param id
	 * @return
	 */
	public Role findRole(Integer id);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	public Page<Role> findPage(Role condition,Page<Role> page);
	
	public Page<Role> findPage(Page<Role> page);
	
	/**
	 * 查找所有
	 * @return
	 */
	public List<Role> findAll(Role condition);

	public List<Role> findAll();
	/**查询所有有效的岗位*/
	public List<Role> findAll2();
	
	public Integer count();
	
	/**
	 * 得到最顶层的父级权限
	 * @return
	 */
	List<Role> findTop();
	
	
	/**
	 * 得到子级
	 * @param parentId
	 * @return
	 */
	List<Role> findChildren(Integer parentId);
	
	/**
	 * 查找部门下的顶级领导
	 * @param departmentId
	 * @return
	 */
	List<Role> findTopRoleByDepartmentId(Integer departmentId);
	
	Role findByName(String name);

	/**
	 * 根据enpowerid获取角色
	 * @param enpowerId
	 * @return
	 */
	Role findByEnpowerId(String enpowerId);
	
	/**
	 * 根据id获取角色
	 * @param enpowerId
	 * @return
	 */
	Role findById(Integer id);
	/**
	 * 根据id获取角色,目的是为了兼容已有的业务方法，已有的业务方法是需要List
	 * @param enpowerId
	 * @return
	 */
	List<Role> findListById(List<Role> roles,Integer id);

	/**
	 * 根据enpower提供的角色编号得到角色
	 * @param roleNo
	 * @return
	 */
	Role findByRoleNo(String roleNo);
}
