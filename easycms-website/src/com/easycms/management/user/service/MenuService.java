package com.easycms.management.user.service;

import java.util.List;

import com.easycms.core.enums.SortEnum;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Role;

public interface MenuService{
	
	/**
	 * 保存权限
	 * @param p
	 * @return
	 */
	Menu save(Menu m);
	
	/**
	 * 删除
	 * @param id
	 */
	int delete(String id);
	
	/**
	 * 删除所有
	 */
	void deleteAll();
	
	/**
	 * 删除
	 * @param ids
	 */
	void deleteAll(String[] ids);
	
	Menu findById(String id);
	
	Menu findByName(String name);
	
	Menu findByUri(String uri);
	
	/**
	 * 查找所有
	 * @return
	 */
	List<Menu> findAll(Menu condition);
	
	List<Menu> findAll();
	
	
	void updateMenus(List<Menu> list);

	/**
	 * 新增时判断名称是否重复
	 * @param name
	 * @return
	 */
	boolean exist(String name);
	/**
	 * 修改时判断名称是否重复
	 * @param name
	 * @return
	 */
	boolean exist(String name, String id);

	Page<Menu> findPage(Menu condition, Page<Menu> page);
	
	List<Menu> findTop();
	
	List<Menu> findChildren(String parentId);
	
	List<Menu> getTree(SortEnum sort);
	
	List<Menu> getTree(List<Role> roles,SortEnum sort);
}
