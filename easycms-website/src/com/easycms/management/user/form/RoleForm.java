
package com.easycms.management.user.form;

import com.easycms.core.form.BasicForm;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class RoleForm extends BasicForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name = "";
	private String[] menuId = {};
	private Integer parentId;
	private Integer coexist;
	
	private String searchValue;
	
	
	public String getSearchValue() {
		return searchValue;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getMenuId() {
		if(menuId.length == 1) {
			String pid = menuId[0];
			String[] pids = pid.split(",");
			if(pids.length > 1){
				return pids;
			}
		}
		return menuId;
	}
	public void setMenuId(String[] menuId) {
		this.menuId = menuId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCoexist() {
		return coexist;
	}
	public void setCoexist(Integer coexist) {
		this.coexist = coexist;
	}
	
}
