
package com.easycms.management.user.form;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class PersonalForm {
	private String id = "";
	private String username = "";
	private String realname = "";
	private String password = "";
	private String oldPWD = "";
	private String newPWD = "";
	private String confPWD = "";
	private String email = "";
	private String verifyCode="";
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	public String getOldPWD() {
		return oldPWD;
	}
	public void setOldPWD(String oldPWD) {
		this.oldPWD = oldPWD;
	}
	public String getNewPWD() {
		return newPWD;
	}
	public void setNewPWD(String newPWD) {
		this.newPWD = newPWD;
	}
	public String getConfPWD() {
		return confPWD;
	}
	public void setConfPWD(String confPWD) {
		this.confPWD = confPWD;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
}
