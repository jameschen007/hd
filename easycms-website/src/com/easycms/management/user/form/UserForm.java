
package com.easycms.management.user.form;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class UserForm extends BasicForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5778232707296977882L;
	private Integer id;	//用户ID
	private String name = "";
	private String realname = "";
	private String registerTime = null;
	private String email="";
	private String password = "";
	private String confirmPassword = "";
	private String departmentName = "";
	private String rolename = "";
	private Integer parentId;
	private Integer departmentId;
	private Integer[] roleId;
	
	private String searchValue;
	
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRealname() {
		return realname.trim();
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getEmail() {
		return email.trim();
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password.trim();
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword.trim();
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer[] getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer[] roleId) {
		this.roleId = roleId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSearchValue() {
		return searchValue;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
}
