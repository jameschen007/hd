package com.easycms.hd.question.service;

import java.util.List;

import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.management.user.domain.User;

public interface RollingQuestionSolverService {
	public RollingQuestionSolver add(RollingQuestionSolver p);
	public RollingQuestionSolver update(RollingQuestionSolver p);
	public RollingQuestionSolver findById(Integer id);
	public List<RollingQuestionSolver> findByQuestionId(Integer id);
	public List<RollingQuestionSolver> findByUserId(String id);
	public List<RollingQuestionSolver> findByQuestionIdAndUserId(int problemId,Integer userId);
//	public List<RollingQuestionSolver> findByUserIdAndStatus(String userid, String status);

	/**
	 * 查被指派人
	 * @param problemId
	 * @param userId
	 * @return
	 */
	public List<RollingQuestionSolver> findByQuestionIdAndType(int problemId,int solverType);
	
/**
 * 根据状态,问题id,和solverType查询
 * @param problemId
 * @param solverType
 * @return
 */
	public List<RollingQuestionSolver> findByQuestionIdAndStatus(int problemId,String status,Integer solverType);

	public List<RollingQuestionSolver> findByQuestionIdAndSolverType(int problemId,Integer solverType);
	/**
	 * 如果是技术主管改派，要将前一个技术人员的记录修改为未能解决的 状态
	 * @param ques
	 * @return
	 */
	public boolean setOlderUnsolved(RollingQuestion ques);

	/**
	 * 延时接口:技术主管为技术人员延时
	 * @param ques
	 * @return
	 */
	public boolean setTimeoutForSolver(Integer questionId,User solver,Integer hour);
	
/***
 * 根据问题id查询当前待办人
 * @param id
 * @return
 */
	public List<RollingQuestionSolver> findPreSolverById(Integer id);	
	/***
	 * 根据问题id查询当前待办人
	 * @param id
	 * @return
	 */
	public String findPreSolverNameById(Integer id);
	

	/**
	 *　根据问题id 查询　当前待处理人的　id 
	 */
	public List<Integer> findPreSolverIdById(Integer id);
}
