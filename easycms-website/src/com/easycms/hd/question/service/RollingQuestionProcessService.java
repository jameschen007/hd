package com.easycms.hd.question.service;

import java.util.List;

import com.easycms.hd.question.domain.RollingQuestionProcess;

public interface RollingQuestionProcessService {
	public RollingQuestionProcess add(RollingQuestionProcess p);
	public RollingQuestionProcess update(RollingQuestionProcess p);
	public int updateStatusByQuestionIdAndSolv(Integer qId,String s,Integer solv);
	public List<RollingQuestionProcess> findByQuestionId(Integer id);
}
