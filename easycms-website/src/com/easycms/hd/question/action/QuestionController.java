package com.easycms.hd.question.action;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.api.baseservice.question.QuestionTransForResult;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.question.RollingQuestionResult;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/hd/workstep/question")
@Transactional
public class QuestionController {
  private Logger logger = Logger.getLogger(QuestionController.class);
  
  @Autowired
  private RollingPlanService rollingPlanService;
  @Autowired
  private RoleService roleService;
  @Autowired
  private DepartmentService departmentService;
  @Autowired
  private UserService userService;
  @Autowired
  private VariableSetService variableSetService;
  @Autowired
  private QuestionApiService questionApiService;
  @Autowired
  private RollingQuestionService questionService;
  @Autowired
  private RollingQuestionSolverService rollingQuestionSolverService;
  @Autowired
  private RollingPlanApiService rollingPlanApiService;


  @RequestMapping(value = "", method = RequestMethod.GET)
  public String listUI(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page ");
    String userIdStr = WebUtility.getUserIdFromSession(request);
    User loginUser = null;
    if(CommonUtility.isNonEmpty(userIdStr)){
    	loginUser = userService.findUserById(Integer.parseInt(userIdStr));
    }
    String loginRole = "other";
    boolean chooseUser = false;
    List<Role> roles = loginUser.getRoles();
    //下级人员列表
    List<User> childrenUsers = null;
    for (Role role : roles) {
    	Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
    	if (roleNameList.contains("team")) {
    		loginRole = "team";
    	}else if (roleNameList.contains("monitor")) {
    		chooseUser = true;
    		loginRole = "monitor";
    		childrenUsers = userService.findUserByDeptParentIdAndRoleName(loginUser.getId(),ConstantVar.TEAM);
    	}else if (roleNameList.contains(ConstantVar.COORDINATOR)) {
    		loginRole = "coordinator";
    	}else if (roleNameList.contains(ConstantVar.solver)) {
    		loginRole = "solver";
    	}else if (roleNameList.contains(ConstantVar.supervisor)) {
    		chooseUser = true;
    		loginRole = "solver3";
    		childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(358).getChildren(), new String[]{"solver"});
    	}else if (roleNameList.contains(ConstantVar.solver3)) {
    		chooseUser = true;
    		loginRole = "solver4";//TODO 这里上下两行是没有看明白的。
			childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(358).getChildren(), new String[]{ConstantVar.supervisor});
    	}else if (roleNameList.contains(ConstantVar.solver4)) {
    		chooseUser = true;
    		loginRole = "solver4";//下在的人员列表应该是用于分派使用的。了吧
//			childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(314).getChildren(), new String[]{ConstantVar.solver3});
    	}else if (roleNameList.contains("captain")) {
    		chooseUser = true;
    		loginRole = "captain";
    		childrenUsers = userService.findUserByDeptParentIdAndRoleName(loginUser.getId(),ConstantVar.MONITOR);
    	}
    }
    chooseUser = false;
    request.setAttribute("chooseUser", chooseUser);
    request.setAttribute("loginRole", loginRole);
    request.setAttribute("childrenUsers", childrenUsers);
    
    return ForwardUtility.forwardAdminView("/hdService/question/list_question");
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public String listData(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/question/data/data_json_question");
  }

  @RequestMapping(value = "/detail", method = RequestMethod.GET)
  public String detail(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request question detail data ");
    String id = request.getParameter("qid");
    Integer qid = (id==null) ? 0 : Integer.parseInt(id);
    String loginIdStr = WebUtility.getUserIdFromSession(request);
    Integer loginId = null;
    if(CommonUtility.isNonEmpty(loginIdStr)){
    	loginId = Integer.parseInt(loginIdStr);
    }
    
    RollingQuestion q = questionService.findById(qid);

    RollingQuestionResult aQues = null;
	
	aQues = questionApiService.transferForList(q,true,loginId);
	
	aQues.setRollingPlan(rollingPlanApiService.rollingPlanTransferToSixLevelRollingPlan(rollingPlanService.findById(aQues.getRollingPlanId())));
    
	//获取不能处理的原因
	List<RollingQuestionSolver> solvers = rollingQuestionSolverService.findByQuestionIdAndUserId(q.getId(), loginId);
	List<RollingQuestionSolver> solver = solvers.stream().filter(s ->{
		return QuestionStatusDbEnum.unsolved.toString().equals(s.getStatus());
	}).collect(Collectors.toList());
	if(solver != null && solver.size() > 0){
		aQues.setUnableReason(solver.get(0).getUnableReason());
	}
    
    request.setAttribute("questionResult", aQues);

    return ForwardUtility.forwardAdminView("/hdService/question/page_question_detail");
  }

  @RequestMapping(value = "/handle", method = RequestMethod.GET)
  public String handle(HttpServletRequest request, HttpServletResponse response){
	  String questionId = request.getParameter("id");
	  String loginUser = WebUtility.getUserIdFromSession(request);
	  logger.info("Solve the rollingplan question by WEB. QuestionID:"+questionId+", loginUser:"+loginUser);
	 
	  request.setAttribute("questionId", questionId);
	  return ForwardUtility.forwardAdminView("/hdService/question/model_question_handle");
  }
  
  @RequestMapping(value = "/assgin", method = RequestMethod.GET)
  public String assgin(HttpServletRequest request, HttpServletResponse response){
	  String qId = request.getParameter("id");
	  String loginUserId = WebUtility.getUserIdFromSession(request);
	  Integer questionId = (qId==null) ? 0 : Integer.parseInt(qId);
	  Integer loginId = (loginUserId==null) ? 0 : Integer.parseInt(loginUserId);
	  logger.info("Assgin the rollingplan question by WEB. QuestionID:"+questionId+", loginUserId:"+loginId);
	 
	  RollingQuestion ques = questionService.findById(questionId);
	  if(ques == null){
		  throw new RuntimeException("问题"+questionId+"为空异常！");
	  }
	  boolean moniterCoordinateShowUserList = true ;
	  //根据当前登录人查出是哪种角色：1班长,2队部协，3技术，如果是班长时，显示协调人员列表；如果是协调时，显示处理人列表
	  Integer solverRole = null;
	  List<RollingQuestionSolver> oldSolver = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId, loginId);
	  if(oldSolver==null || oldSolver.size()==0){
		  moniterCoordinateShowUserList = false ;
	  }else{
		  RollingQuestionSolver loginSolver = oldSolver.get(0);
		  if(loginSolver==null){
			  throw new RuntimeException("问题"+questionId+"的当前登录处理人记录为空异常！");
		  }
		  solverRole = loginSolver.getSolverRole();
		  if(solverRole==null || solverRole.intValue()==0){
			  throw new RuntimeException("问题"+questionId+"的当前登录处理人记录为空0异常！");
		  }
	  }
	  
	  //查询技术人员列表
		boolean isSupervisor = false;
		User loginUser = userService.findUserById(loginId);
		List<Role> roles = loginUser.getRoles();
		for (Role role : roles) {
			Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
			if (roleNameList.contains(ConstantVar.supervisor)) {
				isSupervisor = true;
				break;
			}
		}
		
		
		String nextSolver = null;
		List<User> userList = null;
		List<UserResult> userListRet = null;
		RollingPlan rollingPlan = rollingPlanService.findById(ques.getRollingPlanId());
		QuestionTransForResult trans = new QuestionTransForResult();
		if(isSupervisor){
			moniterCoordinateShowUserList = true;
			nextSolver = "solver";
			userList = userService.findUserByDepartmentVariable("roleType", nextSolver,rollingPlan.getType());
		}
		if(solverRole!=null && solverRole.intValue()==1){
			nextSolver = "coordinationDepartment";
//			userList = userService.findUserByDepartmentVariableAndModule("departmentId", nextSolver,rollingPlan.getType());
			//查询协调人员不分专业
			userList = userService.findUserByRoleAndDepartmentVariable("coordinator",nextSolver);
		}else if(solverRole!=null && solverRole.intValue()==2){
			nextSolver = "solver";
			userList = userService.findUserByDepartmentVariable("roleType", nextSolver,rollingPlan.getType());
		}else{
			//技术角色本身，可组长角色本身（组长本身不应该报错）
		}
	  if(moniterCoordinateShowUserList){
		  userListRet = trans.user(userList);
	  }

	  
	  request.setAttribute("questionId", questionId);
	  request.setAttribute("userList", userListRet);
	  return ForwardUtility.forwardAdminView("/hdService/question/model_question_assgin");
  }
  
  @RequestMapping(value = "/unable", method = RequestMethod.GET)
  public String unable(HttpServletRequest request, HttpServletResponse response){
	  String questionId = request.getParameter("id");
	  String loginUser = WebUtility.getUserIdFromSession(request);
	  logger.info("Unable the rollingplan question by WEB. QuestionID:"+questionId+", loginUser:"+loginUser);
	 
	  request.setAttribute("questionId", questionId);
	  return ForwardUtility.forwardAdminView("/hdService/question/model_question_unable");
  }

}
