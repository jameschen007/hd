package com.easycms.hd.question.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.question.domain.RollingQuestionSolver;

@Transactional
public interface RollingQuestionSolverDao  extends Repository<RollingQuestionSolver,Integer>,BasicDao<RollingQuestionSolver>{
	
	@Query("From RollingQuestionSolver ps where ps.question.id = ?1")
	List<RollingQuestionSolver> findByQuestionId(Integer id);

	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.solver = ?2")
	List<RollingQuestionSolver> findByQuestionIdAndUserId(int questionId, Integer userid);

	@Query("From RollingQuestionSolver ps where ps.solver = ?1")
	List<RollingQuestionSolver> findByUserId(String id);

//	@Query("From RollingQuestionSolver ps where ps.solver = ?1 and ps.status = ?2")
//	List<RollingQuestionSolver> findByUserIdAndStatus(String userid, String status);

	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.solver = ?2")
	List<RollingQuestionSolver> findByQuestionIdAndUserId(int questionId,String userid);

	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.status = ?2")
	List<RollingQuestionSolver> findByIdAndStatus(Integer id, String status);
/**
 * 查问题被指派人用
 * @param problemId
 * @param solverType
 * @return
 */
	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.solverRole = ?2")
	List<RollingQuestionSolver> findByQuestionIdAndType(int problemId, int solverType);

	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.status = ?2 and ps.solverRole=?3")
	List<RollingQuestionSolver> findByQuestionIdAndStatus(int problemId, String status,Integer solverRole);
	@Query("From RollingQuestionSolver ps where ps.question.id = ?1 and ps.solverRole=?2")
	List<RollingQuestionSolver> findByQuestionIdAndSolverType(int problemId,Integer solverRole);
	/**
	 * 查询已超时的记录
	 * 状态 pre
solver_role = 2

当前系统时间 》 创建时间+3天
或
当前系统时间 》 delay 时间
	 */
	@Query(value="From RollingQuestionSolver ps where ?1 >createTime and status=?2 and solver_role=?3 and delayTime is null)")
	List<RollingQuestionSolver> findTimeoutSolverPlus3(Date sysdateMinus3,String status,Integer solverRole);

	@Query(value="From RollingQuestionSolver ps where ?1 > delayTime and status=?2 and solver_role=?3 and delayTime is not null)")
	List<RollingQuestionSolver> findTimeoutSolver(Date sysdate,String status,Integer solverRole);
}
