package com.easycms.hd.question.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.question.domain.RollingQuestion;

@Transactional
public interface RollingQuestionDao  extends Repository<RollingQuestion,Integer>,BasicDao<RollingQuestion>{

	@Query("FROM RollingQuestion p where p.id in ?1 order by p.questionTime desc")
	List<RollingQuestion> findByIdS(Integer[] ids);
//
//	@Query("FROM RollingQuestion p where p.worstepid = ?1 order by p.questionTime desc")
//	List<RollingQuestion> findByWorkstepId(int worstepid);

	@Query("FROM RollingQuestion p where p.owner = ?1 order by p.questionTime desc")
	List<RollingQuestion> findByUserId(String userid);

	@Query("FROM RollingQuestion p where p.id in ?1 and p.status = ?2 order by p.questionTime desc")
	List<RollingQuestion> findByIdsAndStatus(Integer[] ids, String status);

	@Query("FROM RollingQuestion p where p.owner = ?1 and p.status = ?2 order by p.questionTime desc")
   List<RollingQuestion> findByIdAndStatus(String userid, String status);
	
	@Query("FROM RollingQuestion p where p.id in ?1 and p.status = ?2 order by p.questionTime desc")
	 List<RollingQuestion> findConfirmByIdsAndStatus(Integer[] ids, String status);
	
	@Query("FROM RollingQuestion p where p.rollingPlanId = ?1 and isok = 0 order by p.questionTime desc")
	List<RollingQuestion> findByRollingPlanIdAndIsOk(int rolingPlanId);
	
	@Query("FROM RollingQuestion p where p.rollingPlanId = ?1 and p.up is null order by p.questionTime desc")
	List<RollingQuestion> findByRollingPlanId(int rid);
	
//	Page<RollingQuestion> findByWorstepidOrderByCreateOnDesc(int worstepid,Pageable page);
	
//	Page<RollingQuestion> findByCreatedByOrderByCreateOnDesc(String userid,Pageable page);
	
	@Query("FROM RollingQuestion p where p.owner = ?1 and (p.questionType like ?2 or p.describe like ?2) order by p.questionTime desc")
	Page<RollingQuestion> findByCreatedByOrderByCreateOnDesc(String userid, String keyword, Pageable page);

	@Query("FROM RollingQuestion p where p.owner = ?1 and (p.questionType like ?2 or p.describe like ?2) order by p.questionTime desc")
	Page<RollingQuestion> findByIdInAndIsOkOrderByCreateOnDesc(Integer[] ids, String status,Pageable page);
	
	@Query("FROM RollingQuestion p where p.id in ?1 and p.status = ?2 and (p.questionType like ?3 or p.describe like ?3) order by p.questionTime desc")
	Page<RollingQuestion> findByIdInAndIsOkOrderByCreateOnDesc(Integer[] ids, String status, String keyword, Pageable page);
//	Page<RollingQuestion> findByCreatedByAndConfirmOrderByCreateOnDesc(String userid, int con,Pageable page);
//	Page<RollingQuestion> findByIdInAndConfirmOrderByCreateOnDesc(Integer[] ids, int con,Pageable page);
	
	@Query("FROM RollingQuestion p where p.id in ?1 and p.status = ?2 and (p.questionType like ?3 or p.describe like ?3) order by p.questionTime desc")
	Page<RollingQuestion> findByIdInAndConfirmOrderByCreateOnDesc(Integer[] ids, String status, String keyword, Pageable page);
//	Page<RollingQuestion> findByIdInOrderByCreateOnDesc(Integer[] ids,Pageable page);
	
	@Query("FROM RollingQuestion p where p.id in ?1 and (p.questionType like ?2 or p.describe like ?2) order by p.questionTime desc")
	Page<RollingQuestion> findByIdInOrderByCreateOnDesc(Integer[] ids, String keyword, Pageable page);

	@Query("SELECT count(1) FROM RollingQuestion p where p.owner = ?1 order by p.questionTime desc")
    int countByUserId(String userid);

	@Query("SELECT count(1) FROM RollingQuestion p where p.owner = ?1 and p.status = ?2 order by p.questionTime desc")
    int findCountByUserIdAndConfirm(String userid, String status);

	@Query("SELECT count(1) FROM RollingQuestion p where p.id in ?1 and status = 'pre' order by p.questionTime desc")
    int findCountConcernedNotSolved(Integer[] ids);

	@Query("SELECT count(1) FROM RollingQuestion p where p.id in ?1 and status = 'pre' order by p.questionTime desc")
    int findByNeedToSolveCount(Integer[] ids);

//	Page<RollingQuestion> findByRollingPlanIdOrderByCreateOnDesc(int id,Pageable pageable);

	@Query("SELECT count(1) FROM RollingQuestion p where p.id in ?1 order by p.questionTime desc")
    int findCountConcerned(Integer[] ids);
	@Query("FROM RollingQuestion p where p.up.id = ?1 order by p.questionTime desc")
	List<RollingQuestion> findByUpId(int upId);
}
