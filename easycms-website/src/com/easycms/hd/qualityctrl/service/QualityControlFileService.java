package com.easycms.hd.qualityctrl.service;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.hd.qualityctrl.domain.QualityControlFile;

public interface QualityControlFileService {
	
	public QualityControlFile add(QualityControlFile qualityControlFile);
	public boolean delete(QualityControlFile qualityControlFile);
	public QualityControlFile update(QualityControlFile qualityControlFile);
	public QualityControlFile findById(Integer id);
	public Integer count();
	
	/**
	 * 获取指定质量问题ID的图片数据集合
	 * @param qcId
	 * @return
	 */
	public List<QualityControlFile> findByQctrlId(Integer qcId);
	
	public List<QualityControlFile> findByQcIdAndFileType(Integer qcId, String filetype);
	/**
	 * 上传质量图片文件，，如果是二次整改的，删除以前的图片。
	 * @param problemId
	 * @param files
	 * @param type
	 * @return
	 */
	public boolean createFilemoreUpload(Integer problemId, CommonsMultipartFile files[], String type);

}
