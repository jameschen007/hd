package com.easycms.hd.qualityctrl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.qualityctrl.dao.QualityControlStepDao;
import com.easycms.hd.qualityctrl.domain.QualityControlStep;
import com.easycms.hd.qualityctrl.service.QualityControlStepService;

@Service("qualityControlStepService")
public class QualityControlStepServiceImpl implements QualityControlStepService {
	
	@Autowired
	private QualityControlStepDao qualityControlStepDao;

	@Override
	public QualityControlStep add(QualityControlStep qualityControlStep) {
		return qualityControlStepDao.save(qualityControlStep);
	}

	@Override
	public boolean delete(QualityControlStep qualityControlStep) {
		return qualityControlStepDao.deleteById(qualityControlStep.getId()) > 0;
	}

	@Override
	public QualityControlStep update(QualityControlStep qualityControlStep) {
		return qualityControlStepDao.save(qualityControlStep);
	}

	@Override
	public QualityControlStep findById(Integer id) {
		return qualityControlStepDao.findById(id);
	}

	@Override
	public Integer count() {
		return qualityControlStepDao.count();
	}

	@Override
	public List<QualityControlStep> findByQctrlId(Integer qctrlId) {
		return qualityControlStepDao.findByQctrlId(qctrlId);
	}

	@Override
	public List<QualityControlStep> findByQctrlIdAndStepDetails(Integer qctrlId, String stepDetails) {
		return qualityControlStepDao.findByQctrlIdAndStep(qctrlId, stepDetails);
	}

}
