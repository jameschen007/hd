package com.easycms.hd.qualityctrl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.hd.qualityctrl.dao.QualityControlOptionsDao;
import com.easycms.hd.qualityctrl.domain.QualityControlOptions;

@Service("qualityControlOptionsService")
public class QualityControlOptionsServiceImpl {
	
	@Autowired
	private QualityControlOptionsDao qualityControlOptionsDao;

	public QualityControlOptions add(QualityControlOptions qualityControl) {
		return qualityControlOptionsDao.save(qualityControl);
	}

	
	public boolean delete(QualityControlOptions qualityControl) {
		return qualityControlOptionsDao.deleteById(qualityControl.getId()) > 0;
	}

	public QualityControlOptions update(QualityControlOptions qualityControl) {
		return qualityControlOptionsDao.save(qualityControl);
	}

	public List<QualityControlOptions> findAll(QualityControlOptions qualityControl) {
		return qualityControlOptionsDao.findAll(QueryUtil.queryConditions(qualityControl));
	}
	
	public QualityControlOptions findById(Integer id) {
		return qualityControlOptionsDao.findById(id);
	}


}
