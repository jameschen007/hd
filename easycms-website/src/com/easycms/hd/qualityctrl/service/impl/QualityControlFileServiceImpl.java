package com.easycms.hd.qualityctrl.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.hd.qualityctrl.dao.QualityControlFileDao;
import com.easycms.hd.qualityctrl.domain.QualityControlFile;
import com.easycms.hd.qualityctrl.service.QualityControlFileService;

@Service("qualityControlFileService")
public class QualityControlFileServiceImpl implements QualityControlFileService {
	@Autowired
	private QualityControlFileDao qualityControlFileDao;

	@Override
	public QualityControlFile add(QualityControlFile qualityControlFile) {
		return qualityControlFileDao.save(qualityControlFile);
	}

	@Override
	public boolean delete(QualityControlFile qualityControlFile) {
		return qualityControlFileDao.deleteById(qualityControlFile.getId()) > 0;
	}

	@Override
	public QualityControlFile update(QualityControlFile qualityControlFile) {
		return qualityControlFileDao.save(qualityControlFile);
	}

	@Override
	public QualityControlFile findById(Integer id) {
		return qualityControlFileDao.findById(id);
	}

	@Override
	public Integer count() {
		return qualityControlFileDao.count();
	}

	@Override
	public List<QualityControlFile> findByQctrlId(Integer qcId) {
		return qualityControlFileDao.findByQctrlId(qcId);
	}

	@Override
	public List<QualityControlFile> findByQcIdAndFileType(Integer qcId, String filetype) {
		return qualityControlFileDao.findByqcIdFileType(qcId, filetype);
	}

	/**
	 * 文件上传
	 * @param problemId
	 * @param files
	 * @return
	 */
	@Override
	public boolean createFilemoreUpload(Integer problemId, CommonsMultipartFile files[], String type) {
		// 上传位置  设定文件保存的目录 
		String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath+DateUtility.getYearMonth();
		List<FileInfo> fileInfos = UploadUtil.moreUpload(filePath, "utf-8", true, files);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				QualityControlFile file = new QualityControlFile();
				file.setFilepath(fileInfo.getNewFilename());
				file.setUploadDate(new Date());
				file.setQcId(problemId);
				if(fileInfo.getFilename()!=null && fileInfo.getFilename().length()>50){
					file.setFilename(fileInfo.getFilename().substring(0, 50));
				}else{
					file.setFilename(fileInfo.getFilename());
				}
				file.setFiletype(type);
				qualityControlFileDao.save(file);
			}
		}
		return true;
	}
}
