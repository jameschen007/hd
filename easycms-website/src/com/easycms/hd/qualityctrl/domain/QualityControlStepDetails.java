package com.easycms.hd.qualityctrl.domain;

public enum QualityControlStepDetails {

	/**
	 * QC主任指派QC阶段
	 */
	QCLeaderAssign,
	/**
	 * 责任部门指派责任班组人员
	 */
	responsibilityLeaderAssign,
	
	/**
	 * QC指派整改队伍阶段
	 */
	QCAssign,
	
	/**
	 * 整改阶段
	 */
	TeamRenovete,
	
	/**
	 * QC核实整改结果阶段
	 */
	QCverify

}
