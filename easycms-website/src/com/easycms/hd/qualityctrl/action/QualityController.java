package com.easycms.hd.qualityctrl.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.StringUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.request.qualityctrl.QcAssignRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcNoteRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcRenovateResultRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcResultCheckEnum;
import com.easycms.hd.api.request.qualityctrl.QualityControlRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlResult;
import com.easycms.hd.api.service.HseProblemApiService;
import com.easycms.hd.api.service.QualityControlApiService;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/quality")
@Slf4j
public class QualityController {
	
	@Autowired
	private QualityControlService qualityControlService;
	@Autowired
	private QualityControlApiService qualityControlApiService;
	@Autowired
	private HseProblemApiService hseProblemApiService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showList(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("form") BasicForm form) throws Exception {
		String func = form.getFunc();
		log.debug("[func] = " + func + " : " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/quality/list_quality_problem");
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("form") QualityControl form) throws Exception {
		log.debug("==> Show quality data list.");
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        form.setUserId(loginUser.getId());
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		
		return ForwardUtility.forwardAdminView("/quality/data/data_json_quality_problem");
	}
	
	/**
	 * 新增质量问题
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String id = request.getParameter("id");
		int deptId = ("".equals(id) || id == null) ? 0 : Integer.parseInt(id);
		log.debug("DeptId: "+ deptId);
//		HseProblemCreatUiDataResult resultData = new HseProblemCreatUiDataResult();
//		resultData = hseProblemApiService.getHseProblemCreatUiResult(deptId,null);
//		request.setAttribute("resultData", resultData);
		return ForwardUtility.forwardAdminView("/quality/page_quality_problem_add");
	}
	
	/**
	 * 保存新增
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") QualityControlRequestForm qualityForm,
			@RequestParam(value="file") CommonsMultipartFile files[]) throws Exception{
		boolean status = false;
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(qualityForm));
		
		String loginId = WebUtility.getUserIdFromSession(request);
		if(CommonUtility.isNonEmpty(loginId)){
			qualityForm.setLoginId(Integer.parseInt(loginId));			
		}else{
			HttpUtility.writeToClient(response, "false");
			return;
		}
		
		if(files == null){
			HttpUtility.writeToClient(response, "false");
			return;
		}
		status = qualityControlApiService.createQualityProblem(qualityForm, files);

		HttpUtility.writeToClient(response, String.valueOf(status));
	}
	
	/**
	 * 显示详情
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String showDetils(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id") Integer id){
		QualityControlResult qcResult = null;
		String showresponsibilityLeaderAssign = "false";
		QualityControl qc = qualityControlService.findById(id);
		if(qc != null){
			qcResult = qualityControlApiService.transferToResultList(qc);

	        HttpSession session = request.getSession();
	        User loginUser = (User) session.getAttribute("user");
	        
	        loginUser = userService.findUserById(loginUser.getId());//直接获取department获取不了，需要重新查询
			//如果是当前责任部门的人员，可以分派责任班组长
			if(qcResult.getResponsibleDept()!=null && loginUser.getDepartment().getId().intValue()==StringUtility.parseInt(qcResult.getResponsibleDept().getDeptId()).intValue()){
				showresponsibilityLeaderAssign = "true";
			}
		}
		log.info("createUser------->  "+qcResult.getCreateUser());
		log.info("FilePath------->  "+qcResult.getFiles().get(0).getPath());
		request.setAttribute("qcResult", qcResult);
		request.setAttribute("qcResult", qcResult);
		request.setAttribute("showresponsibilityLeaderAssign", showresponsibilityLeaderAssign);
		return ForwardUtility.forwardAdminView("/quality/page_quality_problem_detils");
		
	}

	@RequestMapping(value = "/qcLeaderAssign", method = RequestMethod.GET)
	public String qcLeaderAssignUI(HttpServletRequest request, HttpServletResponse response){
		List<User> userList = null;
		String id = request.getParameter("id");
		String idStr = WebUtility.getUserIdFromSession(request);
		int qcProblemId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);

		User user = userService.findUserById(loginId);	
		if(user != null){
			List<Department> departments = user.getDepartment().getChildren();
			if(departments==null ||departments.size()==0){
				departments = new ArrayList<Department>();
				departments.add(user.getDepartment());
			}
			//如果是QC专业室主任，则将下属QC1查询出来
			userList = userService.findUserByDepartmentAndRoleNames(departments, 
					new String[]{ConstantVar.witness_team_qc1,ConstantVar.witness_team_qc2,ConstantVar.witness_member_qc1,ConstantVar.witness_member_qc2,ConstantVar.qc_member});// "QC1");
		}
		request.setAttribute("qcProblemId", qcProblemId);
		request.setAttribute("qcList", userList);
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_qcleaderassgin");
	}
	
	/**
	 * QC专业室主任分派QC1
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/qcLeaderAssign", method = RequestMethod.POST)
	public JsonResult<Boolean> qcLeaderAssign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") QcAssignRequestForm form){
		log.info("Assign quality problem by WEB. QuestionID:"+form.getQcProblrmId()+" assignID："+form.getAssignedId());
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		String idStr = WebUtility.getUserIdFromSession(request);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);
		form.setLoginId(loginId);
		status = qualityControlApiService.qcLeaderAssign(form);
//		status = true;
		if(!status){
			result.setMessage("执行分派出错！");
		}
		result.setResponseResult(status);
		return result;
	}
	@RequestMapping(value = "/responsibilityLeaderAssign", method = RequestMethod.GET)
	public String responsibilityLeaderAssignUI(HttpServletRequest request, HttpServletResponse response){
		List<User> userList = null;
		String id = request.getParameter("id");
		String idStr = WebUtility.getUserIdFromSession(request);
		int qcProblemId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);

		User user = userService.findUserById(loginId);	
		if(user != null){
			
			//查询所有部门下的子部门，形成按部门选择下级部门的集合。
			
		}
		String responsibilityDeptId = request.getParameter("responsibilityDeptId");
		request.setAttribute("qcProblemId", qcProblemId);
		request.setAttribute("qcList", userList);
		request.setAttribute("responsibilityDeptId", responsibilityDeptId);
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_responsibilityleaderassgin");
	}
	
	/**
	 * 责任部门分派责任部门
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/responsibilityLeaderAssign", method = RequestMethod.POST)
	public JsonResult<Boolean> responsibilityLeaderAssign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") QcAssignRequestForm form){
		log.info("Assign quality problem by WEB. QuestionID:"+form.getQcProblrmId()+" assignID："+form.getAssignedId());
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		String idStr = WebUtility.getUserIdFromSession(request);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);
		form.setLoginId(loginId);
		status = qualityControlApiService.responsibilityLeaderAssign(form);
//		status = true;
		if(!status){
			result.setMessage("执行分派出错！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@RequestMapping(value = "/qcClose", method = RequestMethod.GET)
	public String qcCloseUI(HttpServletRequest request, HttpServletResponse response){
		String id = request.getParameter("id");
		int qcProblemId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		request.setAttribute("qcProblemId", qcProblemId);
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_qcclose");
	}
	
	/**
	 * 经QC1审核，当前问题不需要处理，QC1关闭问题
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/qcClose", method = RequestMethod.POST)
	public JsonResult<Boolean> qcClose(HttpServletRequest request, HttpServletResponse response,
			 @RequestParam(value="qualityFlag") boolean qualityFlag,
			@ModelAttribute("form") QcNoteRequestForm form){
		log.info("Close quality problem by WEB. QuestionID:"+form.getQcProblrmId()+" Close Note："+form.getNote());
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		String id = WebUtility.getUserIdFromSession(request);
		int loginId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		form.setLoginId(loginId);
		log.debug("开启质量问题单："+qualityFlag);
		try {
			status = qualityControlApiService.qcAssignClose(form,qualityFlag);
		} catch (Exception e) {
			e.printStackTrace();
			result.setResponseResult(status);
			return result;
		}
//		status = true;
		if(!status){
			result.setMessage("关闭问题失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@RequestMapping(value = "/qcConfirm", method = RequestMethod.GET)
	public String qcConfirmModel(HttpServletRequest request, HttpServletResponse response){
		String qcProblemId = request.getParameter("id");
		log.debug("质量问题ID："+qcProblemId);
		request.setAttribute("problemId", qcProblemId);
		request.setAttribute("responsibleDept", request.getParameter("responsibleDept"));
		request.setAttribute("responsibleTeam", request.getParameter("responsibleTeam"));
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_qcassgin");
	}
	
	/**
	 * 经QC1审核，确认存在质量问题
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/qcConfirm", method = RequestMethod.POST)
	public void qcConfirm(HttpServletRequest request, HttpServletResponse response){
		boolean status = false;
		String updateDeptId = request.getParameter("responsibleDept");
		String updateTeamId = request.getParameter("responsibleTeam");

		log.info("updateDeptId="+updateDeptId);
		log.info("updateTeamId="+updateTeamId);
		
		String id = request.getParameter("id");
		String idStr = WebUtility.getUserIdFromSession(request);
		Integer qcProblemId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);
		String targetDate = request.getParameter("timelimit");
		Long timeLimit = 1500000000000L;
		if(CommonUtility.isNonEmpty(targetDate)){
			targetDate += " 23:59:59";
			timeLimit = CommonUtility.convertDateStringToLong(targetDate);			
		}
		log.info("Confirm the quality problem by WEB. QuestionID:"+qcProblemId+" LoginId："+loginId+" TimeLimit： "+targetDate);
		QcAssignRequestForm form = new QcAssignRequestForm();
		form.setQcProblrmId(qcProblemId);
		form.setLoginId(loginId);
		
		try{
			Map<String,Object> rsMap = qualityControlApiService.qcAssignRenovateTeam(form,timeLimit,StringUtility.parseInt(updateDeptId),StringUtility.parseInt(updateTeamId));
			if(!rsMap.isEmpty()){
				status = (Boolean) rsMap.get("status");
			}
		}catch(Exception e){
			e.printStackTrace();
			WebUtility.writeToClient(response, "false");
		}
//		status = true;
		WebUtility.writeToClient(response, String.valueOf(status));

	}
	
	/**
	 * 责任单位整改合格，QC1结束当前问题
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/qcFinish", method = RequestMethod.GET)
	public String qcFinishUI(HttpServletRequest request, HttpServletResponse response){
		String qcProblemId = request.getParameter("id");
		log.debug("质量问题ID："+qcProblemId);
		request.setAttribute("qcProblemId", qcProblemId);
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_reVerify");
	}
	@RequestMapping(value = "/qcFinish", method = RequestMethod.POST)
	public void qcFinish(HttpServletRequest request, HttpServletResponse response){
		String id = request.getParameter("qcProblrmId");
		String idStr = WebUtility.getUserIdFromSession(request);
		Integer qcProblemId = ("".equals(id)||null==id)? 0 : Integer.parseInt(id);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);
		String isQualified = request.getParameter("qcResultCheckEnum");
		QcNoteRequestForm form = new QcNoteRequestForm();
		form.setQcProblrmId(qcProblemId);
		form.setLoginId(loginId);
		if(QcResultCheckEnum.Unqualified.toString().equals(isQualified)){
			form.setNote(request.getParameter("note"));
		}
		try {
			qualityControlApiService.qcVerifyResult(form,QcResultCheckEnum.valueOf(isQualified));
		} catch (Exception e) {
			e.printStackTrace();
			WebUtility.writeToClient(response, "false");
		}
		WebUtility.writeToClient(response, "true");
	}
	
	@RequestMapping(value = "/renovateResult", method = RequestMethod.GET)
	public String renovateResultModel(HttpServletRequest request, HttpServletResponse response){
		String qcProblemId = request.getParameter("id");
		request.setAttribute("problemId", qcProblemId);
		return ForwardUtility.forwardAdminView("/quality/model_quality_problem_submitRenovateResult");
	}
	
	@ResponseBody
	@RequestMapping(value = "/renovateResult", method = RequestMethod.POST)
	public void renovateResult(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QcRenovateResultRequestForm qcRequestForm,@RequestParam(value="file") CommonsMultipartFile[] files){
		boolean status = false;
		String idStr = WebUtility.getUserIdFromSession(request);
		Integer loginId = ("".equals(idStr)||null==idStr)? 0 : Integer.parseInt(idStr);
		log.info("Submit the renovate result by WEB. QuestionID:"+qcRequestForm.getQcProblrmId()+"LoginID:"+loginId);
		
		qcRequestForm.setLoginId(loginId);
		status = qualityControlApiService.teamRenoveteResult(qcRequestForm, files);
		
		if(status){
			WebUtility.writeToClient(response, "true");			
		}else{
			WebUtility.writeToClient(response, "false");		
		}
	}
	
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public void statisticalReportGet(HttpServletRequest request, HttpServletResponse response){
    	response.setHeader("Content-Disposition", "attachment");
    	HttpUtility.writeToClient(response, "true");
	}
	
	@RequestMapping(value = "/report", method = RequestMethod.POST)
	public void statisticalReportPost(HttpServletRequest request, HttpServletResponse response){
		String mimetype = "application/vnd.ms-excel;charset=utf-8";
    	response.setContentType(mimetype);
    	String filename = CommonUtility.getPropertyFile("qc.properties").getProperty("ExportFileName");
    	filename += ".xls";
    	String headerValue = "attachment;";
    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
    	response.setHeader("Content-Disposition", headerValue);
    	qualityControlService.statisticalReport(request, response);
	}

}
