package com.easycms.hd.qualityctrl.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.qualityctrl.domain.QualityControlDetils;
import com.easycms.hd.qualityctrl.service.QualityControlService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QualityDetilsDirective extends BaseDirective<QualityControlDetils> {
	
	@Autowired
	private QualityControlService qualityControlService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return qualityControlService.count();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected QualityControlDetils field(Map params, Map<String, Object> envParams) {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<QualityControlDetils> list(Map params, String filter, String order, String sort, boolean pageable,
			Page<QualityControlDetils> pager, Map<String, Object> envParams) {
		String status = FreemarkerTemplateUtility.getStringValueFromParams(params, "status");
		log.debug("status------->"+status);

		pager = qualityControlService.findDetilsByStatus(pager, status);

		return pager.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<QualityControlDetils> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
