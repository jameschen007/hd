package com.easycms.hd.witness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.witness.dao.WitnessFileDao;
import com.easycms.hd.witness.domain.WitnessFile;
import com.easycms.hd.witness.service.WitnessFileService;

@Service("witnessFileService")
public class WitnessFileServiceImpl implements WitnessFileService{

	@Autowired
	private WitnessFileDao witnessFileDao;
	@Override
	public WitnessFile add(WitnessFile pfile) {
		return witnessFileDao.save(pfile);
	}
	@Override
	public List<WitnessFile> findFilesByWitnessId(Integer id) {
	
		return witnessFileDao.findByWitnessId(id);
	}

}
