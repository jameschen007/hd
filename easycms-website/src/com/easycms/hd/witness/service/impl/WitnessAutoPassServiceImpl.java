package com.easycms.hd.witness.service.impl;

import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepFlagService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.quartz.service.JobService;

@Service("witnessAutoPassService")
public class WitnessAutoPassServiceImpl implements JobService, Serializable {
	private static final long serialVersionUID = -5690901744850949508L;
	private static final Logger logger = LoggerFactory.getLogger(WitnessAutoPassServiceImpl.class);
	
	private int witnessId;
	
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private WorkStepFlagService workStepFlagService;

	@SuppressWarnings("unchecked")
	@Override
	public void doJob(Object jobDataMap) {
		Map<String, Object> jobMap = (Map<String, Object>) jobDataMap;
		
		this.witnessId = (Integer) jobMap.get("witnessId");
		WorkStepWitness wsw = witnessService.findById(witnessId);
		wsw.setIsok(2);
		wsw = witnessService.add(wsw);
		
		if (null != wsw){
			WorkStep workStep = wsw.getWorkStep();
			
			if (null != workStep){
				workStep.setNoticeresult("2");
				workStep.setNoticeresultdesc("--超时系统自动通过见证--");
				workStep.setStepflag(StepFlag.DONE);
				workStep = workStepService.add(workStep);
				if (null != workStep){
					try {
						workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),"");
					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info("[ " + wsw.getWorkStep().getStepno() + " -- " + wsw.getWorkStep().getStepname() + " ] is auto pass.");
				}
			}
		}
	}
}
