package com.easycms.hd.witness.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.ConfigurationUtil;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.plan.domain.FakeMap;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.form.WorkStepForm;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.hd.witness.domain.WitnessFile;
import com.easycms.hd.witness.form.WorkStepWitnessForm;
import com.easycms.hd.witness.service.WitnessFileService;
import com.easycms.hd.witness.service.WitnessFlagService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/witness")
@Slf4j
public class WitnessController {
	private static String WITNESS_RESULT = "witnessResult.properties";
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private WitnessApiService witnessApiService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private WitnessFlagService witnessFlagService;
	@Autowired
	private WitnessFileService witnessFileService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_witness_path']}")
	private String fileWitnessPath;
	
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}

//-----------------------------------------[2017-10-03]-------------------------------------------------------------
	@RequestMapping(value = "/witnesser", method = RequestMethod.GET)
	public String listUI(HttpServletRequest request, HttpServletResponse response){
		log.debug("request witness list page ");
		return ForwardUtility.forwardAdminView("/hdService/witness/list_witness_assign");
	}

	@RequestMapping(value = "/witnesser", method = RequestMethod.POST)
	public String listData(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		log.debug("request witness list page ");
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		User user = userService.findUserById(loginUser.getId());
		if (null != user){
			Department department = user.getDepartment();

			if (null != department){
				form.setWitness(CommonUtility.toJson(department.getId()));
			}
		}
		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("witnesser");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hdService/witness/data/data_json_witness_assign");
	}

	@RequestMapping(value = "/witnesser/already", method = RequestMethod.GET)
	public String listUIAlready(HttpServletRequest request, HttpServletResponse response){
		log.debug("request witness list page ");
		return ForwardUtility.forwardAdminView("/hdService/witness/list_witness_assign_already");
	}

	@RequestMapping(value = "/witnesser/already", method = RequestMethod.POST)
	public String listDataAlready(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		log.debug("request witness list page ");
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		User user = userService.findUserById(loginUser.getId());
		if (null != user){
			Department department = user.getDepartment();

			if (null != department){
				form.setWitness(CommonUtility.toJson(department.getId()));
			}
		}
		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("already");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hdService/witness/data/data_json_witness_assign_already");
	}
	
	@RequestMapping(value = "/mylaunch", method = RequestMethod.GET)
	public String listMylaunchUI(HttpServletRequest request, HttpServletResponse response){
		log.debug("request witness mylaunch list page ");
		return ForwardUtility.forwardAdminView("/hdService/witness/list_witness_mylaunch1");
	}

	@RequestMapping(value = "/mylaunch", method = RequestMethod.POST)
	public String listMylaunchData(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		log.debug("request witness mylaunch list page ");
		form.setCustom("mylaunch");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hdService/witness/data/data_json_witness_mylaunch1");
	}
	
	@RequestMapping(value = "/mylaunch/view", method = RequestMethod.GET)
	public String mylaunchViewUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_mylaunch_view");
	}
	
	@RequestMapping(value = "/witnesser/assign", method = RequestMethod.GET)
	public String assignUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		User user = ContextUtil.getCurrentLoginUser();
		
		List<User> members = null;
		String qcDepartment = variableSetService.findValueByKey("departmentQc", "departmentId");
		Integer qcDepartmentId= Integer.valueOf(qcDepartment);
		for (Role role : user.getRoles()) {
			Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
			if (roleNameList.contains("witness_team_qc1")) {
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc1");
				break;
			}else if(roleNameList.contains("witness_team_qc2")){
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc2");
				break;
			} else if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
				String type = CommonUtility.isNonEmpty(form.getType()) ? form.getType() : NoticePointType.CZEC_QC.name();
				String roleType = "witness_member_czecqc";
				if (type.equals(NoticePointType.CZEC_QC.name())) {
					roleType = "witness_member_czecqc";
				} else if (type.equals(NoticePointType.CZEC_QA.name())) {
					roleType = "witness_member_czecqa";
				} else if (type.equals(NoticePointType.PAEC.name())) {
					roleType = "witness_member_paec";
				}
				members = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{roleType});
				break;
			}
		}
		
		request.setAttribute("member", members);
		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_assign");
	}

	@RequestMapping(value = "/witnesser/assign", method = RequestMethod.POST)
	public void assign(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if (null == user){
			return;
		}	

		if (null != form.getWitness()){
			boolean QC2Member = false;
			for (Role role : user.getRoles()) {
				Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
				QC2Member = ComputedConstantVar.qc2OrFqQc1(roleNameList);
			}
			if (QC2Member) {
				witnessApiService.assignToWitnesserQC2(new Integer[]{form.getId()}, user.getId(), form.getWitness());
			} else{
				witnessApiService.assignToWitnessMember(new Integer[]{form.getId()}, user.getId(), form.getWitness());
			}
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson("true"));
		return;
	}
	

	@RequestMapping(value = "/witnesser/batch_assign", method = RequestMethod.GET)
	public String batchAssignUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		User user = ContextUtil.getCurrentLoginUser();
		
		List<User> members = null;
		String qcDepartment = variableSetService.findValueByKey("departmentQc", "departmentId");
		Integer qcDepartmentId= Integer.valueOf(qcDepartment);
		for (Role role : user.getRoles()) {
			Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
			if (roleNameList.contains("witness_team_qc1")) {
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc1");
				break;
			}else if(roleNameList.contains("witness_team_qc2")){
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc2");
				break;
			} else if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
				String type = CommonUtility.isNonEmpty(form.getType()) ? form.getType() : NoticePointType.CZEC_QC.name();
				String roleType = "witness_member_czecqc";
				if (type.equals(NoticePointType.CZEC_QC.name())) {
					roleType = "witness_member_czecqc";
				} else if (type.equals(NoticePointType.CZEC_QA.name())) {
					roleType = "witness_member_czecqa";
				} else if (type.equals(NoticePointType.PAEC.name())) {
					roleType = "witness_member_paec";
				}
				members = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{roleType});
				break;
			}
		}
		
		request.setAttribute("member", members);

		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_batch_assign");
	}

	@RequestMapping(value = "/witnesser/batch_assign", method = RequestMethod.POST)
	public void batchAssign(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepForm form){
		User user = (User) request.getSession().getAttribute("user");
		if (null == user){
			return;
		}	
		boolean QC2Member = false;
		for (Role role : user.getRoles()) {
			Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
			QC2Member =  ComputedConstantVar.qc2OrFqQc1(roleNameList) ;
		}
		if (QC2Member) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(witnessApiService.assignToWitnesserQC2(form.getIds(), user.getId(), form.getWitness())));
		} else { 
			HttpUtility.writeToClient(response, CommonUtility.toJson(witnessApiService.assignToWitnessMember(form.getIds(), user.getId(), form.getWitness())));
		}
		return;
	}

	@RequestMapping(value = "/myevent", method = RequestMethod.GET)
	public String myeventListUI(HttpServletRequest request, HttpServletResponse response){
		log.debug("request witness list page ");
		return ForwardUtility.forwardAdminView("/hdService/witness/list_witness");
	}

	@RequestMapping(value = "/myevent", method = RequestMethod.POST)
	public String myeventListData(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		log.debug("request witness list page ");
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		if (null == loginUser){
			return ForwardUtility.forwardAdminView("login");
		}
		form.setWitnesser(CommonUtility.toJson(loginUser.getId()));

		form.setFilter(CommonUtility.toJson(form));
		form.setCustom("myevent");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hdService/witness/data/data_json_witness");
	}

	@RequestMapping(value = "/myevent/already", method = RequestMethod.GET)
	public String myeventAlreadyListUI(HttpServletRequest request, HttpServletResponse response){
		log.debug("request witness list page ");
		return ForwardUtility.forwardAdminView("/hdService/witness/list_witness_already");
	}

	@RequestMapping(value = "/myevent/already", method = RequestMethod.POST)
	public String myeventAlreadyListData(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		log.debug("request witness list page ");
		form.setCustom("myeventAlready");
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hdService/witness/data/data_json_witness_already");
	}

	@RequestMapping(value = "/myevent/launch", method = RequestMethod.GET)
	public String myeventUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form) throws IOException{
		Map<String, String> witnessTypeMap = ConfigurationUtil.readFileAsMap(WITNESS_RESULT);
		form.setMap(witnessTypeMap);

		List<FakeMap> fakeMapList = new ArrayList<FakeMap>();

		Iterator<Entry<String, String>> fakeMapIter = witnessTypeMap.entrySet().iterator();
		while(fakeMapIter.hasNext()) {
			Entry<String, String> entry = fakeMapIter.next();
			FakeMap fakeMap = new FakeMap();
			fakeMap.setKey(entry.getKey());
			fakeMap.setValue(entry.getValue());
			fakeMapList.add(fakeMap);
		}
		form.setFakeMap(fakeMapList);

		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_mywitness");
	}

	@RequestMapping(value = "/myevent/launch", method = RequestMethod.POST)
	public void myevent(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") WorkStepWitnessForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		if (null == loginUser){
			return;
		}

		WorkStepWitness wsw = witnessService.findById(form.getId());
		if (null != wsw){
			if (0 != wsw.getIsok()){
				HttpUtility.writeToClient(response, CommonUtility.toJson("false"));
				return;
			}
			wsw.setIsok(form.getIsok());
			wsw.setNoticeresultdesc(form.getNoticeresultdesc());
			wsw.setFailType(form.getFailType());
			wsw.setRemark(form.getRemark());
			wsw.setRealwitnessaddress(form.getWitnessaddress());
			wsw.setRealwitnessdata(form.getWitnessdate());
			wsw.setDosage(form.getDosage());
			wsw.setUpdatedBy(loginUser.getId());
			wsw.setUpdatedOn(new Date());
			wsw.setSubstitute(form.getSubstitute());

			wsw = witnessService.add(wsw);

			if (null != wsw && null != wsw.getParent()){
				try{
					witnessFlagService.changeWitnessFlag(wsw.getParent().getId(),form.getDosage());
				}catch(Exception e){
					e.printStackTrace();
					HttpUtility.writeToClient(response,"操作异常:"+e.getMessage());
					return ;
				}
				
			} else if (null != wsw){
				try{
					witnessFlagService.changeWitnessFlagSingle(wsw.getId(),form.getDosage());
				}catch(Exception e){
					e.printStackTrace();
					HttpUtility.writeToClient(response,"操作异常:"+e.getMessage());
					return ;
				}
			}
		}

		HttpUtility.writeToClient(response, CommonUtility.toJson(null != wsw));
		return;
	}
	
	
	@RequestMapping(value = "/myevent/launch/view", method = RequestMethod.GET)
	public String myeventViewUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form) throws IOException{
		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_result_view");
	}

	@RequestMapping(value = "/myevent/launch/edit", method = RequestMethod.GET)
	public String myeventEditUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form) throws IOException{
		Map<String, String> witnessTypeMap = ConfigurationUtil.readFileAsMap(WITNESS_RESULT);
		form.setMap(witnessTypeMap);

		List<FakeMap> fakeMapList = new ArrayList<FakeMap>();

		Iterator<Entry<String, String>> fakeMapIter = witnessTypeMap.entrySet().iterator();
		while(fakeMapIter.hasNext()) {
			Entry<String, String> entry = fakeMapIter.next();
			FakeMap fakeMap = new FakeMap();
			fakeMap.setKey(entry.getKey());
			fakeMap.setValue(entry.getValue());
			fakeMapList.add(fakeMap);
		}
		form.setFakeMap(fakeMapList);

		return ForwardUtility.forwardAdminView("/hdService/witness/modal_witness_mywitness_edit");
	}

	@RequestMapping(value = "/myevent/launch/edit", method = RequestMethod.POST)
	public void myeventEdit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") WorkStepWitnessForm form){
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		if (null == loginUser){
			return;
		}
		WorkStepWitness wsw = witnessService.findById(form.getId());
		if (null != wsw){
			wsw.setIsok(form.getIsok());
			wsw.setNoticeresultdesc(form.getNoticeresultdesc());
			wsw.setUpdatedBy(loginUser.getId());
			wsw.setUpdatedOn(new Date());

			wsw = witnessService.add(wsw);

			if (null != wsw && null != wsw.getParent()){
				try{
					witnessFlagService.changeWitnessFlag(wsw.getParent().getId(),form.getDosage());
				}catch(Exception e){
					e.printStackTrace();
					HttpUtility.writeToClient(response,"操作异常:"+e.getMessage());
				}
				
			}
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(null != wsw));
		return;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void importFile(HttpServletRequest request, HttpServletResponse response) {
		User user = (User) request.getSession().getAttribute("user");
		if (null == user){
			return;
		}
		
		String filePath = basePath + fileWitnessPath;
		
		String witnessId = request.getParameter("id");
		
		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "UTF-8", true, request);
		
		if (null != fileInfos && !fileInfos.isEmpty()){
			if (fileInfos != null && fileInfos.size() > 0) {
				for (FileInfo fileInfo : fileInfos) {
					WitnessFile file = new WitnessFile();
					file.setPath(fileInfo.getNewFilename());
					file.setTime(new Date());
					file.setWitnessid(Integer.parseInt(witnessId));
					if(fileInfo.getFilename()!=null && fileInfo.getFilename().length()>50){
						file.setFileName(fileInfo.getFilename().substring(0, 50));
					}else{
						file.setFileName(fileInfo.getFilename());
					}
					log.debug("upload file success : " + file.toString());
					witnessFileService.add(file);
				}
			}
		} else {  
			WebUtility.writeToClient(response, CommonUtility.toJson("false"));
			return;
		} 
		
		WebUtility.writeToClient(response, CommonUtility.toJson("true"));
		return;
	}
}

