package com.easycms.hd.witness.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.easycms.basic.BaseDirective;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.witness.domain.WitnessFile;
import com.easycms.hd.witness.service.WitnessFileService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;

public class WitnessDirective extends BaseDirective<WorkStepWitness>{
	private static final Logger logger = Logger.getLogger(WitnessDirective.class);
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private WitnessFileService witnessFileService;
	@Autowired
	private DepartmentService departmentService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected WorkStepWitness field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			WorkStepWitness workStepWitness = witnessService.findById(id);
			List<WitnessFile> witnessFiles = witnessFileService.findFilesByWitnessId(id);
			if (null != workStepWitness && null != witnessFiles) {
				workStepWitness.setWitnessFiles(witnessFiles);
			}
			return workStepWitness;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepWitness> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<WorkStepWitness> pager,
			Map<String, Object> envParams) {
		String type =  (String) ContextUtil.getSession().getAttribute("type");
		User loginUser =  ContextUtil.getCurrentLoginUser();
		
		String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		WorkStepWitness condition = null;
		
		String QC2Type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
		
		boolean QC2Member = false;
		if (null != loginUser.getRoleType() && !loginUser.getRoleType().isEmpty()) {
			QC2Member =  ComputedConstantVar.qc2OrFqQc1(loginUser.getRoleType()) ;
		}
		
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(WorkStepWitness.class, filter,"yyyy-MM-dd HH:mm:ss");
		}else{
			condition = new WorkStepWitness();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		if (pageable) {
			if (null != custom && custom.equals("witnesser")){
				if (QC2Member) {
					pager = witnessService.findMemberUnassignQC2WitnessByPage(loginUser.getId(), type, CommonUtility.isNonEmpty(QC2Type) ? QC2Type : NoticePointType.CZEC_QC.name(), pager);
				} else {
					pager = witnessService.findQCTeamUnassignWitnessByPage(loginUser.getId(), type, pager);
				}
				pager.setDatas(transferWitness(pager.getDatas()));
				logger.info("==> witnesser Witness length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			} else if (null != custom && custom.equals("already")){
				pager = witnessService.findQCTeamAssignedWitnessByPage(loginUser.getId(), type, pager);
				pager.setDatas(transferWitness(pager.getDatas()));
				logger.info("==> already Witness length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			} else if (null != custom && custom.equals("mylaunch")){
				pager = witnessService.findMyLaunchedWitnessByPage(loginUser.getId(), type, pager);
				pager.setDatas(transferWitness(pager.getDatas()));
				logger.info("==> my launch Witness length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			} else {
				pager = witnessService.findQCTeamUnassignWitnessByPage(loginUser.getId(), type, pager);
				
				pager.setDatas(transferWitness(pager.getDatas()));
				logger.info("------------------------------------------------------------------------------------------");
				logger.info("==> Witness length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			}
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepWitness> tree(Map params, Map<String, Object> envParams) {
		return null;
	}
	
	private List<WorkStepWitness> transferWitness(List<WorkStepWitness> datas){
		if (null != datas){
			for(WorkStepWitness workStepWitness : datas){
				if (null != workStepWitness.getWitness() && !"".equals(workStepWitness.getWitness().toString())){
					Department department = departmentService.findById(workStepWitness.getWitness());
					
					if (null != department){
						workStepWitness.setWitnesserName(department.getName());
					} else {
						workStepWitness.setWitnesserName(null);
					}
				}
			}
		}
		
		logger.info("==> Witness length [" + datas.size() + "]");
		return datas;
	}
}
