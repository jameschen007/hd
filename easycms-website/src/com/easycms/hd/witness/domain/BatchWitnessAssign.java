package com.easycms.hd.witness.domain;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;

public class BatchWitnessAssign extends BasicForm implements Serializable{
	private static final long serialVersionUID = -807375739373456782L;
	
	private String AQAW;
	private String AQAH;
	private String AQAR;
	private String AQC1W;
	private String AQC1H;
	private String AQC1R;
	private String AQC2W;
	private String AQC2H;
	private String AQC2R;
	private String BW;
	private String BH;
	private String BR;
	private String CW;
	private String CH;
	private String CR;
	private String DW;
	private String DH;
	private String DR;
	
	public String getAQAW() {
		return AQAW;
	}
	public void setAQAW(String aQAW) {
		AQAW = aQAW;
	}
	public String getAQAH() {
		return AQAH;
	}
	public void setAQAH(String aQAH) {
		AQAH = aQAH;
	}
	public String getAQAR() {
		return AQAR;
	}
	public void setAQAR(String aQAR) {
		AQAR = aQAR;
	}
	public String getAQC1W() {
		return AQC1W;
	}
	public void setAQC1W(String aQC1W) {
		AQC1W = aQC1W;
	}
	public String getAQC1H() {
		return AQC1H;
	}
	public void setAQC1H(String aQC1H) {
		AQC1H = aQC1H;
	}
	public String getAQC1R() {
		return AQC1R;
	}
	public void setAQC1R(String aQC1R) {
		AQC1R = aQC1R;
	}
	public String getAQC2W() {
		return AQC2W;
	}
	public void setAQC2W(String aQC2W) {
		AQC2W = aQC2W;
	}
	public String getAQC2H() {
		return AQC2H;
	}
	public void setAQC2H(String aQC2H) {
		AQC2H = aQC2H;
	}
	public String getAQC2R() {
		return AQC2R;
	}
	public void setAQC2R(String aQC2R) {
		AQC2R = aQC2R;
	}
	public String getBW() {
		return BW;
	}
	public void setBW(String bW) {
		BW = bW;
	}
	public String getBH() {
		return BH;
	}
	public void setBH(String bH) {
		BH = bH;
	}
	public String getBR() {
		return BR;
	}
	public void setBR(String bR) {
		BR = bR;
	}
	public String getCW() {
		return CW;
	}
	public void setCW(String cW) {
		CW = cW;
	}
	public String getCH() {
		return CH;
	}
	public void setCH(String cH) {
		CH = cH;
	}
	public String getCR() {
		return CR;
	}
	public void setCR(String cR) {
		CR = cR;
	}
	public String getDW() {
		return DW;
	}
	public void setDW(String dW) {
		DW = dW;
	}
	public String getDH() {
		return DH;
	}
	public void setDH(String dH) {
		DH = dH;
	}
	public String getDR() {
		return DR;
	}
	public void setDR(String dR) {
		DR = dR;
	}
}
