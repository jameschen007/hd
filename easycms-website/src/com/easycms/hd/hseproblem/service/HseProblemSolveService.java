package com.easycms.hd.hseproblem.service;

import java.util.List;
import java.util.Set;

import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;

public interface HseProblemSolveService {
	public HseProblemSolve add(HseProblemSolve hseProblemSolve);
	public boolean delete(HseProblem hseProblemSolve);
	public HseProblemSolve update(HseProblemSolve hseProblemSolve);
	public HseProblemSolve findById(Integer id);
	public List<HseProblemSolve> findByProblemId(Integer hseProblemId);
	public List<HseProblemSolve> findByProblenIdAndSolveStep(Integer id, String solveRole);
	public Set<String> findByStatus(String status);

}
