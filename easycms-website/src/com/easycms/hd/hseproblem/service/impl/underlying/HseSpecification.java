package com.easycms.hd.hseproblem.service.impl.underlying;

import com.easycms.hd.api.enums.hseproblem.*;
import com.easycms.hd.api.enums.hseproblem.underlying.FilterItemTimeFrame;
import com.easycms.hd.api.enums.hseproblem.underlying.TheFilterItem;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.hseproblem.domain.HseProblem3mDelay;
import com.easycms.hd.hseproblem.domain.HseProblem3mTotal;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wangzhi
 * @Description: 查询hseProblem的
 * @date 2019/7/2 11:57
 */
public class HseSpecification {

    /**
     * 连续三个月(90天)安全
     * 超期未整改 隐患总数 或　待整改数
     *
     * @param responsibleDept
     * @param oneOf3Type
     * @return
     */
    public Specification hse3mSpecification(final Integer responsibleDept, final Hse3mEnum oneOf3Type, final HseProblemExListRequestForm form, final HseProblemStatusEnum problemStatus) {


        if (oneOf3Type.equals(Hse3mEnum.delay)) {

            //超期未整改
            Specification<HseProblem3mDelay> querySpeci = new Specification<HseProblem3mDelay>() {
                @Override
                public Predicate toPredicate(Root<HseProblem3mDelay> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

                    Path<HseProblem3mDelay> path = root.get("id");
                    return getPredicate(root, query, builder, responsibleDept, oneOf3Type, problemStatus, form);
                }
            };
            return querySpeci;
        }else{

            //隐患总数 或　待整改数
            Specification<HseProblem3mTotal> querySpeci = new Specification<HseProblem3mTotal>() {
                @Override
                public Predicate toPredicate(Root<HseProblem3mTotal> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                    Path<HseProblem3mTotal> path = root.get("id");
                    return getPredicate(root, query, builder, responsibleDept, oneOf3Type, problemStatus, form);
                }
            };
            return querySpeci;
        }
    }

    private Predicate getPredicate(Root root, CriteriaQuery<?> query, CriteriaBuilder builder, Integer responsibleDept, Hse3mEnum oneOf3Type, HseProblemStatusEnum problemStatus, HseProblemExListRequestForm form) {
        Predicate predicates = null;
        predicates = builder.equal(root.get("responsibleDept"), responsibleDept);
        if (oneOf3Type.equals(Hse3mEnum.waitingModify)) {
            CriteriaBuilder.In<String> in = builder.in(root.get("problemStatus").as(String.class));
            in.value(HseProblemStatusEnum.Need_Handle.name());
            in.value(HseProblemStatusEnum.Renovating.name());
            predicates = builder.and(predicates, in);
        }
        if (problemStatus != null) {
            predicates = builder.and(predicates,
                    builder.equal(root.get("problemStatus"), problemStatus.name()));
        }

        DynamicFilterItemList inst = new DynamicFilterItemList();
        Map<String, List<TheFilterItem>> dynamic = null;
        //form 创建时间和截止时间筛选和排序
        DynamicFilterItems createTime = null;
        Boolean createTimeSort = false;//false=0降序=默认降序desc；true=1升序=asc
        //解析其中的0,1

        if (form != null && form.getCreateTime() != null) {
            Hse3mEnumCreateTime tmp = form.getCreateTime();

            createTime = DynamicFilterItems.valueOf(tmp.name().substring(0, tmp.name().length() - 1));
            createTimeSort = "1".equals(tmp.name().substring(tmp.name().length() - 1));
        }


        if (createTime != null) {
            if (dynamic==null) {
                dynamic = inst.getDynamic(true);
            }
            List<TheFilterItem> dynamics = dynamic.get("createTime");

            //正式进入时间筛选逻辑中
            DynamicFilterItems finalCreateTime = createTime;
            FilterItemTimeFrame theFilterItem = (FilterItemTimeFrame) dynamics.stream().filter(b -> {
                return b.getKey().equals(finalCreateTime.getCode());
            }).collect(Collectors.toList()).get(0);

           //theFilterItem.print();
            if (theFilterItem.getBegin() != null) {
                predicates = builder.and(predicates, builder.greaterThanOrEqualTo(root.get("createDate"), theFilterItem.getBegin()));
            }
            if (theFilterItem.getEnd() != null) {
                predicates = builder.and(predicates, builder.lessThanOrEqualTo(root.get("createDate"), theFilterItem.getEnd()));
            }

        }

        //targetDate 截止日期
        DynamicFilterItems targetTime = null;
        Boolean targetTimeSort = false;//false=0降序=默认降序desc；true=1升序=asc
        //解析其中的0,1

        if (form != null && form.getTargetTime() != null) {
            Hse3mEnumCutOffTime tmp = form.getTargetTime();

            targetTime = DynamicFilterItems.valueOf(tmp.name().substring(0, tmp.name().length() - 1));
            targetTimeSort = "1".equals(tmp.name().substring(tmp.name().length() - 1));
        }

        if (targetTime != null) {
            if (dynamic==null) {
                dynamic = inst.getDynamic(true);
            }
            List<TheFilterItem> dynamicstargetTime = dynamic.get("targetTime");
            //正式进入时间筛选逻辑中
            DynamicFilterItems finalTargetTime = targetTime;
            FilterItemTimeFrame theFilterItem = (FilterItemTimeFrame) dynamicstargetTime.stream().filter(b -> {
                return b.getKey().equals(finalTargetTime.getCode());
            }).collect(Collectors.toList()).get(0);

            //theFilterItem.print();
            if (theFilterItem.getBegin() != null) {
                predicates = builder.and(predicates, builder.greaterThanOrEqualTo(root.get("targetDate"), theFilterItem.getBegin()));
            }
            if (theFilterItem.getEnd() != null) {
                predicates = builder.and(predicates, builder.lessThanOrEqualTo(root.get("targetDate"), theFilterItem.getEnd()));
            }
        }


        //正式进入时间排序逻辑中
        List<Order> orders = new ArrayList<>();
        if (createTimeSort) {
            orders.add(builder.asc(root.get("createDate")));
        } else {
            orders.add(builder.desc(root.get("createDate")));
        }
        if (targetTimeSort) {
            orders.add(builder.asc(root.get("targetDate")));
        } else {
            orders.add(builder.desc(root.get("targetDate")));
        }

        query.orderBy(orders);
        return query.where(predicates).getRestriction();
    }
}
