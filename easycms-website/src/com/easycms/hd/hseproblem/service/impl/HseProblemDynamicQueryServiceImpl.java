package com.easycms.hd.hseproblem.service.impl;

import com.easycms.hd.api.enums.hseproblem.*;
import com.easycms.hd.api.enums.hseproblem.underlying.FilterItemTimeFrame;
import com.easycms.hd.api.enums.hseproblem.underlying.TheFilterItem;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.hseproblem.dao.HseProblem3mDelayDao;
import com.easycms.hd.hseproblem.dao.HseProblem3mTotalDao;
import com.easycms.hd.hseproblem.dao.HseProblemDao;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblem3mDelay;
import com.easycms.hd.hseproblem.domain.HseProblem3mTotal;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.easycms.hd.hseproblem.mybatis.dao.HseProblemMybatisDao;
import com.easycms.hd.hseproblem.service.impl.underlying.HseSpecification;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 专用于动态查询安全列表数据
 */
@Service("HseProblemDynamicQueryServiceImpl")
public class HseProblemDynamicQueryServiceImpl {
    public static Logger logger = Logger.getLogger(HseProblemDynamicQueryServiceImpl.class);
    @Autowired
    private HseProblemDao hseProblemDao;
    @Autowired
    private HseProblem3mDelayDao hseProblem3mDelayDao;
    @Autowired
    private HseProblem3mTotalDao hseProblem3mTotalDao;
    @Autowired
    private HseProblemMybatisDao hseProblemMybatisDao;


    //这里我不写大而全的查询，多写几个，可读性好一些，两百行以为？
//hseProblemDao.findByStatusAndTeamId(problemStatus, teamId, pageable)
    public Page<HseProblem> findByStatusAndTeamId(final String problemStatus, final Integer responsibleTeam, final Integer responsibleDept, Pageable pageable, HseProblemExListRequestForm hseProblemExListRequestForm) {

        Specification<HseProblem> querySpeci = new Specification<HseProblem>() {

            @Override
            public Predicate toPredicate(Root<HseProblem> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return getPredicate(root, criteriaQuery, criteriaBuilder
                        ,  responsibleDept,  responsibleTeam,  problemStatus!=null&&!"".equals(problemStatus)?HseProblemStatusEnum.valueOf(problemStatus):null,  null
                        ,  null, hseProblemExListRequestForm ,  null,  null);
            }
        };
        org.springframework.data.domain.Page<HseProblem> springPage = hseProblemDao.findAll(querySpeci, pageable);
        return springPage;
    }


    //hseProblemDao.findByStatusAndTeamId(problemStatus, teamId, pageable)
    public Page<HseProblem> findByStatusAndCreateUser(final String problemStatus, final Integer createUser, Pageable pageable, final HseProblemExListRequestForm form) {

        Specification<HseProblem> querySpeci = new Specification<HseProblem>() {

            @Override
            public Predicate toPredicate(Root<HseProblem> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

                return getPredicate(root, criteriaQuery, criteriaBuilder
                        ,  null,  null, problemStatus!=null&&!"".equals(problemStatus)?HseProblemStatusEnum.valueOf(problemStatus):null,  createUser
                        ,  null, form ,  null,  null);
            }
        };
        org.springframework.data.domain.Page<HseProblem> springPage = hseProblemDao.findAll(querySpeci, pageable);
        return springPage;
    }

    //findByHseProblemAndSolveStatus(String solveStatus, String status,Pageable page)
    public Page<HseProblem> findByHseProblemAndSolveStatus(final String solveStatus, final String problemStatus, Pageable pageable, final HseProblemExListRequestForm form) {
        Specification<HseProblem> querySpeci = new Specification<HseProblem>() {

            @Override
            public Predicate toPredicate(Root<HseProblem> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return getPredicate(root, criteriaQuery, criteriaBuilder, null, null, problemStatus!=null&&!"".equals(problemStatus)?HseProblemStatusEnum.valueOf(problemStatus):null,
                        null, null, null, null, solveStatus);
            }
        };
        org.springframework.data.domain.Page<HseProblem> springPage = hseProblemDao.findAll(querySpeci, pageable);
        return springPage;
    }


    public Page getHseProblem3mResult(com.easycms.core.util.Page page, final Integer responsibleDept, final Hse3mEnum oneOf3Type, final HseProblemExListRequestForm hseProblemExListRequestForm, final HseProblemStatusEnum problemStatus) {
        Specification specification = null;
        HseSpecification help = new HseSpecification();
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
        specification = help.hse3mSpecification(responsibleDept, oneOf3Type, hseProblemExListRequestForm, problemStatus);

        if (oneOf3Type.equals(Hse3mEnum.delay)) {
            //超期未整改
            org.springframework.data.domain.Page<HseProblem3mDelay> springPage = hseProblem3mDelayDao.findAll(specification, pageable);
            return springPage;
        } else {

            org.springframework.data.domain.Page<HseProblem3mTotal> springPage = hseProblem3mTotalDao.findAll(specification, pageable);
            return springPage;
        }
    }


    private Predicate getPredicate(Root root, CriteriaQuery<?> query, CriteriaBuilder builder
            , Integer responsibleDept, Integer responsibleTeam, HseProblemStatusEnum problemStatus, Integer createUser
            , String problemStep, HseProblemExListRequestForm form, String solveStep, String solveStatus) {
        Predicate predicates = builder.conjunction();

        //responsibleDept 责任部门
        if (org.springframework.util.StringUtils.isEmpty(responsibleDept) == false) {
            predicates = builder.and(predicates, builder.equal(root.get("responsibleDept"), responsibleDept));
        }
        /*responsibleTeam 责任班组*/
        if (org.springframework.util.StringUtils.isEmpty(responsibleTeam) == false) {
            predicates = builder.and(predicates, builder.equal(root.get("responsibleTeam"), responsibleTeam));
        }
        //problemStatus 问题状态
        if (org.springframework.util.StringUtils.isEmpty(problemStatus) == false) {
            predicates = builder.and(predicates, builder.equal(root.get("problemStatus"), problemStatus.name()));
        }
        //问题创建者 Integer createUser;
        if (org.springframework.util.StringUtils.isEmpty(createUser) == false) {
            predicates = builder.and(predicates, builder.equal(root.get("createUser"), createUser));
        }
        //problemStep 主表 处理步骤
        if (org.springframework.util.StringUtils.isEmpty(problemStep) == false) {
            predicates = builder.and(predicates, builder.equal(root.get("problemStep"), problemStep));
        }

        /**
         * 子表：HseProblemSolve　当前步骤　String solveStep;　处理状态　String solveStatus;
         */
        /**
         * 子表：HseProblemSolve　当前步骤　String solveStep;
         * hseProblemSolve.setSolveStep
         HSE确认当前问题 	hseConfirm
         HSE审核问题 	hseCheck
         HSE审核问题 	hseCheckAgain
         责任班组整改 	teamSolve
         责任班组重新整改 	teamSolveAgain
         */

        if (org.springframework.util.StringUtils.isEmpty(solveStep) == false || org.springframework.util.StringUtils.isEmpty(solveStatus) == false) {
            Subquery<HseProblem> query2a = query.subquery(HseProblem.class);
            Root<HseProblemSolve> root2a = (Root<HseProblemSolve>) query2a.from(HseProblemSolve.class);
            Predicate predicate2a = null;
            query2a.select(root2a.<HseProblem>get("problemId"));
            if (org.springframework.util.StringUtils.isEmpty(solveStep)==false) {
                //当前步骤　String solveStep
                predicate2a = builder.equal(root2a.get("solveStep").as(String.class), solveStep);
            }

            if (org.springframework.util.StringUtils.isEmpty(solveStatus)==false) {
                //处理状态　String solveStatus
                predicate2a = builder.equal(root2a.get("solveStatus").as(String.class), solveStatus);
            }
            query2a.where(predicate2a);
            predicates = builder.and(predicates, builder.in(root.get("id")).value(query2a));
        }

        /**
         * 子表：HseProblemSolve　 　处理状态　String solveStatus;
         * 待处理 Pre,已处理 Done
         */

        DynamicFilterItemList inst = new DynamicFilterItemList();
        Map<String, List<TheFilterItem>> dynamic = null;
        //form 创建时间和截止时间筛选和排序
        DynamicFilterItems createTime = null;
        Boolean createTimeSort = false;//false=0降序=默认降序desc；true=1升序=asc
        //解析其中的0,1

        if (form != null && form.getCreateTime() != null) {
            Hse3mEnumCreateTime tmp = form.getCreateTime();

            createTime = DynamicFilterItems.valueOf(tmp.name().substring(0, tmp.name().length() - 1));
            createTimeSort = "1".equals(tmp.name().substring(tmp.name().length() - 1));
        }


        if (createTime != null) {
            if (dynamic == null) {
                dynamic = inst.getDynamic(true);
            }
            List<TheFilterItem> dynamics = dynamic.get("createTime");

            //正式进入时间筛选逻辑中
            DynamicFilterItems finalCreateTime = createTime;
            FilterItemTimeFrame theFilterItem = (FilterItemTimeFrame) dynamics.stream().filter(b -> {
                return b.getKey().equals(finalCreateTime.getCode());
            }).collect(Collectors.toList()).get(0);

            //theFilterItem.print();
            if (theFilterItem.getEnd() != null) {
                predicates = builder.and(predicates, builder.lessThanOrEqualTo(root.get("createDate"), theFilterItem.getEnd()));
            }
            if (theFilterItem.getBegin() != null) {
                predicates = builder.and(predicates, builder.greaterThanOrEqualTo(root.get("createDate"), theFilterItem.getBegin()));
            }

        }

        //targetDate 截止日期
        DynamicFilterItems targetTime = null;
        Boolean targetTimeSort = false;//false=0降序=默认降序desc；true=1升序=asc
        //解析其中的0,1

        if (form != null && form.getTargetTime() != null) {
            Hse3mEnumCutOffTime tmp = form.getTargetTime();

            targetTime = DynamicFilterItems.valueOf(tmp.name().substring(0, tmp.name().length() - 1));
            targetTimeSort = "1".equals(tmp.name().substring(tmp.name().length() - 1));
        }

        if (targetTime != null) {
            if (dynamic == null) {
                dynamic = inst.getDynamic(true);
            }
            List<TheFilterItem> dynamicsTargetTime = dynamic.get("targetTime");
            //正式进入时间筛选逻辑中
            DynamicFilterItems finalTargetTime = targetTime;
            FilterItemTimeFrame theFilterItem = (FilterItemTimeFrame) dynamicsTargetTime.stream().filter(b -> {
                return b.getKey().equals(finalTargetTime.getCode());
            }).collect(Collectors.toList()).get(0);

            //theFilterItem.print();
            if (theFilterItem.getEnd() != null) {
                predicates = builder.and(predicates, builder.lessThanOrEqualTo(root.get("targetDate"), theFilterItem.getEnd()));
            }
            if (theFilterItem.getBegin() != null) {
                predicates = builder.and(predicates, builder.greaterThanOrEqualTo(root.get("targetDate"), theFilterItem.getBegin()));
            }
        }


        //正式进入时间排序逻辑中
        List<Order> orders = new ArrayList<>();
        if (createTimeSort) {
            orders.add(builder.asc(root.get("createDate")));
        } else {
            orders.add(builder.desc(root.get("createDate")));
        }
        if (targetTimeSort) {
            orders.add(builder.asc(root.get("targetDate")));
        } else {
            orders.add(builder.desc(root.get("targetDate")));
        }

        query.orderBy(orders);
        return query.where(predicates).getRestriction();
    }


}
