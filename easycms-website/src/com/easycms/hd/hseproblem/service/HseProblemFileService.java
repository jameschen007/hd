package com.easycms.hd.hseproblem.service;

import java.util.List;

import com.easycms.hd.hseproblem.domain.HseProblemFile;

public interface HseProblemFileService {
	public HseProblemFile add(HseProblemFile hseProblemFile);
	public boolean delete(HseProblemFile hseProblemFile);
	public HseProblemFile update(HseProblemFile hseProblemFile);
	public HseProblemFile findById(Integer id);
	public List<HseProblemFile> findByProblemId(Integer id);
	public List<HseProblemFile> findByProblemIdAndFileType(Integer id, String type);

}
