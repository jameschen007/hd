package com.easycms.hd.hseproblem.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hse_problem_file")
public class HseProblemFile implements Serializable {
	private static final long serialVersionUID = -4715643086250448113L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	/**
	 * 问题ID
	 */
	@Column(name="problem_id")
    private Integer problemId;

	/**
	 * 文件路径名
	 */
	@Column(name="filepath")
    private String filepath;

	/**
	 * 上传时间
	 */
	@Column(name="uploadtime")
    private Date uploadtime;

	/**
	 * 文件名
	 */
	@Column(name="filename")
    private String filename;
	
	/**
	 * 文件类型
	 */
	@Column(name="filetype")
    private String filetype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath == null ? null : filepath.trim();
    }

    public Date getUploadtime() {
        return uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
    
}