package com.easycms.hd.hseproblem.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.easycms.core.form.BasicForm;
import com.wordnik.swagger.annotations.ApiParam;

@Entity
@Table(name = "hse_problem")
@Inheritance(strategy = InheritanceType.JOINED)
public class HseProblem extends BasicForm implements Serializable {
	private static final long serialVersionUID = 900105837436361256L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	/**
	 * 问题创建者
	 */
	@Column(name="create_user")
    private Integer createUser;
	
	/**
	 * 标题
	 */
	@Column(name="problem_title")
	private String problemTitle;

	/**
	 * 机组
	 */
	@Column(name="unit")
    private String unit;

	/**
	 * 厂房
	 */
	@Column(name="wrokshop")
    private String wrokshop;

	/**
	 * 标高
	 */
	@Column(name="eleration")
    private String eleration;

	/**
	 * 房间号
	 */
	@Column(name="roomno")
    private String roomno;

	/**
	 * 问题描述
	 */
	@Column(name="description")
    private String description;

	/**
	 * 创建时间
	 */
	@Column(name="create_date")
    private Date createDate;

	/**
	 * 问题状态
	 */
	@Column(name="problem_status")
    private String problemStatus;

	/**
	 * 责任部门
	 */
	@Column(name="responsible_dept")
    private Integer responsibleDept;
	
	/**
	 * 责任班组
	 */
	@Column(name="responsible_team")
	private Integer responsibleTeam;
	
	/**
	 * 整改期限
	 */
	@Column(name="target_date")
    private Date targetDate;
	
	/**
	 * 结束日期
	 */
	@Column(name="finish_date")
    private Date finishDate;
	
	/**
	 * 处理步骤
	 */
	@Column(name="problem_step")
	private String problemStep;
	
	/**
	 * 一级编码id
	 */
	@Column(name="code1id")
	private String code1Id;

	/**
	 * 二级编码
	 */
	@Column(name="code2id")
	private String code2Id;
	
	/**
	 * 三级编码
	 */
	@Column(name="code3id")
	private String code3Id;
	/**
	 * 二级编码
	 */
	@Column(name="code2desc")
	private String code2Desc;
	
	/**
	 * 三级编码
	 */
	@Column(name="code3desc")
	private String code3Desc;
	
/*
 * ２０１８１００４
 */
	/**
	 * 检查类型
	 */
	@Column(name="checkType")
	String checkType;
	/**
	 * 隐患严重性类别
	 */
	@Column(name="criticalLevel")
	String criticalLevel;

	/**
	 *整改要求
	 */
	@Column(name="requirement")
	String requirement;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public String getProblemTitle() {
		return problemTitle;
	}

	public void setProblemTitle(String problemTitle) {
		this.problemTitle = problemTitle;
	}

	public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public String getWrokshop() {
        return wrokshop;
    }

    public void setWrokshop(String wrokshop) {
        this.wrokshop = wrokshop == null ? null : wrokshop.trim();
    }

    public String getEleration() {
        return eleration;
    }

    public void setEleration(String eleration) {
        this.eleration = eleration == null ? null : eleration.trim();
    }

    public String getRoomno() {
        return roomno;
    }

    public void setRoomno(String roomno) {
        this.roomno = roomno == null ? null : roomno.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getProblemStatus() {
        return problemStatus;
    }

    public void setProblemStatus(String problemStatus) {
        this.problemStatus = problemStatus == null ? null : problemStatus.trim();
    }

    public Integer getResponsibleDept() {
        return responsibleDept;
    }

    public void setResponsibleDept(Integer responsibleDept) {
        this.responsibleDept = responsibleDept;
    }

	public Integer getResponsibleTeam() {
		return responsibleTeam;
	}

	public void setResponsibleTeam(Integer responsibleTeam) {
		this.responsibleTeam = responsibleTeam;
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getProblemStep() {
		return problemStep;
	}

	public void setProblemStep(String problemStep) {
		this.problemStep = problemStep;
	}

	public String getCode1Id() {
		return code1Id;
	}

	public void setCode1Id(String code1Id) {
		this.code1Id = code1Id;
	}

	public String getCode2Id() {
		return code2Id;
	}

	public void setCode2Id(String code2Id) {
		this.code2Id = code2Id;
	}

	public String getCode3Id() {
		return code3Id;
	}

	public void setCode3Id(String code3Id) {
		this.code3Id = code3Id;
	}

	public String getCode2Desc() {
		return code2Desc;
	}

	public void setCode2Desc(String code2Desc) {
		this.code2Desc = code2Desc;
	}

	public String getCode3Desc() {
		return code3Desc;
	}

	public void setCode3Desc(String code3Desc) {
		this.code3Desc = code3Desc;
	}

	public String getCheckType() {
		return checkType;
	}

	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}

	public String getCriticalLevel() {
		return criticalLevel;
	}

	public void setCriticalLevel(String criticalLevel) {
		this.criticalLevel = criticalLevel;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

    
}