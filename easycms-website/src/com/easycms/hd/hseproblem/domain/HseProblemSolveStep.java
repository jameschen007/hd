package com.easycms.hd.hseproblem.domain;

public enum HseProblemSolveStep {
	
	/**
	 * HSE确认当前问题
	 */
	hseConfirm,
	
	/**
	 * HSE审核问题
	 */
	hseCheck,
	
	/**
	 * HSE审核问题
	 */
	hseCheckAgain,
	
	/**
	 * 责任班组整改
	 */
	teamSolve,

	/**
	 * 编制A审核问题
	 */
	aCheck,
	/**
	 * 责任班组重新整改
	 */
	teamSolveAgain

}
