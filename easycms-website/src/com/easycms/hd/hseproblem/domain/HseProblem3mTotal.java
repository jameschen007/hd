package com.easycms.hd.hseproblem.domain;

import com.easycms.core.form.BasicForm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 仅连续三个月(90天)内的安全hse_problem　数据记录,所有记录，其中包括待整改数
 */
@Entity
@Table(name = "hse_problem_3m_total")
@PrimaryKeyJoinColumn(name="id")
public class HseProblem3mTotal extends HseProblem implements Serializable {
	private static final long serialVersionUID = 900105837436361256L;

}