package com.easycms.hd.hseproblem.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hse_problem_solve")
public class HseProblemSolve implements Serializable {
	private static final long serialVersionUID = -4061846403745785135L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	/**
	 * 问题ID
	 */
	@Column(name="problem_id")
    private Integer problemId;

	/**
	 * 处理人
	 */
	@Column(name="solve_user")
    private Integer solveUser;
	
	/**
	 * 当前步骤
	 */
	@Column(name="solve_step")
    private String solveStep;

	/**
	 * 处理状态
	 */
	@Column(name="solve_status")
    private String solveStatus;

	/**
	 * 处理时间
	 */
	@Column(name="solve_date")
    private Date solveDate;

	/**
	 * 描述
	 */
	@Column(name="solve_description")
    private String solveDescription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public Integer getSolveUser() {
        return solveUser;
    }

    public void setSolveUser(Integer solveUser) {
        this.solveUser = solveUser;
    }

	public String getSolveStep() {
		return solveStep;
	}

	public void setSolveStep(String solveStep) {
		this.solveStep = solveStep;
	}

	public String getSolveStatus() {
        return solveStatus;
    }

    public void setSolveStatus(String solveStatus) {
        this.solveStatus = solveStatus == null ? null : solveStatus.trim();
    }

    public Date getSolveDate() {
        return solveDate;
    }

    public void setSolveDate(Date solveDate) {
        this.solveDate = solveDate;
    }

    public String getSolveDescription() {
        return solveDescription;
    }

    public void setSolveDescription(String solveDescription) {
        this.solveDescription = solveDescription == null ? null : solveDescription.trim();
    }
}