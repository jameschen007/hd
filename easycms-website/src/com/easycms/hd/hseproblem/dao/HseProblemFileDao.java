package com.easycms.hd.hseproblem.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.hseproblem.domain.HseProblemFile;

public interface HseProblemFileDao extends Repository<HseProblemFile,Integer>,BasicDao<HseProblemFile> {

	@Query("From HseProblemFile hpf where hpf.problemId = ?1")
	List<HseProblemFile> findByHseProblemId(Integer id);
	
	@Query("From HseProblemFile hpf where hpf.problemId = ?1 and hpf.filetype= ?2")
	List<HseProblemFile> findByHseProblemIdAndFileType(Integer id, String type);
}
