package com.easycms.hd.hseproblem.dao;

import com.easycms.basic.BasicDao;
import com.easycms.hd.hseproblem.domain.HseProblem3mDelay;
import com.easycms.hd.hseproblem.domain.HseProblem3mTotal;
import org.springframework.data.repository.Repository;

public interface HseProblem3mTotalDao extends Repository<HseProblem3mTotal, Integer>, BasicDao<HseProblem3mTotal> {

}
