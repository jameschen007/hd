package com.easycms.hd.hseproblem.mybatis.dao;

import java.util.List;

import com.easycms.hd.hseproblem.domain.HseProblemDetils;

public interface HseProblemMybatisDao {
	Integer getCount();
	Integer getCountByStatus(String problemStatus);
	List<HseProblemDetils> findAll(Integer pageNumber,Integer pageSize);
	List<HseProblemDetils> findByStatus(String problemStatus,Integer pageNumber,Integer pageSize);
}
