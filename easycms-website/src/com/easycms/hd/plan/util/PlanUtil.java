package com.easycms.hd.plan.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;

public class PlanUtil {
	public static int personAmount(List<RollingPlan> rps, String type, String id) {
		int amount = 0;

		if (type.equals("consteam")) {
			for (RollingPlan rp : rps) {
				if (null != rp.getConsteam() && rp.getConsteam().equals(id)) {
					amount++;
				}
			}
		} else if (type.equals("consendman")) {
			for (RollingPlan rp : rps) {
				if (null != rp.getConsendman() && rp.getConsendman().equals(id)) {
					amount++;
				}
			}
		}

		return amount;
	}

	@SuppressWarnings("unchecked")
	public static WorkStep lastOneMark(WorkStep workStep) {
		List<WorkStep> workStepList = workStep.getRollingPlan().getWorkSteps();
		if (null != workStepList && workStepList.size() > 0) {
			Collections.sort(workStepList);
			WorkStep lastOne = workStepList.get(workStepList.size() - 1);

			if (workStep.getId() == lastOne.getId()) {
				workStep.setLastone(true);
			}
		}
		return workStep;
	}
	
	@SuppressWarnings("unchecked")
	public static void lastOneMark(List<WorkStep> workStepList) {
		if (null != workStepList && workStepList.size() > 0) {
//			ComparatorWorkStep comparator = new ComparatorWorkStep();
//			Collections.sort(workStepList, comparator);
//			WorkStep lastOne = workStepList.get(workStepList.size() - 1);
//			lastOne.setLastone(true);
			Collections.sort(workStepList); 
			WorkStep lastOne = workStepList.get(workStepList.size() - 1);
			lastOne.setLastone(true);
//			 System.out.println("=======================");
//			 workStepList.forEach(b-> System.out.println(b.getStepIdentifier()));
		
		}
	}
	

	public static boolean lastOneWorkStepWitness(WorkStepWitness workStepWitness) {
		List<Integer> successSymbol = new ArrayList<Integer>();
		successSymbol.add(2);
		successSymbol.add(3);
		WorkStep workStep = workStepWitness.getWorkStep();
		
		if (null != workStep) {
			RollingPlan rollingPlan = workStep.getRollingPlan();
			if (null != rollingPlan) {
				List<WorkStep> workSteps = rollingPlan.getWorkSteps();
				if (null != workSteps && !workSteps.isEmpty()) {
					long wsCount = (long)workSteps.size();
					long doneCount = workSteps.stream().filter(ws -> ws.getStepflag().equals(StepFlag.DONE)).count();
					if ((doneCount + 1L) == wsCount) {
						long noticePointCount = 0L;
						if (CommonUtility.isNonEmpty(workStep.getNoticeaqc1())) {
							noticePointCount++;
						}
						if (CommonUtility.isNonEmpty(workStep.getNoticeaqc2())) {
							noticePointCount++;
						}
						if (CommonUtility.isNonEmpty(workStep.getNoticeczecqa())) {
							noticePointCount++;
						}
						if (CommonUtility.isNonEmpty(workStep.getNoticeczecqc())) {
							noticePointCount++;
						}
						if (CommonUtility.isNonEmpty(workStep.getNoticepaec())) {
							noticePointCount++;
						}
						
						long successCount = workStep.getWorkStepWitnesses().stream().filter(wsw -> (successSymbol.contains(wsw.getIsok()) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()))).count();
						
						if ((successCount + 1L) == noticePointCount) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static boolean lastOneWorkStepWitnessQC1(WorkStepWitness workStepWitness) {
		if (workStepWitness.getNoticePoint().equals(NoticePointType.QC1.name())) {
			List<Integer> successSymbol = new ArrayList<Integer>();
			successSymbol.add(2);
			successSymbol.add(3);
			WorkStep workStep = workStepWitness.getWorkStep();
			
			if (null != workStep) {
				RollingPlan rollingPlan = workStep.getRollingPlan();
				if (null != rollingPlan) {
					List<WorkStep> workSteps = rollingPlan.getWorkSteps();
					if (null != workSteps && !workSteps.isEmpty()) {
						long wsQC1Count = workSteps.stream().filter(ws -> CommonUtility.isNonEmpty(ws.getNoticeaqc1())).count();
						long doneQC1Count = workSteps.stream().filter(ws -> (ws.getStepflag().equals(StepFlag.DONE) && CommonUtility.isNonEmpty(ws.getNoticeaqc1()))).count();
						if ((doneQC1Count + 1L) == wsQC1Count) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static String[] arrContrast(String[] arr1, String[] arr2) {
		List<String> list = new LinkedList<String>();
		for (String str : arr1) { 
			if (!list.contains(str)) {
				list.add(str);
			}
		}
		for (String str : arr2) {
			if (list.contains(str)) {
				list.remove(str);
			}
		}
		String[] result = {}; 
		return list.toArray(result);
	}
	
	public static Integer[] arrContrast(Integer[] arr1, Integer[] arr2) {
		List<Integer> list = new LinkedList<Integer>();
		for (Integer str : arr1) { 
			if (!list.contains(str)) {
				list.add(str);
			}
		}
		for (Integer str : arr2) {
			if (list.contains(str)) {
				list.remove(str);
			}
		}
		Integer[] result = {}; 
		return list.toArray(result);
	}
}
