//package com.easycms.hd.plan.action;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import com.easycms.common.util.CommonUtility;
//import com.easycms.common.util.Messenger;
//import com.easycms.core.util.ForwardUtility;
//import com.easycms.core.util.HttpUtility;
//import com.easycms.hd.plan.form.RollingPlanForm;
//import com.easycms.oracle.hd.plan.service.RollingPlanOracleService;
//
//@Controller
//@RequestMapping("/baseservice/rollingplan/original")
//public class RollingPlanOracleController {
//	/**
//	 * Logger for this class
//	 */
//	private static final Logger logger = Logger.getLogger(RollingPlanOracleController.class);
//
//	@Autowired
//	private RollingPlanOracleService rollingPlanOracleService;
//
//	@RequestMapping(value = "", method = RequestMethod.GET)
//	public String settings(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		return ForwardUtility.forwardAdminView("/hdService/rollingPlan/list_rollingPlan_original");
//	}
//
//	/**
//	 * 数据片段
//	 * 
//	 * @param request
//	 * @param response
//	 * @param pageNum
//	 * @return
//	 */
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	public String dataList(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute("form") RollingPlanForm form) {
//		form.setSearchValue(request.getParameter("search[value]"));
//		logger.debug("==> Show rollingPlan data list.");
//		form.setFilter(CommonUtility.toJson(form));
//		String returnString = ForwardUtility
//				.forwardAdminView("/hdService/rollingPlan/data/data_json_rollingPlan_original");
//		return returnString;
//	}
//
//	/**
//	 * 批量导入
//	 * 
//	 * @param id
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@RequestMapping(value = "/import")
//	public void batchImport(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute("form") RollingPlanForm form) {
//		logger.debug("==> Start import rollingPlan.");
//
//		String[] sIds = form.getsIds();
//		logger.debug("==> To import rollingPlan [" + CommonUtility.toJson(sIds) + "]");
//		if (sIds != null && sIds.length > 0) {
//			Messenger msg = rollingPlanOracleService.importToLocal(sIds);
//			if (msg.isContainError()){
//				HttpUtility.writeToClient(response, "false");
//				return;
//			}
//		}
//		HttpUtility.writeToClient(response, "true");
//		logger.debug("==> End import rollingPlan.");
//	}
//}
