package com.easycms.hd.plan.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.form.WorkStepForm;
import com.easycms.hd.plan.form.validator.WorkStepFormValidator;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;

@Controller
@RequestMapping("/baseservice/rollingplan/workstep")
public class WorkStepController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WorkStepController.class);
	
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	@Qualifier("workStepFormValidator")
	private org.springframework.validation.Validator validator;
	@Autowired
	private PrivilegeService privilegeService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response){
		String params = request.getParameter("autoidup");
		String[] datas =	params.split("[?]");
		request.setAttribute("autoidup",datas[0]);
		return ForwardUtility.forwardAdminView("/hdService/workstep/list_workStep");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") WorkStepForm form) {
		logger.debug("==> Show workStep data list.");
		String params = request.getParameter("_autoidup");
			String[] datas =	params.split("[?]");
		Integer autoidup = Integer.parseInt(datas[0]);
		
		form.setAutoidup(autoidup);
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/workstep/data/data_json_workStep");
		
		return returnString;
	}

	/**
	 * 新增单价信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		String params = request.getParameter("autoidup");
		String[] datas =	params.split("[?]");
		request.setAttribute("autoidup", datas[0]);
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/workstep/modal_workStep_add");
		return returnString;
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("workStepForm") @Valid WorkStepForm workStepForm,
			BindingResult errors) {
		logger.debug("==> Start save new workStep.");

		// 校验新增表单信息
		WorkStepFormValidator validator = new WorkStepFormValidator();
		validator.validate(workStepForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			logger.debug("==> End save new work step.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		RollingPlan rollingPlan = new RollingPlan();
		rollingPlan.setId(workStepForm.getAutoidup());
		
		// 校验通过，保存新增
		WorkStep workStep = new WorkStep();
		workStep.setRollingPlan(rollingPlan);
		workStep.setStepno(workStepForm.getStepno());
		workStep.setStepname(workStepForm.getStepname());
		workStep.setOperater(workStepForm.getOperater());
		workStep.setNoticeaqa(workStepForm.getNoticeaqa());
		workStep.setWitnesseraqa(workStepForm.getWitnesseraqa());
		workStep.setWitnessdateaqa(workStepForm.getWitnessdateaqa());
		
		workStep.setNoticeaqc1(workStepForm.getNoticeaqc1());
		workStep.setWitnesseraqc1(workStepForm.getWitnesseraqc1());
		workStep.setWitnessdateaqc1(workStepForm.getWitnessdateaqc1());
		
		workStep.setNoticeaqc2(workStepForm.getNoticeaqc2());
		workStep.setWitnesseraqc2(workStepForm.getWitnesseraqc2());
		workStep.setWitnessdateaqc2(workStepForm.getWitnessdateaqc2());
		
		
		workStep.setNoticeb(workStepForm.getNoticeb());
		workStep.setWitnesserb(workStepForm.getWitnesserb());
		workStep.setWitnessdateb(workStepForm.getWitnessdateb());
		workStep.setNoticec(workStepForm.getNoticec());
		workStep.setWitnesserc(workStepForm.getWitnesserc());
		workStep.setWitnessdatec(workStepForm.getWitnessdatec());
		workStep.setNoticed(workStepForm.getNoticed());
		workStep.setWitnesserd(workStepForm.getWitnesserd());
		workStep.setWitnessdated(workStepForm.getWitnessdated());
		workStep.setCreatedOn(new Date());
		workStep.setCreatedBy(loginUser.getName());
		workStep.setStepflag(StepFlag.UNDO);
		
		if (!CommonUtility.isNonEmpty(workStep.getNoticeaqa()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeaqc1()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeaqc2()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeb()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticec()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticed())){
//			workStep.setStepflag(StepFlag.SKIP);
		}
		
		logger.debug("save work step info ==> [" + workStep.toString() +"]");
		workStepService.add(workStep);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save new workStep.");
		return;
	}


	/**
	 * 修改用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") WorkStepForm form) {
		return ForwardUtility.forwardAdminView("/hdService/workstep/modal_workStep_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") WorkStepForm workStepForm) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start save eidt workStep.");
		}

		// #########################################################
		// 校验提交信息
		// #########################################################
		if (workStepForm.getId() == null) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save eidt workStep failed.");
			logger.debug("==> End save eidt workStep.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		// #########################################################
		// 校验通过
		// #########################################################
		Integer id = workStepForm.getId();
		WorkStep workStep = workStepService.findById(id);
		
		
		
		// 校验通过，保存更新
        workStep.setStepno(workStepForm.getStepno());
        workStep.setStepname(workStepForm.getStepname());
        workStep.setNoticeaqa(workStepForm.getNoticeaqa());
        workStep.setNoticeaqc1(workStepForm.getNoticeaqc1());
        workStep.setNoticeaqc2(workStepForm.getNoticeaqc2());
        
        workStep.setNoticeb(workStepForm.getNoticeb());
        workStep.setNoticec(workStepForm.getNoticec());
        workStep.setNoticed(workStepForm.getNoticed());
		
		workStep.setUpdatedBy(loginUser.getName());
		workStep.setUpdatedOn(new Date());
		
		if (!CommonUtility.isNonEmpty(workStep.getNoticeaqa()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeaqc1()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeaqc2()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticeb()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticec()) && 
				!CommonUtility.isNonEmpty(workStep.getNoticed())){
//			workStep.setStepflag(StepFlag.SKIP);
		}
		
		workStepService.add(workStep);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save eidt workStep.");
		return;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") WorkStepForm form) {
		logger.debug("==> Start delete workStep.");

		Integer[] ids = form.getIds();
		logger.debug("==> To delete workStep [" + CommonUtility.toJson(ids) + "]");
		if (ids != null && ids.length > 0) {
			workStepService.delete(ids);
		}
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete workStep.");
	}

	
	/**
	 * 导入工序UI
	 * 
	 */
	@RequestMapping(value = "/import",method = RequestMethod.GET)
	public String workStepImportUI(HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("==> Start import  workStep UI");
		return ForwardUtility.forwardAdminView("/hdService/workstep/modal_workStep_import");
	}
}
