package com.easycms.hd.plan.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.hd.plan.form.RollingPlanForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Component
public class RollingPlanFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RollingPlanForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "qualityplanno",
		"form.qualityplanno.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "drawno",
				"form.drawno.empty");
	}
}
