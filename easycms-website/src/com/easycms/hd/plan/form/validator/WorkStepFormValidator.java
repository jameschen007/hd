package com.easycms.hd.plan.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.hd.plan.form.WorkStepForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Component("workStepFormValidator")
public class WorkStepFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(WorkStepForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stepno",
		"form.stepno.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stepname",
				"form.stepname.empty");
	}
}
