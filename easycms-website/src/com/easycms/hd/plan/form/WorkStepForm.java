package com.easycms.hd.plan.form;


import java.util.Date;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.domain.RollingPlan;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepForm extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = -1444429527520023227L;
	
	private String drawno;
	private	String weldno;
	private Integer id;
	private Integer autoidup;
	private Integer stepno;
	private String stepname;
	private String operater;
	private Date operatedate;
	private String operatedesc;
	private String noticeainfo;
	private String noticeresult;
	private String noticeresultdesc;
	private String noticeaqc1;
	private String witnesseraqc1;
	private Date witnessdateaqc1;
	private String noticeaqc2;
	private String witnesseraqc2;
	private Date witnessdateaqc2;
	private String noticeaqa;
	private String witnesseraqa;
	private Date witnessdateaqa;
	private String noticeb;
	private String witnesserb;
	private Date witnessdateb;
	private String noticec;
	private String witnesserc;
	private Date witnessdatec;
	private String noticed;
	private String witnesserd;
	private Date witnessdated;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	private Integer qcsign;
	private Integer witness;
	private String qcman;
	private Date qcdate;
	private String dosage;
	
	private RollingPlan rollingPlan;
}