package com.easycms.hd.plan.form;

import java.util.Date;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RollingPlanForm extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = -3107106268846750247L;
	private Integer id;
	private Integer sequenceno;
	private String qualityplanno;
	private String weldlistno;
	private String drawno;
	private String rccm;
	private String areano;
	private String unitno;
	private String weldno;
	private String speciality;
	private String materialtype;
	private Double workpoint;
	private Double qualitynum;
	private Double worktime;
	private String consteam;
	private String plandate;
	private Date consdate;
	private Integer isend;
	private String consendman;
	private String welder;
	private Date welddate;
	private Date enddate;
	private Integer qcsign;
	private String qcman;
	private Date qcdate;
	private String remark;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String technologyAsk;
	private String qualityRiskCtl;
	private String securityRiskCtl;
	private String experienceFeedback;
	private String workTool;
	private String type;
	private String dosage;
	
	private boolean ignore = false;
	
	private String searchValue;

	@Override
	public String toString() {
		return "RollingPlanForm [id=" + id + ", sequenceno=" + sequenceno
				+ ", qualityplanno=" + qualityplanno + ", weldlistno="
				+ weldlistno + ", drawno=" + drawno + ", rccm=" + rccm
				+ ", areano=" + areano + ", unitno=" + unitno + ", weldno="
				+ weldno + ", speciality=" + speciality + ", materialtype="
				+ materialtype + ", workpoint=" + workpoint + ", qualitynum="
				+ qualitynum + ", consteam=" + consteam + ", plandate="
				+ plandate + ", consdate=" + consdate + ", isend=" + isend
				+ ", consendman=" + consendman + ", welder=" + welder
				+ ", enddate=" + enddate + ", qcsign=" + qcsign + ", qcman="
				+ qcman + ", qcdate=" + qcdate + ", remark=" + remark
				+ ", createdOn=" + createdOn + ", createdBy=" + createdBy
				+ ", updatedOn=" + updatedOn + ", updatedBy=" + updatedBy + "]";
	}
}