package com.easycms.hd.plan.mybatis.dao;

import com.easycms.kpi.underlying.domain.KpiPendingSheet;

import java.util.List;

public interface KpiPendingSheetMybatisDao {
    List<KpiPendingSheet> findPageBySql(KpiPendingSheet bean);

    Integer findPageBySqlCount(KpiPendingSheet bean);
}
