package com.easycms.hd.plan.mybatis.dao;

import java.util.List;

import com.easycms.hd.plan.domain.WorkStepWitness;

public interface WitnessMybatisDao {
	List<WorkStepWitness> findQC2MemberUnCompleteWitnessByPage(WorkStepWitness wsw);
	Integer findQC2MemberUnCompleteWitnessCount(WorkStepWitness wsw);
	List<WorkStepWitness> findQC2MemberUnQulifiedWitnessByPage(WorkStepWitness wsw);
	Integer findQC2MemberUnQulifiedWitnessCount(WorkStepWitness wsw);
	List<WorkStepWitness> findQC2MemberQulifiedWitnessByPage(WorkStepWitness wsw);
	Integer findQC2MemberQulifiedWitnessCount(WorkStepWitness wsw);
//------------------[QC2组长查看处理与未处理的列表]-----------------------	
	List<WorkStepWitness> findQC2TeamUnHandledWitnessByPage(WorkStepWitness wsw);
	Integer findQC2TeamUnHandledWitnessCount(WorkStepWitness wsw);
	
	List<WorkStepWitness> findQC2TeamHandledWitnessByPage(WorkStepWitness wsw);
	Integer findQC2TeamHandledWitnessCount(WorkStepWitness wsw);
}
