package com.easycms.hd.plan.mybatis.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.easycms.hd.plan.mybatis.dao.bean.StatisticsBean;

public interface RollingQuestionMybatisDao {
	//当前登录人是队长，查看班长下的问题统计 :已解决的问题数量、未解决的问题数量 
	List<StatisticsBean> problemStatisticsForCaptain(Map<String,Object> inPara);
	StatisticsBean problemStatisticsForTeam(Map<String,Object> inPara);

	public Integer getStatistics(@Param("status")String status,@Param("userId")Integer userId,@Param("type")String type);
	/**@Param("status")String status,@Param("userId")Integer userId,@Param("type")String type*/
	public Integer getStatisticsByMap(Map<String,Object> parm);
}
