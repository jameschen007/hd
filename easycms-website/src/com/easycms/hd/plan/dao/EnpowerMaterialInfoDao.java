package com.easycms.hd.plan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.EnpowerMaterialInfo;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface EnpowerMaterialInfoDao extends Repository<EnpowerMaterialInfo,Integer>,BasicDao<EnpowerMaterialInfo> {

	@Query("from EnpowerMaterialInfo u where u.rollingPlanId=?1 and isDel=0")
	List<EnpowerMaterialInfo> findByRollingPlanId(Integer rollingPlanId);
}
