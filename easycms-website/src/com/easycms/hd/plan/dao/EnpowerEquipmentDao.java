package com.easycms.hd.plan.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.EnpowerEquipment;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface EnpowerEquipmentDao extends Repository<EnpowerEquipment,Integer>,BasicDao<EnpowerEquipment> {

	@Query("from EnpowerEquipment u where u.rollingPlanId=?1")
	EnpowerEquipment findByRollingPlanId(Integer rollingPlanId);
}
