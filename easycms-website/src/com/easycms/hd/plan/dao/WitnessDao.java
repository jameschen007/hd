package com.easycms.hd.plan.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.WorkStepWitness;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface WitnessDao extends Repository<WorkStepWitness,Integer>,BasicDao<WorkStepWitness>{
	//有值　和　空：　有判断空则不管，　有判断值也不管　　无判断的添加
	String andwitnessflag2null = " and ((witnessflag is not null and witnessflag!='EXPIRED') or witnessflag is null )";
	
	@Modifying
	@Query("UPDATE WorkStepWitness w SET w.witness=?1,w.witnessdes=?2,w.witnessaddress=?3, w.witnessdate=?4 WHERE w.id=?5")
	int update(Integer witness,String witnessdes,String witnessaddress,Date witnessdate, Integer id);
	
	@Modifying
	@Query("DELETE FROM WorkStepWitness wsw WHERE wsw.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Modifying
	@Query("DELETE FROM WorkStepWitness wsw WHERE wsw.id=?1")
	int delete(Integer id);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.id IN (?1)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> getByIds(Integer[] ids);

	@Modifying
	@Query("DELETE FROM WorkStepWitness wsw WHERE wsw.workStep.id=?1")
	int deleteByWorkStep(Integer id);
//	不必在原表中存在　　witnessflag= 'EXPIRED'
//	@Modifying
//	@Query("UPDATE WorkStepWitness wsw SET wsw.witnessflag= 'EXPIRED' WHERE wsw.workStep.id=?1")
//	int expireByWorkStep___(Integer id);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.workStep.id=?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> getByWorkStepId(Integer id);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.workStep.id=?1 and wsw.witness is not null"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> getParentByWorkStepId(Integer id);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.workStep.id=?1 and wsw.witness is null"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> getChilrenByWorkStepId(Integer id);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByWitness(String[] ids);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByWitnesserId(String witnesserId);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok =?2 and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult  = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByWitnesserAndIsok(String witnesserId, Integer isok);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok =?2 and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult  = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByWitnesserAndIsok(String witnesserId, Integer isok, Pageable pageable);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok in ?2 and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult  = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByPageWitnesserAndIsok(Pageable pageable, String witnesserId, Integer[] isok);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok in ?2 and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult  = '')"+andwitnessflag2null+" and (wsw.workStep.rollingPlan.weldno like ?3) ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByPageWitnesserAndIsok(Pageable pageable, String witnesserId, Integer[] isok, String keyword);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok !=?2))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByWitnesserAndIsokNot(String witnesserId, Integer isok);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok !=?2))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByWitnesserAndIsokNot(Pageable pageable, String witnesserId, Integer isok);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok !=?2)) and (wsw.workStep.rollingPlan.weldno like ?3)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByWitnesserAndIsokNot(Pageable pageable, String witnesserId, Integer isok, String keyword);
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witness is null"+andwitnessflag2null+"")
//--------------------------------------Witness-------------------------------------------	
	Page<WorkStepWitness> findDistinctByWitnessIsNull(Pageable pageable);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witness is null and (wsw.workStep.rollingPlan.weldno like ?1)"+andwitnessflag2null+"")
	Page<WorkStepWitness> findDistinctByWitnessIsNull(Pageable pageable, String keyword);

	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witness is not null"+andwitnessflag2null+"")
	Page<WorkStepWitness> findDistinctByWitnessIsNotNull(Pageable pageable);
	
	@Query("SELECT distinct wsw FROM WorkStepWitness wsw WHERE wsw.witness is not null and (wsw.workStep.rollingPlan.weldno like ?1)"+andwitnessflag2null+"")	
	Page<WorkStepWitness> findDistinctByWitnessIsNotNull(Pageable pageable, String keyword);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and wsw.witness in ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(List<String> witness, Pageable pageable);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and wsw.witness in ?1 and (wsw.workStep.rollingPlan.weldno like ?2)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(List<String> witness, String keyword, Pageable pageable);

	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty"+andwitnessflag2null+"")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNull(Pageable pageable);

	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and witness is not null"+andwitnessflag2null+"")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIsNotNull(Pageable pageable);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and (wsw.workStep.rollingPlan.weldno like ?1)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNull(String keyword, Pageable pageable);

	@Query("select distinct wsw from WorkStepWitness wsw where wsw.children is empty"+andwitnessflag2null+"")
	Page<WorkStepWitness> findDistinctByChildrenIsNull(Pageable pageable);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByWitnessIn(List<String> witness, Pageable pageable);
	
	//这一行是包含图纸，焊口和地点的，要改成只有焊口，待他们返回的时候，可以从这里改回去。
//	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1 and (wsw.workStep.rollingPlan.drawno like ?2 or wsw.workStep.rollingPlan.weldno like ?2 or wsw.witnessaddress like ?2) ORDER BY wsw.createdOn desc")
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1 and (wsw.workStep.rollingPlan.weldno like ?2)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByWitnessIn(List<String> witness, String keyword, Pageable pageable);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNullAndWitnessIn(List<String> witness);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNullAndWitnessIn(List<String> witness, Pageable pageable);
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNull();
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.witness is not null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNullAndWitnessIsNotNull();
	
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNull(Pageable pageable);

	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and wsw.witness in (?1)"+andwitnessflag2null+"")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIn(List<String> witness);

	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty"+andwitnessflag2null+"")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNull();
	@Query("select distinct wsw from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and wsw.witness is not null"+andwitnessflag2null+"")
	List<WorkStepWitness> findDistinctByParentIsNullAndChildrenIsNotNullAndWitnessIsNotNull();
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByCompleteWitness(List<String> witness);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByCompleteWitness(List<String> witness, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '') and (wsw.workStep.rollingPlan.weldno like ?2)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByCompleteWitness(List<String> witness, String keyword, Pageable pageable);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.parent IS NULL and wsw.witness is not null and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByComplete();
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.isok != 0 ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByComplete_();
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.parent IS NULL and wsw.witness is not null and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByComplete(Pageable pageable);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByUnCompleteWitness(List<String> witness);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByUnCompleteWitness(List<String> witness, Pageable pageable);
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.parent IS NULL and wsw.witness is not null and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByUnComplete();
	
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE (wsw.noticeresultdesc is NULL or wsw.noticeresultdesc = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByUnComplete_();
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.parent IS NULL and wsw.witness is not null and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByUnComplete(Pageable pageable);
	
	//我发起的见证
	@Modifying
	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.witness IS NOT NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findMyLaunchWitness(String userId);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.witness IS NOT NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.witness IS NOT NULL and (wsw.workStep.rollingPlan.weldno like ?2)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE witness not in (?1)")
	Page<WorkStepWitness> findByWitnessNotIn(List<String> witness, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness in (?1) and (wsw.workStep.rollingPlan.weldno like ?2)"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findByWitnessNotIn(List<String> witness, String keyword, Pageable pageable);
	
	//我收到的见证数量
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok in (0, 1) and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+"")
	int countdMyReciveWitness(String userId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.noticePoint = ?2 and wsw.isok in (0, 1) and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '')"+andwitnessflag2null+"")
	int countdMyReciveWitness(String userId, String categoryFilter);
	//待我分派的见证数量
	@Query("select count(DISTINCT wsw.id) from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1"+andwitnessflag2null+"")
	int countByWitnessToAssign(List<String> witness);
	
	@Query("select count(DISTINCT wsw.id) from WorkStepWitness wsw where wsw.parent is null and wsw.children is empty and wsw.noticePoint = ?2 and (wsw.workStep.noticeresult is NULL or wsw.workStep.noticeresult = '') and wsw.witness in ?1"+andwitnessflag2null+"")
	int countByWitnessToAssign(List<String> witness, String categoryFilter);
	
	//已分派的见证数量
	@Query("select count(DISTINCT wsw.id) from WorkStepWitness wsw where wsw.parent is null and wsw.children is not empty and wsw.witness in ?1"+andwitnessflag2null+"")
	int countMyWitnessAssigned(List<String> witness);
	
	//我发起的见证
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.witness IS NOT NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn")
	int countMyCreateWitness(String userId);
	
	//见证组长已完成的见证数量。
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countCompleteWitness(List<String> witness);
	
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE wsw.witness IN (?1) and wsw.noticePoint = ?2 and (wsw.workStep.noticeresult is not NULL and wsw.workStep.noticeresult != '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countCompleteWitness(List<String> witness, String categoryFilter);
	
	//普通见证员已完成的数量
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot(String witnesserId);
	
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'A_QC1') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_QC1(String witnesserId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'A_QC2') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_QC2(String witnesserId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'A_QA') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_A(String witnesserId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'B') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_B(String witnesserId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'C') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_C(String witnesserId);
	@Query("select count(DISTINCT wsw.id) FROM WorkStepWitness wsw WHERE (wsw.witnesser = ?1 and (wsw.noticePoint is not Null and wsw.noticePoint = 'D') and ((wsw.workStep.noticeresult is not NULL or wsw.workStep.noticeresult != '') or wsw.isok != 0 ))"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	int countWitnesserAndIsokNot_D(String witnesserId);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findAll_of_witnesser(String id);
	
	@Query("FROM WorkStepWitness wsw WHERE (wsw.isok != 0) and wsw.witnesser = ?1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByComplete_of_witnesser(String id);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and (wsw.noticeresultdesc is NULL or wsw.noticeresultdesc = '')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findByUnComplete_of_witnesser(String id);
	
//---------------------------------------------[2017-09-16为适应中核五公司而做的添加]---------------------------------------------------------------------------

	@Query("FROM WorkStepWitness wsw WHERE wsw.workStep.id=?1 and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1 and wsw.witnessflag is null"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> getActiveByWorkStepId(Integer id);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessmonitor = ?1 and wsw.witness IS NULL and wsw.witnessteam IS NULL and wsw.workStep.rollingPlan.type = ?2 and wsw.workStep.rollingPlan.isdel != 1 and wsw.noticePoint = ?3 and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag is null"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMonitorPendingWitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMyLaunchedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMemberPendingQC2WitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.workStep.id = ?3 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2', 'CZEC_QA', 'CZEC_QC', 'PAEC') and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	List<WorkStepWitness> findMemberPendingQC2WitnessAdvance(Integer userId, String rollingPlanType, Integer workStepId);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.workStep.id = ?3 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2') and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	WorkStepWitness findQC2SingleItem(Integer userId, String rollingPlanType, Integer workStepId);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.workStep.id = ?3 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1') and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	WorkStepWitness findQC1SingleItem(Integer userId, String rollingPlanType, Integer workStepId);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.workStep.id = ?3 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in (?4) and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMemberPendingQC2WitnessAdvanceByPage(Integer userId, String rollingPlanType, Integer workStepId, String noticePointType, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in (?3) and wsw.witnesser is null and wsw.witnessflag is null and wsw.isok=0 and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMemberUnassignQC2WitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Pageable pageable);
	
	@Modifying
	@Query("UPDATE WorkStepWitness wsw SET wsw.witnessteam=?2 WHERE wsw.id in (?1)")
	int assignToWitnessTeam(Integer[] ids, Integer teamId);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessteam = ?1 and wsw.witness is null and (wsw.workStep.rollingPlan.type = ?2) and wsw.workStep.rollingPlan.isdel != 1 and wsw.noticePoint in ('QC1', 'QC2')  AND wsw.witnessflag IS NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamUnassignWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);

	@Modifying
	@Query("UPDATE WorkStepWitness wsw SET wsw.witness=?3 WHERE wsw.id in (?1) and wsw.witnessteam=?2")
	int assignToWitnessMember(Integer[] ids, Integer teamId, Integer memberId);

	@Modifying
	@Query("UPDATE WorkStepWitness wsw SET wsw.witness=?1 WHERE wsw.workStep.id = ?2 and wsw.witnessflag is null and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC')")
	int assignToWitnessMemberQC2(Integer memberId, Integer workStepId);
	@Modifying
	@Query("UPDATE WorkStepWitness wsw SET witnessteam=?3 , wsw.witness=?1 WHERE wsw.workStep.id = ?2 and wsw.witnessflag is null and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC')")
	int assignToWbWitnessMemberFqQC1(Integer memberId, Integer workStepId,Integer witnessteam);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessteam = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag is null and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamUnCompleteWitnessByPage(Integer teamId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in (2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberQualifiedWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.id = ?1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	WorkStepWitness findByWitnessIdAndRollingPlanType(Integer witnessId, String rollingPlanType);

	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1 and wsw.witnessflag IS NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findTeamUnCompleteWitnessByPage(Integer teamId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1 and wsw.witnessflag IS NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberCompleteWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.workStep.rollingPlan.isdel != 1 and wsw.witnessflag IS NULL"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findTeamCompleteWitnessByPage(Integer teamId, String rollingPlanType, Pageable pageable);

//---------------------[2017-11-20]组长查看QC2的处理未处理列表-问题还很严重 已改用mybatis实现-----------------
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in(0, 1) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag IS NULL and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamUnHandledWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in(2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag IS NULL and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamHandledWitnessByPage(Integer memberId, String rollingPlanType, Pageable pageable);
//-------------------------------------------
	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessteam = ?1 and wsw.witness is not null and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2')"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamAssignedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);

	@Modifying
	@Query("UPDATE WorkStepWitness wsw SET wsw.witnesser=?3 WHERE wsw.id in (?1) and wsw.witness=?2 and wsw.witnesser is null and wsw.isok=0")
	int assignToWitnesserQC2(Integer[] ids, Integer qc2Id, Integer memberId);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok = 1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
//---------------------QC2得到列表-问题还很严重 已改用mybatis实现-----------------	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2', 'CZEC_QA', 'CZEC_QC', 'PAEC') AND wsw.witnessflag IS NULL and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" GROUP BY wsw.workStep.id ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2MemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
	
	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in (2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2', 'CZEC_QA', 'CZEC_QC', 'PAEC') AND wsw.witnessflag IS NULL and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" GROUP BY wsw.workStep.id ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2MemberQulifiedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC2', 'CZEC_QA', 'CZEC_QC', 'PAEC') AND wsw.witnessflag IS NULL and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" GROUP BY wsw.workStep.id ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2MemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
//-------------------------------------------
	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok in (2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') and wsw.workStep.rollingPlan.isdel != 1"+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPage(Integer userId, String rollingPlanType, Pageable pageable);
//----------------------[2017-10-30见证列表分页信息带全局搜索条件]--------------------
	String searchCondition = " and wsw.workStep.rollingPlan.isdel != 1 and (wsw.witnessaddress like ?3 or wsw.realwitnessaddress like ?3) ";
	String searchCondition4 = " and wsw.workStep.rollingPlan.isdel != 1 and (wsw.witnessaddress like ?4 or wsw.realwitnessaddress like ?4) ";

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag is null " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPageWithKeyword(Integer memberId, String rollingPlanType,
			String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') " +  searchCondition + ""+andwitnessflag2null+"  ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findTeamUnCompleteWitnessByPageWithKeyword(Integer teamId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok=0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC')  " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2')  AND wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+"  ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberCompleteWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.createdBy = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2')  AND wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+"  ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findTeamCompleteWitnessByPageWithKeyword(Integer teamId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok != 0 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC')  AND wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+"  ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessteam = ?1 and wsw.witness is null and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') AND wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamUnassignWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in (?3) and wsw.witnesser is null and wsw.witnessflag is null and wsw.isok=0 " +  searchCondition4 + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMemberUnassignQC2WitnessByPageWithKeyword(Integer userId, String rollingPlanType,
			String noticePointType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnessmonitor = ?1 and wsw.witness IS NULL and wsw.witnessteam IS NULL and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint = ?3 and wsw.noticePoint in ('QC1', 'QC2')  and wsw.witnessflag is null " +  searchCondition4 + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findMonitorPendingWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String noticePointType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok = 1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPageWithKeyword(Integer userId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok=1 and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPageWithKeyword(Integer memberId, String rollingPlanType, String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witnesser = ?1 and wsw.isok in (2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('CZEC_QA', 'CZEC_QC', 'PAEC') " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPageWithKeyword(Integer userId, String rollingPlanType,
			String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in (2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCMemberQualifiedWitnessByPageWithKeyword(Integer memberId, String rollingPlanType,
			String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in(0, 1) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamUnHandledWitnessByPageWithKeyworde(Integer memberId, String rollingPlanType,
			String keyword, Pageable pageable);

	@Query("FROM WorkStepWitness wsw WHERE wsw.witness = ?1 and wsw.isok in(2, 3) and (wsw.workStep.rollingPlan.type = ?2) and wsw.noticePoint in ('QC1', 'QC2') and wsw.witnessflag IS NULL " +  searchCondition + ""+andwitnessflag2null+" ORDER BY wsw.createdOn desc")
	Page<WorkStepWitness> findQCTeamHandledWitnessByPageWithKeyword(Integer memberId, String rollingPlanType,
			String keyword, Pageable pageable);
}
