package com.easycms.hd.plan.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.WorkStep;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface WorkStepDao extends Repository<WorkStep,Integer>,BasicDao<WorkStep> {
	@Query("FROM WorkStep ws WHERE ws.id IN (?1)")
	List<WorkStep> findByIds(Integer[] ids);
	@Modifying
	@Query("UPDATE Price p SET p.speciality=?1,p.pricetype=?2,p.unitprice=?3 WHERE p.id=?4")
	int update(String speciality,String pricetype,Integer integer,Integer id);
	
	@Modifying
	@Query("DELETE FROM WorkStep ws WHERE ws.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Modifying
	@Query("DELETE FROM WorkStep ws WHERE ws.rollingPlan.id IN (?1)")
	int deleteByIDUPIds(Integer[] ids);
	
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.id IN (?1)")
	List<WorkStep> findByRollingplanIds(Integer[] ids);
	
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.id = ?1")
	Page<WorkStep> findByPageWithRollingPlanId(Pageable pageable, Integer id);

	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 "
			+ " and ws.witnessflag is not null"
			+ " and ws.rollingPlan.witnessflag is not null"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	Page<WorkStep> findByPageTeamAll(String type, Integer teamId, Pageable pageable);
	
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag != 'DONE'"
			+ " and ws.witnessflag = 'LAUNCHED'"
			+ " and ws.rollingPlan.witnessflag = 'LAUNCHED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	Page<WorkStep> findByPageTeamUnCompleted(String type, Integer teamId, Pageable pageable);
	
	//见证工序：查询这条工序上的见证点，使用上面的方法，但不判断计划状态
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag != 'DONE'"
			+ " and ws.witnessflag = 'LAUNCHED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	Page<WorkStep> findByPageTeamUnCompletedNotPlan(String type, Integer teamId, Pageable pageable);
	
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag = 'DONE'"
			+ " and ws.witnessflag = 'COMPLETED'"
			+ " and ws.rollingPlan.witnessflag = 'COMPLETED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	Page<WorkStep> findByPageTeamComplete(String type, Integer teamId, Pageable pageable);
	
//	组长完成的见证工序：查询这条工序上的所有见证点都完成了
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag = 'DONE'"
			+ " and ws.witnessflag = 'COMPLETED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	Page<WorkStep> findByPageTeamCompleteNotPlanComplete(String type, Integer teamId, Pageable pageable);

//------------------------------------------------------[2017-10-30带搜索获取工序步骤]---------------------------------------------------------------	
	String searchCondition = " and (ws.stepname like ?3 or ws.itpName like ?3) ";
	
	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag = 'DONE'"
			+ " and ws.witnessflag = 'COMPLETED'"
			+ " and ws.rollingPlan.witnessflag = 'COMPLETED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ") " + searchCondition)
	Page<WorkStep> findByPageTeamCompleteWithKeyword(String type, Integer teamId, String keyword, Pageable pageable);

	@Query("FROM WorkStep ws WHERE ws.rollingPlan.type = ?1 and ws.rollingPlan.consteam = ?2 and ws.stepflag != 'DONE'"
			+ " and ws.witnessflag = 'LAUNCHED'"
			+ " and ws.rollingPlan.witnessflag = 'LAUNCHED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ") " + searchCondition)
	Page<WorkStep> findByPageTeamUnCompletedWithKeyword(String type, Integer teamId, String keyword, Pageable pageable);
	
/**
 * 查询如果不存在　不为空的，取消见证时，就回退计划状态*/
	@Query("FROM WorkStep ws WHERE ws.witnessflag is not null and ws.rollingPlan.id = ?1")
	List<WorkStep> findForCancelById(Integer rollingPlanId);
	
	
/**
 * 全部未完成的见证，用于修复应该完成却显示未完成的　都消点了，但管焊队队长点进去看，还在未完成界面
 * @param type
 * @param teamId
 * @param pageable
 * @return
 */
	@Query("FROM WorkStep ws WHERE ws.stepflag != 'DONE'"
			+ " and ws.witnessflag = 'LAUNCHED'"
			+ " and ws.rollingPlan.witnessflag = 'LAUNCHED'"
			+ " and ((ws.noticeaqc1 is not null and ws.noticeaqc1 != '')"
			+ " or (ws.noticeaqc2 is not null and ws.noticeaqc2 != '')"
			+ " or (ws.noticeczecqc is not null and ws.noticeczecqc != '')"
			+ " or (ws.noticeczecqa is not null and ws.noticeczecqa != '')"
			+ " or (ws.noticepaec is not null and ws.noticepaec != '')"
			+ ")")
	List<WorkStep> findByPageUnCompleted();
	
	
}
