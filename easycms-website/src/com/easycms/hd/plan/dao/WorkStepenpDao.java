package com.easycms.hd.plan.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.WorkStepenp;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface WorkStepenpDao extends Repository<WorkStepenp,Integer>,BasicDao<WorkStepenp> {
	@Modifying
	@Query("UPDATE Price p SET p.speciality=?1,p.pricetype=?2,p.unitprice=?3 WHERE p.id=?4")
	int update(String speciality,String pricetype,Integer integer,Integer id);
	
	@Modifying
	@Query("DELETE FROM WorkStepenp ws WHERE ws.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Modifying
	@Query("DELETE FROM WorkStepenp ws WHERE ws.rollingPlan.id IN (?1)")
	int deleteByIDUPIds(Integer[] ids);
	
	@Query("FROM WorkStepenp ws WHERE ws.rollingPlan.id IN (?1)")
	List<WorkStepenp> findByIDs(Integer[] ids);
	
	@Query("FROM WorkStepenp ws WHERE ws.rollingPlan.id = ?1")
	Page<WorkStepenp> findByPageWithRollingPlanId(Pageable pageable, Integer id);
}
