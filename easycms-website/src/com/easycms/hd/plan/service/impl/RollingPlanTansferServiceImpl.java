package com.easycms.hd.plan.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

@Service("rollingPlanTansferService")
public class RollingPlanTansferServiceImpl implements RollingPlanTansferService{
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	
	@Override
	public List<RollingPlan> transferRollingPlan(List<RollingPlan> rollingPlans){
		if (null != rollingPlans){
			for(RollingPlan rollingPlan : rollingPlans){
				rollingPlan = transferRollingPlan(rollingPlan);
			}
		}
		return rollingPlans;
	}

	@Override
	public RollingPlan transferRollingPlan(RollingPlan rollingPlan) {
		if (null != rollingPlan.getConsteam() && !"".equals(rollingPlan.getConsteam())){
			Department department = departmentService.findById(rollingPlan.getConsteam());
			
			if (null != department){
				rollingPlan.setConsteamName(department.getName());
			} else {
				rollingPlan.setConsteamName(null);
			}
		}
		
		if (null != rollingPlan.getConsendman() && !"".equals(rollingPlan.getConsendman())){
			User user = userService.findUserById(Integer.parseInt(rollingPlan.getConsendman()));
			
			if (null != user){
				rollingPlan.setConsendmanName(user.getRealname());
			} else {
				rollingPlan.setConsendmanName(null);
			}
		}
		
		List<WorkStep> wsList = rollingPlan.getWorkSteps();
		if (null != wsList && wsList.size() > 0){
			for (WorkStep ws : wsList){
				if (!ws.getStepflag().equals(StepFlag.UNDO) && !ws.getStepflag().equals(StepFlag.PREPARE)){
					rollingPlan.setStepStart(true);
					break;
				}
			}
		} 
		return rollingPlan;
	}
	
	@Override
	public RollingPlan markNoticePoint(RollingPlan rollingPlan){
		if (null != rollingPlan){
			List<WorkStep> workSteps = rollingPlan.getWorkSteps();
			Set<String> point = new HashSet<String>();
			
			for (WorkStep ws : workSteps){
				if (null == ws.getWorkStepWitnesses() || ws.getWorkStepWitnesses().size() == 0){
					if (CommonUtility.isNonEmpty(ws.getNoticeaqa())){
						point.add(ws.getNoticeaqa());
					}
					if (CommonUtility.isNonEmpty(ws.getNoticeaqc1())){
						point.add(ws.getNoticeaqc1());
					}
					if (CommonUtility.isNonEmpty(ws.getNoticeaqc2())){
						point.add(ws.getNoticeaqc2());
					}
					if (CommonUtility.isNonEmpty(ws.getNoticeb())){
						point.add(ws.getNoticeb());
					}
					if (CommonUtility.isNonEmpty(ws.getNoticec())){
						point.add(ws.getNoticec());
					}
					if (CommonUtility.isNonEmpty(ws.getNoticed())){
						point.add(ws.getNoticed());
					}
				}
				if (CommonUtility.isNonEmpty(ws.getNoticeaqa()) || 
						CommonUtility.isNonEmpty(ws.getNoticeaqc1()) ||
						CommonUtility.isNonEmpty(ws.getNoticeaqc2()) ||
						CommonUtility.isNonEmpty(ws.getNoticeb()) ||
						CommonUtility.isNonEmpty(ws.getNoticec()) ||
						CommonUtility.isNonEmpty(ws.getNoticed())){
					rollingPlan.setNoticePoint(true);
					
					if (ws.getWorkStepWitnesses().isEmpty()){
						rollingPlan.setWintnessUnCreate(true);
					}
				}
			}
			rollingPlan.setNoticeType(point);
		}
		
		return rollingPlan;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RollingPlan markBackFill(RollingPlan rollingPlan){
		if (null != rollingPlan){
			List<WorkStep> workSteps = rollingPlan.getWorkSteps();
			
			List<Integer> backFillWorkStepId = new ArrayList<Integer>();
			if (null != workSteps && workSteps.size() > 0){
				Collections.sort(workSteps);
				for (int i = 0; i < workSteps.size(); i++){
					WorkStep ws = workSteps.get(i);
					int result = 0;
					if (CommonUtility.isNonEmpty(ws.getNoticeresult())){
						result = Integer.parseInt(ws.getNoticeresult());
					}
					if (i == workSteps.size() - 1){
						if (CommonUtility.isNonEmpty(ws.getNoticeaqa()) || 
								CommonUtility.isNonEmpty(ws.getNoticeaqc1()) ||
								CommonUtility.isNonEmpty(ws.getNoticeaqc2()) ||
								CommonUtility.isNonEmpty(ws.getNoticeb()) ||
								CommonUtility.isNonEmpty(ws.getNoticec()) ||
								CommonUtility.isNonEmpty(ws.getNoticed())){
							if (result > 1 && !CommonUtility.isNonEmpty(ws.getOperater())) {
								rollingPlan.setAllBackFilled(true);
							}
						} else {
							if (!CommonUtility.isNonEmpty(ws.getOperater())){
								rollingPlan.setAllBackFilled(true);
							}
						}
					} 
					if (CommonUtility.isNonEmpty(ws.getNoticeaqa()) || 
							CommonUtility.isNonEmpty(ws.getNoticeaqc1()) ||
							CommonUtility.isNonEmpty(ws.getNoticeaqc2()) ||
							CommonUtility.isNonEmpty(ws.getNoticeb()) ||
							CommonUtility.isNonEmpty(ws.getNoticec()) ||
							CommonUtility.isNonEmpty(ws.getNoticed())){
						
						if (result <= 1 && !CommonUtility.isNonEmpty(ws.getOperater()) ) {
							break;
						} else if (result <= 1 && CommonUtility.isNonEmpty(ws.getOperater())){
							continue;
						} else if (result > 1 && !CommonUtility.isNonEmpty(ws.getOperater())) {
							backFillWorkStepId.add(ws.getId());
						}
					} else {
						if (CommonUtility.isNonEmpty(ws.getOperater())){
							continue;
						} else {
							backFillWorkStepId.add(ws.getId());
						}
					}
				}
			}
			rollingPlan.setBackFillWorkStepId(backFillWorkStepId);
		}
		return rollingPlan;
	}
	
	@Override
	public List<RollingPlan> markBackFill(List<RollingPlan> rollingPlans){
		if (null != rollingPlans && rollingPlans.size() > 0){
			for (RollingPlan rollingPlan : rollingPlans){
				markBackFill(rollingPlan);
			}
		}
		return rollingPlans;
	}
}