package com.easycms.hd.plan.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.dao.WorkStepenpDao;
import com.easycms.hd.plan.domain.WorkStepenp;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepenpService;
import com.easycms.hd.plan.service.WorkStepTansferService;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

@Service("workStepenpService")
public class WorkStepenpServiceImpl implements WorkStepenpService {
	@Autowired
	private WorkStepenpDao workStepenpDao;
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepTansferService workStepTansferService;

	@Override
	public WorkStepenp add(WorkStepenp website) {
		return workStepenpDao.save(website);
	}

	@Override
	public WorkStepenp findById(Integer id) {
		return workStepenpDao.findById(id);
	}
	
	@Override
	public WorkStepenp getById(Integer id) {
		return workStepenpDao.findById(id);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != workStepenpDao.deleteById(id)){
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Integer[] ids) {
		if (0 != workStepenpDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}
	
	@Override
	public boolean deleteByIDUP(Integer[] ids) {
		if (0 != workStepenpDao.deleteByIDUPIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<WorkStepenp> findAll() {
		return workStepenpDao.findAll();
	}

	@Override
	public List<WorkStepenp> findAll(WorkStepenp condition) {
		return workStepenpDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public Page<WorkStepenp> findPage(Page<WorkStepenp> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		
		org.springframework.data.domain.Page<WorkStepenp> springPage = workStepenpDao.findAll(null, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStepenp> findPageByRollingPlanId(Page<WorkStepenp> page, final Integer rollingPlanId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
//		final RollingPlan rollingPlan = new RollingPlan();
//		rollingPlan.setId(rollingPlanId);
//		
//		Specification<WorkStep> spec = new Specification<WorkStep>() {  
//			@Override
//			public Predicate toPredicate(Root<WorkStep> root,
//					CriteriaQuery<?> query, CriteriaBuilder cb) {
//				List<Predicate> list = new ArrayList<Predicate>();  
//		          
//			    if(rollingPlanId != 0){  
//			        list.add(cb.equal(root.get("rollingPlan").as(RollingPlan.class), rollingPlan));  
//			    }  
//			    
//			    Predicate[] p = new Predicate[list.size()];  
//			    return cb.and(list.toArray(p));  
//			}  
//		}; 
//		
//		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findAll(spec, pageable);
		org.springframework.data.domain.Page<WorkStepenp> springPage = workStepenpDao.findByPageWithRollingPlanId(pageable, rollingPlanId);
		
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		List<WorkStepenp> wsList = page.getDatas();
		for (WorkStepenp ws : wsList){
			ws = workStepTansferService.addWitnessInfo(ws);
		}
		page.setDatas(wsList);
		
		return page;
	}
	@Override
	public Page<WorkStepenp> findPage(WorkStepenp condition,Page<WorkStepenp> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStepenp> springPage = workStepenpDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<WorkStepenp> findByIDs(Integer[] ids) {
		return workStepenpDao.findByIDs(ids);
	}
	
//	private List<WorkStep> addWitnessInfo(List<WorkStep> wsList){
//		for (WorkStep ws : wsList){
//			ws.setWitnessInfo(witnessService.getByWorkStepId(ws.getId()));
//		}
//		return wsList;
//	}
}
