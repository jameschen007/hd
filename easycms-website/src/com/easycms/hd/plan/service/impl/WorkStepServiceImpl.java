package com.easycms.hd.plan.service.impl;


import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.dao.WorkStepDao;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.mybatis.dao.WorkStepMybatisDao;
import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.service.WorkStepTansferService;

@Service("workStepService")
public class WorkStepServiceImpl implements WorkStepService {
	@Autowired
	private WorkStepDao workStepDao;
	@Autowired
	private WorkStepTansferService workStepTansferService;
	@Autowired
	private RollingPlanTansferService rollingPlanTansferService;
	@Autowired
	private WorkStepMybatisDao workStepMybatisDao ;

	@Override
	public WorkStep add(WorkStep workStep) {
		return workStepDao.save(workStep);
	}

	@Override
	public WorkStep findById(Integer id) {
		return transferWitnessNotificationPoint(workStepDao.findById(id));
	}

	@Override
	public List<WorkStep> findByIds(Integer[] ids){

		return transferWitnessNotificationPoint(workStepDao.findByIds(ids));
	}
	
	@Override
	public WorkStep getById(Integer id) {
		return workStepDao.findById(id);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != workStepDao.deleteById(id)){
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Integer[] ids) {
		if (0 != workStepDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}
	
	@Override
	public boolean deleteByIDUP(Integer[] ids) {
		if (0 != workStepDao.deleteByIDUPIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<WorkStep> findAll() {
		return workStepDao.findAll();
	}

	@Override
	public List<WorkStep> findAll(WorkStep condition) {
		return transferWitnessNotificationPoint(workStepDao.findAll(QueryUtil.queryConditions(condition)));
	}

	@Override
	public Page<WorkStep> findPage(Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findAll(null, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStep> findPageByRollingPlanId(Page<WorkStep> page, final Integer rollingPlanId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageWithRollingPlanId(pageable, rollingPlanId);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		
		List<WorkStep> wsList = page.getDatas();
		for (WorkStep ws : wsList){
			ws = workStepTansferService.addWitnessInfo(ws);
			ws = transferWitnessNotificationPoint(ws);
			if (null != ws.getRollingPlan()){
				ws.setRollingPlan(rollingPlanTansferService.markNoticePoint(ws.getRollingPlan()));
			}
		}
		page.setDatas(wsList);
		
		return page;
	}
	@Override
	public Page<WorkStep> findPage(WorkStep condition,Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<WorkStep> findByRollingplanIds(Integer[] ids) {
		return workStepDao.findByRollingplanIds(ids);
	}
	
	private List<WorkStep> transferWitnessNotificationPoint(List<WorkStep> wsList){
		for (WorkStep ws : wsList){
			transferWitnessNotificationPoint(ws);
		}
		return wsList;
	}
	
	private WorkStep transferWitnessNotificationPoint(WorkStep ws){
		Set<String> point = new HashSet<String>();
		if (null != ws){
			if (CommonUtility.isNonEmpty(ws.getNoticeaqa())){
				point.add(ws.getNoticeaqa());
			}
			if (CommonUtility.isNonEmpty(ws.getNoticeaqc1())){
				point.add(ws.getNoticeaqc1());
			}
			if (CommonUtility.isNonEmpty(ws.getNoticeaqc2())){
				point.add(ws.getNoticeaqc2());
			}
			if (CommonUtility.isNonEmpty(ws.getNoticeb())){
				point.add(ws.getNoticeb());
			}
			if (CommonUtility.isNonEmpty(ws.getNoticec())){
				point.add(ws.getNoticec());
			}
			if (CommonUtility.isNonEmpty(ws.getNoticed())){
				point.add(ws.getNoticed());
			}
			ws.setNoticeType(point);
		}
		return ws;
	}
	
//------------------------------------------------------[2017-09-23添加]---------------------------------------------------------------

	@Override
	public Page<WorkStep> findByPageTeamAll(String type, Integer teamId, Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamAll(type, teamId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStep> findByPageTeamUnCompleted(String type, Integer teamId, Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamUnCompleted(type, teamId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<WorkStep> findByPageTeamUnCompletedNotPlan(String type, Integer teamId, Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamUnCompletedNotPlan(type, teamId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<WorkStep> findByPageTeamComplete(String type, Integer teamId, Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamComplete(type, teamId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
//	组长完成的见证工序：查询这条工序上的所有见证点都完成了
	@Override
	public Page<WorkStep> findByPageTeamCompleteNotPlanComplete(String type, Integer teamId, Page<WorkStep> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamCompleteNotPlanComplete(type, teamId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
//------------------------------------------------------[2017-10-30带搜索获取工序步骤]---------------------------------------------------------------
	
	@Override
	public Page<WorkStep> findByPageTeamUnCompletedWithKeyword(String type, Integer teamId, String keyword, Page<WorkStep> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamUnCompletedWithKeyword(type, teamId, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<WorkStep> findByPageTeamCompleteWithKeyword(String type, Integer teamId, String keyword, Page<WorkStep> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<WorkStep> springPage = workStepDao.findByPageTeamCompleteWithKeyword(type, teamId, keyword, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	

	//根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	/**
	 * 
	 * @param map [planIds 计划数组]
	 * @return
	 */
	public List<WorkStepForBatchWitness> getWorkStep(Map<String,Object> map){
		return workStepMybatisDao.getWorkStepByRollingIds(map);
	}
	
	/**
	 * 全部未完成的见证，用于修复应该完成却显示未完成的　都消点了，但管焊队队长点进去看，还在未完成界面
	 */
	@Override
	public List<WorkStep> findByPageUnCompleted() {
		List<WorkStep> dd = workStepDao.findByPageUnCompleted();
		return dd;
	}
	
}
