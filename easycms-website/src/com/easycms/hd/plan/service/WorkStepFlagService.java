package com.easycms.hd.plan.service;

public interface WorkStepFlagService {
	void changeStepFlag(Integer rollingPlanId,String dosage) throws Exception;
}
