package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStepWitness;

public interface WitnessService {
	
	List<WorkStepWitness> findAll();
	
	List<WorkStepWitness> findAll_of_witnesser(Integer id);
	
	List<WorkStepWitness> getByIds(Integer[] ids);
	
	List<WorkStepWitness> getByWorkStepId(Integer id);
	
	List<WorkStepWitness> getParentByWorkStepId(Integer id);
	
	List<WorkStepWitness> getChilrenByWorkStepId(Integer id);
	
	List<WorkStepWitness> findAll(WorkStepWitness condition);
	
	List<WorkStepWitness> findByWitnesserComplate(String witnesserId);
	
    List<WorkStepWitness> findByWitnesserUnComplate(String witnesserId);
    
    Page<WorkStepWitness> findByPageWitnesserUnComplate(Page<WorkStepWitness> page, String witnesserId);
    
	Page<WorkStepWitness> findByPageWitnesserComplate(Page<WorkStepWitness> page, String witnesserId);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	Page<WorkStepWitness> findPage(Page<WorkStepWitness> page, final String condition, final String[] value);
	
	Page<WorkStepWitness> findPage(Page<WorkStepWitness> page);
	
	Page<WorkStepWitness> findPageByWorkStepId(Page<WorkStepWitness> page, final Integer workStepId);
	
	Page<WorkStepWitness> findPage(WorkStepWitness condition,Page<WorkStepWitness> page);
	/**
	 * 根据ID查找
	 * @param id
	 * @return
	 */
	WorkStepWitness findById(Integer id);
	
	boolean delete(Integer id);
	
	boolean deleteByWorkStep(Integer id);
	
//	boolean expireByWorkStep_XX(Integer id);
	
	boolean delete(Integer[] ids);
	
	boolean update(WorkStepWitness workStepWitness);
	
	WorkStepWitness add(WorkStepWitness workStepWitness);
	
	int batchAdd(List<WorkStepWitness> workStepWitnessList);
	
	List<WorkStepWitness> findByWitness(String[] ids);
	
	List<WorkStepWitness> findByWitnesserId(String witnesserId);
	
	List<WorkStepWitness> findByChildrenIsNullAndWitnessIn(List<String> witnessId);
	
	List<WorkStepWitness> findByChildrenIsNotNullAndWitnessIn(List<String> witnessId);
	
	List<WorkStepWitness> findByCompleteWitness(List<String> witnessId);
	
	List<WorkStepWitness> findByUnCompleteWitness(List<String> witnessId);
	
	List<WorkStepWitness> findMyLaunchWitness(String userId);
	
	Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId, Page<WorkStepWitness> page);
//-----------------for admin	
	List<WorkStepWitness> findByChildrenIsNull();
	
	List<WorkStepWitness> findByChildrenIsNullAndWitnessIsNotNull();
	
	List<WorkStepWitness> findByChildrenIsNotNull();
	
	List<WorkStepWitness> findByChildrenIsNotNullAndWitnessIsNotNull();
	
	List<WorkStepWitness> findByComplete();
	
	List<WorkStepWitness> findByComplete_();
	
	List<WorkStepWitness> findByComplete_of_witnesser(Integer id);
	
	List<WorkStepWitness> findByUnComplete_of_witnesser(Integer id);
	
	List<WorkStepWitness> findByUnComplete();
	
	List<WorkStepWitness> findByUnComplete_();
	//-----------------------count
	int countByWitnessToAssign(List<String> witness);
	
	int countByWitnessToAssign(List<String> witness, String categoryFilter);
	
	int countdMyReciveWitness(String userId);
	
	int countMyCreateWitness(String userId);
	
	int countMyWitnessAssigned(List<String> witness);

	Page<WorkStepWitness> findByChildrenIsNullAndWitnessIn(
			List<String> witnessId, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByChildrenIsNull(Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByChildrenIsNotNullAndWitnessIn(
			List<String> witnessId, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByChildrenIsNotNull(Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByCompleteWitness(List<String> witnessId,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByComplete(Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByUnCompleteWitness(List<String> witnessId,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByUnComplete(Page<WorkStepWitness> page);

	Page<WorkStepWitness> findMyLaunchWitness(String userId,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByWitnesserUnComplate(String witnesserId,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findByWitnesserComplate(String witnesserId,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findPageByKeyWords(Page<WorkStepWitness> page,
			String condition, String[] value, String keyword);

	int countCompleteWitness(List<String> witness);
	int countCompleteWitness(List<String> witness, String categoryFilter);

	int countWitnesserAndIsokNot(String userId);
	
	int countWitnesserAndIsokNot_QC1(String userId);
	int countWitnesserAndIsokNot_QC2(String userId);
	int countWitnesserAndIsokNot_A(String userId);
	int countWitnesserAndIsokNot_B(String userId);
	int countWitnesserAndIsokNot_C(String userId);
	int countWitnesserAndIsokNot_D(String userId);

	Page<WorkStepWitness> findByPageWitnesserComplate(
			Page<WorkStepWitness> page, String witnesserId, String keyword);

	Page<WorkStepWitness> findByCompleteWitness(List<String> witnessId,
			Page<WorkStepWitness> page, String keyword);

	Page<WorkStepWitness> findByPageWitnesserUnComplate(
			Page<WorkStepWitness> page, String witnesserId, String keyword);

	Page<WorkStepWitness> findMyLaunchWitnessByPage(String userId,
			String keyword, Page<WorkStepWitness> page);

	int countdMyReciveWitness(String userId, String categoryFilter);

	Page<WorkStepWitness> findByChildrenIsNullAndWitnessIsNotNull(Page<WorkStepWitness> page);

//---------------------------------------------[2017-09-16为适应中核五公司而做的添加]---------------------------------------------------------------------------
	//班长未提交到见证组的列表
	Page<WorkStepWitness> findMonitorPendingWitnessByPage(Integer userId, String rollingPlanType, String noticePointType,
			Page<WorkStepWitness> page);
	
	//QC2见证列表
	Page<WorkStepWitness> findMemberPendingQC2WitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Page<WorkStepWitness> page);
	
	//QC2见证列表中的额外见证信息
	Page<WorkStepWitness> findMemberPendingQC2WitnessAdvanceByPage(Integer userId,
			String rollingPlanType, Integer workStepId, String noticePointType, Page<WorkStepWitness> page);
	
	/**QC2中的未分派的见证  适用于　QC1ReplaceQC2*/
	Page<WorkStepWitness> findMemberUnassignQC2WitnessByPage(Integer userId, String rollingPlanType, String noticePointType, Page<WorkStepWitness> page);
	
	//我发起的见证
	Page<WorkStepWitness> findMyLaunchedWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
	//班长分派见证到见证组组长
	boolean assignToWitnessTeam(Integer[] ids, Integer teamId);

	//见证组长查看所有未分派的见证
	Page<WorkStepWitness> findQCTeamUnassignWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//分派见证到见证组员
	boolean assignToWitnessMember(Integer[] ids, Integer teamId, Integer memberId);

	//按见证组员去找未完成的列表
	Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
	//按组长去找未完成的列表
	Page<WorkStepWitness> findTeamUnCompleteWitnessByPage(Integer teamId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//按见证组员查找不合格的列表
	Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
	//按见证组员查找合格的列表
	Page<WorkStepWitness> findQCMemberQualifiedWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//得到某一条见证的详细信息
	WorkStepWitness findByWitnessIdAndRollingPlanType(Integer witnessId, String rollingPlanType);

	//得到某个人未完成的列表
	Page<WorkStepWitness> findQCMemberCompleteWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//得到某个组长下面未完成的列表
	Page<WorkStepWitness> findTeamCompleteWitnessByPage(Integer teamId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//见证组长得到未处理的见证列表
	Page<WorkStepWitness> findQCTeamUnHandledWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//见证组长得到已处理的见证列表
	Page<WorkStepWitness> findQCTeamHandledWitnessByPage(Integer memberId, String rollingPlanType,
			Page<WorkStepWitness> page);

	//见证组长已分派的见证列表
	Page<WorkStepWitness> findQCTeamAssignedWitnessByPage(Integer userId, String rollingPlanType, Page<WorkStepWitness> page);

	List<WorkStepWitness> findMemberPendingQC2WitnessAdvance(Integer userId, String rollingPlanType, Integer workStepId);

	//将分派到QC2的见证数据，默认将其下的QC， QA， PAEC打上QC2的分派信息
	boolean assignToWitnessMemberQC2(Integer memberId, Integer workStepId);

	//QC2成员的分派
	boolean assignToWitnesserQC2(Integer[] ids, Integer qc2Id, Integer memberId);

	//得到QC2成员类型为QC2的独立项
	WorkStepWitness findQC2SingleItem(Integer userId, String rollingPlanType, Integer workStepId);
	//得到QC1成员类型为QC1的独立项
	WorkStepWitness findQC1SingleItem(Integer userId, String rollingPlanType, Integer workStepId);

	//QC2未完成的列表  //noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	Page<WorkStepWitness> findQC2MemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint);
	
	//QC2下的成员查看未完成的列表
	Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
	//QC2下的成员查看完成的列表
	Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);

	List<WorkStepWitness> getActiveByWorkStepId(Integer id);

	//QC2不合格列表      noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	Page<WorkStepWitness> findQC2MemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint);

	//QC2下线成员不合格列表
	Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
	//QC2合格列表		noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	Page<WorkStepWitness> findQC2MemberQulifiedWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint);
	
	//QC2下线成员合格列表
	Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPage(Integer userId, String rollingPlanType,
			Page<WorkStepWitness> page);
	
//----------------------[2017-10-30见证列表分页信息带全局搜索条件]--------------------	
	Page<WorkStepWitness> findQCMemberUnCompleteWitnessByPageWithKeyword(Integer memberId, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findTeamUnCompleteWitnessByPageWithKeyword(Integer id, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQC2DownLineMemberUnCompleteWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCMemberCompleteWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findTeamCompleteWitnessByPageWithKeyword(Integer id, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQC2DownLineMemberCompleteWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCTeamUnassignWitnessByPageWithKeyword(Integer id, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page);

	/**适用于　QC1ReplaceQC2*/
	Page<WorkStepWitness> findMemberUnassignQC2WitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String noticePointType, String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findMonitorPendingWitnessByPageWithKeyword(Integer id, String rollingPlanType, String noticePointType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQC2DownLineMemberUnQulifiedWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCMemberUnQulifiedWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQC2DownLineMemberQulifiedWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCMemberQualifiedWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCTeamUnHandledWitnessByPageWithKeyword(Integer id, String rollingPlanType,
			String keyword, Page<WorkStepWitness> page);

	Page<WorkStepWitness> findQCTeamHandledWitnessByPageWithKeyword(Integer id, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page);

	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	Page<WorkStepWitness> findQC2TeamUnHandledWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint);

	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 */
	Page<WorkStepWitness> findQC2TeamHandledWitnessByPage(Integer userId, String rollingPlanType, String keyword,
			Page<WorkStepWitness> page,Integer witnessteam,Integer witness,String noticePoint);

	/**
	 * 福清QC１组长分源组员时，附带打外部单位的也分配了。
	 * @param memberId
	 * @param workStepId
	 * @param witnessteam
	 * @return
	 */
	boolean assignToWbWitnessMemberFqQC1(Integer memberId, Integer workStepId, Integer witnessteam);
}
