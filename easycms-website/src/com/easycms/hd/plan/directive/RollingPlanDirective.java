package com.easycms.hd.plan.directive;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.JsonUtils;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取滚动计划列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 */
@Slf4j
public class RollingPlanDirective extends BaseDirective<RollingPlan>  {
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private RollingPlanTansferService rollingPlanTansferService;
	@Autowired
	private UserService userService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected RollingPlan field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		String drawno = FreemarkerTemplateUtility.getStringValueFromParams(params, "drawno");
		String weldno = FreemarkerTemplateUtility.getStringValueFromParams(params, "weldno");
		log.debug("[weldno : drawno] ==> " + weldno + " : " + drawno);
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			RollingPlan rollingPlan = rollingPlanService.findById(id);
			return rollingPlan;
		}
		
		if (CommonUtility.isNonEmpty(drawno) && CommonUtility.isNonEmpty(weldno)) {
			RollingPlan rollingPlan = rollingPlanService.findByDrawnoAndWeldno(drawno, weldno);
			return rollingPlan;
		}
		return null;
	}
	
	@SuppressWarnings({ "rawtypes"})
	@Override
	protected List<RollingPlan> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<RollingPlan> pager,
			Map<String, Object> envParams) {
		String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		String keyword = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		String type =  (String) ContextUtil.getSession().getAttribute("type");
		User user =  ContextUtil.getCurrentLoginUser();
		
		RollingPlan condition = null;
		
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(RollingPlan.class, filter,"yyyy-MM-dd");
		}else{
			condition = new RollingPlan();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		
		if (pageable) {
			if (null != custom && custom.equals("captain")){
//				List<User> children = userService.findUserByParentId(user.getId());
			    //直接领导查询下级，通过部门上下级查询
				List<User> children = userService.findUserByDeptParentId(user.getId());
				
				if (null != children && !children.isEmpty()) {
					List<Integer> monitorId = children.stream().map(child -> {return child.getId();}).collect(Collectors.toList());

					if (CommonUtility.isNonEmpty(keyword)) {
						pager = rollingPlanService.findByPageConsmonitorWithKeyword(type, monitorId,keyword, pager);
					}else{
						pager = rollingPlanService.findByPageConsmonitor(type, monitorId, pager);
					}
					
					log.info(JsonUtils.objectToJson(pager.getDatas()));
					
					log.info("==> monitor RollingPlan length [" + pager.getDatas().size() + "]");
				}
				return pager.getDatas();
			} else if (null != custom && custom.equals("monitor")){
					List<Integer> monitorId = new ArrayList<Integer>();
					monitorId.add(user.getId());
					if (CommonUtility.isNonEmpty(keyword)) {
						pager = rollingPlanService.findByPageMonitorUnAssignWithKeyword(type, monitorId,keyword, pager);
					}else{
						pager = rollingPlanService.findByPageMonitorUnAssign(type, monitorId, pager);
					}
					
					log.info(JsonUtils.objectToJson(pager.getDatas()));
					
					log.info("==> monitor unassign RollingPlan length [" + pager.getDatas().size() + "]");
					return pager.getDatas();
			} else if (null != custom && custom.equals("monitorAssigned")){
				List<Integer> monitorId = new ArrayList<Integer>();
				monitorId.add(user.getId());
				if (CommonUtility.isNonEmpty(keyword)) {
					pager = rollingPlanService.findByPageMonitorAssignedWithKeyword(type, monitorId,keyword, pager);
				}else{
					pager = rollingPlanService.findByPageMonitorAssigned(type, monitorId, pager);
				}
				
				log.info(JsonUtils.objectToJson(pager.getDatas()));
				
				log.info("==> monitor assinged RollingPlan length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			} else {
				pager = rollingPlanService.findPage(condition, pager);
				pager.setDatas(rollingPlanTansferService.transferRollingPlan(pager.getDatas()));			
				log.info("==> RollingPlan length [" + pager.getTotalCounts() + "]");
				return pager.getDatas();
			} 
		} else {
			List<RollingPlan> rollingPlans = new ArrayList<RollingPlan>();
			if (null != custom && custom.equals("teamAlready")){
				rollingPlans = rollingPlanService.findByConsteamIsNotNull();
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> teamAlready RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			} else if (null != custom && custom.equals("team")){
				rollingPlans = rollingPlanService.findByConsteamIsNull();
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> team RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			} else if (null != custom && custom.equals("groupAlready")){
				if (null != condition.getConsteam()){
//					if (condition.getConsteam().contains("\"")){
//						log.info("findByConsteamInAndConsendmanIsNotNull with : " + condition.getConsteam());
//						Gson gson = new Gson();  
//						List<String> consteams = gson.fromJson(condition.getConsteam(), new TypeToken<List<String>>(){}.getType());  
//						rollingPlans = rollingPlanService.findByConsteamInAndConsendmanIsNotNull(consteams);
//					} else {
						rollingPlans = rollingPlanService.findByConsteamIsNotNullAndConsendmanIsNotNull();
//					}
				} else {
					rollingPlans = rollingPlanService.findByConsteamIsNotNullAndConsendmanIsNotNull();
				}
				
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> groupAlready RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			} else if (null != custom && custom.equals("group")){
				if (null != condition.getConsteam()){
//					if (condition.getConsteam().contains("\"")){
//						log.info("findByConsteamInAndConsendmanIsNull with : " + condition.getConsteam());
//						Gson gson = new Gson();  
//						List<String> consteams = gson.fromJson(condition.getConsteam(), new TypeToken<List<String>>(){}.getType());  
//						rollingPlans = rollingPlanService.findByConsteamInAndConsendmanIsNull(consteams);
//					} else {
						rollingPlans = rollingPlanService.findByConsteamIsNotNullAndConsendmanIsNull();
//					}
				} else {
					rollingPlans = rollingPlanService.findByConsteamIsNotNullAndConsendmanIsNull();
				}
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> group RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			} else if (null != custom && custom.equals("task")){
				if (null != condition.getConsendman() && condition.getConsendman().contains("\"")){
					log.info(condition.getConsendman());
					Gson gson = new Gson();  
					List<String> consendmen = gson.fromJson(condition.getConsendman(), new TypeToken<List<String>>(){}.getType());  
					log.info(CommonUtility.toJson(consendmen));
					rollingPlans = rollingPlanService.findByConsendmanInAndIsendNot(consendmen, 2);
				} else {
					rollingPlans = rollingPlanService.findByConsendmanIsNotNullAndIsendNot(2);
				}
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> task RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlanTansferService.markBackFill(rollingPlans);
			} else if (null != custom && custom.equals("taskAlready")){
				if (null != condition.getConsendman() && condition.getConsendman().contains("\"")){
					log.info(condition.getConsendman());
					Gson gson = new Gson();  
					List<String> consendmen = gson.fromJson(condition.getConsendman(), new TypeToken<List<String>>(){}.getType());  
					log.info(CommonUtility.toJson(consendmen));
					rollingPlans = rollingPlanService.findByConsendmanInAndIsend(consendmen, 2);
				} else {
					rollingPlans = rollingPlanService.findByConsendmanIsNotNullAndIsend(2);
				}
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				log.info("==> taskAlready RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			} else {
				rollingPlans = rollingPlanService.findAll(condition);
				rollingPlans = rollingPlanTansferService.transferRollingPlan(rollingPlans);
				
				log.info("==> RollingPlan length [" + rollingPlans.size() + "]");
				return rollingPlans;
			}
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<RollingPlan> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}
}
