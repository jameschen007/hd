package com.easycms.hd.plan.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.response.WorkStepResult;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.WorkStepService;

/**
 * 获取工序步骤列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-07 
 */
public class WorkStepTransferDirective extends BaseDirective<WorkStepResult>  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WorkStepTransferDirective.class);
	
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private WorkStepApiService workStepApiService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected WorkStepResult field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			WorkStep workStep = workStepService.findById(id);
			if (null == workStep.getRollingPlan()){
				return null;
			}

			return workStepApiService.workStepTransferForMobile(workStep);
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepResult> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<WorkStepResult> pager,
			Map<String, Object> envParams) {
		//不允许有任何的操作。
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepResult> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}
}
