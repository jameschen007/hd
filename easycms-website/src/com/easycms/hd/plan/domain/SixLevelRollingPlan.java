package com.easycms.hd.plan.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;

import lombok.Data;

@Data
public class SixLevelRollingPlan implements Serializable{
	private static final long serialVersionUID = 2526312957959968609L;
	
//	//序号
//	private String id;
/* enpower	计划主键id */
	private String enpowerPlanId;
	//作业条目编号
	private String workListNo;
	//房间号
	private String roomNo;
	//系统号
	private String systemNo;
//	图纸版本号	drawSerial	draw_serial
	private String drawSerial;
	//图纸号
	private String drawingNo;
//	//作业包名称  进度计划同步 没有
//	private String workPackageName;
	//工程最编号
//	private String projectTimeNo;
	//管线号
//	private String lineNo;
//	//工程 量
//	private String projectTime;
	//规格
	private String speification;
	//材质
	private String matelial;
	//核级
	private String croeLevel;
//	//工程最单位
//	private String projectTimeUnit;
//	点数
	private BigDecimal points;
////	点值
//	private String spot;
//	//Itp编号
//	private String itpNo;
//	//计划施工日期
//	private String planDate;
	//计划施工日期
	private String planStartDate;
//	//计划施工日期
	private String planEndDate;
//	施工队	conscaptainName	conscaptain_name
	private String conscaptainName;
//	班长名称		
	private String consmonitorName;
	private Integer consmonitor;
	private String consmonitorEnpower;
//	施工班组代码
	private String consmonitorCode;
//	//施工班组
//	private String consTeam;
	//状态
	private String status;
	//备注
	private String remarks;
	//子项
	private String subItem;
	//单位
	private String projectUnit;
	//工程量
	private String projectCost;
	private String projectCost2;
	//机组号
	private String unitNo;
	//工程量类别
	private String projectType;
	//焊口号　有个weldid字段，这个字段是焊口id字段，不为空就是焊口条目，为空就是非焊口条目
	private String weldno;
	//工程量编号
	private String projectNo;
//	质量计划id	qualityplanid
	private String qualityplanid;
	//质量计划号
	private String qualityplanno;
	//工程量名称
	private String projectName;
	//模块类型：管道计划GDJH
	private String type;
	//enpower的关联作业条目和材料信息字段，只做保存即可
	private String pMainId;
	/**
"是否报量FLAG
	 * "(向app推送时一起推)  
	 */
	private String isReport;
	
	/**
	 * enpower作业条目id
	 * (向app推送时一起推)
	 */
	private String workListId;
	
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 设备编号
	 */
	private String deviceNo;
	/**
	 * 设备类型
	 */
	private String deviceType;
	/**
	 * 部件号/标识号
	 */
	private String markNo;
	/**
	 * 单元件总数
	 */
	private String unitTotalCount;
	/**
	 * 单元件未完成数
	 */
	private String unitUncompleteCount;
	/**
	 * 回路总数
	 */
	private String loopTotalCount;
	/**
	 * 回路未完成数
	 */
	private String loopUncompleteCount;
	/**
	 * 冲洗/试压名称
	 */
	private String rinseName;
	/**
	 * 冲洗/试压类型
	 */
	private String rinseType;
	
	/**
	 * 20171110添加同步接口添加以下属性
	 * ITP类型（类别）
	 */
	@Column(name = "ITP_TYPES")
	private String ITP_TYPES;
	/**
	 * ITP模板类型（模板类型）
	 */
	@Column(name = "ITP_TYPE")
	private String ITP_TYPE;
	/**
	 * (质量计划的)专业
	 */
	@Column(name = "SPECIALTY")
	private String SPECIALTY;
	/**
	 * 版本（质量计划版本）
	 */
	@Column(name = "T_REV")
	private String T_REV;
	/**
	 * 区域 
	 */
	@Column(name = "AREA")
	private String AREA;
	/**
	 * 通知单名称（ITP模板名称(中文)）
	 */
	@Column(name = "MQP_NAME")
	private String MQP_NAME;
	
	/**
	 * 材料信息
	 */
	private List<EnpowerMaterialInfo> materialInfoList;
/**
 * 工序信息
 */
	private List<WorkStep> workStepList;
//	/**
//	 * 设备信息
//	 */
//	private List<EnpowerEquipment> equipmentList;
	/**
	 * 任务单信息，即之前的设备信息，应为任务单信息
	 */
	private List<EnpowerJobOrder> workOrderList;
	
}
