package com.easycms.hd.plan.domain;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.conference.domain.ExpireNoticeFile;

import lombok.Data;

@Data
public class EnpowerExpireNoticeResponse implements Serializable{
	private static final long serialVersionUID = 2526312957959968609L;

	private String id;//

	private String cancCode;// 文件失效通知单编号

	private String askReturnDate;// 要求归还时间

	private String releaseDate;// 发布时间

	private String projCode;// 项目代号

	private String compCode;// 公司代码

	private String drafter;// 编制人id

	private String draftDate;// 编制日期

	private String signer;// 审批人id

	private List<ExpireNoticeFile> cancFileList;
}
