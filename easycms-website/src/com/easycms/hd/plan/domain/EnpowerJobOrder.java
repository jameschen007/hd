package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>任务单信息列表<br>
 * <b>创建时间：</b>2017-10-23 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name = "enpower_job_order")
@Data
@EqualsAndHashCode(callSuper = false)
public class EnpowerJobOrder extends BasicForm implements Serializable {
	private static final long serialVersionUID = 7991371355432184827L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	/**
	 * 滚动计划id
	 */
	@Column(name = "rolling_plan_id", nullable = false)
	private Integer rollingPlanId;

	/**
	 * enpower作业包工程量的id
	 */
	@Column(name = "enpower_id", nullable = false)
	private String enpowerId;
	/**
	 * enpower作业包图纸号
	 */
	@Column(name = "draw_no", nullable = false)
	private String drawNo;
	/**
	 * enpower作业包位号
	 */
	@Column(name = "pitem_no", nullable = false)
	private String pitemNo;

}
