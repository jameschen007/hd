package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "push_log")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class PushLog extends BasicForm implements Serializable {
	private static final long serialVersionUID = 6747243810387430052L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "extra_user_id", length = 500)
	private String extraUserId;
	@Column(name = "extra_category")
	private String extraCategory;
	@Column(name = "extra_data")
	private String extraData;
	@Column(name = "extra_remark")
	private String extraRemark;
	@Column(name = "message")
	private String message;
	@Column(name = "title")
	private String title;
	@Column(name = "device")
	private String device;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "send_on")
	private Date sendOn;
}