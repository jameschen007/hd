package com.easycms.hd.plan.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.form.WorkStepWitnessFake;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Entity
@Table(name = "work_stepenp")
public class WorkStepenp extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = 1487991188000135619L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "autoid", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "stepno")
	private Integer stepno;
	
	@Column(name = "stepflag")
	private String stepflag;
	
	@Column(name = "stepname", length = 50)
	private String stepname;
	
	@Column(name = "operater", length = 50)
	private String operater;
	
	@Column(name = "operatedate", length = 0)
	private Date operatedate;
	
	@Column(name = "operatedesc", length = 1000)
	private String operatedesc;
	
	@Column(name = "noticeainfo", length = 100)
	private String noticeainfo;
	
	@Column(name = "noticeresult", length = 50)
	private String noticeresult;
	
	@Column(name = "noticeresultdesc", length = 100)
	private String noticeresultdesc;
	
	@Column(name = "noticeaqc1", length = 1)
	private String noticeaqc1;
	
	@Column(name = "witnesseraqc1", length = 50)
	private String witnesseraqc1;
	
	@Column(name = "witnessdateaqc1", length = 0)
	private Date witnessdateaqc1;
	
	@Column(name = "noticeaqc2", length = 1)
	private String noticeaqc2;
	
	@Column(name = "witnesseraqc2", length = 50)
	private String witnesseraqc2;
	
	@Column(name = "witnessdateaqc2", length = 0)
	private Date witnessdateaqc2;
	
	@Column(name = "noticeaqa", length = 1)
	private String noticeaqa;
	
	@Column(name = "witnesseraqa", length = 50)
	private String witnesseraqa;
	
	@Column(name = "witnessdateaqa", length = 0)
	private Date witnessdateaqa;

	@Column(name = "noticeb", length = 50)
	private String noticeb;
	
	@Column(name = "witnesserb", length = 50)
	private String witnesserb;
	
	@Column(name = "witnessdateb", length = 0)
	private Date witnessdateb;
	
	@Column(name = "noticec", length = 50)
	private String noticec;
	
	@Column(name = "witnesserc", length = 50)
	private String witnesserc;
	
	@Column(name = "witnessdatec", length = 0)
	private Date witnessdatec;
	
	@Column(name = "noticed", length = 50)
	private String noticed;
	
	@Column(name = "witnesserd", length = 50)
	private String witnesserd;
	
	@Column(name = "witnessdated", length = 0)
	private Date witnessdated;
	
	@Column(name = "created_on", length = 0)
	private Date createdOn;
	
	@Column(name = "created_by", length = 40)
	private String createdBy;
	
	@Column(name = "updated_on", length = 0)
	private Date updatedOn;
	
	@Column(name = "updated_by", length = 40)
	private String updatedBy;
	
	@Column(name = "drawno", length = 45)
	private String drawno;
	
	public String getDrawno() {
		return drawno;
	}

	public void setDrawno(String drawno) {
		this.drawno = drawno;
	}

	public String getWeldno() {
		return weldno;
	}

	public void setWeldno(String weldno) {
		this.weldno = weldno;
	}

	@Column(name = "weldno", length = 45)
	private String weldno;
	
	@Transient
	private boolean lastone = false;
	
	@Column(name = "autoidup")
	private String rollingPlan;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStepno() {
		return stepno;
	}

	public void setStepno(Integer stepno) {
		this.stepno = stepno;
	}

	public String getStepflag() {
		return stepflag;
	}

	public void setStepflag(String stepflag) {
		this.stepflag = stepflag;
	}

	public String getStepname() {
		return stepname;
	}

	public void setStepname(String stepname) {
		this.stepname = stepname;
	}

	public String getOperater() {
		return operater;
	}

	public void setOperater(String operater) {
		this.operater = operater;
	}

	public Date getOperatedate() {
		return operatedate;
	}

	public void setOperatedate(Date operatedate) {
		this.operatedate = operatedate;
	}

	public String getNoticeainfo() {
		return noticeainfo;
	}

	public void setNoticeainfo(String noticeainfo) {
		this.noticeainfo = noticeainfo;
	}

	public String getNoticeresult() {
		return noticeresult;
	}

	public void setNoticeresult(String noticeresult) {
		this.noticeresult = noticeresult;
	}

	public String getNoticeresultdesc() {
		return noticeresultdesc;
	}

	public void setNoticeresultdesc(String noticeresultdesc) {
		this.noticeresultdesc = noticeresultdesc;
	}

	public String getNoticeaqc1() {
		return noticeaqc1;
	}

	public void setNoticeaqc1(String noticeaqc1) {
		this.noticeaqc1 = noticeaqc1;
	}

	public String getWitnesseraqc1() {
		return witnesseraqc1;
	}

	public void setWitnesseraqc1(String witnesseraqc1) {
		this.witnesseraqc1 = witnesseraqc1;
	}

	public Date getWitnessdateaqc1() {
		return witnessdateaqc1;
	}

	public void setWitnessdateaqc1(Date witnessdateaqc1) {
		this.witnessdateaqc1 = witnessdateaqc1;
	}

	public String getNoticeaqc2() {
		return noticeaqc2;
	}

	public void setNoticeaqc2(String noticeaqc2) {
		this.noticeaqc2 = noticeaqc2;
	}

	public String getWitnesseraqc2() {
		return witnesseraqc2;
	}

	public void setWitnesseraqc2(String witnesseraqc2) {
		this.witnesseraqc2 = witnesseraqc2;
	}

	public Date getWitnessdateaqc2() {
		return witnessdateaqc2;
	}

	public void setWitnessdateaqc2(Date witnessdateaqc2) {
		this.witnessdateaqc2 = witnessdateaqc2;
	}

	public String getNoticeaqa() {
		return noticeaqa;
	}

	public void setNoticeaqa(String noticeaqa) {
		this.noticeaqa = noticeaqa;
	}

	public String getWitnesseraqa() {
		return witnesseraqa;
	}

	public void setWitnesseraqa(String witnesseraqa) {
		this.witnesseraqa = witnesseraqa;
	}

	public Date getWitnessdateaqa() {
		return witnessdateaqa;
	}

	public void setWitnessdateaqa(Date witnessdateaqa) {
		this.witnessdateaqa = witnessdateaqa;
	}

	public String getNoticeb() {
		return noticeb;
	}

	public void setNoticeb(String noticeb) {
		this.noticeb = noticeb;
	}

	public String getWitnesserb() {
		return witnesserb;
	}

	public void setWitnesserb(String witnesserb) {
		this.witnesserb = witnesserb;
	}

	public Date getWitnessdateb() {
		return witnessdateb;
	}

	public void setWitnessdateb(Date witnessdateb) {
		this.witnessdateb = witnessdateb;
	}

	public String getNoticec() {
		return noticec;
	}

	public void setNoticec(String noticec) {
		this.noticec = noticec;
	}

	public String getWitnesserc() {
		return witnesserc;
	}

	public void setWitnesserc(String witnesserc) {
		this.witnesserc = witnesserc;
	}

	public Date getWitnessdatec() {
		return witnessdatec;
	}

	public void setWitnessdatec(Date witnessdatec) {
		this.witnessdatec = witnessdatec;
	}

	public String getNoticed() {
		return noticed;
	}

	public void setNoticed(String noticed) {
		this.noticed = noticed;
	}

	public String getWitnesserd() {
		return witnesserd;
	}

	public void setWitnesserd(String witnesserd) {
		this.witnesserd = witnesserd;
	}

	public Date getWitnessdated() {
		return witnessdated;
	}

	public void setWitnessdated(Date witnessdated) {
		this.witnessdated = witnessdated;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public String getOperatedesc() {
		return operatedesc;
	}

	public void setOperatedesc(String operatedesc) {
		this.operatedesc = operatedesc;
	}

	public boolean isLastone() {
		return lastone;
	}

	public void setLastone(boolean lastone) {
		this.lastone = lastone;
	}

}