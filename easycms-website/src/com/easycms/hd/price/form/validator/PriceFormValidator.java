package com.easycms.hd.price.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.hd.price.form.PriceForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-08 
 * 
 */
@Component("priceFormValidator")
public class PriceFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PriceForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "speciality",
		"form.speciality.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pricetype",
		"form.pricetype.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "unitprice",
		"form.unitprice.empty");
	}
}
