package com.easycms.hd.price.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.price.domain.Price;

public interface PriceService{
	
	List<Price> findAll();
	
	List<Price> findAll(Price condition);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	
	Page<Price> findPage(Page<Price> page);
	
	Page<Price> findPage(Price condition,Page<Price> page);
	/**
	 * 根据ID查找
	 * @param id
	 * @return
	 */
	Price findById(Integer id);
	
	boolean delete(Integer id);
	
	boolean delete(Integer[] ids);
	
	/**
	 * 更新单价信息
	 * @param price
	 * @return
	 */
	boolean update(Price price);
	
	/**
	 * 添加单价信息
	 * @param price
	 * @return
	 */
	Price add(Price price);
	
	boolean checkPricetypeExist(String speciality, String pricetype);
	
	boolean checkPricetypeExistWithId(String speciality, String pricetype, Integer id);
	
	Price getPricetypeExist(String speciality, String pricetype);
}
