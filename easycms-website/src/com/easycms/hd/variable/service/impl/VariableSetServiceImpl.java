package com.easycms.hd.variable.service.impl;


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;

@Service("variableSetService")
public class VariableSetServiceImpl implements VariableSetService {

	@Autowired
	private VariableSetDao variableSetDao;


	@Override
	public void empty(String type) {
		variableSetDao.deleteAllValue(type);
	}

	@Override
	public boolean updateById(VariableSet variableSet) {

		int result = variableSetDao.update(variableSet.getSetvalue(),variableSet.getStatus(), variableSet.getUpdateon(), variableSet.getUpdateby(), variableSet.getSetkey(), variableSet.getType(),variableSet.getId());
		return result > 0;
	}
	@Override
	public boolean update(VariableSet variableSet) {
		int result = variableSetDao.update(variableSet.getSetvalue(),variableSet.getStatus(), variableSet.getUpdateon(), variableSet.getUpdateby(), variableSet.getSetkey(), variableSet.getType());
		return result > 0;
	}

	@Override
	public VariableSet add(VariableSet variableSet) {
		return variableSetDao.save(variableSet);
	}

	@Override
	public VariableSet findById(Integer id) {
		return variableSetDao.findById(id);
	}

	@Override
	public VariableSet findByKey(String key, String type) {
		return variableSetDao.findByKey(key, type);
	}
	
	@Override
	public VariableSet findByKey(String key) {
		return variableSetDao.findBySetkey(key);
	}
	
	@Override
	public List<VariableSet> findByType(String type) {
		
		if(type==null && "".equals(type.trim())){
			return Collections.emptyList();
		}
		
		return variableSetDao.findByType(type.trim());
	}

	@Override
	public String findValueByKey(String key, String type) {
		VariableSet variableSet = variableSetDao.findByKey(key, type);
		
		if (null != variableSet){
			return variableSet.getSetvalue();
		}
		return null;
	}
	
	@Override
	public String findValueByKey(String key) {
		VariableSet variableSet = variableSetDao.findByKey(key);
		
		if (null != variableSet){
			return variableSet.getSetvalue();
		}
		return null;
	}

	@Override
	public List<VariableSet> findByAll() {
		return variableSetDao.findByAll();
	}

	@Override
	public Set<String> findKeyByValue(String value, String type) {
		return variableSetDao.findSetkeyBySetvalueAndType(value, type);
	}
	@Override
	public Set<String> findSetValueBySetKeyAndType(String key, String type){
		return variableSetDao.findSetValueBySetKeyAndType(key, type);
	}
}
