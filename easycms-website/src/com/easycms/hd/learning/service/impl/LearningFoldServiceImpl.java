package com.easycms.hd.learning.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.dao.LearningFoldDao;
import com.easycms.hd.learning.domain.LearningFold;
import com.easycms.hd.learning.service.LearningFoldService;

@Service("learningFoldService")
public class LearningFoldServiceImpl implements LearningFoldService {

	@Autowired
	private LearningFoldDao learningFoldDao;

	@Override
	public List<LearningFold> findByAll() {
		return learningFoldDao.findAll();
	}

	@Override
	public List<LearningFold> findByStatus(String status) {
		return learningFoldDao.findByStatus("ACTIVE");
	}

	@Override
	public LearningFold findByType(String type) {
		return learningFoldDao.findByType(type);
	}

	@Override
	public LearningFold findById(Integer id) {
		return learningFoldDao.findById(id);
	}

	@Override
	public List<LearningFold> findByModule(String module) {
		return learningFoldDao.findByModule(module);
	}

	@Override
	public List<LearningFold> findBySection(String section) {
		return learningFoldDao.findBySection(section);
	}
	
	@Override
	public Page<LearningFold> findBySection(String section, Page<LearningFold> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<LearningFold> springPage = learningFoldDao.findBySection(section, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<LearningFold> findByModuleAndSection(String module, String section) {
		return learningFoldDao.findByModuleAndSection(module, section);
	}
	
	@Override
	public Page<LearningFold> findByModuleAndSection(String module, String section, Page<LearningFold> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<LearningFold> springPage = learningFoldDao.findByModuleAndSection(module, section, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<LearningFold> findByPage(Page<LearningFold> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<LearningFold> springPage = learningFoldDao.findByPage(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public LearningFold add(LearningFold learningFold) {
		// TODO Auto-generated method stub
		return learningFoldDao.save(learningFold);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != learningFoldDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}
}
