package com.easycms.hd.learning.form;

import lombok.Data;

@Data
public class ConvertForm {
	private Integer id;
	private String suffix;
	private String sourceFilePath;
	private String basePath;
}
