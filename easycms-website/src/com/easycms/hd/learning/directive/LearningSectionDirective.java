package com.easycms.hd.learning.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearningSectionService;
import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-22
 */
@Slf4j
public class LearningSectionDirective extends BaseDirective<LearningSection>  {
	
	@Autowired
	private LearningSectionService learningSectionService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected LearningSection field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			LearningSection learningSection = learningSectionService.findById(id);
			return learningSection;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearningSection> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<LearningSection> page,
			Map<String, Object> envParams) {
		page = learningSectionService.findByPage(page);
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearningSection> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
