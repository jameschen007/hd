package com.easycms.hd.learning.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-22
 */
public class LearningModuleDirective extends BaseDirective<Modules>  {

	@Autowired
	private ModulesService modulesService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Modules field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		// 查询ID信息
		if (id != null) {
			Modules module = modulesService.findById(id);
			return module;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<Modules> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Modules> page,
			Map<String, Object> envParams) {
		String section = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		page = modulesService.findByStatusAndSection("ACTIVE", section, page);
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<Modules> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
