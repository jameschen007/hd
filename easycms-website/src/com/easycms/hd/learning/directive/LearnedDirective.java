package com.easycms.hd.learning.directive;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.util.HtmlUtils;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearnedHistory;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.learning.service.LearnedHistoryService;
import com.easycms.hd.learning.service.LearningFileService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取参培人员学习列表
 * 
 * @author wz: 
 * @areate Date:2018 2 26 
 */
@Slf4j
public class LearnedDirective extends BaseDirective<LearnedHistory>  {
	
	@Autowired
	private LearnedHistoryService LearnedHistoryService;
	@Autowired
	private LearningFileService learningFileService;
	@Autowired
	private UserService userService;
	
	@Value("#{APP_SETTING['file_learning_path']}")
	private String fileLearningPath;
	
	@Value("#{APP_SETTING['file_learning_convert_path']}")
	private String fileLearningConvertPath;
	
	@Value("#{APP_SETTING['file_learning_cover_path']}")
	private String fileLearningCoverPath;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

//	@SuppressWarnings("rawtypes")
//	@Override
	protected LearnedHistory field(Map params, Map<String, Object> envParams) {
//		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
//		log.debug("[id] ==> " + id);
//		// 查询ID信息
//		if (id != null) {
//			LearnedHistory learning = LearnedHistoryService.findById(id);
//			if (null != learning) {
//				List<LearningFile> learningFiles = learningFileService.findFilesByLearningId(learning.getId());
//				if (null != learningFiles) {
//					learningFiles.stream().forEach(item -> {
//						if (CommonUtility.isNonEmpty(item.getConventerPath())) {
//							item.setConventerPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningConvertPath.replace("/", "*") + item.getConventerPath()));
//						}
//						if (CommonUtility.isNonEmpty(item.getPath())) {
//							item.setPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + item.getPath()));
//						}
//						if (CommonUtility.isNonEmpty(item.getCoverPath())) {
//							item.setCoverPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningCoverPath.replace("/", "*") + item.getCoverPath()));
//						}
//					});
////					learning.setLearningFiles(learningFiles);
//				}
//			}
////			return learning;
//		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearnedHistory> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<LearnedHistory> pager,
			Map<String, Object> envParams) {
		
//		User user =  ContextUtil.getCurrentLoginUser();
		
		LearnedHistory condition= new LearnedHistory();
		condition.setOrder("desc");
		
		condition.setSort("endDate");
		
		pager = LearnedHistoryService.findPage(condition, pager);

        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        Date t = Calendar.getInstance().getTime();
        System.out.println(f.format(t));
		List<LearnedHistory> list = pager.getDatas();
		list.stream().forEach(b->{
			User u = userService.findUserById(b.getCreateBy());
			if(u!=null){
				b.setUserName(u.getRealname());
			}
			String d = b.getDuration();
			if(d!=null && d.matches("\\d+")){
		        t.setTime(Long.valueOf(d));
				b.setDuration(f.format(t));
			}
		});

		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearnedHistory> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
