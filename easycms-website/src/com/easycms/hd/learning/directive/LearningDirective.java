package com.easycms.hd.learning.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.util.HtmlUtils;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.learning.service.LearningFileService;
import com.easycms.hd.learning.service.LearningService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-18 
 */
@Slf4j
public class LearningDirective extends BaseDirective<Learning>  {
	
	@Autowired
	private LearningService learningService;
	@Autowired
	private LearningFileService learningFileService;
	
	@Value("#{APP_SETTING['file_learning_path']}")
	private String fileLearningPath;
	
	@Value("#{APP_SETTING['file_learning_convert_path']}")
	private String fileLearningConvertPath;
	
	@Value("#{APP_SETTING['file_learning_cover_path']}")
	private String fileLearningCoverPath;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Learning field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			Learning learning = learningService.findById(id);
			if (null != learning) {
				List<LearningFile> learningFiles = learningFileService.findFilesByLearningId(learning.getId());
				if (null != learningFiles) {
					learningFiles.stream().forEach(item -> {
						if (CommonUtility.isNonEmpty(item.getConventerPath())) {
							item.setConventerPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningConvertPath.replace("/", "*") + item.getConventerPath()));
						}
						if (CommonUtility.isNonEmpty(item.getPath())) {
							item.setPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + item.getPath()));
						}
						if (CommonUtility.isNonEmpty(item.getCoverPath())) {
							item.setCoverPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningCoverPath.replace("/", "*") + item.getCoverPath()));
						}
					});
					learning.setLearningFiles(learningFiles);
				}
			}
			return learning;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<Learning> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Learning> page,
			Map<String, Object> envParams) {
		User user =  ContextUtil.getCurrentLoginUser();
		
		page = learningService.findByCreater(user.getId(), page);
		if (null != page.getDatas() && !page.getDatas().isEmpty()) {
			page.getDatas().stream().forEach(learning -> {
				if (null != learning) {
					List<LearningFile> learningFiles = learningFileService.findFilesByLearningId(learning.getId());
					if (null != learningFiles) {
						learningFiles.stream().forEach(item -> {
							if (CommonUtility.isNonEmpty(item.getConventerPath())) {
								item.setConventerPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningConvertPath.replace("/", "*") + item.getConventerPath()));
							}
							if (CommonUtility.isNonEmpty(item.getPath())) {
								item.setPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + item.getPath()));
							}
							if (CommonUtility.isNonEmpty(item.getCoverPath())) {
								item.setCoverPath(HtmlUtils.htmlEscape("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningCoverPath.replace("/", "*") + item.getCoverPath()));
							}
						});
						learning.setLearningFiles(learningFiles);
					}
				}
			});
		}
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<Learning> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
