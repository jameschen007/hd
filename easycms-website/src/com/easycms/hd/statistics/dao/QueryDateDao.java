package com.easycms.hd.statistics.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.statistics.domain.QueryDate;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface QueryDateDao extends Repository<QueryDate,Integer>,BasicDao<QueryDate> {
	@Modifying
	@Query("DELETE FROM QueryDate qd")
	int deleteAll();
}
