package com.easycms.hd.statistics.dao;

import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.statistics.domain.StatisticsQcSituationBean;
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface StatisticsQcSituationDao extends BasicDao<StatisticsQcSituationBean> ,Repository<StatisticsQcSituationBean,Integer>{
	
//
//	List<T> findAll(Specification<com.easycms.core.util.Page<StatisticsPlan>> specification);
	
	
}
