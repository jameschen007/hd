package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsWitnessSituationBean;

public interface StatisticsWitnessSituationService {

	public List<StatisticsWitnessSituationBean> findAll(StatisticsWitnessSituationBean condition);


	public Page<StatisticsWitnessSituationBean> findPage(StatisticsWitnessSituationBean condition,Page<StatisticsWitnessSituationBean> page) ;
}
