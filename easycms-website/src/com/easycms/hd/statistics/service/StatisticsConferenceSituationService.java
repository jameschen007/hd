package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsConferenceSituationBean;
import com.easycms.hd.statistics.domain.StatisticsPlan;

public interface StatisticsConferenceSituationService {

	public List<StatisticsConferenceSituationBean> findAll(StatisticsConferenceSituationBean condition);


	public Page<StatisticsConferenceSituationBean> findPage(StatisticsConferenceSituationBean condition,Page<StatisticsConferenceSituationBean> page) ;
}
