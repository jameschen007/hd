package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsPlanSituationBean;

public interface StatisticsPlanSituationService {

	public List<StatisticsPlanSituationBean> findAll(StatisticsPlanSituationBean condition);


	public Page<StatisticsPlanSituationBean> findPage(StatisticsPlanSituationBean condition,Page<StatisticsPlanSituationBean> page) ;
}
