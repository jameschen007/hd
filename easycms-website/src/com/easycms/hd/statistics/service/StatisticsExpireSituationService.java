package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsExpireSituationBean;

public interface StatisticsExpireSituationService {

	public List<StatisticsExpireSituationBean> findAll(StatisticsExpireSituationBean condition);


	public Page<StatisticsExpireSituationBean> findPage(StatisticsExpireSituationBean condition,Page<StatisticsExpireSituationBean> page) ;
}
