package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsPlanDao;
import com.easycms.hd.statistics.domain.StatisticsPlan;
import com.easycms.hd.statistics.service.StatisticsPlanService;

@Service
public class StatisticsPlanServiceImpl implements StatisticsPlanService  {
	@Autowired
	private StatisticsPlanDao statisticsPlanDao;
	
	@Override
	public List<StatisticsPlan> findAll(StatisticsPlan condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsPlan> findPage(StatisticsPlan condition,Page<StatisticsPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsPlan> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
