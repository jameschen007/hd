package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsExpireSituationDao;
import com.easycms.hd.statistics.domain.StatisticsExpireSituationBean;
import com.easycms.hd.statistics.service.StatisticsExpireSituationService;

@Service
public class StatisticsExpireSituationServiceImpl implements StatisticsExpireSituationService  {
	@Autowired
	private StatisticsExpireSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsExpireSituationBean> findAll(StatisticsExpireSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsExpireSituationBean> findPage(StatisticsExpireSituationBean condition,Page<StatisticsExpireSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsExpireSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
