package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsWitnessSituationDao;
import com.easycms.hd.statistics.domain.StatisticsWitnessSituationBean;
import com.easycms.hd.statistics.service.StatisticsWitnessSituationService;

@Service
public class StatisticsWitnessSituationServiceImpl implements StatisticsWitnessSituationService  {
	@Autowired
	private StatisticsWitnessSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsWitnessSituationBean> findAll(StatisticsWitnessSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsWitnessSituationBean> findPage(StatisticsWitnessSituationBean condition,Page<StatisticsWitnessSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsWitnessSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
