package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsNotificationSituationDao;
import com.easycms.hd.statistics.domain.StatisticsNotificationSituationBean;
import com.easycms.hd.statistics.service.StatisticsNotificationSituationService;

@Service
public class StatisticsNotificationSituationServiceImpl implements StatisticsNotificationSituationService  {
	@Autowired
	private StatisticsNotificationSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsNotificationSituationBean> findAll(StatisticsNotificationSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsNotificationSituationBean> findPage(StatisticsNotificationSituationBean condition,Page<StatisticsNotificationSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsNotificationSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
