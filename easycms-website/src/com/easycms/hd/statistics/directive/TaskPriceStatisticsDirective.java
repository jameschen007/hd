package com.easycms.hd.statistics.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.price.domain.Price;
import com.easycms.hd.price.service.PriceService;
import com.easycms.hd.statistics.domain.TaskStatistics;
import com.easycms.hd.variable.service.VariableSetService;

public class TaskPriceStatisticsDirective extends BaseDirective<TaskStatistics>  {
	private static final Logger logger = Logger.getLogger(TaskPriceStatisticsDirective.class);
	
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private PriceService priceService;
	@Autowired
	private VariableSetService variableSetService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected TaskStatistics field(Map params, Map<String, Object> envParams) {
		String id = FreemarkerTemplateUtility.getStringValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		String[] ids = new String[]{id};
		List<RollingPlan> rollingPlanList = rollingPlanService.findByConsteamsAndIsend(ids, 2);
		
		Double cost = 0d;
		
		for (RollingPlan rp : rollingPlanList){
			if (null != rp.getWorkpoint()){
				Price price = priceService.getPricetypeExist(rp.getSpeciality(), "DZ");
				if (null != price){
					cost += rp.getWorkpoint() * price.getUnitprice();
				}
			}
			if (null != rp.getQualitynum()){
				Price price = priceService.getPricetypeExist(rp.getSpeciality(), "GCL");
				if (null != price){
					cost += rp.getQualitynum() * price.getUnitprice();
				}
			}
			if (null != rp.getWorktime()){
				Price price = priceService.getPricetypeExist(rp.getSpeciality(), "GS");
				if (null != price){
					cost += rp.getWorktime() * price.getUnitprice();
				}
			}
		}
		
		TaskStatistics taskStatistics = new TaskStatistics();
		taskStatistics.setCost(cost);
		
		logger.info(taskStatistics);
		return taskStatistics;
	}
	
	@SuppressWarnings({ "rawtypes"})
	@Override
	protected List<TaskStatistics> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<TaskStatistics> pager,
			Map<String, Object> envParams) {
		
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<TaskStatistics> tree(Map params, Map<String, Object> envParams) {
		return null;
	}
}
