package com.easycms.hd.statistics.directive;

import java.util.List;
import java.util.Map;

import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.statistics.domain.StatisticsPlan;
import com.easycms.hd.statistics.service.StatisticsPlanService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import net.sf.json.JSONObject;

public class StatisticsPlanDirective extends BaseDirective<StatisticsPlan> {

	@Autowired
	private RollingQuestionService questionService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private QuestionApiService questionApiService;
	@Autowired
	private StatisticsPlanService statisticsPlanService;
	
	

	private Logger logger = Logger.getLogger(StatisticsPlanDirective.class);

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected StatisticsPlan field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<StatisticsPlan> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<StatisticsPlan> pager,
			Map<String, Object> envParams) {
		
		String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
		String rollingType = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search rollingType : " + rollingType);
		
		StatisticsPlan condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(StatisticsPlan.class, filter,"yyyy-MM-dd");
//			JSONObject json = JSONObject.fromObject(filter);
//			if(json!=null && json.get("departmentId")!=null && CommonUtility.isNonEmpty(json.get("departmentId").toString()) && !"null".equals(json.get("departmentId").toString().trim())){
//				Department dept = departmentService.findById(Integer.valueOf(json.get("departmentId").toString()));
//				condition.setDepartment(dept);
//			}
		}else{
			condition = new StatisticsPlan();
		}
		condition.setType(rollingType);
		
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		// 查询列表
		if (pageable) {
			if (CommonUtility.isNonEmpty(search)){
				try {
					condition.setCreatedOn(DateUtils.parseDate(search));
				} catch (DateParseException e) {
					e.printStackTrace();
				}
				pager = statisticsPlanService.findPage(condition, pager);
			} else {
				pager = statisticsPlanService.findPage(condition, pager);
			}
			return pager.getDatas();
		} else {
			List<StatisticsPlan> datas = statisticsPlanService.findAll(condition);
			logger.debug("==> StatisticsPlan length [" + datas.size() + "]");
			return datas;
		}
	}

	@Override
	protected List<StatisticsPlan> tree(Map params, Map<String, Object> envParams) {
		return null;
	}


}
