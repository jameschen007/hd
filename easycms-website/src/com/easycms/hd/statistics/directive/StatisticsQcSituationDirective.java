package com.easycms.hd.statistics.directive;

import java.util.List;
import java.util.Map;

import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsQcSituationBean;
import com.easycms.hd.statistics.service.StatisticsQcSituationService;

public class StatisticsQcSituationDirective extends BaseDirective<StatisticsQcSituationBean> {

	@Autowired
	private StatisticsQcSituationService statisticsPlanService;
	
	

	private Logger logger = Logger.getLogger(StatisticsQcSituationDirective.class);

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected StatisticsQcSituationBean field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<StatisticsQcSituationBean> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<StatisticsQcSituationBean> pager,
			Map<String, Object> envParams) {
		
		String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
		String rollingType = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search rollingType : " + rollingType);
		
		StatisticsQcSituationBean condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(StatisticsQcSituationBean.class, filter,"yyyy-MM-dd");
//			JSONObject json = JSONObject.fromObject(filter);
//			if(json!=null && json.get("departmentId")!=null && CommonUtility.isNonEmpty(json.get("departmentId").toString()) && !"null".equals(json.get("departmentId").toString().trim())){
//				Department dept = departmentService.findById(Integer.valueOf(json.get("departmentId").toString()));
//				condition.setDepartment(dept);
//			}
		}else{
			condition = new StatisticsQcSituationBean();
		}
		if(rollingType!=null && rollingType.matches("\\d")){
			condition.setType(Integer.valueOf(rollingType));
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		// 查询列表
		if (pageable) {
			if (CommonUtility.isNonEmpty(search)){
				try {
					condition.setCreatedOn(DateUtils.parseDate(search));
				} catch (DateParseException e) {
					e.printStackTrace();
				}
				pager = statisticsPlanService.findPage(condition, pager);
			} else {
				pager = statisticsPlanService.findPage(condition, pager);
			}
			return pager.getDatas();
		} else {
			List<StatisticsQcSituationBean> datas = statisticsPlanService.findAll(condition);
			logger.debug("==> StatisticsQcSituationBean length [" + datas.size() + "]");
			return datas;
		}
	}

	@Override
	protected List<StatisticsQcSituationBean> tree(Map params, Map<String, Object> envParams) {
		return null;
	}


}
