package com.easycms.hd.statistics.directive;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.WebUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.service.StatisticsRollingPlanService;
import com.easycms.hd.statistics.util.StatisticsUtil;
import com.easycms.hd.user.service.HDUserService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

public class TaskStatusStatisticsDirective extends BaseDirective<Long> {
  private static final Logger logger = Logger.getLogger(TaskStatusStatisticsDirective.class);
  private static String WELD_DISTINGUISH = "welddistinguish";
  private static String HYSTERESIS = "hysteresis";

  @Autowired
  private VariableSetService variableSetService;
  @Autowired
  private DepartmentService departmentService;
  @Autowired
  private UserService userService;
  @Autowired
  private HDUserService hdUserService;
  @Autowired
  private StatisticsRollingPlanService statisticsRollingPlanService;

  @SuppressWarnings("rawtypes")
  @Override
  protected Integer count(Map params, Map<String, Object> envParams) {
    return null;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected Long field(Map params, Map<String, Object> envParams) {
    String isByUser = FreemarkerTemplateUtility.getStringValueFromParams(params, "isByUser");
    if ("true".equalsIgnoreCase(isByUser)) {
      return getAllStasticsByUser(params);
    }
    return getAllStastics(params);
  }

  @SuppressWarnings("rawtypes")
  private Long getAllStastics(Map params) {
    Integer status = FreemarkerTemplateUtility.getIntValueFromParams(params, "status");
    String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
    String date = FreemarkerTemplateUtility.getStringValueFromParams(params, "taskDate");
    String category = FreemarkerTemplateUtility.getStringValueFromParams(params, "category");
    logger.debug("[params] ==> [status]:" + status + " [date]:" + date + " [type]:" + type
        + " [category]:" + category);

    if (null == date || date.equals("")) {
      date = StatisticsUtil.getDate(0);
    }

    if (null == category || category.equals("")) {
      category = "dateCurrent";
    }

    if (null == status || null == type) {
      return null;
    }

    Integer hysteresis = 2;
    try {
      hysteresis = Integer.parseInt(variableSetService.findValueByKey("hysteresis", HYSTERESIS));
    } catch (NumberFormatException e) {
      e.printStackTrace();
      hysteresis = 2;
    }

    if (type.equals("weldHK")) {
      type = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
    } else if (type.equals("weldZJ")) {
      type = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);
    } else {
      return null;
    }
    Long result = 0l;

    return adminData(status, type, date, category, hysteresis, result);
  }

  @SuppressWarnings("rawtypes")
  private Long getAllStasticsByUser(Map params) {

    Integer status = FreemarkerTemplateUtility.getIntValueFromParams(params, "status");
    String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "type");
    String date = FreemarkerTemplateUtility.getStringValueFromParams(params, "taskDate");
    String category = FreemarkerTemplateUtility.getStringValueFromParams(params, "category");
    String id = FreemarkerTemplateUtility.getStringValueFromParams(params, "id");

    User user = null;
    if (id != null && !"".equals(id)) {
      user = userService.findUserById(Integer.parseInt(id));
    } else {
      user = (User) WebUtility.getSession().getAttribute("user");
    }

    User loginUser = userService.findUserById(user.getId());


    if (null == date || date.equals("")) {
      date = StatisticsUtil.getDate(0);
    }

    if (null == category || category.equals("")) {
      category = "dateCurrent";
    }

    if (null == status || null == type) {
      return null;
    }

    Integer hysteresis = 2;
    try {
      hysteresis = Integer.parseInt(variableSetService.findValueByKey("hysteresis", HYSTERESIS));
    } catch (NumberFormatException e) {
      e.printStackTrace();
      hysteresis = 2;
    }

    if (type.equals("weldHK")) {
      type = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
    } else if (type.equals("weldZJ")) {
      type = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);
    } else {
      return null;
    }
    Long result = 0l;

    // 如果是默认管理员则查全部
    if (user.isDefaultAdmin()) {
      return adminData(status, type, date, category, hysteresis, result);
      // 如果是承包人则按用户查找
    } else {
      return userData(status, type, date, category, hysteresis, result, loginUser);
    }

  }

  private Long adminData(Integer status, String type, String date, String category,
      Integer hysteresis, Long result) {
    if (category.equals("dateAfter") || category.equals("dateCurrent")
        || category.equals("dateBefore")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusDayStatus0(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0(type, new Date());
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus4(type, date);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusDayStatus5(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus3(type, date);
      }
    } else if (category.equals("dateWeek")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus0(type, new
        // Date());
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0(type, new Date());
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusWeekStatus1(type, new Date());
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusWeekStatus2(type, new Date());
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus6(type, new Date(),
                hysteresis);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusWeekStatus4(type, new Date());
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus5(type, new
        // Date());
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusWeekStatus3(type, new Date());
      }
    } else if (category.equals("dateMonth")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus0(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0(type, new Date());
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus6(type, new Date(),
                hysteresis);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus4(type, date);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus5(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus3(type, date);
      }
    } else if (category.equals("dateYear")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus0(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0(type, new Date());
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus6(type, new Date(),
                hysteresis);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus4(type, date);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus5(type,
        // date);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus3(type, date);
      }
    } else {
      return null;
    }

    logger.info(result);
    return result;
  }

  private Long userData(Integer status, String type, String date, String category,
      Integer hysteresis, Long result, User user) {

    if (hdUserService.isClasz(user.getId())) {
      return getStatusByClasz(status, type, date, category, hysteresis, result, user);
    } else if (hdUserService.isGroup(user.getId())) {
      return getStatusByGroup(status, type, date, category, hysteresis, result, user);
    } else {
      return getStatusByPlanner(status, type, date, category, hysteresis, result,user);
    }
  }

  private Long getStatusByGroup(Integer status, String type, String date, String category,
      Integer hysteresis, Long result, User user) {
    String[] ids = {user.getId() + ""};
    if (category.equals("dateAfter") || category.equals("dateCurrent")
        || category.equals("dateBefore")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusDayStatus0(type, date,
        // ids);
        // result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByGroup(
        // type, new Date(), ids);
        result = 0l;
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus1ByGroup(type, date, ids);
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus2ByGroup(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6ByGroup(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus4ByGroup(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusDayStatus5(type, date,
        // ids);
        result =
            statisticsRollingPlanService.getRollingPlanStatusAllStatus5ByGroup(type, new Date(),
                ids);
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus3ByGroup(type, date, ids);
      }

    } else if (category.equals("dateWeek")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus0(type, new
        // Date(), ids);
        // result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByGroup(
        // type, new Date(), ids);
        result = 0l;
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus1ByGroup(type, new Date(),
                ids);
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus2ByGroup(type, new Date(),
                ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6ByGroup(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus4ByGroup(type, new Date(),
                ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus5(type, new
        // Date(), ids);
        result =
            statisticsRollingPlanService.getRollingPlanStatusAllStatus5ByGroup(type, new Date(),
                ids);
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus3ByGroup(type, new Date(),
                ids);
      }
    } else if (category.equals("dateMonth")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus0(type,
        // date, ids);
        // result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByGroup(
        // type, new Date(), ids);
        result = 0l;
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus1ByGroup(type, date, ids);
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus2ByGroup(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6ByGroup(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus4ByGroup(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus5(type,
        // date, ids);
        result =
            statisticsRollingPlanService.getRollingPlanStatusAllStatus5ByGroup(type, new Date(),
                ids);
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus3ByGroup(type, date, ids);
      }
    } else if (category.equals("dateYear")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus0(type,
        // date, ids);
        // result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByGroup(
        // type, new Date(), ids);
        result = 0l;
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus1ByGroup(type, date, ids);
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus2ByGroup(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6ByGroup(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus4ByGroup(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus5(type,
        // date, ids);
        result =
            statisticsRollingPlanService.getRollingPlanStatusAllStatus5ByGroup(type, new Date(),
                ids);
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus3ByGroup(type, date, ids);
      }
    } else {
      return null;
    }

    logger.info(result);
    return result;
  }

  private Long getStatusByClasz(Integer status, String type, String date, String category,
      Integer hysteresis, Long result, User user) {
    Department dep = user.getDepartment();
    String[] ids = new String[1];
    if (dep != null) {
      ids[0] = dep.getId() + "";
    } else {
      ids[0] = "0";
    }
    if (category.equals("dateAfter") || category.equals("dateCurrent")
        || category.equals("dateBefore")) {
      if (status == 0) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByClasz(type, ids);
      } else if (status == 1) {
        
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus1(type, date, ids);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus2(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus4ByClasz(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusDayStatus5(type, date,
        // ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date(), ids);
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus3(type, date, ids);
      }

    } else if (category.equals("dateWeek")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus0(type, new
        // Date(), ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByClasz(type, ids);
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus1(type, new Date(), ids);
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus2(type, new Date(), ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
//        result =
//            statisticsRollingPlanService.getRollingPlanStatusWeekStatus4(type, new Date(), ids);
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus4ByClasz(type, new Date(), ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus5(type, new
        // Date(), ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date(), ids);
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus3(type, new Date(), ids);
      }
    } else if (category.equals("dateMonth")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus0(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByClasz(type, ids);
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus1(type, date, ids);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus2(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        
//        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus4(type, date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus4ByClasz(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus5(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date(), ids);
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus3(type, date, ids);
      }
    } else if (category.equals("dateYear")) {
      if (status == 0) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus0(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByClasz(type, ids);
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus1(type, date, ids);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus2(type, date, ids);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus4ByClasz(type, date, ids);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus5(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date(), ids);
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus3(type, date, ids);
      }
    } else {
      return null;
    }

    logger.info(result);
    return result;
  }

  private Long getStatusByPlanner(Integer status, String type, String date, String category,
      Integer hysteresis, Long result, User user) {
    Department dep = user.getDepartment();
    String[] ids = new String[1];
    if (dep != null) {
      ids[0] = dep.getId() + "";
    } else {
      ids[0] = "0";
    }
    if (category.equals("dateAfter") || category.equals("dateCurrent")
        || category.equals("dateBefore")) {
      if (status == 0) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByPlanner(type);
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusDayStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus4(type, date);
      } else if (status == 5) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusDayStatus3(type, date);
      }

    } else if (category.equals("dateWeek")) {
      if (status == 0) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByPlanner(type);
      } else if (status == 1) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus1(type, new Date());
      } else if (status == 2) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus2(type, new Date());
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus4(type, new Date());
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusWeekStatus5(type, new
        // Date(), ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusWeekStatus3(type, new Date());
      }
    } else if (category.equals("dateMonth")) {
      if (status == 0) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByPlanner(type);
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusMonthStatus6(type, new Date(),
                hysteresis, ids);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus4(type, date);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusMonthStatus5(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusMonthStatus3(type, date);
      }
    } else if (category.equals("dateYear")) {
      if (status == 0) {
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus0ByPlanner(type);
      } else if (status == 1) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus1(type, date);
      } else if (status == 2) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus2(type, date);
      } else if (status == 6) {
        result =
            statisticsRollingPlanService.getRollingPlanStatusYearStatus6(type, new Date(),
                hysteresis);
      } else if (status == 4) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus4(type, date);
      } else if (status == 5) {
        // result =
        // statisticsRollingPlanService.getRollingPlanStatusYearStatus5(type,
        // date, ids);
        result = statisticsRollingPlanService.getRollingPlanStatusAllStatus5(type, new Date());
      } else if (status == 3) {
        result = statisticsRollingPlanService.getRollingPlanStatusYearStatus3(type, date);
      }
    } else {
      return null;
    }

    logger.info(result);
    return result;
  }

  @SuppressWarnings({"rawtypes"})
  @Override
  protected List<Long> list(Map params, String filter, String order, String sort, boolean pageable,
      Page<Long> pager, Map<String, Object> envParams) {

    return null;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected List<Long> tree(Map params, Map<String, Object> envParams) {
    return null;
  }
}
