package com.easycms.hd.statistics.form;

import com.easycms.core.form.BasicForm;

public class TaskStatisticsForm extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = -8229451924733268211L;
	private String consteam;
	private String consendman;
	private int complate;
	private int total;
	private int id;
	private String category;
	
	private String customRole;
	
	public String getConsteam() {
		return consteam;
	}
	public void setConsteam(String consteam) {
		this.consteam = consteam;
	}
	public String getConsendman() {
		return consendman;
	}
	public void setConsendman(String consendman) {
		this.consendman = consendman;
	}
	public int getComplate() {
		return complate;
	}
	public void setComplate(int complate) {
		this.complate = complate;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCustomRole() {
		return customRole;
	}
	public void setCustomRole(String customRole) {
		this.customRole = customRole;
	}
}
