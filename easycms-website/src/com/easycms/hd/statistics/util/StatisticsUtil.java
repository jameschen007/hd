package com.easycms.hd.statistics.util;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.easycms.common.util.WebUtility;
import com.easycms.hd.plan.domain.TaskHyperbolaResult;

public class StatisticsUtil {
	private static final Logger logger = Logger.getLogger(StatisticsUtil.class);
	
	@SuppressWarnings("static-access")
	public static String getDate(int amount){
		Date date=new Date();
		 Calendar calendar = new GregorianCalendar();
		 calendar.setTime(date);
		 calendar.add(calendar.DATE,amount);
		 date=calendar.getTime(); 
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		 String dateString = formatter.format(date);
		 
		 return dateString;
	}
	
	public static String getWeek(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		int weekday = c.get(7)-1;
		c.add(5,-weekday);
		String start = formatter.format(c.getTime());
		c.add(5,6);
		String end = formatter.format(c.getTime());
		
		 return start + " ~ " + end;
	}
	
	public static String getMonth(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		
		String month = formatter.format(new Date());
		
		return month;
	}
	
	public static String getYear(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		
		String year = formatter.format(new Date());
		
		return year;
	}
	
	public static int datesOfMonth(String yearAndMonth){
		if (!yearAndMonth.contains("-")){
			return 0;
		} else {
			String[] s = yearAndMonth.split("-");
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, Integer.parseInt(s[0]));
			cal.set(Calendar.MONTH, Integer.parseInt(s[1]) - 1);
			int dateOfMonth = cal.getActualMaximum(Calendar.DATE);
			
			return dateOfMonth;
		}
	}
	
	public static List<TaskHyperbolaResult> objectToTaskHyperbolaResult(List<Object[]> list, int dateOfMonth){
		List<TaskHyperbolaResult> taskList = new ArrayList<TaskHyperbolaResult>();
		for (int i = 0; i < list.size(); i++) {
			if (i == dateOfMonth){
				break;
			}
			
			Object[] objects = list.get(i);
			TaskHyperbolaResult task = new TaskHyperbolaResult();
			for (int j = 0; j < objects.length; j++) {
				task = arrayToBean(j , task, objects[j]);
			}
			taskList.add(task);
		}
			
		return taskList;
	}
	
	public static TaskHyperbolaResult arrayToBean(int i, TaskHyperbolaResult task, Object obj){
		if (0 == i){
			String day;
			try {
				day = String.valueOf((Integer)obj);
			} catch (ClassCastException e) {
				logger.info("Day result : " + e.getMessage());
				Timestamp ts = (Timestamp)obj;
				day = WebUtility.convertTimestampToString(ts, "yyyy-MM-dd");
			}
			task.setDay(day);
		} else if (1 == i){
			task.setAmount((BigInteger)obj);
		}
		
		return task;
	}
	
	public static List<String> datePeriodList(Date startTime, Date endTime) {
		List<String> dateList = new ArrayList<String>();
		
		Calendar start = Calendar.getInstance();
		start.setTime(startTime);
		Long startTimeLoog = start.getTimeInMillis();

		Calendar end = Calendar.getInstance();
		end.setTime(endTime);
		Long endTimeLoog = end.getTimeInMillis();

		Long oneDay = 1000 * 60 * 60 * 24l;

		Long time = startTimeLoog;
		while (time <= endTimeLoog) {
			Date d = new Date(time);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			dateList.add(df.format(d));
			time += oneDay;
		}
		
		return dateList;
	}
}
