package com.easycms.hd.statistics.domain;

import java.io.Serializable;

public class TaskStatistics implements Serializable{
	private static final long serialVersionUID = 2819273236658867390L;
	
	private String type;
	private String consteam;
	private String consendman;
	private Long complate = 0l;
	private Long surplus = 0l;
	private Long total = 0l;
	private Double percent = 0d;
	private Double cost = 0d;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getConsteam() {
		return consteam;
	}
	public void setConsteam(String consteam) {
		this.consteam = consteam;
	}
	public String getConsendman() {
		return consendman;
	}
	public void setConsendman(String consendman) {
		this.consendman = consendman;
	}
	public Long getComplate() {
		return complate;
	}
	public void setComplate(Long complate) {
		this.complate = complate;
	}
	public Long getSurplus() {
		return surplus;
	}
	public void setSurplus(Long surplus) {
		this.surplus = surplus;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public Double getPercent() {
		return percent;
	}
	public void setPercent(Double percent) {
		this.percent = percent;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "TaskStatistics [type=" + type + ", consteam=" + consteam
				+ ", consendman=" + consendman + ", complate=" + complate
				+ ", surplus=" + surplus + ", total=" + total + ", percent="
				+ percent + ", cost=" + cost + "]";
	}
}
