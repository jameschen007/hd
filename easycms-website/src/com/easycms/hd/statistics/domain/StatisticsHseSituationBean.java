package com.easycms.hd.statistics.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;

@Entity
@Table(name = "statistics_hse_situation")
@Data
public class StatisticsHseSituationBean extends BasicForm implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	/** 创建时间 */
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "total")
	private Integer total;

	//模块类型：管道计划GDJH
	@Column(name = "problem_title")
	private String problemTitle;

	@Column(name = "isNew")
	private Integer isNew;

	@Column(name = "isNeedRenovate")
	private Integer isNeedRenovate;

	@Column(name = "isNeedCheck")
	private Integer isNeedCheck;

	@Column(name = "isRenovateAgain")
	private Integer isRenovateAgain;

	@Column(name = "isCheckAgain")
	private Integer isCheckAgain;

	@Column(name = "isFinished")
	private Integer isFinished;
}
