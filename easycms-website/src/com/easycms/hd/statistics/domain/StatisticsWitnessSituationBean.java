package com.easycms.hd.statistics.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;

@Entity
@Table(name = "statistics_witness_situation")
@Data
public class StatisticsWitnessSituationBean extends BasicForm implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	/** 创建时间 */
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "total")
	private Integer total;

	@Column(name = "no_assign_team")
	private Integer noAssignTeam;
	@Column(name = "assigned_team")
	private Integer assignedTeam;
	
	
	

	@Column(name = "no_assign_witess")
	private Integer noAssignWitess;
	@Column(name = "assigned_witness")
	private Integer assignedWitness;
	
	
	

	//模块类型：管道计划GDJH
	private String type;
}
