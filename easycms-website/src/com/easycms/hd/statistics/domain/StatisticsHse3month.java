package com.easycms.hd.statistics.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * -- 连续三个月(90天)安全
 -- 隐患总数　待整改数　超期未整改
 */
@Entity
@Table(name="statistics_hse3month")
public class StatisticsHse3month implements Serializable {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "deptName")
	private String deptName;
	@Column(name = "total")
	private Integer total;
	@Column(name = "waitingModify")
	private Integer waitingModify;
	@Column(name = "delay")
	private Integer delay;

	@Column(name = "day90ago")
	private Date day90ago;

	@Column(name = "responsible_dept")
	private Integer deptId;
	@Transient
	private String day90agoTips ;
	
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getWaitingModify() {
		return waitingModify;
	}
	public void setWaitingModify(Integer waitingModify) {
		this.waitingModify = waitingModify;
	}
	public Integer getDelay() {
		return delay;
	}
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Date getDay90ago() {
		return day90ago;
	}

	public void setDay90ago(Date day90ago) {
		this.day90ago = day90ago;
	}

	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getDay90agoTips() {
		return day90agoTips;
	}
	public void setDay90agoTips(String day90agoTips) {
		this.day90agoTips = day90agoTips;
	}
	
	
}
