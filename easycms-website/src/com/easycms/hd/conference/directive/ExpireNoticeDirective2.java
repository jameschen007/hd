package com.easycms.hd.conference.directive;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.conference.service.impl.EnpowerExpireNoticeServiceImpl;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-11-09 
 */
@Slf4j
public class ExpireNoticeDirective2 extends BaseDirective<EnpowerExpireNotice>  {
	private static Logger logger = Logger.getLogger(ExpireNoticeDirective2.class);
	@Autowired
	private EnpowerExpireNoticeServiceImpl enpowerExpireNoticeService;
	@Autowired
	private UserService userService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected EnpowerExpireNotice field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			EnpowerExpireNotice expireNotice = enpowerExpireNoticeService.findById(id);//.findById(id);
			return expireNotice;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<EnpowerExpireNotice> list(Map params, String filter, String order, String sort, boolean pageable,
			Page<EnpowerExpireNotice> pager, Map<String, Object> envParams) {
		
		String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
		
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search EnpowerExpireNotice : " + search);
		
		EnpowerExpireNotice condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(EnpowerExpireNotice.class, filter,"yyyy-MM-dd");
		}else{
			condition = new EnpowerExpireNotice();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		// 查询列表
		Map<String,Object> additionalCondition = new HashMap<String,Object>();
		pager = enpowerExpireNoticeService.findByPage(condition, additionalCondition, pager);
		
		List<EnpowerExpireNotice> expireNoticeResults = pager.getDatas();
		if(expireNoticeResults!=null){

			List<EnpowerExpireNotice> expireNoticeResults2 = expireNoticeResults.stream().map(expireNotice -> {
				if(CommonUtility.isNonEmpty(expireNotice.getDrafter())){
					User u = userService.findUserEnpowerId(expireNotice.getDrafter());
					if(u!=null){
						expireNotice.setDrafter(u.getRealname());
					}
				}

				if(CommonUtility.isNonEmpty(expireNotice.getSigner())){
					User u = userService.findUserEnpowerId(expireNotice.getSigner());
					if(u!=null){
						expireNotice.setSigner(u.getRealname());
					}
				}
				return expireNotice;
				
			}).collect(Collectors.toList());
			return expireNoticeResults2;
		}

		
		
		return pager.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<EnpowerExpireNotice> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
