package com.easycms.hd.conference.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycms.hd.api.response.CommonMapResult;

public enum ConferenceType {
	/**
	 * 已发送的会议
	 */
	SEND("发送"),
	/**
	 * 保存为草稿的会议
	 */
	DRAFT("草稿");
	
	private String name;
	ConferenceType(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    /**
     * enum lookup map
     */
    private static final Map<String, String> lookup = new HashMap<String, String>();
    /**
     * enum lookup list
     */
    private static final List<CommonMapResult> mapResult = new ArrayList<CommonMapResult>();

    static {
        for (ConferenceType s : EnumSet.allOf(ConferenceType.class)) {
            lookup.put(s.name(), s.getName());
            CommonMapResult commonMapResult = new CommonMapResult();
            commonMapResult.setKey(s.name());
            commonMapResult.setValue(s.getName());
            mapResult.add(commonMapResult);
        }
    }

    public static  Map<String, String> getMap(){
        return lookup;
    }
    
    public static  List<CommonMapResult> getList(){
    	return mapResult;
    }
}
