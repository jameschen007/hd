package com.easycms.hd.conference.enums;

public enum NotificationConfirmType {
	/**
	 * 标记已收到反馈
	 */
	CONFIRM
}
