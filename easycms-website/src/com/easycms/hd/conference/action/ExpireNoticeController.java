package com.easycms.hd.conference.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.function.EnpowerExpireNoticeFunction;
import com.easycms.hd.api.response.EnpowerExpireNoticeResult;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.service.impl.EnpowerExpireNoticeServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/baseservice/expire_notice")
@Slf4j
public class ExpireNoticeController {

	@Autowired
	private EnpowerExpireNoticeServiceImpl enpowerExpireNoticeService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") ExpireNotice form) throws Exception {
		String func = form.getFunc();
		log.debug("[func] = " + func + " : " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/expireNotice/list_expireNotice");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") ExpireNotice form) {
		log.debug("==> Show ExpireNotice data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/expireNotice/data/data_json_expireNotice");
		return returnString;
	}

	/**
	 * 查看失效文件信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") ExpireNotice form) {
		String returnString = ForwardUtility.forwardAdminView("/expireNotice/modal_expireNotice_view");
		
		Integer id = form.getId();
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			EnpowerExpireNotice expireNotice = enpowerExpireNoticeService.findById(id);//.findById(id);
			
			EnpowerExpireNoticeResult n = new EnpowerExpireNoticeFunction().translate(expireNotice,true);
			request.setAttribute("item", n);
		}
		

		return returnString;
	}
}
