package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.NotificationFile;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface NotificationFileDao extends Repository<NotificationFile,Integer>,BasicDao<NotificationFile>{
	@Query("From NotificationFile nf where nf.notificationid = ?1")
	List<NotificationFile> findByNotificationId(Integer id);
	
	@Modifying
	@Query("DELETE FROM NotificationFile nf where nf.id IN (?1)")
	int batchRemove(Integer[] ids);
	
	@Modifying
	@Query("DELETE FROM NotificationFile nf where nf.notificationid IN (?1)")
	int batchRemoveNotification(Integer[] ids);

	@Query("From NotificationFile nf where nf.notificationid IN (?1)")
	List<NotificationFile> findFilesByNotificationIds(Integer[] ids);

	@Modifying
	@Query("UPDATE NotificationFile nf SET nf.notificationid = ?2 WHERE nf.id IN (?1)")
	int batchUpdateNotification(Integer[] ids, Integer notificationId);
}
