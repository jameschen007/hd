package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.conference.domain.ExpireNotice;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface EnpowerExpireNoticeDao extends Repository<EnpowerExpireNotice,Integer>,BasicDao<EnpowerExpireNotice>{
	@Query("FROM EnpowerExpireNotice en ORDER BY en.releaseDate desc")
	Page<ExpireNotice> findByPage(Pageable pageable);
	
	List<EnpowerExpireNotice> findByCancCode(String CancCode);
}
