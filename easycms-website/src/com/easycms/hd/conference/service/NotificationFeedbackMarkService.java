package com.easycms.hd.conference.service;

import java.util.Date;

import com.easycms.hd.conference.domain.NotificationFeedbackMark;

public interface NotificationFeedbackMarkService {

	NotificationFeedbackMark add(NotificationFeedbackMark mark);

	NotificationFeedbackMark findByNotificationIdAndUserId(Integer notificationId, Integer userId);

	int markRead(Integer markId, Date markTime);
	
	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveNotification(Integer[] ids);
}
