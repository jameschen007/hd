package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.ConferenceFile;

public interface ConferenceFileService {
	ConferenceFile add(ConferenceFile pfile);
	
	ConferenceFile findById(Integer id);
	
	List<ConferenceFile> findFilesByConferenceId(Integer id);

	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveConference(Integer[] ids);
	
	boolean batchUpdateConference(Integer[] ids, Integer conferenceId);

	List<ConferenceFile> findFilesByConferenceIds(Integer[] ids);
}
