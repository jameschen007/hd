package com.easycms.hd.conference.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.JpaMethod;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.conference.dao.EnpowerExpireNoticeDao;
import com.easycms.hd.conference.dao.ExpireNoticeFileDao;
import com.easycms.hd.conference.dao.ExpireNoticeReaderDao;
import com.easycms.hd.conference.domain.ExpireNoticeFile;
import com.easycms.hd.conference.domain.ExpireNoticeReader;
import com.easycms.hd.plan.domain.EnpowerExpireNoticeResponse;

@Service("enpowerExpireNoticeImpl")
public class EnpowerExpireNoticeServiceImpl  {

	private static Logger logger = Logger.getLogger(EnpowerExpireNoticeServiceImpl.class);
	@Autowired
	private EnpowerExpireNoticeDao enpowerExpireNoticeDao;
	@Autowired
	private ExpireNoticeFileDao expireNoticeFileDao;
	@Autowired
	private ExpireNoticeReaderDao expireNoticeReaderDao;

	public EnpowerExpireNotice add(EnpowerExpireNotice expireNotice) {
		return enpowerExpireNoticeDao.save(expireNotice);
	}
	public  JsonResult<Boolean> save(List<EnpowerExpireNoticeResponse> expireNotices) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean f = false ;
		if(expireNotices==null || expireNotices.size()==0){
			f = false ;
			result.setResponseResult(f);
			return result ;
		}else{
			try{
				for(EnpowerExpireNoticeResponse resp:expireNotices){
					
					List<EnpowerExpireNotice> already = enpowerExpireNoticeDao.findByCancCode(resp.getCancCode());
					if(already!=null && already.size()>0){
						throw new RuntimeException("失效文件已经存在！expired file info is already exists !cancCode="+resp.getCancCode());
					}

					EnpowerExpireNotice b = new EnpowerExpireNotice();
					List<ExpireNoticeFile> materialInfoListx = resp.getCancFileList();
					if(materialInfoListx==null || materialInfoListx.size()==0){
						throw new RuntimeException("失效文件信息没有！expired file info is losted!WorkListId="+resp.getCancCode());
					}
					try{

						/**/
//						b.setId(resp.getId());
						/* 文件失效通知单编号 */
						b.setCancCode(resp.getCancCode());
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						try{
							String sDate = resp.getAskReturnDate();
							if(CommonUtility.isNonEmpty(sDate)){
								sDate = sDate.replaceAll("-", "/");
								b.setAskReturnDate(sdf.parse(sDate));
								/* 要求归还时间 */
							}
							String eDate3 = resp.getDraftDate();
							if(CommonUtility.isNonEmpty(eDate3)){
								eDate3 = eDate3.replaceAll("-", "/");
								b.setDraftDate(sdf.parse(eDate3));
								/* 编制日期 */
							}
							String eDate = resp.getReleaseDate();
							if(CommonUtility.isNonEmpty(eDate)){
								eDate = eDate.replaceAll("-", "/");
								b.setReleaseDate(sdf.parse(eDate));
								/* 发布时间 */
							}
						}catch(Exception e){
							e.printStackTrace();
							f = false ;
							result.setMessage("操作执行成功失败");
							result.setResponseResult(f);
							return result ;
						}
						
						/* 项目代号 */
						b.setProjCode(resp.getProjCode());
						/* 公司代码 */
						b.setCompCode(resp.getCompCode());
						/* 编制人id */
						b.setDrafter(resp.getDrafter());
						/* 审批人id */
						b.setSigner(resp.getSigner());

						EnpowerExpireNotice notice = null;
						try{
							notice= this.enpowerExpireNoticeDao.save(b);
						}catch(Exception e){
							e.printStackTrace();
							logger.error("add ExpireNotice into mysql db error!");
							throw e ;
						}
						/**
						 * 材料信息 保存
						 */
						
						if(notice==null){
							throw new RuntimeException("文件失效通知单 信息保存不成功！enpowerExpireNoticeDao add err");
						}
						
						List<ExpireNoticeFile> fileList = resp.getCancFileList();
						if(fileList==null || fileList.size()==0){
							throw new RuntimeException("文件失效通知单文件信息没有！CancFileList info is losted!");
						}else{
							for(ExpireNoticeFile fl:fileList){
								fl.setCancCode(resp.getCancCode());
								fl.setNotice(notice);
//								INV
								if("INV".equals(fl.getStatus())){
									fl.setStatus("已失效");
								}
								this.expireNoticeFileDao.save(fl);
								
								List<ExpireNoticeReader> userList = fl.getUserList();
								if(userList!=null && userList.size()>0){
									
									for(ExpireNoticeReader rea:userList){
										rea.setCancId(fl.getCancId());
										rea.setFile(fl);
										rea.setStatus("N");
										if(rea.getBorNum()!=null && rea.getBorNum().trim().matches("\\d+")){
											rea.setBorNum(String.valueOf(Integer.valueOf(rea.getBorNum().trim())));
										}
										expireNoticeReaderDao.save(rea);
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
						throw e ;
					}
				}
			}catch(Exception e){
				
				e.printStackTrace();
				throw e;
			}
			
			//最终成功返回
			f = true ;
			result.setResponseResult(f);
			return result ;
		}
	}
//	文件失效通知单归还写入APP
	public  JsonResult<Boolean> update(List<ExpireNoticeReader> readers) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean f = false ;
		if(readers==null || readers.size()==0){
			f = false ;
			result.setResponseResult(f);
			return result ;
		}else{
			try{

					if(readers==null || readers.size()==0){
						throw new RuntimeException("失效文件信息没有！NoticeReader info is losted");
					}
					try{

						if(readers!=null && readers.size()>0){
							
							for(ExpireNoticeReader rea:readers){
								ExpireNoticeReader ch = new ExpireNoticeReader();
								ch.setCancId(rea.getCancId());
								ch.setLoginId(rea.getLoginId());

								List<ExpireNoticeReader> alreadys = expireNoticeReaderDao.findAll(QueryUtil.queryConditions(ch));
								if(alreadys!=null && alreadys.size()>0){
									alreadys.forEach(bn->{
										String borNum = rea.getBorNum();
										if(borNum!=null && borNum.matches("\\d+")){
											Integer old = 0;
											try{
												old = Integer.valueOf( bn.getBorNum());
											}catch(Exception e){
												e.printStackTrace();
											}
											
											Integer nowNum = old-Integer.valueOf(borNum);
											//如果已经还完
											if(nowNum<=0){
												bn.setStatus("Y");
												expireNoticeReaderDao.updateById("Y", bn.getCancId(),bn.getLoginId());
											}else{//还没有还完的情况
												expireNoticeReaderDao.updateBorNumById(nowNum+"", bn.getCancId(),bn.getLoginId());
											}

										}else{
											bn.setStatus("Y");
											expireNoticeReaderDao.updateById("Y", bn.getCancId(),bn.getLoginId());
										}
									});
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
						throw e ;
					}
			}catch(Exception e){
				e.printStackTrace();
				f = false ;
				result.setCode("-2001");
				result.setMessage("操作异常："+e.getMessage());
				result.setResponseResult(f);
				return result ;
			}
			
			//最终成功返回
			f = true ;
			result.setResponseResult(f);
			return result ;
		}
	}
	/**
	 * 查询最近的文件失效单记录
	 * @param condition 查询条件
	 * @param additionalCondition　针对某些额外的查询，不是相等的限制时
	 * @param page
	 * @return
	 */
	public Page<EnpowerExpireNotice> findByPage(EnpowerExpireNotice condition,Map<String,Object> additionalCondition, Page<EnpowerExpireNotice> page) {
		
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<EnpowerExpireNotice> springPage = null;
		if(condition==null){
			condition = new EnpowerExpireNotice();
		}
		
		Map<String,String> cmethod= new HashMap<String,String>();
		cmethod.put("askReturnDate", JpaMethod.greaterThanEquals);
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -90);
		
		condition.setAskReturnDate(c.getTime());
		condition.setOrder("desc");
		condition.setSort("releaseDate");
		
		springPage = enpowerExpireNoticeDao.findAll(QueryUtil.queryConditions(condition, cmethod, null, null),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
//	public Page<EnpowerExpireNotice> findByPage(Integer userId, Page<EnpowerExpireNotice> page) {
//		ConferenceReceive conferenceReceive = new ConferenceReceive();
//		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
//		conferenceReceive.setPageSize(page.getPagesize());
//		conferenceReceive.setUserId(userId);
//
//		int count = 0;
//		List<Integer> result = null;
//		result = expireNoticeMybatisDao.findExpireNoticeReceive(conferenceReceive);
//		count = expireNoticeMybatisDao.findExpireNoticeReceiveCount(conferenceReceive);
//		
//		if (null != result && !result.isEmpty()) {
//			List<ExpireNotice> expireNotices = result.stream().map(id -> {
//				ExpireNotice expireNotice = expireNoticeDao.findById(id);
//				return expireNotice;
//			}).collect(Collectors.toList());
//			
//			page.execute(count, page.getPageNum(), expireNotices);
//		}
//		
//		return page;
//	}

	public EnpowerExpireNotice findById(Integer id) {
		return enpowerExpireNoticeDao.findById(id);
	}
}
