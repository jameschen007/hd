package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ConferenceFileDao;
import com.easycms.hd.conference.domain.ConferenceFile;
import com.easycms.hd.conference.service.ConferenceFileService;

@Service("conferenceFileService")
public class ConferenceFileServiceImpl implements ConferenceFileService{

	@Autowired
	private ConferenceFileDao conferenceFileDao;
	@Override
	public ConferenceFile add(ConferenceFile pfile) {
		return conferenceFileDao.save(pfile);
	}
	@Override
	public List<ConferenceFile> findFilesByConferenceId(Integer id) {
	
		return conferenceFileDao.findByConferenceId(id);
	}
	
	@Override
	public boolean batchRemove(Integer[] ids) {
		return conferenceFileDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveConference(Integer[] ids) {
		return conferenceFileDao.batchRemoveConference(ids) > 0;
	}
	@Override
	public List<ConferenceFile> findFilesByConferenceIds(Integer[] ids) {
		return conferenceFileDao.findFilesByConferenceIds(ids);
	}
	@Override
	public boolean batchUpdateConference(Integer[] ids, Integer conferenceId) {
		return conferenceFileDao.batchUpdateConference(ids, conferenceId) > 0;
	}
	@Override
	public ConferenceFile findById(Integer id) {
		return conferenceFileDao.findById(id);
	}

}
