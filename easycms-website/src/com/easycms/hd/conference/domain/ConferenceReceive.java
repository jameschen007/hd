package com.easycms.hd.conference.domain;

import lombok.Data;

@Data
public class ConferenceReceive {
	private Integer userId;
	private Integer pageNumber;
	private Integer pageSize;
}
