package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "conference")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class Conference extends BasicForm implements Serializable {
	private static final long serialVersionUID = 1280545995308362123L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "subject")
	private String subject;
	@Column(name = "content")
	private String content;
	@Column(name = "category")
	private String category;
	@Column(name = "type")
	private String type;
	@Column(name = "project")
	private String project;
	@Column(name = "host")
	private String host;
	@Column(name = "recorder")
	private String recorder;
	@Column(name = "supplies")
	private String supplies;
	@Column(name = "address")
	private String address;
	@Column(name = "remark")
	private String remark;
	@Column(name = "status")
	private String status;
	@Column(name = "start_time")
	private Date startTime;
	@Column(name = "end_time")
	private Date endTime;
	@Column(name = "alarm_time")
	private String alarmTime;
	@Column(name = "trigger_name")
	private String triggerName;
	@Column(name = "source")
	private String source;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
	
	@JsonIgnore
	@ManyToMany(targetEntity=com.easycms.management.user.domain.User.class,fetch=FetchType.EAGER,cascade={CascadeType.REMOVE, CascadeType.REFRESH})
	@JoinTable(
			name="conference_user",
			joinColumns=@JoinColumn(name="conference_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="user_id",referencedColumnName="id")
			)
	private List<User> participants;
	
	@Transient
	private List<ConferenceFile> conferenceFiles;
	
	@JsonIgnore
	@Transient
	private List<Integer> userIds;
	@JsonIgnore
	@Transient
	private Integer[] userIdarray;
	
	public List<Integer> getUserIds(){
		if (null != participants) {
			List<Integer> ids = participants.stream().map(p -> p.getId()).collect(Collectors.toList());
			return ids;
		}
		return null;
	}
	
	public Integer[] getUserIdarray(){
		if (null != participants) {
			List<Integer> ids = participants.stream().map(p -> p.getId()).collect(Collectors.toList());
			if (null != ids && !ids.isEmpty()) {
				return ids.toArray(new Integer[] {});
			}
			return null;
		}
		return null;
	}
	
	
}
