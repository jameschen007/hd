package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.WitnessTypeEnum;
import com.easycms.hd.api.response.WitnessPageResult;
import com.easycms.hd.api.response.WitnessResult;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.management.user.domain.User;

public interface WitnessApiService {
	WitnessPageResult getWitnessPageResult(Page<WorkStepWitness> page, User user, WitnessTypeEnum witnessType, String rollingPlanType, NoticePointType noticePointType, String keyword, User loginUser,Integer usingRoleId);
	
	boolean assignToWitnessTeam(Integer[] ids, Integer teamId);
	
	boolean assignToWitnessMember(Integer[] ids, Integer teamId, Integer memberId);
	
	List<String> getWitnessAddresses();
	
	WitnessResult findByWitnessIdAndRollingPlanType(User user, Integer witnessId, String rollingPlanType, boolean ref);

	WitnessResult workStepWitnessTransferToWitnessResult(WorkStepWitness workStepWitness);

	/**
	 *适用于　福清qc1ReplaceQc2
	 * */
	boolean assignToWitnesserQC2(Integer[] ids, Integer qc2Id, Integer memberId);
}
