package com.easycms.hd.api.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.WorkStepTypeEnum;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.api.response.WorkStepPageDataResult;
import com.easycms.hd.api.response.WorkStepResult;
import com.easycms.hd.api.response.WorkStepWitnessItemsResult;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;
import com.easycms.management.user.domain.User;

public interface WorkStepApiService {
	WorkStepResult workStepTransferForMobile(WorkStep workStep);
	
	List<WorkStepResult> workStepTransferForMobile(List<WorkStep> workSteps);
	
	WorkStepPageDataResult getWorkStepPageDataResult(Page<WorkStep> page, User user, WorkStepTypeEnum status, String type, String keyword);
	
	List<WorkStepWitnessItemsResult> launchWitness(List<WitingPlan> planList, User user) throws Exception;
	

	//根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	/**
	 * 
	 * @param map [planIds 计划数组]
	 * @return
	 */
	public List<WorkStepForBatchWitness> getWorkStep(Map<String,Object> map);

	/**
	 * 取消见证
	 * @param ids
	 * @param user
	 * @return
	 */
	List<WorkStepWitnessItemsResult> cancelWitness(List<Integer> ids, User user);
}
