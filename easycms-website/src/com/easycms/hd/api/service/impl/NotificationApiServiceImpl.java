package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.domain.Statistics;
import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.enums.ConferenceStatusEnum;
import com.easycms.hd.api.request.NotificationRequestForm;
import com.easycms.hd.api.response.FileResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.NotificationPageResult;
import com.easycms.hd.api.response.NotificationResult;
import com.easycms.hd.api.service.NotificationApiService;
import com.easycms.hd.api.service.NotificationFeedbackApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.domain.NotificationConfirm;
import com.easycms.hd.conference.domain.NotificationFeedback;
import com.easycms.hd.conference.domain.NotificationFile;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.hd.conference.enums.ConferenceType;
import com.easycms.hd.conference.enums.NotificationConfirmType;
import com.easycms.hd.conference.service.NotificationConfirmService;
import com.easycms.hd.conference.service.NotificationFeedbackMarkService;
import com.easycms.hd.conference.service.NotificationFeedbackService;
import com.easycms.hd.conference.service.NotificationFileService;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.hd.plan.mybatis.dao.ConferenceStatisticsMybatisDao;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.SchedulerService;

@Service("notificationApiService")
public class NotificationApiServiceImpl implements NotificationApiService {
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private NotificationFileService notificationFileService;
	@Autowired
	private NotificationConfirmService notificationConfirmService;
	@Autowired
	private NotificationFeedbackService notificationFeedbackService;
	@Autowired
	private NotificationFeedbackMarkService notificationFeedbackMarkService;
	@Autowired
	private NotificationFeedbackApiService notificationFeedbackApiService;
	@Autowired
	private ConferenceStatisticsMybatisDao conferenceStatisticsMybatisDao;
	@Autowired
	private SchedulerService schedulerService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_notification_path']}")
	private String fileNotificationPath;

	@Override
	public NotificationResult notificationTransferToNotificationResult(Notification notification, Integer userId, ConferenceApiRequestType type, boolean feedback) {
		if (null != notification) {
			NotificationResult notificationResult = new NotificationResult();
			notificationResult.setId(notification.getId());
			notificationResult.setSubject(notification.getSubject());
			notificationResult.setContent(notification.getContent());
			notificationResult.setCategory(notification.getCategory());
			notificationResult.setDepartment(notification.getDepartment());
			notificationResult.setAlarmTime(notification.getAlarmTime());
			notificationResult.setStartTime(notification.getStartTime());
			notificationResult.setEndTime(notification.getEndTime());
			notificationResult.setSource(notification.getSource());
			
			if (null != userId) {
				Statistics statistics = new Statistics();
				statistics.setUserId(userId);
				statistics.setId(notification.getId());
				if (type.equals(ConferenceApiRequestType.SEND)) {
					notificationResult.setUnread(conferenceStatisticsMybatisDao.notificationEachSendUnReadFeedback(statistics));
				} else if (type.equals(ConferenceApiRequestType.RECEIVE)) {
					notificationResult.setUnread(conferenceStatisticsMybatisDao.notificationEachReceiveUnReadFeedback(statistics));
				}
				
			}
			List<NotificationConfirm> notificationConfirms = notificationConfirmService.findByNotificationId(notification.getId(), userId);
	        
	        if (null != notificationConfirms && !notificationConfirms.isEmpty()) {
	        	notificationResult.setConfirm(true);
	        }
			
			if (notification.getType().equals(ConferenceType.DRAFT.name())) {
				notificationResult.setStatus(ConferenceStatusEnum.DRAFT.name());
			} else {
				if (null != notification.getStartTime() && null != notification.getEndTime()) {
					Long currentTime = System.currentTimeMillis();
					Long startTime = notification.getStartTime().getTime();
					Long endTime = notification.getEndTime().getTime();
					
					if (currentTime < startTime) {
						notificationResult.setStatus(ConferenceStatusEnum.UNSTARTED.name());
					} else if (currentTime >= startTime && currentTime <= endTime) {
						notificationResult.setStatus(ConferenceStatusEnum.PROGRESSING.name());
					} else if (currentTime > endTime) {
						notificationResult.setStatus(ConferenceStatusEnum.EXPIRED.name());
					}
				} else {
					notificationResult.setStatus(ConferenceStatusEnum.UNKNOWN.name());
				}
			}
			
			if (null != notification.getParticipants() && !notification.getParticipants().isEmpty()) {
				notificationResult.setParticipants(new HashSet<>(userApiService.userTransferToUserResult(notification.getParticipants(),true)));//显示原始的角色，没有roleType显示
			}
			
			List<NotificationFile> notificationFiles = notificationFileService.findFilesByNotificationId(notification.getId());
			if (null != notificationFiles) {
				notification.setNotificationFiles(notificationFiles);
			}
			if (null != notification.getNotificationFiles() && !notification.getNotificationFiles().isEmpty()) {
				List<FileResult> fileResults = notification.getNotificationFiles().stream().map(file -> {
					FileResult fileResult = new FileResult();
					fileResult.setId(file.getId());
					fileResult.setUrl("/hdxt/api/files/notification/" + file.getPath());
					fileResult.setFileName(file.getFileName());
					return fileResult;
				}).collect(Collectors.toList());
				notificationResult.setFiles(fileResults);
			}
			
			if (feedback) {
				List<NotificationFeedback> notificationFeedbacks = notificationFeedbackService.findByNotificationId(notification.getId());
				notificationResult.setFeedback(notificationFeedbackApiService.notificationFeedbackTransferToNotificationFeedbackResult(notificationFeedbacks));
			}
			
			return notificationResult;
		}
		return null;
	}
	
	@Override
	public List<NotificationResult> notificationTransferToNotificationResult(List<Notification> notifications, Integer userId, ConferenceApiRequestType type) {
		if (null != notifications && !notifications.isEmpty()) {
			List<NotificationResult> notificationResults = notifications.stream().map(notification -> {
				return notificationTransferToNotificationResult(notification, userId, type, false);
			}).collect(Collectors.toList());
			
			return notificationResults;
		}
		return null;
	}

	@Override
	public JsonResult<Integer> insert(NotificationRequestForm form, User user, ConferenceSource source) {
		if (null != form) {
			JsonResult<Integer> result = new JsonResult<Integer>();
			
			if (form.getType().equals(ConferenceType.SEND)) {
				List<String> message = new ArrayList<String>();
				
				if (!CommonUtility.isNonEmpty(form.getContent())) {
					message.add("* 正文不能为空.");
				}
				if (!CommonUtility.isNonEmpty(form.getSubject())) {
					message.add("* 主题不能为空.");
				}
				if (null == form.getStartTime()) {
					message.add("* 开始时间不能为空.");
				}
				if (null == form.getEndTime()) {
					message.add("* 结束时间不能为空.");
				}
				if (null == form.getAlarmTime()) {
					message.add("* 提醒类型不能为空.");
				}
				if (null != form.getStartTime() && null != form.getEndTime()) {
					if (form.getStartTime().getTime() > form.getEndTime().getTime()) {
						message.add("* 开始时间不能大于结束时间.");
					}
				}
				
				if (!message.isEmpty()) {
					result.setCode("-1000");
					result.setMessage(message.stream().reduce((a, b) -> a + b).get());
					result.setResponseResult(null);
					return result;
				}
			}
			
			Notification notification = new Notification();
			if (null != form.getId()) {
				notification = notificationService.findById(form.getId());
				if (null == notification) {
					notification = new Notification();
				}
			} 
			
			notification.setSubject(form.getSubject());
			notification.setContent(form.getContent());
			notification.setCategory(form.getCategory());
			notification.setDepartment(form.getDepartment());
			notification.setAlarmTime(form.getAlarmTime().name());
			notification.setStartTime(form.getStartTime());
			notification.setEndTime(form.getEndTime());
			notification.setStatus("ACTIVE");
			notification.setSource(source.name());
			
			if (null != form.getParticipants() && form.getParticipants().length > 0) {
				Set<User> users = new HashSet<User>();
				users.add(user);
				for (Integer id : form.getParticipants()) {
					User u = userService.findUserById(id);
					if (null != u) {
						users.add(u);
					}
				}
				notification.setParticipants(new ArrayList<>(users));
			}
			
			notification.setCreateBy(user.getId());
			notification.setCreateOn(new Date());
			if (null == form.getType()) {
				notification.setType(ConferenceType.SEND.name());
			} else {
				notification.setType(form.getType().name());
			}

			if (null != notificationService.add(notification)) {
				NotificationConfirm confirm = new NotificationConfirm();
	        	confirm.setUserid(user.getId());
	        	confirm.setCreateBy(user.getId());
	        	confirm.setCreateOn(new Date());
	        	confirm.setNotificationid(notification.getId());
	        	confirm.setStatus(NotificationConfirmType.CONFIRM.name());
	            
	            notificationConfirmService.add(confirm);
				
				//所有的操作都完成之后，发起push
	            if (null == form.getType() || form.getType().equals(ConferenceType.SEND)) {
	            	if (null != form.getAlarmTime() && null != form.getStartTime() && !form.getAlarmTime().equals(ConferenceAlarmTypsEnums.NONE)) {
	            		Map<String, Object> dataMap = new HashMap<String, Object>();
	            		dataMap.put("userId", user.getId());
	            		dataMap.put("notificationId", notification.getId());
	            		
	            		Date startTime = DateUtility.dateMinus(notification.getStartTime(), form.getAlarmTime().getMillis());
	            		
		            	Map<String, Object> jobDataMap = new HashMap<String, Object>();
		            	jobDataMap.put("jobService", "notificationAlarmService");
		            	jobDataMap.put("jobDataMap", dataMap);
		            	String name = schedulerService.schedule(startTime, jobDataMap);
		            	
		            	notification.setTriggerName(name);
		            	notificationService.add(notification);
	            	}
	            } else if (form.getType().equals(ConferenceType.DRAFT)) {
	            	if (CommonUtility.isNonEmpty(notification.getTriggerName())) {
	            		schedulerService.removeTrigger(notification.getTriggerName());
	            	}
	            }
	            
	            result.setResponseResult(notification.getId());
	            return result;
			}
		}
		return null;
	}

	@Override
	public NotificationPageResult getNotificationPageResult(ConferenceApiRequestType type, Integer userId, Page<Notification> page) {
		if (null != type && null != userId) {
			NotificationPageResult notificationPageResult = new NotificationPageResult();
			if (type.equals(ConferenceApiRequestType.DRAFT)) {
				page = notificationService.findByTypeAndCreater(type.name(), userId, page);
			} else if (type.equals(ConferenceApiRequestType.SEND)) {
				page = notificationService.findNotificationSend(userId, page);
			} else if (type.equals(ConferenceApiRequestType.RECEIVE)){
				page = notificationService.findNotificationReceive(userId, page);
			}
			
			if (null != page.getDatas()) {
				List<NotificationResult> notificationResults = page.getDatas().stream().map(notification -> {
					return this.notificationTransferToNotificationResult(notification, userId, type, false);
				}).collect(Collectors.toList());
				
				notificationPageResult.setPageCounts(page.getPageCount());
				notificationPageResult.setPageNum(page.getPageNum());
				notificationPageResult.setPageSize(page.getPagesize());
				notificationPageResult.setTotalCounts(page.getTotalCounts());
				notificationPageResult.setData(notificationResults);
			}
			
			return notificationPageResult;
		}
		return null;
	}
	
	@Override
	public boolean batchDelete(Integer[] ids) {
		if (null != ids && ids.length > 0) {
			String filePath = com.easycms.common.logic.context.ContextPath.fileBasePath + fileNotificationPath;
			List<NotificationFile>  notificationFiles = notificationFileService.findFilesByNotificationIds(ids);
			for (NotificationFile notificationFile : notificationFiles) {
					FileUtils.remove(filePath + notificationFile.getPath());
			}
			
			
			notificationService.batchDelete(ids);
			notificationFileService.batchRemoveNotification(ids);
			notificationFeedbackService.batchRemoveNotification(ids);
			notificationFeedbackMarkService.batchRemoveNotification(ids);
			return true;
		}
		return false;
	}
	

	/**批量取消会议，并能参会人员发送推送消息*/
	@Override
	public boolean batchCancel(Integer[] ids){
		if (null != ids && ids.length > 0) {
			return notificationService.batchCancel(ids);
		}
		
		return false ;
	}
	
	
	
}
