package com.easycms.hd.api.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.api.response.MaterialPageResult;
import com.easycms.hd.api.response.MaterialResult;
import com.easycms.hd.api.service.MaterialApiService;
import com.easycms.hd.material.domain.Material;
import com.easycms.hd.material.service.MaterialService;

@Service("materialApiService")
public class MaterialApiServiceImpl implements MaterialApiService {
	
	@Autowired
	private MaterialService materialService;

	@Override
	public MaterialResult materialTransferToMaterialResult(Material material, Integer userId) {
		if (null != material) {
			MaterialResult materialResult = new MaterialResult();
			materialResult.setID(material.getId()+"");
			materialResult.setName(material.getName());
			materialResult.setWarrantyNo(material.getWarrantyNo());
			materialResult.setNumber(material.getNumber()+"");
			materialResult.setMaterial(material.getMaterial());
			materialResult.setSpecificationNo(material.getSpecificationNo());
			materialResult.setFurnaceNo(material.getFurnaceNo());
			materialResult.setSecurityLevel(material.getSecurityLevel());
			materialResult.setWarrantyLevel(material.getWarrantyLevel());
			materialResult.setShipNo(material.getShipNo());
			materialResult.setNameEn(material.getNameEn());
			materialResult.setStandard(material.getStandard());
			materialResult.setPositionNo(material.getPositionNo());
			materialResult.setRemark(material.getRemark());
			materialResult.setPrice(material.getPrice());
			materialResult.setWarrantyCount(material.getWarrantyCount());
			materialResult.setLocation(material.getLocation());
			materialResult.setWarehouse(material.getWarehouse());
			
			return materialResult;
		}
		return null;
	}
	
	@Override
	public List<MaterialResult> materialTransferToMaterialResult(List<Material> materials, Integer userId) {
		if (null != materials && !materials.isEmpty()) {
			List<MaterialResult> materialResults = materials.stream().map(material -> {
				return materialTransferToMaterialResult(material, userId);
			}).collect(Collectors.toList());
			
			return materialResults;
		}
		return null;
	}

	@Override
	public MaterialPageResult getMaterialPageResult(Integer userId, Page<Material> page) {
		if (null != userId) {
			MaterialPageResult materialPageResult = new MaterialPageResult();
			
			page = materialService.findByPage(page);
			
			if (null != page.getDatas()) {
				List<MaterialResult> materialResults = page.getDatas().stream().map(material -> {
					return this.materialTransferToMaterialResult(material, userId);
				}).collect(Collectors.toList());
				
				materialPageResult.setPageCounts(page.getPageCount());
				materialPageResult.setPageNum(page.getPageNum());
				materialPageResult.setPageSize(page.getPagesize());
				materialPageResult.setTotalCounts(page.getTotalCounts());
				materialPageResult.setData(materialResults);
			}
			
			return materialPageResult;
		}
		return null;
	}
}
