package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.request.NotificationFeedbackRequestForm;
import com.easycms.hd.api.request.NotificationMarkRequestForm;
import com.easycms.hd.api.response.NotificationFeedbackResult;
import com.easycms.hd.api.service.NotificationFeedbackApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.conference.domain.NotificationFeedback;
import com.easycms.hd.conference.domain.NotificationFeedbackMark;
import com.easycms.hd.conference.service.NotificationFeedbackMarkService;
import com.easycms.hd.conference.service.NotificationFeedbackService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Service("notificationFeedbackApiService")
public class NotificationFeedbackApiServiceImpl implements NotificationFeedbackApiService {
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private NotificationFeedbackService notificationFeedbackService;
	@Autowired
	private NotificationFeedbackMarkService notificationFeedbackMarkService;
	@Override
	public NotificationFeedbackResult notificationFeedbackTransferToNotificationFeedbackResult(
			NotificationFeedback notificationFeedback) {
		if (null != notificationFeedback) {
			NotificationFeedbackResult notificationFeedbackResult = new NotificationFeedbackResult();
			notificationFeedbackResult.setId(notificationFeedback.getId());
			notificationFeedbackResult.setMessage(notificationFeedback.getMessage());
			notificationFeedbackResult.setFeedbackTime(notificationFeedback.getCreateOn());
			User user = userService.findUserById(notificationFeedback.getCreateBy());
			if (null != user) {
				notificationFeedbackResult.setUser(userApiService.userTransferToUserResult(user,true));//显示原始的角色，没有roleType显示
			}
			return notificationFeedbackResult;
		}
		return null;
	}
	@Override
	public boolean insert(NotificationFeedbackRequestForm form, User user) {
		if (null != form && null != user) {
			NotificationFeedback notificationFeedback = new NotificationFeedback();
			notificationFeedback.setNotificationid(form.getNotificationId());
			notificationFeedback.setCreateBy(user.getId());
			notificationFeedback.setCreateOn(new Date());
			notificationFeedback.setMessage(form.getMessage());
			if (null != notificationFeedbackService.add(notificationFeedback)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean markRead(NotificationMarkRequestForm form, User user) {
		if (null != form) {
			NotificationFeedbackMark notificationFeedbackMark = notificationFeedbackMarkService.findByNotificationIdAndUserId(form.getNotificationId(), user.getId());
			if (null != notificationFeedbackMark) {
				notificationFeedbackMarkService.markRead(notificationFeedbackMark.getId(), new Date());
			} else {
				notificationFeedbackMark = new NotificationFeedbackMark();
				notificationFeedbackMark.setNotificationid(form.getNotificationId());
				notificationFeedbackMark.setUserId(user.getId());
				notificationFeedbackMark.setMarkTime(new Date());
				notificationFeedbackMark.setCreateBy(user.getId());
				notificationFeedbackMark.setCreateOn(new Date());
				notificationFeedbackMarkService.add(notificationFeedbackMark);
			}
			
			return true;
		}
		return false;
	}
	
	@Override
	public List<NotificationFeedbackResult> notificationFeedbackTransferToNotificationFeedbackResult(
			List<NotificationFeedback> notificationFeedbacks) {
		if (null != notificationFeedbacks && !notificationFeedbacks.isEmpty()) {
			List<NotificationFeedbackResult> notificationFeedbackResults = new ArrayList<NotificationFeedbackResult>();
			for (NotificationFeedback notificationFeedback : notificationFeedbacks) {
				notificationFeedbackResults.add(notificationFeedbackTransferToNotificationFeedbackResult(notificationFeedback));
			}
			return notificationFeedbackResults;
		}
		return null;
	}
}
