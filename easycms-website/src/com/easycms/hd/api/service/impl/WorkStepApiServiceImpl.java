package com.easycms.hd.api.service.impl;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.StatusEnum;
import com.easycms.hd.api.enums.WitnessFlagEnum;
import com.easycms.hd.api.enums.WorkStepTypeEnum;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.api.response.WitnessResult;
import com.easycms.hd.api.response.WorkStepPageDataResult;
import com.easycms.hd.api.response.WorkStepResult;
import com.easycms.hd.api.response.WorkStepWitnessItemsResult;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.hd.plan.domain.*;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.service.impl.WitnessHisServiceImpl;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.management.user.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service("workStepApiService")
public class WorkStepApiServiceImpl implements WorkStepApiService {
	
	private static Logger logger = Logger.getLogger(WorkStepApiServiceImpl.class);
	
	@Autowired
	private WitnessApiService witnessApiService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private WitnessHisServiceImpl witnessHisServiceImpl;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	@Autowired
	private JPushService jPushService;

	@Override
	public WorkStepResult workStepTransferForMobile(WorkStep workStep) {
		if (null != workStep) {
			WorkStepResult workStepResult = new WorkStepResult();
			workStepResult.setId(workStep.getId());
			workStepResult.setStepIdentifier(workStep.getStepIdentifier());
			workStepResult.setStepno(workStep.getStepno());
			workStepResult.setStepname(workStep.getStepname());
			workStepResult.setLastone(workStep.isLastone());
			workStepResult.setStepflag(workStep.getStepflag());
			workStepResult.setNoticeQC1(workStep.getNoticeaqc1());
			workStepResult.setNoticeQC2(workStep.getNoticeaqc2());
			workStepResult.setNoticeCZECQA(workStep.getNoticeczecqa());
			workStepResult.setNoticeCZECQC(workStep.getNoticeczecqc());
			workStepResult.setNoticePAEC(workStep.getNoticepaec());
			workStepResult.setItpName(workStep.getItpName());
			workStepResult.setWitnessAddresses(witnessApiService.getWitnessAddresses());
			if (CommonUtility.isNonEmpty(workStep.getNoticeaqc1()) || 
					CommonUtility.isNonEmpty(workStep.getNoticeaqc2()) || 
					CommonUtility.isNonEmpty(workStep.getNoticeczecqa()) || 
					CommonUtility.isNonEmpty(workStep.getNoticeczecqc()) || 
					CommonUtility.isNonEmpty(workStep.getNoticepaec())) {
				long noticePointCount = 0L;
				if (CommonUtility.isNonEmpty(workStep.getNoticeaqc1())) {
					noticePointCount++;
				}
				if (CommonUtility.isNonEmpty(workStep.getNoticeaqc2())) {
					noticePointCount++;
				}
				if (CommonUtility.isNonEmpty(workStep.getNoticeczecqa())) {
					noticePointCount++;
				}
				if (CommonUtility.isNonEmpty(workStep.getNoticeczecqc())) {
					noticePointCount++;
				}
				if (CommonUtility.isNonEmpty(workStep.getNoticepaec())) {
					noticePointCount++;
				}
				
				List<Integer> successSymbol = new ArrayList<Integer>();
				successSymbol.add(2);
				successSymbol.add(3);

				List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
				if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
					List<WitnessResult> witnessResults = new ArrayList<WitnessResult>();
					workStepWitnesses = workStepWitnesses.stream().filter(wsw -> {
						return (wsw.getIsok() > 0 && CommonUtility.isNonEmpty(wsw.getWitnessflag())) || !CommonUtility.isNonEmpty(wsw.getWitnessflag());
					}).collect(Collectors.toList());
					
					if (null != workStepWitnesses) {
						Collections.sort(workStepWitnesses);
						
						List<String> ids = new ArrayList<String>();
						for (WorkStepWitness wsw : workStepWitnesses) {
							if (!ids.contains(wsw.getBatchId())) {
								ids.add(wsw.getBatchId());
							}
						}
						
						for (int i = 0; i < ids.size(); i++) {
							String id = ids.get(i);
							for (WorkStepWitness wsw : workStepWitnesses) {
								if (CommonUtility.isNonEmpty(wsw.getBatchId())) {
									if (wsw.getBatchId().equals(id)) {
										wsw.setSeq(i);
									}
								}
							}
						}
						
						for (WorkStepWitness workStepWitness : workStepWitnesses) {
							witnessResults.add(witnessApiService.workStepWitnessTransferToWitnessResult(workStepWitness));
						}
						workStepResult.setWitnessInfo(witnessResults);
						workStepResult.setLaunchData(workStep.getWorkStepWitnesses().get(0).getCreatedOn());
						workStepResult.setWitnessAddress(workStep.getWorkStepWitnesses().get(0).getWitnessaddress());
						workStepResult.setWitnessDate(workStep.getWorkStepWitnesses().get(0).getWitnessdate());
						workStepResult.setRealWitnessAddress(workStep.getWorkStepWitnesses().get(0).getRealwitnessaddress());
						workStepResult.setRealWitnessDate(workStep.getWorkStepWitnesses().get(0).getRealwitnessdata());
						
						long successCount = workStep.getWorkStepWitnesses().stream().filter(wsw -> (successSymbol.contains(wsw.getIsok()) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()))).count();
						long failedCount = workStep.getWorkStepWitnesses().stream().filter(wsw -> (wsw.getIsok().equals(1) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()))).count();
						long undoCount = workStep.getWorkStepWitnesses().stream().filter(wsw -> (wsw.getIsok().equals(0) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()))).count();
						if (successCount == noticePointCount) {
							workStepResult.setStatus(StatusEnum.COMPLETED.name());
							workStepResult.setResult(StatusEnum.QUALIFIED.name());
						} else if (failedCount > 0L) {
							workStepResult.setStatus(StatusEnum.PROGRESSING.name());
							workStepResult.setResult(StatusEnum.UNQUALIFIED.name());
						} else if (undoCount > 0L) {
							workStepResult.setStatus(StatusEnum.PROGRESSING.name());
							workStepResult.setResult(StatusEnum.UNCOMPLETED.name());
						}
					}
				} else {
					workStepResult.setStatus(StatusEnum.UNPROGRESSING.name());
					workStepResult.setResult(StatusEnum.UNWITNESS.name());
				}
			} else {
				if (null == workStep.getNoticeresult()) {
					if (workStep.getStepflag().equals(StepFlag.PREPARE)) {
						workStepResult.setStatus(StatusEnum.PROGRESSING.name());
					} else if (workStep.getStepflag().equals(StepFlag.DONE)) {
						workStepResult.setStatus(StatusEnum.COMPLETED.name());
					} else if (workStep.getStepflag().equals(StepFlag.UNDO)) {
						workStepResult.setStatus(StatusEnum.UNPROGRESSING.name());
					}
				}
				workStepResult.setResult(StatusEnum.NONE.name());
			}
			
			if (null != workStep.getRollingPlan()) {
				workStepResult.setRollingPlanId(workStep.getRollingPlan().getId());
				workStepResult.setWeldno(workStep.getRollingPlan().getWeldno());
				if (CommonUtility.isNonEmpty(workStep.getRollingPlan().getRollingplanflag())) {
					workStepResult.setProblemFlag(true);
				} 
			}
			
			return workStepResult;
		}
		return null;
	}

	@Override
	public List<WorkStepResult> workStepTransferForMobile(List<WorkStep> workSteps) {
		if (null != workSteps && !workSteps.isEmpty()) {
			List<WorkStepResult> workStepResults = new ArrayList<WorkStepResult>();
			for (WorkStep workStep : workSteps) {
				workStepResults.add(this.workStepTransferForMobile(PlanUtil.lastOneMark(workStep)));
			}
			return workStepResults;
		}
		return null;
	}

	@Override
	public WorkStepPageDataResult getWorkStepPageDataResult(Page<WorkStep> page, User user, WorkStepTypeEnum status,
			String type, String keyword) {
		WorkStepPageDataResult workStepPageDataResult = new WorkStepPageDataResult();
		
		try {
			if (status.equals(WorkStepTypeEnum.UNCOMPLETED)) {
				if (CommonUtility.isNonEmpty(keyword)) {
					page = workStepService.findByPageTeamUnCompletedWithKeyword(type, user.getId(), keyword, page);
				} else {
//					page = workStepService.findByPageTeamUnCompleted(type, user.getId(), page);
					//组长未完成的见证工序 不判断计划状态？
					page = workStepService.findByPageTeamUnCompleted(type, user.getId(), page);
				}
			} else if (status.equals(WorkStepTypeEnum.COMPLETED)) {
				if (CommonUtility.isNonEmpty(keyword)) {
					page = workStepService.findByPageTeamCompleteWithKeyword(type, user.getId(), keyword, page);
				} else {
//					组长完成的见证工序：查询这条工序上的所有见证点都完成了
//					page = workStepService.findByPageTeamComplete(type, user.getId(), page);
					page = workStepService.findByPageTeamCompleteNotPlanComplete(type, user.getId(), page);
				}
			} else {
				return null;
			}
			
			if (null != page.getDatas()) {
				List<WorkStepResult> workStepResults = page.getDatas().stream().map(workStep -> {
					return this.workStepTransferForMobile(workStep);
				}).collect(Collectors.toList());
				
				workStepPageDataResult.setPageCounts(page.getPageCount());
				workStepPageDataResult.setPageNum(page.getPageNum());
				workStepPageDataResult.setPageSize(page.getPagesize());
				workStepPageDataResult.setTotalCounts(page.getTotalCounts());
				workStepPageDataResult.setData(workStepResults);
			}
			
			return workStepPageDataResult;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public List<WorkStepWitnessItemsResult> launchWitness(List<WitingPlan> witingPlans, User user) throws Exception {

		//TODO  为了避免出现“插入通知单明细”时工序有很多的情况，应该在进入本方法前，就对数据按滚动计划先分好，把数据整理完，本方法进行具体的业务执行了。

		try{
			List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = new ArrayList<WorkStepWitnessItemsResult>();
			//调用Enpower发 点接口？
//			List<WorkStep> sendPointList = new ArrayList<WorkStep>();
			Map<Integer,WorkStepWitnessItemsForm> stepWitnessDateMap = new HashMap<Integer,WorkStepWitnessItemsForm>();
	        //按滚动计划为单位，逐个推送到Enpower
			Set<Integer> instanceIdset = new HashSet<Integer>();
			Map<Integer, List<WorkStep>> stepMapByRollKey = new HashMap<>();
			boolean reCreated = false;
			for (WitingPlan wPlan : witingPlans) {
				for (WorkStepWitnessItemsForm workStepWitnessItemsForm : wPlan.getStep()) {
				WorkStepWitnessItemsResult workStepWitnessItemsResult = new WorkStepWitnessItemsResult();
				workStepWitnessItemsResult.setId(workStepWitnessItemsForm.getId());
				
				String witnessaddress = workStepWitnessItemsForm.getWitnessaddress();
				Date witnessdate = CommonUtility.convertStringToDate(workStepWitnessItemsForm.getWitnessdate());
				
				WorkStep workStep = workStepService.findById(workStepWitnessItemsForm.getId());
				RollingPlan rollingPlan = null ;
				
				if (null != workStep){
					rollingPlan = workStep.getRollingPlan();
					if (null != rollingPlan) {
						if (CommonUtility.isNonEmpty(rollingPlan.getRollingplanflag()) && rollingPlan.getRollingplanflag().equals(RollingPlanFlag.PROBLEM)) {
							logger.error("id对应的滚动计划存在没有解决的问题。"+rollingPlan.getId());
							workStepWitnessItemsResult.setCode("-1002");
							workStepWitnessItemsResult.setMessage("id对应的滚动计划存在没有解决的问题。");
							workStepWitnessItemsResults.add(workStepWitnessItemsResult);
							throw new RuntimeException("滚动计划存在没有解决的问题。");
						}
					} else {
						logger.error("id对应的滚动计划不存在。");
						workStepWitnessItemsResult.setCode("-1002");
						workStepWitnessItemsResult.setMessage("id对应的滚动计划不存在。");
						workStepWitnessItemsResults.add(workStepWitnessItemsResult);
						continue;
					}
					
					//如果qc1,qc2这两个见证点上存在任意一个需要见证的点，那就需要去验证该工序是否已经完成了见证步骤。
					if (CommonUtility.isNonEmpty(workStep.getNoticeaqc1()) || CommonUtility.isNonEmpty(workStep.getNoticeaqc2()) || 
							CommonUtility.isNonEmpty(workStep.getNoticeczecqc()) || CommonUtility.isNonEmpty(workStep.getNoticeczecqa()) || 
							CommonUtility.isNonEmpty(workStep.getNoticepaec())){
						if (null != workStep.getWorkStepWitnesses() && !workStep.getWorkStepWitnesses().isEmpty()) {
							//允许在还没有完全给出见证结果的情况下重新发起见证
//							if (null == workStep.getNoticeresult() || Integer.parseInt(workStep.getNoticeresult()) <= 1){
//								workStepWitnessItemsResult.setCode("-1002");
//								workStepWitnessItemsResult.setMessage("请等待见证结果出来之后再更新工序步骤。");
//								workStepWitnessItemsResults.add(workStepWitnessItemsResult);
//								continue;
//							}
							
							//允许在有一条不合格之后，就对所在工序进行重新发起见证
/*							List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
							
							long undoCount = workStepWitnesses.stream().filter(wsw -> wsw.getIsok().equals(0)).count();
							if (undoCount > 0L) {
								logger.error("id对应的见证结果还未完全给出。"+rollingPlan.getId());
								workStepWitnessItemsResult.setCode("-1002");
								workStepWitnessItemsResult.setMessage("id对应的见证结果还未完全给出。");
								workStepWitnessItemsResults.add(workStepWitnessItemsResult);
								continue;
							}*/
						}
					} else {
						logger.error("没有任何见证点存在，不需要发起见证。"+rollingPlan.getId());
						workStepWitnessItemsResult.setCode("-1002");
						workStepWitnessItemsResult.setMessage("没有任何见证点存在，不需要发起见证。");
						workStepWitnessItemsResults.add(workStepWitnessItemsResult);
						continue;
					}
				} else {
					logger.error("id对应的workstep不存在。");
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("id对应的workstep不存在。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				}
				
	/*			if (null != workStep.getWorkStepWitnesses() && !workStep.getWorkStepWitnesses().isEmpty()){
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("WorkStep [" + workStepWitnessItemsForm.getId() + "] 已经发起过见证，请等待见证结果，当前发起见证请求无效。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				}*/
				
				if (null == witnessaddress){
					logger.error("witnessaddress是必填参数。");
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("witnessaddress是必填参数。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				} else if (witnessaddress.length() > 100){
					logger.error("witnessaddress长度不能超过100。");
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("witnessaddress长度不能超过100。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				}
				if (null == witnessdate){
					logger.error("witnessdate是必填参数。");
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("witnessdate是必填参数。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				}
		
//				User monitor = user.getParent();
				User monitor = rollingPlanService.getConsmoniterBy(null,workStep,null,null,null,null);
				if (null != monitor){
					if (!workStep.getRollingPlan().getConsteam().equals(user.getId())){
						workStepWitnessItemsResult.setCode("-1002");
						logger.error("当前所操作的工序步骤['" + workStep.getStepname() + "']不属于你。");
						workStepWitnessItemsResult.setMessage("当前所操作的工序步骤['" + workStep.getStepname() + "']不属于你。");
						workStepWitnessItemsResults.add(workStepWitnessItemsResult);
						continue;
					}
				} else {
					logger.error("没有找到你的班长");
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("没有找到你的班长");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					continue;
				}
					
				int successCount = 0;
/*	
 * [2017-11-23] 原有的处理方式，即仅将不合格的见证标记过期，并重新创建一条见证记录让原有的见证人来进行见证
/*    
 * [2017-11-25] 以下处理方式为在重新发起见证的情况下，将所有的历史记录都删除掉				
				if (null != workStep.getWorkStepWitnesses() && !workStep.getWorkStepWitnesses().isEmpty()) {
					reCreated = true;
					List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
					
					List<WorkStepWitness> failedList = workStepWitnesses.stream().filter(wsw -> {
					return (wsw.getIsok().equals(1) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()));
					}).collect(Collectors.toList());
					if (null != failedList && !failedList.isEmpty()) {
						reCreated = true;
						witnessService.deleteByWorkStep(workStep.getId());
					}
				}
*/
/*    
 * [2017-11-26] 以下处理方式为在重新发起见证的情况下，将所有的历史记录都标记为过期

                if (null != workStep.getWorkStepWitnesses() && !workStep.getWorkStepWitnesses().isEmpty()) {
                    reCreated = true;
                    List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
                    
                    List<WorkStepWitness> failedList = workStepWitnesses.stream().filter(wsw -> {
                    return (wsw.getIsok().equals(1) && !CommonUtility.isNonEmpty(wsw.getWitnessflag()));
                    }).collect(Collectors.toList());
                    if (null != failedList && !failedList.isEmpty()) {
                        reCreated = true;
                        witnessService.expireByWorkStep(workStep.getId());
                    }
                }	
 */                
				if (null != workStep.getWorkStepWitnesses() && !workStep.getWorkStepWitnesses().isEmpty()) {
                    reCreated = true;
				} 
                boolean isAllSave = this.batchInsertWitness(workStep, user, monitor, witnessaddress, witnessdate);
                
				if (!reCreated) {
					if (isAllSave == false ) {
						logger.error("未能完整的创建所有的见证点。");
						workStepWitnessItemsResult.setCode("-8002");
						workStepWitnessItemsResult.setMessage("未能完整的创建所有的见证点。");
					} else {
						workStepWitnessItemsResult.setMessage("成功创建了[" + successCount + "]条见证记录。");
					}
				} else {
					workStepWitnessItemsResult.setMessage("成功重创建了[" + successCount + "]条见证记录。");
				}
				
				workStepWitnessItemsResults.add(workStepWitnessItemsResult);

				if (workStepWitnessItemsResults.stream().filter(wsir -> wsir.getCode().equals("1000")).count() > 0L) {

					rollingPlan = workStep.getRollingPlan();
					if (null != rollingPlan) {
						rollingPlan.setWitnessflag(WitnessFlagEnum.LAUNCHED.name());
						rollingPlanService.update(rollingPlan);
					}
					//为了下面调用接口准备数据　开始

	                if (instanceIdset.add(rollingPlan.getId())) {
	                	stepMapByRollKey.put(rollingPlan.getId(),new ArrayList<WorkStep>());
	                }
	                stepMapByRollKey.get(rollingPlan.getId()).add(workStep);
//					sendPointList.add(workStep);
					stepWitnessDateMap.put(workStep.getId(), workStepWitnessItemsForm);
					//为了下面调用接口准备数据　完
					workStep.setWitnessflag(WitnessFlagEnum.LAUNCHED.name());
					workStepService.add(workStep);
				}else{
					logger.error("创建见证记录失败。rollingPlan.getId="+rollingPlan.getId());
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("创建见证记录失败。");
					workStepWitnessItemsResults.add(workStepWitnessItemsResult);
					throw new RuntimeException("创建见证记录失败。rollingPlan.getId="+rollingPlan.getId());
				}
				}
			}

	        //按滚动计划为单位，逐个推送到Enpower			
	        
			if(witingPlans.size()>0){
				executePushToEnpowerServiceImpl.insertPointToEnpower(witingPlans);
			}
			if (!reCreated) {
				Integer monitorId = rollingPlanService.getConsmoniterIdBy(null,null,null,null,workStepWitnessItemsResults.get(0).getId(),null);
				if (null != monitorId) {//user.getParent()) {
					String message = user.getRealname() + " 发起了见证请求，请及时处理。";
			        JPushExtra extra = new JPushExtra();
					extra.setCategory(JPushCategoryEnums.WITNESS_LAUNCH.name());
					jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_LAUNCH.getName(), monitorId);
				}
			}
	        
	        return workStepWitnessItemsResults;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * 批量新增见证
	 * @param workStep
	 * @param user
	 * @param monitor
	 * @param witnessaddress
	 * @param witnessdate
	 * @return
	 */
	public boolean batchInsertWitness(WorkStep workStep,User user,User monitor,String witnessaddress,Date witnessdate){

		//过期历史　历史取消见证
		witnessHisServiceImpl.expireByWorkStepId(workStep.getId());
		
		
		String uuid = CommonUtility.generateUUID();
		Date createOn = new Date();
		
		List<WorkStepWitness> batchInsertList = new ArrayList<WorkStepWitness>();
		List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
		
		int count = 0;
		int successCount = 0;
		
		if (CommonUtility.isNonEmpty(workStep.getNoticeaqc1())) {
			count++;
			WorkStepWitness	workStepWitness = new WorkStepWitness();
			workStepWitness.setWorkStep(workStep);
			workStepWitness.setNoticePoint(NoticePointType.QC1.name());
			workStepWitness.setNoticeType(workStep.getNoticeaqc1());
			workStepWitness.setWitnessmonitor(monitor.getId());
			workStepWitness.setWitnessaddress(witnessaddress);
			workStepWitness.setWitnessdate(witnessdate);
			workStepWitness.setStatus(0);
			workStepWitness.setIsok(0);
			workStepWitness.setCreatedBy(user.getId());
			workStepWitness.setCreatedOn(createOn);
			workStepWitness.setBatchId(uuid);
			
			if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
				WorkStepWitness wsw = workStepWitnesses.stream().filter(w ->{
					return (w.getNoticePoint().equals(NoticePointType.QC1.name()) && !CommonUtility.isNonEmpty(w.getWitnessflag()));
				}).findFirst().orElse(null);
				
				if (null != wsw) {
					//TODO EXPIRED 之一
					wsw.setWitnessflag(WitnessFlagEnum.EXPIRED.name());
					witnessService.add(wsw);
					
					workStepWitness.setWitness(wsw.getWitness());
					workStepWitness.setWitnessteam(wsw.getWitnessteam());
					workStepWitness.setWitnesser(wsw.getWitnesser());
				}
			}
			
			WorkStepWitnessHis his = witnessHisServiceImpl.findByStepnoAndWitnessflagAndNoticePoint(workStep, WitnessFlagEnum.CANCEL.toString(), NoticePointType.QC1.name());
			if(his!=null){//在历史取消记录添加上
				workStepWitness.setWitnessteam(his.getWitnessteam());
				workStepWitness.setWitness(his.getWitness());
				workStepWitness.setWitnesser(his.getWitnesser());
			}

			batchInsertList.add(workStepWitness);
			successCount += (witnessService.add(workStepWitness) == null ? 0 : 1);
			
			String message = user.getRealname() + " 请求再次见证工序 " + workStep.getStepno() + ":" + workStep.getStepname() + " [" + NoticePointType.QC1.getName() + "]";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_RELAUNCH.name());
			if (null != workStepWitness.getWitnesser()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitnesser());
			} else if (null != workStepWitness.getWitness()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitness());
			}
		}
		if (CommonUtility.isNonEmpty(workStep.getNoticeaqc2())) {
			count++;
			WorkStepWitness	workStepWitness = new WorkStepWitness();
			workStepWitness.setWorkStep(workStep);
			workStepWitness.setNoticePoint(NoticePointType.QC2.name());
			workStepWitness.setNoticeType(workStep.getNoticeaqc2());
			workStepWitness.setWitnessmonitor(monitor.getId());
			workStepWitness.setWitnessaddress(witnessaddress);
			workStepWitness.setWitnessdate(witnessdate);
			workStepWitness.setStatus(0);
			workStepWitness.setIsok(0);
			workStepWitness.setCreatedBy(user.getId());
			workStepWitness.setCreatedOn(createOn);
			workStepWitness.setBatchId(uuid);
			
			if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
				WorkStepWitness wsw = workStepWitnesses.stream().filter(w ->{
					return (w.getNoticePoint().equals(NoticePointType.QC2.name()) && !CommonUtility.isNonEmpty(w.getWitnessflag()));
				}).findFirst().orElse(null);
				
				if (null != wsw) {
					wsw.setWitnessflag(WitnessFlagEnum.EXPIRED.name());
					witnessService.add(wsw);
					
					workStepWitness.setWitness(wsw.getWitness());
					workStepWitness.setWitnessteam(wsw.getWitnessteam());
					workStepWitness.setWitnesser(wsw.getWitnesser());
				}
			}

			WorkStepWitnessHis his = witnessHisServiceImpl.findByStepnoAndWitnessflagAndNoticePoint(workStep, WitnessFlagEnum.CANCEL.toString(), NoticePointType.QC2.name());
			if(his!=null){//在历史取消记录添加上
				workStepWitness.setWitnessteam(his.getWitnessteam());
				workStepWitness.setWitness(his.getWitness());
				workStepWitness.setWitnesser(his.getWitnesser());
			}
			batchInsertList.add(workStepWitness);
			successCount += (witnessService.add(workStepWitness) == null ? 0 : 1);
			
			String message = user.getRealname() + " 请求再次见证工序 " + workStep.getStepno() + ":" + workStep.getStepname() + " [" + NoticePointType.QC2.getName() + "]";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_RELAUNCH.name());
			if (null != workStepWitness.getWitnesser()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitnesser());
			} else if (null != workStepWitness.getWitness()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitness());
			}
		}
		if (CommonUtility.isNonEmpty(workStep.getNoticeczecqa())) {
			count++;
			WorkStepWitness	workStepWitness = new WorkStepWitness();
			workStepWitness.setWorkStep(workStep);
			workStepWitness.setNoticePoint(NoticePointType.CZEC_QA.name());
			workStepWitness.setNoticeType(workStep.getNoticeczecqa());
			workStepWitness.setWitnessmonitor(monitor.getId());
			workStepWitness.setWitnessaddress(witnessaddress);
			workStepWitness.setWitnessdate(witnessdate);
			workStepWitness.setStatus(0);
			workStepWitness.setIsok(0);
			workStepWitness.setCreatedBy(user.getId());
			workStepWitness.setCreatedOn(createOn);
			workStepWitness.setBatchId(uuid);
			
			if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
				WorkStepWitness wsw = workStepWitnesses.stream().filter(w ->{
					return (w.getNoticePoint().equals(NoticePointType.CZEC_QA.name()) && !CommonUtility.isNonEmpty(w.getWitnessflag()));
				}).findFirst().orElse(null);
				
				if (null != wsw) {
					wsw.setWitnessflag(WitnessFlagEnum.EXPIRED.name());
					witnessService.add(wsw);
					
					workStepWitness.setWitness(wsw.getWitness());
					workStepWitness.setWitnessteam(wsw.getWitnessteam());
					workStepWitness.setWitnesser(wsw.getWitnesser());
				}
			}

			WorkStepWitnessHis his = witnessHisServiceImpl.findByStepnoAndWitnessflagAndNoticePoint(workStep, WitnessFlagEnum.CANCEL.toString(), NoticePointType.CZEC_QA.name());
			if(his!=null){//在历史取消记录添加上
				workStepWitness.setWitnessteam(his.getWitnessteam());
				workStepWitness.setWitness(his.getWitness());
				workStepWitness.setWitnesser(his.getWitnesser());
			}
			batchInsertList.add(workStepWitness);
			successCount += (witnessService.add(workStepWitness) == null ? 0 : 1);
			
			String message = user.getRealname() + " 请求再次见证工序 " + workStep.getStepno() + ":" + workStep.getStepname() + " [" + NoticePointType.CZEC_QA.getName() + "]";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_RELAUNCH.name());
			if (null != workStepWitness.getWitnesser()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitnesser());
			} else if (null != workStepWitness.getWitness()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitness());
			}
		}
		if (CommonUtility.isNonEmpty(workStep.getNoticeczecqc())) {
			count++;
			WorkStepWitness	workStepWitness = new WorkStepWitness();
			workStepWitness.setWorkStep(workStep);
			workStepWitness.setNoticePoint(NoticePointType.CZEC_QC.name());
			workStepWitness.setNoticeType(workStep.getNoticeczecqc());
			workStepWitness.setWitnessmonitor(monitor.getId());
			workStepWitness.setWitnessaddress(witnessaddress);
			workStepWitness.setWitnessdate(witnessdate);
			workStepWitness.setStatus(0);
			workStepWitness.setIsok(0);
			workStepWitness.setCreatedBy(user.getId());
			workStepWitness.setCreatedOn(createOn);
			workStepWitness.setBatchId(uuid);
			
			if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
				WorkStepWitness wsw = workStepWitnesses.stream().filter(w ->{
					return (w.getNoticePoint().equals(NoticePointType.CZEC_QC.name()) && !CommonUtility.isNonEmpty(w.getWitnessflag()));
				}).findFirst().orElse(null);
				
				if (null != wsw) {
					wsw.setWitnessflag(WitnessFlagEnum.EXPIRED.name());
					witnessService.add(wsw);   
					
					workStepWitness.setWitness(wsw.getWitness());
					workStepWitness.setWitnessteam(wsw.getWitnessteam());
					workStepWitness.setWitnesser(wsw.getWitnesser());
				}
			}

			WorkStepWitnessHis his = witnessHisServiceImpl.findByStepnoAndWitnessflagAndNoticePoint(workStep, WitnessFlagEnum.CANCEL.toString(), NoticePointType.CZEC_QC.name());
			if(his!=null){//在历史取消记录添加上
				workStepWitness.setWitnessteam(his.getWitnessteam());
				workStepWitness.setWitness(his.getWitness());
				workStepWitness.setWitnesser(his.getWitnesser());
			}
			batchInsertList.add(workStepWitness);
			successCount += (witnessService.add(workStepWitness) == null ? 0 : 1);
			
			String message = user.getRealname() + " 请求再次见证工序 " + workStep.getStepno() + ":" + workStep.getStepname() + " [" + NoticePointType.CZEC_QC.getName() + "]";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_RELAUNCH.name());
			if (null != workStepWitness.getWitnesser()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitnesser());
			} else if (null != workStepWitness.getWitness()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitness());
			}
		}
		if (CommonUtility.isNonEmpty(workStep.getNoticepaec())) {
			count++;
			WorkStepWitness	workStepWitness = new WorkStepWitness();
			workStepWitness.setWorkStep(workStep);
			workStepWitness.setNoticePoint(NoticePointType.PAEC.name());
			workStepWitness.setNoticeType(workStep.getNoticepaec());
			workStepWitness.setWitnessmonitor(monitor.getId());
			workStepWitness.setWitnessaddress(witnessaddress);
			workStepWitness.setWitnessdate(witnessdate);
			workStepWitness.setStatus(0);
			workStepWitness.setIsok(0);
			workStepWitness.setCreatedBy(user.getId());
			workStepWitness.setCreatedOn(createOn);
			workStepWitness.setBatchId(uuid);
			
			if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
				WorkStepWitness wsw = workStepWitnesses.stream().filter(w ->{
					return (w.getNoticePoint().equals(NoticePointType.PAEC.name()) && !CommonUtility.isNonEmpty(w.getWitnessflag()));
				}).findFirst().orElse(null);
				
				if (null != wsw) {
					wsw.setWitnessflag(WitnessFlagEnum.EXPIRED.name());
					witnessService.add(wsw);
					
					workStepWitness.setWitness(wsw.getWitness());
					workStepWitness.setWitnessteam(wsw.getWitnessteam());
					workStepWitness.setWitnesser(wsw.getWitnesser());
				}
			}

			WorkStepWitnessHis his = witnessHisServiceImpl.findByStepnoAndWitnessflagAndNoticePoint(workStep, WitnessFlagEnum.CANCEL.toString(), NoticePointType.PAEC.name());
			if(his!=null){//在历史取消记录添加上
				workStepWitness.setWitnessteam(his.getWitnessteam());
				workStepWitness.setWitness(his.getWitness());
				workStepWitness.setWitnesser(his.getWitnesser());
			}
			batchInsertList.add(workStepWitness);
			successCount += (witnessService.add(workStepWitness) == null ? 0 : 1);
			
			String message = user.getRealname() + " 请求再次见证工序 " + workStep.getStepno() + ":" + workStep.getStepname() + " [" + NoticePointType.PAEC.getName() + "]";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_RELAUNCH.name());
			if (null != workStepWitness.getWitnesser()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitnesser());
			} else if (null != workStepWitness.getWitness()) {
				jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RELAUNCH.getName(), workStepWitness.getWitness());
			}
		}
		
//批量插入事务没有，以后再看了。
//		int savenum = witnessService.batchAdd(batchInsertList);
//		return count == savenum ;
		return count == successCount ;
	}

	//根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	/**
	 * 
	 * @param map [planIds 计划数组]
	 * @return
	 */
	public List<WorkStepForBatchWitness> getWorkStep(Map<String,Object> map){
		return workStepService.getWorkStep(map);
	}

	@Override
	public List<WorkStepWitnessItemsResult> cancelWitness(List<Integer> ids, User user) {
		List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = new ArrayList<WorkStepWitnessItemsResult>();
		
		if (null != ids) {
			for (Integer id : ids) {
				WorkStepWitnessItemsResult workStepWitnessItemsResult = new WorkStepWitnessItemsResult();
				workStepWitnessItemsResult.setId(id);
				WorkStep workStep = workStepService.findById(id);
				if (null != workStep) {
					
					List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
					if (null != workStepWitnesses) {
						workStepWitnesses = workStepWitnesses.stream().filter(
								wsw -> StringUtils.isBlank(wsw.getWitnessflag()) || WitnessFlagEnum.EXPIRED.name().equals(wsw.getWitnessflag())
								).collect(Collectors.toList());
						if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
							long count = workStepWitnesses.stream().filter(wsw -> wsw.getIsok() == 0).count();
							if (count == workStepWitnesses.size()) {
								for (WorkStepWitness wsw : workStepWitnesses) {
									wsw.setWitnessflag(WitnessFlagEnum.CANCEL.name());
									witnessService.delete(wsw.getId());
									witnessHisServiceImpl.add(wsw);
//									witnessService.add(wsw);
									
									String message = user.getRealname() + " 取消了一条见证。";
									JPushExtra extra = new JPushExtra();
									extra.setCategory(JPushCategoryEnums.WITNESS_CANCEL.name());
									if (null != wsw.getWitnesser()) {
										jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_CANCEL.getName(), wsw.getWitnesser());
									} else if (null != wsw.getWitness()) {
										jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_CANCEL.getName(), wsw.getWitness());
									}
								}
								
								
//								取消了之后，我个人认为应该把witnessflag置为空！
								workStep.setWitnessflag(null);
								workStepService.add(workStep);
//								rollingplan　在所有工序都取消的时候　Witnessflag　置为空
								rollingPlanService.updateWitnessflagNull(workStep.getRollingPlan());

								workStepWitnessItemsResult.setMessage("成功取消见证。");
							} else {
								workStepWitnessItemsResult.setCode("-1002");
								workStepWitnessItemsResult.setMessage("id对应的工序存在已经填写结果的见证条目。");
							}
						} else {
							workStepWitnessItemsResult.setCode("-1002");
							workStepWitnessItemsResult.setMessage("id对应的工序没有需要取消的见证。");
						}
					}
				} else {
					workStepWitnessItemsResult.setCode("-1002");
					workStepWitnessItemsResult.setMessage("id对应的工序没有找到。");
				}
				workStepWitnessItemsResults.add(workStepWitnessItemsResult);
			}
		}
		return workStepWitnessItemsResults;
	}
}
