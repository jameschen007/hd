package com.easycms.hd.api.service.impl;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.request.qualityctrl.QcAssignRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcNoteRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcRenovateResultRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcResultCheckEnum;
import com.easycms.hd.api.request.qualityctrl.QualityControlPageRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityControlRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityReasonCodeEnum;
import com.easycms.hd.api.request.qualityctrl.QualityTypeFormEnum;
import com.easycms.hd.api.request.qualityctrl.QualityTypePageRequestForm;
import com.easycms.hd.api.response.hseproblem.HseProblemFileResult;
import com.easycms.hd.api.response.hseproblem.HseResponsibleDeptResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlPageDataResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlResult;
import com.easycms.hd.api.service.QualityControlApiService;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.domain.QualityControlFile;
import com.easycms.hd.qualityctrl.domain.QualityControlFileType;
import com.easycms.hd.qualityctrl.domain.QualityControlStatus;
import com.easycms.hd.qualityctrl.domain.QualityControlStep;
import com.easycms.hd.qualityctrl.domain.QualityControlStepDetails;
import com.easycms.hd.qualityctrl.domain.QualityControlStepStatus;
import com.easycms.hd.qualityctrl.service.QualityControlFileService;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.easycms.hd.qualityctrl.service.QualityControlStepService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Service("qualityControlApiService")
@Slf4j
@javax.transaction.Transactional
public class QualityControlApiServiceImpl implements QualityControlApiService {
	@Autowired
	private QualityControlService qualityControlService;
	@Autowired
	private QualityControlFileService qualityControlFileService;
	@Autowired
	private QualityControlStepService qualityControlStepService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

	@Override
	public boolean createQualityProblem(QualityControlRequestForm qcRequestForm, CommonsMultipartFile[] files) throws Exception {
		boolean status = false;
		if(qcRequestForm.getProblemDescription() !=null && qcRequestForm.getProblemDescription().length()>400){
			qcRequestForm.setProblemDescription(qcRequestForm.getProblemDescription().substring(0,400));
		}
		if(qcRequestForm.getArea()!=null && qcRequestForm.getArea().length()>50){
			qcRequestForm.setArea(qcRequestForm.getArea().substring(0, 50));
		}
		if(qcRequestForm.getSystem()!=null && qcRequestForm.getSystem().length()>50){
			qcRequestForm.setSystem(qcRequestForm.getSystem().substring(0, 50));
		}
		
		QualityControl qc = new QualityControl();
		qc.setUnit(qcRequestForm.getUnit());
		qc.setSubitem(qcRequestForm.getSubitem());
		qc.setFloor(qcRequestForm.getFloor());
		qc.setRoomnum(qcRequestForm.getRoomnum());
		qc.setSystem(qcRequestForm.getSystem());
		if(qcRequestForm.getType() != null){
			qc.setType(qcRequestForm.getType().getIndex());			
		}
		qc.setArea(qcRequestForm.getArea());
		qc.setResponsibleDept(qcRequestForm.getResponsibleDept());
		qc.setResponsibleTeam(qcRequestForm.getResponsibleTeam());
		qc.setProblemDescription(qcRequestForm.getProblemDescription());
		qc.setCreateUser(qcRequestForm.getLoginId());
		qc.setCreateDate(Calendar.getInstance().getTime());
		qc.setStatus(QualityControlStatus.PreQCLeaderAssign.toString());
		qualityControlService.add(qc);
		if(qc.getId() != 0 || qc.getId() != null){
			status = true;
		}
		if(status && files!=null && files.length>0 ) {
			//处理上传图片信息
			boolean flag = moreUpload(qc.getId(),files,QualityControlFileType.before.toString());
			log.info("创建质量问题id："+qc.getId()+" 图片保存成功:"+flag);
		}

		if(status){
			//更新处理步骤
			QualityControlStep qcStep = new QualityControlStep();
			qcStep.setQcId(qc.getId());
			qcStep.setStepDetails(QualityControlStepDetails.QCLeaderAssign.toString());
			qcStep.setStepStatus(QualityControlStepStatus.Pending.toString());
			qualityControlStepService.add(qcStep);
			
		}else{
			log.error("质量问题创建失败！");
		}
		
//		if(true){
//			throw new RuntimeException("test exception wz");
//		}
		return status;
	}

	@Override
	public QualityControlPageDataResult getQualityProblemPageResult(Page<QualityControl> page,QualityControlPageRequestForm qcRequestForm) {
		QualityControlPageDataResult qcPageResult = new QualityControlPageDataResult();
		Page<QualityControl> qualityControls = new Page<QualityControl>();
		String qcStatus = null;
		
		if(qcRequestForm.getStatus() != null){
			qcStatus = qcRequestForm.getStatus().toString();
		}
		
		Integer loginId = qcRequestForm.getLoginId();
		if(loginId == null || loginId.intValue() == 0){
			return null;
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser == null){
			return null;
		}
		Integer deptId = loginUser.getDepartment().getId();
		//判断角色，返回对应数据
		if(userService.isRoleOf(loginUser, "QCManager")){
			qualityControls = qualityControlService.findByStatus(page, qcStatus);
		}else if(userService.isRoleOf(loginUser, new String[]{
				ConstantVar.project_general_manager
				,ConstantVar.project_vice_manager
				,ConstantVar.solver4
				,ConstantVar.engineering_manager
				,ConstantVar.commerce_manager
				,ConstantVar.QHSE_manager
				,ConstantVar.QC_dept_manager
				,ConstantVar.QC_dept_vice_manager
				,ConstantVar.QA_dept_manager
				,ConstantVar.QA_dept_vice_manager
				,ConstantVar.HSE_dept_manager
				,ConstantVar.HSE_dept_vice_manager
				 
				,ConstantVar.technical_dept_vice_manager
				,ConstantVar.technical_dept_manager
				,ConstantVar.engineering_dept_manager
				,ConstantVar.engineering_dept_vice_manager
				,ConstantVar.commerce_dept_manager
				,ConstantVar.commerce_dept_vice_manager
		})){//项目总经理、项目部副总经理（技术经理，工程经理，商务经理，QHSE经理），QC部经理、副经理，QA部经理、副经理，HSE部经理、副经理。技术部经理、副经理，工程部经理、副经理 ，商务部经理，副经理。管焊队，电仪队，机通队，主系统队，队长副队长
			qualityControls = qualityControlService.findByStatus(page, qcStatus);
		}else if(userService.isRoleOf(loginUser, new String[]{ConstantVar.witness_team_qc1,ConstantVar.witness_team_qc2,ConstantVar.witness_member_qc1,ConstantVar.witness_member_qc2,ConstantVar.qc_member})){//"QC1")){
			qualityControls = qualityControlService.findByQcUser(page, loginId, qcStatus);
		}else if(departmentService.isFromDept(loginUser, departmentService.responsibilityDept())){//是责任部门的人，才查询出来分派

			//当前登录人是否是责任部门人员
			/**
			 * 机通队
				主系统队
				电仪队
				管焊队
			 */
			//班长登录则查看本班组的所有质量问题
			if(loginUser.getDepartment() != null){
				//查出子部门，传部门ｉｄ集合查询
				qualityControls = qualityControlService.findByDept(page, loginUser.getDepartment().getId(),loginId, qcStatus);				
			}else{
				return null;
			}
		}else if(userService.isRoleOf(loginUser, "monitor")||userService.isRoleOf(loginUser,ConstantVar.TEAM)){
			
			//班长登录则查看本班组的所有质量问题
			if(loginUser.getDepartment() != null){
				qualityControls = qualityControlService.findByTeamUser(page, loginId, qcStatus);//.findByTeam(page,deptList,loginId, qcStatus);	
			}else{
				return null;
			}
		}else if(false){//队长，副队长  责任部门 （机通队 ，管焊队） 的队长和副队长有权限查看当前部门的所以问题   电仪队队长、副队长，管焊队队长、副队长，机通队队长、副队长，主系统队队长、副队长；
			
			//班长登录则查看本班组的所有质量问题
			if(loginUser.getDepartment() != null){
				qualityControls = qualityControlService.findByTeamUser(page, loginId, qcStatus);//.findByTeam(page,deptList,loginId, qcStatus);	
			}else{
				return null;
			}
		}else{
			qualityControls = qualityControlService.findByCreateUser(page, loginId, qcStatus);
		}
		
		qcPageResult.setPageCounts(qualityControls.getPageCount());
		qcPageResult.setPageNum(qualityControls.getPageNum());
		qcPageResult.setPageSize(qualityControls.getPagesize());
		qcPageResult.setTotalCounts(qualityControls.getTotalCounts());
		if (null != qualityControls.getDatas()) {
			qcPageResult.setData(qualityControls.getDatas().stream().map(qc -> {
				QualityControlResult qcResult = transferToResultList(qc);
				return qcResult;
			}).collect(Collectors.toList()));
		}
		return qcPageResult;
	}
	
	@Override
	public QualityControlPageDataResult getQualityProblemPageResultByType(Page<QualityControl> page,QualityTypePageRequestForm qcRequestForm){
		QualityControlPageDataResult qcPageResult = new QualityControlPageDataResult();
		Page<QualityControl> qualityControls = new Page<QualityControl>();
		
		if(qcRequestForm.getQueryId() == null){
			return null;
		}
		String problemStatus = null;
		if(qcRequestForm.getStatus() != null){
			problemStatus = qcRequestForm.getStatus().toString();
		}
		
		//根据不同身份类型查询相应数据
		if(qcRequestForm.getType().compareTo(QualityTypeFormEnum.MINE)==0){
			qualityControls = qualityControlService.findByCreateUser(page, qcRequestForm.getLoginId(),problemStatus);
		}else if(qcRequestForm.getType().compareTo(QualityTypeFormEnum.QC)==0){
			qualityControls = qualityControlService.findByQcUser(page, qcRequestForm.getQueryId(),problemStatus);			
		}else if(qcRequestForm.getType().compareTo(QualityTypeFormEnum.DEPT)==0){
			qualityControls = qualityControlService.findByDept(page, qcRequestForm.getQueryId(),qcRequestForm.getLoginId(),problemStatus);
		}else if(qcRequestForm.getType().compareTo(QualityTypeFormEnum.TEAM)==0){
			qualityControls = qualityControlService.findByTeam(page, qcRequestForm.getQueryId(),qcRequestForm.getLoginId(), problemStatus);
		}
		
		qcPageResult.setPageCounts(qualityControls.getPageCount());
		qcPageResult.setPageNum(qualityControls.getPageNum());
		qcPageResult.setPageSize(qualityControls.getPagesize());
		qcPageResult.setTotalCounts(qualityControls.getTotalCounts());
		if (null != qualityControls.getDatas()) {
			qcPageResult.setData(qualityControls.getDatas().stream().map(qc -> {
				QualityControlResult qcResult = transferToResultList(qc);
				return qcResult;
			}).collect(Collectors.toList()));
		}
		return qcPageResult;
	}
	
	public QualityControlResult transferToResultList(QualityControl qc){
		QualityControlResult result = new QualityControlResult();
		result.setId(qc.getId());
		result.setUnit(qc.getUnit());
		result.setSubitem(qc.getSubitem());
		result.setFloor(qc.getFloor());
		result.setRoomnum(qc.getRoomnum());
		result.setSystem(qc.getSystem());
		result.setArea(qc.getArea());
		if(qc.getType() != null){
			result.setType(QualityReasonCodeEnum.getName(qc.getType()));
		}
		//责任部门转换为返回数据
		if(qc.getResponsibleDept()!=null){
			Department responsibleDept = departmentService.findById(qc.getResponsibleDept());
			if(responsibleDept != null){
				HseResponsibleDeptResult responsibleDeptRs = new HseResponsibleDeptResult();
				responsibleDeptRs.setDeptId(responsibleDept.getId().toString());
				responsibleDeptRs.setDeptName(responsibleDept.getName());
				result.setResponsibleDept(responsibleDeptRs);							
			}
		}
		//责任班组转换为返回数据
		if(qc.getResponsibleTeam()!=null){
			Department responsibleTeam = departmentService.findById(qc.getResponsibleTeam());
			if(responsibleTeam != null){
				HseResponsibleDeptResult responsibleTeamRs = new HseResponsibleDeptResult();
				responsibleTeamRs.setDeptId(responsibleTeam.getId().toString());
				responsibleTeamRs.setDeptName(responsibleTeam.getName());
				result.setResponsibleTeam(responsibleTeamRs);							
			}
			if(qc.getRenovateTeam() != null){
				result.setRenovateTeam(responsibleTeam.getName());			
			}
		}
		result.setProblemDescription(qc.getProblemDescription());
		
		//处理返回的图片信息
		List<QualityControlFile> files = qualityControlFileService.findByQctrlId(qc.getId());
		List<HseProblemFileResult> r = new ArrayList<HseProblemFileResult>();
		if(files!=null && files.size()>0){
			for(QualityControlFile f:files){
				HseProblemFileResult r1 = new HseProblemFileResult();
//				String visitPath = com.easycms.common.logic.context.ContextPath.selfProjectContextPath;
				r1.setPath("/hdxt/api/files/problem/"+f.getFilepath());
				r1.setFileName(f.getFilename());
				r1.setFileType(f.getFiletype());
				r.add(r1);
			}
		}
		result.setFiles(r);
		
		if(qc.getCreateUser() != null){
			Integer createUser = qc.getCreateUser();
			User  u = this.userService.findUserById(createUser);
			
			result.setCreateUser(qc.getCreateUser().toString());
			if(u!=null){
				result.setCreateUserName(u.getRealname());
			}
		}
		if(qc.getQcUser() != null){
			result.setQcUserId(qc.getQcUser());
			User u = userService.findUserById(qc.getQcUser());
			result.setQcUser(u.getRealname());	
		}
//		if(qc.getRenovateTeam() != null){
//			result.setRenovateTeam(qc.getRenovateTeam().toString());			
//		}
		
		//处理步骤数据
		List<QualityControlStep> qcStep = qualityControlStepService.findByQctrlId(qc.getId());
		result.setQcStep(qcStep);
		result.setCreateDate(qc.getCreateDate());
		result.setStatus(qc.getStatus());
		result.setRenovateDescription(qc.getRenovateDescription());
		result.setNotes(qc.getNotes());
		return result;
	}

	/**
	 * 责任部门分派责任班组
	 */
	@Override
	public boolean responsibilityLeaderAssign(QcAssignRequestForm qcRequestForm) {
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		QualityControlStep qcStep = new QualityControlStep();
		QualityControlStep qcStepNew = new QualityControlStep();
		
		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		
		if(qualityControl != null && qcRequestForm.getAssignedId() != null){
			//当前是否为待分派状态
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreUpRenovete.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法执行分派操作！");
				return false;
			}
			//指定责任班组人员，更新处理步骤为待责任班组人员处理
			qualityControl.setTeamUser(qcRequestForm.getAssignedId());
			qualityControl.setStatus(QualityControlStatus.PreRenovete.toString());
			User userAssi = userService.findUserById(qcRequestForm.getAssignedId());
			qualityControl.setRenovateTeam(userAssi.getDepartment().getId());
			if(qualityControlService.update(qualityControl) != null){
				log.info("指派给责任班组人员："+qcRequestForm.getAssignedId());
				status = true;
			}
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.responsibilityLeaderAssign.toString());	
		}
		if(qcSteps != null && !qcSteps.isEmpty()){
			//更新责任部门处理情况，状态更新为已分派
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
			qualityControlStepService.update(qcStep);
		}
		if(qcStep.getId() != null){
			//新增责任班组人员处理情况，状态为待处理状态	
			qcStepNew.setQcId(qualityControl.getId());
			qcStepNew.setStepDetails(QualityControlStepDetails.TeamRenovete.toString());
			qcStepNew.setStepStatus(QualityControlStepStatus.Pending.toString());
			qcStepNew.setStepDate(new Date());
			qualityControlStepService.add(qcStepNew);			
		}
		if(qcStepNew.getId() == null){
			status = false;
		}
		return status;
	}

	@Override
	public boolean qcLeaderAssign(QcAssignRequestForm qcRequestForm) {
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		QualityControlStep qcStep = new QualityControlStep();
		QualityControlStep qcStepNew = new QualityControlStep();
		
		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		
		if(qualityControl != null && qcRequestForm.getAssignedId() != null){
			//当前是否为待分派状态
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreQCLeaderAssign.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法执行分派操作！");
				return false;
			}
			//指定QC人员，更新处理步骤为待QC人员处理
			qualityControl.setQcUser(qcRequestForm.getAssignedId());
			qualityControl.setStatus(QualityControlStatus.PreQCAssign.toString());
			if(qualityControlService.update(qualityControl) != null){
				log.info("指派给QC人员："+qcRequestForm.getAssignedId());
				status = true;
			}
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.QCLeaderAssign.toString());	
		}
		if(qcSteps != null && !qcSteps.isEmpty()){
			//更新QC专业室主任处理情况，状态更新为已分派
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
			qualityControlStepService.update(qcStep);
		}
		if(qcStep.getId() != null){
			//新增QC人员处理情况，状态为待处理状态	
			qcStepNew.setQcId(qualityControl.getId());
			qcStepNew.setStepDetails(QualityControlStepDetails.QCAssign.toString());
			qcStepNew.setStepStatus(QualityControlStepStatus.Pending.toString());
			qcStepNew.setStepDate(new Date());
			qualityControlStepService.add(qcStepNew);			
		}
		if(qcStepNew.getId() == null){
			status = false;
		}
		return status;
	}
	/**
	 * QC人员指定整改队伍，增加　可修改责任部门和责任班组
	 * @param qcRequestForm
	 * @return
	 */
	@Override
	public Map<String,Object> qcAssignRenovateTeam(QcAssignRequestForm qcRequestForm,Long timeLimit,Integer deptId,Integer teamId) throws Exception {
		Map<String,Object> rsMap = new HashMap<>();
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		QualityControlStep qcStep = new QualityControlStep();
		QualityControlStep qcStepNew = new QualityControlStep();
		Integer loginId = 0;
		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		if(qcRequestForm.getLoginId() != null){
			loginId = qcRequestForm.getLoginId();
		}
		
		
		if(qualityControl != null){
			//增加　可修改责任部门和责任班组 QC人员接收QC室主任分派的任务后，应当有权限对责任班组和责任部门进行重新选定编辑
			if(deptId!=null){
				qualityControl.setResponsibleDept(deptId);
			}
			if(teamId!=null){
				qualityControl.setResponsibleTeam(teamId);
			}
			//当前登录QC是否是被指派QC
			if(loginId.compareTo(qualityControl.getQcUser()) != 0){
				log.debug("当前登录ID："+loginId+"，已指派QCID："+qualityControl.getQcUser()+"，当前登录用户无法指派该问题！");
				rsMap.put("status", status);
				rsMap.put("msg", "你没有权限指派该问题！");
				return rsMap;
			}
			
			//当前状态是否为待QC处理
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreQCAssign.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法执行指派操作！");
				rsMap.put("status", status);
				rsMap.put("msg", "当前问题无法执行指派操作！");
				return rsMap;
			}
			
			
			//指定整改队伍，更新处理步骤为待整改
			SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			String d = format.format(timeLimit);
			Date date = format.parse(d);
			qualityControl.setTimeLimit(date);
//			qualityControl.setRenovateTeam(qcRequestForm.getAssignedId());
			//整改队伍为责任班组
			qualityControl.setRenovateTeam(qualityControl.getResponsibleTeam());
			qualityControl.setStatus(QualityControlStatus.PreUpRenovete.toString());
			
			//不开启质量问题单
			qualityControl.setQualityFlag(false);
			if(qualityControlService.update(qualityControl) != null){
				log.info("指派整改队伍ID："+qualityControl.getRenovateTeam());
				status = true;
			}
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.QCAssign.toString());	
		}
		if(qcSteps != null && !qcSteps.isEmpty()){
			//更新QC人员处理情况，状态更新为已指派整改队伍
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
			qualityControlStepService.update(qcStep);
		}
		if(qcStep.getId() != null){
			//新增QC人员处理情况，状态为待处理状态	
			qcStepNew.setQcId(qualityControl.getId());
			qcStepNew.setStepDetails(QualityControlStepDetails.responsibilityLeaderAssign.toString());
			qcStepNew.setStepStatus(QualityControlStepStatus.Pending.toString());
			qcStepNew.setStepDate(new Date());
			qualityControlStepService.add(qcStepNew);			
		}
		if(qcStepNew.getId() == null){
			status = false;
			rsMap.put("status", status);
			rsMap.put("msg", "执行操作失败！");
		}else{
			rsMap.put("status", status);
			rsMap.put("msg", "执行操作成功！");
		}
		return rsMap;
	}

	@Override
	public boolean qcAssignClose(QcNoteRequestForm qcRequestForm,boolean qualityFlag) throws Exception {
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		QualityControlStep qcStep = new QualityControlStep();
		Integer loginId = 0;

		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		if(qcRequestForm.getLoginId() != null){
			loginId = qcRequestForm.getLoginId();
		}
		
		if(qualityControl != null){
			//当前登录QC是否是被指派QC
			if(loginId.compareTo(qualityControl.getQcUser()) != 0){
				log.debug("当前登录ID："+loginId+"，已指派QCID："+qualityControl.getQcUser()+"，当前登录用户无法关闭该问题！");
				return false;
			}
			
			//当前状态是否为待QC处理
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreQCAssign.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法执行关闭操作！");
				return false;
			}
			qualityControl.setStatus(QualityControlStatus.Closed.toString());
			qualityControl.setNotes(qcRequestForm.getNote());
			//标识是否开启质量问题单
			qualityControl.setQualityFlag(qualityFlag);
			if(qualityControlService.update(qualityControl) != null){
				log.info("关闭问题："+qualityControl.getId()+",状态为"+qualityControl.getStatus());
				status = true;
			}

			executePushToEnpowerServiceImpl.qualityQuestionSend(qualityControl,null,null,null);
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.QCAssign.toString());	
		}
		if(qcSteps != null && !qcSteps.isEmpty()){
			//更新QC人员处理情况，状态更新为已指派整改队伍
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
			qualityControlStepService.update(qcStep);
		}
		

		
		return status;
	}

	@Override
	public boolean teamRenoveteResult(QcRenovateResultRequestForm qcRequestForm,CommonsMultipartFile files[]) {
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		QualityControlStep qcStep = new QualityControlStep();
		QualityControlStep qcStepNew = new QualityControlStep();
		
		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		//当前状态是否为整改中
		if(qualityControl != null){
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreRenovete.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法提交整改结果！");
				return false;
			}
			qualityControl.setStatus(QualityControlStatus.PreQCverify.toString());
			qualityControl.setRenovateDescription(qcRequestForm.getRenovateDescription());
			if(qualityControlService.update(qualityControl) != null){
				log.info("已提交整改结果，待QC核实结果。");
				status = true;
			}
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.TeamRenovete.toString());
		}
		if(qcSteps != null && !qcSteps.isEmpty()){
			//状态更新为已整改
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
//			qcStep.setStepNotes(null);
			qualityControlStepService.update(qcStep);
		}
		if(qcStep.getId() != null && ("".equals(qcStep.getStepNotes()) || qcStep.getStepNotes()==null)){
			//新增QC人员处理情况，状态为待核实状态	
			qcStepNew.setQcId(qualityControl.getId());
			qcStepNew.setStepDetails(QualityControlStepDetails.QCverify.toString());
			qcStepNew.setStepStatus(QualityControlStepStatus.Pending.toString());
			qcStepNew.setStepDate(new Date());
			qualityControlStepService.add(qcStepNew);
			
			//处理上传图片信息
			moreUpload(qualityControl.getId(),files,QualityControlFileType.after.toString());
		}
		else if(qcStep.getId() != null && qcStep.getStepNotes().equals(QcResultCheckEnum.Unqualified.toString())){
			//不合格问题二次申请核实
			List<QualityControlStep> qcStepTemp = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.QCverify.toString());
			if(!qcStepTemp.isEmpty()){
				qcStepNew = qcStepTemp.get(0);
				qcStepNew.setStepStatus(QualityControlStepStatus.Pending.toString());
				qcStepNew.setStepDate(new Date());
				qualityControlStepService.update(qcStepNew);
			}
			
			//处理上传图片信息
			moreUpload(qualityControl.getId(),files,QualityControlFileType.after.toString());
			
		}
		if(qcStepNew.getId() == null){
			status = false;
		}
		return status;
	}

	@Override
	public boolean qcVerifyResult(QcNoteRequestForm qcRequestForm,QcResultCheckEnum checkResult) throws Exception {
		boolean status = false;
		QualityControl qualityControl = null;
		List<QualityControlStep> qcSteps = null;
		List<QualityControlStep> qcStepsTeamRenovete = null;
		QualityControlStep qcStep = new QualityControlStep();

		if(qcRequestForm.getQcProblrmId() != null){
			qualityControl = qualityControlService.findById(qcRequestForm.getQcProblrmId());
		}
		if(qualityControl != null){
			//当前是否为待核实状态
			if(!qualityControl.getStatus().equals(QualityControlStatus.PreQCverify.toString())){
				log.debug("当前状态："+qualityControl.getStatus()+"，无法进行核实！");
				return false;
			}
			if(checkResult.equals(QcResultCheckEnum.Qualified)){
				qualityControl.setStatus(QualityControlStatus.Finished.toString());
			}
			else if(checkResult.equals(QcResultCheckEnum.Unqualified)){
				qualityControl.setStatus(QualityControlStatus.PreUpRenovete.toString());	
				qualityControl.setNotes(qcRequestForm.getNote());
				qualityControl.setTeamUser(null);//不合格，后面要重新分派？？
			}
			if(qualityControlService.update(qualityControl) != null){
				log.info("已核实问题ID:"+qualityControl.getId()+",结果为:"+checkResult.toString());
				status = true;
			}
		}else{
			log.debug("获取ID为："+qcRequestForm.getQcProblrmId()+"的数据为空！");
		}
		if(status){
			qcSteps = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.QCverify.toString());	
			qcStepsTeamRenovete = qualityControlStepService.findByQctrlIdAndStepDetails(qualityControl.getId(), QualityControlStepDetails.TeamRenovete.toString());
		}
		//更新为已核实状态
		if(qcSteps != null && !qcSteps.isEmpty()){
			qcStep = qcSteps.get(0);
			qcStep.setStepUser(qcRequestForm.getLoginId());
			qcStep.setStepDate(new Date());
			qcStep.setStepStatus(QualityControlStepStatus.Done.toString());
			qualityControlStepService.update(qcStep);
		}
		//更新整改状态
		if(!qcStepsTeamRenovete.isEmpty()){
			QualityControlStep qcstep = new QualityControlStep();
			qcstep = qcStepsTeamRenovete.get(0);
			if(checkResult.equals(QcResultCheckEnum.Qualified)){
				qcstep.setStepNotes(QcResultCheckEnum.Qualified.toString());
				executePushToEnpowerServiceImpl.qualityQuestionSend(qualityControl,qcstep.getStepUser(),qcstep.getStepDate(),Calendar.getInstance().getTime());
			}
			else{
				qcstep.setStepDate(new Date());
				qcstep.setStepStatus(QualityControlStepStatus.Pending.toString());
				qcstep.setStepNotes(QcResultCheckEnum.Unqualified.toString());				
			}
			qualityControlStepService.update(qcstep);

		}

		
		return status;
	}
	
	/**
	 * 文件上传
	 * @param problemId
	 * @param files
	 * @return
	 */
	private boolean moreUpload(Integer problemId, CommonsMultipartFile files[], String type) {
		// 上传位置  设定文件保存的目录 
		String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath;
		List<FileInfo> fileInfos = UploadUtil.moreUpload(filePath, "utf-8", true, files);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				QualityControlFile file = new QualityControlFile();
				file.setFilepath(fileInfo.getNewFilename());
				file.setUploadDate(new Date());
				file.setQcId(problemId);
				if(fileInfo.getFilename()!=null && fileInfo.getFilename().length()>50){
					file.setFilename(fileInfo.getFilename().substring(0, 50));
				}else{
					file.setFilename(fileInfo.getFilename());
				}
				file.setFiletype(type);
				qualityControlFileService.add(file);
			}
		}
		return true;
	}

}
