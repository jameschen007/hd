package com.easycms.hd.api.service.impl;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.easycms.hd.api.enums.hseproblem.Hse3mEnum;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.hseproblem.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.common.util.HttpRequest;
import com.easycms.core.util.Page;
import com.easycms.hd.api.baseservice.hseproblem.HseProblemTransForResult;
import com.easycms.hd.api.enpower.request.EnpowerRequest;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enpower.response.EnpowerResponseHseCode;
import com.easycms.hd.api.enpower.response.EnpowerResponseRoomLevel;
import com.easycms.hd.api.enpower.response.EnpowerResponseUnitWorkshop;
import com.easycms.hd.api.enums.enpower.EnpowerHSEStatus;
import com.easycms.hd.api.enums.hseproblem.HseProblemSolveStatusEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemAssignRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityReasonCodeEnum;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult2;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemResult;
import com.easycms.hd.api.response.hseproblem.HseResponsibleDeptResult;
import com.easycms.hd.api.response.hseproblem.QuaResponsibleDeptResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlOptionResult;
import com.easycms.hd.api.service.HseProblemApiService;
import com.easycms.hd.hseproblem.service.HseProblemFileService;
import com.easycms.hd.hseproblem.service.HseProblemService;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.qualityctrl.domain.QualityControlOptions;
import com.easycms.hd.qualityctrl.service.impl.QualityControlOptionsServiceImpl;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Service("hseProblemApiService")
@Slf4j
public class HseProblemApiServiceImpl implements HseProblemApiService {
	
	@Autowired
	private HseProblemService hseProblemService;
	@Autowired
	private UserService userService;
	@Autowired
	private HseProblemFileService hseProblemFileService;
	@Autowired
	private HseProblemSolveService hseProblemSolveService;
	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private ComputedConstantVar constantVar ;
	
	@Value("#{APP_SETTING['enpower_api_getdbinfo']}")
	private String enpowerApiInfo;
	@Autowired
	private ExecutePushToEnpowerServiceImpl pushToEnpowerService;
	@Autowired
	private VariableSetDao variableSetDao;
	@Autowired
	private QualityControlOptionsServiceImpl qualityControlOptionsService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerService;
	@Autowired
	private JPushService jPushService ;
	
	

	@Override
	public boolean createHseProblem(HseProblemRequestForm hseProblemRequestForm, CommonsMultipartFile files[]) throws Exception {
		log.debug("==========安全文明问题创建开始==========");
		boolean status = false;
		HseProblem hp = new HseProblem();
		User user = userService.findUserById(hseProblemRequestForm.getLoginId());
		hp.setCreateUser(user.getId());
		if(hseProblemRequestForm.getCode1Id()!=null && !"".equals(hseProblemRequestForm.getCode1Id())){
			hp.setProblemTitle(getHseCodeDesc(hseProblemRequestForm.getCode1Id()));
		}else{
			hp.setProblemTitle(hseProblemRequestForm.getProblemTitle());
		}
		hp.setCreateDate(Calendar.getInstance().getTime());
		hp.setUnit(hseProblemRequestForm.getUnit());
		hp.setWrokshop(hseProblemRequestForm.getWrokshop());
		hp.setEleration(hseProblemRequestForm.getEleration());
		hp.setRoomno(hseProblemRequestForm.getRoomno());
		hp.setDescription(hseProblemRequestForm.getDescription());
		hp.setProblemStatus(HseProblemStatusEnum.Need_Handle.toString());
		hp.setResponsibleDept(hseProblemRequestForm.getResponsibleDeptId());
		hp.setResponsibleTeam(hseProblemRequestForm.getResponsibleTeamId());
		hp.setProblemStep(HseProblemStep.isNew.toString());//新问题

		hp.setCode1Id(hseProblemRequestForm.getCode1Id());
		hp.setCode2Id(hseProblemRequestForm.getCode2Id());
		hp.setCode3Id(hseProblemRequestForm.getCode3Id());
		hp.setCode2Desc(getHseCodeDesc(hseProblemRequestForm.getCode2Id()));
		hp.setCode3Desc(getHseCodeDesc(hseProblemRequestForm.getCode3Id()));
//		hp.setCheckType(hseProblemRequestForm.getCheckType());//检查类型
//		hp.setCriticalLevel(hseProblemRequestForm.getCriticalLevel());//隐患严重性类别
		
		//切换key为value
		String checkType = "";
		String criticalLevel = "";
		HseProblemCreatUiDataResult2 forbean = new HseProblemCreatUiDataResult2();
		List<Map<String,String>> list = forbean.getCheckTypeList();
		for(Map<String,String> map:list){//检查类型
			String key = map.get("key");
			if(key.equals(hseProblemRequestForm.getCheckType())){
				checkType =  map.get("value");
				break;
			}
		}
		list = forbean.getCriticalLevelList();
		for(Map<String,String> map:list){//隐患严重性类别
			String key = map.get("key");
			if(key.equals(hseProblemRequestForm.getCriticalLevel())){
				criticalLevel = map.get("value");
				break;
			}
		}
		
		
		hp.setCheckType(checkType);
		hp.setCriticalLevel(criticalLevel);
		
		hp.setRequirement(hseProblemRequestForm.getRequirement());//整改要求
		hseProblemService.add(hp);//创建安全文明问题
		if(hp.getId() != 0 || hp.getId()!=null){
			status = true;
		}
		if(status && files!=null) {
			moreUpload(hp.getId(), files,HseProblemFileType.before.toString());
		}
		if(status){
			HseProblemSolve hseProblemSolve = new HseProblemSolve();
			hseProblemSolve.setProblemId(hp.getId());
			hseProblemSolve.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
			hseProblemSolve.setSolveStep(HseProblemSolveStep.hseConfirm.toString());
			hseProblemSolveService.add(hseProblemSolve);
			
			pushToEnpowerService.hSEQuestionSend(hp);			
		}else{
			log.error("安全文明问题创建出现异常！");
		}
		
		log.debug("==========安全文明问题创建完成 问题ID："+hp.getId()+"===");
		return status;
	}

	@Override
	public HseProblemPageDataResult getHseProblemPageDataResult(Page<HseProblem> page, String problemStatus, String solveStatus,User user,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) throws Exception {
		HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
		Page<HseProblem> hseProblems = new Page<HseProblem>();

		Department userDept = user.getDepartment();
		int userDeptId = 0;
		if(userDept != null){
			userDeptId = userDept.getId();
		}
		
		String userRole = "isOther";
		if(userService.isDepartmentOf(user, "HSEDepartment")){
			userRole = "isHSE";
		}else if(userService.isRoleOf(user, "monitor")){
			userRole = "isMoniter";
		}else if(userService.isRoleOf(user, "captain")){
			userRole = "isCaptain";
//		}else if(userService.isRoleOf(user, "team")){
//			userRole = "isDeptTop";
		}
		
		
		//安全文明施工问题进度状态与当前处理情况
		if(CommonUtility.isNonEmpty(solveStatus) && CommonUtility.isNonEmpty(problemStatus)){
			Set<String> ProblemIds = hseProblemSolveService.findByStatus(solveStatus);
			if(!ProblemIds.isEmpty()){
				if (!StringUtils.isBlank(keyword)) {
					hseProblems = hseProblemService.findByProblemAndSolveStatus(page, problemStatus, ProblemIds,keyword);
				}else{
					hseProblems = hseProblemService.findByProblemAndSolveStatus2(page, problemStatus, solveStatus, hseProblemExListRequestForm);
				}
			}
//		}else{
//			//TODO 增加提出时间和截止时间筛选
//		分角色部门获取数据
		}else if(userDeptId!=0){
				if(userRole.equals("isHSE")){
					if(CommonUtility.isNonEmpty(problemStatus)){
						hseProblems = hseProblemService.findByProblemStatus(page, problemStatus,keyword,hseProblemExListRequestForm);
					}else{
						hseProblems = hseProblemService.findAll(page,keyword,  hseProblemExListRequestForm);
					}
//				}else if(userRole.equals("isDeptTop")){
//					hseProblems = hseProblemService.findByDept(page, problemStatus, userDeptId);
				}else if(userService.isRoleOf(user, new String[]{
						ConstantVar.project_general_manager
						,ConstantVar.project_vice_manager
						,ConstantVar.solver4
						,ConstantVar.engineering_manager
						,ConstantVar.commerce_manager
						,ConstantVar.QHSE_manager
						,ConstantVar.QC_dept_manager
						,ConstantVar.QC_dept_vice_manager
						,ConstantVar.QA_dept_manager
						,ConstantVar.QA_dept_vice_manager
						,ConstantVar.HSE_dept_manager
						,ConstantVar.HSE_dept_vice_manager
	 
						,ConstantVar.technical_dept_vice_manager
						,ConstantVar.technical_dept_manager

						,ConstantVar.engineering_dept_manager
						,ConstantVar.engineering_dept_vice_manager
						,ConstantVar.commerce_dept_manager
						,ConstantVar.commerce_dept_vice_manager
				})){//项目总经理、项目部副总经理（技术经理，工程经理，商务经理，QHSE经理），QC部经理、副经理，QA部经理、副经理，HSE部经理、副经理。技术部经理、副经理，工程部经理、副经理 ，商务部经理，副经理。管焊队，电仪队，机通队，主系统队，队长副队长
					if(CommonUtility.isNonEmpty(problemStatus)){
						hseProblems = hseProblemService.findByProblemStatus(page, problemStatus,keyword,  hseProblemExListRequestForm);
					}else{
						hseProblems = hseProblemService.findAll(page,keyword,  hseProblemExListRequestForm);
					}
				}else if(userRole.equals("isMoniter")){
					hseProblems = hseProblemService.findByTeam(page, problemStatus, userDeptId,keyword,  hseProblemExListRequestForm);
				}else if(userRole.equals("isCaptain")){
					hseProblems = hseProblemService.findByCaptain(page, problemStatus, userDeptId,keyword, hseProblemExListRequestForm);
				}else{
					hseProblems = hseProblemService.findByCreateUser(page, problemStatus, user.getId(),keyword, hseProblemExListRequestForm);
				}
		}
		else{
			return hseProblemPageDataResult;
		}

		hseProblemPageDataResult.setPageCounts(hseProblems.getPageCount());
		hseProblemPageDataResult.setPageNum(hseProblems.getPageNum());
		hseProblemPageDataResult.setPageSize(hseProblems.getPagesize());
		hseProblemPageDataResult.setTotalCounts(hseProblems.getTotalCounts());
		if (null != hseProblems.getDatas()) {
			hseProblemPageDataResult.setData(hseProblems.getDatas().stream().map(hseProblem -> {
				HseProblemResult hseProblemResult = transferForList(hseProblem,null);
				return hseProblemResult;
			}).collect(Collectors.toList()));
		}
		return hseProblemPageDataResult;
	}
	
	@Override
	public HseProblemPageDataResult getHseProblemResultByDept(Page<HseProblem> page, String problemStatus, Integer deptId){
		HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
		Page<HseProblem> hseProblems = new Page<HseProblem>();
		//安全文明施工问题进度状态与当前处理情况

		if(deptId != 0){
			hseProblems = hseProblemService.findByDept(page, problemStatus, deptId);
		}else{
			hseProblems = hseProblemService.findByProblemStatus(page, problemStatus,null,null);
		}

		hseProblemPageDataResult.setPageCounts(hseProblems.getPageCount());
		hseProblemPageDataResult.setPageNum(hseProblems.getPageNum());
		hseProblemPageDataResult.setPageSize(hseProblems.getPagesize());
		hseProblemPageDataResult.setTotalCounts(hseProblems.getTotalCounts());
		if (null != hseProblems.getDatas()) {
			hseProblemPageDataResult.setData(hseProblems.getDatas().stream().map(hseProblem -> {
				HseProblemResult hseProblemResult = transferForList(hseProblem,null);
				return hseProblemResult;
			}).collect(Collectors.toList()));
		}
		
		return hseProblemPageDataResult;
	}
	
	@Override
	public HseProblemPageDataResult getHseProblemResultByUser(Page<HseProblem> page, String problemStatus, User user,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm){
		HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
		Page<HseProblem> hseProblems = new Page<HseProblem>();
		
		//获取登录用户所创建的施工安全隐患问题
		hseProblems = hseProblemService.findByCreateUser(page, problemStatus, user.getId(),keyword, hseProblemExListRequestForm);
		
		hseProblemPageDataResult.setPageCounts(hseProblems.getPageCount());
		hseProblemPageDataResult.setPageNum(hseProblems.getPageNum());
		hseProblemPageDataResult.setPageSize(hseProblems.getPagesize());
		hseProblemPageDataResult.setTotalCounts(hseProblems.getTotalCounts());
		
		if (null != hseProblems.getDatas()) {
			hseProblemPageDataResult.setData(hseProblems.getDatas().stream().map(hseProblem -> {
				HseProblemResult hseProblemResult = transferForList(hseProblem,null);
				return hseProblemResult;
			}).collect(Collectors.toList()));
		}
		
		return hseProblemPageDataResult;
	}

	/**
	 * 查询90天内安全文明问题分页列表
	 * @param page
	 * @param responsibleDept
	 * @param oneOf3Type
	 * @return
	 */
	@Override
	public HseProblemPageDataResult getHseProblem3mResult(Page<HseProblem> page, Integer responsibleDept, Hse3mEnum oneOf3Type,HseProblemExListRequestForm hseProblemExListRequestForm, HseProblemStatusEnum problemStatus){
		HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
		Page hseProblems = new Page<HseProblem>();

		//查询90天内安全文明问题分页列表
		hseProblems = hseProblemService.getHseProblem3mResult(page, responsibleDept,  oneOf3Type,  hseProblemExListRequestForm,  problemStatus);

		hseProblemPageDataResult.setPageCounts(hseProblems.getPageCount());
		hseProblemPageDataResult.setPageNum(hseProblems.getPageNum());
		hseProblemPageDataResult.setPageSize(hseProblems.getPagesize());
		hseProblemPageDataResult.setTotalCounts(hseProblems.getTotalCounts());

		if (null != hseProblems.getDatas()) {
			hseProblemPageDataResult.setData((List<HseProblemResult>) hseProblems.getDatas().stream().map(hseProblem -> {
				HseProblem hseProblem1 = (HseProblem) hseProblem;
				HseProblemResult hseProblemResult = transferForList(hseProblem1,null);
				return hseProblemResult;
			}).collect(Collectors.toList()));
		}

		return hseProblemPageDataResult;
	}

	@Override
	public boolean hseAssignTasks(HseProblemAssignRequestForm hseProblemAssignRequestForm,User logining) {
		try{
//			List<HseProblemSolve> isPreSolve = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblemAssignRequestForm.getProblemId(), HseProblemSolveStep.teamSolve.toString());
//			if(isPreSolve != null && !isPreSolve.isEmpty()){
//				log.debug("该施工安全问题已分派！");
//				return false;
//			}
//			
			//该问题当前状态为待分派才能执行分派
			HseProblem hseProblem = hseProblemService.findById(hseProblemAssignRequestForm.getProblemId());
			if(hseProblem == null){
				return false;
			}else if(!hseProblem.getProblemStep().equals(HseProblemStep.isNew.toString())){
				log.debug("当前步骤："+hseProblem.getProblemStep()+" 无法执行分派操作！");
				return false;
			}
			
			
			//将整改任务指派给责任班组的班长
			HseProblemSolve hseProblemSolve_new = new HseProblemSolve();
			hseProblemSolve_new.setProblemId(hseProblemAssignRequestForm.getProblemId());
			
			List<User> users = userService.findUserByDepartmentId(hseProblemAssignRequestForm.getResponsibleTeamId());
			User monitor = null;
			for(User user : users){
				boolean isMonitor = userService.isRoleOf(user, "monitor");
				if(isMonitor){
					monitor = user;
					break;
				}
			}
			if(monitor == null){
				log.debug("该班组没有班长！");
				return false;
			}
			if (null != logining) {
				String message = logining.getRealname() + " 分派了安全问题，请及时处理。";
				JPushExtra extra = new JPushExtra();
				extra.setCategory(JPushCategoryEnums.HSE_ASSIGN.name());
				jPushService.pushByUserId(extra, message, "安全问题分派", monitor.getId());
			}
						
			hseProblemSolve_new.setSolveUser(monitor.getId());
			hseProblemSolve_new.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
			hseProblemSolve_new.setSolveStep(HseProblemSolveStep.teamSolve.toString());
			hseProblemSolveService.add(hseProblemSolve_new);
			
			//查询HSE待确认分派的数据
			List<HseProblemSolve> hseProblemSolve_olds = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblemAssignRequestForm.getProblemId(), HseProblemSolveStep.hseConfirm.toString());
			//将当前处理情况更新为已处理
			if(hseProblemSolve_olds !=null && !hseProblemSolve_olds.isEmpty()){
				HseProblemSolve hpsOld = hseProblemSolve_olds.get(0);
				hpsOld.setSolveUser(hseProblemAssignRequestForm.getLoginId());
				hpsOld.setSolveDate(Calendar.getInstance().getTime());
				hpsOld.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
				hseProblemSolveService.update(hpsOld);
				
			}

			
			//更新施工问题处理步骤
//			HseProblem hseProblem = hseProblemService.findById(hseProblemAssignRequestForm.getProblemId());
			hseProblem.setResponsibleTeam(hseProblemAssignRequestForm.getResponsibleTeamId());

			hseProblem.setResponsibleDept(hseProblemAssignRequestForm.getResponsibleCaptainId());//责任队
			hseProblem.setProblemStatus(HseProblemStatusEnum.Renovating.toString());
			hseProblem.setProblemStep(HseProblemStep.isNeedRenovate.toString());//待整改
			SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			String d = format.format(hseProblemAssignRequestForm.getStartDate());
			Date date = format.parse(d);
			hseProblem.setTargetDate(date);
			hseProblemService.update(hseProblem);
			
////			APP主键
////			状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
//			log.debug(EnpowerHSEStatus.RECEIVED);
////			整改后照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）
//			
////			整改人
//			log.debug(hseProblem);
////			整改完成日期
////			整改单的编号
////			整改描述

			
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	/**
	 * 责任部门提交整改结果
	 * @param problemId
	 * @param description
	 * @param files
	 * @return
	 */
	@Override
	public boolean processRenovateResult(Integer problemId, String description, CommonsMultipartFile[] files,Integer loginId) {
		boolean status = false;
		try{
			HseProblem hp = hseProblemService.findById(problemId);
			if(hp == null){
				return false;
			}else if(!hp.getProblemStatus().equals(HseProblemStatusEnum.Renovating.toString())){
				log.debug("当前状态："+hp.getProblemStatus()+" 无法提交整改结果！");
				return false;
			}

			User creater = userService.findUserById(hp.getCreateUser());
			boolean toHseCheck = userService.isDepartmentOf(creater, "HSEDepartment");////"当前创建用户是HSE部门成员

			//更新问题状态为待审查，设置整改完成日期
			if (toHseCheck) {
				hp.setProblemStatus(HseProblemStatusEnum.Need_Check.toString());
				hp.setProblemStep(HseProblemStep.isNeedCheck.toString());
			}else{
				hp.setProblemStatus(HseProblemStatusEnum.Need_A_Check.toString());
				hp.setProblemStep(HseProblemStep.isNeedACheck.toString());
			}
			hseProblemService.update(hp);

			//更新责任部门处理情况
			List<HseProblemSolve> hseProblemSolves = null;
			hseProblemSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.teamSolveAgain.toString());
			if(hseProblemSolves == null || hseProblemSolves.isEmpty()){
				hseProblemSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.teamSolve.toString());
			}
			//是否为二次整改
			boolean isAgain = false;
			HseProblemSolve hps = null ;
			if(hseProblemSolves != null && !hseProblemSolves.isEmpty()){
				hps = hseProblemSolves.get(0);
				if(hps.getSolveStep().equals(HseProblemSolveStep.teamSolveAgain.toString())){
					isAgain = true;
				}
				hps.setSolveDescription(description);
				hps.setSolveDate(Calendar.getInstance().getTime());
				hps.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
				hps.setSolveUser(loginId);//修改替换别人的　待处理
				hseProblemSolveService.update(hps);
			}


			String solveRole = null;//HseProblemSolveStep.hseCheck.toString();
			if(toHseCheck){
				//"当前创建用户是HSE部门成员
				solveRole = HseProblemSolveStep.hseCheck.toString();
			}else{
				//"当前创建用户不是HSE部门成员"
				solveRole = HseProblemSolveStep.aCheck.toString();
			}

			//下一个待HSE审核
			List<HseProblemSolve> hseCheck = null;
			HseProblemSolve hseProblemSolveNew = null;
			hseCheck = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, solveRole);
			if(hseCheck != null && !hseCheck.isEmpty()){
				hseProblemSolveNew = hseCheck.get(0);
				hseProblemSolveNew.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
			}else{
				hseProblemSolveNew = new HseProblemSolve();
				hseProblemSolveNew.setProblemId(problemId);
				hseProblemSolveNew.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
				hseProblemSolveNew.setSolveStep(solveRole);
			}


//			APP主键
			log.debug(hp.getId().toString());
//			状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
			log.debug("3");//'整改反馈'
//			整改后照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）
			log.debug("files");
//			整改人 当前人
			log.debug(hps.getSolveUser().toString());
//			整改完成日期
			log.debug(new Date().toString());
//			整改单的编号 后来改为问题标题
			log.debug("先成规则 ");
//			整改描述
			log.debug(description);


			hseProblemSolveService.update(hseProblemSolveNew);

			//上传整改完成照片
			String fileType = null;
			//第二次整改则设置图片类型为再次整改类型
			if(isAgain){
				fileType = HseProblemFileType.again.toString();
			}else{
				fileType = HseProblemFileType.after.toString();
			}
			if (files!=null && !moreUpload(problemId, files,fileType)) {
				log.error("==========上传文件出错==========");
				return false;
			}
			Map<String,Object> inMap = new HashMap<>();
			inMap.put("APPID",problemId);
			inMap.put("MODI_DATE",Calendar.getInstance().getTime());
			pushToEnpowerService.findEnpowerHSEQuestionCompleteBy(inMap,"3",hps.getSolveUser());

			status = true;
		}catch(Exception e){
			e.getMessage();
		}
		return status;
	}

	/**
	 * HSE审核整改结果
	 * @param problemId
	 * @return
	 * @throws Exception
	 */
	public boolean checkRenovateResult(BaseRequestForm requestForm, Integer problemId,Integer checkResult) throws Exception{
		boolean status = false;
		String RE_ZG="";
		String END_DATE="";
		String RQ_YZ=null;
		Integer loginId = requestForm.getLoginId();
		try{
			HseProblem hseProblem = hseProblemService.findById(problemId);
			String enpFlag = "";//enp状态
			if(hseProblem == null){
				return false;
			}else if(!hseProblem.getProblemStatus().equals(HseProblemStatusEnum.Need_Check.toString())){
				log.debug("当前状态："+hseProblem.getProblemStatus()+" 无法审核整改结果！");
				return false;
			}
			HseProblemSolve hpsagain = null;
			if(checkResult == 1){//整改合格，给enp传完成状态
				hseProblem.setProblemStatus(HseProblemStatusEnum.Finish.toString());
				hseProblem.setProblemStep(HseProblemStep.isFinished.toString());
				hseProblem.setFinishDate(Calendar.getInstance().getTime());

				List<HseProblemSolve> hs1 = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblem.getId(), HseProblemSolveStep.teamSolve.toString());
				if(hs1!=null && !hs1.isEmpty()){
					//RE_ZG 整改情况回复
					RE_ZG=hs1.get(0).getSolveDescription();
					//END_DATE 完成日期
					END_DATE=DateUtility.sdfyMdHms.format(hs1.get(0).getSolveDate());
				}
				//RQ_YZ 验收日期
				RQ_YZ=DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime());
				enpFlag="4";
//				enpFlag=EnpowerHSEStatus.COMPLETE;

			}else if(checkResult == 2){//整改不合格

				hseProblem.setProblemStatus(HseProblemStatusEnum.Renovating.toString());
				hseProblem.setProblemStep(HseProblemStep.isRenovateAgain.toString());

				//是否存在二次整改
				List<HseProblemSolve> hseSolves = new ArrayList<>();
				hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.teamSolveAgain.toString());
				if(hseSolves == null || hseSolves.isEmpty()){
					hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.teamSolve.toString());
				}

				if(hseSolves != null && !hseSolves.isEmpty()){
					//已存在则更新二次整改数据，不存在二次整改则添加一条二次整改数据
					if(hseSolves.size()>0 && hseSolves.get(0).getSolveStep().equals(HseProblemSolveStep.teamSolveAgain.toString())){
						hpsagain = hseSolves.get(0);
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
					}else{
						hpsagain = new HseProblemSolve();
						hpsagain.setProblemId(problemId);
						hpsagain.setSolveStep(HseProblemSolveStep.teamSolveAgain.toString());
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
						hpsagain.setSolveUser(hseSolves.get(0).getSolveUser());
					}


					hseProblemSolveService.update(hpsagain);

					//RE_ZG 整改情况回复
					RE_ZG=hseSolves.get(0).getSolveDescription();
					//END_DATE 完成日期
					END_DATE=DateUtility.sdfyMdHms.format(hseSolves.get(0).getSolveDate());
				}

				//RQ_YZ 验收日期
				RQ_YZ=DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime());
				enpFlag=EnpowerHSEStatus.FAILD;//			状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
			}
			pushToEnpowerService.hSEQuestionCompleteBy(hseProblem,enpFlag);

			hseProblemService.update(hseProblem);

			List<HseProblemSolve> hseProblemSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.hseCheck.toString());
			if(hseProblemSolves != null && hseProblemSolves.size()>0){
				HseProblemSolve hps = hseProblemSolves.get(0);
				hps.setSolveUser(loginId);
				hps.setSolveDate(Calendar.getInstance().getTime());
				hps.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
				hseProblemSolveService.update(hps);
			}
			//HSE审完，给enp传完成状态
			//Enpower接口　修改安全整改完成情况
			pushToEnpowerService.hSEQuestionSendUpdate(hseProblem,RE_ZG,END_DATE,RQ_YZ,enpFlag);
			status = true;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return status;
	}
	/**
	 * 编制人A提交 对本人所提问题的审核 结果
	 * @param problemId
	 * @return
	 * @throws Exception
	 */
	public boolean checkProducerResult(BaseRequestForm requestForm, Integer problemId,Integer checkResult) throws Exception{
		boolean status = false;
//		String RQ_YZ=null;
		try{
			HseProblem hseProblem = hseProblemService.findById(problemId);
			if(hseProblem == null){
				return false;
			}else if(!hseProblem.getProblemStatus().equals(HseProblemStatusEnum.Need_A_Check.toString())){
				log.debug("当前状态："+hseProblem.getProblemStatus()+" 无法审核整改结果！");
				return false;
			}
			HseProblemSolve hpsagain = null;
			if(checkResult == 1){//整改合格，应该给HSE审核了
				hseProblem.setProblemStatus(HseProblemStatusEnum.Need_Check.toString());
				hseProblem.setProblemStep(HseProblemStep.isNeedCheck.toString());
				hseProblem.setFinishDate(Calendar.getInstance().getTime());


				List<HseProblemSolve> hseSolves = new ArrayList<>();
				hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.hseCheck.toString());
				if(hseSolves != null ){
					if(hseSolves.size()>0 && hseSolves.get(0).getSolveStep().equals(HseProblemSolveStep.hseCheck.toString())){
						hpsagain = hseSolves.get(0);
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
					}else{
						hpsagain = new HseProblemSolve();
						hpsagain.setProblemId(problemId);
						hpsagain.setSolveStep(HseProblemSolveStep.hseCheck.toString());
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
					}
					hseProblemSolveService.update(hpsagain);
				}

			}else if(checkResult == 2){//整改不合格

				hseProblem.setProblemStatus(HseProblemStatusEnum.Renovating.toString());
				hseProblem.setProblemStep(HseProblemStep.isRenovateAgain.toString());

				//是否存在二次整改
				List<HseProblemSolve> hseSolves = new ArrayList<>();
				hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.teamSolveAgain.toString());

				if(hseSolves != null ){
					//已存在则更新二次整改数据，不存在二次整改则添加一条二次整改数据
					if(hseSolves.size()>0 && hseSolves.get(0).getSolveStep().equals(HseProblemSolveStep.teamSolveAgain.toString())){
						hpsagain = hseSolves.get(0);
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
					}else{
						hpsagain = new HseProblemSolve();
						hpsagain.setProblemId(problemId);
						hpsagain.setSolveStep(HseProblemSolveStep.teamSolveAgain.toString());
						hpsagain.setSolveStatus(HseProblemSolveStatusEnum.Pre.toString());
					}

					hseProblemSolveService.update(hpsagain);

				}
			}

			hseProblemService.update(hseProblem);

			List<HseProblemSolve> hseProblemSolves = hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.aCheck.toString());
			if(hseProblemSolves != null && !hseProblemSolves.isEmpty()){
				HseProblemSolve hps = hseProblemSolves.get(0);
				hps.setSolveUser(requestForm.getLoginId());
				hps.setSolveDate(Calendar.getInstance().getTime());
				hps.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
				hseProblemSolveService.update(hps);

			}
			status = true;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return status;
	}
	
	/**
	 * 
	 * */
	public HseProblemCreatUiDataResult getHseProblemCreatUiResult(Integer deptId,String safe) throws Exception{
		HseProblemCreatUiDataResult hseProblemCreatUiResult = new HseProblemCreatUiDataResult();
		List<HseResponsibleDeptResult> depts = new ArrayList<>();
		List<HseResponsibleDeptResult> teams = new ArrayList<>();
		List<Department> deptParent = null;
		List<Department> deptChidren = null;
		
		//暂时获取部门名为K2/K3核电项目部的下属部门和班组
		VariableSet value = variableSetDao.findByKey("departmentRoot", "departmentType");
		Department dept = null;
		if(value != null){
			dept = departmentService.findByName(value.getSetvalue());
		}
		if(dept != null){
			deptParent = departmentService.findChildren(dept.getId());
			for(Department dpt : deptParent){
				HseResponsibleDeptResult rdpt = new HseResponsibleDeptResult();
				rdpt.setDeptId(dpt.getId().toString());
				rdpt.setDeptName(dpt.getName());
				depts.add(rdpt);
			}
			
			//界面初始化时，没有部门ID传入则获取列表第一部门的下属班组
			if(deptId == null || deptId == 0){
				deptChidren = departmentService.findChildren(Integer.parseInt(depts.get(0).getDeptId()));
			}else{
				//有部门ID传入则获取传入部门的下属班组
				deptChidren = departmentService.findChildren(deptId);
			}
			
			//转换数据
			for(Department dptchlid : deptChidren){
				HseResponsibleDeptResult rdptchlid = new HseResponsibleDeptResult();
				rdptchlid.setDeptId(dptchlid.getId().toString());
				rdptchlid.setDeptName(dptchlid.getName());
				teams.add(rdptchlid);
			}
		}
		
		//机组 厂房为enpower接口查询的数据
		List<String> units = new ArrayList<>();
		List<String> wrokshops = new ArrayList<>();
		String xmlNameU = null;
		String xmlNameW = null;
		xmlNameU = "机组信息查询";//MyURLEncoder.encode("机组信息查询", "UTF-8");
		xmlNameW = "厂房信息查询";//"MyURLEncoder.encode("厂房信息查询","UTF-8");
		List<EnpowerResponseUnitWorkshop> enpowerUnits = getUnitWorkshopFromEnpower(xmlNameU);
		List<EnpowerResponseUnitWorkshop> enpowerWrokshops = getUnitWorkshopFromEnpower(xmlNameW);
		if(enpowerUnits != null && !enpowerUnits.isEmpty()){
			for(EnpowerResponseUnitWorkshop eru : enpowerUnits){
				units.add(eru.getCode());
			}
		}
		if(enpowerWrokshops != null && !enpowerWrokshops.isEmpty()){
			for(EnpowerResponseUnitWorkshop erw : enpowerWrokshops){
				wrokshops.add(erw.getCode());
			}
		}
		
		hseProblemCreatUiResult.setUnit(units);
		hseProblemCreatUiResult.setWrokshop(wrokshops);

		if("safe".equals(safe) == false ){//质量的问题类型

			hseProblemCreatUiResult.setResponsibleDept(depts);
		}else{
			//安全的进来 不设置　setResponsibleDept　安全　报告问题时用的setResponsibleTeam,HSE分派时，使用下面的　setResponsibleDepts
		}
		hseProblemCreatUiResult.setResponsibleTeam(teams);
		
		List<QualityControlOptions> roomLevelList = qualityControlOptionsService.findAll(null);

		//房间号和标高
//		List<QualityControlOptionResult> roomLevel= roomLevelList.stream().map(bn->{
//			QualityControlOptionResult ret = new QualityControlOptionResult();
//			ret.setLevel(bn.getLevel());
//			ret.setRoom(bn.getRoom());
//			return ret;
//		}).collect(Collectors.toList());

		List<QualityControlOptionResult> roomle = new ArrayList<QualityControlOptionResult>();
		Map<String,QualityControlOptionResult> roomlMap = new HashMap<String,QualityControlOptionResult>();

		//房间号和标高
		roomLevelList.stream().forEach(e->{
			Optional<QualityControlOptionResult> bean = Optional.ofNullable(roomlMap.get(e.getRoom()));
			
			QualityControlOptionResult bn = bean.orElseGet(QualityControlOptionResult::new);
			bn.setRoom(e.getRoom());
			
			Optional<List<String>> levl = Optional.ofNullable(bn.getLevel());
			List<String> level = levl.orElseGet(ArrayList<String>::new);
			level.add(e.getLevel());
			
			bn.setLevel(level);
			
			roomlMap.put(e.getRoom(), bn);
		});
		roomle = roomlMap.values().stream().map(e->e).collect(Collectors.toList());
		hseProblemCreatUiResult.setRoomLevel(roomle);
		
		if("safe".equals(safe) == false ){//质量的问题类型
	    	Map<String,String> problemType = new HashMap<String,String>();
			for(QualityReasonCodeEnum bn:QualityReasonCodeEnum.values()){
				
				problemType.put(bn.getName(), bn.toString());
			}
			hseProblemCreatUiResult.setProblemType(problemType);
			

		}else{
			//安全的进来

			List<HseResponsibleDeptResult> deptsForSel = new ArrayList<>();
			List<QuaResponsibleDeptResult> teamsForSel = new ArrayList<>();

			//只有详情页时，查询
				//获取部门名为　XXX核电项目部的下属部门和班组
				VariableSet valueRoot = variableSetDao.findByKey("departmentRoot", "departmentType");
				Department deptsTmp = null;
				if(valueRoot != null){
					deptsTmp = departmentService.findByName(valueRoot.getSetvalue());
				}
				if(deptsTmp != null){
					deptParent = departmentService.findChildren(deptsTmp.getId());
					for(Department dpt : deptParent){
						HseResponsibleDeptResult rdpt = new HseResponsibleDeptResult();
						rdpt.setDeptId(dpt.getId().toString());
						rdpt.setDeptName(dpt.getName());
						deptsForSel.add(rdpt);//责任部门设置完成

						List<Department> child = dpt.getChildren();

						//初始化下面的班、组
						child.stream().forEach(e->{

							QuaResponsibleDeptResult tbn2 = new QuaResponsibleDeptResult();
							tbn2.setParentDeptId(dpt.getId());
							tbn2.setDeptId(e.getId());
							tbn2.setDeptName(e.getName());
							teamsForSel.add(tbn2);
						});
					}
				}

				hseProblemCreatUiResult.setResponsibleDepts(depts);
				hseProblemCreatUiResult.setResponsibleTeams(teamsForSel);
			
		}
		
		return hseProblemCreatUiResult;
	}
	

	public HseProblemCreatUiDataResult2 getHseProblemCreatUiResult_v2(Integer deptId,String safe) throws Exception{
		HseProblemCreatUiDataResult2 hseProblemCreatUiResult = new HseProblemCreatUiDataResult2();
		List<HseResponsibleDeptResult> depts = new ArrayList<>();
		List<QuaResponsibleDeptResult> teams = new ArrayList<>();
		List<Department> deptParent = null;
		List<Department> deptChidren = null;
		
		//暂时获取部门名为K2/K3核电项目部的下属部门和班组
		VariableSet value = variableSetDao.findByKey("departmentRoot", "departmentType");
		Department dept = null;
		if(value != null){
			dept = departmentService.findByName(value.getSetvalue());
		}
		if(dept != null){
			deptParent = departmentService.findChildren(dept.getId());
			for(Department dpt : deptParent){
				HseResponsibleDeptResult rdpt = new HseResponsibleDeptResult();
				rdpt.setDeptId(dpt.getId().toString());
				rdpt.setDeptName(dpt.getName());
				depts.add(rdpt);//责任部门设置完成

				List<Department> child = dpt.getChildren();

				//初始化下面的班、组
				child.stream().forEach(e->{

					QuaResponsibleDeptResult tbn2 = new QuaResponsibleDeptResult();
					tbn2.setParentDeptId(dpt.getId());
					tbn2.setDeptId(e.getId());
					tbn2.setDeptName(e.getName());
					teams.add(tbn2);
					
					List<Department> child2 = e.getChildren();
					child2.stream().forEach(e2->{
						QuaResponsibleDeptResult tbn = new QuaResponsibleDeptResult();
						tbn.setParentDeptId(dpt.getId());
						tbn.setDeptId(e2.getId());
						tbn.setDeptName(e.getName()+"->"+e2.getName());
						teams.add(tbn);
					});
				});
			}
		}
		
		//机组 厂房为enpower接口查询的数据
		List<String> units = new ArrayList<>();
		List<String> wrokshops = new ArrayList<>();
		String xmlNameU = null;
		String xmlNameW = null;
		xmlNameU = "机组信息查询";//MyURLEncoder.encode("机组信息查询", "UTF-8");
		xmlNameW = "厂房信息查询";//"MyURLEncoder.encode("厂房信息查询","UTF-8");
		List<EnpowerResponseUnitWorkshop> enpowerUnits = getUnitWorkshopFromEnpower(xmlNameU);
		List<EnpowerResponseUnitWorkshop> enpowerWrokshops = getUnitWorkshopFromEnpower(xmlNameW);
		if(enpowerUnits != null && !enpowerUnits.isEmpty()){
			for(EnpowerResponseUnitWorkshop eru : enpowerUnits){
				units.add(eru.getCode());
			}
		}

		if(enpowerWrokshops != null && !enpowerWrokshops.isEmpty()){
			for(EnpowerResponseUnitWorkshop erw : enpowerWrokshops){
				wrokshops.add(erw.getCode());
			}
		}
		hseProblemCreatUiResult.setUnit(units);
		hseProblemCreatUiResult.setResponsibleDept(depts);
		hseProblemCreatUiResult.setResponsibleTeam(teams);
		
//		List<QualityControlOptions> roomLevelList = qualityControlOptionsService.findAll(null);
		List<EnpowerResponseRoomLevel> roomLevelListEnp = executePushToEnpowerService.getRoomLevelList(null);

		List<QualityControlOptionResult> roomle = new ArrayList<QualityControlOptionResult>();
		Map<String,QualityControlOptionResult> roomlMap = new HashMap<String,QualityControlOptionResult>();

		//房间号和标高　本地的数据
//		roomLevelList.stream().forEach(e->{
//		//厂房
//		if(ConstantVar.workshop.equals(e.getType())){
//			wrokshops.add(e.getRoom());
//		}else{//房间号
//			Optional<QualityControlOptionResult> bean = Optional.ofNullable(roomlMap.get(e.getRoom()));
//			
//			QualityControlOptionResult bn = bean.orElseGet(QualityControlOptionResult::new);
//			bn.setRoom(e.getRoom());
//			
//			Optional<List<String>> levl = Optional.ofNullable(bn.getLevel());
//			List<String> level = levl.orElseGet(ArrayList<String>::new);
//			level.add(e.getLevel());
//			
//			bn.setLevel(level);
//			
//			roomlMap.put(e.getRoom(), bn);
//		}
//	});
		roomLevelListEnp.stream().forEach(e->{
		//房间号
			if(e.getRoom()!=null && !"".equals(e.getRoom())){

				Optional<QualityControlOptionResult> bean = Optional.ofNullable(roomlMap.get(e.getRoom()));
				
				QualityControlOptionResult bn = bean.orElseGet(QualityControlOptionResult::new);
				bn.setRoom(e.getRoom());
				
				Optional<List<String>> levl = Optional.ofNullable(bn.getLevel());
				List<String> level = levl.orElseGet(ArrayList<String>::new);
				level.add(e.getLevel());
				
				bn.setLevel(level);
				
				roomlMap.put(e.getRoom(), bn);
			}
	});
		roomle = roomlMap.values().stream().map(e->e).collect(Collectors.toList());
		hseProblemCreatUiResult.setRoomLevel(roomle);
		hseProblemCreatUiResult.setWrokshop(wrokshops);
		
		if("safe".equals(safe) == false ){//质量的问题类型
	    	Map<String,String> problemType = new HashMap<String,String>();
			for(QualityReasonCodeEnum bn:QualityReasonCodeEnum.values()){
				
				problemType.put(bn.getName(), bn.toString());
			}
			hseProblemCreatUiResult.setProblemType(problemType);
		}else{//安全的　三级安全编码
        	EnpowerResponseHseCode paramIn = new EnpowerResponseHseCode();
        	paramIn.setStand( EnpConstant.HSE_STAND1 );
        	List<EnpowerResponseHseCode> codeLevel1 = executePushToEnpowerService.getHseCodeList(paramIn);
        	hseProblemCreatUiResult.setCodeLevel1(codeLevel1);

        	paramIn.setStand(EnpConstant.HSE_STAND2);
        	List<EnpowerResponseHseCode> codeLevel2 = executePushToEnpowerService.getHseCodeList(paramIn);
        	paramIn.setStand(EnpConstant.HSE_STAND3);
        	List<EnpowerResponseHseCode> codeLevel3 = executePushToEnpowerService.getHseCodeList(paramIn);
        	codeLevel2.addAll(codeLevel3);
        	hseProblemCreatUiResult.setCodeLevel23(codeLevel2);
		}
		
		return hseProblemCreatUiResult;
	}

	/**
	 * 根据v2的，去掉了责任组的数据。提供用于报告问题使用。
	 */
	public HseProblemCreatUiDataResult2 getHseProblemCreatUiResultForReport(Integer deptId,String safe) throws Exception{
		HseProblemCreatUiDataResult2 hseProblemCreatUiResult = new HseProblemCreatUiDataResult2();
		List<HseResponsibleDeptResult> depts = new ArrayList<>();
		List<QuaResponsibleDeptResult> teams = new ArrayList<>();
		List<Department> deptParent = null;
		
		//暂时获取部门名为K2/K3核电项目部的下属部门和班组
		VariableSet value = variableSetDao.findByKey("departmentRoot", "departmentType");
		Department dept = null;
		if(value != null){
			dept = departmentService.findByName(value.getSetvalue());
		}
		if(dept != null){
			deptParent = departmentService.findChildren(dept.getId());
			for(Department dpt : deptParent){
				HseResponsibleDeptResult rdpt = new HseResponsibleDeptResult();
				rdpt.setDeptId(dpt.getId().toString());
				rdpt.setDeptName(dpt.getName());
				depts.add(rdpt);//责任部门设置完成

				List<Department> child = dpt.getChildren();

				//初始化下面的班、组
				child.stream().forEach(e->{

					QuaResponsibleDeptResult tbn2 = new QuaResponsibleDeptResult();
					tbn2.setParentDeptId(dpt.getId());
					tbn2.setDeptId(e.getId());
					tbn2.setDeptName(e.getName());
					teams.add(tbn2);
				});
			}
		}
		
		//机组 厂房为enpower接口查询的数据
		List<String> units = new ArrayList<>();
		List<String> wrokshops = new ArrayList<>();
		String xmlNameU = null;
		String xmlNameW = null;
		xmlNameU = "机组信息查询";//MyURLEncoder.encode("机组信息查询", "UTF-8");
		xmlNameW = "厂房信息查询";//"MyURLEncoder.encode("厂房信息查询","UTF-8");
		List<EnpowerResponseUnitWorkshop> enpowerUnits = getUnitWorkshopFromEnpower(xmlNameU);
		List<EnpowerResponseUnitWorkshop> enpowerWrokshops = getUnitWorkshopFromEnpower(xmlNameW);
		if(enpowerUnits != null && !enpowerUnits.isEmpty()){
			for(EnpowerResponseUnitWorkshop eru : enpowerUnits){
				units.add(eru.getCode());
			}
		}

		if(enpowerWrokshops != null && !enpowerWrokshops.isEmpty()){
			for(EnpowerResponseUnitWorkshop erw : enpowerWrokshops){
				wrokshops.add(erw.getCode());
			}
		}
		hseProblemCreatUiResult.setUnit(units);
		hseProblemCreatUiResult.setResponsibleDept(depts);
		hseProblemCreatUiResult.setResponsibleTeam(teams);
		
//		List<QualityControlOptions> roomLevelList = qualityControlOptionsService.findAll(null);
		List<EnpowerResponseRoomLevel> roomLevelListEnp = executePushToEnpowerService.getRoomLevelList(null);

		List<QualityControlOptionResult> roomle = new ArrayList<QualityControlOptionResult>();
		Map<String,QualityControlOptionResult> roomlMap = new HashMap<String,QualityControlOptionResult>();

		roomLevelListEnp.stream().forEach(e->{
		//房间号
			if(e.getRoom()!=null && !"".equals(e.getRoom())){

				Optional<QualityControlOptionResult> bean = Optional.ofNullable(roomlMap.get(e.getRoom()));
				
				QualityControlOptionResult bn = bean.orElseGet(QualityControlOptionResult::new);
				bn.setRoom(e.getRoom());
				
				Optional<List<String>> levl = Optional.ofNullable(bn.getLevel());
				List<String> level = levl.orElseGet(ArrayList<String>::new);
				level.add(e.getLevel());
				
				bn.setLevel(level);
				
				roomlMap.put(e.getRoom(), bn);
			}
	});
		roomle = roomlMap.values().stream().map(e->e).collect(Collectors.toList());
		hseProblemCreatUiResult.setRoomLevel(roomle);
		hseProblemCreatUiResult.setWrokshop(wrokshops);
		
		if("safe".equals(safe) == false ){//质量的问题类型
	    	Map<String,String> problemType = new HashMap<String,String>();
			for(QualityReasonCodeEnum bn:QualityReasonCodeEnum.values()){
				
				problemType.put(bn.getName(), bn.toString());
			}
			hseProblemCreatUiResult.setProblemType(problemType);
		}else{//安全的　三级安全编码
        	EnpowerResponseHseCode paramIn = new EnpowerResponseHseCode();
        	paramIn.setStand( EnpConstant.HSE_STAND1 );
        	List<EnpowerResponseHseCode> codeLevel1 = executePushToEnpowerService.getHseCodeList(paramIn);
        	hseProblemCreatUiResult.setCodeLevel1(codeLevel1);

        	paramIn.setStand(EnpConstant.HSE_STAND2);
        	List<EnpowerResponseHseCode> codeLevel2 = executePushToEnpowerService.getHseCodeList(paramIn);
        	paramIn.setStand(EnpConstant.HSE_STAND3);
        	List<EnpowerResponseHseCode> codeLevel3 = executePushToEnpowerService.getHseCodeList(paramIn);
        	codeLevel2.addAll(codeLevel3);
        	hseProblemCreatUiResult.setCodeLevel23(codeLevel2);
		}
		
		return hseProblemCreatUiResult;
	}
	public HseProblemResult transferForList(HseProblem hseProblem, String value){
		HseProblemTransForResult hseProblemTransForResult = new HseProblemTransForResult();
		HseProblemResult hseProblemResult = new HseProblemResult();
		hseProblemResult.setCreateDate(hseProblem.getCreateDate());
		hseProblemResult.setProblemTitle(hseProblem.getProblemTitle());
		hseProblemResult.setId(hseProblem.getId().toString());
		hseProblemResult.setCreateUser(userService.findUserById(hseProblem.getCreateUser()).getRealname());
		hseProblemResult.setCreateUserId(hseProblem.getCreateUser());
		hseProblemResult.setProblemStatus(hseProblem.getProblemStatus());
		List<HseProblemSolve> problemSolves = hseProblemSolveService.findByProblemId(hseProblem.getId());
		HseProblemSolve problemSolve = new HseProblemSolve();
		if(problemSolves != null && problemSolves.size()>0){
			problemSolve = problemSolves.get(0);
		}
		hseProblemResult.setProblemSolveStatus(problemSolve.getSolveStatus());
		hseProblemResult.setUnit(hseProblem.getUnit());
		hseProblemResult.setWrokshop(hseProblem.getWrokshop());
		hseProblemResult.setEleration(hseProblem.getEleration());
		hseProblemResult.setRoomno(hseProblem.getRoomno());
		//责任部门转换为返回数据
		if(hseProblem.getResponsibleDept()!=null){
			Department responsibleDept = departmentService.findById(hseProblem.getResponsibleDept());
			if(responsibleDept != null){
				HseResponsibleDeptResult responsibleDeptRs = new HseResponsibleDeptResult();
				responsibleDeptRs.setDeptId(responsibleDept.getId().toString());
				responsibleDeptRs.setDeptName(responsibleDept.getName());
				hseProblemResult.setResponsibleDept(responsibleDeptRs);							
			}
		}
		//责任班组转换为返回数据
		if(hseProblem.getResponsibleTeam()!=null){
			Department responsibleTeam = departmentService.findById(hseProblem.getResponsibleTeam());
			if(responsibleTeam != null){
				HseResponsibleDeptResult responsibleTeamRs = new HseResponsibleDeptResult();
				responsibleTeamRs.setDeptId(responsibleTeam.getId().toString());
				responsibleTeamRs.setDeptName(responsibleTeam.getName());
				hseProblemResult.setResponsibleTeam(responsibleTeamRs);							
			}
		}
		
		hseProblemResult.setProblemDescription(hseProblem.getDescription());
		hseProblemResult.setProblemStep(hseProblem.getProblemStep());
		
		List<HseProblemSolve> hseProblemSolve = hseProblemSolveService.findByProblemId(hseProblem.getId());
		hseProblemResult.setHseProblemSolve(hseProblemSolve);
		
		List<HseProblemFile> files = hseProblemFileService.findByProblemId(hseProblem.getId());
		if(files!=null && files.size()>0){
			hseProblemResult.setFiles(hseProblemTransForResult.problemFile(files));
			hseProblemResult.setFileSize(files.size());
		}
		
		if(HseProblemStep.isNeedCheck.toString().equals(hseProblem.getProblemStep()) || HseProblemStep.isCheckAgain.toString().equals(hseProblem.getProblemStep())){
			List<HseProblemSolve> hs = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblem.getId(), HseProblemSolveStep.teamSolve.toString());
			if(hs!=null && !hs.isEmpty()){
				hseProblemResult.setRealDate(hs.get(0).getSolveDate());				
			}
		}
		if(HseProblemStep.isFinished.toString().equals(hseProblem.getProblemStep()) || HseProblemStep.isCheckAgain.toString().equals(hseProblem.getProblemStep())){
			List<HseProblemSolve> hs = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblem.getId(), HseProblemSolveStep.hseCheck.toString());
			List<HseProblemSolve> hs1 = hseProblemSolveService.findByProblenIdAndSolveStep(hseProblem.getId(), HseProblemSolveStep.teamSolve.toString());
			if(hs!=null && !hs.isEmpty()){
				hseProblemResult.setCheckDate(hs.get(0).getSolveDate());				
			}
			if(hs1!=null && !hs1.isEmpty()){
				hseProblemResult.setRealDate(hs1.get(0).getSolveDate());				
			}
		}
		//是否超期
		boolean delay = delay(hseProblem) ;
		hseProblemResult.setDelay(delay);
		hseProblemResult.setCriticalLevel(hseProblem.getCriticalLevel());
		hseProblemResult.setCheckType(hseProblem.getCheckType());
		hseProblemResult.setRequirement(hseProblem.getRequirement());

		hseProblemResult.setTargetDate(hseProblem.getTargetDate());
		hseProblemResult.setFinishDate(hseProblem.getFinishDate());

		hseProblemResult.setCode1Id(hseProblem.getCode1Id());
		hseProblemResult.setCode2Id(hseProblem.getCode2Id());
		hseProblemResult.setCode3Id(hseProblem.getCode3Id());
		hseProblemResult.setCode2Desc(hseProblem.getCode2Desc());
		hseProblemResult.setCode3Desc(hseProblem.getCode3Desc());
				
		return hseProblemResult;
	}
	
	/**
	 * 安全文明施工是否超期
	 * @param hseProblem
	 * @return
	 */
	private boolean delay(HseProblem hseProblem) {
		boolean delay = false ;
		if(hseProblem==null || hseProblem.getTargetDate()==null){
			return delay ;
		}
		
		if("Need_Handle".equals(hseProblem.getProblemStatus())
				||"Renovating".equals(hseProblem.getProblemStatus())
				){
			Calendar c = Calendar.getInstance();

			delay = c.getTime().after(hseProblem.getTargetDate());
			return delay;
		}else{
			return delay ;
		}
	}

	/**
	 * 文件上传
	 * @param problemId
	 * @param files
	 * @param type
	 * @return
	 */
	private boolean moreUpload(Integer problemId, CommonsMultipartFile files[], String type) {
		// 上传位置  设定文件保存的目录 
		String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath;
		List<FileInfo> fileInfos = UploadUtil.moreUpload(filePath, "utf-8", true, files);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				HseProblemFile file = new HseProblemFile();
				file.setFilepath(fileInfo.getNewFilename());
				file.setUploadtime(Calendar.getInstance().getTime());
				file.setProblemId(problemId);
				if(fileInfo.getFilename()!=null && fileInfo.getFilename().length()>50){
					file.setFilename(fileInfo.getFilename().substring(0, 50));
				}else{
					file.setFilename(fileInfo.getFilename());
				}
				file.setFiletype(type);
				hseProblemFileService.add(file);
			}
		}
		return true;
	}
	
	public String getHseCodeDesc(String id){
		try{
	    	EnpowerResponseHseCode paramIn = new EnpowerResponseHseCode();
	    	paramIn.setId(id);
	    	List<EnpowerResponseHseCode> codeLevel1 = executePushToEnpowerService.getHseCodeList(paramIn);
	    	return codeLevel1.get(0).getCodeDesc();
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 获取enpower的机组厂房信息
	 * @param xmlName
	 * @return
	 * @throws Exception 
	 */
	public List<EnpowerResponseUnitWorkshop> getUnitWorkshopFromEnpower(String xmlName) throws Exception{
		List<EnpowerResponseUnitWorkshop> enpowerResponseUnitWorkshops = null;
		
		Gson gson = new Gson();
		
		EnpowerRequest enpowerRequest = new EnpowerRequest();
		enpowerRequest.setCondition("");
		enpowerRequest.setXmlname(xmlName);
		enpowerRequest.setIsPage(1);
		enpowerRequest.setPageIndex(1);
		enpowerRequest.setPageSize(20000);
		String parms = enpowerRequest.toString();

		String unitWorkshopRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
		if (null != unitWorkshopRequestResult && !"".equals(unitWorkshopRequestResult)){
			BaseResponse baseResponse = gson.fromJson(unitWorkshopRequestResult, new TypeToken<BaseResponse>() {}.getType());
			if (baseResponse.getIsSuccess()){
				String dataInfo = baseResponse.getDataInfo();
				enpowerResponseUnitWorkshops = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseUnitWorkshop>>() {}.getType());
				log.info(CommonUtility.toJson(enpowerResponseUnitWorkshops));
				log.info("获取到数据总数： " + enpowerResponseUnitWorkshops.size());
			} else {
				log.error(baseResponse.getDataInfo());
			}
		}
		return enpowerResponseUnitWorkshops;
	}

}
