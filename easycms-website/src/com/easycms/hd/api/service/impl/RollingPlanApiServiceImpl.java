package com.easycms.hd.api.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.easycms.hd.api.request.underlying.WitingPlan;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.request.EnpowerRequestDisappearPoint;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialBackfill;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanInsertWeld;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanStatusSyn;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanUpdateWeld;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendEquipment;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendMain;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendNotice;
import com.easycms.hd.api.enums.RollingPlanTypeEnum;
import com.easycms.hd.api.enums.StatusEnum;
import com.easycms.hd.api.enums.WitnessFlagEnum;
import com.easycms.hd.api.enums.enpower.EnpowerSpecialtyCode;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.RollingMaterialResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanDataResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanPageDataResult;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.hd.plan.domain.EnpowerJobOrder;
import com.easycms.hd.plan.domain.EnpowerMaterialInfo;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.RollingPlanFlag;
import com.easycms.hd.plan.domain.SixLevelRollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.mybatis.dao.WitnessStatisticsMybatisDao;
import com.easycms.hd.plan.service.EnpowerEquipmentService;
import com.easycms.hd.plan.service.EnpowerJobOrderService;
import com.easycms.hd.plan.service.EnpowerMaterialInfoService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.itextpdf.text.log.SysoCounter;

@Service("rollingPlanApiService")
public class RollingPlanApiServiceImpl implements RollingPlanApiService {
	
	private Logger logger = Logger.getLogger(RollingPlanApiServiceImpl.class);
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private UserService userService;
	@Autowired
	private EnpowerMaterialInfoService materialInfoService;
	@Autowired
	private EnpowerJobOrderService jobOrderService;
	@Autowired
	private EnpowerEquipmentService equipmentService;
	@Autowired
	private WorkStepService workStepService ;
	@Autowired
	private WitnessService witnessService ;
	@Autowired
	private ModulesService modulesService ;
	/**临时用于 滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长*/
	@Autowired
	private WitnessStatisticsMybatisDao witnessStatisticsMybatisDao ;
	@Autowired
	private RoleService roleService;
	
	/**临时前绩效考核的考核数据全部清理*/
	@Override
	public JsonResult<Boolean> clearAllKpiScore(){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		witnessStatisticsMybatisDao.clearKpi1();
		witnessStatisticsMybatisDao.clearKpi2();
		witnessStatisticsMybatisDao.clearKpi3();
		witnessStatisticsMybatisDao.clearKpi4();
		witnessStatisticsMybatisDao.clearKpi5();
		return result ;
	}
	
	/**临时用于  滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长*/
	@Override
	public JsonResult<Boolean> initRollingPlan(Integer userId, String username) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		User user  = null;
		if(userId!=null){
			user = userService.findUserById(userId);
			if(user==null){
				result.setMessage("没有用户id="+userId);
				return result ;
			}
		}
		if(username!=null && !"".equals(username)){
			user = userService.findByNameIsDel1(username);
			if(user==null){
				result.setMessage("没有用户username="+username);
				return result ;
			}
		}
		
		if(user==null){
			result.setMessage("没有用户");
		}

witnessStatisticsMybatisDao.initRollingPlan1();
witnessStatisticsMybatisDao.initRollingPlan2();
witnessStatisticsMybatisDao.initRollingPlan3();
witnessStatisticsMybatisDao.initRollingPlan4();
if(user==null){

witnessStatisticsMybatisDao.initRollingPlan5(null);
}else{

witnessStatisticsMybatisDao.initRollingPlan5(user);
}
witnessStatisticsMybatisDao.initRollingPlan6();
witnessStatisticsMybatisDao.initRollingPlan7();
		
		return result ;
	}

	/**临时用于  滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长*/
	@Override
	public
	JsonResult<Boolean> initRollingPlan(Integer userId,Integer rollingPlanId, String username){
		JsonResult<Boolean> result = new JsonResult<Boolean>();

		User user  = null;
		if(userId!=null){
			user = userService.findUserById(userId);
			if(user==null){
				result.setMessage("没有用户id="+userId);
				return result ;
			}
		}
		if(username!=null && !"".equals(username)){
			user = userService.findByNameIsDel1(username);
			if(user==null){
				result.setMessage("没有用户username="+username);
				return result ;
			}
		}
		
		if(user==null){
			result.setMessage("没有用户");
			user = new User();
		}

		user.setId(rollingPlanId);//使用id传值
		
		witnessStatisticsMybatisDao.initRollingPlan2del(user);
		witnessStatisticsMybatisDao.initRollingPlan2upd(user);
		
		return result;
	}
	
	//获取所有跟rollingplan相关的分页信息
	@Override
	public RollingPlanPageDataResult getRollingPlanPageDataResult(Page<RollingPlan> page, User user, RollingPlanTypeEnum status, String type, String keyword,Integer usingRoleId) {
		RollingPlanPageDataResult rollingPlanPageDataResult = new RollingPlanPageDataResult();
		try {
			List<Role> roles = user.getRoles();
			if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
				roles = roleService.findListById(roles,usingRoleId);
			}
			
			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains("monitor")) {
						List<Integer> monitorIds = new ArrayList<Integer>();
						monitorIds.add(user.getId());
						//如果是班长
						if (status.equals(RollingPlanTypeEnum.UNASSIGNED)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findByPageMonitorUnAssignWithKeyword(type, monitorIds, keyword, page);
							} else {
								page = rollingPlanService.findByPageMonitorUnAssign(type, monitorIds, page);
							}
						} else if (status.equals(RollingPlanTypeEnum.ASSIGNED)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findByPageMonitorAssignedWithKeyword(type, monitorIds, keyword, page);
							} else {
								page = rollingPlanService.findByPageMonitorAssigned(type, monitorIds, page);
							}
						} else {
							return null;
						}
					} else if (roleNameList.contains("team")) {
						List<Integer> teamIds = new ArrayList<Integer>();
						teamIds.add(user.getId());
						//如果是组长
						if (status.equals(RollingPlanTypeEnum.COMPLETED)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findTeamCompletedByPageWithTypeAndKeyword(teamIds, type, keyword, page);
							} else {
								page = rollingPlanService.findTeamCompletedByPageWithType(teamIds, type, page);
							}
						} else if (status.equals(RollingPlanTypeEnum.PROGRESSING)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findTeamProgressingByPageWithTypeAndKeyword(teamIds, type, keyword, page);
							} else {
								page = rollingPlanService.findTeamProgressingByPageWithType(teamIds, type, page);
							}
						} else if (status.equals(RollingPlanTypeEnum.UNPROGRESS)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findTeamUnProgressByPageWithTypeAndKeyword(teamIds, type, keyword, page);
							} else {
								page = rollingPlanService.findTeamUnProgressByPageWithType(teamIds, type, page);
							}
						} else if (status.equals(RollingPlanTypeEnum.PAUSE)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findTeamPausedByPageWithTypeAndKeyword(teamIds, type, keyword, page);
							} else {
								page = rollingPlanService.findTeamPausedByPageWithType(teamIds, type, page);
							}
						} else if (status.equals(RollingPlanTypeEnum.UNCOMPLETE)) {
							if (CommonUtility.isNonEmpty(keyword)) {
								page = rollingPlanService.findTeamUnCompleteByPageWithTypeAndKeyword(teamIds, type, keyword, page);
							} else {
								page = rollingPlanService.findTeamUnCompleteByPageWithType(teamIds, type, page);
							}
						} else {
							return null;
						}
					}
				}
			}
			
			if (null != page.getDatas()) {
				List<RollingPlanDataResult> rollingPlanDataResults = page.getDatas().stream().map(rollingPlan -> {
					return this.rollingPlanTransferToSixLevelRollingPlan(rollingPlan);
				}).collect(Collectors.toList());
				
				rollingPlanPageDataResult.setPageCounts(page.getPageCount());
				rollingPlanPageDataResult.setPageNum(page.getPageNum());
				rollingPlanPageDataResult.setPageSize(page.getPagesize());
				rollingPlanPageDataResult.setTotalCounts(page.getTotalCounts());
				rollingPlanPageDataResult.setData(rollingPlanDataResults);
			}
			
			return rollingPlanPageDataResult;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public RollingPlanDataResult rollingPlanTransferToSixLevelRollingPlan(RollingPlan rollingPlan) {
		if (null != rollingPlan) {
			RollingPlanDataResult rollingPlanDataResult = new RollingPlanDataResult();
			rollingPlanDataResult.setId(rollingPlan.getId());
			rollingPlanDataResult.setQualityplanno(rollingPlan.getQualityplanno());
			rollingPlanDataResult.setCroeLevel(rollingPlan.getCorelevel());
			rollingPlanDataResult.setDrawingNo(rollingPlan.getDrawno());
			rollingPlanDataResult.setItpNo(rollingPlan.getItpno());
			rollingPlanDataResult.setLineNo(rollingPlan.getLineno());
			rollingPlanDataResult.setMatelial(rollingPlan.getMaterialtype());
			rollingPlanDataResult.setPlanEndDate(rollingPlan.getPlanenddate());
			rollingPlanDataResult.setPlanStartDate(rollingPlan.getPlanstartdate());
			rollingPlanDataResult.setPoints(rollingPlan.getPoints());
			rollingPlanDataResult.setProjectCost2(rollingPlan.getProjectcost2());
			rollingPlanDataResult.setProjectCost(rollingPlan.getProjectcost());
			rollingPlanDataResult.setProjectNo(rollingPlan.getMarkNo());
			rollingPlanDataResult.setProjectUnit(rollingPlan.getProjectunit());
			rollingPlanDataResult.setRoomNo(rollingPlan.getRoomno());
			rollingPlanDataResult.setSpeification(rollingPlan.getSpecification());
			rollingPlanDataResult.setSystemNo(rollingPlan.getSystemno());
			rollingPlanDataResult.setWorkListNo(rollingPlan.getWeldlistno());
			rollingPlanDataResult.setWorkPackageNo(rollingPlan.getWorkpackageno());
			rollingPlanDataResult.setType(rollingPlan.getType());
			rollingPlanDataResult.setWeldno(rollingPlan.getWeldno());
			rollingPlanDataResult.setProjectType(rollingPlan.getProjectType());
			rollingPlanDataResult.setUnitNo(rollingPlan.getUnitno());
			rollingPlanDataResult.setSubItem(rollingPlan.getSubItem());
			rollingPlanDataResult.setWelddate(rollingPlan.getWelddate());
			rollingPlanDataResult.setSpot(rollingPlan.getSpot());
			rollingPlanDataResult.setPlanAmount(rollingPlan.getPlanamount());
			rollingPlanDataResult.setItemName(rollingPlan.getItemname());
			rollingPlanDataResult.setItemNo(rollingPlan.getItemno());
			rollingPlanDataResult.setProjectName(rollingPlan.getProjectname());
			rollingPlanDataResult.setPlanBeginProgressDate(rollingPlan.getPlanfinishdate());
			rollingPlanDataResult.setDrawingVersion(rollingPlan.getDrawSerial());
			rollingPlanDataResult.setDosage(rollingPlan.getRealProjectcost());//实际完成工程量
			//继续添加字段
			rollingPlanDataResult.setDeviceName(rollingPlan.getDeviceName());
			rollingPlanDataResult.setDeviceNo(rollingPlan.getDeviceNo());
			rollingPlanDataResult.setDeviceType(rollingPlan.getDeviceType());
			rollingPlanDataResult.setMarkNo(rollingPlan.getMarkNo());
			rollingPlanDataResult.setUnitTotalCount(rollingPlan.getUnitTotalCount());
			rollingPlanDataResult.setUnitUncompleteCount(rollingPlan.getUnitUncompleteCount());
			rollingPlanDataResult.setLoopTotalCount(rollingPlan.getLoopTotalCount());
			rollingPlanDataResult.setLoopUncompleteCount(rollingPlan.getLoopUncompleteCount());
			rollingPlanDataResult.setRinseName(rollingPlan.getRinseName());
			rollingPlanDataResult.setRinseType(rollingPlan.getRinseType());
			rollingPlanDataResult.setItpName(rollingPlan.getMQP_NAME());
			rollingPlanDataResult.setDeviceName(rollingPlan.getDeviceName());
			rollingPlanDataResult.setDeviceType(rollingPlan.getDeviceType());
			if ((CommonUtility.isNonEmpty(rollingPlan.getRollingplanflag()) && RollingPlanFlag.PROBLEM.equals(rollingPlan.getRollingplanflag())) || 
					(CommonUtility.isNonEmpty(rollingPlan.getWitnessflag()) && WitnessFlagEnum.LAUNCHED.name().equals(rollingPlan.getWitnessflag()))) {
				if (rollingPlan.getRollingplanflag()!=null && rollingPlan.getRollingplanflag().equals(RollingPlanFlag.PROBLEM)) {
					rollingPlanDataResult.setStatus(StatusEnum.PAUSE.name());
					rollingPlanDataResult.setProblemFlag(true);
				} else {
					rollingPlanDataResult.setStatus(StatusEnum.PROGRESSING.name());
				}
			} else if (null != rollingPlan.getWorkSteps() && !rollingPlan.getWorkSteps().isEmpty()) {
				long countDone = rollingPlan.getWorkSteps().stream().filter(workStep -> workStep.getStepflag().equals("DONE")).count();
				if (countDone == rollingPlan.getWorkSteps().size()) {
					rollingPlanDataResult.setStatus(StatusEnum.COMPLETED.name());
				} else if (countDone == 0) {
					if(RollingPlanFlag.COMPLETED.toString().equals(rollingPlan.getRollingplanflag())){
						rollingPlanDataResult.setStatus(StatusEnum.PROGRESSING.name());
					}else{
						rollingPlanDataResult.setStatus(StatusEnum.UNPROGRESSING.name());
					}
				} else {
					if(CommonUtility.isNonEmpty(rollingPlan.getRollingplanflag()) || WitnessFlagEnum.LAUNCHED.name().equals(rollingPlan.getWitnessflag())){
						rollingPlanDataResult.setStatus(StatusEnum.PROGRESSING.name());
					}else{
						rollingPlanDataResult.setStatus(StatusEnum.UNPROGRESSING.name());
					}
				}
			} else {
				rollingPlanDataResult.setStatus(StatusEnum.UNPROGRESSING.name());
			}
			
			if (rollingPlan.getId().intValue() % 3 == 0 ) {
				rollingPlanDataResult.setRemarks("这是一句REMARK, ID-" + rollingPlan.getId());
			}
			if (null != rollingPlan.getConsmonitor()) {
				rollingPlanDataResult.setConsmonitor(userApiService.userTransferToUserResult(userService.findUserById(rollingPlan.getConsmonitor()),false));
			}
			if (null != rollingPlan.getConsteam()) {
				rollingPlanDataResult.setConsteam(userApiService.userTransferToUserResult(userService.findUserById(rollingPlan.getConsteam()),false));
			}
			
			List<WorkStep> workSteps = rollingPlan.getWorkSteps();
			
		
			
			if (null != workSteps && !workSteps.isEmpty() && CommonUtility.isNonEmpty(rollingPlan.getWitnessflag()) && 
					rollingPlan.getWitnessflag().equals(WitnessFlagEnum.COMPLETED.name())) {
				Set<UserResult> witnessers = new HashSet<UserResult>();
				workSteps.stream().forEach(workStep -> {
					List<WorkStepWitness> workStepWitnesses = workStep.getWorkStepWitnesses();
					if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
						workStepWitnesses.stream().forEach(workStepWitness -> {
							if (null != workStepWitness.getWitness()) {
								UserResult witnesser = userApiService.userTransferToUserResult(userService.findUserById(workStepWitness.getWitness()),false);
								if (null != witnesser) {
									witnessers.add(witnesser);
								}
							}
						});
					}
				});
				
				PlanUtil.lastOneMark(workSteps);
				
				WorkStep lastOne = workSteps.stream().filter(ws -> {return ws.isLastone();}).collect(Collectors.toList()).get(0);
				
				List<WorkStepWitness> workStepWitnesses = lastOne.getWorkStepWitnesses();
				if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
					List<WorkStepWitness> qc1WSWResult = workStepWitnesses.stream().filter(wsw -> {
						return !CommonUtility.isNonEmpty(wsw.getWitnessflag()) && wsw.getNoticePoint().equals(NoticePointType.QC1.name());
					}).collect(Collectors.toList());
					
					if (null != qc1WSWResult && !qc1WSWResult.isEmpty()) {
						WorkStepWitness qc1 = qc1WSWResult.get(0);
						UserResult witnesser = userApiService.userTransferToUserResult(userService.findUserById(qc1.getWitness()),false);
						if (null != witnesser) {
							rollingPlanDataResult.setQC1Witnesser(witnesser);
						}
						
						if (null == qc1.getRealwitnessdata()) {
							rollingPlanDataResult.setQC1WitnessDate(qc1.getWitnessdate());
						} else {
							rollingPlanDataResult.setQC1WitnessDate(qc1.getRealwitnessdata());
						}
					}
				}
				
				rollingPlanDataResult.setWitnessers(witnessers);
			}
			
			if (null != rollingPlan.getWelder() || CommonUtility.isNonEmpty(rollingPlan.getWelder2())) {
				if (null != rollingPlan.getWelder()) {
					User welder = userService.findUserById(rollingPlan.getWelder());
					if (null != welder) {
						rollingPlanDataResult.setWelder(userApiService.userTransferToUserResult(welder,false));
					}
				} else {
					UserResult userResult = new UserResult();
					userResult.setRealname(rollingPlan.getWelder2());
					rollingPlanDataResult.setWelder(userResult);
				}
				
			} else if (CommonUtility.isNonEmpty(rollingPlan.getWitnessflag()) && rollingPlan.getWitnessflag().equals(WitnessFlagEnum.COMPLETED.name())
					&& "GDJH".equals(rollingPlan.getType())) {
//				true是需要填
//				backfill=false; 其它专业
				rollingPlanDataResult.setBackFill(true);
			}
			
			List<EnpowerMaterialInfo> materials = materialInfoService.findByRollingPlanId(rollingPlan.getId());
			if(materials!=null && materials.size()>0){
				
				List<RollingMaterialResult> rets = new ArrayList<RollingMaterialResult>();
				
				for(EnpowerMaterialInfo bean:materials){
					RollingMaterialResult ret = new RollingMaterialResult();
					ret.setId(bean.getId());
					/**
					 * 物项名称
					 */
					ret.setMaterialName(bean.getMaterialName());
					/**
					 * 物项编号
					 */
					ret.setMaterialIdentifier(bean.getMaterialIdentifier());
					/**
					 * 规格型号
					 */
					ret.setSpecificationModel(bean.getSpecificationModel());
					/**
					 * 计划用量
					 */
					ret.setPlanDosage(bean.getPlanDosage());
					/**
					 * 实际用量
					 */
					ret.setActualDosage(bean.getActualDosage());
					/**
					 * 材质
					 */
					ret.setMaterialQuality(bean.getMaterialQuality());
					
					/**单位 预留*/
					ret.setUnit(bean.getUnit());
					
					rets.add(ret);
				}
				
				//设置 滚动计划材料列表，用于材料回填
				rollingPlanDataResult.setMaterialList(rets);
			}
			
			
			return rollingPlanDataResult;
		}
		
		return null;
	}

	/**
	 * 进度计划同步
	 * @param sixLevelRollingPlan
	 * @return
	 */
	@Override
	public JsonResult<Boolean> save(List<SixLevelRollingPlan> sixLevelRollingPlans) throws Exception{
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean f = false ;
		if(sixLevelRollingPlans==null || sixLevelRollingPlans.size()==0){
			f = false ;
			result.setResponseResult(f);
			return result ;
		}else{
			try{
				List<Modules> moduleList = modulesService.findByAll();

				for(SixLevelRollingPlan sixLevelRollingPlan:sixLevelRollingPlans){

					RollingPlan b = new RollingPlan();
					RollingPlan check = new RollingPlan();
					
					List<EnpowerMaterialInfo> materialInfoListx = sixLevelRollingPlan.getMaterialInfoList();
					//不为空就是焊口条目
//					boolean weldFlag = sixLevelRollingPlan.getWeldno()!=null && !"".equals(sixLevelRollingPlan.getWeldno().trim());
//					if(weldFlag==false){
//						if(materialInfoListx==null || materialInfoListx.size()==0){//焊口作业条目不推送材料信息，其它的需要
//							throw new RuntimeException("滚动计划材料信息没有！material info is losted!WorkListId="+sixLevelRollingPlan.getWorkListId());
//						}
//					}
//					20180412 取消材料为空的限制条件
					List<WorkStep> workStepListx = sixLevelRollingPlan.getWorkStepList();
					if(workStepListx==null || workStepListx.size()==0){
						logger.debug("sixLevelRollingPlan.getWorkListId()="+sixLevelRollingPlan.getWorkListId());
						throw new RuntimeException("滚动计划工序信息没有！rollingplan work step message losted !!WorkListId="+sixLevelRollingPlan.getWorkListId());
					}
					
					List<EnpowerJobOrder> jobOrderListx = sixLevelRollingPlan.getWorkOrderList();
					if(jobOrderListx==null||jobOrderListx.isEmpty()){
//						throw new RuntimeException("滚动计划任务单信息列表没有！job order list is losted WorkListId="+sixLevelRollingPlan.getWorkListId());
					}
					//所有工序必须存在至少一个工序有一个选点信息
					List<WorkStep> workStepListcheck = sixLevelRollingPlan.getWorkStepList();
					boolean checkstep = workStepListcheck.stream().anyMatch(workstep->{
						boolean hasNotice = false ;

						if (workstep.getNoticeaqc1() != null && !"".equals(workstep.getNoticeaqc1().trim())) {
							hasNotice = true ;
						}else if (workstep.getNoticeaqc2() != null && !"".equals(workstep.getNoticeaqc2().trim())) {
							if(ContextPath.qc1ReplaceQc2){//如果是福清的QC1替换QC2，因不设QC2,忽略QC2
								hasNotice = false ;
							}else{
								hasNotice = true ;
							}
							
						}else if (workstep.getNoticeczecqc() != null && !"".equals(workstep.getNoticeczecqc().trim())) {
							hasNotice = true ;
						}else if (workstep.getNoticeczecqa() != null && !"".equals(workstep.getNoticeczecqa().trim())) {
							hasNotice = true ;
						}else if (workstep.getNoticepaec() != null && !"".equals(workstep.getNoticepaec().trim())) {
							hasNotice = true ;
						}
						return hasNotice ;
					});
					if(checkstep == false){
						throw new RuntimeException("滚动计划无已选点工序错误！rollingplan not exists workstep that selected point !!WorkListId="+sixLevelRollingPlan.getWorkListId());
					}
					
					
					//根据效验重复
//						计划主键id	enpowerPlanId
						b.setEnpowerPlanId(sixLevelRollingPlan.getEnpowerPlanId());
						b.setQualityplanno(sixLevelRollingPlan.getQualityplanno());
						b.setCorelevel(sixLevelRollingPlan.getCroeLevel());
						b.setDrawno(sixLevelRollingPlan.getDrawingNo());
//						图纸版本号	drawSerial
						b.setDrawSerial(sixLevelRollingPlan.getDrawSerial());
//						b.setItpno(sixLevelRollingPlan.getItpNo());
//						b.setLineno(sixLevelRollingPlan.getLineNo());
						b.setMaterialtype(sixLevelRollingPlan.getMatelial());
//						b.setPlanenddate(sixLevelRollingPlan.getPlanEndDate());
						String sDate = sixLevelRollingPlan.getPlanStartDate();
						String eDate = sixLevelRollingPlan.getPlanEndDate();
						//2017-10-11 11:25:34
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						try{
							if(CommonUtility.isNonEmpty(sDate)){
								sDate = sDate.replaceAll("-", "/");
								b.setPlanstartdate(sdf.parse(sDate));
							}
							if(CommonUtility.isNonEmpty(eDate)){
								eDate = eDate.replaceAll("-", "/");
								b.setPlanenddate(sdf.parse(eDate));
							}
						}catch(Exception e){
							e.printStackTrace();
							f = false ;
							result.setMessage("操作执行成功失败");
							result.setResponseResult(f);
							return result ;
						}
						
						if(sixLevelRollingPlan.getPoints()!=null ){
							b.setPoints(sixLevelRollingPlan.getPoints());
						}
						b.setProjectcost(sixLevelRollingPlan.getProjectCost());
						b.setProjectcost2(sixLevelRollingPlan.getProjectCost2());
						b.setProjectno(sixLevelRollingPlan.getProjectNo());
						b.setProjectunit(sixLevelRollingPlan.getProjectUnit());
						b.setRoomno(sixLevelRollingPlan.getRoomNo());
						b.setSpecification(sixLevelRollingPlan.getSpeification());
						b.setSystemno(sixLevelRollingPlan.getSystemNo());
						b.setWeldlistno(sixLevelRollingPlan.getWorkListNo());
						check.setWorkListId(sixLevelRollingPlan.getWorkListId());
//						b.setWorkpackageno(sixLevelRollingPlan.getWorkPackageNo());

						//enpower type=电仪　专业=电气　或者　专业=仪表
						
						//enpower type=通防　专业＝机械　　　或者　通风　　通风就是通防
						
						if("电仪".equals(sixLevelRollingPlan.getType())||"机通".equals(sixLevelRollingPlan.getType())){
							sixLevelRollingPlan.setType(sixLevelRollingPlan.getSPECIALTY());
						}
						if("通防".equals(sixLevelRollingPlan.getType())){
							sixLevelRollingPlan.setType(sixLevelRollingPlan.getSPECIALTY());
						}
						
						b.setType(sixLevelRollingPlan.getType());
						b.setEnpowerType(sixLevelRollingPlan.getType());
						//滚动计划类型 ，需要跟Enpower自动映射
						if("管道".equals(b.getType())){
							b.setType("GDJH");
						}else if("机械".equals(b.getType())){
							b.setType("JXJH");
						}else if("调试".equals(b.getType())){
							b.setType("TSJH");
						}else if("主系统".equals(b.getType())){
							b.setType("ZXT");
						}
						/**
						 * DQJH	GDJH	JXJH	TFJH	YBJH	ZXT
						 */

						moduleList.stream().forEach(mod->{
							if(mod.getEnpowerName()!=null && mod.getEnpowerName().equals(b.getType())){
								b.setType(mod.getType());
							}
						});
						if(b.getType()==null || "".equals(b.getType().trim())){
							logger.error("滚动计划同步 类型为空错误  weldlistno="+sixLevelRollingPlan.getWorkListId());
							throw new RuntimeException("滚动计划同步 类型为空错误  作业条目编号Id="+sixLevelRollingPlan.getWorkListId());
						}
						b.setWeldno(sixLevelRollingPlan.getWeldno());
						b.setProjectType(sixLevelRollingPlan.getProjectType());
//						质量计划id	qualityplanid
						b.setQualityplanid(sixLevelRollingPlan.getQualityplanid());
						b.setUnitno(sixLevelRollingPlan.getUnitNo());
						b.setSubItem(sixLevelRollingPlan.getSubItem());
//						班长名称	consmonitorName
						b.setConsmonitorName(sixLevelRollingPlan.getConsmonitorName());
//						设备名称
						b.setDeviceName(sixLevelRollingPlan.getDeviceName());
//						设备编号
						b.setDeviceNo(sixLevelRollingPlan.getDeviceNo());
//						设备类型
						b.setDeviceType(sixLevelRollingPlan.getDeviceType());
//						部件号/标识号
						b.setMarkNo(sixLevelRollingPlan.getMarkNo());
//						单元件总数
						b.setUnitTotalCount(sixLevelRollingPlan.getUnitTotalCount());
//						单元件未完成数
						b.setUnitUncompleteCount(sixLevelRollingPlan.getUnitUncompleteCount());
//						回路总数
						b.setLoopTotalCount(sixLevelRollingPlan.getLoopTotalCount());
//						回路未完成数
						b.setLoopUncompleteCount(sixLevelRollingPlan.getLoopUncompleteCount());
//						冲洗/试压名称
						b.setRinseName(sixLevelRollingPlan.getRinseName());
//						冲洗/试压类型
						b.setRinseType(sixLevelRollingPlan.getRinseType());
//						ITP类型（类别）
						b.setITP_TYPES(sixLevelRollingPlan.getITP_TYPES());
//						ITP模板类型（模板类型）
						b.setITP_TYPE(sixLevelRollingPlan.getITP_TYPE());
//						(质量计划的)专业
						b.setSPECIALTY(sixLevelRollingPlan.getSPECIALTY());
//						版本（质量计划版本）
						b.setT_REV(sixLevelRollingPlan.getT_REV());
//						区域 
						b.setAREA(sixLevelRollingPlan.getAREA());
//						通知单名称（ITP模板名称(中文)）
						b.setMQP_NAME(sixLevelRollingPlan.getMQP_NAME());
						/**
					"是否报量FLAG
						 * "(向app推送时一起推)  
						 */
						b.setIsReport(sixLevelRollingPlan.getIsReport());
						/**
						 * enpower作业条目id
						 * (向app推送时一起推)
						 */
						b.setWorkListId(sixLevelRollingPlan.getWorkListId());

						//班长标识id赋值   根据enpowerid查本系统的用户id
						User moniter = userService.findUserEnpowerId(sixLevelRollingPlan.getConsmonitorEnpower());
						if(moniter!=null){//已选班长已经映射到本系统用户中
							b.setConsmonitor(moniter.getId());
						}else{
							//查不到 写死一个 已选班长的程序自动映射 287
							b.setConsmonitor(287);
							logger.error("滚动计划同步查不到已选班长,写死一个班长接收了...作业条目编号 weldlistno="+b.getWeldlistno());
						}
						//原始enpower班长标识id保存下来
						b.setConsmonitorEnpower(sixLevelRollingPlan.getConsmonitorEnpower());
						
//						施工班组代码	consmonitorCode
						b.setConsmonitorCode(sixLevelRollingPlan.getConsmonitorCode());
//						施工队	conscaptainName
						b.setConscaptainName(sixLevelRollingPlan.getConscaptainName());
//						b.setWelddate(sixLevelRollingPlan.getWelddate());
//						b.setPlanamount(sixLevelRollingPlan.getPlanAmount());
//						b.setItemname(sixLevelRollingPlan.getItemName());
//						b.setItemno(sixLevelRollingPlan.getItemNo());
						b.setProjectname(sixLevelRollingPlan.getProjectName());
						b.setIsend(0);
						b.setQcsign(0);
						b.setCreatedOn(java.util.Calendar.getInstance().getTime());
						b.setCreatedBy("EnpowerSYSTEM");
						b.setPMainId(sixLevelRollingPlan.getPMainId());//enpower的关联作业条目和材料信息字段，只做保存即可
						b.setConsmonitordate(Calendar.getInstance().getTime());

						//唯一性验证
						List<RollingPlan> datas = rollingPlanService.findByWorkListId(sixLevelRollingPlan.getWorkListId());
						if(datas!=null && datas.size()>0){
							
							String err ="滚动计划基础信息保存不成功！已经存在:"+check.getWorkListId()+" "+check.getWeldlistno() ;
							
							//异常记录原始内容
							FileUtility ut = new FileUtility();
							ut.createFile(null, err+CommonUtility.toJson(sixLevelRollingPlans));
							
							result.setCode("-2018");
							result.setMessage(err);
							result.setResponseResult(false);
							return result ;
						}
						RollingPlan rollingPlan = null;
 						
						try{
							rollingPlan= this.rollingPlanService.add(b);
						}catch(Exception e){
							e.printStackTrace();
							logger.error("add rolling plan into mysql db error!");
							throw e ;
						}
						
//						计划主键id	enpowerPlanId
//						作业条目编号	workListNo
//						机组号	unitNo
//						质量计划id	qualityplanid
//						质量计划号	qualityplanno
//						图纸号	drawingNo
//						图纸版本号	drawSerial
//						房间号	roomNo
//						工程量类别	projectType
//						工程量编号	projectNo
//						焊口号	weldno
//						计划施工日期	planStartDate
//						施工队	conscaptainName
//						施工班组代码	consmonitorCode
//						班长标识id	consmonitor
//						班长名称	consmonitorName
//						点数	points
//						子项	subItem
//						系统号	systemNo
//						工程量名称	projectName
//						工程量	projectCost
//						规格	speification
//						材质	matelial
//						核级	croeLevel
//						单位	projectUnit
////						模块专业	type
						
						/**
						 * 材料信息 保存
						 */
						
						if(rollingPlan==null){
							throw new RuntimeException("滚动计划基础信息保存不成功！");
						}
						
						List<EnpowerMaterialInfo> materialInfoList = sixLevelRollingPlan.getMaterialInfoList();
//						if(weldFlag == false){
							if(materialInfoList==null || materialInfoList.size()==0){
//								throw new RuntimeException("滚动计划材料信息没有！material info is losted!");
							}else{
								for(EnpowerMaterialInfo material:materialInfoList){
									material.setRollingPlanId(rollingPlan.getId());
									if(material.getPlanDosage()==null||"".equals(material.getPlanDosage().trim())){
										material.setPlanDosage(null);
									}
									material.setIsDel(false);
									this.materialInfoService.add(material);
								}
							}
//						}
						
						List<WorkStep> workStepList = sixLevelRollingPlan.getWorkStepList();
						if(workStepList==null || workStepList.size()==0){
							logger.debug("sixLevelRollingPlan.getWorkListId()="+sixLevelRollingPlan.getWorkListId());
							
							throw new RuntimeException("滚动计划工序信息没有！rollingplan work step message losted !!");
						}else{
							//排序
							workStepList  = workStepList.stream().sorted((n1,n2)->
					        {return n1.getStepIdentifier().compareTo(n2.getStepIdentifier());}
					        		).collect(Collectors.toList());
							
							boolean isPrepared = false ;
					        for(int i=0;i<workStepList.size();i++){
					        	WorkStep workstep = workStepList.get(i);
					        	
					        	//为了APP后续测试，滚动计划只取３条，每条只取３个工序，３个工序的QC1全部选上W，第一个工序全选点，APP同步程序临时做修改处理。
//					        	if(i>2){
//					        		
//					        	}else{
//					        		workstep.setNoticeaqc1("W");
//					        	}
//					        	if(i==0){
//					        		workstep.setNoticeaqc1("W");
//
//					        		workstep.setNoticeaqc2("W");
//
//					        		workstep.setNoticeczecqc("W");
//
//					        		workstep.setNoticeczecqa("W");
//
//					        		workstep.setNoticepaec("W");
//					        		
//					        	}
					        	
								if (workstep.getNoticeaqc1() != null && !"".equals(workstep.getNoticeaqc1().trim())) {
									workstep.setNoticeaqc1(workstep.getNoticeaqc1().toUpperCase());
								}else{
									workstep.setNoticeaqc1(null);
								}
								if (workstep.getNoticeaqc2() != null && !"".equals(workstep.getNoticeaqc2().trim())) {
									if(ContextPath.qc1ReplaceQc2){//如果是福清的QC1替换QC2，因不设QC2,忽略QC2

										workstep.setNoticeaqc2(null);
									}else{

										workstep.setNoticeaqc2(workstep.getNoticeaqc2().toUpperCase());
									}
								}else{
									workstep.setNoticeaqc2(null);
								}
								if (workstep.getNoticeczecqc() != null && !"".equals(workstep.getNoticeczecqc().trim())) {
									workstep.setNoticeczecqc(workstep.getNoticeczecqc().toUpperCase());
								}else{
									workstep.setNoticeczecqc(null);
								}
								if (workstep.getNoticeczecqa() != null && !"".equals(workstep.getNoticeczecqa().trim())) {
									workstep.setNoticeczecqa(workstep.getNoticeczecqa().toUpperCase());
								}else{
									workstep.setNoticeczecqa(null);
								}
								if (workstep.getNoticepaec() != null && !"".equals(workstep.getNoticepaec().trim())) {
									workstep.setNoticepaec(workstep.getNoticepaec().toUpperCase());
								}else{
									workstep.setNoticepaec(null);
								}
								workstep.setCreatedOn(java.util.Calendar.getInstance().getTime());
								workstep.setCreatedBy("EnpowerSYSTEM");
								workstep.setRollingPlan(rollingPlan);
//					        	and (noticeaqc1 is not null or noticeaqc2 is not null or noticeczecqc is not null or noticeczecqa is not null or noticepaec is not null )
					        	if(CommonUtility.isNonEmpty(workstep.getNoticeaqc1())
					        			||CommonUtility.isNonEmpty(workstep.getNoticeaqc2())
					        			||CommonUtility.isNonEmpty(workstep.getNoticeczecqc())
					        			||CommonUtility.isNonEmpty(workstep.getNoticeczecqa())
					        			||CommonUtility.isNonEmpty(workstep.getNoticepaec())){//有选点的，初始化
					        		if(isPrepared==false){//还没有首次置prepare的
						        		isPrepared = true ;
						        		workStepList.get(i).setStepflag(StepFlag.PREPARE);
					        		}else{//已有prepare的，其它置为待发起
						        		workStepList.get(i).setStepflag(StepFlag.UNDO);
					        		}
					        	}else{//没有选点的，置为完成
					        		workstep.setStepflag(StepFlag.DONE);//不用发起见证的
					        	}
					        	workStepList.get(i).setStepno(i+1);
								this.workStepService.add(workstep);
					        }
						}
						List<EnpowerJobOrder> jobOrderList = sixLevelRollingPlan.getWorkOrderList();
						if(jobOrderList==null||jobOrderList.isEmpty()){
//							throw new RuntimeException("滚动计划任务单信息列表没有！job order list is losted");
						}else{
							for(EnpowerJobOrder equipment:jobOrderList){
								equipment.setRollingPlanId(rollingPlan.getId());
								this.jobOrderService.add(equipment);
							}
						}
						
//						List<EnpowerEquipment> equipmentList = sixLevelRollingPlan.getEquipmentList();
//						if(equipmentList==null|| equipmentList.size()==0){
//							f = false ;
//							result.setMessage("滚动计划设备信息没有！");
//							result.setResponseResult(f);
//							return result ;
//						}else{
//							equipmentList.stream().forEach(equipment->{
//								equipment.setRollingPlanId(rollingPlan.getId());
//								this.equipmentService.add(equipment);
//							});
//						}
				}
			}catch(Exception e){
				//异常记录原始内容
				String err = e.getMessage();
				FileUtility ut = new FileUtility();
				ut.createFile(null, err+CommonUtility.toJson(sixLevelRollingPlans));
				
				e.printStackTrace();
				f = false ;
				result.setCode("-2001");
				result.setMessage("操作异常："+err);
				result.setResponseResult(f);
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return result ;
			}
			
			//最终成功返回
			f = true ;
			result.setResponseResult(f);
			return result ;
		}
	}
	/**
	 * 查询enpower 任务单信息列表
	 */
	public List<EnpowerJobOrder> findEnpowerJobOrderByRollingPlanId (Integer rollingPlanId){
		return jobOrderService.findByRollingPlanId(rollingPlanId);
	}
	
	

/**
 * 发点子接口 推送接口名: 1插入发点通知单信息		 所需数据
 * @param rollingPlanId
 * @return
 */
	public List<EnpowerRequestSendMain> findEnpowerSendPointMainByRollingPlanId (Integer rollingPlanId){

		logger.info("滚动计划id="+rollingPlanId);
		List<EnpowerRequestSendMain> mainList = new ArrayList<EnpowerRequestSendMain>();
		/**
		 * 一、发点主信息									
		系统通知单编码（APP自动根据规则生成）	itp号	施工班组代码（PW-01这种)	作业组长名称	技术员（Enpower是存在技术员那个字段的，APP传组长名称）	机组指向系统	主专业（电气、管道等。。。）	编制人（APP里面是组长）	编制日期	施工队名称
		二、发点子信息(多条，选了几条工序，发点子信息就是几条）									
		通知单明细	设备清单	任务单信息列表							
		注：ipt信息信息内容+工序信息									
		4 管道发点接口 （作业组长发起见证的时候需要后台发起）
		 */
		RollingPlan plan = rollingPlanService.findById(rollingPlanId);
//				发点主信息
		EnpowerRequestSendMain sendMain = new EnpowerRequestSendMain();
		sendMain.setAppId(String.valueOf(plan.getId()));
		//质量计划号
		sendMain.setQualityplanno(plan.getQualityplanno());
		//机组号
//				机组指向系统
		logger.info("机组指向系统,查的App的机组="+plan.getUnitno() );
		sendMain.setUnitNo(plan.getUnitno());
		//子项
		sendMain.setSubItem(plan.getSubItem());
		//系统号
		sendMain.setSystemNo(plan.getSystemno());
		String noticeOrderNo = getNoticeOrderNo(plan);
		logger.info("系统通知单编码="+noticeOrderNo);
		sendMain.setSEND_NO(noticeOrderNo);

		//通知单名称（ITP模板名称(中文)）
		sendMain.setMQP_NAME(plan.getMQP_NAME());
		//ITP类型（类别）
		sendMain.setITP_TYPES(plan.getITP_TYPES());
		//状态
//		道磊  15:28:43
//		这flag不是用作业条目的那个flag字段的值，直接写接收就行了
		sendMain.setState("接收");//getRollingState(plan));

		//(质量计划的)专业
		sendMain.setSPECIALTY(plan.getSPECIALTY());
		//版本（质量计划版本）
		sendMain.setT_REV(plan.getT_REV());
		//区域 
		sendMain.setAREA(plan.getAREA());
		//ITP模板类型（模板类型）
		sendMain.setITP_TYPE(plan.getITP_TYPE());
//				作业组长名称
		String consteamName =null;
		if(plan.getConsteam()!=null){
			User team = userService.findUserById(plan.getConsteam());
			if(team!=null){
				consteamName=team.getRealname();
			}
		}
//				作业组长名称
		logger.info("作业组长名称="+consteamName );
//				技术员（Enpower是存在技术员那个字段的，APP传组长名称）
		logger.info("技术员（Enpower是存在技术员那个字段的，APP传组长名称）="+consteamName );
//				编制人（APP里面是组长）
		logger.info("编制人（APP里面是组长）="+consteamName );
		sendMain.setConsteamName(consteamName);
		sendMain.setConsteamName2(consteamName);
		sendMain.setConsteamName3(consteamName);
		//施工班组代码
//				施工班组代码（PW-01这种)
		logger.info("施工班组代码（PW-01这种)="+plan.getConsmonitorCode() );
		sendMain.setConsmonitorCode(plan.getConsmonitorCode());
		//电话
		sendMain.setTel("18988888888");//TODO 7
		//技术员（Enpower是存在技术员那个字段的，APP传组长名称）
		sendMain.setConsteamName(plan.getConsteamName());
		logger.info("技术员="+sendMain.getConsteamName());
		//施工队
//				施工队名称
		logger.info("施工队名称="+plan.getConscaptainName() );
		sendMain.setConscaptainName(plan.getConscaptainName());
		//编制人
		sendMain.setConsteamName(consteamName);//plan.getConsteamName() TODO 8
		//取系统当前日期
//				编制日期
//		logger.info("编制日期=传当前时间="+DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime() ));
		//临时去掉
//		sendMain.setSysDate(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));//
		
		//设置　福清测试时，发布消点信息推不到enpower,才从沙道磊处理获知，需要增加以下几个数据。
		/*
		 * A_SELEct 有值　１　没值设0
						B,C,D,H 
		 */

		List<WorkStep> workstepList = plan.getWorkSteps();

		boolean isSelectb=false;
		boolean isSelectc=false;
		boolean isSelectd=false;
		boolean isSelecth=false;

		isSelectb = workstepList.stream().noneMatch(s->StringUtils.isNoneBlank(s.getNoticeczecqc()));
		isSelectc = workstepList.stream().noneMatch(s->StringUtils.isNoneBlank(s.getNoticeczecqa()));
		isSelectd = workstepList.stream().noneMatch(s->StringUtils.isNoneBlank(s.getNoticepaec()));
		isSelecth = workstepList.stream().noneMatch(s->StringUtils.isNoneBlank(s.getNoticeaqc2()));
		
		//noneMatch没有=true 设置0
		sendMain.setBSelect(isSelectb?"0":"1");
		sendMain.setCSelect(isSelectc?"0":"1");
		sendMain.setDSelect(isSelectd?"0":"1");
		sendMain.setHSelect(isSelecth?"0":"1");
		
		mainList.add(sendMain);


		return mainList ;

	}

	/**
	 *
	 * 20190708　优化数据结构
	 * @param wplan
	 * @param consteamName
	 * @return
	 */
	public List<EnpowerRequestSendNotice> findEnpowerSendPointNoticeDetailByWorkStepIds (WitingPlan wplan,String consteamName) {

		//因为只发一次点，所以要将所有的工序都放入插入通知单明细
		List<WorkStep> workStepList = wplan.getPlan().getWorkSteps();
		RollingPlan plan = wplan.getPlan();
		logger.info("发点子接口 推送接口名: 2 插入通知单明细		 所需数据");
		if(consteamName==null){// 为了是测试使用的。
			plan = workStepList.get(0).getRollingPlan();
			Integer rollingPlanId = plan.getId();
			logger.info("当前发点滚动计划id="+rollingPlanId);
//			作业组长名称
			if(plan.getConsteam()!=null){
				User team = userService.findUserById(plan.getConsteam());
				if(team!=null){
					consteamName=team.getRealname();
				}
			}
		}
		String _consteamName = consteamName;
		List<EnpowerRequestSendNotice> noticeList = new ArrayList<EnpowerRequestSendNotice>();
		for(WorkStep bean:workStepList){
			EnpowerRequestSendNotice notice = new EnpowerRequestSendNotice();
			//enpower：id
			logger.info("通知单明细id");
//			notice.setId(bean.getId()+"");//TODO 10
			notice.setAppId(String.valueOf(bean.getId()));
			notice.setAppMainId(String.valueOf(plan.getId()));
			notice.setWorklistId(plan.getWorkListId());

			//enpower：MAIN_ID	外键（来源于发点通知单主信息的主键ID）
			//还有通知单明细反填信息中的MAIN_ID 字段不用返回值，为空就行了
//			notice.setMainId(bean.getId()+"");

			//enpower：NOTE_CODE	通知单编号（app生成）
			logger.info("通知单编号（app生成）="+getNoticeOrderNo(plan));
			notice.setNoticeOrderNo(getNoticeOrderNo(plan));//TODO ?? 这个是起什么关联作用吗？

			//enpower：TASK_NAME	作业名称
			logger.info("作业名称（中文）="+bean.getStepname());
			notice.setStepname(bean.getStepname());

			//enpower：TASK_SEQU	作业序号(工序号)
			logger.info("工序号="+bean.getStepIdentifier());
			notice.setStepIdentifier(bean.getStepIdentifier());

			//enpower：QUAL_PLAN_CODE	质量计划编号
			//质量计划号
			logger.info("质量计划号="+bean.getQualityplanno());
			notice.setQualityplanno(bean.getQualityplanno());
			//质量计划id
//			logger.info("质量计划id="+bean.getQualityplanid());

			//enpower：A_POINT	A单位设点(QC1)
			logger.info("QC1="+bean.getNoticeaqc1());
			notice.setNoticeaqc1(bean.getNoticeaqc1());

			//enpower：B_POINT	B单位设点(CZEC QC)
			logger.info("CZEC QC="+bean.getNoticeczecqc());
			notice.setNoticeczecqc(bean.getNoticeczecqc());

			//enpower：C_POINT	C单位设点(CZEC QA)
			logger.info("CZEC QA="+bean.getNoticeczecqa());
			notice.setNoticeczecqa(bean.getNoticeczecqa());

			//enpower：D_POINT	D单位设点(PAEC)
			logger.info("PAEC="+bean.getNoticepaec());
			notice.setNoticepaec(bean.getNoticepaec());

			//enpower：H_POINT	H单位设点（QC2）
			logger.info("QC2="+bean.getNoticeaqc2());
			notice.setNoticeaqc2(bean.getNoticeaqc2());

			//enpower：NOTE_MAN	通知人
			logger.info("技术员（Enpower是存在技术员那个字段的，APP传组长名称）="+_consteamName);
			notice.setConsteamName(_consteamName);

			//enpower：NOTE_DATE	通知日期
			logger.info("系统当前日期");
//			notice.setCurrentDate(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));//TODO 12

			//enpower：SUB_SPEC	子专业
			logger.info("子专业="+bean.getSubSpecialty());
			notice.setSubSpecialty(bean.getSubSpecialty());

			//enpower：WITN_ADDR	见证地点(数据来自发点通知单主信息的 区域字段)
//			notice.setWitnessaddress(bean.getwitnessad);//TODO 13
			logger.info("见证地点(数据来自发点通知单主信息的 区域字段)="+notice.getWitnessaddress());

			List<WorkStepWitnessItemsForm> collect = wplan.getStep().stream().filter(
					b -> {
						
						
						if(b!=null && b.getId()!=null && bean!=null){
							
							return b.getId().equals(bean.getId());
						}else{
							System.out.println("操作异常"+bean+b.getId());
							return false ;
//							throw new Exception("操作异常");
						}
					}
					).collect(Collectors.toList());

			if (collect != null && collect.size() > 0) {

//			enpower：WITN_DATE	见证日期（值为当前时间加一天）
//			logger.info("见证日期（值为当前时间加一天）");
//			Calendar c = Calendar.getInstance();
//			c.add(Calendar.DATE, 1);
				logger.info("20181116见证日期:"+collect.get(0).getWitnessdate());
				notice.setWitnessDate(collect.get(0).getWitnessdate());
			}
			noticeList.add(notice);
		}
		return noticeList ;
	}
/**
 * 发点子接口 推送接口名: 3插入设备清单 所需数据		
 * @param rollingPlanId
 * @return
 */
	public List<EnpowerRequestSendEquipment> findEnpowerSendPointEquipByRollingPlanId (Integer rollingPlanId){

		logger.info("滚动计划id="+rollingPlanId);
//		任务单信息列表
		List<EnpowerJobOrder> eqip = findEnpowerJobOrderByRollingPlanId(rollingPlanId);
		List<EnpowerRequestSendEquipment> equipList = new ArrayList<EnpowerRequestSendEquipment>();
		RollingPlan plan = rollingPlanService.findById(rollingPlanId);
//		作业组长名称
		String consteamName =null;
		if(plan.getConsteam()!=null){
			User team = userService.findUserById(plan.getConsteam());
			if(team!=null){
				consteamName=team.getRealname();
			}
		}
		String _consteamName = consteamName;
		if(eqip!=null && !eqip.isEmpty()){
			eqip.stream().forEach(bean->{

				EnpowerRequestSendEquipment equip = new EnpowerRequestSendEquipment();

				//enpower：TZD_ID	通知单ID(来源于app创建的发点通知单主信息的id)
				equip.setMainId(String.valueOf(rollingPlanId));//bean.getId()+"");//这个字段是要跟通知单信息表关联关联（主子表），所以可以将插入发点通知单新的APP_ID赋给这个字段
				logger.info("外键（来源于发点通知单主信息的主键ID）="+equip.getMainId());

				//enpower：EQUI_CODE	设备编码
				equip.setEquipmentCode(bean.getPitemNo());//任务单信息 pitemNo --作业包位号   ---------->设备编码 EQUI_CODE
				logger.info("设备编码="+equip.getEquipmentCode());

				//enpower：DRAW_NO	图纸号
				logger.info("图纸号="+bean.getDrawNo());
				equip.setDrawingNo(bean.getDrawNo());

				//enpower：S_ID	源ID(来源于任务单信息的主键id)
				logger.info("任务单信息列表enpower作业包工程量的id="+bean.getEnpowerId());
				equip.setEnpowerId(bean.getEnpowerId());

				//enpower：FOUND_MAN	创建人
				logger.info("技术员（Enpower是存在技术员那个字段的，APP传组长名称）="+_consteamName);
				equip.setConsteamName(_consteamName);

				//enpower：FOUND_DATE	创建时间
				//因为Enpower时间报错,去掉调试
//				logger.info("创建时间:系统当前日期");
//				equip.setCreateDate(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));//TODO 16

//				enpower作业包位号	pitemNo
//				logger.info("enpower作业包位号="+bean.getPitemNo());
				
				equipList.add(equip);
				
			});
		}
		
		return equipList;
	}
	/**
	 * 滚动计划状态同步接口： 推送接口名: 修改作业条目信息  所需数据
	 * @param rollingPlanId
	 * @return
	 */
		public EnpowerRequestPlanStatusSyn findEnpowerStatusSynByRollingPlanId (RollingPlan plan,Integer rollingPlanId){

			logger.info("滚动计划id="+rollingPlanId);
			EnpowerRequestPlanStatusSyn plansyn= new EnpowerRequestPlanStatusSyn();
			if(plan==null){
				plan = rollingPlanService.findById(rollingPlanId);
			}

			logger.info("滚动计划id="+rollingPlanId);

			//enpower：ID	作业条目id
			logger.info("作业条目id="+plan.getWorkListId());
			plansyn.setWorkListId(plan.getWorkListId());
			
//		    'ZY_DEPT':'管道一班一组（作业组名称）',
			User team = userService.findUserById(plan.getConsteam());
			String ZY_DEPT = team.getRealname();
			plansyn.setZY_DEPT(team.getDepartment().getName());
//		    'ZY_MAN':'张三（作业组负责人）',
			plansyn.setZY_MAN(team.getRealname());

			//enpower：STATUS	状态
			String state = getRollingState(plan);
			logger.info("状态(状态编码未确定）="+state);
			plansyn.setStatus(state);//TODO 1

			//enpower：START_DATE	实际开始时间
			logger.info("施工组长拿到的时间="+plan.getConsteamdate());
			if(plan.getConsteamdate()!=null){
				plansyn.setConsteamDate(DateUtility.sdfyMdHms.format(plan.getConsteamdate()));//TODO 2
			}else{
				logger.error("施工组长拿到的时间（实际开始时间），为空异常！测试阶段");
//				throw new RuntimeException("施工组长拿到的时间（实际开始时间），为空异常！稳定阶段");
			}

			//enpower：END_DATE	实际完成时间
			logger.info("完成时间(最后一个消点的时间)="+plan.getQcdate());
			if(plan.getQcdate()!=null){
				plansyn.setQcdate(DateUtility.sdfyMdHms.format(plan.getQcdate()));
			}else if(plan.getIsend()==2){//如果标识为已完成时，时间必须存在。
				logger.error("实际完成时间 ，完成时间(最后一个消点的时间),为空异常 测试阶段");
//				throw new RuntimeException("实际完成时间 ，完成时间(最后一个消点的时间),为空异常 稳定阶段");
			}

			//enpower：REAL_QTY	实际完成工程量
			logger.info("QC1见证回填“实际完成工程量”，两位小数的数字="+plan.getRealProjectcost());
			plansyn.setRealProjectcost(plan.getRealProjectcost());

			//enpower：FLAG	是否报量
			logger.info("是否报量FLAG="+plan.getIsReport());
			plansyn.setIsReport(plan.getIsReport());
			return plansyn;

		}
		

		/**
		 * 管道　插入焊口条目信息
		 * 
		 * @param rollingPlanId
		 * @return
		 */
		public EnpowerRequestPlanInsertWeld findEnpowerStatusInsertWeldRollingPlanId(RollingPlan plan,Integer rollingPlanId){
			EnpowerRequestPlanInsertWeld weld = new EnpowerRequestPlanInsertWeld();

			logger.info("滚动计划id="+rollingPlanId);
			EnpowerRequestPlanStatusSyn plansyn= new EnpowerRequestPlanStatusSyn();
			if(plan==null){
				plan = rollingPlanService.findById(rollingPlanId);
			}
			
			
//			WORKLIST_ID,--作业条目ID，我们推送给app里有
			weld.setWorkListId(plan.getWorkListId());
//			FLAG,--施工是否完成标志，默认N，完成则为Y
			weld.setFlag("N");
//			WORK_STATE,--施工状态，如未分配
			weld.setWORK_STATE("已分派");
//			START_DATE,--实际开始时间
//			END_DATE,--实际完成时间

			//enpower：START_DATE	实际开始时间
			logger.info("施工组长拿到的时间="+plan.getConsteamdate());
			if(plan.getConsteamdate()!=null){
				weld.setSTART_DATE(DateUtility.sdfyMdHms.format(plan.getConsteamdate()));//TODO 2
			}else{
				logger.error("实际开始时间");
//				throw new RuntimeException("施工组长拿到的时间（实际开始时间），为空异常！稳定阶段");
			}

			//enpower：END_DATE	实际完成时间
			logger.info("完成时间(最后一个消点的时间)="+plan.getQcdate());
			if(plan.getQcdate()!=null){
				weld.setEND_DATE(DateUtility.sdfyMdHms.format(plan.getQcdate()));
			}else if(plan.getIsend()==2){//如果标识为已完成时，时间必须存在。
				logger.error("实际完成时间");
//				throw new RuntimeException("实际完成时间 ，完成时间(最后一个消点的时间),为空异常 稳定阶段");
			}
			
			
//			WORK_TEAM_NAME,--作业组名称
//			WORK_TEAM_MAN,--作业组负责人
//			REAL_WORK_GCL,--实际完成工程量
//			WELD_CODE,--焊口号，如A01
//			PROJ_CODE,--固定代码：K2K3
//			COMP_CODE --固定代码：050812

			logger.info("滚动计划id="+rollingPlanId);
			
			return weld ;
		}
		

		/**
		 * 管道　更新焊口条目信息
		 * 
		 * @param rollingPlanId
		 * @return
		 */
		public EnpowerRequestPlanUpdateWeld findEnpowerStatusUpdateWeldRollingPlanId(RollingPlan plan,Integer rollingPlanId){
			EnpowerRequestPlanUpdateWeld weld = new EnpowerRequestPlanUpdateWeld();
			

			logger.info("滚动计划id="+rollingPlanId);
			EnpowerRequestPlanStatusSyn plansyn= new EnpowerRequestPlanStatusSyn();
			if(plan==null){
				plan = rollingPlanService.findById(rollingPlanId);
			}
			
			
//			WORKLIST_ID weldno
			weld.setWorkListIdAndWeldCode(plan.getWorkListId()+"||"+plan.getWeldno());
			String state = getRollingIsOver(plan);
//			FLAG,--施工是否完成标志，默认N，完成则为Y
			weld.setFlag(state);
//			WORK_STATE,--施工状态，如未分配
			String workstate = getRollingState(plan);
			weld.setWORK_STATE(workstate);
			
//			START_DATE,--实际开始时间
//			END_DATE,--实际完成时间

			//enpower：START_DATE	实际开始时间
			logger.info("施工组长拿到的时间="+plan.getConsteamdate());
			if(plan.getConsteamdate()!=null){
				weld.setSTART_DATE(DateUtility.sdfyMdHms.format(plan.getConsteamdate()));//TODO 2
			}else{
				logger.error("实际开始时间");
//				throw new RuntimeException("施工组长拿到的时间（实际开始时间），为空异常！稳定阶段");
			}

			//enpower：END_DATE	实际完成时间
			logger.info("完成时间(最后一个消点的时间)="+plan.getQcdate());
			if(plan.getQcdate()!=null){
				weld.setEND_DATE(DateUtility.sdfyMdHms.format(plan.getQcdate()));
			}else if(plan.getIsend()==2){//如果标识为已完成时，时间必须存在。
				logger.error("实际完成时间");
//				throw new RuntimeException("实际完成时间 ，完成时间(最后一个消点的时间),为空异常 稳定阶段");
			}
			
			
			return weld;
		}
		/**
		 * 推送接口名: 修改材料清单 所需数据
		 */
		public List<EnpowerRequestMaterialBackfill> findEnpowerMaterialInfoByRollingPlanId(Integer rollingPlanId){

			logger.info("滚动计划id="+rollingPlanId);
//			任务单信息列表
			logger.info("enpower修改材料清单id="+rollingPlanId);
			List<EnpowerMaterialInfo> material = materialInfoService.findByRollingPlanId(rollingPlanId);
			List<EnpowerRequestMaterialBackfill> materialList = new ArrayList<EnpowerRequestMaterialBackfill>();
			if(material!=null && !material.isEmpty()){
				material.stream().forEach(bean->{
					EnpowerRequestMaterialBackfill info = new EnpowerRequestMaterialBackfill();
					//enpower：材料ID	ID
					logger.info("enpower材料主键id="+bean.getEnpowerMaterialId());
					info.setEnpowerMaterialId(bean.getEnpowerMaterialId());

					//enpower：实际用量	REAL_USE_QTY
					logger.info("实际用量="+bean.getActualDosage());
					info.setActualDosage(bean.getActualDosage());

					//enpower：材料反填人	MATER_BACK_FILL_MAN
					logger.info("见证人姓名="+bean.getRecorderName());
					info.setRecorderName(bean.getRecorderName());

					//enpower：材料反填日期	MATER_BACK_FILL_DATE
					logger.info("见证表中真实见证时间="+bean.getRecorderTime());
					if(bean.getRecorderTime()!=null){
						info.setReal_witnessdata(DateUtility.sdfyMdHms.format(bean.getRecorderTime()));
					}else{
						//直接使用当前时间，就是材料反填时间了噻
						info.setReal_witnessdata(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));
//						throw new RuntimeException("见证表中真实见证时间 为空异常");
					}
					materialList.add(info);
				});
			}
			
			return materialList;
		}
		

/**
 * 消点接口 推送接口名: 		 所需数据
 * @param rollingPlanId
 * @return
 */
		
//	public List<EnpowerRequestDisappearPoint> findEnpowerDisappearPointByRollingPlanId (Integer rollingPlanId,Integer workStepId){
//
//		logger.info("滚动计划 工序id="+workStepId);
//		RollingPlan plan = rollingPlanService.findById(rollingPlanId);
//		
//		WorkStep step = workStepService.findById(workStepId);
////		作业组长名称
//		String consteamName =null;
//		if(plan.getConsteam()!=null){
//			User team = userService.findUserById(plan.getConsteam());
//			if(team!=null){
//				consteamName=team.getRealname();
//			}
//		}
//		String _consteamName = consteamName;
//		List<WorkStep> workstepList = plan.getWorkSteps();
//		List<EnpowerRequestDisappearPoint> disaList = new ArrayList<EnpowerRequestDisappearPoint>();
//		
//		EnpowerRequestDisappearPoint disa = new EnpowerRequestDisappearPoint();
////		//enpower:机组
////		disa.setUnitNo(plan.getUnitno());//机组号
////		//enpower:子项
////		disa.setSubItem(plan.getSubItem());//子项
////		//enpower:系统
////		disa.setSystemNo(plan.getSystemno());//系统号
////		//enpower:通知单编号
////		disa.setNOTE_CODE(getNoticeOrderNo(plan));//通知单编号（app生成
////		//enpower:质量计划号
////		disa.setQualityplanno(plan.getQualityplanno());//质量计划号
////		//enpower:施工队
////		disa.setConscaptainName(plan.getConscaptainName());//施工队
////			//enpower:wangz22	#VALUE!(plan.get#VALUE!());//
////		//enpower:工程量类别
////		disa.setProjectType(plan.getProjectType());//工程量类别
////			//enpower:wangz22	#VALUE!(plan.get#VALUE!());//
////		//enpower:QC1控制点类型
////		disa.setNoticeaqc1(step.getNoticeaqc1());//
////		//enpower:QC2控制点类型
////		disa.setNoticeaqc2(step.getNoticeaqc2());//
////		//enpower:CZEC QC控制点类型
////		disa.setNoticeczecqc(step.getNoticeczecqc());//
////		//enpower:CZEC QA控制点类型
////		disa.setNoticeczecqa(step.getNoticeczecqa());//
////		//enpower:PAEC QA控制点类型
////		disa.setNoticepaec(step.getNoticepaec());//
////		//enpower:是否合格
////		disa.setNoticeresult(step.getNoticeresult());//
////		//enpower:不合格原因
////		disa.setNoticeresultdesc(step.getNoticeresultdesc());//
////		//enpower:消点人
////		disa.setWitness("");//TODO 取当前登录人
////		//enpower:消点日期
//////		disa.setReal_witnessdata(step.getReal_witnessdata());//
////		//enpower:作业序号
////		disa.setStepIdentifier(step.getStepIdentifier());//工序号
////		//enpower:作业名称
////		disa.setStepname(step.getStepname());//作业名称（中文）
////		//enpower:见证地点
//////		disa.setAREA(plan.getAREA());//区域 
//
//		disaList.add(disa);
//		
//		return disaList ;
//	}
	

/**
 * 消点接口 推送接口名: 		 所需数据
 * @param rollingPlanId
 * @return
 */
	public List<EnpowerRequestDisappearPoint> findEnpowerDisappearPointByWitness(List<WorkStepWitness> wits,Integer[] witness){

//		RollingPlan plan = null;//rollingPlanService.findById(rollingPlanId);
		
		WorkStep step = null;//workStepService.findById(workStepId);
		if(wits==null){
			wits = witnessService.getByIds(witness);
		}
//		if(wits!=null && wits.size()>0){
			step=wits.get(0).getWorkStep();
			logger.info("滚动计划 见证wits.get(0)id="+wits.get(0).getId());
			logger.info("滚动计划 工序id="+step.getId());
			RollingPlan plan = step.getRollingPlan();
			logger.info("滚动计划id="+plan.getId());
//		}
//		作业组长名称
		String consteamName =null;
		if(plan.getConsteam()!=null){
			User team = userService.findUserById(plan.getConsteam());
			if(team!=null){
				consteamName=team.getRealname();
			}
		}
		String _consteamName = consteamName;
		List<WorkStep> workstepList = plan.getWorkSteps();
		List<EnpowerRequestDisappearPoint> disaList = new ArrayList<EnpowerRequestDisappearPoint>();
		
		wits.stream().forEach(workStepWitness->{
			
			String isOk="不合格";
			if(workStepWitness.getIsok()!=null){
				isOk = (3==workStepWitness.getIsok().intValue()?"合格":"不合格");
			}
			String witnessName = "";
			Integer witnessId = workStepWitness.getWitnesser();//czecqc czecqa paec之一
			if(witnessId==null){
				witnessId = workStepWitness.getWitness();//qc1 qc2之一
			}
			if(witnessId==null){
				logger.error("没有取到见证人：见证id="+workStepWitness.getId());
			}else{
				User witnessd = userService.findUserById(witnessId);
				witnessName = witnessd.getRealname();
			}
			EnpowerRequestDisappearPoint disa = new EnpowerRequestDisappearPoint();
			
//			disa.setNoticeOrderNo("d10ee13f536d45e9a59b1fa1b669eed5");// 这里的id 是需要在发点时获取到的。getNoticeOrderNo(plan));
//			disa.setNoticeOrderNo(getNoticeOrderNo(plan));
			disa.setNoticeOrderNo(String.valueOf(workStepWitness.getWorkStep().getId()));//根据沙道磊的截图意思，消点只需要appId即可
//			disa.setMainId(plan.getId()+"");
			
			if (workStepWitness.getNoticePoint().equals(NoticePointType.CZEC_QA.name())) {
//				CZEC_QA是否合格
				disa.setIsOkCzecqa(isOk);
//				CZEC_QA不合格原因
//				disa.setUnqualifiedCzecqaNote(workStepWitness.getNoticeresultdesc());
				disa.setUnqualifiedCzecqaNote(workStepWitness.getFailType());
//				消点人（CZEC_QA）
				disa.setCzecqaMan(witnessName);
//				CZEC_QA消点日期
				if(workStepWitness.getRealwitnessdata()!=null){
					disa.setCzecqaDate(DateUtility.sdfyMdHms.format(workStepWitness.getRealwitnessdata()));
				}
				//根据沙道磊的截图意思，消点只需要appId即可
//				disa.setAppId(String.valueOf(workStepWitness.getWorkStep().getId()));
			}
			if (workStepWitness.getNoticePoint().equals(NoticePointType.CZEC_QC.name())) {
//				CZEC_QC是否合格
				disa.setIsOkCzecqc(isOk);
//				CZEC_QC不合格原因
//				disa.setUnqualifiedCzecqaNote(workStepWitness.getNoticeresultdesc());
				disa.setUnqualifiedCzecqaNote(workStepWitness.getFailType());
//				消点人（CZEC_QC）
				disa.setCzecqcMan(witnessName);
//				CZEC_QC消点日期
				if(workStepWitness.getRealwitnessdata()!=null){
					disa.setCzecqcDate(DateUtility.sdfyMdHms.format(workStepWitness.getRealwitnessdata()));
				}
				//根据沙道磊的截图意思，消点只需要appId即可
//				disa.setAppId(String.valueOf(workStepWitness.getWorkStep().getId()));
			}
			if (workStepWitness.getNoticePoint().equals(NoticePointType.PAEC.name())) {
//				PAEC是否合格
				disa.setIsOkPaec(isOk);
//				PAEC不合格原因
//				disa.setUnqualifiedCzecqaNote(workStepWitness.getNoticeresultdesc());
				disa.setUnqualifiedCzecqaNote(workStepWitness.getFailType());
//				消点人（PAEC）
				disa.setPaecMan(witnessName);
//				PAEC消点日期
				if(workStepWitness.getRealwitnessdata()!=null){
					disa.setPaecDate(DateUtility.sdfyMdHms.format(workStepWitness.getRealwitnessdata()));
				}
				//根据沙道磊的截图意思，消点只需要appId即可
//				disa.setAppId(String.valueOf(workStepWitness.getWorkStep().getId()));
			}
			if (workStepWitness.getNoticePoint().equals(NoticePointType.QC1.name())) {
//				QC1是否合格
				disa.setIsOkQc1(isOk);
//				QC1不合格原因
//				disa.setUnqualifiedCzecqaNote(workStepWitness.getNoticeresultdesc());
				disa.setUnqualifiedCzecqaNote(workStepWitness.getFailType());
//				消点人（QC1）
				disa.setQc1Man(witnessName);
//				QC1消点日期
				if(workStepWitness.getRealwitnessdata()!=null){
					disa.setQc1Date(DateUtility.sdfyMdHms.format(workStepWitness.getRealwitnessdata()));
				}
				//根据沙道磊的截图意思，消点只需要appId即可
//				disa.setAppId(String.valueOf(workStepWitness.getWorkStep().getId()));
			}
			if (workStepWitness.getNoticePoint().equals(NoticePointType.QC2.name())) {
//				QC2是否合格
				disa.setIsOkQc2(isOk);
//				QC2不合格原因
//				disa.setUnqualifiedCzecqaNote(workStepWitness.getNoticeresultdesc());
				disa.setUnqualifiedCzecqaNote(workStepWitness.getFailType());
//				消点人（QC2）
				disa.setQc2Man(witnessName);
//				QC2消点日期
				if(workStepWitness.getRealwitnessdata()!=null){
					disa.setQc2Date(DateUtility.sdfyMdHms.format(workStepWitness.getRealwitnessdata()));
				}
				//根据沙道磊的截图意思，消点只需要appId即可
//				disa.setAppId(String.valueOf(workStepWitness.getWorkStep().getId()));
			}
			disaList.add(disa);
		});
		
		return disaList ;
	}
	
	/**
	 * 返回 通知单编号 
注：编号格式为F-X-CPN-X-XXXX。其中F—CNF，X—机组号，CPN——Control Points Notification，X—专业代码，XXXX—流水编号。
各专业代码：1.管道（P）2. 焊接W 3. 机械(M) 4.电气（E） 5.仪控（I）6.保温（H）7. 通风（V）8. 防腐(A) 
	 */
	public String getNoticeOrderNo(RollingPlan plan) {
		StringBuffer sb = new StringBuffer(1024);
		sb.append("F-");//F—CNF，
		sb.append(plan.getUnitno()).append("-");//，X—机组号
		sb.append("CPN-");//CPN——Control Points Notification，
		for(EnpowerSpecialtyCode x:EnpowerSpecialtyCode.values()){
			if(x.getMsg().equals(plan.getEnpowerType())){
				sb.append(x);//，X—专业代码
				sb.append("-");//，X—专业代码
			}
		}
		sb.append(plan.getId());
		logger.info("系统通知单编码="+sb.toString());
		return sb.toString();
	}
	/**
	 * 返回需要推送给Enpower的状态
	 * 状态：(未分配，已分配，完成，施工中，停滞)
	 * @param plan
	 * @return
	 */
	public String getRollingState(RollingPlan plan){
		String state = "";
		if(plan==null || plan.getIsdel()==1 || plan.getIsend()==null){
			state="无数据";
			logger.error("滚动计划"+plan.getId()+"状态异常"+state);
		}else{
			if(plan.getConsteam()==null){
				state="未分配";
			}else{
				state="已分配";
				if(RollingPlanFlag.PROBLEM.equals(plan.getRollingplanflag())){
					state="停滞";
				}else if (plan.getIsend()==2){
					state="完成";
				} else if (plan.getIsend() != 2 && (RollingPlanFlag.COMPLETED.equals(plan.getRollingplanflag())
						|| WitnessFlagEnum.LAUNCHED.toString().equals(plan.getWitnessflag()))) {
					state="施工中";
				}
			}
		}
		
		if(state.length()>1){
			logger.info("滚动计划"+plan.getId()+"状态是："+state);
		}else{
			logger.error("滚动计划"+plan.getId()+"状态异常：");
		}
		return state;
	}
/**
 * 施工是否完成标志，默认N，完成则为Y
 * @param plan
 * @return
 */
	public String getRollingIsOver(RollingPlan plan){
		String state = "";
		if(plan==null || plan.getIsdel()==1 || plan.getIsend()==null){
			state="N";//"无数据";
			logger.error("滚动计划"+plan.getId()+"状态异常"+state);
		}else{
			if(plan.getConsteam()==null){
				state="N";//"未分配";
			}else{
				state="N";//"已分配";
				if(RollingPlanFlag.PROBLEM.equals(plan.getRollingplanflag())){
					state="N";//"停滞";
				}else if (plan.getIsend()==2){
					state="Y";//"完成";
				} else if (plan.getIsend() != 2 && (RollingPlanFlag.COMPLETED.equals(plan.getRollingplanflag())
						|| WitnessFlagEnum.LAUNCHED.toString().equals(plan.getWitnessflag()))) {
					state="N";//"施工中";
				}
			}
		}
		
		if(state.length()>1){
			logger.info("滚动计划"+plan.getId()+"状态是："+state);
		}else{
			logger.error("滚动计划"+plan.getId()+"状态异常：");
		}
		return state;
	}

}
