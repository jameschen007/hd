package com.easycms.hd.api.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.DateUtility;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.JobService;

import lombok.extern.slf4j.Slf4j;

@Service("conferenceAlarmService")
@Slf4j
public class ConferenceAlarmServiceImpl implements JobService, Serializable {
	private static final long serialVersionUID = -5690901744850949508L;
	
	private int conferenceId;
	private int userId;
	
	@Autowired
	private ConferenceService conferenceService;
	@Autowired
	private JPushService jPushService;
	@Autowired
	private UserService userService;

	@SuppressWarnings("unchecked")
	@Override
	public void doJob(Object jobDataMap) {
		log.info("开始执行会议提醒任务.");
		Map<String, Object> jobMap = (Map<String, Object>) jobDataMap;

		this.userId = (Integer) jobMap.get("userId");
		this.conferenceId = (Integer) jobMap.get("conferenceId");
		Conference conference = conferenceService.findById(conferenceId);
		User user = userService.findUserById(userId);
		
		if (null != conference && null != user){
			if (conference.getType().equals("SEND")) {
				List<User> participants = conference.getParticipants();
				
				if (null != participants && !participants.isEmpty()) {
		            String message = user.getRealname() + " 邀请您于[" + DateUtility.getDateString(conference.getStartTime(), "yyyy-MM-dd HH:mm:ss") + "]参加会议[" + conference.getSubject() + "]";
		            JPushExtra extra = new JPushExtra();
		    		extra.setCategory(JPushCategoryEnums.CONFERENCE.name());
		    		jPushService.pushByUser(extra, message, "会议邀请", participants.stream().filter(p -> p.getId() != conference.getCreateBy()).collect(Collectors.toList()));
				}
			}
		}
	}
}
