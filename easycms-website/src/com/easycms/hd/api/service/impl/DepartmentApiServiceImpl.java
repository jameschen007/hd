package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.response.DepartmentResult;
import com.easycms.hd.api.response.DepartmentUserResult;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.service.DepartmentService;

@Service("departmentApiService")
public class DepartmentApiServiceImpl implements DepartmentApiService {

	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private VariableSetDao variableSetDao;
	
	/**
	 * 部门信息转换
	 * @param department
	 * @return
	 */
	@Override
	public DepartmentUserResult departmentTransferToDepartmentUserResult(Department department, boolean getuser,boolean showOriginRole) {//boolean showOriginRole
		if (null != department) {
			DepartmentUserResult departmentUserResult = new DepartmentUserResult();
			departmentUserResult.setId(department.getId());
			departmentUserResult.setName(department.getName());
			if (getuser) {
				if (null != department.getUsers() && !department.getUsers().isEmpty()) {
					departmentUserResult.setUsers(userApiService.userTransferToUserResult(department.getUsers(),showOriginRole));
				}
			}
			return departmentUserResult;
		}
		return null;
	}

	/**
	 * 部门信息批量转换
	 * @param departments
	 * @return
	 */
	@Override
	public List<DepartmentUserResult> departmentTransferToDepartmentUserResult(List<Department> departments, boolean getuser,boolean showOriginRole) {//boolean showOriginRole
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentUserResult> departmentUserResults = new ArrayList<DepartmentUserResult>();
			for (Department department : departments) {
				DepartmentUserResult departmentUserResult = new DepartmentUserResult();
				departmentUserResult = departmentTransferToDepartmentUserResult(department, getuser, showOriginRole);
				if (null != department.getChildren() && !department.getChildren().isEmpty()) {
					List<DepartmentUserResult> children = departmentTransferToDepartmentUserResult(department.getChildren(), getuser, showOriginRole);
					departmentUserResult.setChildren(children);
				}
				departmentUserResults.add(departmentUserResult);
			}
			return departmentUserResults;
		}
		return null;
	}

	@Override
	public List<DepartmentUserResult> getAllDepartment() {
		String variableType = "departmentId";
		String variableKey = "departmentRoot";//K2/K3核电项目部
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);
		
		List<Department> departments = new ArrayList<Department>();
		for (String val : set) {
			departments.add(departmentService.findById(Integer.valueOf(val)));
		}
		
		long start,end;
		System.out.println("start time:"+(start=System.currentTimeMillis()));
		
//		List<Department> departments = departmentService.findTop();
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentUserResult> departmentUserResults = departmentTransferToDepartmentUserResult(departments, true,true);
			System.out.println("end time:"+(end=System.currentTimeMillis()));
			System.out.println("delta time:"+(end-start)/1000);
			return departmentUserResults;
		}
		return null;
	}
	

	/***
	 * 获取下级部门的人员列表,不包含本部门下的人员
	 */
	@Override
	public List<DepartmentUserResult> memberOfChild(Integer departmentId) {
		List<Department> departments = new ArrayList<Department>();
		departments.add(departmentService.findById(departmentId));
		long start,end;
		System.out.println("start time:"+(start=System.currentTimeMillis()));
		
//		List<Department> departments = departmentService.findTop();
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentUserResult> departmentUserResults = departmentTransferToDepartmentUserResult(departments, true,false);
			System.out.println("end time:"+(end=System.currentTimeMillis()));
			System.out.println("delta time:"+(end-start)/1000);
			return departmentUserResults;
		}
		return null;
	}
	
	

	@Override
	public List<DepartmentUserResult> getDepartmentd(String departmentName) {
		Department department = departmentService.findByName(departmentName);
		if (null != department) {
			List<Department> departments = department.getChildren();
			if (null != departments && !departments.isEmpty()) {
				List<DepartmentUserResult> departmentUserResults = departmentTransferToDepartmentUserResult(departments, true,true);
				
				return departmentUserResults;
			}
		}
		return null;
	}

	/**
	 * 部门信息批量转换 不加载子部门和人员
	 * @param departments
	 * @return
	 */
	private List<DepartmentUserResult> departmentTransferToDepartmentUserResultNoUser(List<Department> departments) {
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentUserResult> departmentUserResults = new ArrayList<DepartmentUserResult>();
			for (Department department : departments) {
				DepartmentUserResult departmentUserResult = new DepartmentUserResult();
				departmentUserResult.setId(department.getId());
				departmentUserResult.setName(department.getName());
				departmentUserResults.add(departmentUserResult);
			}
			return departmentUserResults;
		}
		return null;
	}
	
	
	/**
	 * 根据部门id查部门列表，如果为空，查询根节点
	 * @param departmentId
	 * @return
	 */
	public List<DepartmentUserResult> findDeptList(Integer departmentId){
		List<Department> departments = null;
		if(departmentId!=null){
			departments = departmentService.findChildren(departmentId) ;
		}else{
			departments = departmentService.findTop();
		}
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentUserResult> departmentUserResults = departmentTransferToDepartmentUserResultNoUser(departments);
			return departmentUserResults;
		}
		
		return null;
	}

	@Override
	public DepartmentResult departmentTransferToDepartmentResult(Department department) {
		if (null != department) {
			DepartmentResult departmentResult = new DepartmentResult();
			departmentResult.setId(department.getId());
			departmentResult.setName(department.getName());
			return departmentResult;
		}
		return null;
	}

	@Override
	public List<DepartmentResult> departmentTransferToDepartmentResult(List<Department> departments) {
		if (null != departments && !departments.isEmpty()) {
			List<DepartmentResult> departmentResults = new ArrayList<DepartmentResult>();
			for (Department department : departments) {
				departmentResults.add(departmentTransferToDepartmentResult(department));
			}
			return departmentResults;
		}
		return null;
	}
}
