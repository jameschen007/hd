package com.easycms.hd.api.service;

import com.easycms.hd.api.enums.hseproblem.Hse3mEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemAssignRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemRequestForm;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult2;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemResult;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.management.user.domain.User;

public interface HseProblemApiService {
	
	/**
	 * 创建施工安全问题
	 * @param hseProblemRequestForm
	 * @param files
	 * @return
	 */
	public boolean createHseProblem(HseProblemRequestForm hseProblemRequestForm ,CommonsMultipartFile files[]) throws Exception;
	
	/**
	 * 获取施工安全问题信息
	 * @param page
	 * @param problemStatus
	 * @param solveStatus
	 * @return
	 */
	public HseProblemPageDataResult getHseProblemPageDataResult(Page<HseProblem> page, String problemStatus, String solveStatus,User user,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm)throws Exception;
	
	/**
	 * 
	 * 根据部门ID获取问题信息
	 * @param page
	 * @param problemStatus
	 * @param deptId
	 * @return
	 */
	public HseProblemPageDataResult getHseProblemResultByDept(Page<HseProblem> page, String problemStatus, Integer deptId);
	
	/**
	 * 根据用户获取问题信息
	 * @param page
	 * @param problemStatus
	 * @return
	 */
	public HseProblemPageDataResult getHseProblemResultByUser(Page<HseProblem> page, String problemStatus, User user,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm);

	/**
	 * 查询90天内安全文明问题分页列表
	 *
	 * @param page
	 * @param responsibleDept
	 * @param oneOf3Type
	 * @return
	 */
	public HseProblemPageDataResult getHseProblem3mResult(Page<HseProblem> page, Integer responsibleDept, Hse3mEnum oneOf3Type,HseProblemExListRequestForm hseProblemExListRequestForm, HseProblemStatusEnum problemStatus);
	
	/**
	 * HSE指派整改任务
	 * @return
	 */
	public boolean hseAssignTasks(HseProblemAssignRequestForm hseProblemAssignRequestForm,User logining);
	
	/**
	 * 责任部门提交整改结果
	 * @param problemId
	 * @param description
	 * @param files
	 * @return
	 */
	public boolean processRenovateResult(Integer problemId,String description,CommonsMultipartFile files[],Integer loginId);
	/**
	 * 编制人A提交 对本人所提问题的审核 结果
	 * @param problemId
	 * @return
	 */
	public boolean checkProducerResult(BaseRequestForm requestForm,Integer problemId,Integer checkResult) throws Exception;
	
	/**
	 * HSE审核整改结果
	 * @param problemId
	 * @return
	 * @throws Exception 
	 */
	public boolean checkRenovateResult(BaseRequestForm requestForm,Integer problemId,Integer checkResult) throws Exception;
	
	/**
	 * 获取安全施工部门及班组
	 * @param deptId
	 * @return
	 */
	public HseProblemCreatUiDataResult getHseProblemCreatUiResult(Integer deptId,String safe) throws Exception;
	public HseProblemCreatUiDataResult2 getHseProblemCreatUiResult_v2(Integer deptId,String safe) throws Exception;

	public HseProblemCreatUiDataResult2 getHseProblemCreatUiResultForReport(Integer deptId,String safe) throws Exception;
	/**
	 * 转换为接口返回数据结果
	 * @param hseProblem
	 * @param isShowDetails 
	 * @return
	 */
	public HseProblemResult transferForList(HseProblem hseProblem, String value);

}
