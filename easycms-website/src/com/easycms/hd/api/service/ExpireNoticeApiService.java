package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.response.EnpowerPageResult;
import com.easycms.hd.api.response.ExpireNoticeFilesResult;
import com.easycms.hd.api.response.ExpireNoticeResult;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.domain.ExpireNoticeFiles;

public interface ExpireNoticeApiService {
	EnpowerPageResult<EnpowerExpireNotice> getExpireNoticePageResult(EnpowerExpireNotice condition, Page<EnpowerExpireNotice> page);
	ExpireNoticeResult getExpireNoticeById(Integer id);
	ExpireNoticeResult tranferToResult(ExpireNotice expireNotices, boolean getFiles);
	List<ExpireNoticeResult> tranferToResult(List<ExpireNotice> expireNotices, boolean getFiles);
	
	ExpireNoticeFilesResult tranferToResult(ExpireNoticeFiles expireNoticeFiles);
	List<ExpireNoticeFilesResult> tranferToResult(List<ExpireNoticeFiles> expireNoticeFiles);
}
