package com.easycms.hd.api.baseservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.DepartmentUserResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/department")
@Api(value = "DepartmentAPIController", description = "获取所有部门人员列表相关的api")
public class DepartmentAPIController {

	@Autowired
	private DepartmentApiService departmentApiService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;
	
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取部门人员列表", notes = "得到所有部门的人员列表")
	public JsonResult<List<DepartmentUserResult>> welder(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<List<DepartmentUserResult>> result = new JsonResult<List<DepartmentUserResult>>();
		List<DepartmentUserResult> allDept = departmentApiService.getAllDepartment();

		Set<String> set = variableSetService.findSetValueBySetKeyAndType("frequenceUser", "conference");
		if(set!=null && set.size()>0){
			//加载常用联系人
			DepartmentUserResult freDept = new DepartmentUserResult();
			freDept.setId(-10000);
			freDept.setName("经常参会人组");
			List<UserResult> freUsers = new ArrayList<UserResult>();
			for(String u:set){
				User usr = userService.findByRealame(u);
				if(usr!=null){
					UserResult one = new UserResult();
					one.setId(usr.getId());
					one.setRealname(usr.getRealname());
					freUsers.add(one);
				}
			}
			freDept.setUsers(freUsers);
			allDept.get(0).getChildren().add(freDept);
		}
		

		result.setResponseResult(allDept);
		
//		result.setResponseResult(departmentApiService.getDepartment("K2/K3核电项目部"));
		result.setMessage("成功获取所有部门及人员信息");
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "deptList", method = RequestMethod.GET)
	@ApiOperation(value = "获取下级部门列表", notes = "获取下级部门列表")
	public JsonResult<List<DepartmentUserResult>> deptList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form,
			@ApiParam(required = false, name = "parentId", value = "父部门id，为空时查根部门") @RequestParam(value="parentId",required=false) Integer parentId) throws Exception {
		JsonResult<List<DepartmentUserResult>> result = new JsonResult<List<DepartmentUserResult>>();
		result.setResponseResult(departmentApiService.findDeptList(parentId));
		result.setMessage("成功获取部门列表");
		return result;
	}
	@ResponseBody
	@RequestMapping(value = "memberOfChild", method = RequestMethod.GET)
	@ApiOperation(value = "获取下级部门的人员列表,不包含本部门下的人员", notes = "获取下级部门的人员列表,不包含本部门下的人员")
	public JsonResult<List<DepartmentUserResult>> memberOfChild(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form,
			@ApiParam(required = false, name = "departmentId", value = "父部门id，为空时查根部门") @RequestParam(value="departmentId",required=false) Integer departmentId) throws Exception {
		JsonResult<List<DepartmentUserResult>> result = new JsonResult<List<DepartmentUserResult>>();
		result.setResponseResult(departmentApiService.memberOfChild(departmentId));
		result.setMessage("成功获取部门列表");
		return result;
	}
	@ResponseBody
	@RequestMapping(value = "listOfChild", method = RequestMethod.GET)
	@ApiOperation(value = "获取下级部门的人员列表,不包含本部门下的人员", notes = "获取下级部门的人员列表,不包含本部门下的人员")
	public JsonResult<List<DepartmentUserResult>> listOfChild(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form,
			@ApiParam(required = false, name = "departmentId", value = "父部门id，为空时查根部门") @RequestParam(value="departmentId",required=false) Integer departmentId) throws Exception {
		JsonResult<List<DepartmentUserResult>> result = new JsonResult<List<DepartmentUserResult>>();
		result.setResponseResult(departmentApiService.memberOfChild(departmentId));
		result.setMessage("成功获取部门列表");
		return result;
	}
	
}
