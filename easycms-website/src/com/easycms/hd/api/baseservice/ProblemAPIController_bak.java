package com.easycms.hd.api.baseservice;
//package com.easycms.hd.api.baseservice;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.easycms.core.util.Page;
//import com.easycms.hd.api.request.ProblemRequestForm;
//import com.easycms.hd.api.response.JsonResult;
//import com.easycms.hd.api.response.ProblemPageResult;
//import com.easycms.hd.api.service.ProblemApiService;
//import com.easycms.hd.problem.domain.Problem;
//import lombok.extern.slf4j.Slf4j;
//
//@Controller
//@RequestMapping("/hdxt/api/problem")
//@Slf4j
//public class ProblemAPIController {
////	@Autowired
////	private RuntimeService runtimeService;
//	@Autowired
//	private ProblemApiService problemApiService;
////	@Autowired
////	private ProblemFileService problemFileService;
////	@Autowired
////	private ProblemSolverService problemSolverService;
////	@Autowired
////	private ProblemConcernmanService problemConcernmanService;
////	@Autowired
////	private WorkStepService workStepService;
////	@Autowired
////	private RollingPlanService rollingPlanService;
////	@Autowired
////	private UserService userService;
////
////	@Autowired
////	private TaskService taskService;
////
////	@Autowired
////	private VariableSetService varialbleService;
////
////	@Autowired
////	private SchedulerService schedulerService;
////
////	@Autowired
////	private JPushService jPushService;
//
//	@ResponseBody
//	@RequestMapping(value = "", method = RequestMethod.GET)
//	public JsonResult<ProblemPageResult> getBystatus(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute("requestForm") ProblemRequestForm problemRequestForm) {
//		JsonResult<ProblemPageResult> result = new JsonResult<ProblemPageResult>();
//		if (null == problemRequestForm.getStatus()) {
//			result.setCode("-1001");
//			result.setMessage("查询问题类型必须填写");
//			return result;
//		}
//		
//		if (null != problemRequestForm.getPagenum() && null != problemRequestForm.getPagesize()) {
//			Page<Problem> page = new Page<Problem>();
//
//			page.setPageNum(problemRequestForm.getPagenum());
//			page.setPagesize(problemRequestForm.getPagesize());
//			
//			ProblemPageResult problemPageDataResult = problemApiService.getProblemPageDataResult(page, problemRequestForm.getLoginId(), problemRequestForm.getStatus());
//			result.setResponseResult(problemPageDataResult);
//			return result;
//		} else {
//			log.debug("分页信息有缺失");
//			result.setCode("-1002");
//			result.setMessage("分页信息有缺失.");
//			return result;
//		}
//	}
////
////	private Page<Problem> getConfirmedProblems(String userId, Page<Problem> page) {
////		List<Task> tasks;
////		tasks = taskService.createTaskQuery().taskAssignee(userId).list();
////		if (tasks != null && tasks.size() > 0) {
////			Integer[] ids = new Integer[tasks.size()];
////			for (int i = 0; i < tasks.size(); i++) {
////				Task task = tasks.get(i);
////				ProcessInstance pi = runtimeService
////						.createProcessInstanceQuery()
////						.processInstanceId(task.getProcessInstanceId())
////						.singleResult();
////				ids[i] = Integer.parseInt(pi.getBusinessKey());
////			}
////
////			return problemService.findConfirmProblemsByIds(ids, -1, page);
////		}
////
////		return page;
////	}
////	
////	private Page<Problem> getConfirmedProblems(String userId, String keyword, Page<Problem> page) {
////		List<Task> tasks;
////		tasks = taskService.createTaskQuery().taskAssignee(userId).list();
////		if (tasks != null && tasks.size() > 0) {
////			Integer[] ids = new Integer[tasks.size()];
////			for (int i = 0; i < tasks.size(); i++) {
////				Task task = tasks.get(i);
////				ProcessInstance pi = runtimeService
////						.createProcessInstanceQuery()
////						.processInstanceId(task.getProcessInstanceId())
////						.singleResult();
////				ids[i] = Integer.parseInt(pi.getBusinessKey());
////			}
////			
////			return problemService.findConfirmProblemsByIds(ids, -1, keyword, page);
////		}
////		
////		return page;
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/rollingplan", method = RequestMethod.GET)
////	public JsonResult<ProblemPageResult> getByRollingPlanId(
////			HttpServletRequest request, HttpServletResponse response) {
////		String rollingPlanId = request.getParameter("rollingPlanId");
////		String userid = request.getParameter("loginId");
////		JsonResult<ProblemPageResult> result = new JsonResult<ProblemPageResult>();
////		if (!CommonUtility.isNonEmpty(rollingPlanId)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, rollingPlanId is not available");
////			return result;
////		}
////
////		if (!CommonUtility.isNonEmpty(userid)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////
////		String pageN = request.getParameter("pagenum");
////		String pageS = request.getParameter("pagesize");
////
////		if (null == pageN || pageN == "" || null == pageS || pageS == "") {
////			result.setCode("-1001");
////			result.setMessage("不合法的分页请求");
////			return result;
////		}
////		int pagenum = Integer.parseInt(pageN);
////		int pagesize = Integer.parseInt(pageS);
////		Page<Problem> page = new Page<Problem>();
////		page.setPageNum(pagenum);
////		page.setPagesize(pagesize);
////
////		Page<Problem> problems = new Page<Problem>();
////		problems = problemService.findByRollingPlanId(
////				Integer.parseInt(rollingPlanId), page);
////		ProblemPageResult pResults = new ProblemPageResult();
////		copyPropertiesToProblemResults(pResults, problems);
////		result.setResponseResult(pResults);
////		return result;
////		// return ForwardUtility
////		// .forwardAdminView("hdService/api/problem/data_json_problem");
////
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/statistic", method = RequestMethod.GET)
////	public JsonResult<Map<String, Map<String, Integer>>> getProblemBystatus(
////			HttpServletRequest request, HttpServletResponse response) {
////		String userid = request.getParameter("loginId");
////		JsonResult<Map<String, Map<String, Integer>>> result = new JsonResult<Map<String, Map<String, Integer>>>();
////
////		if (!CommonUtility.isNonEmpty(userid)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////
////		Map<String, Map<String, Integer>> countResult = new HashMap<String, Map<String, Integer>>();
////
////		List<Task> tasks = null;
////		List<ProblemSolver> solvers = new ArrayList<ProblemSolver>();
////		Map<String, Integer> toSolved = new HashMap<String, Integer>();
////		int countNum = 0;
////		// 待解决问题
////		tasks = taskService.createTaskQuery().taskCandidateUser(userid).list();
////		if (tasks != null && tasks.size() > 0) {
////			Integer[] ids = new Integer[tasks.size()];
////			for (int i = 0; i < tasks.size(); i++) {
////				Task task = tasks.get(i);
////				ProcessInstance pi = runtimeService
////						.createProcessInstanceQuery()
////						.processInstanceId(task.getProcessInstanceId())
////						.singleResult();
////				ids[i] = Integer.parseInt(pi.getBusinessKey());
////			}
////
////			// countNum = problemService.findByNeedToSolveCount(ids);
////			List<Problem> problems = problemService.findByIdsAndStatus(ids,
////					ProblemData.IS_NOT_OK);
////			countNum = problems == null ? 0 : problems.size();
////		}
////
////		toSolved.put("key", Problem.TO_SOLVE);
////		toSolved.put("count", countNum);
////		countResult.put("needToSolve", toSolved);
////
////		// 未能解决
////		countNum = 0;
////		Map<String, Integer> notSolved = new HashMap<String, Integer>();
////		solvers = problemSolverService.findByUserId(userid);
////		if (solvers != null && solvers.size() > 0) {
////
////			Integer[] ids = new Integer[solvers.size()];
////			for (int i = 0; i < solvers.size(); i++) {
////				ids[i] = solvers.get(i).getProblemid();
////			}
////			List<Problem> problems = problemService.findByIdsAndStatus(ids,
////					ProblemData.IS_NOT_OK);
////			countNum = problems == null ? 0 : problems.size();
////		}
////
////		notSolved.put("key", Problem.UN_SOLVED);
////		notSolved.put("count", countNum);
////		countResult.put("notSolved", notSolved);
////
////		// 已经解决
////		Map<String, Integer> solved = new HashMap<String, Integer>();
////		countNum = 0;
////		solvers = problemSolverService.findByUserId(userid);
////		if (solvers != null && solvers.size() > 0) {
////
////			Integer[] ids = new Integer[solvers.size()];
////			for (int i = 0; i < solvers.size(); i++) {
////				ids[i] = solvers.get(i).getProblemid();
////			}
////			List<Problem> problems = problemService.findByIdsAndStatus(ids,
////					ProblemData.IS_OK);
////			countNum = problems == null ? 0 : problems.size();
////		}
////		solved.put("key", Problem.SOLVED);
////		solved.put("count", countNum);
////		countResult.put("solved", solved);
////
////		// 发起的问题
////		Map<String, Integer> created = new HashMap<String, Integer>();
////		countNum = 0;
////		countNum = problemService.countByUserId(userid);
////		created.put("key", Problem.CREATED);
////		created.put("count", countNum);
////		countResult.put("created", created);
////
////		// 待确认的问题
////		Map<String, Integer> toConfirm = new HashMap<String, Integer>();
////		countNum = 0;
////		tasks = taskService.createTaskQuery().taskAssignee(userid).list();
////		if (tasks != null && tasks.size() > 0) {
////			Integer[] ids = new Integer[tasks.size()];
////			for (int i = 0; i < tasks.size(); i++) {
////				Task task = tasks.get(i);
////				ProcessInstance pi = runtimeService
////						.createProcessInstanceQuery()
////						.processInstanceId(task.getProcessInstanceId())
////						.singleResult();
////				ids[i] = Integer.parseInt(pi.getBusinessKey());
////			}
////
////			List<Problem> problems = problemService.findConfirmByIdsAndStatus(
////					ids, -1);
////			countNum = problems == null ? 0 : problems.size();
////		}
////		toConfirm.put("key", Problem.TO_CONFIRM);
////		toConfirm.put("count", countNum);
////		countResult.put("needConfirm", toConfirm);
////
////		// 关注的问题
////		Map<String, Integer> concerned = new HashMap<String, Integer>();
////		countNum = 0;
////		List<ProblemConcernman> mans = problemConcernmanService
////				.findByUserId(Integer.parseInt(userid));
////		if (mans != null && mans.size() > 0) {
////			Integer[] ids = new Integer[mans.size()];
////			for (int i = 0; i < ids.length; i++) {
////				ids[i] = mans.get(i).getProblemid();
////			}
////			countNum = problemService.findCountConcerned(ids);
////		}
////		concerned.put("key", Problem.TO_CONCERN);
////		concerned.put("count", countNum);
////		countResult.put("concernedNotSolved", concerned);
////
////		result.setResponseResult(countResult);
////		return result;
////	}
////
////	private void copyPropertiesToProblemResults(ProblemPageResult pResults,
////			Page<Problem> pbms) {
////		pResults.setPagenum(pbms.getPageNum());
////		pResults.setTotalCounts(pbms.getTotalCounts());
////		pResults.setDefaultStartIndex(pbms.getDefaultStartIndex());
////		pResults.setCurrentPage(pbms.getCurrentPage());
////		pResults.setPageSize(pbms.getPagesize());
////		pResults.setStartIndex(pbms.getStartIndex());
////		pResults.setStartPage(pbms.getStartPage());
////		pResults.setPageCount(pbms.getPageCount());
////		pResults.setTotalpage(pbms.getEndPage());
////		List<Problem> problems = pbms.getDatas();
////		List<ProblemResult> problemResults = new ArrayList<ProblemResult>();
////		if (problems != null && problems.size() > 0) {
////			for (Problem problem : problems) {
////				problemResults.add(copyPropertiesToProblemResult(problem));
////			}
////		}
////		pResults.setDatas(problemResults);
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/add", method = RequestMethod.POST)
////	public JsonResult<ProblemResult> add(HttpServletRequest request,
////			HttpServletResponse response,
////			@ModelAttribute ProblemAPIForm problemForm) {
////		log.debug("request add problem  data ");
////		log.debug("add problem input data :["
////				+ JsonUtils.objectToJson(problemForm) + "]");
////		String userid = request.getParameter("loginId");
////		JsonResult<ProblemResult> result = new JsonResult<ProblemResult>();
////		if (null == userid || "" == userid) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, loginId is not available");
////			return result;
////		}
////		// 判断滚动计划非空
////		if (null == problemForm.getRollingPlanId()) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, rollingPlan_id is not available");
////			return result;
////		}
////
////		if (!CommonUtility.isNonEmpty(problemForm.getQuestionname())) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, question_name is not available");
////			return result;
////		}
////
////		if (!CommonUtility.isNonEmpty(problemForm.getDescribe())) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, describe is not available");
////			return result;
////		}
////
////		if (!CommonUtility.isNonEmpty(problemForm.getSolverid())) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, solver_id is not available");
////			return result;
////		}
////
////		User user = userService.findUserById(Integer.parseInt(userid));
////
////		if (user == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////
////		RollingPlan rp = rollingPlanService.findById(Integer
////				.parseInt(problemForm.getRollingPlanId()));
////		if (rp == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, work step id is not exists");
////			return result;
////		}
////
////		List<String> solverIds = new ArrayList<String>();
////		String solver = request.getParameter("solverid");
////
////		if (!veryfiUserid(solver)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, solver id is not available");
////			return result;
////		}
////
////		Map<String, Object> variables = new HashMap<String, Object>();
////
////		solverIds.add(solver);
////		variables.put("solver", solverIds);
////
////		Problem problem = new Problem();
////		problem.setWeldno(rp.getWeldno());
////		problem.setQuestionname(problemForm.getQuestionname());
////		problem.setDrawno(rp.getDrawno());
////		;
////		problem.setDescribe(problemForm.getDescribe());
////
////		User suser = userService.findUserById(Integer.parseInt(solver));
////
////		problem.setCurrentsolver(suser.getRealname());
////		problem.setCurrentsolverid(suser.getId() + "");
////		problem.setConfirm(0);
////		problem.setConcerman(problemForm.getConcernmen());
////		problem.setCreatedBy(user.getId() + "");
////		problem.setCreateOn(new Date());
////		problem.setRollingPlanId(rp.getId());
////
////		User leader = suser.getParent();
////
////		if (leader != null) {
////			problem.setConcermanname(leader.getRealname());
////		}
////
////		ProblemSolver problemSolver = new ProblemSolver();
////		problemSolver.setIsSolve(ProblemData.IS_NOT_OK);
////		problemSolver.setCreateOn(new Date());
////		problemSolver.setCreateBy(user.getName());
////		problemSolver.setProblemid(problem.getId());
////		problemSolver.setSolver(solver);
////		problemSolver.setSolverName(suser.getRealname());
////		ProblemSolver ps = problemSolverService.add(problemSolver);
////		problem.setSolverId(ps.getId());
////		problem = problemService.add(problem);
////
////		if (problem == null) {
////			result.setCode("-1002");
////			result.setMessage("create problem error!");
////			return result;
////		}
////
////		ps.setProblemid(problem.getId());
////		problemSolverService.add(ps);
////
////		if (!upload(problem.getId(), request)) {
////			result.setCode("-1002");
////			result.setMessage("upload files error!");
////			return result;
////		}
////		;
////		user = userService.findUserById(user.getId());
////
////		// 添加关注人
////		ProblemConcernman man = new ProblemConcernman();
////		man.setConcermanid(leader.getId());
////		man.setConcernmanname(leader.getRealname());
////		man.setProblemid(problem.getId());
////		problemConcernmanService.add(man);
////		pushDataToMobileConcernmen(leader.getId());
////
////		log.debug("add problem [" + problem.toString() + "]");
////
////		rp.setDoissuedate(new Date());
////		rp.setRollingplanflag(RollingPlanFlag.PROBLEM);
////		rollingPlanService.update(rp);
////
////		runtimeService.startProcessInstanceByKey(ProblemData.PROCESS_ID,
////				problem.getId() + "", variables);
////
////		log.debug("add problem output data :["
////				+ JsonUtils.objectToJson(problem) + "]");
////		request.getSession()
////				.setAttribute("currentWorkStepAutoIdUp", rp.getId());
////
////		if (leader != null) {
////			problemUpgrade(problem, leader);
////		}
////		pushDataToMobileSolver(solverIds);
////		result.setResponseResult(copyPropertiesToProblemResult(problem));
////		return result;
////	}
////
////	private void problemUpgrade(Problem problem, User leader) {
////		String tp = varialbleService.findValueByKey("changeSolver", "problem");
////		String changeSolverDuration = tp == null || tp == "" ? "1" : tp;
////		Calendar calendar = Calendar.getInstance();
////		// calendar.add(Calendar.DAY_OF_MONTH, changeSolverDuration);
////		calendar.add(Calendar.DAY_OF_MONTH,
////				Integer.parseInt(changeSolverDuration));
////		Map<String, Object> jobDataMap = new HashMap<String, Object>();
////		Map<String, Object> jobMap = new HashMap<String, Object>();
////		jobMap.put("problemid", problem.getId());
////		jobMap.put("leader", leader);
////		jobDataMap.put("jobService", "problemAutoChangeSolverService");
////		jobDataMap.put("jobDataMap", jobMap);
////		schedulerService.schedule(calendar.getTime(), jobDataMap);
////	}
////
////	private ProblemResult copyPropertiesToProblemResult(Problem problem) {
////		ProblemResult pResult = new ProblemResult();
////		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////		if (problem == null) {
////			return null;
////		}
////		pResult.setId(problem.getId() + "");
////		pResult.setWorstepid(problem.getWorstepid() + "");
////		// pResult.setStepname(problem.getStepname());
////		pResult.setWeldno(problem.getWeldno());
////		pResult.setDrawno(problem.getDrawno());
////		pResult.setDescribe(problem.getDescribe());
////		pResult.setQuestionname(problem.getQuestionname());
////		pResult.setIsOk(problem.getIsOk() + "");
////		pResult.setLevel(problem.getLevel() + "");
////		pResult.setSolvemethod(problem.getSolvemethod() == null ? "" : problem
////				.getSolvemethod());
////		pResult.setMethodmanid(problem.getMethodmanid() + "");
////		pResult.setSolvedate(problem.getSolvedate() == null ? "" : sdf
////				.format(problem.getSolvedate()));
////		pResult.setConcerman(problem.getConcerman() == null ? "" : problem
////				.getConcerman());
////		pResult.setCurrentsolver(problem.getCurrentsolver() == null ? ""
////				: problem.getCurrentsolver());
////		pResult.setCreatedBy(problem.getCreatedBy());
////		pResult.setRollingPlanId(problem.getRollingPlanId() + "");
////		List<ProblemFile> paths = problemFileService
////				.findFilesByProblemId(problem.getId());
////		List<ProblemFileResult> filesResult = new ArrayList<ProblemFileResult>();
////		for (ProblemFile problemFile : paths) {
////			ProblemFileResult pfResult = new ProblemFileResult();
////			pfResult.setFileName(problemFile.getFileName());
////			pfResult.setPath(problemFile.getPath());
////			filesResult.add(pfResult);
////		}
////		pResult.setFile(filesResult);
////
////		return pResult;
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/handle", method = RequestMethod.POST)
////	public JsonResult<ProblemResult> handle(HttpServletRequest request,
////			HttpServletResponse response) {
////		log.debug("request handle problem ");
////		String problemId = request.getParameter("id");
////		String isSolve = request.getParameter("isSolve");
////		String method = request.getParameter("solvemethod");
////		String solverid = request.getParameter("solverid");
////		String userid = request.getParameter("loginId");
////
////		JsonResult<ProblemResult> result = new JsonResult<ProblemResult>();
////		if (!CommonUtility.isNonEmpty(userid)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(problemId)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, problem_id is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(isSolve)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, is_solve is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(method)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, solvemethod is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(solverid)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, solverid is not available");
////			return result;
////		}
////		Problem problem = problemService.findById(Integer.parseInt(problemId));
////		User user = userService.findUserById(Integer.parseInt(userid));
////
////		if (user == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, login id is not available");
////			return result;
////		}
////
////		if (problem == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, problem id is not exist");
////			return result;
////
////		}
////		Problem p = null;
////		Map<String, Object> variables = new HashMap<String, Object>();
////		List<Task> tasks = null;
////
////		List<String> solverIds = new ArrayList<String>();
////		List<String> confirmIds = new ArrayList<String>();
////
////		tasks = taskService.createTaskQuery()
////				.taskCandidateUser(user.getId() + "").list();
////
////		if (tasks != null) {
////			for (Task task : tasks) {
////				ProcessInstance pi = runtimeService
////						.createProcessInstanceQuery()
////						.processInstanceId(task.getProcessInstanceId())
////						.singleResult();
////				if (problemId.equalsIgnoreCase(pi.getBusinessKey())) {
////					// 如果能解决
////					if ("1".equalsIgnoreCase(isSolve)) {
////						variables.put("solved", true);
////						variables.put("worker", problem.getCreatedBy());
////						taskService.complete(task.getId(), variables);
////						p = problemService
////								.findById(Integer.parseInt(problemId));
////						List<Problem> ps = problemService.findByWorkstepId(p
////								.getWorstepid());
////						log.debug("current problem size :" + ps.size());
////						p.setIsOk(ProblemData.IS_OK);
////						p.setUpdateBy(user.getId() + "");
////						p.setUpdateOn(new Date());
////						p.setSolvemethod(method);
////						p.setMethodmanid(user.getId());
////						p.setConfirm(-1);
////						p.setSolvedate(new Date());
////						problemService.update(p);
////						confirmIds.add(p.getCreatedBy());
////						break;
////						// 如果不能解决
////					} else if ("0".equalsIgnoreCase(isSolve)) {
////						p = problemService
////								.findById(Integer.parseInt(problemId));
////						log.debug("current user " + user.getName());
////						variables.put("solved", false);
////						List<String> ids = new ArrayList<String>();
////						ids.add(solverid);
////						variables.put("solver", ids);
////						p.setLevel(p.getLevel() + 1);
////						p.setUpdateOn(new Date());
////
////						User suser = userService.findUserById(Integer
////								.parseInt(solverid));
////						p.setCurrentsolver(suser.getRealname());
////						problemService.update(p);
////
////						taskService.complete(task.getId(), variables);
////						ProblemSolver problemSolver = new ProblemSolver();
////						problemSolver.setIsSolve(ProblemData.IS_NOT_OK);
////						problemSolver.setCreateOn(new Date());
////						problemSolver.setCreateBy(user.getName());
////						problemSolver.setProblemid(problem.getId());
////						problemSolver.setSolver(solverid);
////						problemSolver.setSolverName(suser.getRealname());
////
////						ProblemSolver olPS = problemSolverService
////								.findById(problem.getSolverId());
////						ProblemSolver ps = problemSolverService
////								.add(problemSolver);
////						problem.setSolverId(ps.getId());
////
////						olPS.setDescribe(method);
////						problemSolverService.add(olPS);
////
////						problemService.update(problem);
////						solverIds.add(solverid);
////
////						problemUpgrade(p, suser.getParent());
////						addConcernmans(problem, user.getParent());
////						break;
////					}
////				}
////			}
////		}
////		if (p == null) {
////			result.setCode("-1002");
////			result.setMessage("there's no problem :" + problemId
////					+ " for loginId :" + userid);
////			return result;
////		}
////
////		pushDataToMobileSolver(solverIds);
////		pushDataToMobileConfirmer(confirmIds);
////
////		result.setResponseResult(copyPropertiesToProblemResult(problemService
////				.findById(p.getId())));
////		return result;
////	}
////
////	private void addConcernmans(Problem problem, User leader) {
////		if (leader == null) {
////			return;
////		}
////		String concernmen = problem.getConcermanname();
////		ProblemConcernman concernman = new ProblemConcernman();
////		concernman.setConcermanid(leader.getId());
////		concernman.setConcernmanname(leader.getRealname());
////		concernman.setProblemid(problem.getId());
////		ProblemConcernman pc = problemConcernmanService
////				.findByUserIdAndProblemId(leader.getId(), problem.getId());
////		if (pc == null) {
////			problemConcernmanService.add(concernman);
////			if (concernman == null || "".equals(concernmen)) {
////				concernmen = leader.getRealname();
////			} else {
////				concernmen += "|" + leader.getRealname();
////			}
////		}
////
////		if (concernmen == null || "".equals(concernmen)) {
////			return;
////		}
////
////		problem.setConcermanname(concernmen);
////		problemService.update(problem);
////
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
////	public JsonResult<ProblemResult> confirm(HttpServletRequest request,
////			HttpServletResponse response) {
////		log.debug("request problem solveconfirm ");
////		String problemId = request.getParameter("id");
////		String userid = request.getParameter("loginId");
////		String isWork = request.getParameter("isWork");
////
////		JsonResult<ProblemResult> result = new JsonResult<ProblemResult>();
////		if (!CommonUtility.isNonEmpty(userid)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(problemId)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, problem_id is not available");
////			return result;
////		}
////		if (!CommonUtility.isNonEmpty(isWork)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, isWork is not available");
////			return result;
////		}
////
////		User user = userService.findUserById(Integer.parseInt(userid));
////
////		if (user == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, userid is not available");
////			return result;
////		}
////		Problem p = problemService.findById(Integer.parseInt(problemId));
////
////		if (p == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, problem id is not exists");
////			return result;
////		}
////		Map<String, Object> variables = new HashMap<String, Object>();
////		List<Task> tasks = taskService.createTaskQuery()
////				.taskAssignee(user.getId() + "").list();
////		for (Task task : tasks) {
////			ProcessInstance pi = runtimeService.createProcessInstanceQuery()
////					.processInstanceId(task.getProcessInstanceId())
////					.singleResult();
////			if (pi.getBusinessKey().equals(problemId)) {
////				if ("0".equals(isWork)) {
////					variables.put("works", false);
////					p.setIsOk(ProblemData.IS_NOT_OK);
////					p.setConfirm(0);
////					p.setUpdateOn(new Date());
////					taskService.complete(task.getId(), variables);
////					break;
////				} else if ("1".equals(isWork)) {
////					variables.put("works", true);
////					p.setConfirm(1);
////					unlockRollingPlan(p);
////					taskService.complete(task.getId(), variables);
////					break;
////				}
////			}
////		}
////		p = problemService.update(p);
////		result.setResponseResult(copyPropertiesToProblemResult(p));
////		return result;
////		// return ForwardUtility
////		// .forwardAdminView("/hdService/api/problem/data_json_problem_detail");
////
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/detail", method = RequestMethod.GET)
////	public JsonResult<ProblemResult> detail(HttpServletRequest request,
////			HttpServletResponse response) {
////		log.debug("request problem  detail data ");
////		String id = request.getParameter("id");
////		JsonResult<ProblemResult> result = new JsonResult<ProblemResult>();
////		if (!CommonUtility.isNonEmpty(id)) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, id is not available");
////			return result;
////		}
////		// request.setAttribute("id", id);
////		Problem problem = problemService.findById(Integer.parseInt(id));
////
////		if (problem == null) {
////			result.setCode("-1001");
////			result.setMessage("Error/s occurred, problem id is not exists");
////			return result;
////		}
////
////		List<ProblemSolver> solvers = problemSolverService
////				.findByProblemId(problem.getId());
////		List<SolverResult> sr = new ArrayList<SolverResult>();
////		if (solvers != null && solvers.size() >= 0) {
////			for (int i = 0; i < solvers.size() - 1; i++) {
////				ProblemSolver ps = solvers.get(i);
////				sr.add(copySolverToResult(ps));
////			}
////		}
////
////		User user = userService.findUserById(Integer.parseInt(problem
////				.getCreatedBy()));
////
////		ProblemResult preProblemResult = copyPropertiesToProblemResult(problem);
////		preProblemResult.setCreator(user.getRealname());
////		preProblemResult.setSolvers(sr);
////		result.setResponseResult(preProblemResult);
////		return result;
////	}
////
////	private SolverResult copySolverToResult(ProblemSolver ps) {
////		if (ps == null) {
////			return null;
////		}
////		SolverResult sr = new SolverResult();
////		sr.setId(ps.getId() + "");
////		sr.setName(ps.getSolverName());
////		sr.setDesc(ps.getDescribe());
////		return sr;
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/delete", method = RequestMethod.GET)
////	public JsonResult<Boolean> deleteProblem(HttpServletRequest request,
////			HttpServletResponse response) {
////		log.debug("request problem delete ");
////		JsonResult<Boolean> result = new JsonResult<Boolean>();
////		String problemId = request.getParameter("problemId");
////
////		if (problemId == null || problemId == "") {
////			result.setCode("-1001");
////			result.setMessage("problemId is required");
////			return result;
////		}
////		Problem p = problemService.findById(Integer.parseInt(problemId));
////		if (p == null) {
////			result.setCode("-1002");
////			result.setMessage("problemId is not available");
////			return result;
////		}
////
////		WorkStep ws = workStepService.findById(p.getWorstepid());
////		Map<String, Object> variables = new HashMap<String, Object>();
////
////		List<Task> tasks = taskService.createTaskQuery().list();
////
////		for (Task task : tasks) {
////			ProcessInstance pi = runtimeService.createProcessInstanceQuery()
////					.processInstanceId(task.getProcessInstanceId())
////					.singleResult();
////			log.debug("business key:" + pi.getBusinessKey());
////			log.debug("problem Id: " + problemId);
////			if (pi.getBusinessKey().equals(problemId)) {
////				variables.put("solved", "delete");
////				taskService.complete(task.getId(), variables);
////			}
////		}
////		problemService.delete(p);
////		List<Problem> problems = problemService.findByWorkstepId(ws.getId());
////		if (problems == null || problems.size() <= 0) {
////			ws.setStepflag(StepFlag.PREPARE);
////			workStepService.add(ws);
////		}
////		return result;
////	}
////
////	private boolean upload(Integer problemId, HttpServletRequest request) {
////		String filePath = "problem_files";
////		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true,
////				request);
////		if (fileInfos != null && fileInfos.size() > 0) {
////			for (FileInfo fileInfo : fileInfos) {
////				ProblemFile file = new ProblemFile();
////				file.setPath(fileInfo.getNewFilename());
////				file.setTime(new Date());
////				file.setProblemid(problemId);
////				file.setFileName(fileInfo.getFilename());
////				log.debug("upload file success : " + file.toString());
////				problemFileService.add(file);
////			}
////		}
////
////		return true;
////		// WebUtility.writeToClient(response, "true");
////	}
////
////	@ResponseBody
////	@RequestMapping(value = "/upload", method = RequestMethod.POST)
////	public JsonResult<Boolean> upload(HttpServletRequest request,
////			HttpServletResponse response) {
////		String filePath = "problem_files";
////		JsonResult<Boolean> result = new JsonResult<Boolean>();
////		// String id = request.getParameter("id");
////		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true,
////				request);
////		if (fileInfos != null && fileInfos.size() > 0) {
////			for (FileInfo fileInfo : fileInfos) {
////				ProblemFile file = new ProblemFile();
////				file.setPath(fileInfo.getNewFilename());
////				file.setTime(new Date());
////				// file.setProblemid(problemId);
////				file.setFileName(fileInfo.getFilename());
////				log.debug("upload file success : " + file.toString());
////				// problemFileService.add(file);
////			}
////		}
////		// return true;
////		return result;
////		// WebUtility.writeToClient(response, "true");
////	}
////
////	/**
////	 * 推送信息解决人手机
////	 * 
////	 * @param solverIds
////	 */
////	private void pushDataToMobileSolver(List<String> solverIds) {
////		JPushExtra extra = new JPushExtra();
////		extra.setCategory("solve");
////		extra.setData("");
////		extra.setRemark("");
////		for (String string : solverIds) {
////			User user = userService.findUserById(Integer.parseInt(string));
////			log.info("start to push to solver : " + user.getRealname());
////			jPushService.pushByUserId(extra, "你有一条问题需要处理", "", user.getId());
////		}
////	}
////
////	/**
////	 * 将信息推送至关注人手机
////	 * 
////	 * @param concernIds
////	 */
////	private void pushDataToMobileConcernmen(Integer concernId) {
////		JPushExtra extra = new JPushExtra();
////		extra.setCategory("concern");
////		extra.setData("");
////		extra.setRemark("");
////		User user = userService.findUserById(concernId);
////		log.info("start to push to concern man : " + user.getRealname());
////		jPushService.pushByUserId(extra, "你有一条问题需要关注", "", user.getId());
////	}
////
////	/**
////	 * 将信息推送至确认人手机
////	 * 
////	 * @param confirmerids
////	 */
////	private void pushDataToMobileConfirmer(List<String> confirmerids) {
////		JPushExtra extra = new JPushExtra();
////		extra.setCategory("concern");
////		extra.setData("");
////		extra.setRemark("");
////		for (String string : confirmerids) {
////			User user = userService.findUserById(Integer.parseInt(string));
////			log.info("start to push to confirmer : " + user.getRealname());
////			jPushService.pushByUserId(extra, "你有一条问题需要确认", "", user.getId());
////		}
////	}
////
////	private boolean veryfiUserid(String id) {
////		return userService
////				.findUserById(Integer.parseInt(id == null ? "0" : id)) != null;
////	}
////
////	private void unlockRollingPlan(Problem p) {
////		List<Problem> ps = problemService.findByRollingPlanIdAndIsNotOk(p
////				.getRollingPlanId());
////		if (ps == null || ps.size() == 0) {
////			RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
////			rp.setDoissuedate(null);
////			rp.setRollingplanflag(null);
////			rollingPlanService.update(rp);
////		}
////	}
//}
