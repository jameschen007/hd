package com.easycms.hd.api.baseservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.core.util.Page;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.GetChangeResResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.impl.ExecuteSearchPushToEnpowerServiceImpl;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
@Controller
@RequestMapping("/hdxt/api/getChange")
@Api(value = "GetChangeAPIController", description = "变更查询的api")
public class GetChangeAPIController {
	
	@Autowired
	private ExecuteSearchPushToEnpowerServiceImpl executeSearchPushToEnpowerServiceImpl ;


    /**
     * 物项库存查询接口
     * String fileCode,String cnTitle,String remark,
							文件编码 中文标题 备注
     */
    @ResponseBody
    @ApiOperation(value = "文档变更查询接口", notes = "文档变更查询接口")
    @RequestMapping(value = "/getChangeList", method = RequestMethod.GET)
    public JsonResult<Object> materialQueryList(
			@ApiParam(required = false, name = "fileCode", value = "文件编码") @RequestParam(value="fileCode",required=false) String fileCode,
			@ApiParam(required = false, name = "cnTitle", value = "中文标题") @RequestParam(value="cnTitle",required=false) String cnTitle,
			@ApiParam(required = false, name = "remark", value = "备注") @RequestParam(value="remark",required=false) String remark,
			@ModelAttribute("form") BasePagenationRequestForm form
			) {

    	int pagenum = form.getPagenum();
    	int pagesize = form.getPagesize() ;
        JsonResult<Object> result = new JsonResult<Object>();
        List<GetChangeResResult> list = null;
        try{
        	list = executeSearchPushToEnpowerServiceImpl.getChangeResult(fileCode,cnTitle,remark,pagenum,pagesize);

			Page<GetChangeResResult> page = new Page<GetChangeResResult>();
        	if(list==null || list.size()==0){
        		page.setDatas(new ArrayList<GetChangeResResult>());
				result.setResponseResult(page);

        	} else {
				int totalCounts = 100000;// new BigDecimal(list.get(0).getTotal_count()).intValue();
				page.setPagesize(pagesize);
				page.execute(totalCounts, pagenum, list);
			}
			result.setResponseResult(page);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            return result;
        }
        return result;
    }
}
