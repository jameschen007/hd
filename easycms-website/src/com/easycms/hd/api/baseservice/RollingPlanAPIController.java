package com.easycms.hd.api.baseservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.RollingPlanRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.RollingMaterialResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanDataResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanPageDataResult;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/rollingplan")
@Api(value = "RollingPlanAPIController", description = "六级滚动计划相关的api")
@Slf4j
@javax.transaction.Transactional
public class RollingPlanAPIController {
	@Autowired
	private UserService userService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private RollingPlanApiService rollingPlanApiService;
	@Autowired
	private RollingPlanTansferService rollingPlanTansferService;
	
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "六级滚动计划分页信息", notes = "用于返回六级滚动计划中的各类不同情况")
	public JsonResult<RollingPlanPageDataResult> rollingPlanList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") RollingPlanRequestForm rollingPlanRequestForm) throws Exception {
		log.info("Get rollingplan with pagenation");
		JsonResult<RollingPlanPageDataResult> result = new JsonResult<RollingPlanPageDataResult>();
		
		User user = null;
		if (null != rollingPlanRequestForm.getUserId()) {
			user = userService.findUserById(rollingPlanRequestForm.getUserId());
		} else {
			user = userService.findUserById(rollingPlanRequestForm.getLoginId());
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		
		if (null != rollingPlanRequestForm.getPagenum() && null != rollingPlanRequestForm.getPagesize()) {
			Page<RollingPlan> page = new Page<RollingPlan>();

			page.setPageNum(rollingPlanRequestForm.getPagenum());
			page.setPagesize(rollingPlanRequestForm.getPagesize());
			
			RollingPlanPageDataResult rollingPlanPageDataResult = rollingPlanApiService.getRollingPlanPageDataResult(page, user, rollingPlanRequestForm.getStatus(), rollingPlanRequestForm.getType(), rollingPlanRequestForm.getKeyword(),rollingPlanRequestForm.getRoleId());
			result.setResponseResult(rollingPlanPageDataResult);
			result.setMessage("成功获取分页信息");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 根据ID获取滚动计划信息
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "六级滚动计划详情", notes = "根据ID得到一个六级滚动计划的详细信息")
	public JsonResult<RollingPlanDataResult> rollingPlanById(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable @ApiParam(value = "滚动计划ID", required = true) Integer id,
			@RequestParam @ApiParam(value = "登录用户的ID", required = true) Integer loginId) throws Exception {
		JsonResult<RollingPlanDataResult> result = new JsonResult<RollingPlanDataResult>();
		RollingPlan rollingPlan = rollingPlanTansferService.markNoticePoint(rollingPlanTansferService.transferRollingPlan(rollingPlanService.findById(id)));
		result.setResponseResult(rollingPlanApiService.rollingPlanTransferToSixLevelRollingPlan(rollingPlan));
		result.setMessage("成功获取指定ID的滚动计划");
		return result;
	}
}
