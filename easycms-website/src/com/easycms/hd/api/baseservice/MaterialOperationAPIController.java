package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckForm;
import com.easycms.hd.api.request.MaterialExitRequestForm;
import com.easycms.hd.api.request.MaterialOutRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.material.domain.Material;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/material_op")
@Api(value = "MaterialOperationAPIController", description = "物资管理操作相关的api")
public class MaterialOperationAPIController {

	@Autowired
	private ComputedConstantVar constantVar ;
	@Autowired
	private UserService userService ;
	
	
	@Value("#{APP_SETTING['enpower_api_materialData']}")
	private String enpowerApiInfo;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}

	/**
	 * 入库
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/enter", method = RequestMethod.POST)
	@ApiOperation(value = "入库核实接口", notes = "物料入库")
	public JsonResult<Boolean> enter(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") EnpowerRequestMaterialStoreCheckForm form,
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{
	        HttpSession session = request.getSession();
	        User user = (User) session.getAttribute("user");
	        if(user==null){
	        	user = userService.findUserById(loginId);
	        }
	        if(user!=null){
	    		if (CommonUtility.isNonEmpty(form.getGUARANTEENO())) {
	    			executePushToEnpowerServiceImpl.materialStoreCheck(form,user);
	    		}
	        }
		}catch(Exception e){
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		return result;
	}
	
	/**
	 * 出库
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/out", method = RequestMethod.POST)
	@ApiOperation(value = "出库", notes = "物料出库")
	public JsonResult<Boolean> out(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") MaterialOutRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
//		EnpowerRequest enpowerRequest = new EnpowerRequest();
//		enpowerRequest.setCondition("Material='XXX'");
//		enpowerRequest.setXmlname("物资信息查询");
//		enpowerRequest.setIsPage(1);
//		enpowerRequest.setPageIndex(1);
//		enpowerRequest.setPageSize(20000);
//		
//		String parms = enpowerRequest.toString();
//		
//		String materialRequestResult = HttpRequest.sendPost(enpowerHost + enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
//		if (null != materialRequestResult && !"".equals(materialRequestResult)){
//			//TODO 获取物资
//		}
		
		Material material = new Material();
		material.setName("名称1");
		material.setNumber(128);
		material.setMaterial("材质XXX");
		material.setSpecificationNo("KK-2-CSP15-047-ZNAP-04-QR");
		material.setFurnaceNo("KK-2-CSP15-047-ZNAP-04-QR-XX");
		material.setSecurityLevel("1");
		material.setWarrantyLevel("2");
		material.setShipNo("2879725975");
		
		material.setLocation(form.getLocation());
		material.setWarehouse(form.getWarehouse());
		
		if (CommonUtility.isNonEmpty(form.getWarrantyNo())) {
			material.setWarrantyNo(form.getWarrantyNo());
			//TODO 调用Enpower接口出库操作
		}
		for (String no : form.getOutNo()) {
			//TODO 批量出库操作
		}
		
		return result;
	}
	
	/**
	 * 退库
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/exit", method = RequestMethod.POST)
	@ApiOperation(value = "退库", notes = "物料退库")
	public JsonResult<Boolean> exit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") MaterialExitRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
//		EnpowerRequest enpowerRequest = new EnpowerRequest();
//		enpowerRequest.setCondition("Material='XXX'");
//		enpowerRequest.setXmlname("物资信息查询");
//		enpowerRequest.setIsPage(1);
//		enpowerRequest.setPageIndex(1);
//		enpowerRequest.setPageSize(20000);
//		
//		String parms = enpowerRequest.toString();
//		
//		String materialRequestResult = HttpRequest.sendPost(enpowerHost + enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
//		if (null != materialRequestResult && !"".equals(materialRequestResult)){
//			//TODO 获取物资
//		}
		
		Material material = new Material();
		material.setName("名称1");
		material.setNumber(128);
		material.setMaterial("材质XXX");
		material.setSpecificationNo("KK-2-CSP15-047-ZNAP-04-QR");
		material.setFurnaceNo("KK-2-CSP15-047-ZNAP-04-QR-XX");
		material.setSecurityLevel("1");
		material.setWarrantyLevel("2");
		material.setShipNo("2879725975");
		
		material.setLocation(form.getLocation());
		material.setWarehouse(form.getWarehouse());
		
		for (String no : form.getExitNo()) {
			//TODO 调用Enpower接口退库操作
		}
		
		return result;
	}
	
}
