package com.easycms.hd.api.baseservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.base.SimpleBasePagenationRequestForm;
import com.easycms.hd.api.response.EnpowerPageResult;
import com.easycms.hd.api.response.ExpireNoticeResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.ExpireNoticeApiService;
import com.easycms.hd.conference.service.impl.EnpowerExpireNoticeServiceImpl;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/expire_notice")
@Api(value = "ExpireNoticeAPIController", description = "失效文件通知相关的api")
public class ExpireNoticeAPIController {
	
	@Autowired
	private ExpireNoticeApiService expireNoticeApiService;
	@Autowired
	private EnpowerExpireNoticeServiceImpl enpowerExpireNoticeImpl;
	
	/**
	 * 获取失效文件
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取失效文件", notes = "获取所有的失效文件。")
	public JsonResult<EnpowerPageResult<EnpowerExpireNotice>> pagenationExpireNotice(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") SimpleBasePagenationRequestForm form) throws Exception {
		JsonResult<EnpowerPageResult<EnpowerExpireNotice>> result = new JsonResult<EnpowerPageResult<EnpowerExpireNotice>>();
		
		if (null != form.getPagenum() && null != form.getPagesize()) {
			Page<EnpowerExpireNotice> page = new Page<EnpowerExpireNotice>();

			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
			
			EnpowerExpireNotice condition = new EnpowerExpireNotice();
			EnpowerPageResult<EnpowerExpireNotice> expireNoticePageResult = expireNoticeApiService.getExpireNoticePageResult(condition, page);
			result.setResponseResult(expireNoticePageResult);
			result.setMessage("成功获取分页信息");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 获取失效文件 -- 单个
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取失效文件 -- 单个", notes = "获取 单个的失效文件 -- 带文件列表。")
	public JsonResult<ExpireNoticeResult> singleExpireNotice(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form, 
			@PathVariable("id") Integer id) throws Exception {
		JsonResult<ExpireNoticeResult> result = new JsonResult<ExpireNoticeResult>();
			
		ExpireNoticeResult expireNoticeResult = expireNoticeApiService.getExpireNoticeById(id);
		result.setResponseResult(expireNoticeResult);
		return result;
	}
}
