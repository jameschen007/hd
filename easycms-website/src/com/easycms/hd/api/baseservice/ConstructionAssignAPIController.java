//package com.easycms.hd.api.baseservice;
//
//import java.lang.reflect.InvocationTargetException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.beanutils.BeanUtils;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.easycms.common.jpush.JPushExtra;
//import com.easycms.common.util.CommonUtility;
//import com.easycms.core.util.Page;
//import com.easycms.hd.api.domain.Consteam;
//import com.easycms.hd.api.domain.Constendman;
//import com.easycms.hd.api.domain.TeamMap;
//import com.easycms.hd.api.response.JsonResult;
//import com.easycms.hd.plan.domain.Isend;
//import com.easycms.hd.plan.domain.RollingPlan;
//import com.easycms.hd.plan.domain.StepFlag;
//import com.easycms.hd.plan.domain.WorkStep;
//import com.easycms.hd.plan.form.AssignForm;
//import com.easycms.hd.plan.service.JPushService;
//import com.easycms.hd.plan.service.RollingPlanService;
//import com.easycms.hd.plan.service.WorkStepFlagService;
//import com.easycms.hd.plan.service.WorkStepService;
//import com.easycms.hd.plan.util.PlanUtil;
//import com.easycms.hd.variable.service.VariableSetService;
//import com.easycms.management.user.domain.Department;
//import com.easycms.management.user.domain.Role;
//import com.easycms.management.user.domain.User;
//import com.easycms.management.user.service.DepartmentService;
//import com.easycms.management.user.service.RoleService;
//import com.easycms.management.user.service.UserService;
//
//@Controller
//@RequestMapping("/hdxt/api/baseservice/construction")
//public class ConstructionAssignAPIController {
//	private static final Logger logger = Logger.getLogger(ConstructionAssignAPIController.class);
//	private static String ASSIGN_TYPE = "assigntype";
//	private static String CONSTEAM = "consteam";
//	private static String TEAM = "team";
//	private static String END_MAN = "endman";
//	private String formatterDate = "yyyy-MM-dd";
//	
//	@Autowired
//	private RollingPlanService rollingPlanService;
//	@Autowired
//	private RoleService roleService;
//	@Autowired
//	private DepartmentService departmentService;
//	@Autowired
//	private UserService userService;
//	@Autowired
//	private WorkStepService workStepService;
//	@Autowired
//	private VariableSetService variableSetService;
//	@Autowired
//	private WorkStepFlagService workStepFlagService;
//	@Autowired
//	private JPushService jPushService;
//
//	/**
//	 * 获取待分派到班组的滚动计划信息分页列表，
//	 * @param request
//	 * @param response
//	 * @param form
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/team", method = RequestMethod.GET)
//	public JsonResult<Page<RollingPlan>> team(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
//		logger.info("Get rollingplan with pagenation");
//		JsonResult<Page<RollingPlan>> result = new JsonResult<Page<RollingPlan>>();
//		
//		String condition = request.getParameter("condition");
//		
//		String keyword = request.getParameter("keyword");
//		
//		if (!CommonUtility.isNonEmpty(condition)){
//			condition = "equal";
//		} else {
//			if (!condition.equals("equal") && !condition.equals("notequal")){
//				result.setCode("-1002");
//				result.setMessage("不合法的条件参数,只能是[equal]或[notequal]");
//				return result;
//			}
//		}
//		if (null != form.getConsteams() && form.getConsteams().length > 0){
//			for (String s : form.getConsteams()){
//				Integer teamId = 0;
//				try {
//					teamId = Integer.parseInt(s);
//				} catch (NumberFormatException e) {
//					e.printStackTrace();
//					result.setCode("-1002");
//					result.setMessage("错误的数据Consteams [" + teamId + "]");
//					return result;
//				}
//				
//				if (null == departmentService.findById(teamId)){
//					result.setCode("-1002");
//					result.setMessage("未找到teamId [" + teamId + "]");
//					return result;
//				}
//			}
//		}
//		
//		int pagesize = 0;
//		int pagenum = 0;
//		try {
//			pagesize = Integer.parseInt(request.getParameter("pagesize"));
//			pagenum = Integer.parseInt(request.getParameter("pagenum"));
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.setCode("-1001");
//			result.setMessage("不合法的分页数据");
//			return result;
//		}
//		
//		if (0 == pagenum || pagesize == 0){
//			result.setCode("-1001");
//			result.setMessage("不合法的分页请求");
//			return result;
//		}
//		
//		Page<RollingPlan> page = new Page<RollingPlan>();
//		page.setPagesize(pagesize);
//		page.setPageNum(pagenum);
//		
//		Page<RollingPlan> pageResult = null;
//		if (CommonUtility.isNonEmpty(keyword)){
//			keyword = "%" + keyword + "%";
//			pageResult = rollingPlanService.findPageByKeyWords(page, condition, "consteam", form.getConsteams(), keyword);
//		} else {
//			pageResult = rollingPlanService.findPage(page, condition, "consteam", form.getConsteams());
//		}
//		
//		result.setResponseResult(pageResult);
//		return result;
//	}
//	
//	/**
//	 * 获取可分派的班组
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/team/teams", method = RequestMethod.GET)
//	public JsonResult<List<Consteam>> teamTeam(HttpServletRequest request, HttpServletResponse response) {
//		JsonResult<List<Consteam>> result = new JsonResult<List<Consteam>>();
//		String cons_team = variableSetService.findValueByKey(CONSTEAM, ASSIGN_TYPE);
//		String team = variableSetService.findValueByKey(TEAM, ASSIGN_TYPE);
//		
//		Department department = departmentService.findByName(cons_team);
//		
//		List<Consteam> consTeams = new ArrayList<Consteam>();
//		if (null != department.getChildren()){
//			for (Department d : department.getChildren()){
//				if (null != d){
//					Consteam consteam = new Consteam();
//					try {
//						BeanUtils.copyProperties(consteam, d);
//					} catch (IllegalAccessException e) {
//						e.printStackTrace();
//					} catch (InvocationTargetException e) {
//						e.printStackTrace();
//					}
//					
//					Set<User> user = new HashSet<User>();
//					if (null != d.getUsers()){
//						for (User u : d.getUsers()){
//							if(null != u.getRoles()){
//								for (Role r : u.getRoles()){
//									if (r.getName().equals(team)){
//										user.add(u);
//									}
//								}
//							}
//						}
//					}
//					consteam.setUsers(new ArrayList<User>(user));
//					consTeams.add(consteam);
//				}
//			}
//		}
//		
//		result.setResponseResult(consTeams);
//		return result;
//	}
//
//	/**
//	 * 分派到施工班组
//	 * 
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/team", method = RequestMethod.POST)
//	public JsonResult<Boolean> teamAssign(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
//		JsonResult<Boolean> result = new JsonResult<Boolean>();
//		Integer[] ids = form.getIds();
//		String loginuserId = request.getParameter("loginId");
//		Integer userId = Integer.parseInt(loginuserId);
//		User user = userService.findUserById(userId);
//		
//		String team = variableSetService.findValueByKey(TEAM, ASSIGN_TYPE);
//		
//		if (ids.length < 1){
//			result.setCode("-1002");
//			result.setMessage("未指定rollingPlan");
//			return result;
//		}
//		
//		int teamId = 0;
//		try {
//			teamId = Integer.parseInt(form.getTeamId());
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			result.setCode("-1002");
//			result.setMessage("不合法的teamId [" + form.getTeamId() + "]");
//			return result;
//		}
//		
//		if (null == form.getPlanfinishdate()){
//			result.setCode("-1002");
//			result.setMessage("planfinishdate是必须参数");
//			return result;
//		} 
//		
//		if (null == departmentService.findById(teamId)){
//			result.setCode("-1002");
//			result.setMessage("未找到teamId [" + teamId + "]");
//			return result;
//		}
//		
//		for (int i : ids){
//			if (null == rollingPlanService.findById(i)){
//				result.setCode("-1002");
//				result.setMessage("未找到 rollingplan[" + i + "]");
//				return result;
//			}
//		}
//		
//		logger.debug("==> Start save new team assign for : " + CommonUtility.toJson(ids));
//		
//		boolean flag = false;
//		
//		List<RollingPlan> rps = rollingPlanService.findByIds(ids);
//		List<Integer> illegal = new ArrayList<Integer>();
//		for (RollingPlan rp : rps) {
//        	if (rp.getIsend() != 0 && rp.getIsend() != 4){
//        		illegal.add(rp.getId());
//        	}
//        }
//        
//        Integer[] newList = {};
//        ids = PlanUtil.arrContrast(ids, illegal.toArray(newList));
//        
//        if (ids.length < 1){
//			result.setCode("-1002");
//			result.setMessage("没有找到合法的rollingPlan");
//			return result;
//		}
//		
//		flag = rollingPlanService.assign(ids, form.getTeamId(), Isend.ARRANGED, new Date(), form.getPlanfinishdate());
//		
//		if (flag){
//			String alert = user.getRealname() + " - 将 [" + ids.length + "] 条滚动计划分派给了你";
//			JPushExtra extra = new JPushExtra();
//			extra.setCategory("assignTeam");
//			extra.setData(CommonUtility.toJson(ids).replace("\"", "'"));
//			extra.setRemark("data中的数据为滚动计划的ID数组");
//			jPushService.pushByDepartmentId(extra, alert, "分派到班组", teamId, team);
//		}
//		
//		result.setResponseResult(flag);
//		logger.debug("==> End save new team assign.");
//		return result;
//	}
//
////---------------------------------------------------endman-------------------------------------------------------
//	/**
//	 * 班长获取未分派到组长的滚动计划信息列表
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/endman", method = RequestMethod.GET)
//	public JsonResult<Page<RollingPlan>> endmanDataList(HttpServletRequest request, HttpServletResponse response) {
//		logger.debug("==> Show assign endman data list.");
//		JsonResult<Page<RollingPlan>> result = new JsonResult<Page<RollingPlan>>();
//		String loginuserId = request.getParameter("loginId");
//		Integer userId = Integer.parseInt(loginuserId);
//		
//		String condition = request.getParameter("condition");
//		
//		String keyword = request.getParameter("keyword");
//		
//		if (!CommonUtility.isNonEmpty(condition)){
//			condition = "equal";
//		} else {
//			if (!condition.equals("equal") && !condition.equals("notequal")){
//				result.setCode("-1002");
//				result.setMessage("不合法的条件参数,只能是[equal]或[notequal]");
//				return result;
//			}
//		}
//		
//		int pagesize = 0;
//		int pagenum = 0;
//		try {
//			pagesize = Integer.parseInt(request.getParameter("pagesize"));
//			pagenum = Integer.parseInt(request.getParameter("pagenum"));
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.setCode("-1001");
//			result.setMessage("不合法的分页数据");
//			return result;
//		}
//		
//		if (0 == pagenum || pagesize == 0){
//			result.setCode("-1001");
//			result.setMessage("不合法的分页请求");
//			return result;
//		}
//		
//		Page<RollingPlan> page = new Page<RollingPlan>();
//		page.setPagesize(pagesize);
//		page.setPageNum(pagenum);
//		
//		User user = userService.findUserById(userId);
//		if (null != user){
//			List<String> consteams = new ArrayList<String>();
//			if (null != user.getDepartment()){
//				consteams.add(user.getDepartment().getId() + "");
//			} else {
//				result.setCode("-1002");
//				result.setMessage("没有找到你的所属部门。");
//				return result;
//			}
//			
//			String[] ids = null;
//			
//			if (null != consteams && consteams.size() > 0){
//				int size = consteams.size();  
//		        ids = new String[size];  
//		        for(int i = 0; i < size; i++){  
//		        	ids[i] = consteams.get(i);  
//		        }
//		        
//		        logger.info(CommonUtility.toJson(ids));
//		
//			}
//			if(null == ids){
//				result.setCode("-1002");
//				result.setMessage("没有找到你的所属部门ID");
//				return result;
//			}
//			Page<RollingPlan> pageResult = null;
//			if (CommonUtility.isNonEmpty(keyword)){
//				keyword = "%" + keyword + "%";
//				pageResult = rollingPlanService.findPageByKeyWords(page, condition, "consendman", ids, keyword);
//			} else {
//				pageResult = rollingPlanService.findPage(page, condition, "consendman", ids);
//			}
//			
//			result.setResponseResult(pageResult);
//		}
//		return result;
//	}
//	
//
//	/**
//	 * 分派到施工组长的组长列表
//	 * 
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/endman/endmen", method = RequestMethod.GET)
//	public JsonResult<List<TeamMap>> endmanAssignUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
//		JsonResult<List<TeamMap>> result = new JsonResult<List<TeamMap>>();
//		String loginuserId = request.getParameter("loginId");
//		Integer userId = Integer.parseInt(loginuserId);
//		User user = userService.findUserById(userId);
//		
//		String teamString = variableSetService.findValueByKey(TEAM, ASSIGN_TYPE);
//		String endManString = variableSetService.findValueByKey(END_MAN, ASSIGN_TYPE);
//		
//		List<Role> roles = user.getRoles();
//		
//		boolean flag = false;
//		
//		for (Role r : roles){
//			if (r.getName().equals(teamString)){
//				flag = true;
//			}
//		}
//		
//		if (!flag){
//			result.setCode("-1002");
//			result.setMessage("你不是班长.");
//			return result;
//		}
//		
//		List<TeamMap> team = new ArrayList<TeamMap>();
//		
//		TeamMap tm = new TeamMap();
//		Constendman constendman = new Constendman();
//		Department department = user.getDepartment();
//		
//		try {
//			BeanUtils.copyProperties(constendman, department);
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			e.printStackTrace();
//		}
//		
//		List<User> users = department.getUsers();
//		List<User> teamUsers = new ArrayList<User>();
//		List<User> endManUsers = new ArrayList<User>();
//		
//		for (User u : users){
//			for (Role r : u.getRoles()){
//				if (r.getName().equals(teamString)){
//					teamUsers.add(u);
//				}
//				if (r.getName().equals(endManString)){
//					endManUsers.add(u);
//				}
//			}
//		}
//		
//		constendman.setUsers(teamUsers);
//		
//		tm.setParent(constendman);
//		
//		List<Constendman> constendmanList = new ArrayList<Constendman>();
//		
//		for (User u : endManUsers){
//			Constendman c = new Constendman();
//			
//			c.setId(u.getId());
//			c.setName(u.getRealname());
//			
//			List<User> uList = new ArrayList<User>();
//			uList.add(u);
//			
//			c.setUsers(uList);
//			constendmanList.add(c);
//		}
//		tm.setChildren(constendmanList);
//		
//		team.add(tm);
//			
//		result.setResponseResult(team);
//		return result;
//	}
//
//	/**
//	 * 班长分派滚动计划到组长
//	 * 
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/endman", method = RequestMethod.POST)
//	public JsonResult<Boolean> endmanAssign(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") AssignForm form) {
//		JsonResult<Boolean> result = new JsonResult<Boolean>();
//		Integer[] ids = form.getIds();
//		
//		if (ids.length < 1){
//			result.setCode("-1002");
//			result.setMessage("未指定rollingPlan");
//			return result;
//		}
//		
//		int endManId = 0;
//		try {
//			endManId = Integer.parseInt(form.getEndManId());
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			result.setCode("-1002");
//			result.setMessage("不合法的endManId [" + form.getEndManId() + "]");
//			return result;
//		}
//		
//		String loginuserId = request.getParameter("loginId");
//		Integer userId = Integer.parseInt(loginuserId);
//		User user = userService.findUserById(userId);
//		
//		User endMan = userService.findUserById(endManId);
//		if (null == endMan){
//			result.setCode("-1002");
//			result.setMessage("未找到endManId [" + endManId + "]");
//			return result;
//		} else {
//			if (user.isDefaultAdmin()){
//				;
//			} else {
//				boolean flag = true;
//				if(null != endMan.getDepartment() && null != user.getDepartment()){
//					if (endMan.getDepartment().getId() == user.getDepartment().getId()){
//						flag = false;
//					}
//					//需要确定是否必须会有直接上级。
//				}
//				
//				if (flag){
//					result.setCode("-1002");
//					result.setMessage("[" + endManId + "] 不是你的成员。");
//					return result;
//				}
//			}
//		}
//		
//		for (int i : ids){
//			if (null == rollingPlanService.findById(i)){
//				result.setCode("-1002");
//				result.setMessage("未找到 rollingplan[" + i + "]");
//				return result;
//			}
//		}
//		
//		if (null == form.getStartTime() || null == form.getEndTime()){
//			result.setCode("-1002");
//			result.setMessage("起止日期为必须参数");
//			return result;
//		}
//		
//		String startTime = null;
//		String endTime = null;
//		try {
//			startTime = CommonUtility.formateDate(form.getStartTime(), formatterDate);
//			endTime = CommonUtility.formateDate(form.getEndTime(), formatterDate);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.setCode("-1002");
//			result.setMessage("日期转换出错");
//			return result;
//		}
//		
//		String plandate = startTime + "至" + endTime;
//		
//		logger.debug("==> Start save new end man assign for : " + CommonUtility.toJson(ids));
//		List<RollingPlan> rps = rollingPlanService.findByIds(ids);
//		List<Integer> illegal = new ArrayList<Integer>();
//		
//        for (RollingPlan rp : rps) {
//        	List<WorkStep> wsList = rp.getWorkSteps();
//			if (null != wsList && wsList.size() > 0){
//				for (WorkStep ws : wsList){
//					if (!ws.getStepflag().equals(StepFlag.UNDO) && !ws.getStepflag().equals(StepFlag.PREPARE)){
//						rp.setStepStart(true);
//						break;
//					}
//				}
//			}
//        	
//        	if (rp.isStepStart() && rp.getIsend() != 2){
//        		illegal.add(rp.getId());
//        	}
//        }
//        
//        Integer[] newList = {};
//        ids = PlanUtil.arrContrast(ids, illegal.toArray(newList));
//		
//        if (ids.length < 1){
//			result.setCode("-1002");
//			result.setMessage("没有找到合法的rollingPlan");
//			return result;
//		}
//        
//		boolean flag = rollingPlanService.assignEndMan(ids, form.getEndManId(), plandate, Isend.UNDER_CONSTRUCTION);
//		if (flag){
//			String alert = user.getRealname() + " - 将 [" + ids.length + "] 条滚动计划分派给了你";
//			JPushExtra extra = new JPushExtra();
//			extra.setCategory("assignEndMan");
//			extra.setData(CommonUtility.toJson(ids).replace("\"", "'"));
//			extra.setRemark("data中的数据为滚动计划的ID数组");
//			jPushService.pushByUserId(extra, alert, "分派到组长", endManId);
//		}
//		
//		result.setResponseResult(flag);
//		for (Integer id : ids){
//			workStepFlagService.changeStepFlag(id);
//		}
//		
//		logger.debug("==> End save new end man assign.");
//		return result;
//	}
//}
