package com.easycms.hd.api.baseservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.domain.TeamResult;
import com.easycms.hd.api.enums.WitnessTeamTypeEnum;
import com.easycms.hd.api.request.WelderTeamRequestForm;
import com.easycms.hd.api.request.WitnessTeamRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.jcraft.jsch.Logger;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/team")
@Api(value = "TeamAPIController", description = "获取各类所需要人员列表相关的api")
@Slf4j
public class TeamAPIController {
	
	@Autowired
	private VariableSetService variableSetService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private RoleApiService roleApiService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RollingPlanService rollingPlanService;
	
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/witness", method = RequestMethod.GET)
	@ApiOperation(value = "获取人员列表", notes = "得到团队成员列表,例如见证组长列表,见证组员列表")
	public JsonResult<List<TeamResult>> witness(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") WitnessTeamRequestForm witnessTeamRequestForm) throws Exception {
		log.info("Get TeamResult");
		JsonResult<List<TeamResult>> result = new JsonResult<List<TeamResult>>();
		
		if (null == witnessTeamRequestForm.getTeamType()) {
			result.setCode("-2001");
			result.setMessage("必须选择一种类型。");
			return result;
		}
		String qcDepartment = variableSetService.findValueByKey("departmentQc", "departmentId");
		Integer qcDepartmentId= Integer.valueOf(qcDepartment);
		
		if (witnessTeamRequestForm.getTeamType().equals(WitnessTeamTypeEnum.WITNESS_MEMBER)) {
			List<TeamResult> teamResults = new ArrayList<TeamResult>();
			User user = null;
			if (null != witnessTeamRequestForm.getUserId()) {
				user = userService.findUserById(witnessTeamRequestForm.getUserId());
			} else {
				user = userService.findUserById(witnessTeamRequestForm.getLoginId());
			}
			if (null == user) {
				result.setCode("-2001");
				result.setMessage("用户信息为空。");
				return result;
			}

//			List<User> members = userService.findUserByParentId(user.getId());
			List<User> members = null;
			//TODO 判断是否是一个QC组长
			boolean isQCTeam = false;
			boolean isQC2Member = false;
			Set<String> roleNameList = new HashSet<String>();
			for (Role role : user.getRoles()) {
				Set<String> roleNameListx = variableSetService.findKeyByValue(role.getName(), "roleType");
				roleNameList.addAll(roleNameListx);
			}
			
			if (ComputedConstantVar.qc1TeamAndMumber(roleNameList)  &&  ContextPath.qc1ReplaceQc2) {//如果即是QC１组长，又是QC１组员
				isQCTeam = true;
				members = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_qc1","witness_member_czecqc","witness_member_czecqa","witness_member_paec"});
				
			}else if (ComputedConstantVar.qc2TeamAndMumber(roleNameList)  &&  !ContextPath.qc1ReplaceQc2) {//如果即是QC2组长，又是QC2组员
				isQCTeam = true;
				members = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_qc2","witness_member_czecqc","witness_member_czecqa","witness_member_paec"});
				
			}else if (roleNameList.contains("witness_team_qc1")) {
				isQCTeam = true;
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc1");
			}else if(roleNameList.contains("witness_team_qc2")){
				isQCTeam = true;
				members = userService.findUserByDepartmentAndRoleName(departmentService.findById(qcDepartmentId).getChildren(), "witness_member_qc2");
			} else if (ComputedConstantVar.qc2OrFqQc1(roleNameList)) {
				isQC2Member = true;
				members = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_czecqc","witness_member_czecqa","witness_member_paec"});
			}
			
			if (!isQCTeam && !isQC2Member) {
				result.setCode("-2001");
				result.setMessage("你并不是一个见证组组长或者QC2。");
				return result;
			}
			for (User u : members) {
				TeamResult teamResult = new TeamResult();
				teamResult.setId(u.getId());
				teamResult.setRealname(u.getRealname());
				teamResult.setName(u.getName());

				if (u.getRoles() != null && u.getRoles().size() > 0) {
					Set<AuthRole> roles = roleApiService.getAuthRole(u,false);
					teamResult.setRoles(roles);
				}
				
				teamResults.add(teamResult);
			}
			
			result.setResponseResult(teamResults);
		} else if (witnessTeamRequestForm.getTeamType().equals(WitnessTeamTypeEnum.WITNESS_TEAM)) {
			List<TeamResult> teamResults = new ArrayList<TeamResult>();
			
			String roleNameList = variableSetService.findValueByKey("witness_root", "witnessRoot");
			
			if (null == roleNameList || roleNameList.equals("")) {
				result.setCode("-2001");
				result.setMessage("见证最高部门配置有误。");
				return result;
			}
			List<Department> deptList = departmentService.findById(qcDepartmentId).getChildren();
			if(deptList==null || deptList.size()==0){
				deptList = new ArrayList<Department>();
				deptList.add(departmentService.findById(qcDepartmentId));
			}
			
			//海阳的想只是QC部，没有子部门
			List<User> members = userService.findUserByDepartmentAndRoleNames(deptList, new String[]{"witness_team_qc1", "witness_team_qc2"});
			for (User user : members) {
				TeamResult teamResult = new TeamResult();
				teamResult.setId(user.getId());
				teamResult.setRealname(user.getRealname());
				teamResult.setName(user.getName());

				if (user.getRoles() != null && user.getRoles().size() > 0) {
					Set<AuthRole> roles = roleApiService.getAuthRole(user,false);
					teamResult.setRoles(roles);
				}
				
				teamResults.add(teamResult);
			}
//			Department witnessDept = departmentService.findByName(roleNameList);
//			List<Department> qc1qc2List = witnessDept.getChildren();
//			for (Department department : qc1qc2List) {
//				List<Role> qcRoles = department.getTopRoles();
//				for (Role role : qcRoles) {
//					for (User user : role.getUsers()) {
//						TeamResult teamResult = new TeamResult();
//						teamResult.setId(user.getId());
//						teamResult.setRealname(user.getRealname());
//						teamResult.setName(user.getName());
//
//						if (user.getRoles() != null && user.getRoles().size() > 0) {
//							Set<AuthRole> roles = roleApiService.getAuthRole(user);
//							teamResult.setRoles(roles);
//						}
//						
//						teamResults.add(teamResult);
//					}
//				}
//			}
			
			result.setResponseResult(teamResults);
		}
		System.out.println(result.getResponseResult().size());
		result.setMessage("成功获取见证人员信息");
		return result;
	}
	
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/welder", method = RequestMethod.GET)
	@ApiOperation(value = "获取焊工人员列表", notes = "得到指定滚动计划下的焊工人员列表")
	public JsonResult<List<TeamResult>> welder(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") WelderTeamRequestForm form) throws Exception {
		log.info("Get TeamResult");
		JsonResult<List<TeamResult>> result = new JsonResult<List<TeamResult>>();
		
		RollingPlan rollingPlan = rollingPlanService.findById(form.getRollingPlanId());
		
		if (null == rollingPlan) {
			result.setCode("-2001");
			result.setMessage("滚动计划没有找到。");
			return result;
		}
		
		Integer teamId = rollingPlan.getConsteam();
		if (null == teamId) {
			result.setCode("-2001");
			result.setMessage("滚动计划未分派到组。");
			return result;
		}
		
		User team = userService.findUserById(teamId);
		if (null == team) {
			result.setCode("-2001");
			result.setMessage("滚动计划所分派的组长未找到。");
			return result;
		}
		
		List<TeamResult> teamResults = new ArrayList<TeamResult>();
		
		String roleName = variableSetService.findValueByKey("welder", "roleType");
		if (!CommonUtility.isNonEmpty(roleName)){
			result.setCode("-2001");
			result.setMessage("welder并没有在系统中配置。");
			return result;
		}
		Role role = roleService.findByName(roleName);
		if (null == role) {
			result.setCode("-2001");
			result.setMessage("没有找到配置的角色。");
			return result;
		}
		List<User> allWelders = role.getUsers();
		if (null != allWelders && !allWelders.isEmpty()) {
			List<User> welders = allWelders.stream().filter(allWelder -> {
				User parent = allWelder.getParent();
				
				if (null != parent && parent.getId().equals(team.getId())) {
					return true;
				}
				return false;
			}).collect(Collectors.toList());
			
			//因为真实的Enpower数据没有焊工人员，随便查询 焊工管理部门的人员出来
			if(welders.size()==0){
				welders = getWelderByDepartmentId();
			}
			
			for (User u : welders) {
				TeamResult teamResult = new TeamResult();
				teamResult.setId(u.getId());
				teamResult.setRealname(u.getRealname());
				teamResult.setName(u.getName());
	
				if (u.getRoles() != null && u.getRoles().size() > 0) {
					Set<AuthRole> roles = roleApiService.getAuthRole(u,false);
					teamResult.setRoles(roles);
				}
				
				teamResults.add(teamResult);
			}
		}
		
		result.setResponseResult(teamResults);
		result.setMessage("成功获取焊工人员信息");
		return result;
	}
	
	public List<User> getWelderByDepartmentId(){
		String departmentId = variableSetService.findValueByKey("welderDepartment", "departmentId");
		return userService.findUserByDepartmentId(Integer.valueOf(departmentId));
	}
}
