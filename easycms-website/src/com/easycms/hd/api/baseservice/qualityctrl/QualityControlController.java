package com.easycms.hd.api.baseservice.qualityctrl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.StringUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.baseservice.hseproblem.HseProblemTransForResult;
import com.easycms.hd.api.enums.qc.QcConstantVar;
import com.easycms.hd.api.request.LoginIdRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcAssignRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcNoteRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcRenovateResultRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcResultCheckEnum;
import com.easycms.hd.api.request.qualityctrl.QualityControlPageRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityControlRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityTypePageRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlPageDataResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlResult;
import com.easycms.hd.api.service.QualityControlApiService;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.domain.QualityControlOptions;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.easycms.hd.qualityctrl.service.impl.QualityControlOptionsServiceImpl;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/qualityControl")
@Api(value="QualityControlController",description="质量管理模块相关的api")
@Slf4j
@javax.transaction.Transactional
public class QualityControlController {
	@Autowired
	private QualityControlApiService qualityControlApiService;
	@Autowired
	private QualityControlService qualityControlService;
	@Autowired
	private QualityControlOptionsServiceImpl qualityControlOptions;

	@Autowired
	private UserService userService;
	@Autowired
	private DepartmentService departmentService;
	
	
	@ResponseBody
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ApiOperation(value = "创建质量问题", notes = "用于创建质量问题")
	public JsonResult<Boolean> create(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QualityControlRequestForm qcRequestForm,@RequestParam(value="file",required=false) CommonsMultipartFile[] files){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		Boolean status = false ;

//		if(files == null){
//			result.setCode("-1000");
//			result.setMessage("Error/s occurred, Files is not available");
//			return result;
//		}
		try{

			status = qualityControlApiService.createQualityProblem(qcRequestForm, files);

		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}

		if(!status){
			log.debug("创建质量管理问题失败！");
			result.setCode("-1000");
			result.setMessage("Error/s occurred, Create failed!");
			return result;
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "getList", method = RequestMethod.GET)
	@ApiOperation(value = "获取所有质量问题分页列表", notes = "用于查询所有质量管理问题列表")
	public JsonResult<QualityControlPageDataResult> getList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QualityControlPageRequestForm qcPageRequestForm) throws Exception{
		JsonResult<QualityControlPageDataResult> result = new JsonResult<QualityControlPageDataResult>();
		QualityControlPageDataResult qcPqgeResult = null;
		Page<QualityControl> page = new Page<QualityControl>();
		
		if (null != qcPageRequestForm.getPagenum() && null != qcPageRequestForm.getPagesize()) {
			page.setPageNum(qcPageRequestForm.getPagenum());
			page.setPagesize(qcPageRequestForm.getPagesize());
		}
		
		qcPqgeResult = qualityControlApiService.getQualityProblemPageResult(page, qcPageRequestForm);
		
		result.setCode("1000");
		result.setMessage("获取所有列表成功！");
		result.setResponseResult(qcPqgeResult);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getList/type", method = RequestMethod.GET)
	@ApiOperation(value = "获取质量问题列表", notes = "根据loginId、QC1ID、部门ID、班组ID获取相应的质量管理问题")
	public JsonResult<QualityControlPageDataResult> getListByType(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QualityTypePageRequestForm form) throws Exception{
		JsonResult<QualityControlPageDataResult> result = new JsonResult<QualityControlPageDataResult>();
		QualityControlPageDataResult qcPqgeResult = null;
		Page<QualityControl> page = new Page<QualityControl>();

		if(form.getQueryId() == null && form.getQueryId().intValue()==0){
			result.setCode("-1000");
			result.setMessage("必要参数Id为空！");
			return result;
		}
		if(form.getType() == null){
			result.setCode("-1000");
			result.setMessage("必要参数Type为空！");
			return result;
		}
		if (null != form.getPagenum() && null != form.getPagesize()) {
			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
		}
		qcPqgeResult = qualityControlApiService.getQualityProblemPageResultByType(page, form);
		
		result.setCode("1000");
		result.setMessage("获取信息成功！");
		result.setResponseResult(qcPqgeResult);
		return result;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "getList/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取单个质量问题", notes = "用于查询单个质量问题,QC专业室主任分派QC1,")
	public JsonResult<QualityControlResult> getSingle(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm form,
			@PathVariable("id") Integer id) throws Exception{
		JsonResult<QualityControlResult> result = new JsonResult<QualityControlResult>();
		QualityControlResult qcPqgeResult = null;
		QualityControl qc = qualityControlService.findById(id);
		HseProblemTransForResult tran = new HseProblemTransForResult();
		List<User> userList = null;
		
		Integer loginId = 0;
		boolean isQCManager = false;
		
		if(form.getLoginId() != null){
			loginId = form.getLoginId();
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser != null){
			//当前登录人是否是QC专业室主任
			isQCManager = userService.isRoleOf(loginUser, "QCManager");
		}else{
			result.setCode("-1000");
			result.setMessage("操作执行失败！当前登录ID不存在！");
			return result;
		}
		
		if(isQCManager){
			List<Department> departments = loginUser.getDepartment().getChildren();
			if(departments==null ||departments.size()==0){
				departments = new ArrayList<Department>();
				departments.add(loginUser.getDepartment());
			}
			//如果是QC专业室主任，则将下属QC1查询出来
			userList = userService.findUserByDepartmentAndRoleNames(departments,
					new String[]{ConstantVar.witness_team_qc1,ConstantVar.witness_team_qc2,ConstantVar.witness_member_qc1,ConstantVar.witness_member_qc2,ConstantVar.qc_member});//"QC1");
		}
		
		if(qc != null){
			qcPqgeResult = qualityControlApiService.transferToResultList(qc);		
			qcPqgeResult.setUserList(tran.user(userList));
		}
		if(qcPqgeResult !=null){
			result.setResponseResult(qcPqgeResult);
			result.setCode("1000");
			result.setMessage("操作成功！");			
		}else{
			result.setCode("-1000");
			result.setMessage("获取数据为空！");	
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "qcLeaderAssign", method = RequestMethod.POST)
	@ApiOperation(value = "分派QC1", notes = "用于QC专业室主任分派QC1")
	public JsonResult<Boolean> qcLeaderAssign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QcAssignRequestForm qcRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = 0;
		
		if(qcRequestForm.getLoginId() != null){
			loginId = qcRequestForm.getLoginId();
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser != null){
			//当前登录人是否是QC专业室主任
			if(!userService.isRoleOf(loginUser, "QCManager")){//暂时角色数据没有QC专业室主任，取QC经理处理
				result.setCode("-1000");
				result.setMessage("没有权限处理当前操作！");
				return result;
			}
			
		}else{
			result.setCode("-1000");
			result.setMessage("操作执行失败！当前登录ID不存在！");
			return result;
		}
		
		status = qualityControlApiService.qcLeaderAssign(qcRequestForm);
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作执行失败！");
		}
		result.setResponseResult(status);
		return result;
	}
//	@ResponseBody
//	@RequestMapping(value = "responsibilityLeaderAssignUI", method = RequestMethod.GET)
//	@ApiOperation(value = "分派责任班组人员", notes = "用于责任部门分派责任班组人员")
//	public JsonResult<Boolean> responsibilityLeaderAssignUI(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute("requestForm") QcAssignRequestForm qcRequestForm){
//		JsonResult<Boolean> result = new JsonResult<Boolean>();
//		//返回本部门下的子部门下的人员及子子部门下的人员列表
//		
//		boolean status = false;
//	
//		//TODO dddddd
//		return result;
//	}
	
	
	@ResponseBody
	@RequestMapping(value = "responsibilityLeaderAssign", method = RequestMethod.POST)
	@ApiOperation(value = "分派责任班组人员", notes = "用于责任部门分派责任班组人员")
	public JsonResult<Boolean> responsibilityLeaderAssign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QcAssignRequestForm qcRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = 0;
		
		if(qcRequestForm.getLoginId() != null){
			loginId = qcRequestForm.getLoginId();
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser != null){
			//当前登录人是否是责任部门人员
			/**
			 * 机通队
				主系统队
				电仪队
				管焊队
			 */
			if(departmentService.isFromDept(loginUser, departmentService.responsibilityDept()) == false){
				result.setCode("-1000");
				result.setMessage("没有责任部门权限处理当前操作！");
				return result;
			}
			
		}else{
			result.setCode("-1000");
			result.setMessage("操作执行失败！当前登录ID不存在！");
			return result;
		}

		status = qualityControlApiService.responsibilityLeaderAssign(qcRequestForm);
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作执行失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	

	@ResponseBody
	@RequestMapping(value = "qcAssign", method = RequestMethod.POST)
	@ApiOperation(value = "确认存在质量问题", notes = "用于QC1确认存在质量问题")
	public JsonResult<Boolean> qcAssign(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "problemId", value = "质量问题ID") @RequestParam(value="problemId",required=true) Integer problemId,
			@ApiParam(required = true, name = "timeLimit", value = "整改期限",defaultValue="1508210311000") @RequestParam(value="timeLimit",required=true) Long timeLimit,
			@ApiParam(required = false, name = "renovateDeptId", value = "修改责任部门id") @RequestParam(value="renovateDeptId",required=false) Integer renovateDeptId,
			@ApiParam(required = false, name = "renovateTeamId", value = "修改责任班组id") @RequestParam(value="renovateTeamId",required=false) Integer renovateTeamId,
			@ModelAttribute("requestForm") BaseRequestForm qcRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		String msg = null;
		Integer loginId = qcRequestForm.getLoginId();
		if(loginId == null || loginId.intValue() == 0){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, loginId is not available");
			return result;
		}
		if(timeLimit == null){
			timeLimit = 1508210331000L;
		}
		
		QcAssignRequestForm form = new QcAssignRequestForm();
		form.setQcProblrmId(problemId);
		form.setLoginId(loginId);
		
		//QC人员接收QC室主任分派的任务后，应当有权限对责任班组和责任部门进行重新选定编辑


		try{
			Map<String,Object> rsMap = qualityControlApiService.qcAssignRenovateTeam(form,timeLimit,renovateDeptId,renovateTeamId);
			if(!rsMap.isEmpty()){
				status = (boolean) rsMap.get("status");
				msg = String.valueOf(rsMap.get("msg"));
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-1000");
			result.setMessage("操作异常:"+e.getMessage());
			return result;
		}
		if(!status){
			result.setCode("-1000");
			result.setMessage(msg);
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "qcAssignClose", method = RequestMethod.POST)
	@ApiOperation(value = "关闭问题", notes = "用于QC1关闭问题")
	public JsonResult<Boolean> qcAssignClose(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "qualityFlag", value = "是否开启质量问题单") @RequestParam(value="qualityFlag",required=true) boolean qualityFlag,
			@ModelAttribute("requestForm") QcNoteRequestForm qcRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = qcRequestForm.getLoginId();
		if(loginId == null || loginId.intValue() == 0){
			result.setCode("-1000");
			result.setMessage("Error/s occurred, loginId is not available");
			return result;
		}
		try{
			status = qualityControlApiService.qcAssignClose(qcRequestForm,qualityFlag);
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作执行失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "teamRenovete", method = RequestMethod.POST)
	@ApiOperation(value = "提交整改结果", notes = "用于责任单位提交整改结果")
	public JsonResult<Boolean> teamRenovete(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QcRenovateResultRequestForm qcRequestForm,@RequestParam(value="file") CommonsMultipartFile[] files){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = qcRequestForm.getLoginId();
		if(loginId == null || loginId.intValue() == 0){
			result.setCode("-1000");
			result.setMessage("Error/s occurred, loginId is not available");
			return result;
		}
		if(files == null){
			result.setCode("-1000");
			result.setMessage("Error/s occurred, Files is not available");
			return result;
		}
		status = qualityControlApiService.teamRenoveteResult(qcRequestForm,files);
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作执行失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "qcVerify", method = RequestMethod.POST)
	@ApiOperation(value = "核实整改结果", notes = "用于QC人员核实整改结果")
	public JsonResult<Boolean> qcverify(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "checkResult", value = "核实结果，Qualified-合格，Unqualified-不合格") @RequestParam(value="checkResult",required=true) QcResultCheckEnum checkResult,
			@ModelAttribute("requestForm") QcNoteRequestForm qcRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = qcRequestForm.getLoginId();
		if(loginId == null || loginId.intValue() == 0){
			result.setCode("-1000");
			result.setMessage("Error/s occurred, loginId is not available");
			return result;
		}
		
		if(checkResult == null){
			result.setCode("-1000");
			result.setMessage("Error/s occurred, checkResult is not available");
			return result;
		}

		try{

			status = qualityControlApiService.qcVerifyResult(qcRequestForm,checkResult);
					
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作执行失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	//不用这个了，使用http://localhost:8080/hdxt/api/hse/createUI?loginId=256&responsibleDeptId=247　中提供　
//	@ResponseBody
//	@RequestMapping(value = "getLocationLevel", method = RequestMethod.GET)
//	@ApiOperation(value = "获取所有质量下拉选项", notes = "用于获取所有质量下拉选项")
//	public JsonResult<List<String>> getLocationLevel(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(required = false, name = "type", value = "area，room，level之一")@RequestParam(value="type",required=true) String type,
//			@ApiParam(required = false, name = "area", value = "区域过滤查询房间号或标高")@RequestParam(value="area",required=false) String area,
//			@ApiParam(required = false, name = "room", value = "区域和房间号过滤查询房间号或标高")@RequestParam(value="room",required=false) String room,
//			@ModelAttribute("form") LoginIdRequestForm form) throws Exception{
//		JsonResult<List<String>> result = new JsonResult<List<String>>();
//		
//		List<String> retList = new ArrayList<String>();
//
//		try{
//			
//			QualityControlOptions forSelect = new QualityControlOptions();
//			if(QcConstantVar.area.equals(type)){
////				forSelect.setType(type);
//				List<QualityControlOptions> list = qualityControlOptions.findAll(forSelect);
//				
//				list.stream().forEach(
//			        // 去重
//			                p -> {
//			                	
//			                    if (!retList.contains(p.getArea())) {
//			                    	retList.add(p.getArea());
//			                    }
//				});
//				
//			}else if(QcConstantVar.room.equals(type)){
//				forSelect.setArea(area);
//				List<QualityControlOptions> list = qualityControlOptions.findAll(forSelect);
//				list.stream().forEach(
//				        // 去重
//				                p -> {
//				                	
//				                    if (!retList.contains(p.getRoom())) {
//				                    	retList.add(p.getRoom());
//				                    }
//					});
//				
//			}else if(QcConstantVar.level.equals(type)){
//				forSelect.setArea(area);
//				forSelect.setRoom(room);
//				List<QualityControlOptions> list = qualityControlOptions.findAll(forSelect);
//				list.stream().forEach(
//				        // 去重
//				                p -> {
//				                	
//				                    if (!retList.contains(p.getLevel())) {
//				                    	retList.add(p.getLevel());
//				                    }
//					});
//			}
//			
//			result.setResponseResult(retList);
//			
//			
//		}catch(Exception e){
//			e.printStackTrace();
//			result.setMessage("操作异常:"+e.getMessage());
//			result.setResponseResult(Collections.emptyList());
//			return result;
//		}
//		return result ;
//	}
}
