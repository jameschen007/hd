package com.easycms.hd.api.baseservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.request.ConferenceFeedbackRequestForm;
import com.easycms.hd.api.request.ConferenceMarkRequestForm;
import com.easycms.hd.api.request.base.BaseIdsRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.ConferenceApiService;
import com.easycms.hd.api.service.ConferenceFeedbackApiService;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/conference_op")
@Api(value = "ConferenceOperationAPIController", description = "会议通知操作相关的api")
@javax.transaction.Transactional
public class ConferenceOperationAPIController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConferenceService conferenceService;
	
	@Autowired
	private ConferenceApiService conferenceApiService;
	
	@Autowired
	private ConferenceFeedbackApiService conferenceFeedbackApiService;

	/**
	 * 会议反馈
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	@ApiOperation(value = "反馈会议", notes = "用户发起一次会议的反馈")
	public JsonResult<Boolean> conferenceFeedback(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ConferenceFeedbackRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		User user = userService.findUserById(form.getLoginId());
		
		Conference conference = conferenceService.findById(form.getConferenceId());
    	
    	if (null != conference) {
    		List<User> participants = conference.getParticipants();
    		if (null != participants && !participants.isEmpty()) {
    			boolean flag = false;
    			for (User u : participants) {
    				if (u.getId().equals(user.getId())) {
    					flag = true;
    					break;
    				}
    			}
    			
    			if (!flag) {
    				result.setCode("-1000");
	                result.setMessage("你不是参会人之一，不允许会议反馈。");
	                return result;
    			}
		
				if (conferenceFeedbackApiService.insert(form, user)) {
					ConferenceMarkRequestForm conferenceMarkRequestForm = new ConferenceMarkRequestForm();
					conferenceMarkRequestForm.setConferenceId(form.getConferenceId());
					conferenceFeedbackApiService.markRead(conferenceMarkRequestForm, user);
					
					result.setMessage("会议反馈成功。");
				} else {
					result.setCode("-1000");
					result.setMessage("会议反馈失败。");
					return result;
				}
    		} else {
    			result.setCode("-1000");
                result.setMessage("没有任何参会人，不允许会议反馈。");
                return result;
    		}
    	} else {
    		result.setCode("-1000");
            result.setMessage("会议不存在。");
            return result;
    	}
		return result;
	}
	
	/**
	 * 会议已读标记
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/mark", method = RequestMethod.POST)
	@ApiOperation(value = "标记会议反馈已读", notes = "对发起点之前时间的会议反馈标记为已读")
	public JsonResult<Boolean> markConferenceMark(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ConferenceMarkRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User user = userService.findUserById(form.getLoginId());
		
		Conference conference = conferenceService.findById(form.getConferenceId());
    	
    	if (null != conference) {
    		List<User> participants = conference.getParticipants();
    		if (null != participants && !participants.isEmpty()) {
    			boolean flag = false;
    			for (User u : participants) {
    				if (u.getId().equals(user.getId())) {
    					flag = true;
    					break;
    				}
    			}
    			
    			if (!flag) {
    				result.setCode("-1000");
	                result.setMessage("你不是参会人之一，不允许会议已读标记。");
	                return result;
    			}
    			
				if (conferenceFeedbackApiService.markRead(form, user)) {
					result.setMessage("标记会议反馈成功。");
				} else {
					result.setCode("-1000");
					result.setMessage("标记会议反馈失败。");
					return result;
				}
    		} else {
    			result.setCode("-1000");
                result.setMessage("没有任何参会人，不允许会议已读标记。");
                return result;
    		}
    	} else {
    		result.setCode("-1000");
            result.setMessage("会议不存在。");
            return result;
    	}
		return result;
	}

	/**
	 * 删除会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ApiOperation(value = "删除会议", notes = "删除指定IDS的会议")
	public JsonResult<Boolean> deleteConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseIdsRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		if (conferenceApiService.batchDelete(form.getIds())) {
			result.setMessage("删除会议成功。");
		} else {
			result.setCode("-1000");
			result.setMessage("删除会议失败。");
			return result;
		}
		return result;
	}
	/**
	 * 取消会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	@ApiOperation(value = "取消会议", notes = "删除指定IDS的会议")
	public JsonResult<Boolean> cancelConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseIdsRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{
			

			if (conferenceApiService.batchCancel(form.getIds())) {
				result.setMessage("取消会议成功。");
			} else {
				result.setCode("-1000");
				result.setMessage("取消会议失败。");
				return result;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		return result;
	}
}
