package com.easycms.hd.api.baseservice.hseproblem;

import java.util.ArrayList;
import java.util.List;

import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.hseproblem.HseProblemFileResult;
import com.easycms.hd.hseproblem.domain.HseProblemFile;
import com.easycms.management.user.domain.User;

public class HseProblemTransForResult {
	
	public List<HseProblemFileResult> problemFile(List<HseProblemFile> files){
		List<HseProblemFileResult> r = new ArrayList<HseProblemFileResult>();
		if(files!=null && files.size()>0){
			for(HseProblemFile f:files){
				HseProblemFileResult r1 = new HseProblemFileResult();
//				String visitPath = com.easycms.common.logic.context.ContextPath.selfProjectContextPath;
				r1.setPath("/hdxt/api/files/problem/"+f.getFilepath());
				r1.setFileName(f.getFilename());
				r1.setFileType(f.getFiletype());
				r.add(r1);
			}
		}
		return r ;
	}
	
	public List<UserResult> user(List<User> users){
		List<UserResult> ret = new ArrayList<UserResult>();
		if(users!=null && users.size()>0){
			for(User u:users){
				UserResult r = new UserResult();
				r.setId(u.getId());
				r.setUsername(u.getName());
				r.setRealname(u.getRealname());
				ret.add(r);
			}
		}
		return ret ;
	}

}
