package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ProblemPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -5645163813710775281L;
	private List<ProblemResult> data;
}
