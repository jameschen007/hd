package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;

@Data
public class ExpireNoticeResult implements Serializable{
	private static final long serialVersionUID = 478246806320043284L;
	/**
	 * 失效通知ID
	 */
	private Integer id;
	/**
	 * 编号
	 */
	private String no;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 部门
	 */
	private String department;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 审批日期
	 */
	private Date approvelTime;
	/**
	 * 编制日期
	 */
	private Date writeTime;
	/**
	 * 发布日期
	 */
	private Date publishTime;
	/**
	 * 文件列表信息
	 */
	private List<ExpireNoticeFilesResult> files;
	
	/**
	 * 用户领用数量的统计信息
	 */
	private ExpireNoticeExtraResult extra;
}
