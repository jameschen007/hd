package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class ConferenceResult implements Serializable{
	private static final long serialVersionUID = 765785624176552375L;
	/**
	 * 会议唯一标识ID
	 */
	private Integer id;
	/**
	 * 会议主题
	 */
	private String subject;
	/**
	 * 会议正文
	 */
	private String content;
	/**
	 * 会议类型
	 */
	private String category;
	/**
	 * 项目名称
	 */
	private String project;
	/**
	 * 主持人
	 */
	private String host;
	/**
	 * 记录员
	 */
	private String recorder;
	/**
	 * 会议用品
	 */
	private String supplies;
	/**
	 * 会议备注
	 */
	private String remark;
	/**
	 * 会议状态
	 */
	private String status;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 提醒时间
	 */
	private String alarmTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 会议用品
	 */
	private String address;
	/**
	 * 会议创建来源 
	 */
	private String source;
	/**
	 * 未读反馈
	 */
	private Long unread = 0L;
	/**
	 * 会议附件
	 */
	private List<FileResult> files;
	/**
	 * 参会人信息
	 */
	private Set<UserResult> participants;
	/**
	 * 反馈信息
	 */
	private List<ConferenceFeedbackResult> feedback;
}
