package com.easycms.hd.api.response.qualityctrl;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
@Data
public class QualityControlOptionResult implements Serializable {
	private static final long serialVersionUID = 5363912691138076500L;

	private String room;
	
	private List<String> level;

}
