package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
/**
 * 用于物料日志部门出库的统计
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class MaterialStatisticsMainResult implements Serializable{
	private static final long serialVersionUID = 5904853816414148232L;
	private List<MaterialDepartmentStatisticsResult> departments;
	private Long total = 0L;
}
