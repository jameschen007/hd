package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 见证成员的见证点统计信息
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class NoticePointStatisticsResult implements Serializable{
	private static final long serialVersionUID = -7629770563947599686L;
	private Long total = 0L;
	private Long pointW = 0L;
	private Long pointR = 0L;
	private Long pointH = 0L;
	private String mark;
}
