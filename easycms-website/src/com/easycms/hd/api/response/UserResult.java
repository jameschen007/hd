package com.easycms.hd.api.response;

import java.util.Set;

import com.easycms.hd.api.domain.AuthRole;

import lombok.Data;

@Data
public class UserResult {
	private Integer id;
	private String username;
	private String realname;
	private boolean del;
	private Set<AuthRole> roles;
	private DepartmentResult department;
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof UserResult) {
        	UserResult userResult = (UserResult) obj;
            return (id.equals(userResult.id));
        }
        return super.equals(obj);
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
}
