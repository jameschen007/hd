package com.easycms.hd.api.response;

import java.io.Serializable;

import lombok.Data;
/**
 * 查询今日出库量（按部门）接口
 * @author ul-webdev
 *
 */
@Data
public class MaterialOutCountResult implements Serializable {
	private static final long serialVersionUID = 7136267007264102616L;
	
	/**
	 * 数量
	 * "QTY":14.5460
	 */
	private String number;
	/**
	 * Iss_Dept		出库单位
	 */
	private String issDept;
}
