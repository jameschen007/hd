package com.easycms.hd.api.response.question;
import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RollingQuestionPageDataResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -5645163813710775281L;
	private List<RollingQuestionResult> data;
}
