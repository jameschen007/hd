package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.easycms.hd.api.domain.AuthModule;
import com.easycms.hd.api.domain.AuthRole;

import lombok.Data;

@Data
public class AuthorizationResult implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	private Integer id;
	private String username;
	private String realname;
	private Set<AuthRole> roles;
	private String device;
	private Set<AuthModule> modules;
	private DepartmentResult department;	
	public Boolean qc1ReplaceQc2=false ;
	/**
	 * 下一次需要更新密码时间
	 */
	private Date updatePwdTime;
	private Boolean isUpdatePwd;//是否已到时间该修改密码了

	private String updatePwdDesc="请前往“我的/修改密码”！每半年至少修改一次密码，修改的密码应确保与最近５次使用过的密码不重复。" ;//

	private String pwdRule="(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}";
	private String pwdRuleDesc="pwdRuleDesc";

}
