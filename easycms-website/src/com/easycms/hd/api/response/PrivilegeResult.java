package com.easycms.hd.api.response;

import java.io.Serializable;

public class PrivilegeResult implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	
	public PrivilegeResult(){}
	public PrivilegeResult(String name,String icon,String iconClass,String uri){
		this.name = name;
		this.icon = icon;
		this.iconClass = icon;
		this.uri = uri;
	}
	
	private String name;
	private String icon;
	private String iconClass;
	private String uri;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
