package com.easycms.hd.api.response;

import java.io.Serializable;

public class JsonResult<T> implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	
	public JsonResult(){
		this.code = "1000";
		this.message = "操作执行成功";
	}
	public JsonResult(String code,String message){
		this.code = code;
		this.message = message;
	}
	
	private String code;
	private String message;
	private T responseResult;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getResponseResult() {
		return responseResult;
	}
	public void setResponseResult(T responseResult) {
		this.responseResult = responseResult;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
