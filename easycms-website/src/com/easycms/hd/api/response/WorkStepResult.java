package com.easycms.hd.api.response;


import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class WorkStepResult implements java.io.Serializable {
	private static final long serialVersionUID = 82244958518633118L;
	/**
	 * workstep的唯一ID号
	 */
	private Integer id;
	/**
	 * 工序的序列号
	 */
//	private Integer stepno;
	private String stepIdentifier;
	/**
	 * 滚动计划ID
	 */
	private Integer rollingPlanId;
	/**
	 * 完成情况
	 */
	private String stepflag;
	/**
	 * 工序号
	 */
	private Integer stepno;
	/**
	 * 工序名称
	 */
	private String stepname;
	/**
	 * Itp Name
	 */
	private String itpName;
	/**
	 * 焊口支架
	 */
	private String weldno;
	/**
	 * 工序状态
	 */
	private String status;
	/**
	 * 见证结果
	 */
	private String result;
	/**
	 * 是否是最后一道工序
	 */
	private boolean lastone = false;
	/**
	 * 是否存在未解决的问题
	 */
	private boolean problemFlag = false;	
	/**
	 * QC1
	 */
	private String noticeQC1;
	/**
	 * QC1
	 */
	private String noticeQC2;
	/**
	 * CZEC QC
	 */
	private String noticeCZECQC;
	/**
	 * CZEC QA
	 */
	private String noticeCZECQA;
	/**
	 * PAEC
	 */
	private String noticePAEC;
	/**
	 * 发起见证的时间
	 */
	private Date launchData;
	/**
	 * 见证地点
	 */
	private String witnessAddress ;
	
	/**
	 * 见证时间
	 */
	private Date witnessDate;
	
	/**
	 * 真实见证地点
	 */
	private String realWitnessAddress ;
	
	/**
	 * 真实见证时间
	 */
	private Date realWitnessDate;
	
	/**
	 * 见证信息列表
	 */
	private List<WitnessResult> witnessInfo;
	
	/**
	 * 见证地点列表
	 */
	private List<String> witnessAddresses;
}