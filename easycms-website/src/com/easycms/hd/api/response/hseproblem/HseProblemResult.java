package com.easycms.hd.api.response.hseproblem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class HseProblemResult implements Serializable {
	private static final long serialVersionUID = 4330085435684511306L;

	private String id;

    private Integer createUserId;
    private String createUser;
    
	private String problemTitle;

    private String unit;

    private String wrokshop;

    private String eleration;

    private String roomno;

    private String problemDescription;

    //问题提交时间
    private Date createDate;

    private String problemStatus;
    
    private String problemSolveStatus;

    private HseResponsibleDeptResult responsibleDept;
    
    private HseResponsibleDeptResult responsibleTeam;
    
    //要求整改完成日期
    private Date targetDate;
    
    //实际整改完成日期
    private Date realDate;
    
    //审核时间
    private Date checkDate;
    
    //问题关闭时间
    private Date finishDate;
    
    private String problemStep;
    
  //安全编码（三级） id
	private String code1Id;
	private String code2Id;
	private String code3Id;
	private String code2Desc;
	private String code3Desc;
    private List<HseProblemFileResult> files;
    
    private List<HseProblemSolve> hseProblemSolve;
    
	private Integer fileSize;
//	@ApiParam(value="检查类型",required=false)
	private String checkType;
//	@ApiParam(value="隐患严重性类别",required=false)
	private String criticalLevel;
//	@ApiParam(value="整改要求",required=false)
	private String requirement;
	//是否超期
	private boolean delay;
	
	

/***
 * 用于HSE修改责任部门和责任班组的供选项，只有查详情时
 */
	private List<HseResponsibleDeptResult> responsibleDepts ;
	private List<QuaResponsibleDeptResult> responsibleTeams;

}
