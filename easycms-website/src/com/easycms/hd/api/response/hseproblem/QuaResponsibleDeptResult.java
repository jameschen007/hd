package com.easycms.hd.api.response.hseproblem;

import lombok.Data;

/**
 * 责任班组
 * @author ul-webdev
 *
 */
@Data
public class QuaResponsibleDeptResult {

	private Integer parentDeptId;
	private Integer deptId;
	private String deptName;

}
