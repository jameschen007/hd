package com.easycms.hd.api.response.hseproblem;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemPageDataResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -3993263055224988961L;
	
	private List<HseProblemResult> data;

}
