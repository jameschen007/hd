package com.easycms.hd.api.response.hseproblem;

import java.util.List;
import java.util.Map;

import com.easycms.hd.api.response.qualityctrl.QualityControlOptionResult;

import lombok.Data;

@Data
public class HseProblemCreatUiDataResult {
	
	/**
	 * 机组
	 */
	private List<String> unit;
	
	/**
	 * 厂房
	 */
	private List<String> wrokshop;
	
	/**
	 * 责任部门
	 */
	private List<HseResponsibleDeptResult> responsibleDept;
	
	/**
	 * 责任班组
	 */
	private List<HseResponsibleDeptResult> responsibleTeam;

	/**
	 * 房间号和标高
	 */
	private List<QualityControlOptionResult> roomLevel;
	/**
	 * 问题类型
	 */
	private Map<String,String> problemType;
	
	private List<HseResponsibleDeptResult> responsibleDepts;
	private List<QuaResponsibleDeptResult> responsibleTeams;
}
