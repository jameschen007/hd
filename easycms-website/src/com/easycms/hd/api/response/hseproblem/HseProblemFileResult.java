package com.easycms.hd.api.response.hseproblem;

import lombok.Data;

@Data
public class HseProblemFileResult {
	private String path;
	private String fileName;
	private String fileType;

}
