package com.easycms.hd.api.response.hseproblem;

import lombok.Data;

@Data
public class HseResponsibleDeptResult {
	
	private String deptId;
	private String deptName;
	

}
