package com.easycms.hd.api.response.hseproblem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycms.hd.api.enpower.response.EnpowerResponseHseCode;
import com.easycms.hd.api.response.qualityctrl.QualityControlOptionResult;

import lombok.Data;

@Data
public class HseProblemCreatUiDataResult2 {
	
	/**
	 * 机组
	 */
	private List<String> unit;
	
	/**
	 * 厂房
	 */
	private List<String> wrokshop;
	
	/**
	 * 责任部门
	 */
	private List<HseResponsibleDeptResult> responsibleDept;
	
	/**
	 * 责任班组
	 */
	private List<QuaResponsibleDeptResult> responsibleTeam;

	/**
	 * 房间号和标高
	 */
	private List<QualityControlOptionResult> roomLevel;
	/**
	 * 问题类型
	 */
	private Map<String,String> problemType;
	/**
	 * 一级安全编码
	 */
	private List<EnpowerResponseHseCode> codeLevel1;
	/**
	 * 二三级安全编码
	 */
	private List<EnpowerResponseHseCode> codeLevel23;
	/**
	 * key normalCheck leaderCheck specialCheck
陈泳君<chen000888@gmail.com>  13:51:31
jsonArray = [{key:normalCheck,value:日常检查}]
	 */
	private List<Map<String,String>> checkTypeList;
	{
		Map<String,String> map1 = new HashMap<String,String>();
		Map<String,String> map2 = new HashMap<String,String>();
		Map<String,String> map3 = new HashMap<String,String>();
		
		map1.put("key", "normalCheck");
		map1.put("value", "日常检查");
		map2.put("key", "leaderCheck");
		map2.put("value", "领导带班检查");
		map3.put("key", "specialCheck");
		map3.put("value", "专项检查");
		
		checkTypeList = new ArrayList<Map<String,String>>();
		checkTypeList.add(map1);
		checkTypeList.add(map2);
		checkTypeList.add(map3);
	}
	/**
	 * key normalCheck leaderCheck specialCheck
陈泳君<chen000888@gmail.com>  13:51:31
jsonArray = [{key:normalCheck,value:日常检查}]
	 */
	private List<Map<String,String>> criticalLevelList;
	{
		//"criticalLevelList":[{"value":"一般","key":"commonly"},{"value":"较大","key":"more"},{"value":"严重","key":"serious"}]}}
		Map<String,String> map1 = new HashMap<String,String>();
		Map<String,String> map2 = new HashMap<String,String>();
		Map<String,String> map3 = new HashMap<String,String>();
		
		map1.put("key", "commonly");
		map1.put("value", "一般");
		map2.put("key", "more");
		map2.put("value", "较大");
		map3.put("key", "serious");
		map3.put("value", "严重");
		
		criticalLevelList = new ArrayList<Map<String,String>>();
		criticalLevelList.add(map1);
		criticalLevelList.add(map2);
		criticalLevelList.add(map3);
	}
}
