package com.easycms.hd.api.enums;

public enum WitnessFlagEnum {	
	/**
	 * 已完成
	 */
	COMPLETED,
	/**
	 * 已发起
	 */
	LAUNCHED,
	/**
	 * 已过期
	 */
	EXPIRED,
	/**
	 * 已过期
	 */
	CANCEL
}
