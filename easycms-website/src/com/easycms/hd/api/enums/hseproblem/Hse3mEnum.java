package com.easycms.hd.api.enums.hseproblem;

/**
 * @author wangzhi
 * @Description: 隐患总数　待整改数　超期未整改
 * @date 2019/7/2 11:21
 */
public enum Hse3mEnum {
    total("total","隐患总数"),
    waitingModify("waitingModify","待整改数"),
    delay("delay","超期未整改数");

    private String code;
    private String msg;

    Hse3mEnum(String code, String msg) {
        this.code = code;
        this.msg = msg ;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
