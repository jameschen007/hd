package com.easycms.hd.api.enums.hseproblem;

import com.easycms.hd.api.enums.hseproblem.underlying.FilterItemTimeFrame;
import com.easycms.hd.api.enums.hseproblem.underlying.TheFilterItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wangzhi
 * @Description: 标题下拉菜单进行筛选项　集合，集合中放的是map对象
 * @date 2019/7/1 15:58
 */
public class DynamicFilterItemList {


    public Map<String, List<TheFilterItem>> getDynamic() {
        return getDynamic(false);
    }

    public Map<String, List<TheFilterItem>> getDynamic(boolean all) {

        /**
         * 以当前时间动态决定范围
         上月
         这个月的早些时候	可能没有	没有的条件：	上周或本周是本月的首周
         上周
         本周早些时候	可能没有	没有的条件：	昨天或今天是周一
         昨天
         今天

         */

        Map<String, List<TheFilterItem>> list = new HashMap<>();
        List<TheFilterItem> created = new ArrayList<>();
        List<TheFilterItem> targetTime = new ArrayList<>();
        List<TheFilterItem> problemStatus = new ArrayList<>();

        int order = 1 ;
//        提出时间
        FilterItemTimeFrame e1 = new FilterItemTimeFrame(DynamicFilterItems.createdAll.getCode(), DynamicFilterItems.createdAll.getMsg());
        if (e1.isDisplay()) {
            e1.setOrder(order++);
            created.add(e1);//提出时间-全部
        }
        FilterItemTimeFrame e2 = new FilterItemTimeFrame(DynamicFilterItems.monthEarly.getCode(), DynamicFilterItems.monthEarly.getMsg());
        if (e2.isDisplay() || all) {
            e2.setOrder(order++);
            created.add(e2);//这个月的早些时候
        }
        FilterItemTimeFrame e3 = new FilterItemTimeFrame(DynamicFilterItems.lastWeek.getCode(), DynamicFilterItems.lastWeek.getMsg());
        if (e3.isDisplay()) {
            e3.setOrder(order++);
            created.add(e3);//上周
        }
        FilterItemTimeFrame e4 = new FilterItemTimeFrame(DynamicFilterItems.weekEarly.getCode(), DynamicFilterItems.weekEarly.getMsg());
        if (e4.isDisplay() || all) {
            e4.setOrder(order++);
            created.add(e4);//本周早些时候
        }
        FilterItemTimeFrame e5 = new FilterItemTimeFrame(DynamicFilterItems.yesterday.getCode(), DynamicFilterItems.yesterday.getMsg());
        if (e5.isDisplay()) {
            e5.setOrder(order++);
            created.add(e5);//昨天
        }
        FilterItemTimeFrame e6 = new FilterItemTimeFrame(DynamicFilterItems.today.getCode(), DynamicFilterItems.today.getMsg());
        if (e6.isDisplay()) {
            e6.setOrder(order++);
            created.add(e6);//今天
        }

        list.put("createTime",created);
//        截止时间
        FilterItemTimeFrame c1 = new FilterItemTimeFrame(DynamicFilterItems.cutOffAll.getCode(), DynamicFilterItems.cutOffAll.getMsg());
        if (c1.isDisplay()) {
            order = 1;
            c1.setOrder(order++);
            targetTime.add(c1);//截止时间-全部
        }
        FilterItemTimeFrame c2 = new FilterItemTimeFrame(DynamicFilterItems.monthEarly.getCode(), DynamicFilterItems.monthEarly.getMsg());
        if (c2.isDisplay() || all) {
            c2.setOrder(order++);
            targetTime.add(c2);//这个月的早些时候
        }
        FilterItemTimeFrame c3 = new FilterItemTimeFrame(DynamicFilterItems.lastWeek.getCode(), DynamicFilterItems.lastWeek.getMsg());
        if (c3.isDisplay()) {
            c3.setOrder(order++);
            targetTime.add(c3);//上周
        }
        FilterItemTimeFrame c4 = new FilterItemTimeFrame(DynamicFilterItems.weekEarly.getCode(), DynamicFilterItems.weekEarly.getMsg());
        if (c4.isDisplay() || all) {
            c4.setOrder(order++);
            targetTime.add(c4);//本周早些时候
        }
        FilterItemTimeFrame c5 = new FilterItemTimeFrame(DynamicFilterItems.yesterday.getCode(), DynamicFilterItems.yesterday.getMsg());
        if (c5.isDisplay()) {
            c5.setOrder(order++);
            targetTime.add(c5);//昨天
        }
        FilterItemTimeFrame c6 = new FilterItemTimeFrame(DynamicFilterItems.today.getCode(), DynamicFilterItems.today.getMsg());
        if (c6.isDisplay()) {
            c6.setOrder(order++);
            targetTime.add(c6);//今天
        }

        list.put("targetTime",targetTime);
//        状态
        problemStatus.add(new TheFilterItem(DynamicFilterItems.problemStatusAll.getCode(), DynamicFilterItems.problemStatusAll.getMsg(),1));//状态-全部
        problemStatus.add(new TheFilterItem(DynamicFilterItems.Need_Handle.getCode(), DynamicFilterItems.Need_Handle.getMsg(),2));//待处理
        problemStatus.add(new TheFilterItem(DynamicFilterItems.Renovating.getCode(), DynamicFilterItems.Renovating.getMsg(),3));//整改中
        problemStatus.add(new TheFilterItem(DynamicFilterItems.Need_Check.getCode(), DynamicFilterItems.Need_Check.getMsg(),4));//待审查
        problemStatus.add(new TheFilterItem(DynamicFilterItems.Need_A_Check.getCode(), DynamicFilterItems.Need_A_Check.getMsg(),5));//待编制审查
        problemStatus.add(new TheFilterItem(DynamicFilterItems.Finish.getCode(), DynamicFilterItems.Finish.getMsg(),6));//已完成
        problemStatus.add(new TheFilterItem(DynamicFilterItems.None.getCode(), DynamicFilterItems.None.getMsg(),7));//不需要处理


        list.put("problemStatus",problemStatus);

        return list;
    }
}
