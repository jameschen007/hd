package com.easycms.hd.api.enums.hseproblem;

public enum HseProblemStatusEnum {
	
	
	/**
	 * 待处理
	 */
	Need_Handle,
	
	
	/**
	 * 整改中
	 */
	Renovating,
	
	/**
	 * 待审查
	 */
	Need_Check,
	/**
	 * 待A审查
	 */
	Need_A_Check,
	
	/**
	 * 已完成
	 */
	Finish,
	
	/**
	 * 不需要处理
	 */
	None

}
