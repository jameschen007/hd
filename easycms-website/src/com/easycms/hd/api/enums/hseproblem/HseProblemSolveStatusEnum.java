package com.easycms.hd.api.enums.hseproblem;

public enum HseProblemSolveStatusEnum {
	
	/**
	 * 待处理
	 */
	Pre,
	
	/**
	 * 已处理
	 */
	Done

}
