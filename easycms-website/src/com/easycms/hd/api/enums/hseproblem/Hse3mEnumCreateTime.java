package com.easycms.hd.api.enums.hseproblem;

/**
 * @author wangzhi
 * @Description: 标题下拉菜单进行筛选项,全部
 * @date 2019/7/1 15:58
 */
public enum Hse3mEnumCreateTime {
//提出时间-全部
//提出时间-全部"),
    createdAll0,
    createdAll1,
    //这个月的早些时候"),
    monthEarly0,
    monthEarly1,
    //上周"),
    lastWeek0,
    lastWeek1,
    //本周早些时候"),
    weekEarly0,
    weekEarly1,
    //昨天"),
    yesterday0,
    yesterday1,
    //今天"),
    today0,
    today1,
}
