package com.easycms.hd.api.enums;

public enum StatusEnum { 
//施工情况的枚举类型	
	/**
	 * 施工中
	 */
	PROGRESSING,
	/**
	 * 已完成
	 */
	COMPLETED,
	/**
	 * 未施工
	 */
	UNPROGRESSING,
	/**
	 * 停滞中
	 */
	PAUSE,
//见证情况的枚举类型	
	/**
	 * 已见证
	 */
	WITNESSED,
	/**
	 * 未见证
	 */
	UNWITNESS,
	/**
	 * 未指派
	 */
	UNASSIGNED,
	/**
	 * 已指派
	 */
	ASSIGNED,
	/**
	 * 不合格
	 */
	UNQUALIFIED,
	/**
	 * 合格
	 */
	QUALIFIED,
	/**
	 * 自动合格
	 */
	AUTO_QUALIFIED,
	/**
	 * 无需见证
	 */
	NONE,
	/**
	 * 未完成的见证
	 */
	UNCOMPLETED
}
