package com.easycms.hd.api.enums;

public enum ProblemTypeEnum { 
	/**
	 * 所有问题
	 */
	ALL,
	/**
	 * 待解决问题
	 */
	NEED_SOLVE, 
	/**
	 * 未能解决
	 */
	COULD_NOT_SOLVED,
	/**
	 * 已经解决
	 */
	SOLVED,
	/**
	 * 发起的问题
	 */
	RAISED_PROBLEM,
	/**
	 * 待确认的问题
	 */
	NEED_CLEAR_PROBLEM,
	/**
	 * 关注的问题
	 */
	WATCHED_PROBLEM
}
