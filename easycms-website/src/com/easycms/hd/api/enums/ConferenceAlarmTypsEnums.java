package com.easycms.hd.api.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycms.hd.api.response.CommonMapResult;

public enum ConferenceAlarmTypsEnums {
    NONE("不提醒", -0L, 0), 
    MIN_1("1分钟", 1 * 60 * 1000L, 1), 
    MIN_5("5分钟", 5 * 60 * 1000L, 2),
    MIN_10("10分钟", 10 * 60 * 1000L, 3),
    MIN_15("15分钟", 15 * 60 * 1000L, 4),
    MIN_30("30分钟", 30 * 60 * 1000L, 5),
    HOUR_1("1小时", 1 * 60 * 60 * 1000L, 6),
    HOUR_2("2小时", 2 * 60 * 60 * 1000L, 7),
    HOUR_3("3小时", 3 * 60 * 60 * 1000L, 8),
    HOUR_4("4小时", 4 * 60 * 60 * 1000L, 9),
    HOUR_5("5小时", 5 * 60 * 60 * 1000L, 10),
    HOUR_6("6小时", 6 * 60 * 60 * 1000L, 11),
    HOUR_7("7小时", 7 * 60 * 60 * 1000L, 12),
    HOUR_8("8小时", 8 * 60 * 60 * 1000L, 13),
    DAY_1("1天", 1 * 24 * 60 * 60 * 1000L, 14),
    DAY_2("2天", 2 * 24 * 60 * 60 * 1000L, 15),
    DAY_3("3天", 3 * 24 * 60 * 60 * 1000L, 16),
    DAY_4("4天", 4 * 24 * 60 * 60 * 1000L, 17),
    DAY_5("5天", 5 * 24 * 60 * 60 * 1000L, 18),
    DAY_6("6天", 6 * 24 * 60 * 60 * 1000L, 19),
    DAY_7("7天", 7 * 24 * 60 * 60 * 1000L, 20)
    ;

    private String name;
    private Long millis;
    private Integer seq;
    ConferenceAlarmTypsEnums(String name, Long millis, Integer seq){
        this.name = name;
        this.millis = millis;
        this.seq = seq;
    }

    public String getName(){
        return this.name;
    }
    
    public Long getMillis(){
    	return this.millis;
    }
    
    public Integer getSeq(){
    	return this.seq;
    }

    /**
     * enum lookup map
     */
    private static final Map<String, String> lookup = new HashMap<String, String>();
    /**
     * enum lookup list
     */
    private static final List<CommonMapResult> mapResult = new ArrayList<CommonMapResult>();

    static {
        for (ConferenceAlarmTypsEnums s : EnumSet.allOf(ConferenceAlarmTypsEnums.class)) {
            lookup.put(s.name(), s.getName());
            CommonMapResult commonMapResult = new CommonMapResult();
            commonMapResult.setKey(s.name());
            commonMapResult.setValue(s.getName());
            commonMapResult.setSeq(s.getSeq());
            mapResult.add(commonMapResult);
        }
    }

    public static  Map<String, String> getMap(){
        return lookup;
    }
    
    public static  List<CommonMapResult> getList(){
    	return mapResult;
    }
    
}
