package com.easycms.hd.api.enums;

public enum WorkStepTypeEnum { 
	/**
	 * 已完成的见证
	 */
	COMPLETED,
	/**
	 * 未完成的见证书
	 */
	UNCOMPLETED
}
