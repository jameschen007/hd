package com.easycms.hd.api.enums.question;

/**
 * 这两个状态是组长回答,问题是否解决的状态
 *
 */
public enum QuestionAnswerStatusDbEnum {
	/**
	 * 仍未解决
	 */
	unsolved
	,
	/**
	 * 已解决
	 */
	solved
;
}
