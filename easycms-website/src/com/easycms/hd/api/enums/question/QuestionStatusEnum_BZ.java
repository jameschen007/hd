package com.easycms.hd.api.enums.question;

/**
 * 用于班长 Form表单使用
 * @author ul-webdev
 *
 */
public enum QuestionStatusEnum_BZ { 
	/**
	 * 待指派
	 */
	NEED_ASSIGN, 
	/**
	 * 待解决
	 */
	NEED_SOLVE, 
	/**
	 * 已解决
	 */
	SOLVED,
}
