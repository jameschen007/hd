package com.easycms.hd.api.enums.question;

public enum QuestionStatusEnum_MY { 
	/**
	 * 已回复（已提供解决方案）
	 */
	MYREPLY, 
	/**
	 * 未解决
	 */
	MYUNSOLVED, 
	/**
	 * 已解决
	 */
	MYSOLVED,
}
