package com.easycms.hd.api.enums.question;

/**
 * 用于技术人员 Form表单使用
 * @author ul-webdev
 *
 */
public enum QuestionStatusEnum_JS {

	/**
	 * 待处理
	 */
	PRE, 
	/**
	 * 已处理
	 */
	DONE, 
	/**
	 * 未能解决的问题
	 */
	NEED_SOLVE, 
	/**
	 * 已经解决
	 */
	SOLVED,
}
