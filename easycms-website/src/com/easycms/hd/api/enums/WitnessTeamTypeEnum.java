package com.easycms.hd.api.enums;

public enum WitnessTeamTypeEnum { 
	/**
	 * 见证组长
	 */
	WITNESS_TEAM,
	/**
	 * 见证组员
	 */
	WITNESS_MEMBER
}