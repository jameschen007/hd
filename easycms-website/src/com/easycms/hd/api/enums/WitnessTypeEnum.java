package com.easycms.hd.api.enums;

public enum WitnessTypeEnum { 
//见证组长所获取的类型		
	/**
	 * 已完成的见证
	 */
	COMPLETED,
	/**
	 * 已发起的见证
	 */
	LAUNCHED, 
	/**
	 * 未完成的见证
	 */
	UNCOMPLETED,
	/**
	 * 未分派
	 */
	UNASSIGN,
//班长所获取的类型	
	/**
	 * 待提交
	 */
	PENDING,
	
//见证员所获取的类型	
	/**
	 * 不合格
	 */
	UNQUALIFIED,
	/**
	 * 合格
	 */
	QUALIFIED,
	
//组长获取见证员见证列表时的类型	
	/**
	 * 未处理
	 */
	UNHANDLED,
	/**
	 * 已处理
	 */
	HANDLED
}
