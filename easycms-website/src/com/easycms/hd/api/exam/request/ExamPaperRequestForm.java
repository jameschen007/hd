package com.easycms.hd.api.exam.request;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ExamPaperRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -4973603438582526873L;
	@ApiParam(value = "培训主板块,例如'基础培训',传入'JCPX'", required = true)
	private String section;
	@ApiParam(value = "培训模块,例如'管道计划',传入'GDJH'", required = false)
	private String module;
	@ApiParam(value = "培训子折叠项,传入对应ID", required = false)
	private String fold;
}
