package com.easycms.hd.api.exam.request;

import java.util.Set;

import lombok.Data;

/**
 * 试卷答案
 *
 */
@Data
public class ExamPaperSolution {
	
	/**
	 * 试题ID
	 */
	private Integer subjectId;
	/**
	 * 试题选项ID
	 */
	private Set<Integer> itemId;

}
