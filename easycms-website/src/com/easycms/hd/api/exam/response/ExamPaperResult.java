package com.easycms.hd.api.exam.response;

import java.util.List;

import lombok.Data;

@Data
public class ExamPaperResult {
	/**
	 * 总分数
	 */
	private Double score;
	/**
	 * 及格分数
	 */
	private Double passScore;
	/**
	 * 考试时长
	 */
	private Long duration;
	/**
	 * 试题的内容
	 */
	private List<ExamPaperItemResult> results;
}
