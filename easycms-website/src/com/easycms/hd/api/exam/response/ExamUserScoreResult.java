package com.easycms.hd.api.exam.response;

import java.util.Date;

import lombok.Data;

@Data
public class ExamUserScoreResult {
	/**
	 * 考试人员
	 */
	private String realname;

	private Integer id;
	/**
	 * 考试日期
	 */
	private Date examDate;
	
	/**
	 * 考试类别
	 */
	private String examType;
	
	/**
	 * 考试得分
	 */
	private String examScore;
	
	/**
	 * 及格情况
	 */
	private String examResult;

}
