package com.easycms.hd.api.statistics;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.enums.QC2MemberTypeEnum;
import com.easycms.hd.api.request.RollingPlanRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsMainResult;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsMainResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsResult;
import com.easycms.hd.api.response.statistics.ProblemStatisticsResult;
import com.easycms.hd.api.response.statistics.RollingPlanStatisticsResult;
import com.easycms.hd.api.response.statistics.StatisticsMainResult;
import com.easycms.hd.api.response.statistics.WitnessStatisticsResult;
import com.easycms.hd.api.service.StatisticsApiService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/statistics")
@Api(value = "StatisticsAPIController", description = "各类统计数据相关的api")
@Slf4j
public class StatisticsAPIController {
	@Autowired
	private UserService userService;
	@Autowired
	private StatisticsApiService statisticsApiService;

	@ResponseBody
	@RequestMapping(value = "/rollingplan", method = RequestMethod.GET)
	@ApiOperation(value = "六级滚动计划统计信息", notes = "用于返回六级滚动计划中的班长和队长所能看到的统计信息")
	public JsonResult<StatisticsMainResult<RollingPlanStatisticsResult>> rollingPlan(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") RollingPlanRequestForm rollingPlanRequestForm) throws Exception {
		log.info("Get rollingplan statistics");
		JsonResult<StatisticsMainResult<RollingPlanStatisticsResult>> result = new JsonResult<StatisticsMainResult<RollingPlanStatisticsResult>>();
		
		User user = null;
		if (null != rollingPlanRequestForm.getUserId()) {
			user = userService.findUserById(rollingPlanRequestForm.getUserId());
		} else {
			user = userService.findUserById(rollingPlanRequestForm.getLoginId());
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		
		StatisticsMainResult<RollingPlanStatisticsResult> rollingPlanMainResult = statisticsApiService.getRollingPlanStatisticsResult(user,rollingPlanRequestForm.getType(),rollingPlanRequestForm.getRoleId());
		result.setResponseResult(rollingPlanMainResult);
		
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/problem", method = RequestMethod.GET)
	@ApiOperation(value = "问题统计信息pre待解决，done已处理待确认，solved已解决，unsolved未解决，neetAssign需要指派", notes = "用于返回六级滚动计划中所产生的问题的统计信息")
	public JsonResult<StatisticsMainResult<ProblemStatisticsResult>> problem(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) @ApiParam(value = "查看自己已外用户问题的用户 ID信息") Integer userId,
			@RequestParam(required = true) @ApiParam(value = "登录用户的ID") Integer loginId,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@RequestParam(required = false) @ApiParam(value = "滚动计划的类型") String type) throws Exception {
		log.info("Get problem statistics");
		JsonResult<StatisticsMainResult<ProblemStatisticsResult>> result = new JsonResult<StatisticsMainResult<ProblemStatisticsResult>>();
		
		User user = null;
		if (null != userId) {
			user = userService.findUserById(userId);
		} else {
			user = userService.findUserById(loginId);
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		StatisticsMainResult<ProblemStatisticsResult> rollingPlanMainResult = statisticsApiService.getProblemStatisticsResult(user,type, roleId);
		result.setResponseResult(rollingPlanMainResult);
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/witness", method = RequestMethod.GET)
	@ApiOperation(value = "见证统计信息", notes = "用于返回六级滚动计划中所产生的见证的统计信息")
	public JsonResult<StatisticsMainResult<WitnessStatisticsResult>> witness(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@RequestParam(required = false) @ApiParam(value = "查看自己已外用户见证的用户 ID信息") Integer userId,
			@RequestParam(required = true) @ApiParam(value = "登录用户的ID") Integer loginId,
			@RequestParam(required = false) @ApiParam(value = "QC2的用户类型") QC2MemberTypeEnum memberType,
			@RequestParam(required = false) @ApiParam(value = "滚动计划的类型") String type) throws Exception {
		JsonResult<StatisticsMainResult<WitnessStatisticsResult>> result = new JsonResult<StatisticsMainResult<WitnessStatisticsResult>>();
		
		User user = null;
		if (null != userId) {
			user = userService.findUserById(userId);
		} else {
			user = userService.findUserById(loginId);
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		
		StatisticsMainResult<WitnessStatisticsResult> rollingPlanMainResult = statisticsApiService.getWitnessStatisticsResult(user,type, memberType,roleId);
		result.setResponseResult(rollingPlanMainResult);
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/conference", method = RequestMethod.GET)
	@ApiOperation(value = "会议统计信息", notes = "用于返回会议模块的会议及通知的统计信息")
	public JsonResult<ConferenceStatisticsMainResult<ConferenceStatisticsResult>> conference(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) @ApiParam(value = "登录用户的ID") Integer loginId) throws Exception {
		JsonResult<ConferenceStatisticsMainResult<ConferenceStatisticsResult>> result = new JsonResult<ConferenceStatisticsMainResult<ConferenceStatisticsResult>>();
		ConferenceStatisticsMainResult<ConferenceStatisticsResult> conferenceStatisticsMainResult = statisticsApiService.getConferenceStatisticsResult(loginId);
		result.setResponseResult(conferenceStatisticsMainResult);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/material", method = RequestMethod.GET)
	@ApiOperation(value = "物资日志统计信息", notes = "用于返回物资模块的日报出入库统计信息 -- 当前为假数据")
	public JsonResult<MaterialStatisticsResult> material(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) @ApiParam(value = "登录用户的ID") Integer loginId) throws Exception {
		JsonResult<MaterialStatisticsResult> result = new JsonResult<MaterialStatisticsResult>();
		MaterialStatisticsResult materialStatisticsResult = statisticsApiService.getMaterialStatisticsResult(loginId);
		result.setResponseResult(materialStatisticsResult);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/material/department", method = RequestMethod.GET)
	@ApiOperation(value = "物资日志中部门出库统计信息", notes = "用于返回物资模块的日报出库模块中单个部门对应统计信息 -- 当前为假数据")
	public JsonResult<MaterialStatisticsMainResult> materialDepartment(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) @ApiParam(value = "登录用户的ID") Integer loginId) throws Exception {
		JsonResult<MaterialStatisticsMainResult> result = new JsonResult<MaterialStatisticsMainResult>();
		MaterialStatisticsMainResult materialStatisticsMainResult = statisticsApiService.getMaterialStatisticsMainResult(loginId);
		result.setResponseResult(materialStatisticsMainResult);
		return result;
	}
}
