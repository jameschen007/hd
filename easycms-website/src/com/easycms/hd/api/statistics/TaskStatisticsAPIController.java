//package com.easycms.hd.api.statistics;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//
//
//import com.easycms.hd.api.domain.TaskStatisticsResult;
//import com.easycms.hd.api.domain.TaskStatusStatisticsResult;
//import com.easycms.hd.api.domain.TaskTypeStatisticsResult;
//import com.easycms.hd.api.response.JsonResult;
//
//import com.easycms.hd.plan.domain.TaskHyperbolaResult;
//import com.easycms.hd.plan.domain.TaskHyperbolaResultMap;
//import com.easycms.hd.plan.service.StatisticsRollingPlanService;
//import com.easycms.hd.price.service.PriceService;
//import com.easycms.hd.statistics.domain.TaskStatistics;
//import com.easycms.hd.statistics.util.StatisticsUtil;
//import com.easycms.hd.user.service.HDUserService;
//import com.easycms.hd.variable.service.VariableSetService;
//import com.easycms.management.user.domain.Department;
//import com.easycms.management.user.domain.Role;
//import com.easycms.management.user.domain.User;
//import com.easycms.management.user.service.DepartmentService;
//import com.easycms.management.user.service.RoleService;
//import com.easycms.management.user.service.UserService;
//
//@Controller
//@RequestMapping("/hdxt/api/statistics")
//public class TaskStatisticsAPIController {
//	private static final Logger logger = Logger
//			.getLogger(TaskStatisticsAPIController.class);
//	private static String ASSIGN_TYPE = "assigntype";
//	private static String CONSTEAM = "consteam";
//	private static String TEAM = "team";
//	private static String END_MAN = "endman";
//	private static String WELD_DISTINGUISH = "welddistinguish";
//	private static String HYSTERESIS = "hysteresis";
//	@Autowired
//	private StatisticsRollingPlanService statisticsRollingPlanService;
//	@Autowired
//	private VariableSetService variableSetService;
//	@Autowired
//	private RoleService roleService;
//	@Autowired
//	private DepartmentService departmentService;
//	@Autowired
//	private PriceService priceService;
//	@Autowired
//	private UserService userService;
//
//	@Autowired
//	private HDUserService hdUserService;
//
//	@ResponseBody
//	@RequestMapping(value = "/task", method = RequestMethod.GET)
//	public JsonResult<List<TaskStatisticsResult>> taskList(
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//		logger.info("Get task");
//		JsonResult<List<TaskStatisticsResult>> result = new JsonResult<List<TaskStatisticsResult>>();
//		String weldHK = variableSetService.findValueByKey("weldHK",
//				WELD_DISTINGUISH);
//		String weldZJ = variableSetService.findValueByKey("weldZJ",
//				WELD_DISTINGUISH);
//		Department department = departmentService.findByName(variableSetService.findValueByKey(CONSTEAM, ASSIGN_TYPE));
//		
//		List<TaskStatisticsResult> taskStatisticsResult = new ArrayList<TaskStatisticsResult>();
//		if (null != department){
//			for (Department d : department.getChildren()) {
//				TaskStatisticsResult task = new TaskStatisticsResult();
//				List<TaskStatistics> tasks = new ArrayList<TaskStatistics>();
//				if (null != d) {
//					TaskStatistics taskHK = new TaskStatistics();
//					taskHK.setComplate(statisticsRollingPlanService.getRollingPlanComplate(
//							d.getId() + "", weldHK));
//					taskHK.setSurplus(statisticsRollingPlanService.getRollingPlanSurplus(
//							d.getId() + "", weldHK));
//					taskHK.setTotal(statisticsRollingPlanService.getRollingPlanTotal(
//							d.getId() + "", weldHK));
//					taskHK.setType("焊口");
//					
//					if (null != taskHK) {
//						if (0 != taskHK.getTotal()) {
//							taskHK.setPercent((Double.valueOf(taskHK.getComplate()))
//									/ ((Double.valueOf(taskHK.getTotal()))) * 100);
//						}
//					}
//					
//					TaskStatistics taskZJ = new TaskStatistics();
//					taskZJ.setComplate(statisticsRollingPlanService.getRollingPlanComplate(
//							d.getId() + "", weldZJ));
//					taskZJ.setSurplus(statisticsRollingPlanService.getRollingPlanSurplus(
//							d.getId() + "", weldZJ));
//					taskZJ.setTotal(statisticsRollingPlanService.getRollingPlanTotal(
//							d.getId() + "", weldZJ));
//					taskZJ.setType("支架");
//					
//					if (null != taskZJ) {
//						if (0 != taskZJ.getTotal()) {
//							taskZJ.setPercent((Double.valueOf(taskZJ.getComplate()))
//									/ ((Double.valueOf(taskZJ.getTotal()))) * 100);
//						}
//					}
//					
//					tasks.add(taskZJ);
//					tasks.add(taskHK);
//				}
//				
//				task.setDepartment(d);
//				task.setTasks(tasks);
//				taskStatisticsResult.add(task);
//			}
//		} else {
//			result.setCode("-1002");
//			result.setMessage("没有任何班组信息存在!");
//			return result;
//		}
//
//		result.setResponseResult(taskStatisticsResult);
//		return result;
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/teamgroup/task", method = RequestMethod.GET)
//	public JsonResult<List<TaskStatisticsResult>> taskListByTeamGroup(
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//		logger.info("Get task");
//
//		JsonResult<List<TaskStatisticsResult>> result = new JsonResult<List<TaskStatisticsResult>>();
//
//		String loginId = request.getParameter("loginId");
//
//		User user = userService.findUserById(Integer.parseInt(loginId));
//
//		String weldHK = variableSetService.findValueByKey("weldHK",
//				WELD_DISTINGUISH);
//		String weldZJ = variableSetService.findValueByKey("weldZJ",
//				WELD_DISTINGUISH);
//		String team = variableSetService.findValueByKey(TEAM, ASSIGN_TYPE);
//		String endMan = variableSetService.findValueByKey(END_MAN, ASSIGN_TYPE);
//		
//		boolean teamFlag = false;
//		List<Role> roles = user.getRoles();
//		for (Role r : roles){
//			if (r.getName().equals(team)){
//				teamFlag = true;
//				break;
//			}
//		}
//		
//		if (!teamFlag){
//			result.setCode("-1002");
////			result.setMessage("你不是[" + team + "]");
//			return result;
//		}
//
//		Department dpment = user.getDepartment();
//		if (null == dpment){
//			result.setCode("-1002");
//			result.setMessage("未找到你的部门。");
//			return result;
//		}
//
//		List<User> users = dpment.getUsers();
//		List<User> teamUsers = new ArrayList<User>();
//		List<User> endManUsers = new ArrayList<User>();
//		
//		for (User u : users){
//			for (Role r : u.getRoles()){
//				if (r.getName().equals(team)){
//					teamUsers.add(u);
//				}
//				if (r.getName().equals(endMan)){
//					endManUsers.add(u);
//				}
//			}
//		}
//
//		List<TaskStatisticsResult> taskStatisticsResult = new ArrayList<TaskStatisticsResult>();
//		for (User u : endManUsers) {
//			TaskStatisticsResult task = new TaskStatisticsResult();
//			List<TaskStatistics> tasks = new ArrayList<TaskStatistics>();
//			TaskStatistics taskHK = new TaskStatistics();
//			if (hdUserService.isClasz(user.getId())){
//				taskHK.setComplate(statisticsRollingPlanService.getRollingPlanCompleteByConsEndmanAndType(
//						u.getId() + "", weldHK));
//				taskHK.setSurplus(statisticsRollingPlanService.getRollingPlanUnCompleteByConsEndmanAndType(
//						u.getId() + "", weldHK));
//				taskHK.setTotal(statisticsRollingPlanService.getRollingPlanTotalByConsEndman(
//						u.getId() + "", weldHK));
//			} else if(hdUserService.isGroup(user.getId())){
//				taskHK.setComplate(statisticsRollingPlanService.getRollingPlanComplate(
//						u.getId() + "", weldHK));
//				taskHK.setSurplus(statisticsRollingPlanService.getRollingPlanSurplus(
//						u.getId() + "", weldHK));
//				taskHK.setTotal(statisticsRollingPlanService.getRollingPlanTotal(
//						u.getId() + "", weldHK));
//			}
//			taskHK.setType("焊口");
//
//			if (null != taskHK) {
//				if (0 != taskHK.getTotal()) {
//					taskHK.setPercent((Double.valueOf(taskHK.getComplate()))
//							/ ((Double.valueOf(taskHK.getTotal()))) * 100);
//				}
//			}
//
//			TaskStatistics taskZJ = new TaskStatistics();
//			
//			if (hdUserService.isClasz(user.getId())){
//				taskZJ.setComplate(statisticsRollingPlanService.getRollingPlanCompleteByConsEndmanAndType(
//						u.getId() + "", weldZJ));
//				taskZJ.setSurplus(statisticsRollingPlanService.getRollingPlanUnCompleteByConsEndmanAndType(
//						u.getId() + "", weldZJ));
//				taskZJ.setTotal(statisticsRollingPlanService.getRollingPlanTotalByConsEndman(
//						u.getId() + "", weldZJ));
//			} else if(hdUserService.isGroup(user.getId())){
//				taskZJ.setComplate(statisticsRollingPlanService.getRollingPlanComplate(
//						u.getId() + "", weldZJ));
//				taskZJ.setSurplus(statisticsRollingPlanService.getRollingPlanSurplus(
//						u.getId() + "", weldZJ));
//				taskZJ.setTotal(statisticsRollingPlanService.getRollingPlanTotal(
//						u.getId() + "", weldZJ));
//			}
//			taskZJ.setType("支架");
//
//			if (null != taskZJ) {
//				if (0 != taskZJ.getTotal()) {
//					taskZJ.setPercent((Double.valueOf(taskZJ.getComplate()))
//							/ ((Double.valueOf(taskZJ.getTotal()))) * 100);
//				}
//			}
//
//			tasks.add(taskZJ);
//			tasks.add(taskHK);
//			
//			Department dept = new Department();
//			dept.setId(u.getId());
//			dept.setName(u.getRealname());
//			
//			task.setDepartment(dept);
//			task.setTasks(tasks);
//
//			taskStatisticsResult.add(task);
//		}
//
//		result.setResponseResult(taskStatisticsResult);
//		return result;
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/task/status", method = RequestMethod.GET)
//	public JsonResult<TaskStatusStatisticsResult> taskStatusList(
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//		logger.info("Get task status");
//		JsonResult<TaskStatusStatisticsResult> result = new JsonResult<TaskStatusStatisticsResult>();
//		String loginId = request.getParameter("loginId");
//		User user = userService.findUserById(Integer.parseInt(loginId));
//
//		if (loginId == null || loginId == "") {
//			result.setCode("-1001");
//			result.setMessage("login id is required");
//			return result;
//		}
//		
//		
//		
//		if(hdUserService.isClasz(user.getId())){
//			  Department dept = user.getDepartment();
//		      String[] ids = new String[1];
//		      if (dept != null) {
//		        ids[0] = dept.getId() + "";
//		      }
//		      
//			return getStatusListByClasz(result, request, ids);
//		}else if(hdUserService.isGroup(user.getId())){
//		   String[] ids = {user.getId()+""};
//			return getStatusListByGroup(result, request, ids);
//		}else{
//		  return getStatusListAll(result, request);
//		}
//	}
//
//	private JsonResult<TaskStatusStatisticsResult> getStatusListByGroup(
//			JsonResult<TaskStatusStatisticsResult> returnResult,
//			HttpServletRequest request, String[] ids) {
//
//		String date = request.getParameter("taskDate");
//		String category = request.getParameter("category");
//		logger.debug("[params] ==> [date]:" + date + " [category]:" + category);
//
//		if (null == date || date.equals("")) {
//			date = StatisticsUtil.getDate(0);
//		}
//
//		if (null == category || category.equals("")) {
//			category = "dateCurrent";
//		} else {
//			if (!category.equals("dateYear") && !category.equals("dateWeek")
//					&& !category.equals("dateMonth")
//					&& !category.equals("dateAfter")
//					&& !category.equals("dateCurrent")
//					&& !category.equals("dateBefore")) {
//				returnResult.setCode("-1002");
//				returnResult
//						.setMessage("错误的数据category,只能是[dateYear, dateWeek, dateMonth, dateAfter, dateCurrent, dateBefore]中有一种");
//				return returnResult;
//			}
//		}
//
//		Integer hysteresis = 2;
//		try {
//			hysteresis = Integer.parseInt(variableSetService.findValueByKey(
//					"hysteresis", HYSTERESIS));
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			hysteresis = 2;
//		}
//
//		Map<String, String> typeMap = new HashMap<String, String>();
//		typeMap.put(
//				variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH),
//				"焊口");
//		typeMap.put(
//				variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH),
//				"支架");
//
//		Long[] statusArray = { 0l, 1l, 2l, 3l, 4l, 5l, 6l };
//
//		TaskStatusStatisticsResult taskStatusStatisticsResult = new TaskStatusStatisticsResult();
//		if (category.equals("dateAfter") || category.equals("dateCurrent")
//				|| category.equals("dateBefore")) {
//			
//			if (category.equals("dateAfter")){
//				date = StatisticsUtil.getDate(1);
//			} else if (category.equals("dateBefore")){
//				date = StatisticsUtil.getDate(-1);
//			}
//			
//			logger.info("----------------------------" + date);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusDayStatus0(entry.getKey(),
////										date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus1ByGroup(entry.getKey(),
//										date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus2ByGroup(entry.getKey(),
//										date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus6ByGroup(entry.getKey(),
//										new Date(), hysteresis, ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus4ByGroup(entry.getKey(),
//										date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusDayStatus5(entry.getKey(),
////										date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus3ByGroup(entry.getKey(),
//										date, ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateWeek")) {
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusWeekStatus0(
////										entry.getKey(), new Date(), ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus1ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus2ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus6ByGroup(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus4ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusWeekStatus5(
////										entry.getKey(), new Date(), ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus3ByGroup(
//										entry.getKey(), new Date(), ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateMonth")) {
//			date = date.substring(0, 7);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusMonthStatus0(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus1ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus2ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus6ByGroup(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus4ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusMonthStatus5(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5ByGroup(
//										entry.getKey(), new Date(), ids);
//					}  else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus3ByGroup(
//										entry.getKey(), date, ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateYear")) {
//			date = date.substring(0, 4);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusYearStatus0(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0ByGroup(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus1ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus2ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus6ByGroup(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus4ByGroup(
//										entry.getKey(), date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusYearStatus5(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5ByGroup(
//										entry.getKey(), new Date(), ids);
//					}  else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus3ByGroup(
//										entry.getKey(), date, ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else {
//			return null;
//		}
//
//		returnResult.setResponseResult(taskStatusStatisticsResult);
//		return returnResult;
//	}
//
//	private JsonResult<TaskStatusStatisticsResult> getStatusListByClasz(
//			JsonResult<TaskStatusStatisticsResult> returnResult,
//			HttpServletRequest request, String[] ids) {
//
//		String date = request.getParameter("taskDate");
//		String category = request.getParameter("category");
//		logger.debug("[params] ==> [date]:" + date + " [category]:" + category);
//
//		if (null == date || date.equals("")) {
//			date = StatisticsUtil.getDate(0);
//		}
//
//		if (null == category || category.equals("")) {
//			category = "dateCurrent";
//		} else {
//			if (!category.equals("dateYear") && !category.equals("dateWeek")
//					&& !category.equals("dateMonth")
//					&& !category.equals("dateAfter")
//					&& !category.equals("dateCurrent")
//					&& !category.equals("dateBefore")) {
//				returnResult.setCode("-1002");
//				returnResult
//						.setMessage("错误的数据category,只能是[dateYear, dateWeek, dateMonth, dateAfter, dateCurrent, dateBefore]中有一种");
//				return returnResult;
//			}
//		}
//
//		Integer hysteresis = 2;
//		try {
//			hysteresis = Integer.parseInt(variableSetService.findValueByKey(
//					"hysteresis", HYSTERESIS));
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			hysteresis = 2;
//		}
//
//		Map<String, String> typeMap = new HashMap<String, String>();
//		typeMap.put(
//				variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH),
//				"焊口");
//		typeMap.put(
//				variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH),
//				"支架");
//
//		Long[] statusArray = { 0l, 1l, 2l, 3l, 4l, 5l, 6l };
//
//		TaskStatusStatisticsResult taskStatusStatisticsResult = new TaskStatusStatisticsResult();
//		if (category.equals("dateAfter") || category.equals("dateCurrent")
//				|| category.equals("dateBefore")) {
//			
//			if (category.equals("dateAfter")){
//				date = StatisticsUtil.getDate(1);
//			} else if (category.equals("dateBefore")){
//				date = StatisticsUtil.getDate(-1);
//			}
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusDayStatus0(entry.getKey(),
////										date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus1(entry.getKey(),
//										date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus2(entry.getKey(),
//										date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus6(entry.getKey(),
//										new Date(), hysteresis, ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus4ByClasz(entry.getKey(),
//										date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusDayStatus5(entry.getKey(),
////										date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus3(entry.getKey(),
//										date, ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateWeek")) {
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusWeekStatus0(
////										entry.getKey(), new Date(), ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus1(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus2(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus6(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus4ByClasz(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusWeekStatus5(
////										entry.getKey(), new Date(), ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus3(
//										entry.getKey(), new Date(), ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateMonth")) {
//			date = date.substring(0, 7);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusMonthStatus0(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus1(
//										entry.getKey(), date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus2(
//										entry.getKey(), date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus6(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus4ByClasz(
//										entry.getKey(), date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusMonthStatus5(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5(
//										entry.getKey(), new Date(), ids);
//					}  else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus3(
//										entry.getKey(), date, ids);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateYear")) {
//			date = date.substring(0, 4);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusYearStatus0(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus0(
//										entry.getKey(), new Date(), ids);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus1(
//										entry.getKey(), date, ids);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus2(
//										entry.getKey(), date, ids);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus6(
//										entry.getKey(), new Date(), hysteresis,
//										ids);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus4ByClasz(
//										entry.getKey(), date, ids);
//					} else if (status == 5l) {
//						statusString = "处理中";
////						result = statisticsRollingPlanService
////								.getRollingPlanStatusYearStatus5(
////										entry.getKey(), date, ids);
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusAllStatus5(
//										entry.getKey(), new Date(), ids);
//					}  else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus3(
//										entry.getKey(), date, ids);
//						
//						System.out.println("---------------------" + result);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else {
//			return null;
//		}
//
//		returnResult.setResponseResult(taskStatusStatisticsResult);
//		return returnResult;
//	}
//
//	private JsonResult<TaskStatusStatisticsResult> getStatusListAll(
//			JsonResult<TaskStatusStatisticsResult> returnResult,
//			HttpServletRequest request) {
//
//		String date = request.getParameter("taskDate");
//		String category = request.getParameter("category");
//		logger.debug("[params] ==> [date]:" + date + " [category]:" + category);
//
//		if (null == date || date.equals("")) {
//			date = StatisticsUtil.getDate(0);
//		}
//
//		if (null == category || category.equals("")) {
//			category = "dateCurrent";
//		} else {
//			if (!category.equals("dateYear") && !category.equals("dateWeek")
//					&& !category.equals("dateMonth")
//					&& !category.equals("dateAfter")
//					&& !category.equals("dateCurrent")
//					&& !category.equals("dateBefore")) {
//				returnResult.setCode("-1002");
//				returnResult
//						.setMessage("错误的数据category,只能是[dateYear, dateWeek, dateMonth, dateAfter, dateCurrent, dateBefore]中有一种");
//				return returnResult;
//			}
//		}
//
//		Integer hysteresis = 2;
//		try {
//			hysteresis = Integer.parseInt(variableSetService.findValueByKey(
//					"hysteresis", HYSTERESIS));
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//			hysteresis = 2;
//		}
//
//		Map<String, String> typeMap = new HashMap<String, String>();
//		typeMap.put(
//				variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH),
//				"焊口");
//		typeMap.put(
//				variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH),
//				"支架");
//
//		Long[] statusArray = { 0l, 1l, 2l, 3l, 4l, 5l, 6l };
//
//		TaskStatusStatisticsResult taskStatusStatisticsResult = new TaskStatusStatisticsResult();
//		if (category.equals("dateAfter") || category.equals("dateCurrent")
//				|| category.equals("dateBefore")) {
//			if (category.equals("dateAfter")){
//				date = StatisticsUtil.getDate(1);
//			} else if (category.equals("dateBefore")){
//				date = StatisticsUtil.getDate(-1);
//			}
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus0(entry.getKey(),
//										date);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus1(entry.getKey(),
//										date);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus2(entry.getKey(),
//										date);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus3(entry.getKey(),
//										date);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus6(entry.getKey(),
//										new Date(), hysteresis);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus4(entry.getKey(),
//										date);
//					} else if (status == 5l) {
//						statusString = "处理中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusDayStatus5(entry.getKey(),
//										date);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateWeek")) {
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus0(
//										entry.getKey(), new Date());
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus1(
//										entry.getKey(), new Date());
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus2(
//										entry.getKey(), new Date());
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus3(
//										entry.getKey(), new Date());
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus6(
//										entry.getKey(), new Date(), hysteresis);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus4(
//										entry.getKey(), new Date());
//					} else if (status == 5l) {
//						statusString = "处理中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusWeekStatus5(
//										entry.getKey(), new Date());
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateMonth")) {
//			date = date.substring(0, 7);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus0(
//										entry.getKey(), date);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus1(
//										entry.getKey(), date);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus2(
//										entry.getKey(), date);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus3(
//										entry.getKey(), date);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus6(
//										entry.getKey(), new Date(), hysteresis);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus4(
//										entry.getKey(), date);
//					} else if (status == 5l) {
//						statusString = "处理中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusMonthStatus5(
//										entry.getKey(), date);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else if (category.equals("dateYear")) {
//			date = date.substring(0, 4);
//			
//			List<TaskTypeStatisticsResult> taskTypeStatisticsResultList = new ArrayList<TaskTypeStatisticsResult>();
//			Iterator<Entry<String, String>> typeMapIter = typeMap.entrySet()
//					.iterator();
//			while (typeMapIter.hasNext()) {
//				Entry<String, String> entry = typeMapIter.next();
//				TaskTypeStatisticsResult taskTypeStatisticsResult = new TaskTypeStatisticsResult();
//				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
//				for (Long status : statusArray) {
//					String statusString = null;
//					Long result = 0l;
//					Map<String, String> resultMap = new HashMap<String, String>();
//					if (status == 0l) {
//						statusString = "未分配";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus0(
//										entry.getKey(), date);
//					} else if (status == 1l) {
//						statusString = "施工中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus1(
//										entry.getKey(), date);
//					} else if (status == 2l) {
//						statusString = "完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus2(
//										entry.getKey(), date);
//					} else if (status == 3l) {
//						statusString = "未完成";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus3(
//										entry.getKey(), date);
//					} else if (status == 6l) {
//						statusString = "滞后";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus6(
//										entry.getKey(), new Date(), hysteresis);
//					} else if (status == 4l) {
//						statusString = "计划";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus4(
//										entry.getKey(), date);
//					} else if (status == 5l) {
//						statusString = "处理中";
//						result = statisticsRollingPlanService
//								.getRollingPlanStatusYearStatus5(
//										entry.getKey(), date);
//					}
//					resultMap.put("type", status + "");
//					resultMap.put("status", statusString);
//					resultMap.put("result", result + "");
//					resultList.add(resultMap);
//				}
//				taskTypeStatisticsResult.setResult(resultList);
//				taskTypeStatisticsResult.setType(entry.getValue());
//				taskTypeStatisticsResultList.add(taskTypeStatisticsResult);
//			}
//			taskStatusStatisticsResult.setCategory(category);
//			taskStatusStatisticsResult.setRetuls(taskTypeStatisticsResultList);
//		} else {
//			return null;
//		}
//
//		returnResult.setResponseResult(taskStatusStatisticsResult);
//		return returnResult;
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/task/hyperbola", method = RequestMethod.GET)
//	public JsonResult<List<TaskHyperbolaResultMap>> taskHyperbolaList(
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//		logger.info("Get task hyperbola");
//		return getHyperbolaListByUser(request);
//	}
//
//	private JsonResult<List<TaskHyperbolaResultMap>> getHyperbolaListByUser(
//			HttpServletRequest request) {
//		JsonResult<List<TaskHyperbolaResultMap>> result = new JsonResult<List<TaskHyperbolaResultMap>>();
//
//		String month = StatisticsUtil.getMonth();
//		String loginId = request.getParameter("loginId");
//
//		if (loginId == null || loginId == "") {
//			result.setCode("-1001");
//			result.setMessage("login id is required");
//			return result;
//		}
//
//		User user = userService.findUserById(Integer.parseInt(loginId));
//		Department department = user.getDepartment();
//
//		
//		if (!user.isDefaultAdmin() && department == null) {
//			return result;
//		}
//		String[] ids = {department.getId() + ""};
//		
//		String weldHK = variableSetService.findValueByKey("weldHK",
//				WELD_DISTINGUISH);
//		String weldZJ = variableSetService.findValueByKey("weldZJ",
//				WELD_DISTINGUISH);
//
//		List<TaskHyperbolaResultMap> taskHyperbolaResultList = new ArrayList<TaskHyperbolaResultMap>();
//
//		TaskHyperbolaResultMap taskHyperbolaResult = new TaskHyperbolaResultMap();
//
//		int dateOfMonth = StatisticsUtil.datesOfMonth(StatisticsUtil.getMonth());
//		
//		List<TaskHyperbolaResult> weldHKResult = null;
//		if (user.isDefaultAdmin()) {
//			weldHKResult = StatisticsUtil
//					.objectToTaskHyperbolaResult(statisticsRollingPlanService
//							.getRollingPlanTaskHyperbola(month, weldHK), dateOfMonth);
//		} else {
//			weldHKResult = StatisticsUtil
//					.objectToTaskHyperbolaResult(statisticsRollingPlanService
//							.getRollingPlanTaskHyperbola(month, weldHK,
//									ids), dateOfMonth);
//		}
//
//		taskHyperbolaResult.setType("焊口");
//		taskHyperbolaResult.setResult(weldHKResult);
//		taskHyperbolaResult.setDate(month);
//		taskHyperbolaResultList.add(taskHyperbolaResult);
//
//		List<TaskHyperbolaResult> weldZJResult = null;
//		if (user.isDefaultAdmin()) {
//			weldZJResult = StatisticsUtil
//					.objectToTaskHyperbolaResult(statisticsRollingPlanService
//							.getRollingPlanTaskHyperbola(month, weldZJ), dateOfMonth);
//		} else {
//			weldZJResult = StatisticsUtil
//					.objectToTaskHyperbolaResult(statisticsRollingPlanService
//							.getRollingPlanTaskHyperbola(month, weldZJ,
//									ids), dateOfMonth);
//		}
//
//		taskHyperbolaResult = new TaskHyperbolaResultMap();
//		taskHyperbolaResult.setType("支架");
//		taskHyperbolaResult.setResult(weldZJResult);
//		taskHyperbolaResult.setDate(month);
//		taskHyperbolaResultList.add(taskHyperbolaResult);
//
//		result.setResponseResult(taskHyperbolaResultList);
//		return result;
//	}
//
//	@SuppressWarnings("unused")
//	private JsonResult<List<TaskHyperbolaResultMap>> getHyperbolaListAll(
//			HttpServletRequest request) {
//		JsonResult<List<TaskHyperbolaResultMap>> result = new JsonResult<List<TaskHyperbolaResultMap>>();
//
//		String month = StatisticsUtil.getMonth();
//
//		String weldHK = variableSetService.findValueByKey("weldHK",
//				WELD_DISTINGUISH);
//		String weldZJ = variableSetService.findValueByKey("weldZJ",
//				WELD_DISTINGUISH);
//
//		List<TaskHyperbolaResultMap> taskHyperbolaResultList = new ArrayList<TaskHyperbolaResultMap>();
//
//		TaskHyperbolaResultMap taskHyperbolaResult = new TaskHyperbolaResultMap();
//
//		int dateOfMonth = StatisticsUtil.datesOfMonth(StatisticsUtil.getMonth());
//		
//		List<TaskHyperbolaResult> weldHKResult = null;
//		weldHKResult = StatisticsUtil
//				.objectToTaskHyperbolaResult(statisticsRollingPlanService
//						.getRollingPlanTaskHyperbola(month, weldHK), dateOfMonth);
//
//		taskHyperbolaResult.setType("焊口");
//		taskHyperbolaResult.setResult(weldHKResult);
//		taskHyperbolaResult.setDate(month);
//		taskHyperbolaResultList.add(taskHyperbolaResult);
//
//		List<TaskHyperbolaResult> weldZJResult = null;
//		weldZJResult = StatisticsUtil
//				.objectToTaskHyperbolaResult(statisticsRollingPlanService
//						.getRollingPlanTaskHyperbola(month, weldZJ), dateOfMonth);
//
//		taskHyperbolaResult = new TaskHyperbolaResultMap();
//		taskHyperbolaResult.setType("支架");
//		taskHyperbolaResult.setResult(weldZJResult);
//		taskHyperbolaResult.setDate(month);
//		taskHyperbolaResultList.add(taskHyperbolaResult);
//
//		result.setResponseResult(taskHyperbolaResultList);
//		return result;
//	}
//}
