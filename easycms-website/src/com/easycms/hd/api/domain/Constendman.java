package com.easycms.hd.api.domain;

import java.util.List;

import com.easycms.management.user.domain.User;

public class Constendman {
	private Integer id;
	private String name;
	List<User> users;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
}
