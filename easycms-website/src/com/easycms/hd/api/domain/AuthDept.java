package com.easycms.hd.api.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class AuthDept implements Serializable{
	private static final long serialVersionUID = 5027901730320228346L;
	private Integer id;
	private String name;
}
