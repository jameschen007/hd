package com.easycms.hd.api.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class AuthModule implements Serializable{
	private static final long serialVersionUID = 2480177962782148936L;
	private Integer id;
	private String name;
	private String type;
}
