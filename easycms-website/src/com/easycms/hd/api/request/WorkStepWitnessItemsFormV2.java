package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class WorkStepWitnessItemsFormV2 implements Serializable {
	private static final long serialVersionUID = 8399359361033529614L;
	@ApiParam(value = "workstep ID号s", required = true)
	private Integer[] ids;
	@ApiParam(value = "见证地址", required = true)
	private String witnessaddress;
	@ApiParam(value = "见证日期", required = true)
	private String witnessdate;
	
	public WorkStepWitnessItemsFormV2() {}
/**	
  	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private Integer qcsign;
	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private String qcman;
	**/
}
