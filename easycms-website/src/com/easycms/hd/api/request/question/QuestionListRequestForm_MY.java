package com.easycms.hd.api.request.question;

import java.io.Serializable;

import com.easycms.hd.api.enums.question.QuestionStatusEnum_MY;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QuestionListRequestForm_MY extends BasePagenationRequestForm implements Serializable{

	private static final long serialVersionUID = -6892963500085316925L;

	@ApiParam(value = "问题状态，MYREPLY已反馈、MYUNSOLVED未解决、MYSOLVED已解决", required = true)
	QuestionStatusEnum_MY questionStatus;
}
