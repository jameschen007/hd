package com.easycms.hd.api.request.base;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -3843225729688347406L;
	@ApiParam(value = "登录用户ID",required=true,defaultValue="516")
	Integer loginId;
	@ApiParam(value = "登录用户当前选择角色id",required=false,defaultValue="516")
	Integer roleId;
}
