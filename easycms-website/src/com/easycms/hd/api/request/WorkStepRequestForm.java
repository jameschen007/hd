package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.WorkStepTypeEnum;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class WorkStepRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = -6246784548858362417L;
	@ApiParam(value = "滚动计划的类型", required = true)
	String type;
	@ApiParam(value = "请求分页的类型", required = true)
	WorkStepTypeEnum status;
	@ApiParam(value = "查看自己已外用户滚动计划的用户 ID", required = false)
	Integer userId;
}
