package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.MaterialTypeEnum;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class MaterialPageRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = -8970875632128129532L;
	@ApiParam(value = "类型", required = false)
	private MaterialTypeEnum type;
	@ApiParam(value = "部门ID", required = false)
	private Integer departmentId;
	
}
