package com.easycms.hd.api.request;

import java.io.Serializable;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class MaterialExitRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 5201402934146632732L;
	
	@ApiParam(value = "退库单号", required = false)
	private String[] exitNo;
	@ApiParam(value = "货位", required = false)
	private String location;
	@ApiParam(value = "仓库", required = false)
	private String warehouse;
	
}
