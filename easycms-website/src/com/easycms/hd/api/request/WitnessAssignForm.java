package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Set;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WitnessAssignForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 8270628938150008073L;
	@ApiParam(value = "见证组长需要分派的见证信息ID号", required = true)
	Set<Integer> ids;
	@ApiParam(value = "见证组组员ID号, 即QC1或者QC2见证组员的ID号.", required = true)
	Integer memberId;
}
