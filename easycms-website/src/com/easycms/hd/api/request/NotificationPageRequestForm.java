package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotificationPageRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = -9152524426015238170L;
	@ApiParam(value = "通知的存档类型", required = true)
	private ConferenceApiRequestType type;
	
}
