package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.ProblemTypeEnum;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ProblemRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = 8706404993377018587L;
	
	@ApiParam(value = "请求的问题状态", required = true)
	ProblemTypeEnum status;
	@ApiParam(value = "请求的滚动计划类型", required = false)
	String type;
}
