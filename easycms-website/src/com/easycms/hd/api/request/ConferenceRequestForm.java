package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.conference.enums.ConferenceType;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ConferenceRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -4243628993031692764L;
	
	@ApiParam(value = "会议ID", required = false)
	private Integer id;
	@ApiParam(value = "会议主题", required = false)
	private String subject;
	@ApiParam(value = "会议正文", required = false)
	private String content;
	@ApiParam(value = "会议类别", required = false)
	private String category;
	@ApiParam(value = "项目名称", required = false)
	private String project;
	@ApiParam(value = "主持人", required = false)
	private String host;
	@ApiParam(value = "记录员", required = false)
	private String recorder;
	@ApiParam(value = "会议用品", required = false)
	private String supplies;
	@ApiParam(value = "会议地点", required = false)
	private String address;
	@ApiParam(value = "会议备注", required = false)
	private String remark;
	@ApiParam(value = "开始时间", required = false)
	private Date startTime;
	@ApiParam(value = "结束时间", required = false)
	private Date endTime;
	@ApiParam(value = "提醒时间", required = false)
	private ConferenceAlarmTypsEnums alarmTime = ConferenceAlarmTypsEnums.NONE;
	@ApiParam(value = "会议的存档类型", required = true)
	private ConferenceType type;
	@ApiParam(value = "参会人", required = false)
	private Integer[] participants;
	@ApiParam(value = "需要保留的文件ID", required = false)
	private List<Integer> fileIds;
}
