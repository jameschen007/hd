package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WelderTeamRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 4380398401692810729L;
	@ApiParam(value = "滚动计划的id号", required = true)
	Integer rollingPlanId;
}
