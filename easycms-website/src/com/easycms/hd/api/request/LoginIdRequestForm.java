package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class LoginIdRequestForm implements Serializable{
	private static final long serialVersionUID = -3843225729688347406L;
	@ApiParam(value = "用户ID")
	Integer loginId;
}
