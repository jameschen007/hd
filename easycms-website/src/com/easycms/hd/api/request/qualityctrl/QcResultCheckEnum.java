package com.easycms.hd.api.request.qualityctrl;

public enum QcResultCheckEnum {
	
	/**
	 * 合格
	 */
	Qualified,
	
	/**
	 * 不合格
	 */
	Unqualified
	

}
