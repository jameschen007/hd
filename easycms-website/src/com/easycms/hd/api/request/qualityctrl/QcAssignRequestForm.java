package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QcAssignRequestForm extends BaseRequestForm implements Serializable  {
	private static final long serialVersionUID = 6019194351780237082L;
	
	@ApiParam(required = true, name = "qcProblrmId", value = "质量管理问题ID")
	private Integer qcProblrmId;
	
	@ApiParam(required = true, name = "assignedId", value = "被指派ID")
	private Integer assignedId;

}
