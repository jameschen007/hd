package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControlRequestForm extends BaseRequestForm implements Serializable {

	private static final long serialVersionUID = -1976959558756148374L;

	@ApiParam(value="类别",required=true)
	private QualityReasonCodeEnum type;
	
	@ApiParam(value="区域",required=true)
	private String area;
	
	@ApiParam(value="机组",required=true)
	private String unit;

	@ApiParam(value="子项",required=true)
	private String subitem;

	@ApiParam(value="楼层",required=true)
	private String floor;

	@ApiParam(value="房间号",required=true)
	private String roomnum;

	@ApiParam(value="系统",required=false)
	private String system;

	@ApiParam(value="责任部门",required=true)
	private Integer responsibleDept;

	@ApiParam(value="责任班组",required=true)
	private Integer responsibleTeam;

	@ApiParam(value="问题描述",required=true)
	private String problemDescription;
}
