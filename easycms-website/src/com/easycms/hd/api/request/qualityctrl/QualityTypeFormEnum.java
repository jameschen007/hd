package com.easycms.hd.api.request.qualityctrl;

public enum QualityTypeFormEnum {
	/**
	 * 用户创建的问题
	 */
	MINE,
	
	/**
	 * 查询分派给QC1的问题 
	 */
	QC,
	
	/**
	 * 查询责任部门的问题
	 */
	DEPT,
	
	/**
	 * 查询责任班组的问题
	 */
	TEAM

}
