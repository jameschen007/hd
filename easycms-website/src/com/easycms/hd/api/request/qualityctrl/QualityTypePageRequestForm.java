package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.qualityctrl.domain.QualityControlStatus;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QualityTypePageRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = 6345184410403623606L;
	
	@ApiParam(value = "查询类型,MINE-登录人创建的问题，QC-分派给QC1的问题,DEPT-相关部门的问题,TEAM-相关班组的问题", required = true)
	private QualityTypeFormEnum type;

	@ApiParam(value = "LoginID、QC1、部门、班组ID，与查询类型对应", required = true)
	private Integer queryId;

	@ApiParam(value = "问题状态，为空则查询指定类型下所有问题", required = false)
	private QualityControlStatus status;
	
}
