package com.easycms.hd.api.request;

import java.io.Serializable;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotificationMarkRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 5723214498321521210L;
	@ApiParam(value = "通知Id", required = true)
	private Integer notificationId;
}
