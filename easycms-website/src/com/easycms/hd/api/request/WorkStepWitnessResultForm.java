package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepWitnessResultForm extends BaseRequestForm implements Serializable {
	private static final long serialVersionUID = -3526872765287225077L;
	@ApiParam(value = "见证条目ID", required=true)
	private Integer id;
	@ApiParam(value = "见证地点")
	private String witnessaddress;
	@ApiParam(value = "简要描述")
	private String witnessdesc;
	@ApiParam(value = "见证时间")
	private Date witnessdate;
	
	@ApiParam(value = "实际完成工程量")
	private String dosage;
	@ApiParam(value = "见证结果，1-不合格，3-合格", required=true)	
	private Integer isok;
	@ApiParam(value = "见证不合格类型，1， 2， 3", required=false)	
	private String failType;
	@ApiParam(value = "不合格时的描述")
	private String remark;
//	private List<RollingMaterialResult> materialList;
	@ApiParam(value = "材料列表结果")
	private String materiaJsonlList ;
	@ApiParam(value = "被代替见证者")
	private String substitute; ;
}
