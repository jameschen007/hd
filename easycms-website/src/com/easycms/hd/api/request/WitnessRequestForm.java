package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.WitnessTypeEnum;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.plan.enums.NoticePointType;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WitnessRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = 8270628938150008073L;
	
	@ApiParam(value = "请求的见证状态", required = true)
	WitnessTypeEnum status;
	@ApiParam(value = "请求的滚动计划类型", required = true)
	String type;
	@ApiParam(value = "通知点类型", required = false)
	NoticePointType noticePointType;
	@ApiParam(value = "用户ID", required = false)
	Integer userId;
	@ApiParam(value = "登录用户当前选择角色id",required=false,defaultValue="516")
	Integer roleId;
}
