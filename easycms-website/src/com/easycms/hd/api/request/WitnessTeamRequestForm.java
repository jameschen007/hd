package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.enums.WitnessTeamTypeEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WitnessTeamRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 4380398401692810729L;
	@ApiParam(value = "所需要获取的见证团队类型", required = true)
	WitnessTeamTypeEnum teamType;
	@ApiParam(value = "获取见证成员时的组长 ID", required = false)
	Integer userId;
}
