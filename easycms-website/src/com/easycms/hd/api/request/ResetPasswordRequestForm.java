package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ResetPasswordRequestForm implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	
	@ApiParam(value = "用户登录的密码", required = true)
	String password;
	
	@ApiParam(value = "用户登录的新密码", required = true)
	String newPassword = "";
	
	@ApiParam(value = "用户ID", required = true)
	Integer loginId;
}
