package com.easycms.hd.api.enpower.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.api.enpower.domain.EnpowerUser;

@Transactional
public interface EnpowerUserDao extends Repository<EnpowerUser,Integer>,BasicDao<EnpowerUser>{

}
