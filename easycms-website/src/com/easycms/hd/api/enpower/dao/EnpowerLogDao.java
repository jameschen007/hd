package com.easycms.hd.api.enpower.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.api.enpower.domain.EnpowerLog;

@Transactional
public interface EnpowerLogDao extends Repository<EnpowerLog,Integer>,BasicDao<EnpowerLog>{

}
