package com.easycms.hd.api.enpower;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.function.EnpowerExpireNoticeFunction;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.base.SimpleBasePagenationRequestForm;
import com.easycms.hd.api.response.EnpowerExpireNoticeResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.conference.domain.ExpireNoticeReader;
import com.easycms.hd.conference.service.ExpireNoticeFilesService;
import com.easycms.hd.conference.service.ExpireNoticeService;
import com.easycms.hd.conference.service.impl.EnpowerExpireNoticeServiceImpl;
import com.easycms.hd.plan.domain.EnpowerExpireNoticeResponse;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower")

//consumes = {MediaType.APPLICATION_JSON_VALUE},
//produces = {MediaType.APPLICATION_JSON_VALUE}
@Api(value = "EnpowerExpireNoticeApiController", description = "Enpower过期文件相关api")
public class EnpowerExpireNoticeApiController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EnpowerExpireNoticeServiceImpl enpowerExpireNoticeImpl;
	
	@Autowired
	private ExpireNoticeService expireNoticeService;
	
	@Autowired
	private ExpireNoticeFilesService expireNoticeFilesService;
	
	@Autowired
	private JPushService jPushService;
	
	/**
	 * 示例请求格式：
[{
  "no": "01d2f260-c6a7-4512-bdbb-ac145032ba24",
  "title": "中国五公司XX项目部文件失效通知",
  "content": "<div class='text' style='padding: 0px;'><p>各位领导，各部门负责人：</p><p>下列文件已失效，不再使用，请在<font color='#e51c23'> 2017 年 11 月 29 日</font>前，将失效文件（纸质版）退回文档组，同时请自行删除电子版（PDF版），以避免误用。</p></div>",
  "department": "中国五公司XX项目部",
  "approvelTime": "2017-11-16 07:09:40",
  "writeTime": "2017-11-16 07:09:40",
  "publishTime": "2017-11-16 07:09:40",
  "participants": [
    "8c8fee65e52f417ba991196a8136fd95",
	"f52d519a944346629d65509db26f13e9"
  ],
  "files": [
    {
      "no": "e754013f-d85f-4c31-86de-45dc7f84d84d",
      "number": 4,
      "version": "V4.5",
      "receiver": "杨婕生",
      "status": "EXPIRED",
      "filename": "红外遥控密码锁设计.doc"
    }
  ]
}]
	 * @param request
	 * @param response
	 * @param domain
	 * @param timestamp
	 * @param signature
	 * @param data
	 * @return
	 */
//	@SuppressWarnings("serial")
//	@ResponseBody
//	@ApiOperation(value = "由Enpower推送过期文件", notes = "enpower将过期文件推送到HD服务器")
//	@RequestMapping(value = "/expire_notice", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
//	public JsonResult<Boolean> enpowerExpireNotice(HttpServletRequest request, HttpServletResponse response, 
//			@RequestHeader(required=true, defaultValue="enpower") String domain,
//			@RequestHeader(required=true, defaultValue="123") String timestamp,
//			@RequestHeader(required=true, defaultValue="251402f3307cb8f7e7bf53671e210144") String signature,
//			@RequestBody(required=true) String data, @ModelAttribute EnpowerRequestExpireNoticeForm test) {
//		
//		JsonResult<Boolean> result = new JsonResult<Boolean>();
//		
//		log.info(data);
//		EnpowerExpireNotice enpowerExpireNotice = new EnpowerExpireNotice();
//		enpowerExpireNotice.setCreatedate(new Date());
//		enpowerExpireNotice.setData(data);
//		enpowerExpireNotice.setStatus(EnpowerDataStatusEnum.INIT.name());
//		if (!enpowerExpireNoticeService.save(enpowerExpireNotice)) {
//			result.setCode("-1001");
//			result.setMessage("数据存储失败.");
//			return result;
//		} else {
//			try {
//				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
//				List<EnpowerRequestExpireNoticeForm> forms = gson.fromJson(data, new TypeToken<List<EnpowerRequestExpireNoticeForm>>() {}.getType());
//				for (EnpowerRequestExpireNoticeForm form : forms) {
//					ExpireNotice expireNotice = new ExpireNotice();
//					expireNotice.setApprovelTime(form.getApprovelTime());
//					expireNotice.setContent(form.getContent());
//					expireNotice.setDepartment(form.getDepartment());
//					expireNotice.setNo(form.getNo());
//					expireNotice.setPublishTime(form.getPublishTime());
//					expireNotice.setWriteTime(form.getWriteTime());
//					expireNotice.setTitle(form.getTitle());
//					if (null != form.getParticipants()) {
//						List<User> users = new ArrayList<User>();
//						for (String uId : form.getParticipants()){
//							User participant = userService.findUserEnpowerId(uId);
//							if (null != participant) {
//								users.add(participant);
//							}
//						}
//						expireNotice.setParticipants(users);
//					}
//					
//					if (null != expireNoticeService.add(expireNotice)) {
//						if (null != form.getFiles() && !form.getFiles().isEmpty()) {
//							for (EnpowerRequestExpireNoticeFilesForm file : form.getFiles()) {
//								ExpireNoticeFiles expireNoticeFiles = new ExpireNoticeFiles();
//								expireNoticeFiles.setNo(file.getNo());
//								expireNoticeFiles.setStatus(file.getStatus());
//								expireNoticeFiles.setFilename(file.getFilename());
//								expireNoticeFiles.setNumber(file.getNumber());
//								expireNoticeFiles.setReceiver(file.getReceiver());
//								expireNoticeFiles.setVersion(file.getVersion());
//								expireNoticeFiles.setNoticeId(expireNotice.getId());
//								
//								expireNoticeFilesService.add(expireNoticeFiles);
//							}
//						}
//						
//						String message = "您有一个失效文件通知[" + expireNotice.getTitle() + "]";
//			            JPushExtra extra = new JPushExtra();
//			    		extra.setCategory(JPushCategoryEnums.EXPIRE_NOTICE.name());
//			    		jPushService.pushByUser(extra, message, JPushCategoryEnums.EXPIRE_NOTICE.getName(), expireNotice.getParticipants());
//					}
//				}
//				
//				enpowerExpireNotice.setStatus(EnpowerDataStatusEnum.SUCCESS.name());
//				enpowerExpireNoticeService.save(enpowerExpireNotice);
//			} catch (Exception e) {
//				e.printStackTrace();
//				enpowerExpireNotice.setStatus(EnpowerDataStatusEnum.FAILED.name());
//				enpowerExpireNotice.setDescription(e.getMessage());
//				enpowerExpireNoticeService.save(enpowerExpireNotice);
//				
//				result.setCode("-1001");
//				result.setMessage("数据转换与存储失败.");
//				return result;
//			}
//		}
//		
//		return result;
//	}

	@ResponseBody
	@ApiOperation(value = "文件失效通知单同步写入APP", notes = "enpower远程调用接口，用于创建文件失效通知单")
	@RequestMapping(value = "/expire_notice", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> expire_notice(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="123") String timestamp,
			@RequestHeader(required=true, defaultValue="ca50b9b8596de27d7080f9ead13d0c84") String signature,
			@RequestBody String form
			) {

		//正常记录原始内容
		FileUtility ut = new FileUtility();

		SimpleDateFormat sdf =new SimpleDateFormat("MM_dd_HHmmssS");
		String rollingfile = sdf.format(Calendar.getInstance().getTime())+"_expire_notice.txt";
		String filePath_ = com.easycms.common.logic.context.ContextPath.fileBasePath+com.easycms.common.util.FileUtils.rollingPlanFilePath;
		String filenameTemp = filePath_+"/"+rollingfile;//文件路径+名称+文件类型

		ut.createFile(filenameTemp, form);

		Gson gson = new Gson();

		List<EnpowerExpireNoticeResponse> sixLevelRollingPlans = gson.fromJson(form, new TypeToken<List<EnpowerExpireNoticeResponse>>() {}.getType());
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		boolean f = false;
		
		try{
			result = enpowerExpireNoticeImpl.save(sixLevelRollingPlans);



		}catch(Exception e){
			e.printStackTrace();

//			FileUtility ut = new FileUtility();
			ut.createFile(null, e.getMessage()+form);
			
			f = false ;
			result.setResponseResult(f);
			result.setCode("-1001");
			result.setMessage("操作异常"+e.getMessage()+form);
		}
		
		return result;
	}

	@ResponseBody
	@ApiOperation(value = "文件失效通知单归还写入APP", notes = "enpower远程调用接口，用于归还文件失效通知单")
	@RequestMapping(value = "/updateCancLoginId", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> updateReader(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="123") String timestamp,
			@RequestHeader(required=true, defaultValue="ca50b9b8596de27d7080f9ead13d0c84") String signature,
			@RequestBody String form
			) {
		Gson gson = new Gson();

		List<ExpireNoticeReader> sixLevelRollingPlans = gson.fromJson(form, new TypeToken<List<ExpireNoticeReader>>() {}.getType());
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		boolean f = false;
		
		try{
			result = enpowerExpireNoticeImpl.update(sixLevelRollingPlans);
		}catch(Exception e){
			e.printStackTrace();
			f = false ;
			result.setResponseResult(f);
			result.setCode("-1001");
			result.setMessage("操作异常"+e.getMessage()+form);
		}
		
		return result;
	}
	

//  /**
//   * 
//   */
//  @ResponseBody
//  @ApiOperation(value = "文件失效单列表", notes = "文件失效单列表")
//  @RequestMapping(value = "/invalidFileList", method = RequestMethod.GET)
//  public JsonResult<List<ExpireNoticeFileResult>> invalidFileList(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(required = false, name = "cancelId", value = "失效通知单id",defaultValue="09f4f74a-4119-4ba2-89b5-6d87ad7978e9")@RequestParam(value="cancelId",required=false) String cancelId, 
//			@ApiParam(required = true, name = "loginId", value = "loginId",defaultValue="434") @RequestParam(value="loginId",required=true) Integer loginId
//         ) {
//
//      JsonResult<List<ExpireNoticeFileResult>> result = new JsonResult<List<ExpireNoticeFileResult>>();
//      try{
//          HttpSession session = request.getSession();
//          User loginUser = (User) session.getAttribute("user");
//          if(loginUser==null){
//          	loginUser = userService.findUserById(loginId);
//          }
////          new ArrayList<ExpireNoticeFileResult>()
////      	String cancelId,String enpowerUserId,int pagenum,int pagesize
//          List<ExpireNoticeFileResult> list = executePushToEnpowerServiceImpl.fileInvalidNoticeList(cancelId,loginUser.getEnpowerId(),
//          		EnpowerConstantVar.enpowerPageIndex,EnpowerConstantVar.enpowerPageSize);
//          result.setResponseResult(list);
//      }catch(Exception e){
//          e.printStackTrace();
//          result.setMessage("操作异常:"+e.getMessage());
//          result.setResponseResult(new ArrayList<ExpireNoticeFileResult>());
//          return result;
//      }
//      return result;
//  }
	
    /**
     * 
     */
    @ResponseBody
    @ApiOperation(value = "文件失效单列表", notes = "文件失效单列表")
    @RequestMapping(value = "/invalidFileList", method = RequestMethod.GET)
    public JsonResult<Object> invalidFileList(HttpServletRequest request, HttpServletResponse response,
    		@ModelAttribute SimpleBasePagenationRequestForm form
           ) {
        JsonResult<Object> result = new JsonResult<Object>();
        try{
            HttpSession session = request.getSession();
            User loginUser = (User) session.getAttribute("user");
            if(loginUser==null){
            	loginUser = userService.findUserById(form.getLoginId());
            }
            EnpowerExpireNotice condition= new EnpowerExpireNotice();
            Map<String,Object> additionalCondition = new HashMap<String,Object>();
            Page<EnpowerExpireNotice> page = new Page<EnpowerExpireNotice>();
            page.setPagesize(form.getPagesize());
            page.setPageNum(form.getPagenum());
            page = this.enpowerExpireNoticeImpl.findByPage(condition, additionalCondition, page);
            //翻译
            List<EnpowerExpireNotice> notices = page.getDatas();

            Page<EnpowerExpireNoticeResult> page2 = new Page<EnpowerExpireNoticeResult>();
            page2.setPagesize(form.getPagesize());
            page2.setPageNum(form.getPagenum());
            

            if(null != notices){
            	page2.setTotalCounts(page.getTotalCounts());

                EnpowerExpireNoticeFunction func =  new EnpowerExpireNoticeFunction();
            	page2.setDatas(notices.stream().map(question -> {
            		return func.translate(question,false);
    			}).collect(Collectors.toList()));
            }
            
            result.setResponseResult(page);
        	
        	
        }catch(Exception e){
            e.printStackTrace();
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(new Page<EnpowerExpireNotice>());
            return result;
        }
        return result;
    }

	@ResponseBody
	@RequestMapping(value = "/invalidFileDetail/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取失效文件 -- 单个", notes = "获取 单个的失效文件 -- 带文件列表。")
	public JsonResult<EnpowerExpireNoticeResult> singleExpireNotice(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form, 
			@PathVariable("id") Integer id) throws Exception {
		JsonResult<EnpowerExpireNoticeResult> result = new JsonResult<EnpowerExpireNoticeResult>();
			
		EnpowerExpireNotice newNotice = enpowerExpireNoticeImpl.findById(id);
		
		EnpowerExpireNoticeResult n = new EnpowerExpireNoticeFunction().translate(newNotice,true);
		result.setResponseResult(n);
		
		return result;
	}
	
	
//	
//    /**
//     * 
//     */
//    @ResponseBody
//    @ApiOperation(value = "文件失效单详情", notes = "文件失效单列表")
//    @RequestMapping(value = "/invalidFileDetail", method = RequestMethod.GET)
//    public JsonResult<List<ExpireNoticeFileResult>> invalidFileDetail(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(required = false, name = "cancelId", value = "失效通知单id",defaultValue="09f4f74a-4119-4ba2-89b5-6d87ad7978e9")@RequestParam(value="cancelId",required=false) String cancelId, 
//			@ApiParam(required = true, name = "loginId", value = "loginId",defaultValue="434") @RequestParam(value="loginId",required=true) Integer loginId
//           ) {
//
//        JsonResult<List<ExpireNoticeFileResult>> result = new JsonResult<List<ExpireNoticeFileResult>>();
//        try{
//            HttpSession session = request.getSession();
//            User loginUser = (User) session.getAttribute("user");
//            if(loginUser==null){
//            	loginUser = userService.findUserById(loginId);
//            }
////            new ArrayList<ExpireNoticeFileResult>()
////        	String cancelId,String enpowerUserId,int pagenum,int pagesize
//            List<ExpireNoticeFileResult> list = executePushToEnpowerServiceImpl.fileInvalidNoticeList(cancelId,loginUser.getEnpowerId(),
//            		EnpowerConstantVar.enpowerPageIndex,EnpowerConstantVar.enpowerPageSize);
//            result.setResponseResult(list);
//        }catch(Exception e){
//            e.printStackTrace();
//            result.setMessage("操作异常:"+e.getMessage());
//            result.setResponseResult(new ArrayList<ExpireNoticeFileResult>());
//            return result;
//        }
//        return result;
//    }
}
