package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EnpowerRequestMaterialStoreCheckForm implements Serializable{
	private static final long serialVersionUID = 2546469478464114750L;

	@ApiParam(value = "质保编号", required = true)
	private String GUARANTEENO;
	@ApiParam(value = "物项编码", required = true)
	private String MATE_CODE;
	
	@ApiParam(value = "制造商", required = false)
	private String MANUFACTURER;
	@ApiParam(value = "生产日期", required = false)
	private String YIELD_DATE;
	@ApiParam(value = "有效期", required = false)
	private String WARRANT_START_DATE;
	@ApiParam(value = "保质期/月", required = false)
	private String SHELIFE_MONTHS;
	@ApiParam(value = "保修期/月", required = false)
	private String MONTHS_WARRANT;
	@ApiParam(value = "船次", required = true)
	private String DELIV_LOT;
	@ApiParam(value = "存储仓库", required = true)
	private String STORAGES_CODE;
	@ApiParam(value = "存储级别", required = false)
	private String STRG_CLASS;
	@ApiParam(value = "货位", required = true)
	private String VESSEL_NO;
	@ApiParam(value = "文件编号", required = false)
	private String FILE_NO;
	@ApiParam(value = "核实数量", required = true)
	private String CHECK_QTY;
	@ApiParam(value = "储存区域", required = false)
	private String WAREH_AREA;
	@ApiParam(value = "是否设备工机具", required = false)
	private String GJJSBFL;

}
