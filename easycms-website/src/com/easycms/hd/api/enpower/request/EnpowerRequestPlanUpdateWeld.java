package com.easycms.hd.api.enpower.request;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 更新焊口条目信息
 * @author wangz
 *
 */
@Data
public class EnpowerRequestPlanUpdateWeld {

	public static Logger logger = Logger.getLogger(EnpowerRequestPlanStatusSyn.class);
	
//	{ "xml": "更新焊口条目信息",
//		 "date_col":"START_DATE,
//		 END_DATE",
//		"photo_col":"",
//		"_value":" [{'WORKLIST_ID||WELD_CODE':'5de4530958bf4c298419be9943e0a308A2',
//		'FLAG':'Y', v
//		'WORK_STATE':'完成',     v
//		'START_DATE':'2018-1-29 01:01:01',  v
//		'END_DATE':'2018-1-30 01:01:01',  v
//		'WORK_TEAM_NAME':'焊接一组',   
//		'WORK_TEAM_MAN':'李四',
//		'REAL_WORK_GCL':'2.5'}]"}

	@SerializedName(value = "WORKLIST_ID||WELD_CODE")
	// 作业条目id和焊口号　以||
	private String workListIdAndWeldCode;

	// FLAG,--施工是否完成标志，默认N，完成则为Y

	// 施工是否完成标志，默认N，完成则为Y
	@SerializedName(value = "FLAG")
	private String flag = "N";
	// WORK_STATE,--施工状态，如未分配
	@SerializedName(value = "WORK_STATE")
	private String WORK_STATE;
	// START_DATE,--实际开始时间
	@SerializedName(value = "START_DATE")
	private String START_DATE;

	// END_DATE,--实际完成时间
	@SerializedName(value = "END_DATE")
	private String END_DATE; 
	// WORK_TEAM_NAME,--作业组名称
	@SerializedName(value = "WORK_TEAM_NAME")
	private String WORK_TEAM_NAME;

	// WORK_TEAM_MAN,--作业组负责人
	@SerializedName(value = "WORK_TEAM_MAN")
	private String WORK_TEAM_MAN;
	// REAL_WORK_GCL,--实际完成工程量
	@SerializedName(value = "REAL_WORK_GCL")
	private String realProjectcost;

	// PROJ_CODE,--固定代码：K2K3
	@SerializedName(value = "PROJ_CODE")
	private String PROJ_CODE = EnpConstant.PROJ_CODE;

	// COMP_CODE --固定代码：050812
	@SerializedName(value = "COMP_CODE")
	private String COMP_CODE= EnpConstant.COMP_CODE;

}

