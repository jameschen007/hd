package com.easycms.hd.api.enpower.request;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EnpowerRequestExpireNoticeForm implements Serializable{
	private static final long serialVersionUID = 2546469478464114750L;
	@ApiParam(value = "编号", required = false)
	private String no;
	@ApiParam(value = "标题", required = false)
	private String title;
	@ApiParam(value = "正文", required = false)
	private String content;
	@ApiParam(value = "部门", required = false)
	private String department;
	@ApiParam(value = "审批日期", required = false)
	private Date approvelTime;
	@ApiParam(value = "编制日期", required = false)
	private Date writeTime;
	@ApiParam(value = "发布日期", required = false)
	private Date publishTime;
	@ApiParam(value = "参会人", required = false)
	private String[] participants;
	@ApiParam(value = "文件列表信息", required = false)
	private List<EnpowerRequestExpireNoticeFilesForm> files;
}
