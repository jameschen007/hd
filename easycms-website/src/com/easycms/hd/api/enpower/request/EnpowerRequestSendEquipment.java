package com.easycms.hd.api.enpower.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 推送接口名: 插入设备清单					

 * @author ul-webdev
 *
 */
@Data
public class EnpowerRequestSendEquipment {
//  通知单ID(来源于app创建的发点通知单主信息的id)
	@SerializedName(value = "TZD_ID")
	//外键（来源于发点通知单主信息的主键ID）
	private String mainId ;
//  设备编码
	@SerializedName(value = "EQUI_CODE")
	//设备编码
	private String equipmentCode ;
//  图纸号
	@SerializedName(value = "DRAW_NO")
	//图纸号
	private String drawingNo ;
//  源ID(来源于任务单信息的主键id)
	@SerializedName(value = "S_ID")
	//任务单信息列表enpower作业包工程量的id
	private String enpowerId ;
//  创建人
	@SerializedName(value = "FOUND_MAN")
	//技术员（Enpower是存在技术员那个字段的，APP传组长名称）
	private String consteamName ;
//  创建时间
//	@SerializedName(value = "FOUND_DATE")
//	//创建时间:系统当前日期
//	private String createDate ;

}
