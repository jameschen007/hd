package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.wordnik.swagger.annotations.ApiModel;

import lombok.Data;

/***
 * 修改物项库存信息
 */
@Data
@ApiModel(value = "enpowerRequestMaterialOutCheckStock")
public class EnpowerRequestMaterialOutCheckStock implements Serializable {
	private static final long serialVersionUID = 1L;
	private String ID ;
	//库存数量
	private String STK_QTY ;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
