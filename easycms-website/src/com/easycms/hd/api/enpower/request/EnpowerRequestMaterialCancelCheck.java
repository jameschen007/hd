package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 *  物项退库核实接口 		需要接收的数据bean	
 *  推送接口名: 物项退库核实接口
 */
@Data
public class EnpowerRequestMaterialCancelCheck  implements Serializable{
	private static final long serialVersionUID = 1L;
	//物项编码   
    private String MATE_CODE;
//退库单号   
	private String ISSNO ;
//退库量    
    private String ISSQTY;
//核实量    
    private String QTY_RELEASED;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
