package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModel;

import lombok.Data;

@Data
@ApiModel(value = "enpowerRequestMaterialCancelCheckInfo")
public class EnpowerRequestMaterialCancelCheckInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ID;
	/** 存储仓库 */
	private String WAREH_CODE;
	/** 货位 */
	private String WAREH_PLACE;
	/** 退库数量 */
	private String ISSQTY;
	/** 核实量 */
	private String QTY_RELEASED;
	/** 状态 */
	private String STATUS;

}
