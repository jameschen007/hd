package com.easycms.hd.api.enpower.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
/**
 * enpower材料回填
 * @author ul-webdev
 *
 */
@Data
public class EnpowerRequestMaterialBackfill {

	// 材料ID
	@SerializedName(value = "ID")
	// enpower材料主键id
	private String enpowerMaterialId;

	// 实际用量
	@SerializedName(value = "REAL_USE_QTY")
	// 实际用量
	private String actualDosage;

	// 材料反填人
	@SerializedName(value = "MATER_BACK_FILL_MAN")
	// 见证人姓名
	private String recorderName;

	// 材料反填日期
	@SerializedName(value = "MATER_BACK_FILL_DATE")
	// 见证表中真实见证时间
	private String real_witnessdata;
	
}
