package com.easycms.hd.api.enpower.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 *   查询今日出库量明细接口		需要接收的数据bean	
 *  推送接口名: 查询今日出库量明细接口
 */
@Data
public class EnpowerRequestMaterialTodayOutList {
//  通知单ID(来源于app创建的发点通知单主信息的id)
	@SerializedName(value = "TZD_ID")
	//外键（来源于发点通知单主信息的主键ID）
	private String mainId ;
	
// 
//	当天日期
//	项目代号（默认值“K2K3”）
//	公司代码（K项默认值：“050812”)
}
