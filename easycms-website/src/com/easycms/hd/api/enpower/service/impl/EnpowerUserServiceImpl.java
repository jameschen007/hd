package com.easycms.hd.api.enpower.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.enpower.dao.EnpowerUserDao;
import com.easycms.hd.api.enpower.domain.EnpowerRole;
import com.easycms.hd.api.enpower.domain.EnpowerRoleDomain;
import com.easycms.hd.api.enpower.domain.EnpowerRoleEntity;
import com.easycms.hd.api.enpower.domain.EnpowerRoleMenuDomain;
import com.easycms.hd.api.enpower.domain.EnpowerRoleMenuEntity;
import com.easycms.hd.api.enpower.domain.EnpowerUser;
import com.easycms.hd.api.enpower.response.EnpowerResponseDepartment;
import com.easycms.hd.api.enpower.response.EnpowerResponseRole;
import com.easycms.hd.api.enpower.response.EnpowerResponseUser;
import com.easycms.hd.api.enpower.service.EnpowerUserService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("enpowerUserService")
@Transactional
public class EnpowerUserServiceImpl implements EnpowerUserService {
	private static Logger logger = Logger.getLogger(EnpowerUserServiceImpl.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private PrivilegeService privilegeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private EnpowerUserDao enpowerUserDao;

	@Autowired
	private ModulesService modulesService;
	
	@Value("#{APP_SETTING['enpower_default_password']}")
	private String enpowerDefaultPassword;
	
	@Value("#{APP_SETTING['enpower_role_policy']}")
	private String enpowerRolePolicy;
	
	private static final String ROLE_TREE = "role_tree.json";
	private static final String ROLE_MENU = "role_menu.json";

	@Override
	public boolean insert(EnpowerUser enpowerUser) {
		return null != enpowerUserDao.save(enpowerUser);
	}

	@Override
	public boolean insertBatchDepartmentAndUser(List<EnpowerResponseDepartment> enpowerResponseDepartments, List<EnpowerResponseUser> enpowerResponseUsers) {
		if (null != enpowerResponseDepartments && !enpowerResponseDepartments.isEmpty() && 
				null != enpowerResponseUsers && !enpowerResponseUsers.isEmpty()) {
			if (validataEnpowerResponseUser(enpowerResponseUsers) && validataEnpowerResponseDepartment(enpowerResponseDepartments)) {
				//将所有部门和用户信息建立成树状结构
				List<EnpowerResponseDepartment> parentDepartments = enpowerResponseDepartments.stream()
						.filter(department -> {
							return department.getParentId().equals("0");
						}).collect(Collectors.toList());
				for (EnpowerResponseDepartment department : parentDepartments) {
					buildEnpowerDepartmentTree(enpowerResponseDepartments, enpowerResponseUsers, department);
				}
				
				log.info(CommonUtility.toJson(parentDepartments));
				
				//使用parentDepartments作为带有上下级关系的主数据对Department表进行插入操作！
				//以下(1, 2, 3)三者的顺序不能改变
				/**
				 * 1. 将用户中的岗位信息转化为角色插入数据库中
				 */
				insertBatchRole(enpowerResponseUsers);
				/**
				 * 2. 将部门信息插入到数据库中
				 */
				insertBatchDepartment(parentDepartments, null);
				/**
				 * 3. 将用户信息及其角色插入到数据库中
				 */
				insertBatchUser(enpowerResponseUsers);
				/**
				 * 4. 创建角色与菜单关联关系
				 */
				buildEnpowerRoleMenu();
				
				return true;
			}
		}
		return false;
	}

	/**
	 * 批量处理银角色信息，并更新数据库
	 * @param enpowerResponseUsers
	 * @return
	 */
	private boolean insertBatchRole(List<EnpowerResponseUser> enpowerResponseUsers) {
		if (null != enpowerResponseUsers && !enpowerResponseUsers.isEmpty()) {
			Set<EnpowerResponseRole> enpowerResponseRoles = transferRole(enpowerResponseUsers);
			
			//先将所有角色插入到数据库中
			for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
				Role role = new Role();
				role.setName(enpowerResponseRole.getRoleName());
				role.setRoleNo(enpowerResponseRole.getRoleNo());
				role.setEnpowerId(enpowerResponseRole.getRoleId());
				role.setEnpowerDepartmentId(enpowerResponseRole.getDepartmentId());
				role.setLevel(1);
				Role enpowerRoleExist = roleService.findByEnpowerId(enpowerResponseRole.getRoleId());
				if (null != enpowerRoleExist) {
					//修改已经存在的角色
					enpowerRoleExist.setName(enpowerResponseRole.getRoleName());
					enpowerRoleExist.setRoleNo(enpowerResponseRole.getRoleNo());
					enpowerRoleExist.setEnpowerDepartmentId(enpowerResponseRole.getDepartmentId());
					roleService.update(enpowerRoleExist);
				} else {
					roleService.add(role);
				}
			}
			/**
			 * 将从用户中获取的角色建立成树型结构，按照ALL里的规则
			 */
			enpowerResponseRoles = buildEnpowerRoleTree(enpowerResponseRoles, "ALL");
			
			for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
				Role parentRole = roleService.findByEnpowerId(enpowerResponseRole.getRoleId());
				if (null != parentRole) {
					/**
					 * 将原有的每一个角色子集继续回调更新数据库
					 */
					buildEnpowerRoleTree(enpowerResponseRole.getChildren(), parentRole);
				}
				
			}
			return true;
		}
		return false;
	}
	
	/**
	 * 批量处理用户信息并更新数据库
	 * @param enpowerResponseUsers
	 * @return
	 */
	private boolean insertBatchUser(List<EnpowerResponseUser> enpowerResponseUsers) {
		if (null != enpowerResponseUsers && !enpowerResponseUsers.isEmpty()) {
			Map<String, List<EnpowerResponseUser>> mapperEnpowerUsers = enpowerResponseUsers.stream().collect(Collectors.groupingBy(EnpowerResponseUser::getUserId));
			List<EnpowerResponseUser> collectEnpowerUsers = mapperEnpowerUsers.entrySet().stream().map(mapper -> {
				List<EnpowerResponseUser> values = mapper.getValue();
				
				EnpowerResponseUser result = values.get(0);
				Set<Modules> modules = values.stream().map(value -> {
					Modules module = modulesService.findByEnpowerName(value.getSpecialty());
					return module;
				}).collect(Collectors.toSet());
				
				Set<EnpowerRole> enpowerRoles = values.stream().map(value -> {
					EnpowerRole enpowerRole = new EnpowerRole();
					enpowerRole.setRoleId(value.getRoleId());
					enpowerRole.setRoleName(value.getRoleName());
					enpowerRole.setRoleNo(value.getRoleNo());
					return enpowerRole;
				}).collect(Collectors.toSet());
				result.setRoles(enpowerRoles);
				
				result.setModules(modules);
				
				return result;
			}).collect(Collectors.toList());
			
			for (EnpowerResponseUser enpowerResponseUser : collectEnpowerUsers) {
				User user = new User();
				user.setName(enpowerResponseUser.getLoginId());
				user.setRealname(enpowerResponseUser.getRealName());
				user.setDel(enpowerResponseUser.getStatus() == 0 ? true : false);
				user.setRegisterTime(new Date());
				user.setRegisterIp("127.0.0.1");
				user.setDefaultAdmin(false);
				user.setEnpowerId(enpowerResponseUser.getUserId());
				user.setMineProject(enpowerResponseUser.getMineProject());
				
				/**
				 * 将用户的所属模块信息写入
				 */
				if (null != enpowerResponseUser.getModules() && !enpowerResponseUser.getModules().isEmpty()) {
					user.setModules(new ArrayList<>(enpowerResponseUser.getModules()));
				}
				
				/**
				 * 将用户的角色信息写入
				 */
				if (null != enpowerResponseUser.getRoles() && !enpowerResponseUser.getRoles().isEmpty()) {
					List<Role> roleList = new ArrayList<Role>();
					for (EnpowerRole enpowerRole : enpowerResponseUser.getRoles()) {
						Role enpowerRoleExist = roleService.findByEnpowerId(enpowerRole.getRoleId());
						if (null != enpowerRoleExist) {
							roleList.add(enpowerRoleExist);
						}
					}
					user.setRoles(roleList);
				}
				
				/**
				 * 将用户的部门信息写入
				 */
				if (CommonUtility.isNonEmpty(enpowerResponseUser.getDepartmentId())) {
					Department department = departmentService.findByEnpowerId(enpowerResponseUser.getDepartmentId());
					if(department!=null){
						department.setBusinessNo(enpowerResponseUser.getDepartmentBusinessNo());
						departmentService.update(department);
						user.setDepartment(department);
					}else{
						logger.info("部门"+enpowerResponseUser.getDepartmentId()+" "+enpowerResponseUser.getDepartmentName()+"不存在");
					}
				}
				
				User enpowerUserExist = userService.findUserEnpowerId(enpowerResponseUser.getUserId());
				if (null != enpowerUserExist) {
					user.setId(enpowerUserExist.getId());
					user.setPassword(enpowerUserExist.getPassword());
					userService.update(user);
				} else {
					user.setPassword(enpowerDefaultPassword);
					
					//每个新增的用户，将模块全部初始录入进去
					user.setModules(
					modulesService.findByAll());
					user = userService.add(user);
				}
			}
/*			
			List<User> users = userService.findAll();
			for (User user : users) {
				List<Role> userRoles = user.getRoles();
				if (null != userRoles && !userRoles.isEmpty()) {
					for (Role role : userRoles) {
						log.error("Refiny:" + role.getName() + " : " + ((role.getUsers() == null || role.getUsers().isEmpty()) ? 0+"" : role.getUsers().size()+""));
					}
					
					 // 根据用户的角色，反查用户的上级人员
					 // 这个位置始终都是没有数据的状态，单独调用有效。
					 // 为什么，为什么，role.getUsers始终获取不到数据，在contrller里直接调用又是有效的！
					Role parentRole = userRoles.get(0).getParent();
					if (null != parentRole) {
						List<User> parentRoleUsers = parentRole.getUsers();
						if (null != parentRoleUsers && !parentRoleUsers.isEmpty()) {
							user.setParent(parentRoleUsers.get(0));
							userService.update(user);
						}
					};
				}
			}
	*/
			return true;
		}
		return false;
	}
	/**
	 * 批量处理部门信息并更新数据库
	 * @param enpowerResponseDepartments
	 * @param parent
	 * @return
	 */
	private boolean insertBatchDepartment(List<EnpowerResponseDepartment> enpowerResponseDepartments, Department parent) {
		if (null != enpowerResponseDepartments && !enpowerResponseDepartments.isEmpty()) {
			for (EnpowerResponseDepartment enpowerResponseDepartment : enpowerResponseDepartments) {
				Department department = new Department();
				department.setName(enpowerResponseDepartment.getOrgName());
				department.setOrgCode(enpowerResponseDepartment.getOrgCode());
				department.setEnpowerId(enpowerResponseDepartment.getId());
				department.setOrgAppcode(enpowerResponseDepartment.getOrgAppcode());
//				department.setEndDate(enpowerResponseDepartment.getEndDate());
				department.setDeleteYn(enpowerResponseDepartment.getDeleteYn());
				department.setRemark(enpowerResponseDepartment.getRemark());
				department.setOther1(enpowerResponseDepartment.getOther1());
				department.setOther2(enpowerResponseDepartment.getOther2());
				department.setOther3(enpowerResponseDepartment.getOther3());
				department.setOther4(enpowerResponseDepartment.getOther4());
				department.setOther5(enpowerResponseDepartment.getOther5());
				department.setOther6(enpowerResponseDepartment.getOther6());
				department.setOther7(enpowerResponseDepartment.getOther7());
				department.setOther8(enpowerResponseDepartment.getOther8());
				department.setOther9(enpowerResponseDepartment.getOther9());
				department.setOther10(enpowerResponseDepartment.getOther10());
				department.setEnpowerId(enpowerResponseDepartment.getId());
				department.setEnpowerParentId(enpowerResponseDepartment.getParentId());
				department.setParent(parent);
				
				if (null != enpowerResponseDepartment.getRoles() && !enpowerResponseDepartment.getRoles().isEmpty()) {
					Set<EnpowerResponseRole> roles = enpowerResponseDepartment.getRoles().stream().sorted((role1, role2) -> { 
			            return (role1.getRoleNo()).compareTo(role2.getRoleNo());  
			        }).collect(Collectors.toSet());
//					 * 当前上下级关系将由一个系统文件来指定，系统将从最高职务中选出ID最小的那一个来作为当前部门的最高权限职务
//					 * 职务树请参考role_tree.json
					//这里默认是拿的第一个职务作为其最高职务处理
					Role parentRole = roleService.findByEnpowerId(roles.toArray(new EnpowerResponseRole[]{})[0].getRoleId());
//					buildEnpowerRoleTree(roles, parentRole);
					department.setTopRole(parentRole);
				}
				
				Department enpowerDepartmentExist = departmentService.findByEnpowerId(enpowerResponseDepartment.getId());
				if (null != enpowerDepartmentExist) {
					department.setId(enpowerDepartmentExist.getId());
					departmentService.update(department);
				} else {
					departmentService.add(department);
				}
				
				if (null != enpowerResponseDepartment.getChildren() && !enpowerResponseDepartment.getChildren().isEmpty()) {
					insertBatchDepartment(enpowerResponseDepartment.getChildren(), department);
				}
			}
			return true;
		}
		return false;
	}
	
	//已无用
	@Deprecated
	@Override
	public List<EnpowerResponseUser> transferResponseUser(List<Map<String, Object>> dataList){
		List<EnpowerResponseUser> enpowerResponseUsers = new ArrayList<EnpowerResponseUser>();
		if (null != dataList && !dataList.isEmpty()) {
			for (Map<String, Object> data : dataList) {
				EnpowerResponseUser enpowerResponseUser = new EnpowerResponseUser();
				enpowerResponseUser.setUserId(data.get("用户ID").toString());
				enpowerResponseUser.setLoginId(data.get("登录名").toString());
				enpowerResponseUser.setRealName(data.get("中文名").toString());
				if(data.get("激活状态") instanceof Integer) {
					enpowerResponseUser.setStatus((Integer)data.get("激活状态"));
				}
				enpowerResponseUser.setDepartmentId(data.get("组织机构ID").toString());
				enpowerResponseUser.setDepartmentName(data.get("组织机构名称").toString());
				enpowerResponseUser.setDepartmentBusinessNo(data.get("组织机构业务编码").toString());
				enpowerResponseUser.setParentTeamId(data.get("上级组织机构ID").toString());
				enpowerResponseUser.setParentTeamName(data.get("上级组织机构名称").toString());
				enpowerResponseUser.setRoleId(data.get("岗位ID").toString());
				enpowerResponseUser.setRoleName(data.get("岗位名称").toString());
				enpowerResponseUser.setRoleNo(data.get("岗位编码").toString());
				enpowerResponseUser.setSpecialty(data.get("专业").toString());
				if(data.get("TOTAL_COUNT") instanceof Integer) {
					enpowerResponseUser.setTotalCount((Integer)data.get("TOTAL_COUNT"));
				}
				if(data.get("TOTAL_COUNT") instanceof Double) {
					enpowerResponseUser.setTotalCount(((Double)data.get("TOTAL_COUNT")).intValue());
				}
				if(data.get("ROW_NUM") instanceof Integer) {
					enpowerResponseUser.setRowNum((Integer)data.get("ROW_NUM"));
				}
				if(data.get("ROW_NUM") instanceof Integer) {
					enpowerResponseUser.setRowNum(((Double)data.get("ROW_NUM")).intValue());
				}
				enpowerResponseUsers.add(enpowerResponseUser);
			}
		}
		return enpowerResponseUsers;
	}
	@Override
	public boolean validataEnpowerResponseUser(List<EnpowerResponseUser> enpowerResponseUsers) {
		//验证用户的正确性，暂时还没有规则反馈
		return true;
		
	}
	@Override
	public boolean validataEnpowerResponseDepartment(List<EnpowerResponseDepartment> enpowerResponseDepartments) {
		//验证部门的正确性，暂时还没有规则反馈
		return true;
	}
	
	//建立Department的上下级关系结构树
	private void buildEnpowerDepartmentTree(List<EnpowerResponseDepartment> enpowerResponseDepartments, List<EnpowerResponseUser> enpowerResponseUsers,
			EnpowerResponseDepartment parent) {
		List<EnpowerResponseDepartment> children = enpowerResponseDepartments.stream().filter(data -> {
			if (null != data && CommonUtility.isNonEmpty(data.getParentId()) && null != parent && CommonUtility.isNonEmpty(parent.getId())) {
				if (data.getParentId().equals(parent.getId())) {
					return true;
				}
			}

			return false;
		}).collect(Collectors.toList());

		if (null != children && !children.isEmpty()) {
			parent.setChildren(children);
			
			for (EnpowerResponseDepartment department : children) {
				buildEnpowerDepartmentTree(enpowerResponseDepartments, enpowerResponseUsers, department);
			}
		}	
		
		List<EnpowerResponseUser> users = enpowerResponseUsers.stream().filter(user -> {
			if (CommonUtility.isNonEmpty(user.getDepartmentId()) && parent.getId().equals(user.getDepartmentId())) {
				return true;
			}
			return false;
		}).collect(Collectors.toList());
		
		if (null != users && !users.isEmpty()) {
			log.info("部门 " + parent.getOrgName() + " 有用户 " + users.size() + " 个.");
			
			
			//不同的部门将按照不同的职务来建立最高职务请根据ID来指定，当前是根据名称来
			Set<EnpowerResponseRole> roles = transferRole(users);
//以下以六行代码是为了定制不同不部门下的指定角色上下级关系			
//			if (parent.getOrgName().equals("QC部")) {
//				roles = buildEnpowerRoleTree(roles, "GDJH");
//				log.info("-----------------------------------------");
//				log.info(CommonUtility.toJson(roles));
//				log.info("-----------------------------------------");
//			}
			/**
			 * 找到指定岗位列表中的最高级岗位
			 */
			roles = retrogradeTopRole(roles);
			
			//这里是将现在的岗位按role_tree.json的规则生成一个合理的关系树结构，用于department表的保存
			parent.setRoles(roles);
		}
	}
	
	//建立Roles的上下级关系结构树 -- 更新数据库部分
	private void buildEnpowerRoleTree(Set<EnpowerResponseRole> enpowerResponseRoles, Role parent) {
		if (null != enpowerResponseRoles) {
			for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
				Role role = roleService.findByEnpowerId(enpowerResponseRole.getRoleId());
				if (null != role && !role.getEnpowerId().equals(parent.getEnpowerId())) {
					role.setParent(parent);
					role.setLevel(parent.getLevel() + 1);
					roleService.update(role);
				}
				if (null != enpowerResponseRole.getChildren() && !enpowerResponseRole.getChildren().isEmpty()) {
					buildEnpowerRoleTree(enpowerResponseRole.getChildren(), role);
				}
			}
		}
	}
	
	//建立Roles的上下级关系结构树 -- 原生对象的处理工作
	private Set<EnpowerResponseRole> buildEnpowerRoleTree(Set<EnpowerResponseRole> enpowerResponseRoles, String type) {
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(ROLE_TREE);
		Gson gson = new Gson();
		EnpowerRoleDomain enpowerRoleDomain = gson.fromJson(CommonUtility.readStringFromFile(in),
				new TypeToken<EnpowerRoleDomain>() {
				}.getType());
		
		if (null != enpowerRoleDomain) {
			List<EnpowerRoleEntity> enpowerRoleEntities = new ArrayList<EnpowerRoleEntity>();
			if (type.equals("GDJH")) {
				EnpowerRoleEntity enpowerRoleEntity = enpowerRoleDomain.getGDJH();
				enpowerRoleEntities.add(enpowerRoleEntity);
			} else if (type.equals("ALL")) {
				enpowerRoleEntities = enpowerRoleDomain.getALL();
			}
			if (null != enpowerRoleEntities && !enpowerRoleEntities.isEmpty() && null != enpowerResponseRoles) {
				Set<EnpowerResponseRole> results = new HashSet<EnpowerResponseRole>(); 
				
				for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
					for (EnpowerRoleEntity enpowerRoleEntity : enpowerRoleEntities) {
						if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("id")) {
							if (enpowerResponseRole.getRoleId().equals(enpowerRoleEntity.getId())) {
								enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, enpowerRoleEntity.getSubRole()));
								results.add(enpowerResponseRole);
							}
						} else if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("name")) {
							if (enpowerResponseRole.getRoleName().equals(enpowerRoleEntity.getName())) {
								enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, enpowerRoleEntity.getSubRole()));
								results.add(enpowerResponseRole);
							}
						}
					}
				}
				
				return results;
			}
		}
		
		return null;
	}
	
	//建立Roles的菜单关联 -- 确保已建立好相应的菜单树形结构
	@Override
	public void buildEnpowerRoleMenu() {
		try {
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(ROLE_MENU);
			Gson gson = new Gson();
			EnpowerRoleMenuDomain enpowerRoleMenuDomain = gson.fromJson(CommonUtility.readStringFromFile(in),
					new TypeToken<EnpowerRoleMenuDomain>() {
			}.getType());
			
			if (null != enpowerRoleMenuDomain) {
				logger.info("菜单重建开始...");
				List<String> allEntity = enpowerRoleMenuDomain.getALL();
				
				List<Role> roles = roleService.findAll2();
				Set<Menu> all = new HashSet<Menu>();
				if (null != allEntity && !allEntity.isEmpty()) {
					for (String m : allEntity) {
						Privilege privilege = privilegeService.findByUri(m);
						if (null != privilege) {
							all.add(privilege.getMenu());
							Set<Menu> results = getParentMenus(privilege.getMenu());
							if (null != results) {
								all.addAll(results);
							}
						}
					}
				}

				for (Role role : roles) {
					List<EnpowerRoleMenuEntity> spList = enpowerRoleMenuDomain.getSPLIT();
					Set<Menu> roleMenu = new HashSet<Menu>();
					//重建菜单：会将已有岗位（组长）的已有权限（我发的见证）取消掉，这是不应该的。
					for (EnpowerRoleMenuEntity enpowerRoleMenuEntity : spList) {
						if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("id")) {
							if (null != enpowerRoleMenuEntity.getId() && !enpowerRoleMenuEntity.getId().isEmpty()) {
								if (enpowerRoleMenuEntity.getId().contains(role.getEnpowerId())) {
									for (String m : enpowerRoleMenuEntity.getMenus()) {
										Privilege privilege = privilegeService.findByUri(m);
										if (null != privilege) {
											roleMenu.add(privilege.getMenu());
											Set<Menu> results = getParentMenus(privilege.getMenu());
											if (null != results) {
												roleMenu.addAll(results);
											}
										}
									}
								}
							}
						} else if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("name")) {
							if (null != enpowerRoleMenuEntity.getName() && !enpowerRoleMenuEntity.getName().isEmpty()) {
								boolean isCompare = enpowerRoleMenuEntity.getName().stream().anyMatch(b->{
									return b.trim().equals(role.getName().trim());
								});
								if (isCompare){//enpowerRoleMenuEntity.getName().contains(role.getName().trim())) {
									for (String m : enpowerRoleMenuEntity.getMenus()) {
										Privilege privilege = privilegeService.findByUri(m);
										if (null != privilege) {
											roleMenu.add(privilege.getMenu());
											Set<Menu> results = getParentMenus(privilege.getMenu());
											if (null != results) {
												roleMenu.addAll(results);
											}
										}
									}
								}
							}
						}
					}
					if (null != all) {
						roleMenu.addAll(all);
					}
					List<Menu> oldMenus = role.getMenus();//保留已有的权限不动。
					if(oldMenus!=null){
						roleMenu.addAll(oldMenus);
					}
					role.setMenus(new ArrayList<>(roleMenu));
					roleService.add(role);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		log.info("独立线程中菜单重建完成。");
	}
	
	private Set<Menu> getParentMenus(Menu menu){
		if (null != menu) {
			Set<Menu> menus = new HashSet<Menu>();
			Menu parent = menu.getParent();
			 
			if (null != parent) {
				menus.add(parent);
				Set<Menu> results = getParentMenus(parent);
				if (null != results) {
					menus.addAll(results);
				}
			}
			return menus;
		}
		return null;
	}
	
	//递归处理角色成树的转换工作--重要
	private Set<EnpowerResponseRole> buildEnpowerRoleTree(Set<EnpowerResponseRole> enpowerResponseRoles, EnpowerResponseRole parent, List<EnpowerRoleEntity> subRole) {
				
		Set<EnpowerResponseRole> results = new HashSet<EnpowerResponseRole>(); 
		
		if (null != enpowerResponseRoles && null != parent && null != subRole) {
			for (EnpowerRoleEntity role : subRole) {
				for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
					if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("id")) {
						if (enpowerResponseRole.getRoleId().equals(role.getId()) && !enpowerResponseRole.getRoleId().equals(parent.getRoleId())) {
							enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, role.getSubRole()));
							results.add(enpowerResponseRole);
						}
					} else if (CommonUtility.isNonEmpty(enpowerRolePolicy) && enpowerRolePolicy.equals("name")) {
						if (enpowerResponseRole.getRoleName().equals(role.getName()) && !enpowerResponseRole.getRoleName().equals(parent.getRoleName())) {
							enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, role.getSubRole()));
							results.add(enpowerResponseRole);
						}
					}
				}
			}
			return results;
		}
		
		return null;
	}
	
	//将用户里的所有角色提取成单独的角色对象
	private Set<EnpowerResponseRole> transferRole(List<EnpowerResponseUser> enpowerResponseUsers){
		if (null != enpowerResponseUsers && !enpowerResponseUsers.isEmpty()) {
			Set<EnpowerResponseRole> roles = enpowerResponseUsers.stream().map(user -> {
				EnpowerResponseRole role = new EnpowerResponseRole();
				role.setRoleId(user.getRoleId());
				role.setRoleName(user.getRoleName());
				role.setRoleNo(user.getRoleNo());
				role.setDepartmentId(user.getDepartmentId());
				return role;
			}).collect(Collectors.toSet());
			
			return roles;
		}
		
		return null;
	}
	
	//将用户里的所有角色提取成单独的角色对象
	private Set<EnpowerResponseRole> retrogradeTopRole(Set<EnpowerResponseRole> enpowerResponseRoles){
		if (null != enpowerResponseRoles && !enpowerResponseRoles.isEmpty()) {
			Set<EnpowerResponseRole> roles = new HashSet<EnpowerResponseRole>();
			Set<String> roleIds = enpowerResponseRoles.stream().map(role -> {
				return role.getRoleId();
			}).collect(Collectors.toSet());
			
			Set<Role> realRoles = enpowerResponseRoles.stream().map(role -> {
				return roleService.findByEnpowerId(role.getRoleId());
			}).collect(Collectors.toSet());
			
			Set<Role> tempRoles = new HashSet<Role>();
			for (Role role : realRoles) {
				if (null != role) {
					Role temp = null;
					Role parentRole = role.getParent();
					while (null != parentRole) {
						if (realRoles.contains(role.getParent())) {
							temp = role;
						}
						parentRole = parentRole.getParent();
					}
					
					if (null != temp && roleIds.contains(temp.getEnpowerId())) {
						tempRoles.add(temp);
					}
				}
			}
			
			if (null != tempRoles && !tempRoles.isEmpty()) {
				roles = tempRoles.stream().map(role -> {
					for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
						if (role.getEnpowerId().equals(enpowerResponseRole.getRoleId())) {
							return enpowerResponseRole; 
						}
					}
					return null;
				}).collect(Collectors.toSet());
				
				return roles;
			}
			
			return null;
		}
		
		return null;
	}
}
