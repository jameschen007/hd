package com.easycms.hd.api.enpower.response;

import java.util.Set;

import com.easycms.hd.api.enpower.domain.EnpowerRole;
import com.easycms.hd.mobile.domain.Modules;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
@Data
public class EnpowerResponseUser {
//	"用户ID":"8ba4ed4e5ae04992b0b760d3e54fe84c",
	@SerializedName(value = "用户ID")
	private String userId;
//    "登录名":"yanw",
	@SerializedName(value = "登录名")
	private String loginId;
//    "中文名":"闫外",
	@SerializedName(value = "中文名")
	private String realName;
//    "激活状态":1,
	@SerializedName(value = "激活状态")
	private Integer status;
//    "组织机构ID":"11f5d0c7488e442889d6fc0759ea79f5",
	@SerializedName(value = "组织机构ID")
	private String departmentId;
//    "组织机构名称":"物资部",
	@SerializedName(value = "组织机构名称")
	private String departmentName;
//    "组织机构业务编码":"MMO",
	@SerializedName(value = "组织机构业务编码")
	private String departmentBusinessNo;
//    "上级组织机构ID":"92b48fcd23244bdfbaf817daa9fa8ad9",
	@SerializedName(value = "上级组织机构ID")
	private String parentTeamId;
//    "上级组织机构名称":"K2/K3核电项目部",
	@SerializedName(value = "上级组织机构名称")
	private String parentTeamName;
//    "岗位ID":"c96110c8388f4ba6be5b4b956530a127",
	@SerializedName(value = "岗位ID")
	private String roleId;
//    "岗位编码":"1014",
	@SerializedName(value = "岗位编码")
	private String roleNo;
//    "岗位名称":"管道主任工程师",
	@SerializedName(value = "岗位名称")
	private String roleName;
//  "专业":"综合",
	@SerializedName(value = "专业")
	private String specialty;
//  "专业":"综合",
	@SerializedName(value = "所在项目")
	private String mineProject;
//    "TOTAL_COUNT":571,
	@SerializedName(value = "TOTAL_COUNT")
	private Integer totalCount;
//    "ROW_NUM":1
	@SerializedName(value = "ROW_NUM")
	private Integer rowNum;
	
	private Set<Modules> modules;
	private Set<EnpowerRole> roles;
	
}
