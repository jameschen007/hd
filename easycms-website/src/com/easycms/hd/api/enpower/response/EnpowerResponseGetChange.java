package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EnpowerResponseGetChange implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	/**文档类型: "正式工程图纸",*/
	@SerializedName(value = "文档类型")
	private String docType;
/**文件类型: "安装文件",*/
	@SerializedName(value = "文件类型")
	private String fileType;
/**版本: "A",*/
	@SerializedName(value = "版本")
	private String version;
/**内部文件编号: "07123B54BZS08-892",*/
	@SerializedName(value = "内部文件编号")
	private String innerFileCode;
/**文件编码: "KK3B5416892B40743SD",*/
	@SerializedName(value = "文件编码")
	private String fileCode;
/**中文标题: "支吊架组装图",*/
	@SerializedName(value = "中文标题")
	private String cnTitle;
/**项目代号: "K2K3",*/
	@SerializedName(value = "项目代号")
	private String projCode;
/**公司代号: "050812"*/
	@SerializedName(value = "公司代号")
	private String compCode;
	/**"状态":"INV"*/
	@SerializedName(value = "状态")
	private String status ;
	/**"备注":"remark"*/
	@SerializedName(value = "备注")
	private String remark ;
}
