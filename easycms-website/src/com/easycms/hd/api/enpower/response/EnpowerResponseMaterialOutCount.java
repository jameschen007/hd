package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;
/**
 * 查询今日出库量（按部门）接口
 * @author ul-webdev
 *
 */
@Data
public class EnpowerResponseMaterialOutCount implements Serializable{
	private static final long serialVersionUID = 1L;
	/**出库单位*/
	private String ISS_DEPT;
	/**出库数量*/
	private String TS;
}
