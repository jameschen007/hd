package com.easycms.hd.api.enpower;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower/material")
@Api(value = "enpowerMaterialController", description = "enpowerMaterialController相关api")
public class EnpowerMaterialController {
	
	private static Logger logger = Logger.getLogger(EnpowerMaterialController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

//    /**
//     * 物项入库(未核实)查询接口
//     */
//    @ResponseBody
//    @ApiOperation(value = "物项入库(未核实)查询(汇总)接口，只有单条数据", notes = "物项入库(未核实)查询(汇总)接口，只有单条数据")
//    @RequestMapping(value = "/materialStoreSearch", method = RequestMethod.GET)
//    public JsonResult<MaterialResult> materialStoreSearch(HttpServletRequest request, HttpServletResponse response, 
//            @RequestHeader(required=true, defaultValue="enpower") String domain,
//            @ApiParam(required = true, name = "warrantyNo", value = "质保编号",defaultValue="CS-APP-002") @RequestParam(value="warrantyNo",required=true) String warrantyNo,
//			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {
//
//        JsonResult<MaterialResult> result = new JsonResult<MaterialResult>();
//        try{
//        	MaterialResult ret = executePushToEnpowerServiceImpl.materialStoreSearchUnexamined(warrantyNo);
//        	result.setResponseResult(ret);
//        }catch(Exception e){
//            e.printStackTrace();
//            result.setMessage("操作异常:"+e.getMessage());
//            return result;
//        }
//        return result;
//    }

    /**
     * 物项入库核实接口   
     */
    @ResponseBody
    @ApiOperation(value = "物项入库核实接口  ", notes = "物项入库核实接口   ")
    @RequestMapping(value = "/materialStoreCheck", method = RequestMethod.POST)
    public JsonResult<Boolean> materialStoreCheck (HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") EnpowerRequestMaterialStoreCheckForm form,
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {
        JsonResult<Boolean> result = new JsonResult<Boolean>();

        boolean test = true ;
        if(test){//测试数据
        	
        	
        }

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user==null){
        	user = userService.findUserById(loginId);
        }
        try{
            result = executePushToEnpowerServiceImpl.materialStoreCheck(form,user);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(false);
            return result;
        }
        return result;
    }

    
//    /**
//     * 物项出库查询接口   
//     */
//    @ResponseBody
//    @ApiOperation(value = "物项出库(未核实)查询接口", notes = "物项出库(未核实)查询接口")
//    @RequestMapping(value = "/materialOutSearch", method = RequestMethod.GET)
//    public JsonResult<Object> materialOutSearch  (HttpServletRequest request, HttpServletResponse response, 
//            @RequestHeader(required=true, defaultValue="enpower") String domain,
//            @ApiParam(required = true, name = "outNo", value = "出库单号",defaultValue="KK-WXCK-201712-00188") @RequestParam(value="outNo",required=true) String outNo,
//			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {
//
//    	JsonResult<Object> retObj = new JsonResult<Object>();
//    	List<MaterialResult> result = new ArrayList<MaterialResult>();
//        try{
//            result = executePushToEnpowerServiceImpl.materialOutSearchUnexamined(outNo);
//            retObj.setResponseResult(result);
//        }catch(Exception e){
//            e.printStackTrace();
//            retObj.setMessage("操作异常:"+e.getMessage());
//            retObj.setResponseResult(false);
//        }
//        return retObj;
//    }

    /**
     * 物项出库核实接口   
     */
    @ResponseBody
    @ApiOperation(value = "物项出库核实接口  ", notes = "物项出库核实接口   ")
    @RequestMapping(value = "/materialOutCheck", method = RequestMethod.POST)
    public JsonResult<Boolean> materialOutCheck   (HttpServletRequest request, HttpServletResponse response, 
			@ApiParam(required = true, name = "ISSNO", value = "出库单号",defaultValue="KK-WXCK-201712-00188")@RequestParam(value="ISSNO",required=true) String ISSNO,
			@ApiParam(required = true, name = "QTY_RELEASED", value = "核实量")@RequestParam(value="QTY_RELEASED",required=true) String QTY_RELEASED,
			@ApiParam(required = true, name = "WHO_GET", value = "领料人")@RequestParam(value="WHO_GET",required=true) String WHO_GET,
			@ApiParam(required = false, name = "ISSUSEREMARK", value = "备注")@RequestParam(value="ISSUSEREMARK",required=false) String ISSUSEREMARK,
//			@ApiParam(required = true, name = "STK_QTY", value = "库存数量")@RequestParam(value="STK_QTY",required=true) String STK_QTY,
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {

        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            if(user==null){
            	user = userService.findUserById(loginId);
            }
            result = executePushToEnpowerServiceImpl.materialOutCheck(ISSNO,QTY_RELEASED,WHO_GET,ISSUSEREMARK,user);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(false);
            return result;
        }
        return result;
    }
//    /**
//     * 物项退库查询接口   
//     */
//    @ResponseBody
//    @ApiOperation(value = "物项退库(未核实)查询接口", notes = "物项退库(未核实)查询接口")
//    @RequestMapping(value = "/materialCancelSearch", method = RequestMethod.GET)
//    public JsonResult<Object> materialCancelSearch   (HttpServletRequest request, HttpServletResponse response, 
//            @RequestHeader(required=true, defaultValue="enpower") String domain,
//            @ApiParam(required = true, name = "cancelNo", value = "退库单号",defaultValue="TK-KK-WXCK-201707-00192") @RequestParam(value="cancelNo",required=true) String cancelNo,
//			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {
//
//    	JsonResult<Object> retObj = new JsonResult<Object>();
//    	List<MaterialResult> result = new ArrayList<MaterialResult>();
//        try{
//            result = executePushToEnpowerServiceImpl.materialCancelSearchUnexamind(cancelNo);
//            retObj.setResponseResult(result);
//        }catch(Exception e){
//            e.printStackTrace();
//            retObj.setMessage("操作异常:"+e.getMessage());
//        }
//        return retObj;
//    }

    
    /**
     * 物项退库核实接口   
     */
    @ResponseBody
    @ApiOperation(value = "物项退库核实接口", notes = "物项退库核实接口")
    @RequestMapping(value = "/materialCancelCheck", method = RequestMethod.POST)
    public JsonResult<Boolean> materialCancelCheck(HttpServletRequest request, HttpServletResponse response, 
//			@ApiParam(required = true, name = "MATE_CODE", value = "物项编码")@RequestParam(value="MATE_CODE",required=true) String MATE_CODE,
			@ApiParam(required = true, name = "ID", value = "ENPOWER的ID")@RequestParam(value="ID",required=true) String ID,
			@ApiParam(required = true, name = "ISSNO", value = "退库单号")@RequestParam(value="ISSNO",required=true) String ISSNO,
			@ApiParam(required = true, name = "ISSQTY", value = "退库量")@RequestParam(value="ISSQTY",required=true) String ISSQTY,
			@ApiParam(required = true, name = "QTY_RELEASED", value = "核实量")@RequestParam(value="QTY_RELEASED",required=true) String QTY_RELEASED,
//			@ApiParam(required = true, name = "STK_QTY", value = "库存数量")@RequestParam(value="STK_QTY",required=true) String STK_QTY,
			@ApiParam(required = true, name = "WAREH_CODE", value = "存储仓库")@RequestParam(value="WAREH_CODE",required=true) String WAREH_CODE,
			@ApiParam(required = true, name = "WAREH_PLACE", value = "货位")@RequestParam(value="WAREH_PLACE",required=true) String WAREH_PLACE,
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId
			) {

    	JsonResult<Boolean> result = new JsonResult<Boolean>();
   		if(QTY_RELEASED==null || !QTY_RELEASED.matches("-\\d+")){
			result.setCode(ExceptionCode.E4);
            result.setMessage("退库核实量限制必须输入负值！");
            result.setResponseResult(false);
            return result;
    	}
        try{
            result = executePushToEnpowerServiceImpl.materialCancelCheck(ID,ISSNO, ISSQTY, QTY_RELEASED,WAREH_CODE,WAREH_PLACE);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(false);
            return result;
        }
        return result;
    }
    
	
}
