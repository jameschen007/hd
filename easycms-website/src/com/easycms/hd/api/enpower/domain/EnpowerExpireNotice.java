package com.easycms.hd.api.enpower.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.conference.domain.ExpireNoticeFile;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "enpower_expire_notice")
public class EnpowerExpireNotice extends BasicForm {
	private static final long serialVersionUID = 7136267007264102616L;

	@Id
	@GeneratedValue
	@Column(name = "id ")
	private Integer id;//

	@Column(name = "canc_code")
	private String cancCode;// 文件失效通知单编号

	@Column(name = "ask_return_date")
	private Date askReturnDate;// 要求归还时间

	@Column(name = "release_date ")
	private Date releaseDate;// 发布时间

	@Column(name = "proj_code")
	private String projCode;// 项目代号

	@Column(name = "comp_code")
	private String compCode;// 公司代码

	@Column(name = "drafter")
	private String drafter;// 编制人id

	@Column(name = "draft_date ")
	private Date draftDate;// 编制日期

	@Column(name = "signer")
	private String signer;// 审批人id

	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "notice")
	private List<ExpireNoticeFile> cancFileList;
	
	
}
