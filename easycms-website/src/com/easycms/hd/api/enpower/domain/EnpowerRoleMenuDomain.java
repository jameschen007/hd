package com.easycms.hd.api.enpower.domain;

import java.util.List;

import lombok.Data;

@Data
public class EnpowerRoleMenuDomain {
	private List<String> ALL;	//所有角色均有的菜单
	private List<EnpowerRoleMenuEntity> SPLIT;	//指定角色才有的菜单
}

