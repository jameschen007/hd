package com.easycms.hd.api.learning.response;

import java.io.Serializable;

import lombok.Data;

/**
 * 学习开始，返回唯一识别码
 * @author Mao.Zeng@MG
 *
 */
@Data
public class LearningBeginResult implements Serializable {
	private static final long serialVersionUID = -994647560925172667L;
	
	private String uniqueCode;

}
