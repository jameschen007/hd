package com.easycms.hd.api.function;

import java.util.ArrayList;
import java.util.List;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.response.EnpowerExpireNoticeResult;
import com.easycms.hd.api.response.ExpireNoticeFileResult;
import com.easycms.hd.api.response.ExpireNoticeReaderResult;
import com.easycms.hd.conference.domain.ExpireNoticeFile;
import com.easycms.hd.conference.domain.ExpireNoticeReader;

public class EnpowerExpireNoticeFunction {

	public EnpowerExpireNoticeResult translate(EnpowerExpireNotice o,boolean transFile){
		EnpowerExpireNoticeResult n = new EnpowerExpireNoticeResult();
		
		if(o!=null){
			n.setId(o.getId());
			n.setCancCode(o.getCancCode());
			n.setAskReturnDate(o.getAskReturnDate());
			n.setReleaseDate(o.getReleaseDate());
			n.setDrafter(o.getDrafter());
			if(o.getProjCode()==null || "".equals(o.getProjCode())){
				o.setProjCode(EnpConstant.PROJ_CODE);
			}
			n.setProjName(ConstantVar.projBegin+o.getProjCode()+ConstantVar.projEnd);
			if(transFile){//详情页需要

				List<ExpireNoticeFile> cancFileList = o.getCancFileList();
				if(cancFileList!=null && cancFileList.size()>0){
					List<ExpireNoticeFileResult> fList = new ArrayList<ExpireNoticeFileResult>();
					cancFileList.stream().forEach(fileList->{
						ExpireNoticeFileResult nf = new ExpireNoticeFileResult();
						nf.setId(fileList.getId());
						nf.setCancCode(fileList.getCancCode());
						nf.setCancId(fileList.getCancId());
						nf.setIntercode(fileList.getIntercode());
						nf.setCTitle(fileList.getCTitle());
						nf.setRevsion(fileList.getRevsion());
						nf.setStatus(fileList.getStatus());
						
						List<ExpireNoticeReader> userList = fileList.getUserList();
						if(userList!=null && userList.size()>0){
							List<ExpireNoticeReaderResult> uList = new ArrayList<ExpireNoticeReaderResult>();
							userList.stream().forEach(user->{
								ExpireNoticeReaderResult us = new ExpireNoticeReaderResult();
								us.setId(user.getId());
								us.setCancId(user.getCancId());
								us.setBorNum(user.getBorNum());
								us.setBorrower(user.getBorrower());
								us.setLoginname(user.getLoginname());
								us.setLoginId(user.getLoginId());
								us.setStatus(user.getStatus());
								
								//如果没有借阅人　也没有使用人，则APP上不显示份数了。
								if(CommonUtility.isNonEmpty(user.getBorrower())||CommonUtility.isNonEmpty(user.getLoginname())){

									uList.add(us);
								}
							});
							
							nf.setUserList(uList);
						}
						
						fList.add(nf);
						
					});
					n.setCancFileList(fList);
				}
			}
		}
		
		return n;
	}
}
