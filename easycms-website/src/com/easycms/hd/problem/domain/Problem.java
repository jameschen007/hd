package com.easycms.hd.problem.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rolling_plan_question")
public class Problem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9005823978677644494L;
	public static final Integer TO_SOLVE = 1;
	public static final Integer UN_SOLVED = 0;
	public static final Integer SOLVED = 2;
	public static final Integer CREATED = 3;
	public static final Integer TO_CONFIRM = 4;
	public static final Integer TO_CONCERN = 5;

	@Id
	@Column(name = "autoid")
	@GeneratedValue
	private int id;
	@Column(name = "autoidup")
	private int worstepid;
	@Column(name = "weldno")
	private String weldno;
	@Column(name = "drawno")
	private String drawno;
	@Column(name = "questiondes")
	private String describe;
	@Column(name = "questionname")
	private String questionname;
	@Column(name = "isok")
	private int isOk;
	@Column(name = "queslevel")
	private int level;
	@Column(name = "remark")
	private String remark;
	@Column(name = "solvemethod")
	private String solvemethod;
	@Column(name = "confirm")
	private int confirm;
	@Column(name = "methodmanid")
	private int methodmanid;
	@Column(name = "solvedate")
	private Date solvedate;
	@Column(name = "concernman")
	private String concerman;
	@Column(name = "concernmanname")
	private String concermanname;
	@Column(name = "currentsolver")
	private String currentsolver;
	@Column(name = "currentsolverid")
	private String currentsolverid;
	@Column(name = "rolling_plan_id")
	private Integer rollingPlanId;
	@Column(name = "created_on")
	private Date createOn;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_by_name")
    private String createdByName;
	@Column(name = "updated_on")
	private Date updateOn;
	@Column(name = "updated_by")
	private String updateBy;

	@Column(name = "solver_id")
	private Integer solverId;

	public Integer getSolverId() {
		return solverId;
	}

	public void setSolverId(Integer solverId) {
		this.solverId = solverId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWorstepid() {
		return worstepid;
	}

	public void setWorstepid(int worstepid) {
		this.worstepid = worstepid;
	}

	public String getWeldno() {
		return weldno;
	}

	public void setWeldno(String weldno) {
		this.weldno = weldno;
	}

	public String getDrawno() {
		return drawno;
	}

	public void setDrawno(String drawno) {
		this.drawno = drawno;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getQuestionname() {
		return questionname;
	}

	public void setQuestionname(String questionname) {
		this.questionname = questionname;
	}

	public int getIsOk() {
		return isOk;
	}

	public void setIsOk(int isOk) {
		this.isOk = isOk;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getSolvemethod() {
		return solvemethod;
	}

	public void setSolvemethod(String solvemethod) {
		this.solvemethod = solvemethod;
	}

	public int getMethodmanid() {
		return methodmanid;
	}

	public void setMethodmanid(int methodmanid) {
		this.methodmanid = methodmanid;
	}

	public int getConfirm() {
		return confirm;
	}

	public void setConfirm(int confirm) {
		this.confirm = confirm;
	}

	public Date getSolvedate() {
		return solvedate;
	}

	public void setSolvedate(Date solvedate) {
		this.solvedate = solvedate;
	}

	public String getConcerman() {
		return concerman;
	}

	public void setConcerman(String concerman) {
		this.concerman = concerman;
	}

	public String getCurrentsolver() {
		return currentsolver;
	}

	public void setCurrentsolver(String currentsolver) {
		this.currentsolver = currentsolver;
	}

	public String getCurrentsolverid() {
		return currentsolverid;
	}

	public void setCurrentsolverid(String currentsolverid) {
		this.currentsolverid = currentsolverid;
	}

	public String getConcermanname() {
		return concermanname;
	}

	public void setConcermanname(String concermanname) {
		this.concermanname = concermanname;
	}

	public Integer getRollingPlanId() {
		return rollingPlanId;
	}

	public void setRollingPlanId(Integer rollingPlanId) {
		this.rollingPlanId = rollingPlanId;
	}

	
	public String getCreatedByName() {
    return createdByName;
  }

  public void setCreatedByName(String createdByName) {
    this.createdByName = createdByName;
  }

  @Override
	public String toString() {
		return "Problem [id=" + id + ", worstepid=" + worstepid + ", weldno="
				+ weldno + ", drawno=" + drawno + ", describe=" + describe
				+ ", questionname=" + questionname + ", isOk=" + isOk
				+ ", level=" + level + ", remark=" + remark + ", solvemethod="
				+ solvemethod + ", confirm=" + confirm + ", methodmanid="
				+ methodmanid + ", solvedate=" + solvedate + ", concerman="
				+ concerman + ", concermanname=" + concermanname
				+ ", currentsolver=" + currentsolver + ", currentsolverid="
				+ currentsolverid + ", rollingPlanId=" + rollingPlanId
				+ ", createOn=" + createOn + ", createdBy=" + createdBy
				+ ", updateOn=" + updateOn + ", updateBy=" + updateBy + "]";
	}

}
