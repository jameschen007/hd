package com.easycms.hd.problem.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "solve_question_person")
public class ProblemSolver {
	public static int NOT_SOLVED = 0;
	public static int SOLVED = 1;
	@Id
	@Column(name = "autoid")
	@GeneratedValue
	private int id;
	@Column(name = "problemid")
	private int problemid;
	@Column(name = "okmanid")
	private String solver;
	@Column(name = "okmanname")
	private String solverName;
	@Column(name = "okdes")
	private String describe;
	@Column(name = "okdate")
	private Date solveDate;
	@Column(name = "isok")
	private int isSolve;
	@Column(name = "ison")
	private int currentSolver;
	@Column(name = "stepno")
	private int stepno;
	@Column(name = "oklevel")
	private String solveLevel;
	@Column(name = "created_on")
	private Date createOn;
	@Column(name = "created_by")
	private String createBy;
	@Column(name = "updated_on")
	private Date updateOn;
	@Column(name = "updated_by")
	private String updateBy;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getSolver() {
		return solver;
	}
	public void setSolver(String solver) {
		this.solver = solver;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public Date getSolveDate() {
		return solveDate;
	}
	public void setSolveDate(Date solveDate) {
		this.solveDate = solveDate;
	}
	public int getIsSolve() {
		return isSolve;
	}
	public void setIsSolve(int isSolve) {
		this.isSolve = isSolve;
	}
	public int getCurrentSolver() {
		return currentSolver;
	}
	public void setCurrentSolver(int currentSolver) {
		this.currentSolver = currentSolver;
	}
	public String getSolveLevel() {
		return solveLevel;
	}
	public void setSolveLevel(String solveLevel) {
		this.solveLevel = solveLevel;
	}
	public Date getCreateOn() {
		return createOn;
	}
	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateOn() {
		return updateOn;
	}
	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public int getProblemid() {
		return problemid;
	}
	public void setProblemid(int problemid) {
		this.problemid = problemid;
	}
	
	public int getStepno() {
		return stepno;
	}
	public void setStepno(int stepno) {
		this.stepno = stepno;
	}
	
	public String getSolverName() {
		return solverName;
	}
	public void setSolverName(String solverName) {
		this.solverName = solverName;
	}
	@Override
	public String toString() {
		return "ProblemSolver [id=" + id + ", problemid=" + problemid
				+ ", solver=" + solver + ", describe=" + describe
				+ ", solveDate=" + solveDate + ", isSolve=" + isSolve
				+ ", currentSolver=" + currentSolver + ", solveLevel="
				+ solveLevel + ", createOn=" + createOn + ", createBy="
				+ createBy + ", updateOn=" + updateOn + ", updateBy="
				+ updateBy + "]";
	}

	
}
