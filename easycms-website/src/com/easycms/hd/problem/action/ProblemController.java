package com.easycms.hd.problem.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.JsonUtils;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.plan.domain.Isend;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.RollingPlanFlag;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.domain.ProblemConcernman;
import com.easycms.hd.problem.domain.ProblemData;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.problem.domain.ProblemSolver;
import com.easycms.hd.problem.form.ProblemForm;
import com.easycms.hd.problem.form.ProblemSolverForm;
import com.easycms.hd.problem.service.ProblemConcernmanService;
import com.easycms.hd.problem.service.ProblemFileService;
import com.easycms.hd.problem.service.ProblemService;
import com.easycms.hd.problem.service.ProblemSolverService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.SchedulerService;

@Controller
@RequestMapping("/hd/workstep/problem")
@Transactional
public class ProblemController {
  private Logger logger = Logger.getLogger(ProblemController.class);
  @Autowired
  private RuntimeService runtimeService;
  @Autowired
  private ProblemService problemService;
  @Autowired
  private ProblemFileService problemFileService;
  @Autowired
  private ProblemSolverService problemSolverService;
  @Autowired
  private ProblemConcernmanService problemConcernmanService;
  @Autowired
  private WorkStepService workStepService;
  @Autowired
  private RollingPlanService rollingPlanService;
  @Autowired
  private RoleService roleService;
  @Autowired
  private DepartmentService departmentService;
  @Autowired
  private UserService userService;

  @Autowired
  private VariableSetService varialbleService;

  @Autowired
  private SchedulerService schedulerService;

  @Autowired
  private TaskService taskService;

  @Autowired
  private JPushService jPushService;

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String listUI(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request problem list page ");
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_tosolve");
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public String listData(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_tosolve");
  }

  @RequestMapping(value = "/rollingplan", method = RequestMethod.GET)
  public String listDataByRollingPlanUI(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request problem list  data by rollingplan id UI");
    String rollingPlanId = request.getParameter("rollingPlanId");
    if (rollingPlanId == null || "".equals(rollingPlanId)) {
      rollingPlanId = "0";
    } else {
      rollingPlanId = rollingPlanId.substring(0, rollingPlanId.indexOf("?"));
    }

    request.setAttribute("rollingPlanId", rollingPlanId);
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_rolling_plan");
  }

  @RequestMapping(value = "/rollingplan", method = RequestMethod.POST)
  public String listDataByRollingPlan(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request problem list  data by rollingplan id");

    String rollingPlanId = request.getParameter("rollingPlanId");
    int rid = 0;
    if (rollingPlanId != null && !"".equals(rollingPlanId)) {
      rid = Integer.parseInt(rollingPlanId);
    }

    List<Problem> problems = problemService.findByRollingPlanId(rid);
    request.setAttribute("problems", problems);
    return ForwardUtility
        .forwardAdminView("/hdService/problem/data/data_json_problem_rolling_plan");
  }

  @RequestMapping(value = "/rollingplan/detail", method = RequestMethod.GET)
  public String problemRollingPlanDetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem list  data by rollingplan id");
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);

    logger.debug("===================================current sid:" + id);
    form.setId(Integer.parseInt(id.substring(0, id.indexOf("?"))));
    form.setFilter(CommonUtility.toJson(form));

    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_rolling_plan_detail");
  }

  @RequestMapping(value = "/add", method = RequestMethod.GET)
  public String addUI(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request add problem  page ");

    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());
    // User leader = user.getLeader();

    String rollingPlanId = request.getParameter("id");
    String sid = rollingPlanId.substring(0, rollingPlanId.indexOf("?"));
    int id = rollingPlanId == null ? 0 : Integer.parseInt(sid);
    RollingPlan rp = rollingPlanService.findById(id);
    logger.debug("add problem for rooling plan [" + rp.toString() + "]");
    request.setAttribute("rp", rp);

    List<User> solvers = userService.getSolvers(user);
    request.setAttribute("solvers", solvers);

    return ForwardUtility.forwardAdminView("/hdService/problem/problem_add");
  }

  @RequestMapping(value = "/add", method = RequestMethod.POST)
  public void add(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute ProblemForm problemForm) {
    logger.debug("request add problem  data ");
    logger.debug("add problem input data :[" + JsonUtils.objectToJson(problemForm) + "]");
    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());
    String solver = request.getParameter("solverid");
    // String concernmenid = request.getParameter("concernmen");
    Map<String, Object> variables = new HashMap<String, Object>();
    List<String> solvers = new ArrayList<String>();
    solvers.add(solver);
    variables.put("solver", solvers);

    Problem problem = new Problem();
    problem.setRollingPlanId(problemForm.getRollingPlanId());
    problem.setQuestionname(problemForm.getQuestionname());
    problem.setDescribe(problemForm.getDescribe());

    User suser = userService.findUserById(Integer.parseInt(solver));
    User leader = null;//修改不管这个，这个是以前的无用代码吧suser.getParent();

    problem.setCurrentsolver(suser.getRealname());
    problem.setCurrentsolverid(suser.getId() + "");
    problem.setCreatedByName(user.getRealname());
    problem.setConfirm(0);
    problem.setConcerman(problemForm.getConcernmen());

    problem.setCreatedBy(user.getId()+"");
    problem.setCreateOn(new Date());

    // WorkStep workStep = workStepService.findById(problem.getWorstepid());
    RollingPlan rp = rollingPlanService.findById(problem.getRollingPlanId());
    problem.setWeldno(rp.getWeldno());
    problem.setDrawno(rp.getDrawno());


    problem.setConcermanname(leader.getRealname());

    ProblemSolver problemSolver = new ProblemSolver();
    problemSolver.setIsSolve(ProblemData.IS_NOT_OK);
    problemSolver.setCreateOn(new Date());
    problemSolver.setCreateBy(user.getName());
    problemSolver.setProblemid(problem.getId());
    problemSolver.setSolverName(suser.getRealname());
    problemSolver.setSolver(solver);
    // problemSolver.setStepno(problem.getStepno());
    ProblemSolver ps = problemSolverService.add(problemSolver);

    problem.setSolverId(ps.getId());
    problem = problemService.add(problem);
    ps.setProblemid(problem.getId());
    problemSolverService.add(ps);
    // 添加关注人


    ProblemConcernman man = new ProblemConcernman();
    man.setConcermanid(leader.getId());
    man.setConcernmanname(leader.getRealname());
    man.setProblemid(problem.getId());
    problemConcernmanService.add(man);
    pushDataToMobileConcernmen(leader.getId());

    user = userService.findUserById(user.getId());

    runtimeService.startProcessInstanceByKey(ProblemData.PROCESS_ID, problem.getId() + "",
        variables).getId();
    logger.debug("add problem [" + problem.toString() + "]");
    rp.setDoissuedate(new Date());
    rp.setRollingplanflag(RollingPlanFlag.PROBLEM);
    rollingPlanService.update(rp);

    List<String> solverIds = new ArrayList<String>();
    solverIds.add(solver);
    logger.debug("add problem output data :[" + JsonUtils.objectToJson(problem) + "]");
    request.getSession().setAttribute("currentWorkStepAutoIdUp", rp.getId());

    problemUpgrade(problem, leader);

    pushDataToMobileSolver(solverIds);
    WebUtility.writeToClient(response, "" + problem.getId());
  }

  private void problemUpgrade(Problem problem, User leader) {
    String tp = varialbleService.findValueByKey("changeSolver", "problem");
    String changeSolverDuration = tp == null || tp == "" ? "1" : tp;
    Calendar calendar = Calendar.getInstance();
    // calendar.add(Calendar.DAY_OF_MONTH, changeSolverDuration);
    calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(changeSolverDuration));
    Map<String, Object> jobDataMap = new HashMap<String, Object>();
    Map<String, Object> jobMap = new HashMap<String, Object>();
    jobMap.put("problemid", problem.getId());
    jobMap.put("leader", leader);
    jobDataMap.put("jobService", "problemAutoChangeSolverService");
    jobDataMap.put("jobDataMap", jobMap);
    schedulerService.schedule(calendar.getTime(), jobDataMap);
  }

  /**
   * 推送信息解决人手机
   * 
   * @param solverIds
   */
  private void pushDataToMobileSolver(List<String> solverIds) {

    JPushExtra extra = new JPushExtra();
    extra.setCategory("solve");
    extra.setData("");
    extra.setRemark("");
    for (String string : solverIds) {
      User user = userService.findUserById(Integer.parseInt(string));
      jPushService.pushByUserId(extra, "你有一条问题需要处理", "", user.getId());
    }
  }

  /**
   * 将信息推送至关注人手机
   * 
   * @param concernIds
   */
  private void pushDataToMobileConcernmen(Integer concernId) {
    JPushExtra extra = new JPushExtra();
    extra.setCategory("concern");
    extra.setData("");
    extra.setRemark("");
    User user = userService.findUserById(concernId);
    jPushService.pushByUserId(extra, "你有一条问题需要关注", "", user.getId());
  }

  /**
   * 将信息推送至确认人手机
   * 
   * @param confirmerids
   */
  private void pushDataToMobileConfirmer(List<String> confirmerids) {
    JPushExtra extra = new JPushExtra();
    extra.setCategory("confirm");
    extra.setData("");
    extra.setRemark("");
    for (String string : confirmerids) {
      User user = userService.findUserById(Integer.parseInt(string));
      jPushService.pushByUserId(extra, "你有一条问题需要确认", "", user.getId());
    }
  }

  @RequestMapping(value = "/solved/detail", method = RequestMethod.GET)
  public String solvedDetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());

    logger.debug("===================================current sid:" + id);
    form.setId(iid);
    form.setFilter(CommonUtility.toJson(form));

    request.setAttribute("rp", rp);
    
    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_solved_detail");
  }

  @RequestMapping(value = "/unsolved/detail", method = RequestMethod.GET)
  public String unsolvedDetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);

    logger.debug("===================================current sid:" + id);
    form.setId(Integer.parseInt(id.substring(0, id.indexOf("?"))));
    form.setFilter(CommonUtility.toJson(form));
    
    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_unsolved_detail");
  }

  @RequestMapping(value = "/created/detail", method = RequestMethod.GET)
  public String createdDetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);

    logger.debug("===================================current sid:" + id);
    form.setId(Integer.parseInt(id.substring(0, id.indexOf("?"))));
    form.setFilter(CommonUtility.toJson(form));
    
    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_created_detail");
  }

  @RequestMapping(value = "/confirm/detail", method = RequestMethod.GET)
  public String confirmDetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);
    logger.debug("===================================current sid:" + id);
    form.setId(Integer.parseInt(id.substring(0, id.indexOf("?"))));
    form.setFilter(CommonUtility.toJson(form));
    
    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_confirm_detail");
  }

  @RequestMapping(value = "/detail", method = RequestMethod.GET)
  public String detail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem  detail data ");
    String id = request.getParameter("tid");

    Integer iid = Integer.parseInt(id.substring(0, id.indexOf("?")));
    Problem p = problemService.findById(iid);
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);

    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);

    logger.debug("===================================current sid:" + id);
    form.setId(Integer.parseInt(id.substring(0, id.indexOf("?"))));
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_tosolve_detail");
  }

  @RequestMapping(value = "/upload", method = RequestMethod.GET)
  public String uploadUI(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request upload problem  file ");
    String problemId = request.getParameter("problemId");
    request.setAttribute("problemId", problemId.substring(0, problemId.indexOf("?")));
    request.setAttribute("autoidup", request.getSession().getAttribute("currentWorkStepAutoIdUp"));
    return ForwardUtility.forwardAdminView("/hdService/problem/problem_add_upload");
  }

  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public void upload(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request upload problem  file ");

    Integer problemId =
        Integer.parseInt(request.getParameter("problemId") == null ? "0" : request
            .getParameter("problemId"));
    String filePath = "problem_files";
    List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
    for (FileInfo fileInfo : fileInfos) {
      ProblemFile file = new ProblemFile();
      file.setPath(fileInfo.getNewFilename());
      file.setTime(new Date());
      file.setProblemid(problemId);
      file.setFileName(fileInfo.getFilename());
      logger.debug("upload file success : " + file.toString());
      problemFileService.add(file);
    }
    WebUtility.writeToClient(response, "true");
  }

  @RequestMapping(value = "/handle", method = RequestMethod.GET)
  public String handleUI(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request handle problem  ui ");
    String problemId = request.getParameter("id");
    request.setAttribute("id", problemId);
    User user = (User) request.getSession().getAttribute("user");
    user = userService.findUserById(user.getId());
    List<User> solvers = userService.getSolvers(user);
    request.setAttribute("solvers", solvers);
    return ForwardUtility.forwardAdminView("/hdService/problem/modal_problem_handle");
  }

  @RequestMapping(value = "/handle", method = RequestMethod.POST)
  public void handle(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request handle problem ");
    String problemId = request.getParameter("id");
    String isSolve = request.getParameter("isSolve");
    String method = request.getParameter("solvemethod");
    String solverid = request.getParameter("solverid");
    Problem problem = problemService.findById(Integer.parseInt(problemId));
    User user = (User) WebUtility.getSession().getAttribute("user");

    user = userService.findUserById(user.getId());

    Map<String, Object> variables = new HashMap<String, Object>();
    List<Task> tasks = null;

    List<String> solverIds = new ArrayList<String>();
    List<String> confirmerIds = new ArrayList<String>();

    tasks =
        taskService.createTaskQuery().taskCandidateUser(problem.getCurrentsolverid() + "").list();
    if (tasks != null) {
      for (Task task : tasks) {
        ProcessInstance pi =
            runtimeService.createProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId()).singleResult();
        if (problemId.equalsIgnoreCase(pi.getBusinessKey())) {
          // 如果能解决
          if ("1".equalsIgnoreCase(isSolve)) {
            variables.put("solved", true);
            variables.put("worker", problem.getCreatedBy());
            taskService.complete(task.getId(), variables);

            List<Problem> ps = problemService.findByWorkstepId(problem.getWorstepid());
            logger.debug("current problem size :" + ps.size());
            problem.setIsOk(ProblemData.IS_OK);
            problem.setUpdateBy(user.getId() + "");
            problem.setUpdateOn(new Date());
            problem.setSolvemethod(method);
            problem.setMethodmanid(user.getId());
            problem.setConfirm(-1);
            problem.setSolvedate(new Date());
            problem.setCurrentsolver(user.getRealname());
            problem.setCurrentsolverid(user.getId() + "");
            problemService.update(problem);

            // 将需要被推送的用户id加入准备推送list存放
            confirmerIds.add(problem.getCreatedBy());
            break;
            // 如果不能解决
          } else if ("0".equalsIgnoreCase(isSolve)) {

            logger.debug("current user " + user.getName());
            List<String> ids = new ArrayList<String>();
            ids.add(solverid);
            variables.put("solved", false);
            variables.put("solver", ids);

            // 将需要被推送的用户id加入准备推送list存放
            solverIds.add(solverid);

            problem.setLevel(problem.getLevel() + 1);
            problem.setUpdateOn(new Date());
            User suser = userService.findUserById(Integer.parseInt(solverid));
            problem.setCurrentsolver(suser.getRealname());
            problem.setCurrentsolverid(suser.getId() + "");
            problem = problemService.update(problem);
            taskService.complete(task.getId(), variables);
            ProblemSolver problemSolver = new ProblemSolver();
            problemSolver.setIsSolve(ProblemData.IS_NOT_OK);
            problemSolver.setCreateOn(new Date());
            problemSolver.setCreateBy(user.getName());
            problemSolver.setProblemid(problem.getId());
            problemSolver.setSolverName(suser.getRealname());
            problemSolver.setSolver(solverid);
            problemSolverService.add(problemSolver);

            ProblemSolver olPS = problemSolverService.findById(problem.getSolverId());
            ProblemSolver ps = problemSolverService.add(problemSolver);
            problem.setSolverId(ps.getId());

            olPS.setDescribe(method);
            problemSolverService.add(olPS);

            problemService.update(problem);

            //修改不管这个，这个是以前的无用代码吧，把它注释掉
//            problemUpgrade(problem, suser.getParent());
//            addConcernmans(problem, suser.getParent());
            break;
          }
        }
      }
    }

    pushDataToMobileSolver(solverIds);
    pushDataToMobileConfirmer(confirmerIds);

    WebUtility.writeToClient(response, "true");
  }

  private void addConcernmans(Problem problem, User leader) {
    if (leader == null) {
      return;
    }
    String concernmen = problem.getConcermanname();
    ProblemConcernman concernman = new ProblemConcernman();
    concernman.setConcermanid(leader.getId());
    concernman.setConcernmanname(leader.getRealname());
    concernman.setProblemid(problem.getId());
    ProblemConcernman pc =
        problemConcernmanService.findByUserIdAndProblemId(leader.getId(), problem.getId());
    if (pc == null) {
      problemConcernmanService.add(concernman);
      if (concernman == null || "".equals(concernmen)) {
        concernmen = leader.getRealname();
      } else {
        concernmen += "|" + leader.getRealname();
      }
    }

    if (concernmen == null || "".equals(concernmen)) {
      return;
    }

    problem.setConcermanname(concernmen);
    problemService.update(problem);

  }


  @RequestMapping(value = "/solved", method = RequestMethod.POST)
  public String solved(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemSolverForm form) {
    logger.debug("request problem solved list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_solved");
  }

  @RequestMapping(value = "/solved", method = RequestMethod.GET)
  public String solvedUI(HttpServletRequest request, HttpServletResponse response) {
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_solved");
  }

  @RequestMapping(value = "/unsolved", method = RequestMethod.POST)
  public String unsolved(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemSolverForm form) {
    logger.debug("request problem unsolved list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_unsolved");
  }

  @RequestMapping(value = "/unsolved", method = RequestMethod.GET)
  public String unsolvedUI(HttpServletRequest request, HttpServletResponse response) {
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_unsolved");
  }

  @RequestMapping(value = "/created", method = RequestMethod.GET)
  public String createdUI(HttpServletRequest request, HttpServletResponse response) {
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_created");
  }

  @RequestMapping(value = "/created", method = RequestMethod.POST)
  public String created(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemSolverForm form) {
    logger.debug("request problem created list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_created");
  }

  @RequestMapping(value = "/toconfirm", method = RequestMethod.GET)
  public String toconfirmUI(HttpServletRequest request, HttpServletResponse response) {

    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_toconfirm");
  }

  @RequestMapping(value = "/toconfirm", method = RequestMethod.POST)
  public String toconfirm(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemSolverForm form) {

    logger.debug("request problem created list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_toconfirm");
  }

  @RequestMapping(value = "/solveconfirm", method = RequestMethod.GET)
  public String solveconfirmUI(HttpServletRequest request, HttpServletResponse response) {
    String id = request.getParameter("id");
    request.setAttribute("id", id);
    return ForwardUtility.forwardAdminView("/hdService/problem/modal_problem_solve_confirm");
  }

  @RequestMapping(value = "/toconcerned", method = RequestMethod.GET)
  public String toconcernedUI(HttpServletRequest request, HttpServletResponse response) {
    return ForwardUtility.forwardAdminView("/hdService/problem/list_problem_toconcerned");
  }

  @RequestMapping(value = "/toconcerned", method = RequestMethod.POST)
  public String toconcerned(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemSolverForm form) {
    logger.debug("request problem created list  data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/problem/data/data_json_problem_toconcerned");
  }

  @RequestMapping(value = "/toconcerned/detail", method = RequestMethod.GET)
  public String toconcerneddetail(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") ProblemForm form) {
    logger.debug("request problem concerned detail data ");
    String tid = request.getParameter("tid");
    int id = Integer.parseInt(tid.substring(0, tid.indexOf("?")));
    logger.debug("===================================current sid:" + id);
    form.setId(id);
    form.setFilter(CommonUtility.toJson(form));

    Problem p = problemService.findById(id);
    User user = (User) request.getSession().getAttribute("user");
    int ProblemUserId = Integer.parseInt(p.getCreatedBy() == null ? "0" : p.getCreatedBy());
    User puser = userService.findUserById(ProblemUserId);

    List<ProblemConcernman> concernmans = problemConcernmanService.findByProblemId(p.getId());
    request.setAttribute("concernmans", concernmans);

    User leader =null;//修改不管这个，这个是以前的无用代码吧 puser.getParent();
    if (user.getId() == leader.getId()) {
      logger.debug("user is problem created leader");
      request.setAttribute("isLeader", true);
    }
    RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
    request.setAttribute("rp", rp);
    
    List<ProblemSolver> solvers = problemSolverService.findByProblemId(p.getId());
    request.setAttribute("solvers", solvers);
    
    return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_concerned_detail");
  }

  @RequestMapping(value = "/solveconfirm", method = RequestMethod.POST)
  public void solveconfirm(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request problem solveconfirm ");
    String problemId = request.getParameter("id");
    String isWork = request.getParameter("isWork");
    Problem p = problemService.findById(Integer.parseInt(problemId));
    Map<String, Object> variables = new HashMap<String, Object>();
    List<Task> tasks =
        taskService.createTaskQuery().taskAssignee(WebUtility.getUserIdFromSession(request)).list();
    for (Task task : tasks) {
      ProcessInstance pi =
          runtimeService.createProcessInstanceQuery()
              .processInstanceId(task.getProcessInstanceId()).singleResult();
      if (pi.getBusinessKey().equals(problemId)) {
        // 方法无效
        if ("0".equals(isWork)) {
          variables.put("works", false);
          p.setIsOk(ProblemData.IS_NOT_OK);
          p.setConfirm(0);
          p.setUpdateOn(new Date());
          taskService.complete(task.getId(), variables);
          break;
          // 方法有效
        } else if ("1".equals(isWork)) {
          variables.put("works", true);
          p.setConfirm(1);

          unlockRollingPlan(p);
          problemService.update(p);
          taskService.complete(task.getId(), variables);
          break;
        }
      }
    }
    problemService.update(p);
    WebUtility.writeToClient(response, "true");

  }

  private void unlockRollingPlan(Problem p) {
    List<Problem> ps = problemService.findByRollingPlanIdAndIsNotOk(p.getRollingPlanId());
    if (ps == null || ps.size() == 0) {
      RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
      rp.setDoissuedate(null);
      rp.setRollingplanflag(null);
      rollingPlanService.update(rp);
    }
  }

  @RequestMapping(value = "/created/delete", method = RequestMethod.GET)
  public void delete(HttpServletRequest request, HttpServletResponse response) {
    logger.debug("request problem delete ");
    String problemId = request.getParameter("problemId");
    boolean result = false;
    if (problemId != null && problemId != "") {
      Problem p = problemService.findById(Integer.parseInt(problemId));
      if (p != null) {
        RollingPlan rp = rollingPlanService.findById(p.getRollingPlanId());
        Map<String, Object> variables = new HashMap<String, Object>();

        List<Task> tasks = taskService.createTaskQuery().list();

        for (Task task : tasks) {
          ProcessInstance pi =
              runtimeService.createProcessInstanceQuery()
                  .processInstanceId(task.getProcessInstanceId()).singleResult();
          logger.debug("business key:" + pi.getBusinessKey());
          logger.debug("problem Id: " + problemId);
          if (pi.getBusinessKey().equals(problemId)) {
            variables.put("solved", "delete");
            taskService.complete(task.getId(), variables);
          }
        }
        problemService.delete(p);
        List<Problem> problems = problemService.findByRollingPlanIdAndIsNotOk(p.getRollingPlanId());
        if (problems == null || problems.size() <= 0) {
          rp.setRollingplanflag(null);
          rollingPlanService.add(rp);
        }
        result = true;
      }
    }
    WebUtility.writeToClient(response, result + "");

  }

}
