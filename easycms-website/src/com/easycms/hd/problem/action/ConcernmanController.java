package com.easycms.hd.problem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;

import com.easycms.common.util.WebUtility;
import com.easycms.hd.problem.service.ProblemConcernmanService;

@Controller
@RequestMapping("/hd/workstep/problem/concernman")
public class ConcernmanController {
  @Autowired
  private ProblemConcernmanService problemConcermanService;
  @RequestMapping("/delete")
  public void delete(HttpServletRequest request, HttpServletResponse response){
    String id = request.getParameter("id");
    Assert.notNull(id);
    int cid = Integer.parseInt(id);
    problemConcermanService.delete(cid);
    WebUtility.writeToClient(response, "true");
  }
}
