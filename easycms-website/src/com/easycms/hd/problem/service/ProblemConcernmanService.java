package com.easycms.hd.problem.service;

import java.util.List;

import com.easycms.hd.problem.domain.ProblemConcernman;

public interface ProblemConcernmanService {
	public ProblemConcernman add(ProblemConcernman man);
	public List<ProblemConcernman> findByUserId(int userid);
	public ProblemConcernman findByUserIdAndProblemId(Integer userid, int problemid);
    public List<ProblemConcernman> findByProblemId(int id);
    public int delete(int cid);
}
