package com.easycms.hd.problem.service;

import java.util.List;

import com.easycms.hd.problem.domain.ProblemFile;

public interface ProblemFileService {
	ProblemFile add(ProblemFile pfile);
	
	List<ProblemFile> findFilesByProblemId(Integer id);
}
