package com.easycms.hd.problem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.problem.dao.ProblemFileDao;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.problem.service.ProblemFileService;

@Service
public class ProblemFileServiceImpl implements ProblemFileService{

	@Autowired
	private ProblemFileDao problemFileDao;
	@Override
	public ProblemFile add(ProblemFile pfile) {
		return problemFileDao.save(pfile);
	}
	@Override
	public List<ProblemFile> findFilesByProblemId(Integer id) {
	
		return problemFileDao.findByProblemId(id);
	}

}
