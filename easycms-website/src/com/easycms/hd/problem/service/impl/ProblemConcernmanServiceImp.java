package com.easycms.hd.problem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.problem.dao.ProblemConcernmanDao;
import com.easycms.hd.problem.domain.ProblemConcernman;
import com.easycms.hd.problem.service.ProblemConcernmanService;

@Service
public class ProblemConcernmanServiceImp implements ProblemConcernmanService{

	@Autowired
	private ProblemConcernmanDao concernmanDao;
	
	@Override
	public ProblemConcernman add(ProblemConcernman man) {
		return concernmanDao.save(man);
	}

	@Override
	public List<ProblemConcernman> findByUserId(int userid) {
		return concernmanDao.findByUserId(userid);
	}

	@Override
	public ProblemConcernman findByUserIdAndProblemId(Integer userid,
			int problemid) {
		return concernmanDao.findByUserIdAndproblemId(userid,problemid);
	}

  @Override
  public List<ProblemConcernman> findByProblemId(int id) {
    return concernmanDao.findByProblemId(id);
  }

  @Override
  public int delete(int cid) {
    return concernmanDao.deleteById(cid);
  }

}
