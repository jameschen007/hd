package com.easycms.hd.problem.service;

import java.util.List;

import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.domain.ProblemSolver;

public interface ProblemSolverService {
	public ProblemSolver add(ProblemSolver p);
	public ProblemSolver update(ProblemSolver p);
	public ProblemSolver findById(Integer id);
	public List<ProblemSolver> findByProblemId(Integer id);
	public List<ProblemSolver> findByUserId(String id);
	public List<ProblemSolver> findByProblemIdAndUserId(int problemId,String userId);
	public List<ProblemSolver> findByUserIdAndStatus(String userid, int iS_OK);
}
