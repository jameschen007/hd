package com.easycms.hd.problem.directive;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.WebUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.domain.ProblemConcernman;
import com.easycms.hd.problem.domain.ProblemData;
import com.easycms.hd.problem.domain.ProblemSolver;
import com.easycms.hd.problem.service.ProblemConcernmanService;
import com.easycms.hd.problem.service.ProblemService;
import com.easycms.hd.problem.service.ProblemSolverService;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

public class ProblemDirective extends BaseDirective<Problem> {

	@Autowired
	private ProblemService problemService;
	@Autowired
	private ProblemSolverService problemSolverSerivice;
	@Autowired
	private ProblemConcernmanService problemConcernmanService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;

	// @Autowired
	// private WorkflowHelper workflowHelper;

	private Logger logger = Logger.getLogger(ProblemDirective.class);

	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Problem field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			Problem p = problemService.findById(id);
			return p;
		}

		return null;
	}

	@Override
	protected List<Problem> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Problem> pager,
			Map<String, Object> envParams) {
		Integer problemType = FreemarkerTemplateUtility.getIntValueFromParams(
				params, "problemType");
		String userid = WebUtility
					.getUserIdFromSession(WebUtility.getRequest());
		User user = userService.findUserById(Integer.parseInt(userid));
		List<Problem> problems = null;
		switch (problemType) {
		// 待解决问题
		case 1:
			problems = getToSolveProblemsByUser(user.getId() + "");
			break;
		// 未能解决
		case 0:
			problems = getUnSolvedProblemsByUser(user.getId() + "");
			break;
		// 已经解决
		case 2:
			problems = getSolvedProblemsByUser(user.getId() + "");
			break;
			// 发起的问题
		case 3:
		  problems = problemService.findByUserId(user.getId() + "");
		  break;
			//待确认的问题
		case 4:
		  problems = getConfirmedProblems(user.getId() + "");
		  break;
		  // 关注的问题
		case 5:
			problems = getConcernedProblemsByUser(user.getId() + "");
			break;
		default:
			break;
		}
		return problems;
	}

	private List<Problem> getConcernedProblemsByUser(String userid) {
		List<Problem> problems = new ArrayList<Problem>();
		List<ProblemConcernman> mans = problemConcernmanService.findByUserId(Integer.parseInt(userid));
		if(mans != null && mans.size() > 0){
			Integer[] ids = new Integer[mans.size()];
			for (int i = 0; i < ids.length; i++) {
				ids[i] = mans.get(i).getProblemid();
			}
			problems =  problemService.findByIds(ids);
		}
		return problems;
	}

	   private List<Problem> getConfirmedProblems(String userId) {
	      List<Task> tasks;
	      List<Problem> problems = new ArrayList<Problem>();
	      tasks = taskService.createTaskQuery().taskAssignee(userId).list();
	      if (tasks != null && tasks.size() > 0) {
	        Integer[] ids = new Integer[tasks.size()];
	        for (int i = 0; i < tasks.size(); i++) {
	            Task task = tasks.get(i);
	            ProcessInstance pi = runtimeService
	                    .createProcessInstanceQuery()
	                    .processInstanceId(task.getProcessInstanceId())
	                    .singleResult();
	            ids[i] = Integer.parseInt(pi.getBusinessKey());
	        }

	        problems = problemService.findConfirmByIdsAndStatus(ids,-1);
	    }
	    
	    return problems;
	  }
	private List<Problem> getSolvedProblemsByUser(String userid) {
		List<ProblemSolver> solvers;
		List<Problem> problems = new ArrayList<Problem>();
//		solvers = problemSolverSerivice.findByUserIdAndStatus(userid,
//				ProblemData.IS_OK);
		solvers = problemSolverSerivice.findByUserId(userid);
		if (solvers != null && solvers.size() > 0) {

			Integer[] ids = new Integer[solvers.size()];
			for (int i = 0; i < solvers.size(); i++) {
			  ids[i] = solvers.get(i).getProblemid();
		    }
//			problems = problemService.findByIdsAndStatus(ids, ProblemData.IS_OK);
			problems = problemService.findByIdsAndStatus(ids, ProblemData.IS_OK);
		}
		return problems;
	}

	private List<Problem> getUnSolvedProblemsByUser(String userid) {
		List<ProblemSolver> solvers;
		List<Problem> problems = new ArrayList<Problem>();
//		solvers = problemSolverSerivice.findByUserIdAndStatus(userid,
//				ProblemData.IS_NOT_OK);
		solvers = problemSolverSerivice.findByUserId(userid);
		if (solvers != null && solvers.size() > 0) {

			Integer[] ids = new Integer[solvers.size()];
			for (int i = 0; i < solvers.size(); i++) {
		      ids[i] = solvers.get(i).getProblemid();
		    }
			//problems = problemService.findByIds(ids);
			problems = problemService.findByIdsAndStatus(ids,ProblemData.IS_NOT_OK);
		}
		return problems;
	}

	private List<Problem> getToSolveProblemsByUser(String userId) {
		List<Task> tasks;
		List<Problem> problems = new ArrayList<Problem>();
		tasks = taskService.createTaskQuery().taskCandidateUser(userId).list();
		if (tasks != null && tasks.size() > 0) {
			Integer[] ids = new Integer[tasks.size()];
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				ProcessInstance pi = runtimeService
						.createProcessInstanceQuery()
						.processInstanceId(task.getProcessInstanceId())
						.singleResult();
				ids[i] = Integer.parseInt(pi.getBusinessKey());
			}

			problems =  problemService.findByIdsAndStatus(ids,ProblemData.IS_NOT_OK);
		}
		return problems;
	}

	@Override
	protected List<Problem> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
