package com.easycms.hd.problem.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.problem.service.ProblemFileService;

public class ProblemFileDirective extends BaseDirective<ProblemFile> {

	@Autowired
	private ProblemFileService problemFileService;
	

	private Logger logger = Logger.getLogger(ProblemFileDirective.class);
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ProblemFile field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			//ProblemFile p = problemFileService.findById(id);
		//	return p;
		}

		return null;
	}

	@Override
	protected List<ProblemFile> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<ProblemFile> pager,
			Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		List<ProblemFile> files = problemFileService.findFilesByProblemId(id);
		return files;
	}

	@Override
	protected List<ProblemFile> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
