package com.easycms.hd.problem.directive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.common.util.JsonUtils;
import com.easycms.core.directive.LocationDirective;
import com.easycms.core.util.ContextUtil;
import com.easycms.hd.plan.domain.FakeMap;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.UserService;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

public class OrgRelationshipDirective implements TemplateDirectiveModel {
	private static final Logger logger = Logger.getLogger(LocationDirective.class);

	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel.getDefaultObjectWrapper();
	
	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private UserService userService;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if(loopVars.length < 1) {
			throw new RuntimeException("Loop variable is required.");
		}
		logger.info("[OrgRelationshipDirective] ==> start");
		
		HttpServletRequest request = ContextUtil.getRequest();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		User user = userService.findUserById(loginUser.getId());
		
		Map<String, List<FakeMap>> map = new HashMap<String, List<FakeMap>>();
		
//		if (null != user){
//			List<Role> roles = user.getRoles();
			
//			List<FakeMap> leadership = new ArrayList<FakeMap>();
//			if (null != roles && !roles.isEmpty()){
//				for (Role r : roles){
//					if (null != r.getDepartment()){
//						if (null != r.getDepartment().getParent()){
//							FakeMap fake = new FakeMap();
//							fake.setKey(r.getDepartment().getParent().getId() + "");
//							fake.setValue(r.getDepartment().getParent().getName());
//							leadership.add(fake);
//						}
//					}
//				}
//			}
//			map.put("leadership", leadership);
//			logger.info("leadership ==> " + JsonUtils.objectToJson(leadership));
//			List<FakeMap> colleague = new ArrayList<FakeMap>();
//			if (null != roles && !roles.isEmpty()){
//				for (Role r : roles){
//					if (null != r.getDepartment()){
//						List<Role> rls = r.getDepartment().getRoles();
//						for (Role rl : rls){
//							List<User> users = rl.getUsers();
//							for (User u : users){
//								if (u.getId() != user.getId()){
//									FakeMap fake = new FakeMap();
//									fake.setKey(u.getId() + "");
//									fake.setValue(u.getName());
//									colleague.add(fake);
//								}
//							}
//						}
//						FakeMap fake = new FakeMap();
//						fake.setKey(r.getDepartment().getId() + "");
//						fake.setValue(r.getDepartment().getName());
//						colleague.add(fake);
//					}
//				}
//			}
//			map.put("colleague", colleague);
//			logger.info("colleague ==> " + JsonUtils.objectToJson(colleague));
//			List<FakeMap> subordinate = new ArrayList<FakeMap>();
//			if (null != roles && !roles.isEmpty()){
//				for (Role r : roles){
//					if (null != r.getDepartment()){
//						List<Department> department = r.getDepartment().getChildren();
//						if (null != department && !department.isEmpty()){
//							for (Department d : department){
//								List<Role> rls = d.getRoles();
//								for (Role rl : rls){
//									List<User> users = rl.getUsers();
//									for (User u : users){
//										FakeMap fake = new FakeMap();
//										fake.setKey(u.getId() + "");
//										fake.setValue(u.getName());
//										subordinate.add(fake);
//									}
//								}
//								FakeMap fake = new FakeMap();
//								fake.setKey(d.getId() + "");
//								fake.setValue(d.getName());
//								subordinate.add(fake);
//							}
//						}
//					}
//				}
//			}
//			map.put("subordinate", subordinate);
//			logger.info("subordinate ==> " + JsonUtils.objectToJson(subordinate));
//		}
		
		loopVars[0] = DEFAULT_WRAPER.wrap(map);
		body.render(env.getOut());
	}
}
