package com.easycms.hd.problem.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.problem.domain.ProblemConcernman;

@Transactional
public interface ProblemConcernmanDao extends Repository<ProblemConcernman,Integer>,BasicDao<ProblemConcernman>{


	@Query("FROM ProblemConcernman p where p.concermanid = ?1 ")
	List<ProblemConcernman> findByUserId(int userid);

	@Query("FROM ProblemConcernman p where p.concermanid = ?1 and p.problemid =?2 ")
	ProblemConcernman findByUserIdAndproblemId(Integer userid, int problemid);

	@Query("FROM ProblemConcernman p where p.problemid =?1 ")
	List<ProblemConcernman> findByProblemId(int id);
	
	int deleteById(int id);
}
