package com.easycms.hd.problem.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.problem.domain.ProblemChangeTrigger;

@Transactional
public interface ProblemChangeTriggerDao extends Repository<ProblemChangeTrigger,Integer>,BasicDao<ProblemChangeTrigger>{
	ProblemChangeTrigger findByName(String name);
}
