package com.easycms.hd.problem.setting.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;

@Controller
@RequestMapping("/baseservice/setting")
public class SolverChangeTriggerSettingController {
    @Autowired
	private VariableSetService variableSetService;
	@RequestMapping(value = "/problem",method = RequestMethod.GET)
	public String settingUI(HttpServletRequest request,HttpServletResponse response){
		VariableSet set = variableSetService.findByKey("changeSolver", "problem");
		request.setAttribute("id", set.getId());
		request.setAttribute("duration", set.getSetvalue());
		return ForwardUtility.forwardAdminView("/hdService/problem/page_problem_setting");
	}
	@RequestMapping(value = "/problem",method = RequestMethod.POST)
	public void setting(HttpServletRequest request,HttpServletResponse response){
		String id = request.getParameter("id");
		VariableSet set = null;
		User user = (User) request.getSession().getAttribute("user");
		if(id == null || "".equals(id)){
			set = new VariableSet();
			set.setSetkey("changeSolver");
			set.setType("problem");
			set.setCreateby(user.getName());
			set.setCreateon(new Date());
		}else{
			set = variableSetService.findById(Integer.parseInt(id));
		}
		set.setSetvalue(request.getParameter("duration"));
		variableSetService.update(set);
		WebUtility.writeToClient(response, "true");
	}
}
