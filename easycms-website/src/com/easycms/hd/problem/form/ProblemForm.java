package com.easycms.hd.problem.form;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;


public class ProblemForm extends BasicForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	//private int workstepid;
	//private int stepno;
	private String stepname;
	private String describe;
	private int rollingPlanId;
	private String filePath;
	private String solverid;
	private String questionname;
	private String concernmen;

	public int getRollingPlanId() {
		return rollingPlanId;
	}
	public void setRollingPlanId(int rollingPlanId) {
		this.rollingPlanId = rollingPlanId;
	}
	public String getConcernmen() {
		return concernmen;
	}
	public void setConcernmen(String concernmen) {
		this.concernmen = concernmen;
	}
	public String getQuestionname() {
		return questionname;
	}
	public void setQuestionname(String questionname) {
		this.questionname = questionname;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getSolverid() {
		return solverid;
	}
	public void setSolverid(String solverid) {
		this.solverid = solverid;
	}
	public String getStepname() {
		return stepname;
	}
	public void setStepname(String stepname) {
		this.stepname = stepname;
	}
	
}
