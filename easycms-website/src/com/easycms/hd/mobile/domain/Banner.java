package com.easycms.hd.mobile.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "banner")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class Banner extends BasicForm implements Serializable {
	private static final long serialVersionUID = -3226339763747329773L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "description", length = 500)
	private String description;
	@Column(name = "url", length = 250)
	private String url;
	@Column(name = "alt", length = 50)
	private String alt;
	@Column(name = "order_num")
	private Integer orderNumber;
	@JsonIgnore
	@Column(name = "status", length = 10)
	private String status;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private String createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private String updateBy;
}