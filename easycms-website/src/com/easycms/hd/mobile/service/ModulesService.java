package com.easycms.hd.mobile.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.mobile.domain.Modules;


public interface ModulesService {
	

	Modules add(Modules modules);
	boolean batchDelete(Integer[] ids);
	List<Modules> findByAll();
	List<Modules> findByStatus(String status);
	Modules findByType(String type);
	Modules findById(Integer id);
	Modules findByEnpowerName(String enpowerName);
	
	Page<Modules> findByStatus(String status, Page<Modules> page);
	Page<Modules> findByStatusAndSection(String status,String section, Page<Modules> page);
}
