package com.easycms.hd.material.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.material.dao.MaterialExtractDao;
import com.easycms.hd.material.domain.MaterialExtract;
import com.easycms.hd.material.service.MaterialExtractService;

@Service("materialExtractService")
public class MaterialExtractServiceImpl implements MaterialExtractService {

	@Autowired
	private MaterialExtractDao materialExtractDao;

	@Override
	public MaterialExtract add(MaterialExtract materialExtract) {
		return materialExtractDao.save(materialExtract);
	}

	@Override
	public Page<MaterialExtract> findByPage(Integer departmentId, Page<MaterialExtract> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<MaterialExtract> springPage = materialExtractDao.findByPage(departmentId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}


	@Override
	public MaterialExtract findById(Integer id) {
		return materialExtractDao.findById(id);
	}

	@Override
	public boolean batchRemove(Integer[] ids) {
		if (0 != materialExtractDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<MaterialExtract> findByWarrantyNo(String warrantyNo) {
		if (CommonUtility.isNonEmpty(warrantyNo)) {
			return materialExtractDao.findByWarrantyNo(warrantyNo);
		}
		return null;
	}
}
