package com.easycms.hd.materialtype.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.materialtype.domain.MaterialType;
import com.easycms.hd.materialtype.service.MaterialTypeService;
import com.easycms.management.user.domain.User;
import com.jhlabs.image.LightFilter.Material;

@Component
public class MaterialTypeDirective extends BaseDirective<MaterialType>{

	private Logger logger = Logger.getLogger(MaterialTypeDirective.class);
	@Autowired
	private MaterialTypeService materialTypeService;
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected MaterialType field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			MaterialType materialType = materialTypeService.findById(id);
			return materialType;
		}

		return null;
	}

	@Override
	protected List<MaterialType> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<MaterialType> pager,
			Map<String, Object> envParams) {

		
		MaterialType condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(MaterialType.class, filter,"yyyy-MM-dd");
		}else{
			condition = new MaterialType();
		}
		
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}

		// 查询列表
		if (pageable) {
			pager = materialTypeService.findPage(condition, pager);
			return pager.getDatas();
		} else {
			List<MaterialType> datas = materialTypeService.findAll(condition);
			logger.debug("==> Material type length [" + datas.size() + "]");
			return datas;
		}
	}
		

	@Override
	protected List<MaterialType> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
