package com.easycms.hd.materialtype.form;

import java.io.Serializable;
import java.util.Date;
import com.easycms.core.form.BasicForm;

/**
 * 
 * @author zhangjian
 * desc 材料类型类表单
 *
 */
public class MaterialTypeForm extends BasicForm implements Serializable{
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String materialTypeId;
	private String MaterialTypeName;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaterialTypeId() {
		return materialTypeId;
	}

	public void setMaterialTypeId(String materialTypeId) {
		this.materialTypeId = materialTypeId;
	}

	public String getMaterialTypeName() {
		return MaterialTypeName;
	}

	public void setMaterialTypeName(String materialTypeName) {
		MaterialTypeName = materialTypeName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

//	public String[] getIds() {
//		return ids;
//	}
//
//	public void setIds(String[] ids) {
//		this.ids = ids;
//	}


}
