package com.easycms.hd.materialtype.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.hd.materialtype.form.MaterialTypeForm;

/** 
 * @author zhangjian: 
 * @areate Date:2015-04-09 
 * 
 */
@Component
public class MaterialTypeFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(MaterialTypeForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "materialTypeId",
		"form.materialTypeId.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "materialTypeName",
		"form.materialTypeName.empty");
	}
}
