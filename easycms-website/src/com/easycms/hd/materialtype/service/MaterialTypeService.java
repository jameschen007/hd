package com.easycms.hd.materialtype.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.materialtype.domain.MaterialType;
/**
 * 
 * @author zhangjian
 * 2015.04.09
 */
public interface MaterialTypeService {
	List<MaterialType> findAll();

	MaterialType findById(Integer id);

	MaterialType add(MaterialType mt);

	Integer deleteById(Integer id);

	MaterialType update(MaterialType mt);

	Page<MaterialType> findPage(MaterialType condition, Page<MaterialType> page);

	List<MaterialType> findAll(MaterialType condition);

	boolean materialTypeExists(String materialTypeId);

	Integer delete(Integer[] ids);

}
