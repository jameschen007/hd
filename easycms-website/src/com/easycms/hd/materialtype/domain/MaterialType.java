package com.easycms.hd.materialtype.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

/**
 * 
 * @author zhangjian desc 材料类型类
 *
 */
@Entity
@Table(name = "material_type")
@Cacheable
public class MaterialType extends BasicForm implements Serializable {

	private static final long serialVersionUID = -1690318268080787686L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", length = 20)
	private Integer id;
	
	@Column(name = "materialtypeid", length = 20)
	private String materialTypeId;

	@Column(name = "materialtypename", length = 20)
	private String materialTypeName;

	@Column(name = "created_on", length = 0)
	private Date createdOn;

	@Column(name = "created_by", length = 40)
	private String createdBy;

	@Column(name = "updated_on", length = 0)
	private Date updatedOn;

	@Column(name = "updated_by", length = 40)
	private String updatedBy;


	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaterialTypeId() {
		return materialTypeId;
	}

	public void setMaterialTypeId(String materialTypeId) {
		this.materialTypeId = materialTypeId;
	}

	public String getMaterialTypeName() {
		return materialTypeName;
	}

	public void setMaterialTypeName(String materialTypeName) {
		this.materialTypeName = materialTypeName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "MaterialType [id=" + id + ", materialTypeName="
				+ materialTypeName + ", createdOn=" + createdOn
				+ ", createdBy=" + createdBy + ", updatedOn=" + updatedOn
				+ ", updatedBy=" + updatedBy + "]";
	}
	
	

}
