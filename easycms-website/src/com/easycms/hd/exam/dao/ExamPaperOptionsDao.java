package com.easycms.hd.exam.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.exam.domain.ExamPaperOptions;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExamPaperOptionsDao extends Repository<ExamPaperOptions,Integer>,BasicDao<ExamPaperOptions>{

	@Modifying
	@Query("DELETE FROM ExamPaperOptions e WHERE e.id IN (?1)")
	int deleteByIds(Integer[] ids);
}
