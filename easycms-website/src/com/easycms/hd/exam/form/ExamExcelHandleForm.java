package com.easycms.hd.exam.form;

import lombok.Data;

@Data
public class ExamExcelHandleForm {
	private String sourceFilePath;
	private String section;
	private String module;
	private Integer fold;
}
