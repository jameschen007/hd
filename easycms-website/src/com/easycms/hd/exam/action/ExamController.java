package com.easycms.hd.exam.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.exam.domain.ExamPaper;
import com.easycms.hd.exam.domain.ExamPaperOptions;
import com.easycms.hd.exam.enums.ExamPagerOptionEnum;
import com.easycms.hd.exam.form.ExamExcelHandleForm;
import com.easycms.hd.exam.service.ExamPaperOptionsService;
import com.easycms.hd.exam.service.ExamPaperService;
import com.easycms.hd.exam.util.ExamPaperExcelUtil;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.response.JsonResult;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/exam")
@Slf4j
public class ExamController {
	
	@Autowired
	private ExamPaperService examPaperService;
	
	@Autowired
	private ExamPaperOptionsService examPaperOptionsService;
	
	@Autowired
	private ThreadPoolTaskExecutor poolTaskExecutor; 
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_exam_path']}")
	private String fileExamPath;
	
	private static final String MODEL = "试题样例表格.xls";
	
	Queue<ExamExcelHandleForm> examExcelHandleFormQueue = new LinkedBlockingQueue<ExamExcelHandleForm>();
	
	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") ExamPaper form) {
		log.debug("==> Show ExamPaper list.");
		String returnString = ForwardUtility.forwardAdminView("/learning/list_exam");
		
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") ExamPaper form) {
		log.debug("==> Show ExamPaper data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_json_exam");
		return returnString;
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.info("download exam sample");
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(MODEL);
		if(null != is) {
			try {
				String mimetype = "application/vnd.ms-excel;charset=utf-8";
		    	response.setContentType(mimetype);
		    	String filename = MODEL;
		    	filename += ".xls";
		    	String headerValue = "attachment;";
		    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
		    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
		    	response.setHeader("Content-Disposition", headerValue);
		    	
		    	
				FileCopyUtils.copy(is, response.getOutputStream());
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") ExamPaper form) {
		log.debug("==> Start delete ExamPaper.");

		Integer[] ids = form.getIds();
		log.debug("==> To delete ExamPaper ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			examPaperService.batchDelete(ids);
		}
		
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete ExamPaper.");
	}
	
	/**
	 * 修改课程
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") ExamPaper form) {
		return ForwardUtility.forwardAdminView("/learning/page_exam_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<String> edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid ExamPaper form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start edit ExamPaper.");
			log.debug("[ExamPaper] ==> " + CommonUtility.toJson(form));
		}
		
		JsonResult<String> result = new JsonResult<String>();
		log.info(CommonUtility.toJson(form));
		
		ExamPaper examPaper = examPaperService.findById(form.getId());
		if (null == examPaper) {
			result.setCode("-1000");
	    	result.setMessage("未能获取题目信息。");
	    	return result;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		examPaper.setNumber(form.getNumber());
		examPaper.setSubject(form.getSubject());
		examPaper.setScore(form.getScore());
		examPaper.setUpdateBy(user.getId());
		examPaper.setUpdateOn(new Date());
		
		if (examPaperService.add(examPaper) != null) {
			result.setMessage("修改成功。");
		} else {
			result.setCode("-1000");
	    	result.setMessage("修改题目信息失败。");
		}
		log.debug("==> End edit ExamPaper.");
		return result;
	}
	
	/**
	 * 添加课程UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/import", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			, @ModelAttribute("form") ExamPaper form) {
		if (log.isDebugEnabled()) {
			log.debug("==> Show add new ExamPaper import UI.");
		}
		return ForwardUtility.forwardAdminView("/learning/modal_exam_import");
	}
	
	@ResponseBody
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public JsonResult<Boolean> importFiles(HttpServletRequest request, HttpServletResponse response
			, @ModelAttribute("form") ExamPaper form){
		String filePath = basePath + fileExamPath;
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
        if (fileInfos != null && fileInfos.size() > 0) {
            for (FileInfo fileInfo : fileInfos) {
            	LearningFile file = new LearningFile();
                file.setPath(fileInfo.getNewFilename());
                file.setTime(new Date());
                String fileName = fileInfo.getFilename();
                if (fileName.length() > 90) {
                	fileName = fileName.substring(0, 90);
                }
                
                String suffix = null;
                if (fileInfo.getNewFilename().contains(".")) {
                	suffix = fileInfo.getNewFilename().substring(fileInfo.getNewFilename().lastIndexOf(".") + 1, fileInfo.getNewFilename().length());
                }
                if (CommonUtility.isNonEmpty(suffix)) {
                	suffix = suffix.toLowerCase();
                }
                
                if ("xls".equals(suffix) || "xlsx".equals(suffix)) {
                	//创建处理线程
                	ExamExcelHandleForm examExcelHandleForm = new ExamExcelHandleForm();
                	examExcelHandleForm.setSourceFilePath(fileInfo.getAbsolutePath());
                	examExcelHandleForm.setFold(form.getFold());
                	examExcelHandleForm.setModule(form.getModule());
                	examExcelHandleForm.setSection(form.getSection());
                	
                	boolean offerResult = examExcelHandleFormQueue.offer(examExcelHandleForm);
    				if (offerResult && (poolTaskExecutor.getPoolSize() < poolTaskExecutor.getMaxPoolSize())){
    					Thread examExcelHandleThread = new Thread(new ExamExcelHandleThread());
    					poolTaskExecutor.execute(examExcelHandleThread);
    				}
                } else {
                	FileUtils.remove(fileInfo.getAbsolutePath());
                	result.setCode("-1000");
    		    	result.setMessage("试题格式有误");
    		    	return result;
                }
            }
            result.setResponseResult(true);
            return result;
        }
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/options", method = RequestMethod.POST)
	public JsonResult<String> options(HttpServletRequest request, HttpServletResponse response
			, @RequestBody ExamPaperOptions form){
		JsonResult<String> result = new JsonResult<String>();
		User user =  ContextUtil.getCurrentLoginUser();
		
		if (form.getId() != null) {
			ExamPaperOptions examPaperOptions = examPaperOptionsService.findById(form.getId());
			if (null != examPaperOptions) {
				examPaperOptions.setNumber(form.getNumber());
				examPaperOptions.setContent(form.getContent());
				if (CommonUtility.isNonEmpty(form.getCorrect())) {
					examPaperOptions.setCorrect("Y");
				} else {
					examPaperOptions.setCorrect(null);
				}
				examPaperOptions.setUpdateBy(user.getId());
				examPaperOptions.setUpdateOn(new Date());
				examPaperOptionsService.add(examPaperOptions);
				result.setMessage("选项修改成功");
			} else {
				result.setCode("-1000");
				result.setMessage("选项没有找到，请重新刷新页面。");
			}
		} else {
			result.setCode("-1000");
			result.setMessage("ID不能为空，请重新刷新页面。");
		}
		
		return result;
	}
	
	/**
	 * 添加课程UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/options/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") ExamPaperOptions form) {
		if (log.isDebugEnabled()) {
			log.debug("==> Show add new learning UI.");
		}
		request.setAttribute("type", ExamPagerOptionEnum.getList());
		return ForwardUtility.forwardAdminView("/learning/modal_exam_options_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/options/add", method = RequestMethod.POST)
	public void optionAdd(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ExamPaperOptions form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start add Exam Paper Options.");
			log.debug("[Learning] ==> " + CommonUtility.toJson(form));
		}
		
		if (null == form.getCustomId()) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		ExamPaper examPaper = examPaperService.findById(form.getCustomId());
		if (null == examPaper) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		form.setExamPaper(examPaper);
		form.setType(form.getType());
		form.setCreateBy(user.getId());
		form.setCreateOn(new Date());
		if (examPaperOptionsService.add(form) != null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
		}
		log.debug("==> End add Exam Paper Options.");
		return;
	}
	
	@ResponseBody
	@RequestMapping(value = "/options/delete", method = RequestMethod.POST)
	public JsonResult<String> optionsDelete(HttpServletRequest request, HttpServletResponse response
			, @RequestParam("id") Integer id){
		JsonResult<String> result = new JsonResult<String>();
		
		if (id != null) {
			ExamPaperOptions examPaperOptions = examPaperOptionsService.findById(id);
			if (null != examPaperOptions) {
				Integer[] ids = {id};
				examPaperOptionsService.batchDelete(ids);
				result.setMessage("选项删除成功");
			} else {
				result.setCode("-1000");
				result.setMessage("选项没有找到，请重新刷新页面。");
			}
		} else {
			result.setCode("-1000");
			result.setMessage("ID不能为空，请重新刷新页面。");
		}
		
		return result;
	}
	
	private class ExamExcelHandleThread implements Runnable {
        public void run() {
			while(!examExcelHandleFormQueue.isEmpty()){
				ExamExcelHandleForm examExcelHandleForm = examExcelHandleFormQueue.poll();
				if (null != examExcelHandleForm){
					List<ExamPaper> examPapers = ExamPaperExcelUtil.readExcelToObj(examExcelHandleForm);
					if (null != examPapers && !examPapers.isEmpty()) {
						examPapers.stream().forEach(examPaper -> {
							examPaperService.add(examPaper);
						});
					}
				}
	    	}
		}
	}
}
