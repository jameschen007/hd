package com.easycms.kpi.underlying.dao;

import com.easycms.basic.BasicDao;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface KpiSummarySheetDao extends BasicDao<KpiSummarySheet> ,Repository<KpiSummarySheet,Integer>{


}
