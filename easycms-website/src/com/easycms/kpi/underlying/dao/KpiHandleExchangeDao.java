package com.easycms.kpi.underlying.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.domain.KpiLib;

import jodd.jtx.meta.Transaction;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电绩效流转状态表		kpi_handle_exchange<br>
 * <b>创建时间：</b>2017-08-20 下午05:18:15<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface KpiHandleExchangeDao extends BasicDao<KpiHandleExchange> ,Repository<KpiHandleExchange,Integer>{

    @Transaction
    @Modifying
	@Query(value="from KpiHandleExchange u where u.monthlyMain.id=?1 order by id")
	 public List<KpiHandleExchange> findByMainId(Integer mainId);
    

	@Modifying
	@Query(value="delete from KpiHandleExchange u where u.monthlyMain.id=?1")
	 public int deleteByMainId(Integer mainId);
	@Modifying
	@Query(value="update KpiHandleExchange u set u.handleUserId=?1 where u.monthlyMain.currentStatus!='kpiOver' and u.handleUserId=?2 and u.segment in ('1.parentOk','3.parentScore','4.parentScore','4.departmentManagerScore','4.chargeLeaderScore','5.generalScore','6.generalScore2')")
	public int updateHandleIdByOldId(Integer newId,Integer oldId);


	@Modifying
	@Query(value="from KpiHandleExchange u where u.monthlyMain.id=?1 and u.handleUserId=?2 and u.segment=?3 and u.segmentStatus='done'")
	public List<KpiHandleExchange> findByMainIdAndHandleUserIdAndSegmentDone(Integer mainId,Integer handleUserId,String segment);
}