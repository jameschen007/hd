package com.easycms.kpi.underlying.action;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.kpi.underlying.form.KpiLibForm;
import com.easycms.kpi.underlying.service.KpiLibMgrService;
import com.easycms.kpi.underlying.service.KpiLibService;
//import com.easycms.hd.variable.domain.VariableSet;
//import com.easycms.hd.variable.service.VariableSetService;

@Controller
public class KpiLibManagerController {
    private static final Logger logger = Logger.getLogger(KpiLibController.class);

    @Autowired
    @Resource
    private KpiLibService kpiLibService ;
    @Autowired
    private KpiLibMgrService kpiLibMgrService;
    /**
     * 人员指标列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/kpi/lib/usersLibList", method = RequestMethod.GET)
    public String list(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiLibForm kpiLibForm) {
        logger.debug("==> Start show KpiLib list.");
        ///kpi/underlying/list_kpi_lib
        ///sys_template/kpi/underlying/list_kpi_lib_usersLibList.html
        return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_lib_usersLibList");
    }

    /**
     * 人员指标列表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "/kpi/lib/usersLibList", method = RequestMethod.POST)
    public String dataList(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiLibForm kpiLibForm) {
        kpiLibForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiLib data list.");
        kpiLibForm.setFilter(CommonUtility.toJson(kpiLibForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiLibForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/data/data_kpi_lib_usersLibList");
        return returnString;
    }
    
    @RequestMapping(value = "/kpi/lib/export", method = RequestMethod.GET)
    public void exportKpiGet(HttpServletRequest request,HttpServletResponse response){
    	String mimetype = "application/msexcel";
    	response.setContentType(mimetype);
    	String filename = CommonUtility.getPropertyFile("kpi.properties").getProperty("kpi.KpiLibExportFileName");
    	filename += ".xlsx";
    	String headerValue = "attachment;";
    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
    	response.setHeader("Content-Disposition", headerValue);
    	HttpUtility.writeToClient(response, "true");
    }
    
    /**
     * 导出指标库
     * @param request
     * @param response
     * @throws IOException 
     */
    @RequestMapping(value = "/kpi/lib/export", method = RequestMethod.POST)
    public void exportKpi(HttpServletRequest request,HttpServletResponse response)	{
    	//设置下载文件信息
    	String mimetype = "application/vnd.ms-excel;charset=utf-8";
    	response.setContentType(mimetype);
    	String filename = CommonUtility.getPropertyFile("kpi.properties").getProperty("kpi.KpiLibExportFileName");
    	filename += ".xlsx";
    	String headerValue = "attachment;";
    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
    	response.setHeader("Content-Disposition", headerValue);

    	//将文件写入流
    	kpiLibMgrService.exportKpiLib(request,response);
    }
    
}
