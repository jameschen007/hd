package com.easycms.kpi.underlying.action;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.constant.KpiTypeConstant;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.form.KpiLibForm;
import com.easycms.kpi.underlying.form.MyLibRealLibForm;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
import com.easycms.kpi.underlying.service.KpiLibService;
import com.easycms.kpi.underlying.service.KpiMonthlyEntryService;
import com.easycms.kpi.underlying.service.KpiMonthlyMainService;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlow;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlowChain;
import com.easycms.kpi.underlying.service.impl.chain.KpiFlowChainNode;
import com.easycms.kpi.underlying.service.impl.sys.MonthTool;
import com.easycms.kpi.underlying.service.impl.sys.PersonnelCategory;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.Data;

//import com.easycms.hd.variable.domain.VariableSet;
//import com.easycms.hd.variable.service.VariableSetService;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>我的指标库表,用于非绩效管理员的指标确定相关		kpi_lib<br>
 * <b>创建时间：</b>2017-08-16 下午11:37:55<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/mylib/")
public class MyKpiLibController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(MyKpiLibController.class);
    @Resource
    private KpiLinkHandleController kpiLinkHandleController ;

    @Autowired
    @Resource
    private KpiLibService kpiLibService;
    @Autowired
    private KpiUserBaseService kpiUserBaseService;
    @Autowired
    private KpiMonthlyMainService kpiMonthlyMainService;
    @Autowired
    private KpiMonthlyEntryService kpiMonthlyEntryService;
    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;
    @Autowired
    private UserService userService;

    /**
     * 我的绩效表，根据当前登录用户的id查询出这个用户的指标库表记录
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "manger", method = RequestMethod.GET)
    public String mangerGet(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiLibForm kpiLibForm,
            @RequestParam(value="mainId",required=true)Integer mainId) {
        logger.debug("==> Start show KpiLib list.");
        logger.debug(CommonUtility.toJson(kpiLibForm));
        Map<String, Object> mp =new HashMap<String,Object>();
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        try {
        	
        	//如果已经确定指标了,直接打开 我的绩效表

        	//主表验证
//        	logger.info("下个月:"+nextMonth);
        	//以评分周期为准，可以修改当前评分月份，选择评分周期不可以跨月，选择的开始时间和结束时间的月份必须是相同的。
        	//没有就添加主表
            KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);//.findKpiMonthlyMainByUserIdMonth(loginUser.getId(), nextMonth);
            if(main==null&&(!"Y".equals(request.getParameter("isMustAdd")))){
            	return "forward:/kpi/mylib/show";
            }
            

            if (false == CommonUtility.isNonEmpty(kpiLibForm.getCustom())) {//为空时候, 大页面

                //被考核人id,取当前登录人员的
                Integer beExaminedId=loginUser.getId();
                KpiUserBase ownBase =kpiUserBaseService.findKpiUserBaseByUserId(beExaminedId);
                if(ownBase==null){
                	ownBase = new KpiUserBase();
                }
                request.setAttribute("ownBase", ownBase);
                
                //查询已有的指标规划

                List<KpiLib> libs = kpiLibService.findByUserIdUseType(beExaminedId, "mine");
                List<KpiLib> libsPage = kpiLibService.findByUserIdUseType(beExaminedId, "import");//岗位值标
                List<KpiLib> libsPage1 = kpiLibService.findByUserIdUseType(0, "import");//通用指标
                if(libsPage1!=null&& libsPage1.size()>0){
                	libsPage.addAll(libsPage1);
                }
                
                List<Integer> hisIds = new ArrayList<Integer>();
                if(libs!=null && libs.size()>0 && libsPage!=null && libsPage.size()>0){
                	for(KpiLib pag:libsPage){

                    	for(KpiLib l:libs){
                    		if(pag.getKpiName().equals(l.getKpiName())){
                        		hisIds.add(pag.getId());
                        		break;
                    		}
                    	}
                	}
                }
                logger.debug(CommonUtility.toJson(hisIds));

                request.setAttribute("hisIds", CommonUtility.toJson(hisIds));
            }
   		 

            List<KpiLib> generalLibs = new ArrayList<>();
            List<KpiLib> purposelLibs = new ArrayList<>();
            KpiLib condition = new KpiLib();
            condition.setUser(loginUser);
            condition.setDel(false);
            condition.setUseType("import");
            condition.setKpiType("general");
            generalLibs = kpiLibService.findAll(condition);
            
            KpiLib condition1 = new KpiLib();
            condition1.setUser(loginUser);
            condition1.setDel(false);
            condition1.setUseType("import");
            condition1.setKpiType("purpose");
            purposelLibs = kpiLibService.findAll(condition1);
            
            request.setAttribute("generalList", generalLibs);
            request.setAttribute("purposeList", purposelLibs);
            

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CommonUtility.isNonEmpty(kpiLibForm.getCustom())) {
            return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_mylib");
        } else {
        	request.setAttribute("isMustAdd", "Y");
        	request.setAttribute("mainId", mainId);
            return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_mylib_page");
        }
    }

    /**
     * 待考核人员　指标显示　，准备选择指标。
     * 指标库表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "manger", method = RequestMethod.POST)
    public String mangerPost(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiLibForm kpiLibForm) {
        kpiLibForm.setSearchValue(request.getParameter("search[value]"));

        logger.debug("==> Show KpiLib data list.");
        kpiLibForm.setFilter(CommonUtility.toJson(kpiLibForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiLibForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/data/data_kpi_mylib");
        return returnString;
    }

//    /**
//     * 新增-指标库表
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/add", method = RequestMethod.GET)
//    public String addUI(HttpServletRequest request, HttpServletResponse response) {
////要为谁配置指标库 直接查询session就可以了。
//        //为他添加什么类型的指标库
//        logger.info("他添加什么类型的指标库="+request.getParameter("kpiType"));
//        request.setAttribute("kpiType", request.getParameter("kpiType"));
//
//        List<User> userList = userService.findAll(new User());
//        request.setAttribute("userList", userList);
//
//        String returnString = ForwardUtility
//                .forwardAdminView("/kpi/underlying/modal_kpi_lib_add");
//        return returnString;
//    }

    /**
     * 保存新增-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(HttpServletRequest request, HttpServletResponse response,
                    @ModelAttribute("kpiLibForm") @Valid KpiLibForm form,
                    BindingResult errors) {
//员工选择指标后，
//这里保存他先择好的指标　入库。
     try {
         Integer[] ids = form.getIds();
         logger.debug("==> To add libs of his [" + CommonUtility.toJson(ids) + "]");
         KpiLib nlib = null;
         HttpSession session = request.getSession();
         User loginUser = (User) session.getAttribute("user");
         Integer beExaminedId = loginUser.getId();
         if (ids != null && ids.length > 0) {
//根据当前人员，添加他选择的绩效指标。
        	 //指标表中　前一个列表查询的是　通用指标和当前用户的专用指标，由绩效管理员初始的指标，使用类型为import 导入类型。
        	 //本人选择后，在这里新增使用类型为‘mine 我的类型’的记录。
        	 //现在要把它选择的存到子表里面，在存子表前先存主表
        	 List<KpiLib> oldToDel = new ArrayList<KpiLib>();
             List<KpiLib> libs = kpiLibService.findByUserIdUseType(beExaminedId, "mine");
        	 List<KpiLib> newLibList = new ArrayList<KpiLib>();
        	 for(Integer newid:ids){
        		 KpiLib olib = kpiLibService.findKpiLibById(newid);
        		 newLibList.add(olib);

                 //查询已有的指标规划
        		 boolean ishav = false;
                 if(libs!=null&&libs.size()>0){
                	 for(KpiLib old:libs){
                		 if(old.getKpiName().equals(olib.getKpiName())&& old.getKpiType().equals(olib.getKpiType())){
                			 ishav = true ;break;
                		 }
                	 }
                 }
                 if(ishav == false){//过去没有,可以填加

            		 //设置新的mine记录
            		 KpiLib addlib = new KpiLib();
            		 if(olib!=null){
            			 addlib.setDel(true);
            			 addlib.setKpiName(olib.getKpiName());
            			 addlib.setUser(userService.findUserById(loginUser.getId()));
            			 addlib.setStakeholder(olib.getStakeholder());
            			 addlib.setKpiType(olib.getKpiType());
            			 addlib.setUseType("mine");
            			 addlib.setDel(false);
            			 kpiLibService.add(addlib);
            		 }
                 }
        	 }
        	 
        	 //把没有选的删除掉
        	 for(KpiLib dbold:libs){
        		 boolean isin = false ;
        		 for(KpiLib newadd: newLibList){

            		 if(dbold.getKpiName().equals(newadd.getKpiName())&& dbold.getKpiType().equals(newadd.getKpiType())){
            			 isin = true ;break;
            		 }
        		 }
        		 if(isin==false){
        			 dbold.setDel(true);
        			 kpiLibService.update(dbold);
        			 
        			 //要实现连动，把刚才那个进入　新增指标　的那个月的　对应的指标也删除掉，其它月份的不要动。
        			 logger.info("libId="+dbold.getId());
        			 
        			 String mainId=request.getParameter("mainId");
        			 logger.info("mainId="+mainId);
        			 kpiMonthlyEntryService.deleteByMainIdAndLibId(Integer.valueOf(mainId),dbold.getId());
        		 }
        	 }
        	 

             HttpUtility.writeToClient(response, "true");
             return ;
         }
         logger.debug("==> To add libs of his. error!"+CommonUtility.toJson(ids));
        } catch (Exception e) {
        	e.printStackTrace();
            logger.error(e);
        }

     	HttpUtility.writeToClient(response, "false");
    }

    private User getHe(HttpServletRequest request){
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        Integer hisId = loginUser.getId();
        User he = userService.findUserById(hisId);
        return he ;
    }
    /**
     * 打开指标规划
     *进入这个有３种情况：１　从菜单进入，此时mainId为空，转到选年月中转页面　２　从中转页面进入带年月needNewMonthMain，
     *３　从添加指标回来的，是带了mainId的
     * @param request
     * @param response
     * @param needNewMonthMain 规划详情页中切换了其它的月份，在realLib之后查询不到时，需要带此参数过来刷新，在这里需要按本参数新增
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String show(HttpServletRequest request,HttpServletResponse response,
    		@RequestParam(value="mainId",required=false)Integer mainId,
    		@RequestParam(value="addNewLlib",required=false)String addNewLlib,
    		@RequestParam(value="needNewMonthMain",required=false)String needNewMonthMain) {
    	boolean isaddNewLlib = addNewLlib!=null&&"Y".equals(addNewLlib);
    	User he = getHe(request);
        Integer hisId = he.getId();
    	//查询基本信息

        KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
    	
    	request.setAttribute("ownBase", userBase);
    	
    	//主表验证
        
        KpiMonthlyMain searchMain = new KpiMonthlyMain();
        String nextMonth = "201709";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 1);
    	nextMonth =new SimpleDateFormat("yyyyMM").format(c.getTime());
    	logger.info("下个月:"+nextMonth);
    	if(needNewMonthMain!=null && !"".equals(needNewMonthMain)){
    		searchMain.setMonth(needNewMonthMain);//从详情页中切换月份，到这里来新增这个月的主信息。
    	}else{
    		if(mainId==null){

            	//直接从菜单进入时的情况
            	return "forward:/kpi/mylib/realYeanMonth";
    		}
    	}
    	searchMain.setOwner(he);
    	//没有就添加主表
        KpiMonthlyMain main = null;
        if(mainId!=null){//通过指定的mainId打开页面
        	 main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        }else if(needNewMonthMain!=null && !"".equals(needNewMonthMain)){
            main = kpiMonthlyMainService.findOrAdd(searchMain);
        }else{
        	//修改前是，从绩效规划进来直接打开详情，现在先到选择年月的页面　
        }
        
        mainId = main.getId();
        //没有打开个 最近的需求是，先进入选择时间页面，直接初始化掉。

        //因为流程表中干系人的数据已经生成后，子表中的指标对应的干系人有变化，这里做一个最新的调整
        kpiLinkHandleController.fixHandleId(mainId);
        try{

            KpiCycleHandle t = new KpiCycleHandle();
            String[] cycleDate = t.getcycleSE(main.getKpiCycle());
            main.setCycleStart(cycleDate[0]);
            main.setCycleEnd(cycleDate[1]);
        }catch(Exception e){
        	e.printStackTrace();
        }
        if(FlowName.selfShowInit.getCode().equals(main.getCurrentStatus())==false && isaddNewLlib){//如果当前状态不在规划确认阶段，不允许添加。
        	isaddNewLlib = false ;
        }
        
        request.setAttribute("main", main);
    	boolean guihuaing = FlowName.selfShowInit.getCode().equals(main.getCurrentStatus());
    	//查询我的指标mine
        KpiLib lib1 = new KpiLib();
        lib1.setUseType("mine");
        lib1.setUser(he);
        lib1.setKpiType("general");
        List<KpiLib> generalLibs = kpiLibService.findAll(lib1);
    	List<KpiMonthlyEntry> glist = new ArrayList<KpiMonthlyEntry>();
        if(guihuaing && generalLibs!=null && generalLibs.size()>0){//只有在规划的时候才根据　我的指标生成记录供显示
        	for(KpiLib g:generalLibs){
        		KpiMonthlyEntry n = new KpiMonthlyEntry();
        		n.setLib(g);
        		n.setMonthlyMain(main);
        		n.setOwner(he);
        		n.setStakeholder(g.getStakeholder());
        		n.setDel(false);
        		glist.add(n);
        	}
        	glist = kpiMonthlyEntryService.findOrAdd(glist,isaddNewLlib);
        }
        //上面的是根据现有的已选规划查询，如果历史的未选的，还需要查询出来
        List<KpiMonthlyEntry> selfAllEntrys = kpiMonthlyEntryService.findByMainId(mainId);
        for(KpiMonthlyEntry dbAlready:selfAllEntrys){
        	boolean isin = false ;
        	for(KpiMonthlyEntry libAlready:glist){
        		if(libAlready!=null&&dbAlready!=null&&
        				libAlready.getId()!=null && dbAlready.getId()!=null&&        				
        				KpiTypeConstant.GENERAL.equals(libAlready.getLib().getKpiType())
        				&& KpiTypeConstant.GENERAL.equals(dbAlready.getLib().getKpiType())
        				&& libAlready.getId().intValue()==dbAlready.getId().intValue()){
        			isin = true ;
        		}
        	}
        	if(isin==false && KpiTypeConstant.GENERAL.equals(dbAlready.getLib().getKpiType())){
        		glist.add(dbAlready);
        	}
        }
        KpiLib lib2 = new KpiLib();
        lib2.setUseType("mine");
        lib2.setUser(he);
        lib2.setKpiType("purpose");
        List<KpiLib> purposelLibs = kpiLibService.findAll(lib2);
    	List<KpiMonthlyEntry> plist = new ArrayList<KpiMonthlyEntry>();
        if(guihuaing && purposelLibs!=null && purposelLibs.size()>0){//只有在规划的时候才根据　我的指标生成记录供显示
        	for(KpiLib g:purposelLibs){
        		KpiMonthlyEntry n = new KpiMonthlyEntry();
        		n.setLib(g);
        		n.setMonthlyMain(main);
        		n.setOwner(he);
        		n.setStakeholder(g.getStakeholder());
        		n.setDel(false);
        		plist.add(n);
        	}
        	plist = kpiMonthlyEntryService.findOrAdd(plist,isaddNewLlib);
        }
        //上面的是根据现有的已选规划查询，如果历史的未选的，还需要查询出来
        for(KpiMonthlyEntry dbAlready:selfAllEntrys){
        	boolean isin = false ;
        	for(KpiMonthlyEntry libAlready:plist){
        		if(libAlready!=null&&dbAlready!=null&&
        				KpiTypeConstant.PURPOSE.equals(libAlready.getLib().getKpiType())
        				&& KpiTypeConstant.PURPOSE.equals(dbAlready.getLib().getKpiType())
        				&&libAlready.getId()!=null && dbAlready.getId()!=null
        				&& libAlready.getId().intValue()==dbAlready.getId().intValue()){
        			isin = true ;
        		}
        	}
        	if(isin==false && KpiTypeConstant.PURPOSE.equals(dbAlready.getLib().getKpiType())){
        		plist.add(dbAlready);
        	}
        }
    	request.setAttribute("generalList", glist);
        request.setAttribute("purposeList", plist);

        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/page_kpi_mylib_show");
        
        logger.debug("准备我的绩效指标规划信息显示！- end");
        return returnString;
    }

    /**
     * 修改-指标库表
     *保存我的绩效表	/kpi/mylib/edit
     删除我的绩效规划
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUI(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
        return ForwardUtility.forwardAdminView("/user-group/modal_kpi_lib_edit");
    }

    /**
     * 保存修改-指标库表
     保存我的绩效表	/kpi/mylib/edit
     删除我的绩效规划

     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") KpiLibForm form) {
        if (logger.isDebugEnabled()) {
            logger.debug("==> Start save edit kpi_lib.");
        }

        // #########################################################
        // 校验提交信息
        // #########################################################
        if (form.getId() == null) {
            HttpUtility.writeToClient(response, "false");
            logger.debug("==> Save edit kpi_lib failed.");
            logger.debug("==> End save edit kpi_lib.");
            return;
        }

        // #########################################################
        // 校验通过
        // #########################################################
        Integer id = form.getId();
//		KpiLib user = userService.findKpiLibById(id);
//		user.setName(form.getName());
//		user.setRealname(form.getRealname());
//		user.setEmail(form.getEmail());
//		userService.update(user);
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End save edit kpi_lib.");
        return;
    }

    /**
     * 批量删除-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/delete")
    public void batchDelete(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
        logger.debug("==> Start delete kpi_lib.");

        Integer[] ids = form.getIds();
        logger.debug("==> To delete kpi_lib [" + CommonUtility.toJson(ids) + "]");
        if (ids != null && ids.length > 0) {
        	
			kpiMonthlyEntryService.delete(ids);
        }
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End delete kpi_lib.");
    }
    

    /**
     * 被考核人提交规划
     * 保存指标规划
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/addPlan", method = RequestMethod.POST)
    public void addPlan(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiLibForm form,
                            @RequestParam(value="mainId",required=true)Integer mainId) {
    	String falseMsg = "";

    	User he = getHe(request);
    	Department hisDepartment = he.getDepartment();
        Integer hisId = he.getId();

        String sId = request.getParameter("sId");//mainId 入参：当前mainId
        KpiUserBase ownBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
        if(ownBase==null||ownBase.getPersonnelCategory()==null || "".equals(ownBase.getPersonnelCategory())){
        	logger.error("你的信息未初始化!! name="+he.getName()+" realname="+he.getRealname());
        	return ;
        }
        //绩效主表
//        KpiMonthlyMain searchMain = new KpiMonthlyMain();
//        String nextMonth = "201709";
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.MONTH, 1);
//    	nextMonth =new SimpleDateFormat("yyyyMM").format(c.getTime());
//    	searchMain.setMonth(nextMonth);
//    	searchMain.setOwner(he);
    	//没有就添加主表
        KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        if(ownBase.getPersonnelCategory()==null || "".equals(ownBase.getPersonnelCategory())){
        	logger.error("人员类别信息未找到! persional category");
        }
        if(main!=null && FlowName.selfShowInit.getCode().equals(main.getCurrentStatus())){
        	
        }else{
        	logger.error("已经完成指标确定环节! mainId="+main.getId());
            HttpUtility.writeToClient(response, "已经完成指标确定环节!");
            return ;
        }

		KpiCycleHandle ff = new KpiCycleHandle();
		main.setKpiCycle(ff.handle(main.getKpiCycle(), form.getCycleStart(), form.getCycleEnd()));
		
		kpiMonthlyMainService.update(main);
		
        //保存规划到 子表
        
        List<KpiMonthlyEntry> entrys = kpiMonthlyEntryService.findByMainId(main.getId());
        Map<String,String> map = new HashMap<String,String>();
        Enumeration enum2 =request.getParameterNames();  
        while (enum2.hasMoreElements()) {  
            String paramName = (String) enum2.nextElement();  
	   	    String paramValue = request.getParameter(paramName);  
	   	    map.put(paramName, paramValue);  
   	   }
        //保存备注
        String mark = map.get("mark");
        logger.info("mark="+mark);
        if(CommonUtility.isNonEmpty(mark)){
        	if(mark.length()>200){
        		mark = mark.substring(0,200);
        	}
        	main.setMark(StringEscapeUtils.escapeHtml4(mark));
        }

 	   int generalSize = 0 ;
 	   int purposeSize = 0 ;
 	   int generalWeight=0;
 	   int purposeWeight=0;
       if(entrys!=null && entrys.size()>0){
    	   for(KpiMonthlyEntry entry:entrys){
    		   if("general".equals(entry.getLib().getKpiType())){

        		   String weight = map.get("melibweight_"+entry.getLib().getId());
        		   String finishTimeStr = map.get("melibid_"+entry.getLib().getId());
        		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        		   if(finishTimeStr==null || "".equals(finishTimeStr)||!finishTimeStr.matches("\\d{4}-\\d{2}-\\d{2}")){
        			   falseMsg+="时间格式不正确!!";
        			   break;
        		   }
        		   try{
        			   entry.setFinishTime(sdf.parse(finishTimeStr));
        			   entry.setWeight(Integer.valueOf(weight));
        			   generalWeight+=Integer.valueOf(weight);
        		   }catch(Exception e){
        			   e.printStackTrace();
        		   }
        		   generalSize++ ;
    		   }else if("purpose".equals(entry.getLib().getKpiType())){

        		   String weight = map.get("melibweight_"+entry.getLib().getId());
        		   String finishTimeStr = map.get("melibid_"+entry.getLib().getId());
        		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        		   if(finishTimeStr==null || "".equals(finishTimeStr)||!finishTimeStr.matches("\\d{4}-\\d{2}-\\d{2}")){
        			   falseMsg+="时间格式不正确!!";
        			   break;
        		   }
        		   try{
        			   entry.setFinishTime(sdf.parse(finishTimeStr));
        			   entry.setWeight(Integer.valueOf(weight));
        			   purposeWeight+=Integer.valueOf(weight);
        		   }catch(Exception e){
        			   e.printStackTrace();
        		   }
        		   purposeSize++ ;
    		   }
    	   }
    	   if(generalSize<2 || generalSize>5){
    		   falseMsg+="通用指标需要2-5条!";
    	   }
    	   if(generalWeight<20 || generalWeight>30){
    		   falseMsg+="通用指标需要占20%~30%的比重!";
    	   }
    	   if(purposeSize<5 || purposeSize>8){
    		   falseMsg+="岗位指标需要5-8条!";
    	   }
    	   if(purposeWeight<70 || purposeWeight>80){
    		   falseMsg+="岗位指标需要占70%~80%的比重!";
    	   }
    	   if((purposeWeight+generalWeight)!=100){
    		   falseMsg+="所有指标的权重之和必须为100%!";
    	   }
    	   if(falseMsg.length()>0){
    		   HttpUtility.writeToClient(response, falseMsg);//"false"+
    		   return  ;
    	   }else{
    		   kpiMonthlyEntryService.updateAll(entrys);
    		   
    	   }
    	   
       }
        

	   


//    	１　将所有后续要参与的人员查询出来，按顺序生成流程记录，保存后当前pending的是直接上级待审核。
        List<KpiHandleExchange> dbFlowData = new ArrayList<KpiHandleExchange>();
        KpiFlowChain flow = KpiFlow.getFlowMap().get(ownBase.getPersonnelCategory());
		KpiFlowChainNode flowNode= flow.getHead();
		for (int i=0;i<flow.getSize();i++){
			logger.info("当前处理环节:"+flowNode.current);
			//上级待直接领导审核
			if(FlowName.parentOk.getCode().equals(flowNode.current)){
				
				if(he.getParent()==null){
					logger.error("用户上级未找到:id="+hisId);
				}else{
					KpiHandleExchange po = new KpiHandleExchange();
					po.setMonthlyMain(main);
					po.setHandleUserId(he.getParent().getId());
					po.setSegment(flowNode.current);//循环出来的
					po.setSegmentStatus("pending");
					po.setVisitStatus("Y");//待直接领导审核

					KpiHandleExchange po2 = new KpiHandleExchange();
					po2.setMonthlyMain(main);
					po2.setHandleUserId(he.getParent().getId());
					po2.setSegment(flowNode.current);//循环出来的
					po2.setSegmentStatus("done");
					po2.setVisitStatus("N");

					dbFlowData.add(po);
					dbFlowData.add(po2);
				}

			}else if(FlowName.selfScore.getCode().equals(flowNode.current)){

				KpiHandleExchange po = new KpiHandleExchange();
				po.setMonthlyMain(main);
				po.setHandleUserId(he.getId());
				po.setSegment(flowNode.current);
				po.setSegmentStatus("pending");
				po.setVisitStatus("N");

				KpiHandleExchange po2 = new KpiHandleExchange();
				po2.setMonthlyMain(main);
				po2.setHandleUserId(he.getId());
				po2.setSegment(flowNode.current);
				po2.setSegmentStatus("done");
				po2.setVisitStatus("N");
				dbFlowData.add(po);
				dbFlowData.add(po2);
			}else if(FlowName.parentScore.getCode().equals(flowNode.current)){
				if(he.getParent()==null){
					logger.error("用户上级未找到:id="+hisId);
			        HttpUtility.writeToClient(response, "你的上级领导未找到！");
			        return ;
				}else{
					KpiHandleExchange po = new KpiHandleExchange();
					po.setMonthlyMain(main);
					po.setHandleUserId(he.getParent().getId());
					po.setSegment(flowNode.current);
					po.setSegmentStatus("pending");
					po.setVisitStatus("N");

					KpiHandleExchange po2 = new KpiHandleExchange();
					po2.setMonthlyMain(main);
					po2.setHandleUserId(he.getParent().getId());
					po2.setSegment(flowNode.current);
					po2.setSegmentStatus("done");
					po2.setVisitStatus("N");
					
					dbFlowData.add(po);
					dbFlowData.add(po2);
				}
			}else if(FlowName.departmentManagerScore.getCode().equals(flowNode.current)){
				if(hisDepartment==null){
					logger.error("你的部门经理未找到:id="+hisId);
			        HttpUtility.writeToClient(response, "你的部门经理未找到！");
			        return ;
				}
				int departmentManagerId = 0 ;
				try{
					hisDepartment.getTopRole();
					for(User u : hisDepartment.getTopRole().getUsers()) {
						if(u.getDepartment() != null &&
								hisDepartment.getId().intValue() == u.getDepartment().getId().intValue()) {
							departmentManagerId =u.getId();
							break;
						}
					}
				}catch(Exception e){
					e.printStackTrace();
			        HttpUtility.writeToClient(response, "你的部门经理未找到！");
			        return ;
				}

				/*
				查找本部门的最高职务的人员,即为部门经理
				*/
				if(departmentManagerId==0){

					logger.error("你的部门经理未找到:id="+hisId);
			        HttpUtility.writeToClient(response, "你的部门经理未找到！");
			        return ;
				}else{
					KpiHandleExchange po = new KpiHandleExchange();
					po.setMonthlyMain(main);
					po.setHandleUserId(departmentManagerId);
					po.setSegment(flowNode.current);
					po.setSegmentStatus("pending");
					po.setVisitStatus("N");

					KpiHandleExchange po2 = new KpiHandleExchange();
					po2.setMonthlyMain(main);
					po2.setHandleUserId(departmentManagerId);
					po2.setSegment(flowNode.current);
					po2.setSegmentStatus("done");
					po2.setVisitStatus("N");
					
					dbFlowData.add(po);
					dbFlowData.add(po2);
				}
			}else if(FlowName.chargeLeaderScore.getCode().equals(flowNode.current)){
				/**
				 * 部门正职是查当前用户的上级,如果是副职查当前用户的上级的上级。因为在系统中部门正职、部门副职的流程是一个流程，
				 * 程序查分管领导的逻辑为：查当前登录用户的直接领导，查到直接领导是不同部门的时，即判断为分管领导。
				 * 
				 * 另一种方法：找部门领导的上级
				 */

				int chargeLeaderId = 0;

				try{
					for(User u : hisDepartment.getTopRole().getUsers()) {
						if(u.getDepartment() != null &&
								hisDepartment.getId().intValue() == u.getDepartment().getId().intValue()) {
							chargeLeaderId =u.getParent().getId();
							break;
						}
					}
				}catch(Exception e){
					e.printStackTrace();
			        HttpUtility.writeToClient(response, "你的分管领导未找到！");
			        return ;
				}
				
				if(chargeLeaderId==0){
					logger.error("你的分管领导未找到:id="+hisId);
			        HttpUtility.writeToClient(response, "你的分管领导未找到！");
			        return ;
				}else{
					KpiHandleExchange po = new KpiHandleExchange();
					po.setMonthlyMain(main);
					po.setHandleUserId(chargeLeaderId);
					po.setSegment(flowNode.current);
					po.setSegmentStatus("pending");
					po.setVisitStatus("N");

					KpiHandleExchange po2 = new KpiHandleExchange();
					po2.setMonthlyMain(main);
					po2.setHandleUserId(chargeLeaderId);
					po2.setSegment(flowNode.current);
					po2.setSegmentStatus("done");
					po2.setVisitStatus("N");
					
					dbFlowData.add(po);
					dbFlowData.add(po2);
				}
			}else if(FlowName.stakeholderScore.getCode().equals(flowNode.current)){
                List<KpiLib> libs = kpiLibService.findByUserIdUseType(hisId, "mine");
                if(libs!=null && libs.size()>0){
                	Set<Integer> set = new HashSet<Integer>();
                	for(KpiLib lb:libs){
                		if(set.add(lb.getStakeholder().getId())){//去重

            				KpiHandleExchange po = new KpiHandleExchange();
            				po.setMonthlyMain(main);
            				po.setHandleUserId(lb.getStakeholder().getId());
            				po.setSegment(flowNode.current);
            				po.setSegmentStatus("pending");
            				po.setVisitStatus("N");

            				KpiHandleExchange po2 = new KpiHandleExchange();
            				po2.setMonthlyMain(main);
            				po2.setHandleUserId(lb.getStakeholder().getId());
            				po2.setSegment(flowNode.current);
            				po2.setSegmentStatus("done");
            				po2.setVisitStatus("N");
            				
            				dbFlowData.add(po);
            				dbFlowData.add(po2);
                		}
                	}
                }
			}else if(FlowName.generalScore.getCode().equals(flowNode.current)
					||FlowName.generalScore2.getCode().equals(flowNode.current)){
				
				//总经理识别
				int generalId = 0 ;
				try{
					for(User u : hisDepartment.getParent().getTopRole().getUsers()) {
						if(u.getDepartment() != null &&
								hisDepartment.getParent().getId().intValue() == u.getDepartment().getId().intValue()) {
							generalId =u.getId();
							break;
						}
					}
				}catch(Exception e){
					e.printStackTrace();
			        HttpUtility.writeToClient(response, "总经理未找到！");
			        return ;
				}
				
				
				if(generalId==0){
					logger.error("总经理未找到:id="+hisId);
			        HttpUtility.writeToClient(response, "总经理未找到！");
			        return ;
				}else{
					KpiHandleExchange po = new KpiHandleExchange();
					po.setMonthlyMain(main);
					po.setHandleUserId(generalId);
					po.setSegment(flowNode.current);
					po.setSegmentStatus("pending");
					po.setVisitStatus("N");

					KpiHandleExchange po2 = new KpiHandleExchange();
					po2.setMonthlyMain(main);
					po2.setHandleUserId(generalId);
					po2.setSegment(flowNode.current);
					po2.setSegmentStatus("done");
					po2.setVisitStatus("N");
					dbFlowData.add(po);
					dbFlowData.add(po2);
				}
			}

			
			//循环下一个环节
			if(flow.searchNode(flowNode.current).nextNode!=null){
				flowNode = flow.searchNode(flowNode.current).nextNode;
			}
		}
		

		//主表状态修改
//		kpi_status 考核状态:待处理环节
//		current_status 当前状态:待处理环节code
//		kpi_type 类型:正在进行或已完成
		main.setKpiStatus(FlowName.parentOk.getMsg());
		main.setCurrentStatus(FlowName.parentOk.getCode());
		main.setKpiType("正在进行");
        kpiMonthlyMainService.update(main);
		
		if(dbFlowData!=null){
			kpiHandleExchangeService.addOrUpdateAll(dbFlowData);
		}

        HttpUtility.writeToClient(response, "true");
        logger.info("指标提交成功完成!");
    }
    /**
     * 打开本月考核自评
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/selfScoreUI", method = {RequestMethod.POST,RequestMethod.GET})
    public String selfScoreUI(HttpServletRequest request,
                            HttpServletResponse response,@RequestParam(value="yyyymm",required=false)String yyyymm) {

    	
    	//切换自评月份
    	request.setAttribute("yyyymm", getYYYYMM());
    	request.setAttribute("cyyyymm", yyyymm==null?getcYYYYMM():yyyymm);
    	
    	User me = getHe(request);
        Integer myId = me.getId();    	

        
        //打开待自评的，
        //直接查询是否有待自评的，不要通过时间进行查询。
        KpiMonthlyMain condition = new KpiMonthlyMain();
        Page<KpiMonthlyMain> pager = new Page<KpiMonthlyMain>();
        User usMe = new User();
        usMe.setId(me.getId());
        condition.setOwner(usMe);
        condition.setCurrentStatus(FlowName.selfScore.getCode());
        
        pager = kpiMonthlyMainService.findPage(condition, pager);
        
        List<KpiMonthlyMain> mains= pager.getDatas();
        String month = null;
        if(mains==null ||mains.size()==0){
        	month = MonthTool.getMonthStr(0) ;
        }else{
            if(yyyymm!=null){//优先打开指定的那个年月的自评
            	for(KpiMonthlyMain main:mains){
            		if(yyyymm.equals(main.getMonth())){
            			month = yyyymm;
            			break;
            		}
            	}
            }else{
            	month = mains.get(0).getMonth();
            }
            
        }
        kpiLinkHandleController.commonShowLink(request,myId,month,true);
        
//    2.selfScore
        return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_selfScore");
    }
    /**
     * 查询我的绩效
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/openScoreUI", method = {RequestMethod.POST,RequestMethod.GET})
    public String openScoreUI(HttpServletRequest request,
                            HttpServletResponse response) {
    	
    	String year = request.getParameter("year");
    	String month = request.getParameter("month");
    	String searchMonth = null ;
    	if(CommonUtility.isNonEmpty(year) && CommonUtility.isNonEmpty(month)){
    		if(month.length()==1){
    			month = "0"+month ;
    		}
    		searchMonth = year + month ;
    	}else{
    		searchMonth = MonthTool.getMonthStr(0);
    	}
    	
    	Calendar c = Calendar.getInstance();

    	List<Integer> yyyy = new  ArrayList<Integer>();
    	List<Integer> mm = new  ArrayList<Integer>();
    	
    	int currentYYYY = c.get(Calendar.YEAR);
    	for(int i=0;i>-3;i--){
    		yyyy.add(currentYYYY+i);
    	}
    	request.setAttribute("yyyys", yyyy);
    	for(int i=1;i<13;i++){
    		mm.add(i);
    	}
    	request.setAttribute("mms", mm);
    	if(CommonUtility.isNonEmpty(year)){
        	request.setAttribute("currentyear", Integer.valueOf(year));
    	}else{
        	request.setAttribute("currentyear", (c.get(Calendar.YEAR))+1);
    	}
    	if(CommonUtility.isNonEmpty(month)){
    		
        	request.setAttribute("currentMonth", Integer.valueOf(month));
    	}else{

        	request.setAttribute("currentMonth", (c.get(Calendar.MONTH))+1);
    	}

    	User me = getHe(request);
        Integer myId = me.getId();
        
        //被考核人id,取当前登录人员的
//        KpiUserBase ownBase =kpiUserBaseService.findKpiUserBaseByUserId(myId);
//        if(ownBase==null){
//        	ownBase = new KpiUserBase();
//        }
//        request.setAttribute("ownBase", ownBase);//用于页面验证
        
        kpiLinkHandleController.commonShowLink(request,myId,searchMonth,false);
        return ForwardUtility.forwardAdminView("/kpi/score/page_mylib_seeScore");
    }
    /**
     * 绩效规划前的选择时间页面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "realYeanMonth", method = {RequestMethod.GET,RequestMethod.POST})
    public String realYeanMonth(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiLibForm kpiLibForm) {
        kpiLibForm.setSearchValue(request.getParameter("search[value]"));
        

    	String year = request.getParameter("year");
    	String month = request.getParameter("month");
    	String searchMonth = null ;
    	if(CommonUtility.isNonEmpty(year) && CommonUtility.isNonEmpty(month)){
    		if(month.length()==1){
    			month = "0"+month ;
    		}
    		searchMonth = year + month ;
    	}else{
    		searchMonth = MonthTool.getMonthStr(0);
    	}
    	
    	Calendar c = Calendar.getInstance();

    	List<Integer> yyyy = new  ArrayList<Integer>();
    	List<Integer> mm = new  ArrayList<Integer>();
    	
    	int currentYYYY = c.get(Calendar.YEAR);
    	for(int i=1;i>-3;i--){
    		yyyy.add(currentYYYY+i);
    	}
    	request.setAttribute("yyyys", yyyy);
    	for(int i=1;i<13;i++){
    		mm.add(i);
    	}
    	request.setAttribute("mms", mm);
    	if(CommonUtility.isNonEmpty(year)){
        	request.setAttribute("currentyear", Integer.valueOf(year));
    	}else{
        	request.setAttribute("currentyear", (c.get(Calendar.YEAR))+1);
    	}
    	if(CommonUtility.isNonEmpty(month)){
    		
        	request.setAttribute("currentMonth", Integer.valueOf(month));
    	}else{

        	request.setAttribute("currentMonth", (c.get(Calendar.MONTH))+1);
    	}

    	User me = getHe(request);
        Integer myId = me.getId();
        

        logger.debug("==> Show KpiLib data list.");
        return ForwardUtility.forwardAdminView("/kpi/underlying/page_mylib_entry");
    }

    
    /**
     * 实时修改规划指标值 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "realLib", method = {RequestMethod.POST,RequestMethod.GET})
    public void realLib(HttpServletRequest request,HttpServletResponse response,
    		@ModelAttribute("form")MyLibRealLibForm form,@RequestParam(value="mainId",required=true)Integer mainId
    		) {
    	
    	logger.debug("当前考核mainId："+mainId);

    	String modifyDate=request.getParameter("modifyDate");
    	String newDate = request.getParameter("newDate");
    	Integer userId = form.getUserId();
    	KpiMonthlyMain kmm = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
    	logger.debug("当前考核状态："+kmm.getCurrentStatus());
    	if(!FlowName.selfShowInit.getCode().equals(kmm.getCurrentStatus())){
    		if("start".equals(modifyDate)){
        		Integer monthMainId = kpiMonthlyMainService.getMainIdByUserIdAndStartDate(userId, newDate);
        		if(monthMainId==null){//如果没有查到，但本主信息已经提交规划后，在评分阶段,需要新增
        			KpiCycleHandle ff = new KpiCycleHandle();
            		HttpUtility.writeToClient(response,"needNewMonthMain="+ff.pauseMonthyyyyMM(newDate));
        		}else{
            		HttpUtility.writeToClient(response,"thisMonthExist="+monthMainId);
        		}
        		
        		return ;
        	}
    		HttpUtility.writeToClient(response,false+"");
    		return;
    	}
    	
    	if("start".equals(modifyDate)){
    		HttpUtility.writeToClient(response,this.kpiMonthlyMainService.updateMonth(userId,form.getLibId(), newDate,null));
    		return ;
    	}
    	if("end".equals(modifyDate)){
    		HttpUtility.writeToClient(response,this.kpiMonthlyMainService.updateMonth(userId,form.getLibId(),null, newDate));
    		return ;
    	}

    	Boolean f = kpiMonthlyEntryService.realLib( form.getUserId(), mainId, form.getLibId(), form.getWeight(), form.getFinishDate());
        HttpUtility.writeToClient(response, f+"");
    }
    
//    public static void main(String[] args) {
//    	@SuppressWarnings("unused")
//    	MyKpiLibController.RealLibForm t = new MyKpiLibController.RealLibForm();
//	}
    
    /***
     * 获得最当前　往后最近两年的　年月
     * @return
     */
    public List<String> getYYYYMM(){
    	Calendar c = Calendar.getInstance();
    	
//    	DateUtility.sdfyMdHms.format(c.getTime());
    	
//    	System.out.println("当月："+(c.get(Calendar.MONTH)+1));

    	List<Integer> yyyy = new  ArrayList<Integer>();
    	List<Integer> mm = new  ArrayList<Integer>();
    	List<String> yyyymm = new ArrayList<String>();
    	
    	int currentYYYY = c.get(Calendar.YEAR);
    	yyyy.add(currentYYYY);
    	int i=0;
    	for(int y=(c.get(Calendar.MONTH)+1);y>0;y--){
    		mm.add(y);
    		String yymm = (currentYYYY+i)+(y<10?"0":"")+y;
    		yyyymm.add(yymm);
    	}
    	i=-1;
    	yyyy.add(currentYYYY+i);
    	for(int y=12;y>0;y--){
    		mm.add(y);
    		String yymm = (currentYYYY+i)+(y<10?"0":"")+y;
    		yyyymm.add(yymm);
    	}
    	
//    	for(Integer yymm:yyyymm){
//    		System.out.println(yymm);
//    	}
    	return yyyymm;
	}
    public String getcYYYYMM(){
    	Calendar c = Calendar.getInstance();
    	int currentYYYY = c.get(Calendar.YEAR);
    	int y=(c.get(Calendar.MONTH));
    	return currentYYYY+(y<10?"0":"")+y;
	}

}