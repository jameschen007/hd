package com.easycms.kpi.underlying.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KpiCycleHandle {

	public static void main(String[] args) {
		String mon = "yyyyM";
		if(mon.length()==5){
			mon=mon.substring(0, 4)+"0"+mon.substring(4);
		}
		System.out.println(mon);
		KpiCycleHandle ff = new KpiCycleHandle();
		
		System.out.println(ff.handle("2017年11月1日~2017年12月1日", "2017-10-10", "2013-03-13"));
	}
	
	/**
	 * 处理绩效考核周期，传入已有的  ，开始日期有可能传入，结果日期有可能传入
	 * @return
	 */
	public String handle(String cycle,String start,String end){
		//传入参数样本数据：考核日期：2017年11月1日~2017年12月1日
		//2017-10-10
		//2017-10-10
		
	    //修改考核周期
        String s = "",e="";
        String[] t = cycle.split("~");
        if(start!=null && !"".equals(start)){
        	s = pauseDate(start);
        }else{
        	s = t[0];
        }
        
        if(end!=null && !"".equals(end)){
        	e = pauseDate(end);
        }else{
        	e = t[1];
        }
        
        return s+"~"+e;
	}
	
	/**
	 * 传入考核周期或开始时间，返回月份yyyyMM
	 * @param cycle
	 * @param start
	 * @return
	 */
	public String getStartMonthyyyyMM(String cycle,String start){
        String s = "";
        String[] t = cycle.split("~");
        if(start!=null && !"".equals(start)){
        	s = pauseMonthyyyyMM(start);
        }else{
        	s = t[0];
        }
		if(s.length()==5){
			s=s.substring(0, 4)+"0"+s.substring(4);
		}
		return s ;
	}
	
	public String[] getcycleSE(String se){
		if(se==null || se.indexOf("~")==-1 ){
			return null;
		}
			
		String[] ret = null;
		ret = se.split("~");

		String[] ret2 = new String[ret.length];
		for(int i=0;i<ret.length;i++){
			String d = ret[i];
			d = d.replace("年", "-");
			d = d.replace("月", "-");
			d = d.replace("日", "");
			ret2[i]=d;
		}
		return ret2;
	}
	/**
	 * 传入 2017-10-10
	 * 转出 2017年11月1日
	 * @return
	 */
	public String pauseDate(String in){
		SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfb = new SimpleDateFormat("yyyy年MM月dd日");
		Date d = new Date();
		try {
			d = sdfa.parse(in);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sdfb.format(d);
	}
	/**
	 * 传入 2017-10-10
	 * 转出 201711
	 * @return
	 */
	public String pauseMonthyyyyMM(String in){
		SimpleDateFormat sdfa = new SimpleDateFormat("yyyy-MM-dd");	
		Date d = new Date();
		try {
			d = sdfa.parse(in);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat sdfb = new SimpleDateFormat("yyyyMM");
		String retrun = sdfb.format(d);
		if(retrun.length()==5){
			retrun=retrun.substring(0, 4)+"0"+retrun.substring(4);
		}
		return retrun;
	}
	
}
