package com.easycms.kpi.underlying.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.kpi.underlying.constant.KpiTypeConstant;
import com.easycms.kpi.underlying.domain.KpiHandleExchange;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.form.KpiMonthlyMainForm;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
import com.easycms.kpi.underlying.service.KpiLibService;
import com.easycms.kpi.underlying.service.KpiMonthlyEntryService;
import com.easycms.kpi.underlying.service.KpiMonthlyMainService;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.kpi.underlying.service.impl.sys.PersonnelCategory;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller(value="kpiLinkHandleController")
@RequestMapping("/kpi/link")
@Transactional
public class KpiLinkHandleController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiLinkHandleController.class);

    @Autowired
    private KpiMonthlyMainService kpiMonthlyMainService;
    @Autowired
    private UserService userService;
    @Autowired
    private KpiUserBaseService kpiUserBaseService;

    @Autowired
    @Resource
    private KpiLibService kpiLibService;
    @Autowired
    private KpiMonthlyEntryService kpiMonthlyEntryService;
    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;
    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;
    /**
     * 打开待审指标页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/pending/verify", method = RequestMethod.GET)
    public String pendingVerify(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyMainForm kpiMonthlyMainForm) {
    	commonShowLink(request,null,null,true);
    	return ForwardUtility.forwardAdminView("/kpi/underlying/page_hislib_parentShow");
    }
    
    /**
     * 环节处理公共方法 查询某一绩效 的主表和子表信息
     * @param request
     * @param sessionUserId
     * @param searchMonth 查询年月，正常不用传，只有查询历史才会用到。
     */
    public void commonShowLink(HttpServletRequest request,Integer sessionUserId,String searchMonth,boolean compatibility){
//        String sId = request.getParameter("sId");//mainId
//        String custom = request.getParameter("custom");//linkName

    	Object sIds = request.getParameter("sId");
    	Integer mainId = 0;
    	if(sIds != null){
//        	request.setAttribute("sId", sIds);
        	logger.info("sIds="+sIds);
        	mainId = Integer.valueOf(String.valueOf(sIds));
    	}

    	
        KpiMonthlyMain main ;
        if(CommonUtility.isNonEmpty(searchMonth)){
        	//使用场景：1 打开自评分 ；2 查询我的历史绩效
        	main = kpiMonthlyMainService.findKpiMonthlyMainByUserIdMonth(sessionUserId, searchMonth); 
        	//如果没有查询到，暂时查下一个月的，即当前规划的。在页面上需要做相应判断逻辑。
        	if(main==null&& compatibility){
        		logger.error("查询指定月份"+searchMonth+"的 用户"+sessionUserId+"的绩效考核没有找到,暂时查他下一个月的出来显示."+mainId);
            	main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        	}
        }else{
        	//审核场景：直接传入主表标识查询。
        	main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        }
        if(main == null){
        	return;
        }
        mainId = main.getId();
        

		try{
	        //因为流程表中干系人的数据已经生成后，子表中的指标对应的干系人有变化，这里做一个最新的调整
	        fixHandleId(mainId);
		}catch(Exception e){
			e.printStackTrace();
		}
        
        request.setAttribute("sId", main.getId());        	
        Integer hisId = main.getOwner().getId();
        KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
        User he = main.getOwner();
    	//查询基本信息
    	request.setAttribute("ownBase", userBase);
    	request.setAttribute("he", he);
//    	main.setKpiStatus("正在进行");
//    	main.setCurrentStatus(FlowName.selfShowInit.getCode());
//    	main.setKpiType("待指标保存");
//    	main.setDel(false);
//        kpiMonthlyMainService.update(main);
        request.setAttribute("main", main);

    	//查询我的指标mine  在走流程中的时候，不需要根据我的指标，生成对应的显示的。
//        KpiLib lib1 = new KpiLib();
//        lib1.setUseType("mine");
//        lib1.setUser(he);
//        lib1.setKpiType("general");
//        List<KpiLib> generalLibs = kpiLibService.findAll(lib1);
    	List<KpiMonthlyEntry> glist = new ArrayList<KpiMonthlyEntry>();
//        if(generalLibs!=null && generalLibs.size()>0){
//        	for(KpiLib g:generalLibs){
//        		KpiMonthlyEntry n = new KpiMonthlyEntry();
//        		n.setLib(g);
//        		n.setMonthlyMain(main);
//        		n.setOwner(he);
//        		n.setDel(false);
//        		glist.add(n);
//        	}
//        	glist = kpiMonthlyEntryService.findOrAdd(glist,false);
//        }
        //上面的是根据现有的已选规划查询，如果历史的未选的，还需要查询出来
        List<KpiMonthlyEntry> selfAllEntrys = kpiMonthlyEntryService.findByMainId(mainId);
        for(KpiMonthlyEntry dbAlready:selfAllEntrys){
        	boolean isin = false ;
        	for(KpiMonthlyEntry libAlready:glist){
        		if(
        				libAlready!=null && dbAlready!=null &&
						libAlready.getId()!=null && dbAlready.getId()!=null &&
						KpiTypeConstant.GENERAL.equals(libAlready.getLib().getKpiType())
        				&& KpiTypeConstant.GENERAL.equals(dbAlready.getLib().getKpiType())
        				&& libAlready.getId().intValue()==dbAlready.getId().intValue()){
        			isin = true ;
        		}
        	}
        	if(isin==false && KpiTypeConstant.GENERAL.equals(dbAlready.getLib().getKpiType())){
        		glist.add(dbAlready);
        	}
        }
        //在走流程中的时候，不需要根据我的指标，生成对应的显示的。
//        KpiLib lib2 = new KpiLib();
//        lib2.setUseType("mine");
//        lib2.setUser(he);
//        lib2.setKpiType("purpose");
//        List<KpiLib> purposelLibs = kpiLibService.findAll(lib2);
    	List<KpiMonthlyEntry> plist = new ArrayList<KpiMonthlyEntry>();
//        if(purposelLibs!=null && purposelLibs.size()>0){
//        	for(KpiLib g:purposelLibs){
//        		KpiMonthlyEntry n = new KpiMonthlyEntry();
//        		n.setLib(g);
//        		n.setMonthlyMain(main);
//        		n.setOwner(he);
//        		n.setDel(false);
//        		plist.add(n);
//        	}
//        	plist = kpiMonthlyEntryService.findOrAdd(plist,false);
//        }
        //上面的是根据现有的已选规划查询，如果历史的未选的，还需要查询出来
        for(KpiMonthlyEntry dbAlready:selfAllEntrys){
        	boolean isin = false ;
        	for(KpiMonthlyEntry libAlready:plist){
        		if(
        				libAlready!=null && dbAlready!=null &&
        						libAlready.getId()!=null && dbAlready.getId()!=null &&
        				KpiTypeConstant.PURPOSE.equals(libAlready.getLib().getKpiType())
        				&& KpiTypeConstant.PURPOSE.equals(dbAlready.getLib().getKpiType())
        				&& libAlready.getId().intValue()==dbAlready.getId().intValue()){
        			isin = true ;
        		}
        	}
        	if(isin==false && KpiTypeConstant.PURPOSE.equals(dbAlready.getLib().getKpiType())){
        		plist.add(dbAlready);
        	}
        }
        
//干系人环节,被考核人还是可见所有指标.
        if(sessionUserId==null && FlowName.stakeholderScore.getCode().equals(main.getCurrentStatus())){
        	List<KpiMonthlyEntry> gg = new ArrayList<KpiMonthlyEntry>();
        	List<KpiMonthlyEntry> pg = new ArrayList<KpiMonthlyEntry>();
        	Integer meId = getMe(request).getId();
        	for(KpiMonthlyEntry g:glist){
        		if(g.getStakeholder().getId().intValue()==meId.intValue()){
        			gg.add(g);
        		}
        	}
        	for(KpiMonthlyEntry p:plist){
        		
        		if(p==null || p.getStakeholder()==null || p.getStakeholder().getId()==null ){
        			logger.info(p);
        		}
        		
        		if(p.getStakeholder().getId().intValue()==meId.intValue()){
        			pg.add(p);
        		}
        	}

        	request.setAttribute("generalList", gg);
            request.setAttribute("purposeList", pg);
            return ;
    	}
    	request.setAttribute("generalList", glist);
        request.setAttribute("purposeList", plist);
    }
    //TODO 打开待审指标页
//    打开评分页
//
//    审核指标确定
//    保存评分
//    这４个功能都是按mainId,linkName
    /**
     * 打开评分页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/pending/score", method = RequestMethod.GET)
    public String pendingScore(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyMainForm kpiMonthlyMainForm) {

        String custom = request.getParameter("custom");//linkName
    	Object sIds = request.getParameter("sId");
    	logger.info("sIds="+sIds);
    	logger.info("custom="+custom);
    	Integer mainId = Integer.valueOf(String.valueOf(sIds));
        KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        
        logger.info("当前环节:"+main.getCurrentStatus());
    	commonShowLink(request,null,null,true);
    	
    	if(FlowName.stakeholderScore.getCode().equals(main.getCurrentStatus())){
        	User me = getMe(request);
        	Integer meId = me.getId();
    		request.setAttribute("me", me);
        	request.setAttribute("meId", meId);

        	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_stakeholderScore");
    	}else if(FlowName.generalScore.getCode().equals(main.getCurrentStatus())){
        	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_generalScore");
    	}else if(FlowName.generalScore2.getCode().equals(main.getCurrentStatus())){
        	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_generalScore2");
    	}else if(FlowName.departmentManagerScore.getCode().equals(main.getCurrentStatus())){
        	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_departmentManagerScore");
    	}else if(FlowName.chargeLeaderScore.getCode().equals(main.getCurrentStatus())){
        	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_chargeLeaderScore");
    	}
    	
    	logger.info(main.getCurrentStatus());
    	
    	return ForwardUtility.forwardAdminView("/kpi/score/page_hislib_parentScore");
    }

    /**
     * 审核指标确定
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/done/verify", method = {RequestMethod.GET,RequestMethod.POST})
    public void doneVerify(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyMainForm kpiMonthlyMainForm) {

//        if(FlowName.parentScore3.getCode().equals(main.getCurrentStatus())||
//        		FlowName.parentScore4.getCode().equals(main.getCurrentStatus())){
//        	
//        }else if(FlowName.stakeholderScore.getCode().equals(main.getCurrentStatus())){
//        	
//        }else if(FlowName.generalScore.getCode().equals(main.getCurrentStatus())){
//        	
//        }else if(FlowName.generalScore2.getCode().equals(main.getCurrentStatus())){
//        	
//        }
    	boolean f = commonHandleLink(request);
    	
		HttpUtility.writeToClient(response, f+"");
    }
    /**
     * 保存评分
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/done/score", method = {RequestMethod.GET,RequestMethod.POST})
    public void doneScore(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiMonthlyMainForm kpiMonthlyMainForm) {

        String custom = request.getParameter("custom");//linkName
    	Object sIds = request.getParameter("sId");
    	logger.info("sIds="+sIds);
    	logger.info("custom="+custom);
    	Integer mainId = Integer.valueOf(String.valueOf(sIds));
        KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);

		try{
	        //因为流程表中干系人的数据已经生成后，子表中的指标对应的干系人有变化，这里做一个最新的调整
	        fixHandleId(mainId);
		}catch(Exception e){
			e.printStackTrace();
		}
        
        logger.info("当前环节:"+main.getCurrentStatus());
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        
        if(
        		FlowName.departmentManagerScore.getCode().equals(main.getCurrentStatus())||
        		FlowName.chargeLeaderScore.getCode().equals(main.getCurrentStatus())){
            String directSuperiorScoring = request.getParameter("directSuperiorScoring");//directSuperiorScoring
            main.setDirectSuperiorScoring(Integer.valueOf(directSuperiorScoring));
			kpiMonthlyMainService.update(main);
        }else if(FlowName.parentScore.getCode().equals(main.getCurrentStatus())||FlowName.stakeholderScore.getCode().equals(main.getCurrentStatus())){

    		Map<String, String> map = new HashMap<String, String>();
    		Enumeration enum2 = request.getParameterNames();
    		while (enum2.hasMoreElements()) {
    			String paramName = (String) enum2.nextElement();
    			String paramValue = request.getParameter(paramName);
    			map.put(paramName, paramValue);
    		}

        	List<KpiMonthlyEntry> entrys = kpiMonthlyEntryService.findByMainId(mainId);
        	List<KpiMonthlyEntry> updateEntrys = new ArrayList<KpiMonthlyEntry>();
        	boolean oneTicketVeto = false ;
        	int StakeholderScoringSum = 0;
        	for(KpiMonthlyEntry db:entrys){
     		   
        		//只有当前session干系人记录才行
        		//parentScore的情况时，应该一次性过，但是由于修改了干系人，这里就对不上了。所以应放在
        		if(FlowName.parentScore.getCode().equals(main.getCurrentStatus())){
        			
        		}else if( (loginUser.getId().intValue()==db.getStakeholder().getId().intValue() ) == false){
        			continue ;
        		}
        		
     		   try{
     			  String score = map.get("stakeholderScore_"+db.getId());
         		  String reason = map.get("melibselfReason_"+db.getId());
         		  String vetoReason = map.get("melibVetoReason_"+db.getId());

         		  if( (reason!=null && !"".equals(reason.trim()))){

         			  db.setStakeholderScoringReason(reason);
         		  }

         		  if( (vetoReason!=null && !"".equals(vetoReason.trim()))){

         			 db.setOneTicketVetoReason(vetoReason);
         			 oneTicketVeto = true ;
         		  }
         		  if(score==null ||"".equals(score.trim())){
         				HttpUtility.writeToClient(response, "评分不能为空！！");
         				return ;
         		  }
         		  
         		 score = score.trim();
        		   if(score!=null && score.matches("\\d+")){
        			  db.setStakeholderScoring(Integer.valueOf(score));
        			  updateEntrys.add(db);
        		   }
        		   StakeholderScoringSum += Integer.valueOf(score);
     		   }catch(Exception e){
     			   e.printStackTrace();
     			   throw e ;
     		   }
        	}
        	main.setStakeholderScoring(StakeholderScoringSum);
        	kpiMonthlyMainService.update(main);
        	//出现一票否决，则该员工本月所有考核为0，即为最低考核，出现一票否决需要填写详细理由，
        	if(oneTicketVeto){
        		int selfScores = 0;
            	if(entrys!=null && entrys.size()>0){
            		for(KpiMonthlyEntry db:entrys){
            			db.setStakeholderScoring(0);
            			selfScores+= db.getSelfScoring();
            		}
            		kpiMonthlyEntryService.updateAll(entrys);
            	}
            	//流程 一票否决 强制完成
				main.setCompositeScoring(new BigDecimal(0));
				main.setSelfScoring(selfScores);
				main.setGeneralManagerScoring(0);
				main.setDirectSuperiorScoring(0);
				main.setKpiCoefficient(new BigDecimal(0.8));
//
//所有考核完成后，系统根据以上计算方法进行每个人的月度绩效考核分数计算
//（三）考核系数确定规则（该部分不在系统里计算）
				main.setCurrentStatus("kpiOver");
				main.setKpiStatus("已完成考核(一票否决)");
				main.setKpiType("已完成");
				kpiMonthlyMainService.update(main);

				List<KpiHandleExchange> dbFlows = kpiHandleExchangeService.findByMainId(mainId);

				List<KpiHandleExchange> dbFlowsUpdate = new ArrayList<KpiHandleExchange>();
				for(KpiHandleExchange db:dbFlows){
					if("Y".equals(db.getVisitStatus())){
						db.setVisitStatus("N");
						dbFlowsUpdate.add(db);
					}
				}
				if(dbFlowsUpdate!=null && dbFlowsUpdate.size()>0){
					kpiHandleExchangeService.addOrUpdateAll(dbFlowsUpdate);
				}
				//统计数据提取
				KpiSummarySheet summary = new KpiSummarySheet();
				summary.setUserId(main.getOwner().getId());
				summary.setMonth(main.getMonth());
				summary.setDepartmentName(main.getOwner().getDepartment().getName());
				summary.setRealName(main.getOwner().getRealname());
				summary.setSelfScoring(main.getSelfScoring());
				summary.setStakeholderScoring(main.getStakeholderScoring());
				summary.setDirectSuperiorScoring(main.getDirectSuperiorScoring());
				summary.setGeneralManagerScoring(main.getGeneralManagerScoring());
				summary.setCompositeScoring(main.getCompositeScoring());
				summary.setMonthlyMainId(main.getId());
				kpiSummarySheetService.add(summary);
				
				HttpUtility.writeToClient(response, true+"");
				return ;//一票否决 强制完成
            	
        	}else{
            	if(updateEntrys!=null && updateEntrys.size()>0){
            		kpiMonthlyEntryService.updateAll(updateEntrys);
            	}
        	}
        	
        }else if(FlowName.generalScore.getCode().equals(main.getCurrentStatus())){
            String directSuperiorScoring = request.getParameter("generalManagerScoring");//directSuperiorScoring
            main.setGeneralManagerScoring(Integer.valueOf(directSuperiorScoring));
			kpiMonthlyMainService.update(main);
        }else if(FlowName.generalScore2.getCode().equals(main.getCurrentStatus())){

            String directSuperiorScoring = request.getParameter("twoProcessingScoring");//directSuperiorScoring
            main.setTwoProcessingScoring(Integer.valueOf(directSuperiorScoring));
			kpiMonthlyMainService.update(main);
        }
		try{
			
	    	boolean f = commonHandleLink(request);
			HttpUtility.writeToClient(response, f+"");
		}catch(Exception e){
			HttpUtility.writeToClient(response, "false");
		}
    	
    }

    //因为流程表中干系人的数据已经生成后，子表中的指标对应的干系人有变化，这里做一个最新的调整
    void fixHandleId(Integer mainId) {
    	KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
    	
    	if("kpiOver".equals(main.getCurrentStatus())){
    		return ;
    	}
    	
    	List<KpiHandleExchange> flows = kpiHandleExchangeService.findByMainId(mainId);

    	
    	
    	//有可能因为这个原因，导致流程表中没有一个是在待办了。这里根据主表的状态进行恢复
    	List<KpiHandleExchange> one = flows.stream().filter(b->{
    		return main.getCurrentStatus()!=null && main.getCurrentStatus().equals(b.getSegment())
    				&& "pending".equals(b.getSegmentStatus()) && "N".equals(b.getVisitStatus());
    	}).collect(Collectors.toList());
    	

    	List<KpiHandleExchange> oneY = flows.stream().filter(b->{
    		return main.getCurrentStatus()!=null && main.getCurrentStatus().equals(b.getSegment())
    				&& "pending".equals(b.getSegmentStatus()) && "Y".equals(b.getVisitStatus());
    	}).collect(Collectors.toList());
    	//如果在直接领导确认时，有待办，不　修复它，20181213碰到一次修复出问题了。 　
    	boolean isParentFiex =one!=null && oneY!=null && one.size()>0 &&  (!FlowName.parentOk.getCode().equals(one.get(0).getSegment()) ) && (oneY==null || oneY.size()==0);//不是parentOk 且真的没有待办了呀
    	
    	if(one!=null && one.size()==1 && isParentFiex){//并且没有评分的，才修改。这里修改的是那种状态异常的。
    	    		
    		List<KpiMonthlyEntry> entrys = kpiMonthlyEntryService.findByMainIdAndStak(mainId, one.get(0).getHandleUserId());
    		if(false == entrys.stream().anyMatch(e->e.getStakeholderScoring()!=null && e.getStakeholderScoring()>0)){

        		one.stream().forEach(b->{
        			b.setVisitStatus("Y");
        			kpiHandleExchangeService.add(b);
        		});
    		}
    	}
    	
    	Optional<KpiHandleExchange> self = flows.stream().filter(b->
    		FlowName.selfScore.getCode().toString().equals(b.getSegment())
    	).findAny();
    	if(self.isPresent() == false){
    		return ;
    	}

    	KpiUserBase userBase = kpiUserBaseService.findKpiUserBaseByUserId(self.get().getHandleUserId());
    	User owner = userService.findUserById(self.get().getHandleUserId());
    	if(owner!=null && owner.getParent()!=null){

        	//恢复流程表中直接领导修改的情况
        	flows.stream().forEach(b->{
        		if(FlowName.parentOk.getCode().toString().equals(b.getSegment()) 
        				|| FlowName.parentScore.getCode().toString().equals(b.getSegment())){
        			if(b.getHandleUserId().intValue()!=owner.getParent().getId().intValue()){
        				b.setHandleUserId(owner.getParent().getId());
        				kpiHandleExchangeService.add(b);
        			}
        		}
        		
        	});
    	}
    	
    	if(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())){//如果是普通员工，则返回，不处理。
    		//将原来没有添加此判断时添加的多余的数据进行删除掉。
    		if(flows!=null && flows.size()>0){
    			flows.stream().forEach(b->{
    				if(FlowName.stakeholderScore.getCode().toString().equals(b.getSegment())){
    					kpiHandleExchangeService.delete(b.getId());
    				}
    			});
    		}
    		return ;
    	}
    	
    	
    	String addVisitStatus = "N";//如果要新增，新增的值
    	if(flows==null || flows.size()==0){
    		return ;
    	}else{
    		boolean isY = flows.stream().anyMatch(b->"Y".equals(b.getVisitStatus()));
    		if(isY){
    			addVisitStatus = "Y";
    		}
    	}

    	List<KpiMonthlyEntry> libs = kpiMonthlyEntryService.findByMainId(mainId);
    	
        List<KpiHandleExchange> dbFlowData = new ArrayList<KpiHandleExchange>();

        //修复干系人因修改等原因异常的数据。
        if(libs!=null && libs.size()>0){
        	Set<Integer> setStand = new HashSet<Integer>();
        	for(KpiMonthlyEntry lb:libs){
        		if(lb.getStakeholder()==null){
        			continue ;
        		}
        		Integer standId = lb.getStakeholder().getId();
        		if(setStand.add(standId)){//去重
        			
        			boolean hased = flows.stream().anyMatch(b->
        			FlowName.stakeholderScore.getCode().equals(b.getSegment())
    				&& b.getHandleUserId()!=null && b.getHandleUserId().intValue()==standId.intValue()
        					);
        			if(hased == false ){//子表中存在，流程表中不存在时，新增

        				KpiHandleExchange po2 = new KpiHandleExchange();
        				po2.setMonthlyMain(main);
        				po2.setHandleUserId(lb.getStakeholder().getId());
        				po2.setSegment(FlowName.stakeholderScore.getCode());
        				po2.setSegmentStatus("pending");
        				po2.setVisitStatus(addVisitStatus);
        				dbFlowData.add(po2);
        				
        				KpiHandleExchange po = new KpiHandleExchange();
        				po.setMonthlyMain(main);
        				po.setHandleUserId(lb.getStakeholder().getId());
        				po.setSegment(FlowName.stakeholderScore.getCode());
        				po.setSegmentStatus("done");
        				po.setVisitStatus("N");
        				dbFlowData.add(po);
        			}
        			

        		}
        	}
        	
        	//如果流程表中存在，但是子表中不存在，则需要删除
        	flows.stream().forEach(b->{
        		if(setStand.contains(b.getHandleUserId()) == false && FlowName.stakeholderScore.getCode().equals(b.getSegment())){
        			kpiHandleExchangeService.delete(b.getId());
        		}
        	});
        }
        

		if(dbFlowData!=null && dbFlowData.size()>0){
			kpiHandleExchangeService.addAll(dbFlowData);
		}
    	
	}

	/**
     * 自评分
    *
    * @param request
    * @param response
    * @return
    */
   @RequestMapping(value = "/done/selfScore", method = {RequestMethod.GET,RequestMethod.POST})
   public void doneSelfScore(HttpServletRequest request, HttpServletResponse response
			, @ModelAttribute("form") @Valid KpiMonthlyMainForm kpiMonthlyMainForm) {

		Map<String, String> map = new HashMap<String, String>();
		Enumeration enum2 = request.getParameterNames();
		while (enum2.hasMoreElements()) {
			String paramName = (String) enum2.nextElement();
			String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}
        //保存备注
        String mark = map.get("mark");
        if(CommonUtility.isNonEmpty(mark)){
        	if(mark.length()>200){
        		mark = mark.substring(0,200);
        	}
        }

		String custom = request.getParameter("custom");// linkName
		Object sIds = request.getParameter("sId");
		logger.info("sIds=" + sIds);
		logger.info("custom=" + custom);
		Integer mainId = Integer.valueOf(String.valueOf(sIds));
		List<KpiMonthlyEntry> entrys = kpiMonthlyEntryService.findByMainId(mainId);
		KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
    	main.setMark(StringEscapeUtils.escapeHtml4(mark));
		int selfScoreSum = 0;
		if (entrys != null && entrys.size() > 0) {
			for (KpiMonthlyEntry entry : entrys) {
     		  String selfScore = map.get("melibselfScoring_"+entry.getId());
     		  String selfReason = map.get("melibselfReason_"+entry.getId());
     		  if(selfScore==null || !selfScore.matches("\\d+")){

   				HttpUtility.writeToClient(response, "自评分输入不正确!");
   				return ;
     		  }
     		  Integer score = Integer.valueOf(selfScore);
     		  if(score.intValue()>entry.getWeight().intValue() && (selfReason==null || "".equals(selfReason.trim()))){

     				HttpUtility.writeToClient(response, "自评分大于权重，需要输入自评理由!");
     				return ;
     		  }
     		  selfScoreSum+=score;
     		  entry.setSelfScoring(score);
     		  entry.setSelfReason(selfReason);
			}
		}
		main.setSelfScoring(selfScoreSum);
		kpiMonthlyEntryService.updateAll(entrys);
		kpiMonthlyMainService.update(main);

		boolean f = commonHandleLink(request);

		HttpUtility.writeToClient(response, f + "");
	}
    

    /**
     * 环节处理公共方法
     * @return
     */
    public boolean commonHandleLink(HttpServletRequest request){
    	return kpiMonthlyMainService.commonHandleLink(request);
    }

	private User getMe(HttpServletRequest request){
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
        Integer hisId = loginUser.getId();
        User he = userService.findUserById(hisId);
        return he ;
    }
	
//    /**
//     * 查询某人的绩效
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/seeScoreUI", method = {RequestMethod.POST,RequestMethod.GET})
//    public String seeScoreUI(HttpServletRequest request,
//                            HttpServletResponse response) {
//        commonShowLink(request,0,null,true);
//        return ForwardUtility.forwardAdminView("/kpi/score/page_lib_seeScore");
//    }
	
    /**
     * 查询某人的绩效
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/seeScoreUI", method = {RequestMethod.POST,RequestMethod.GET})
    public String seeScoreUI(HttpServletRequest request,
                            HttpServletResponse response) {
        commonShowLink(request,0,null,true);
        return ForwardUtility.forwardAdminView("/kpi/score/modal_page_lib_seeScore");
    }
    

    
}
