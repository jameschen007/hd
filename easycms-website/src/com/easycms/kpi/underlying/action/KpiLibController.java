package com.easycms.kpi.underlying.action;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.response.JsonResult;
//import com.easycms.hd.variable.domain.VariableSet;
//import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiLibDetails;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.form.KpiLibForm;
import com.easycms.kpi.underlying.service.KpiLibExcelUtil;
import com.easycms.kpi.underlying.service.KpiLibService;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.kpi.underlying.service.impl.sys.PersonnelCategory;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.UserForm;
import com.easycms.management.user.service.UserService;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-16 下午11:37:55<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/lib/")
public class KpiLibController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiLibController.class);

    @Autowired
    @Resource
    private KpiLibService kpiLibService;
    @Autowired
    private KpiUserBaseService kpiUserBaseService;
    @Autowired
    private UserService userService;
//    @Autowired
//    private VariableSetService variableSetService;
	@Autowired
	private ThreadPoolTaskExecutor poolTaskExecutor; 

    /**
     *抄的
     *  用户模块 - 用户列表
     *用于　指标人员列表
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "kpiUserList", method = RequestMethod.GET)
    public String kpiUserList(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid UserForm userForm) {
        String func = userForm.getFunc();
        logger.debug("[func] = " + func);
        // #########################################################
        // 校验用户是否存在
        // #########################################################
        if ("exist".equalsIgnoreCase(func)) {
            logger.debug("==> Valid user is exist or not.");
            boolean avalible = false;
            avalible = !userService.userExist(userForm.getName());
            HttpUtility.writeToClient(response, String.valueOf(avalible));
            return null;

        }
        // #########################################################
        // 修改用户username
        // #########################################################
        if ("edit".equalsIgnoreCase(func)) {
            logger.debug("==> Valid username is exist or not except with id.");
            boolean avalible = false;
            avalible = !userService.userExistExceptWithId(userForm.getName(),userForm.getId());
            HttpUtility.writeToClient(response, String.valueOf(avalible));
            return null;

        }
        logger.debug("==> Start show kpiUserList list.");
        return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_user");
    }
    /**
     * 数据片段
     *
     * @param request
     * @param response
     * @param form
     * @return
     */
    @RequestMapping(value = "kpiUserList", method = RequestMethod.POST)
    public String kpiUserListData(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") UserForm form) {
        form.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show kpiUserList data list.");
        form.setFilter(CommonUtility.toJson(form));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
        String returnString = ForwardUtility
                .forwardAdminView("/user-group/data/data_json_user");
        return returnString;
    }

    @RequestMapping(value = "libManger2", method = RequestMethod.GET)
    public String libManger2(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiLibForm kpiLibForm) {
    	request.setAttribute("this2", "2");
    	return libManger(request, response
                , kpiLibForm);
    }
    /**
     * 指标库表列表
     *
     * 待考核人员　指标显示　，准备选择指标。
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "libManger", method = RequestMethod.GET)
    public String libManger(HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("form") @Valid KpiLibForm kpiLibForm) {

//        Object ob = kpiLibService.getKpiUsersLibListList(mp);
//        logger.info(CommonUtility.toJson(ob));
        try {

//
//            List<VariableSet> variableSet = variableSetService.findByType("pointerType");
//            for (VariableSet as : variableSet){
//                if(userBase.getPersonnelCategory().equals(as.getSetkey())){
//                    userBase.setPersonnelCategory(as.getSetvalue());
//                }
////                resultMap.put(as.getSetkey(), as.getSetvalue());
//            }
        	
      	


        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CommonUtility.isNonEmpty(kpiLibForm.getCustom())) {
            return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_lib");
        }else{
            Integer hisId = null;
            if (kpiLibForm.getsId()!=null) {
                hisId = Integer.valueOf(kpiLibForm.getsId());
                User he = userService.findUserById(hisId);
                request.setAttribute("he",he);
                request.setAttribute("sId", hisId);

                //初始如果不是导入的话，是没有基本信息记录的，要判断新增


                //被考核人id,
                KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
                if(userBase==null){
                }else{
                    request.setAttribute("userBase", userBase);
                }
            }else{
//            	20171024添加
//            	如果没有sId则获取当前登录用户ID
            	hisId = Integer.valueOf(WebUtility.getUserIdFromSession(request));
            	logger.info("hisId:"+hisId);
            	User me = userService.findUserById(hisId);
            	
            	request.setAttribute("he",me);
                request.setAttribute("sId", hisId);
                KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
                if(userBase==null){
                }else{
                    request.setAttribute("userBase", userBase);
                }
                return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_lib_myPage");
            }

            return ForwardUtility.forwardAdminView("/kpi/underlying/list_kpi_lib_page");
        }
    }

    /**
     * 待考核人员　指标显示　，准备选择指标。
     * 指标库表-数据片段
     * @param request
     * @param response
     * @return String
     */
    @RequestMapping(value = "libManger2", method = RequestMethod.POST)
    public String libMangerData2(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiLibForm kpiLibForm) {
     
        return libMangerData(request,
                 response, kpiLibForm);
    }
    @RequestMapping(value = "libManger", method = RequestMethod.POST)
    public String libMangerData(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") KpiLibForm kpiLibForm) {
        kpiLibForm.setSearchValue(request.getParameter("search[value]"));
        

        String sId = request.getParameter("sId");
        if(kpiLibForm==null){
            logger.error("kpiLibForm ==null");

        }
        if (kpiLibForm.getsId() != null) {
            logger.info("kpiLibForm.getsid 不为空的，可以获得");
        } else {
            logger.info("sid="+sId);
            kpiLibForm.setsId(sId);
        }

        logger.debug("==> Show KpiLib data list.");
        kpiLibForm.setFilter(CommonUtility.toJson(kpiLibForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiLibForm));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/data/data_kpi_lib");
        return returnString;
    }

    @RequestMapping(value = "/add2", method = RequestMethod.GET)
    public String addUI2(HttpServletRequest request, HttpServletResponse response) {
    	request.setAttribute("thisurl", "/kpi/lib/add2");
    	request.setAttribute("this2", "2");
    	return addUI(request, response);
//    	return "forward:/kpi/lib/add";
    }
    /**
     * 新增-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addUI(HttpServletRequest request, HttpServletResponse response) {
    	if(request.getAttribute("thisurl")==null){//为了复用这个功能
        	request.setAttribute("thisurl", "/kpi/lib/add");
    	}
//要为谁配置指标库
    	String hisIdStr = request.getParameter("userId");
        logger.info("要为谁配置指标库="+request.getParameter("userId"));
        request.setAttribute("userId", request.getParameter("userId"));
        //为他添加什么类型的指标库
        logger.info("他添加什么类型的指标库="+request.getParameter("kpiType"));
        request.setAttribute("kpiType", request.getParameter("kpiType"));

        List<User> userList = userService.findAll(new User());
        request.setAttribute("userList", userList);
        
        KpiUserBase userBase = kpiUserBaseService.findKpiUserBaseByUserId(Integer.valueOf(hisIdStr));
        request.setAttribute("userBase", userBase);
        logger.info(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())+"");
        String isEmployees = PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())+"";
        request.setAttribute("isEmployees",isEmployees);
        
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/underlying/modal_kpi_lib_add");
        return returnString;
    }

    @RequestMapping(value = "/add2", method = RequestMethod.POST)
    public void add2(HttpServletRequest request, HttpServletResponse response,
                    @ModelAttribute("kpiLibForm") @Valid KpiLibForm kpiLibForm,
                    BindingResult errors) {
    	add(request, response,
                kpiLibForm,
                 errors);
    }
    /**
     * 保存新增-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(HttpServletRequest request, HttpServletResponse response,
                    @ModelAttribute("kpiLibForm") @Valid KpiLibForm kpiLibForm,
                    BindingResult errors) {
        logger.info("进入新增指标库了"+kpiLibForm.getKpiType());
        logger.info("进入新增指标库userId="+kpiLibForm.getUserId());
        KpiLib newLib = new KpiLib();
        Integer userId = 0;
//        if("general".equals(kpiLibForm.getKpiType())){
//        	userId=0;
//        }else{
        	userId = kpiLibForm.getUserId() ;
//        }
    	User user = userService.findUserById(userId);
        newLib.setUser(user);
        
        if(kpiLibForm.getStakeholder()!=null){

            newLib.setStakeholder(userService.findUserById(kpiLibForm.getStakeholder().getId()));
        }
        
        newLib.setKpiName(ComputedVar.replaceKpiLib(StringEscapeUtils.escapeHtml4(kpiLibForm.getKpiName())));
        logger.info(newLib.getKpiName());
        newLib.setKpiType(kpiLibForm.getKpiType());
        newLib.setDel(false);
        newLib.setUseType("import");

        logger.info("新增指标库了："+kpiLibForm.getKpiName());
        try {
            kpiLibService.add(newLib);
            HttpUtility.writeToClient(response,(newLib.getId() != null)+"");
        } catch (Exception e) {
            logger.error(e);
        }

    }

    /**
     * 查看-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String show(@RequestParam String id, HttpServletRequest request,
                       HttpServletResponse response) {
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - start");
        }

        request.setAttribute("id", id);
        String returnString = ForwardUtility
                .forwardAdminView("/template/frames/module_users/kpiLib/kpi_lib_show");
        if (logger.isDebugEnabled()) {
            logger.debug("edit(String, HttpServletRequest, HttpServletResponse) - end");
        }
        return returnString;
    }
    /**
     * 修改-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit2", method = RequestMethod.GET)
    public String editUI2(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
        	request.setAttribute("thisurl", "/kpi/lib/edit2");
        	request.setAttribute("this2", "2");
    	return editUI(request,
                response, form);
    }
    /**
     * 修改-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUI(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
       	if(request.getAttribute("thisurl")==null){//为了复用这个功能
        	request.setAttribute("thisurl", "/kpi/lib/edit");
    	}

        KpiLib kpiLib = kpiLibService.findKpiLibById(form.getId());
//    	String hisIdStr = request.getParameter("userId");
        logger.info("要为谁配置指标库="+request.getParameter("userId"));
        request.setAttribute("userId", request.getParameter("userId"));
        //为他添加什么类型的指标库
        logger.info("他添加什么类型的指标库="+request.getParameter("kpiType"));
        request.setAttribute("kpiType", request.getParameter("kpiType"));
        if(CommonUtility.isNonEmpty(kpiLib.getKpiType())){
        	request.setAttribute("kpiType", kpiLib.getKpiType());
        }

        List<User> userList = userService.findAll(new User());
        request.setAttribute("userList", userList);
        KpiUserBase userBase = kpiUserBaseService.findKpiUserBaseByUserId(kpiLib.getUser().getId());
        request.setAttribute("userBase", userBase);
        logger.info(PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())+"");
        String isEmployees = PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())+"";
        request.setAttribute("isEmployees",isEmployees);
        request.setAttribute("kpiLib", kpiLib);
//        logger.info(PersonnelCategory.employees.toString().equals(kpiLib.getPersonnelCategory())+"");
//        String isEmployees = PersonnelCategory.employees.toString().equals(userBase.getPersonnelCategory())+"";
//        request.setAttribute("isEmployees",isEmployees);
        
        return ForwardUtility.forwardAdminView("/kpi/underlying/modal_kpi_lib_edit");
    }

    @RequestMapping(value = "/edit2", method = RequestMethod.POST)
    public void edit2(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") KpiLibForm form) {
    
    	edit(request, response,
                 form);
    }
    /**
     * 保存修改-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") KpiLibForm form) {
        if (logger.isDebugEnabled()) {
            logger.debug("==> Start save edit kpi_lib.");
        }

        // #########################################################
        // 校验提交信息
        // #########################################################
        if (form.getId() == null) {
            HttpUtility.writeToClient(response, "false");
            logger.debug("==> Save edit kpi_lib failed.");
            logger.debug("==> End save edit kpi_lib.");
            return;
        }

        // #########################################################
        // 校验通过
        // #########################################################
        Integer id = form.getId();
		KpiLib lib = kpiLibService.findKpiLibById(id);
		lib.setKpiName(ComputedVar.replaceKpiLib(StringEscapeUtils.escapeHtml4(form.getKpiName())));
		logger.debug(lib.getKpiName());
		if(form!=null && form.getStakeholder()!=null){
			lib.setStakeholder(form.getStakeholder());
		}
		kpiLibService.update(lib);
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End save edit kpi_lib.");
        return;
    }
    @RequestMapping(value = "/delete2")
    public void batchDelete2(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
        logger.debug("==> Start delete kpi_lib.");
    
         batchDelete(request,response,form) ;
//        return "forward:/kpi/lib/delete";
    }
    /**
     * 批量删除-指标库表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/delete")
    public void batchDelete(HttpServletRequest request,
                            HttpServletResponse response, @ModelAttribute("form") KpiLibForm form) {
        logger.debug("==> Start delete kpi_lib.");

        Integer[] ids = form.getIds();
        logger.debug("==> To delete kpi_lib [" + CommonUtility.toJson(ids) + "]");
        if (ids != null && ids.length > 0) {
			kpiLibService.delete(ids);
        }
        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End delete kpi_lib.");
    }

	Queue<String> examExcelHandleFormQueue = new LinkedBlockingQueue<String>();
	private class KpiExcelHandleThread implements Runnable {
        public void run() {
			while(!examExcelHandleFormQueue.isEmpty()){
				String examExcelHandleForm = examExcelHandleFormQueue.poll();
				if (null != examExcelHandleForm){
					List<KpiLibDetails> examPapers = KpiLibExcelUtil.readExcelToObj(examExcelHandleForm);
					if (null != examPapers && !examPapers.isEmpty()) {
						examPapers.stream().forEach(examPaper -> {
							 KpiLib newLib = new KpiLib();
							    	User user = userService.findDeptmentAndUser(examPaper.getDepartmentName(), examPaper.getUserName());
							    	if(user!=null){

								        newLib.setUser(user);
								        if(StringUtils.isNotBlank(examPaper.getStakeholderName())){

								        	User s = userService.findByRealame(examPaper.getStakeholderName().trim());
								        	if(s==null){
								        		throw new RuntimeException("干系人没查到");
								        	}
								            newLib.setStakeholder(s);
								        }
								        
								        newLib.setKpiName(ComputedVar.replaceKpiLib(StringEscapeUtils.escapeHtml4(examPaper.getKpiName())));
								        logger.info(newLib.getKpiName());
								        
//								        指标类型:general 通用指标，purpose 岗位指标 */
								        if(StringUtils.isNotBlank(examPaper.getKpiType())){

									        if("通用指标".equals(examPaper.getKpiType().trim())){
									        	newLib.setKpiType("general");
									        }else if("岗位指标".equals(examPaper.getKpiType().trim())){
										        newLib.setKpiType("purpose");
									        }
								        }
								        newLib.setDel(false);
								        newLib.setUseType("import"); 
								        kpiLibService.add(newLib);
							    	}
						});
					}
				}
	    	}
		}
	}
//	 * 提交指标库批量导入结果

	@RequestMapping(value = "kpiLibUp", method = RequestMethod.GET)
	public String renovate(HttpServletRequest request, HttpServletResponse response){
		
		return ForwardUtility.forwardAdminView("/kpi/underlying/model_kpi_lib_up");
	}
	/**
	 * 提交指标库批量导入结果
	 * @param request
	 * @param response
	 * @return
	 */

	@ResponseBody
	@RequestMapping(value = "kpiLibUp", method = RequestMethod.POST)
	public JsonResult<Boolean> importFiles(HttpServletRequest request, HttpServletResponse response
			){
		String filePath = com.easycms.common.logic.context.ContextPath.fileBasePath
				+ com.easycms.common.util.FileUtils.questionFilePath;
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
        if (fileInfos != null && fileInfos.size() > 0) {
            for (FileInfo fileInfo : fileInfos) { 
                String fileName = fileInfo.getFilename();
                if (fileName.length() > 90) {
                	fileName = fileName.substring(0, 90);
                }
                
                String suffix = null;
                if (fileInfo.getNewFilename().contains(".")) {
                	suffix = fileInfo.getNewFilename().substring(fileInfo.getNewFilename().lastIndexOf(".") + 1, fileInfo.getNewFilename().length());
                }
                if (CommonUtility.isNonEmpty(suffix)) {
                	suffix = suffix.toLowerCase();
                }
                
                if ("xls".equals(suffix) || "xlsx".equals(suffix)) {
                	//创建处理线程 
                	boolean offerResult = examExcelHandleFormQueue.offer(fileInfo.getAbsolutePath());
    				if (offerResult && (poolTaskExecutor.getPoolSize() < poolTaskExecutor.getMaxPoolSize())){
    					Thread examExcelHandleThread = new Thread(new KpiExcelHandleThread());
    					poolTaskExecutor.execute(examExcelHandleThread);
    				}
                } else {
                	FileUtils.remove(fileInfo.getAbsolutePath());
                	result.setCode("-1000");
    		    	result.setMessage("指标库格式有误");
    		    	return result;
                }
            }
            result.setResponseResult(true);
            return result;
        }
		
		return null;
	}
}