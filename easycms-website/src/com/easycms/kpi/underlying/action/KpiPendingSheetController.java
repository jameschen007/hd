package com.easycms.kpi.underlying.action;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.KpiPendingPageResult;
import com.easycms.kpi.underlying.domain.*;
import com.easycms.kpi.underlying.form.KpiSummarySheetForm;
import com.easycms.kpi.underlying.service.*;
import com.easycms.kpi.underlying.service.impl.chain.FlowName;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.UserForm;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>查询绩效待办列表<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/summary/sheet")
public class KpiPendingSheetController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiPendingSheetController.class);

    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;
    @Autowired
    private IKpiPendingSheetService kpiPendingSheetService;
    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;
    @Autowired
    private KpiMonthlyMainService kpiMonthlyMainService;

    @Autowired
    private KpiUserBaseService kpiUserBaseService;
    @Autowired
    private UserService userService;

    /**
     * 用户模块 - 用户列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("==> Start show user list.");
        return ForwardUtility.forwardAdminView("/kpi/score/list_kpending");
    }

    /**
     * 数据片段
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public String dataList(HttpServletRequest request,
                           HttpServletResponse response, @ModelAttribute("form") UserForm form) {
        form.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show user data list.");
        form.setFilter(CommonUtility.toJson(form));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/data/data_json_kpending");
        return returnString;
    }

    /**
     * 查询绩效待办列表UI
     *
     * @param request
     * @param response
     * @param kpiSummarySheetForm
     * @return
     */
    @RequestMapping(value = "/pendingList", method = RequestMethod.GET)
    public String pendingList(HttpServletRequest request,
                              HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiSummarySheet data list.");
        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));

        List<KpiSummarySheet> kpiCurrentSheet = kpiSummarySheetService.getCurrentSheet();
        request.setAttribute("kpiCurrentSheetList", kpiCurrentSheet);


        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
        return returnString;
    }

    /**
     * 查询绩效待办列表
     *
     * @param request
     * @param response
     * @return
     */
//    @RequestMapping(value = "/pendingList", method = RequestMethod.POST)
//    public String pendingListData(HttpServletRequest request,
//                                  HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
//        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
//        logger.debug("==> Show KpiSummarySheet data list.");
//        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
//        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));
//
//        List<KpiSummarySheet> kpiCurrentSheet =  kpiSummarySheetService.getCurrentSheet();
//        request.setAttribute("kpiCurrentSheetList", kpiCurrentSheet);
//
//
//        String returnString = ForwardUtility
//                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
//        return returnString;
//    }
    @ResponseBody
    @RequestMapping(value = "/pendingList", method = RequestMethod.POST)
    @ApiOperation(value = "查询绩效待办列表", notes = "查询绩效待办列表")
    public JsonResult<KpiPendingPageResult> witness(HttpServletRequest request, HttpServletResponse response,
                                                    @ModelAttribute("requestForm") Map<String, Object> map) {
        //KpiPendingSheet kpiPendingSheet
        JsonResult result = new JsonResult();


        if (map != null && null != map.get("pageNumber") && null != map.get("pageSize")) {
            Page<KpiPendingSheet> page = new Page<KpiPendingSheet>();

            page.setPageNum((Integer) map.get("pageNumber"));
            page.setPagesize((Integer) map.get("pageSize"));


//            Page<KpiPendingSheet> pageBySql = kpiPendingSheetService.findPageBySql(page, map);


//WorkStepWitness
//            loginUser = userService.findUserById(witnessRequestForm.getLoginId());
//            if(witnessRequestForm.getUserId()!=null){
//                user = userService.findUserById(witnessRequestForm.getUserId());
//            }
//
//            WitnessPageResult witnessPageDataResult = witnessApiService.getWitnessPageResult(page, user, witnessRequestForm.getStatus(), witnessRequestForm.getType() , witnessRequestForm.getNoticePointType(), witnessRequestForm.getKeyword(),loginUser,witnessRequestForm.getRoleId());
//            result.setResponseResult(pageBySql);
            result.setMessage("成功获取绩效待办列表");
            return result;
        } else {
            logger.debug("分页信息有缺失");
            result.setCode("-1002");
            result.setMessage("分页信息有缺失.");
            return result;
        }
    }

    /**
     * 修改用户
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUI(HttpServletRequest request,
                         HttpServletResponse response, @ModelAttribute("form") UserForm form) {
        return ForwardUtility.forwardAdminView("/user-group/modal_user_edit");
    }

    /**
     * 保存修改
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(HttpServletRequest request, HttpServletResponse response,
                     @ModelAttribute("form") UserForm form) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("==> Start save eidt user.");
        }

        // #########################################################
        // 校验提交信息
        // #########################################################
        if (form.getId() == null) {
            HttpUtility.writeToClient(response, "false");
            logger.debug("==> Save eidt user failed.");
            logger.debug("==> End save eidt user.");
            return;
        }

        // #########################################################
        // 校验通过
        // #########################################################
        Integer id = form.getId();
//        KpiPendingSheet byId = kpiPendingSheetService.findById(id);
//        User user = userService.findUserById(id);
//        user.setName(form.getName());
//        user.setRealname(form.getRealname());
//        user.setEmail(form.getEmail());

        HttpUtility.writeToClient(response, "true");
        logger.debug("==> End save eidt user.");
        return;
    }

    /**
     * 绩效待办详情UI
     *
     * @param request
     * @param response
     * @param mainId
     * @return
     */
    @RequestMapping(value = "/pendingDetail", method = RequestMethod.GET)
    public String pendingListUI(HttpServletRequest request,
                                HttpServletResponse response, @NotNull Integer mainId) {
        logger.debug("[kpiMainId] ==> " + mainId);
        request.setAttribute("kpiMainId", mainId);

        KpiMonthlyMain main = kpiMonthlyMainService.findKpiMonthlyMainById(mainId);
        request.setAttribute("sId", main.getId());
        Integer hisId = main.getOwner().getId();
        KpiUserBase userBase =kpiUserBaseService.findKpiUserBaseByUserId(hisId);
        User he = main.getOwner();
        //查询基本信息
        request.setAttribute("ownBase", userBase);
        request.setAttribute("he", he);
        request.setAttribute("main", main);

        List<KpiHandleExchange> dbFlows = kpiHandleExchangeService.findByMainId(mainId);

        List<KpiHandleExchange> dbFlowsRet = new LinkedList<>();
        for (KpiHandleExchange flow :
                dbFlows) {
            User user = userService.findUserById(flow.getHandleUserId());
            flow.setHandleUserName(user.getRealname());


            for (FlowName flowx : FlowName.values()) {
                if (flowx.getCode().equals(flow.getSegment())) {
                    flow.setSegmentName(flowx.getMsg());
                }
            }

            if ("Y".equals(flow.getVisitStatus())) {
                flow.setVisitStatus("待处理中");
            }else{
                flow.setVisitStatus("已处理");
            }

            if ("pending".equals(flow.getSegmentStatus())) {
                dbFlowsRet.add(flow);
            }
        }
        request.setAttribute("users", userService.findAll());
        request.setAttribute("dbFlows", dbFlowsRet);

//        request.setAttribute("purposeList", plist);
        return ForwardUtility.forwardAdminView("/kpi/score/modal_page_lib_seePending");
    }

    /**
     * 绩效待办详情
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/pendingDetail", method = RequestMethod.POST)
    public void pendingDetail(HttpServletRequest request, HttpServletResponse response, @NotNull Integer exchangeId, @NotNull Integer handleUserId) throws Exception {

        boolean update = kpiHandleExchangeService.update(exchangeId, handleUserId);
        HttpUtility.writeToClient(response, String.valueOf(update));
    }


}