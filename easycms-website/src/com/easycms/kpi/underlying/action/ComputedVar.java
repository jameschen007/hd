package com.easycms.kpi.underlying.action;

public class ComputedVar {

	public static void main(String[] args) {
		String i = "\\/";
		System.out.println(replaceKpiLib(i));
	}

	public static String replaceKpiLib(String in){
		synchronized(in) {

			in = in.replace("\\","\\\\");
			in = in.replace("\\/","\\\\\\/");
			
			return in ;
		}
	}
}
