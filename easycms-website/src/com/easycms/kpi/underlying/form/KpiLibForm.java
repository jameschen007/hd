package com.easycms.kpi.underlying.form;

import java.io.Serializable;
import java.util.Date;

import com.easycms.core.form.NoUserIdBasicForm;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-20 下午05:15:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiLibForm extends NoUserIdBasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;

    /** 默认ID */
    private Integer id;
    /** 指标名称 */
    private String kpiName;
    /** 用户id */
    private Integer userId;
    /** 干系人_用户id */
    private User stakeholder;
    /** 指标类型:general 通用指标，purpose 岗位指标 */
    private String kpiType;
    /** 使用类型:import 导入类型，mine 我的类型 */
    private String useType;
    /** 删除状态 */
    private boolean isDel;
    /** 创建时间 */
    private Date createTime;
    /**
     * 考核开始日期
     */
    private String cycleStart;
    /**
     * 考核结束日期
     */
    private String cycleEnd;


    /**获取  默认ID */
    public Integer getId() {
        return id;
    }

    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    /**获取  指标名称 */
    public String getKpiName() {
        return kpiName;
    }

    /**设置  指标名称 */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }


    /**获取  用户id */
    public Integer getUserId() {
        return userId;
    }

    /**设置  用户id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    /**获取  干系人_用户id */
    public User getStakeholder() {
        return stakeholder;
    }

    /**设置  干系人_用户id */
    public void setStakeholder(User stakeholder) {
        this.stakeholder = stakeholder;
    }


    /**获取  指标类型:general 通用指标，purpose 岗位指标 */
    public String getKpiType() {
        return kpiType;
    }

    /**设置  指标类型:general 通用指标，purpose 岗位指标 */
    public void setKpiType(String kpiType) {
        this.kpiType = kpiType;
    }


    /**获取  使用类型:import 导入类型，mine 我的类型 */
    public String getUseType() {
        return useType;
    }

    /**设置  使用类型:import 导入类型，mine 我的类型 */
    public void setUseType(String useType) {
        this.useType = useType;
    }


    /**获取  删除状态 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }


    /**获取  创建时间 */
    public Date getCreateTime() {
        return createTime;
    }

    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }

	public String getCycleStart() {
		return cycleStart;
	}

	public void setCycleStart(String cycleStart) {
		this.cycleStart = cycleStart;
	}

	public String getCycleEnd() {
		return cycleEnd;
	}

	public void setCycleEnd(String cycleEnd) {
		this.cycleEnd = cycleEnd;
	}


}
