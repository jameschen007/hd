package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.management.user.domain.User;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiMonthlyMainService{
	
	/**
	 * 如果没有,则进行添加后返回
	 * @param owner
	 * @param stakeholder
	 * @param main
	 * @return
	 */
	KpiMonthlyMain findOrAdd(KpiMonthlyMain main);

    /**
     * 查找所有
     * @return
     */
    List<KpiMonthlyMain> findAll();

    List<KpiMonthlyMain> findAll(KpiMonthlyMain condition);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiMonthlyMain> findPage(Specifications<KpiMonthlyMain> sepc,Pageable pageable);


    Page<KpiMonthlyMain> findPage(Page<KpiMonthlyMain> page);

    Page<KpiMonthlyMain> findPage(KpiMonthlyMain condition,Page<KpiMonthlyMain> page);

    /**
     *
     * @return
     */
    KpiMonthlyMain userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiMonthlyMain findKpiMonthlyMain(String username);

    /**
     * 根据ID查找用户
     * @param id
     * @return
     */
    KpiMonthlyMain findKpiMonthlyMainById(Integer id);

    /**
     * 添加用户
     * @param kpiMonthlyMain
     * @return
     */
    KpiMonthlyMain add(KpiMonthlyMain kpiMonthlyMain);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiMonthlyMain
     * @return
     */
    KpiMonthlyMain update(KpiMonthlyMain kpiMonthlyMain);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiMonthlyMain> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiMonthlyMain> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiMonthlyMain> findKpiMonthlyMainByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiMonthlyMain 发起问题的kpiMonthlyMain
     * @return
     */
    List<KpiMonthlyMain> getSolvers(KpiMonthlyMain kpiMonthlyMain);

	KpiMonthlyMain findKpiMonthlyMainByUserIdMonth(Integer userId, String month);

	/**
	 * 修改考核周期字符串
	 * @param mainId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	String updateMonth(Integer userId,Integer mainId,String startDate,String endDate);
	

    /**
     * 根据用户和开始时间查当 开始时间月的主信息id
     * @param userId
     * @param startDate
     * @return
     */
    public Integer getMainIdByUserIdAndStartDate(Integer userId,String startDate);

    /**
     * 环节处理公共方法
     * @return
     */
    public boolean commonHandleLink(HttpServletRequest request);
}
