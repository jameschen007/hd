package com.easycms.kpi.underlying.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiLib;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-24 下午07:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiLibService{

    /**
     * 查找所有
     * @return
     */
    List<KpiLib> findAll();

    List<KpiLib> findAll(KpiLib condition);
    

    List<KpiLib> findByUserIdUseType(Integer userId,String useType);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiLib> findPage(Specifications<KpiLib> sepc,Pageable pageable);


    Page<KpiLib> findPage(Page<KpiLib> page);

    Page<KpiLib> findPage(KpiLib condition,Page<KpiLib> page);

    /**
     *
     * @return
     */
    KpiLib userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiLib findKpiLib(String username);

    /**
     * 根据ID查找用户
     * @param id
     * @return
     */
    KpiLib findKpiLibById(Integer id);

    /**
     * 添加用户
     * @param kpiLib
     * @return
     */
    KpiLib add(KpiLib kpiLib);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiLib
     * @return
     */
    KpiLib update(KpiLib kpiLib);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiLib> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiLib> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiLib> findKpiLibByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiLib 发起问题的kpiLib
     * @return
     */
    List<KpiLib> getSolvers(KpiLib kpiLib);

}