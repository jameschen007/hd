package com.easycms.kpi.underlying.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.poi.excel.ExcelColumn;
import com.easycms.common.poi.excel.ExcelHead;
import com.easycms.common.poi.excel.ExcelHelper;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.kpi.underlying.mybatis.dao.KpiLibDetailsMybatisDao;
import com.easycms.kpi.underlying.domain.KpiLibDetails;
import com.easycms.kpi.underlying.domain.KpiUsersLibList;
import com.easycms.kpi.underlying.service.KpiLibMgrService;

@Service("kpiLibMgrService")
public class KpiLibMgrServiceImpl implements KpiLibMgrService {
	private static final Logger logger = Logger.getLogger(KpiLibMgrServiceImpl.class);
	
	@Autowired
	private KpiLibDetailsMybatisDao kpiLibDetailsMybatisDao;

	@Override
	public List<KpiUsersLibList> getKpiLibMgrList(Map<String, Object> mp) {
		return null;
	}

	@Override
	public boolean exportKpiLib(HttpServletRequest request,HttpServletResponse response) {
		boolean status = false;
		try{
			List<KpiLibDetails> kpiLibDetails = kpiLibDetailsMybatisDao.findAll();
			
			//获取模板文件
			String tempFlieName = CommonUtility.getPropertyFile("kpi.properties").getProperty("kpi.KpiLibTemplateFile");
	    	String path = this.getClass().getClassLoader().getResource(tempFlieName).getPath();
	    	logger.debug("模板文件路径："+path);
			File modelFile = new File(path);
			
			//设置Excel每列显示内容
			List<ExcelColumn> columns = new ArrayList<>();
			
			columns.add(new ExcelColumn(0,"departmentName","部门"));
			columns.add(new ExcelColumn(1,"userName","用户名"));
			columns.add(new ExcelColumn(2,"userRealName","姓名"));
			columns.add(new ExcelColumn(3,"kpiName","指标名称"));
			columns.add(new ExcelColumn(4,"kpiType","指标类型"));
			columns.add(new ExcelColumn(5,"stakeholderName","干系人"));
			
			ExcelHead head = new ExcelHead();
			head.setColumns(columns);
			head.setColumnCount(6);
			head.setRowCount(1);

			//写入数据
			ExcelHelper.getInstanse().exportExcelFile(head, modelFile, response.getOutputStream(), kpiLibDetails);
			status = true;
		}catch(Exception e){
			logger.error("数据写入文件出错！");
			e.printStackTrace();
		}
		return status;
	}

}
