package com.easycms.kpi.underlying.service.impl;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.mybatis.dao.KpiPendingSheetMybatisDao;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;
import com.easycms.kpi.underlying.service.IKpiPendingSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>项目名称：</b><br>
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>绩效待办列表		kpi_pending_sheet<br>
 * <b>创建时间：</b><br>
 * <b>@Copyright:</b>2019-爱特联
 */
@Service
public class KpiPendingSheetServiceImpl implements IKpiPendingSheetService {
    @Autowired
    private KpiPendingSheetMybatisDao kpiPendingSheetMybatisDao;
    @Override
    public KpiPendingSheet findById(Integer id) throws Exception {

        KpiPendingSheet t = new KpiPendingSheet();
        t.setId(id);

        List<KpiPendingSheet> r = kpiPendingSheetMybatisDao.findPageBySql(t);
        if (r != null && r.size() > 0) {
            return r.get(0);
        }
        return null;
    }

    @Override
    public Page<KpiPendingSheet> findPageBySql(Page<KpiPendingSheet> page,KpiPendingSheet map) {

        KpiPendingSheet search = new KpiPendingSheet();
        search.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
        search.setPageSize(page.getPagesize());

        if (map.getRealName() != null) {
            search.setRealName(map.getRealName().trim());
        }

        int count = 0;
        List<KpiPendingSheet> result = null;
        result = kpiPendingSheetMybatisDao.findPageBySql(search);
        count = kpiPendingSheetMybatisDao.findPageBySqlCount(search);
        page.execute(count, page.getPageNum(), result);


        page.execute(count, page.getPageNum(), result);


        return page;
    }
}
