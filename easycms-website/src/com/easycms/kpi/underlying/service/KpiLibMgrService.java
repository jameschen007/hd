package com.easycms.kpi.underlying.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.kpi.underlying.domain.KpiUsersLibList;

/**
 * Created by Administrator on 2017/8/24.
 */
public interface KpiLibMgrService {

    /**
     * 获取 [所有人员指标库，多表查询] 信息
     * @author wangz
     * @param mp
     * @return
     */
    public List<KpiUsersLibList> getKpiLibMgrList(Map<String, Object> mp);
    
    /**
     * 导出指标库
     * @author ZengMao
     * @param response
     * @return
     */
    public boolean exportKpiLib(HttpServletRequest request,HttpServletResponse response);
}
