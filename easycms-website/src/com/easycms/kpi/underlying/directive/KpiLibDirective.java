package com.easycms.kpi.underlying.directive;

import java.util.List;
import java.util.Map;

import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.service.KpiLibService;
import com.easycms.management.user.service.UserService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>获取指令-指标库表		kpi_lib<br>
 * <b>创建时间：</b>2017-08-25 上午11:15:52<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Component
public class KpiLibDirective extends BaseDirective<KpiLib> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiLibDirective.class);
    @Autowired
    private KpiLibService kpiLibService;

    @Autowired
    private UserService userService;

    @Override
    protected Integer count(Map params,Map<String,Object> envParams) {
        // TODO Auto-generated method stub
        return kpiLibService.count();
    }

    @Override
    protected List<KpiLib> tree(Map params,Map<String,Object> envParams) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected KpiLib field(Map params,Map<String,Object> envParams) {
        // 指标库表ID信息
        Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
                "id");
        logger.debug("[id] ==> " + id);
        // 查询ID信息
        if (id != null) {
            KpiLib kpiLib = kpiLibService.findKpiLibById(id);
            return kpiLib;
        }

        // 指标库表kpiLibname信息
        String kpiLibname = FreemarkerTemplateUtility.getStringValueFromParams(
                params, "kpiLibname");
        logger.debug("[kpiLibname] ==> " + kpiLibname);
        // 查询kpiLibname信息
        if (CommonUtility.isNonEmpty(kpiLibname)) {
            KpiLib kpiLib = kpiLibService.findKpiLib(kpiLibname);
            return kpiLib;
        }
        return null;
    }

    @Override
    protected List<KpiLib> list(Map params, String filter, String order, String sort,
                                boolean pageable, Page<KpiLib> pager,Map<String,Object> envParams) {
        String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
        String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
        if("findSupKpiLib".equals(func)) {
            Integer roleId = FreemarkerTemplateUtility.getIntValueFromParams(params, "roleId");
            logger.debug("==> RoleId = " + roleId);
            Integer departmentId = FreemarkerTemplateUtility.getIntValueFromParams(params, "departmentId");
            logger.debug("==> DepartmentId = " + departmentId);
            if(roleId != null && departmentId != null){
                List<KpiLib> kpiLibs = kpiLibService.findParentByDepartmentIdAndRoleId(departmentId, roleId);
                logger.debug("==> Find kpiLib size  " + (kpiLibs != null?kpiLibs.size():"0"));
                return kpiLibs;
            }
            return null;
        }

        String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
        logger.info("Search KpiLib : " + search);

        KpiLib condition = null;
        if(CommonUtility.isNonEmpty(filter)) {
            condition = CommonUtility.toObject(KpiLib.class, filter,"yyyy-MM-dd");
        }else{
            condition = new KpiLib();
        }

        if (condition.getId() != null && condition.getId() == 0) {
            condition.setId(null);
        }
        if (CommonUtility.isNonEmpty(condition.getsId())) {
//             为了过滤绩效管理员当前操作的用户sId
            condition.setUser(userService.findUserById(Integer.valueOf(condition.getsId())));
        }
        if (CommonUtility.isNonEmpty(order)) {
            condition.setOrder(order);
        }

        if (CommonUtility.isNonEmpty(sort)) {
            condition.setSort(sort);
        }
        if (CommonUtility.isNonEmpty(custom)) {
        	if("general".equals(custom)||"purpose".equals(custom)){
                condition.setKpiType(custom);
            }
//        	if("general".equals(custom)){
//                condition.setUser(userService.findUserById(0));//通用指标时，0表示。不过滤人员。
//            }
        }

        condition.setDel(false);
        condition.setUseType("import");

//        logger.info("查询指标库的入参："+CommonUtility.toJson(condition));
        // 查询列表
        if (pageable) {
            if (CommonUtility.isNonEmpty(search)){
                condition.setKpiName(search);

                pager = kpiLibService.findPage(condition, pager);
//                pager = kpiLibService.findPage(condition, pager);
            } else {
                pager = kpiLibService.findPage(condition, pager);
//                pager = kpiLibService.findPage(condition, pager);
            }
            return pager.getDatas();
        } else {
            List<KpiLib> datas = kpiLibService.findAll(condition);
//            List<KpiLib> datas = kpiLibService.findAll(condition);
            logger.debug("==> KpiLib length [" + datas.size() + "]");
            return datas;
        }
    }

}