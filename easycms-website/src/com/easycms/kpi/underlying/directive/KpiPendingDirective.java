package com.easycms.kpi.underlying.directive;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;
import com.easycms.kpi.underlying.service.IKpiPendingSheetService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>获取 待办列表查询　<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Component
public class KpiPendingDirective extends BaseDirective<KpiPendingSheet> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiPendingDirective.class);
    @Autowired
    private IKpiPendingSheetService pendingSheetService;

    @Override
    protected Integer count(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected KpiPendingSheet field(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected List<KpiPendingSheet> list(Map params, String filter, String order, String sort,
                                boolean pageable, Page<KpiPendingSheet> pager,Map<String,Object> envParams) {
        String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
        String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");

        String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
        logger.info("Search KpiPendingSheet : " + search);
        KpiPendingSheet condition = null;
        if(CommonUtility.isNonEmpty(filter)) {
            condition = CommonUtility.toObject(KpiPendingSheet.class, filter,"yyyy-MM-dd");
        }else{
            condition = new KpiPendingSheet();
        }

        if (condition.getId() != null && condition.getId() == 0) {
            condition.setId(null);
        }
        if (CommonUtility.isNonEmpty(condition.getsId())) {
//             为了过滤绩效管理员当前操作的用户sId
        }
        if (CommonUtility.isNonEmpty(order)) {
            condition.setOrder(order);
        }

        if (CommonUtility.isNonEmpty(sort)) {
            condition.setSort(sort);
        }

        Map<String, Object> map = new HashMap<>();

        if (CommonUtility.isNonEmpty(custom)) {
        }
		//TODO condition.owner.id 页面存在这里哈
		//     condition.owner.name 存环节

        // 查询列表
        if (pageable) {
            try {
                if (CommonUtility.isNonEmpty(search)){
                    condition.setRealName(search);
                    pager = pendingSheetService.findPageBySql(pager,condition);
                } else {
                    pager = pendingSheetService.findPageBySql(pager,condition);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return pager.getDatas();
        } else {
            logger.error("==> kpiMonthlyMain  这里没有写查询呢");
            return null ;
        }
    }

    @Override
    protected List<KpiPendingSheet> tree(Map params, Map<String, Object> envParams) {
        return null;
    }
}