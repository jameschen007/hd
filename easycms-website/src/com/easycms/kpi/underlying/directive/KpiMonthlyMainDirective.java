package com.easycms.kpi.underlying.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.service.KpiMonthlyMainService;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>获取指令-员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Component
public class KpiMonthlyMainDirective extends BaseDirective<KpiMonthlyMain> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiMonthlyMainDirective.class);
    @Autowired
    private KpiMonthlyMainService kpiMonthlyMainService;

    @Override
    protected Integer count(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected KpiMonthlyMain field(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected List<KpiMonthlyMain> list(Map params, String filter, String order, String sort,
                                boolean pageable, Page<KpiMonthlyMain> pager,Map<String,Object> envParams) {
        String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
        String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");

        String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
        logger.info("Search KpiMonthlyMain : " + search);
        KpiMonthlyMain condition = null;
        if(CommonUtility.isNonEmpty(filter)) {
            condition = CommonUtility.toObject(KpiMonthlyMain.class, filter,"yyyy-MM-dd");
        }else{
            condition = new KpiMonthlyMain();
        }

        if (condition.getId() != null && condition.getId() == 0) {
            condition.setId(null);
        }
        if (CommonUtility.isNonEmpty(condition.getsId())) {
//             为了过滤绩效管理员当前操作的用户sId
        }
        if (CommonUtility.isNonEmpty(order)) {
            condition.setOrder(order);
        }

        if (CommonUtility.isNonEmpty(sort)) {
            condition.setSort(sort);
        }
        if (CommonUtility.isNonEmpty(custom)) {
        }

        condition.setDel(false);
		//TODO condition.owner.id 页面存在这里哈
		//     condition.owner.name 存环节

        // 查询列表
        if (pageable) {
            if (CommonUtility.isNonEmpty(search)){
//                condition.setKpiName(search);
                pager = kpiMonthlyMainService.findPage(condition, pager);
            } else {
                pager = kpiMonthlyMainService.findPage(condition, pager);
            }
            return pager.getDatas();
        } else {
            List<KpiMonthlyMain> datas = kpiMonthlyMainService.findAll(condition);
            logger.debug("==> kpiMonthlyMain length [" + datas.size() + "]");
            return datas;
        }
    }

    @Override
    protected List<KpiMonthlyMain> tree(Map params, Map<String, Object> envParams) {
        return null;
    }
}