package com.easycms.kpi.underlying.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>获取指令-核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Component
public class KpiSummarySheetDirective extends BaseDirective<KpiSummarySheet> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiSummarySheetDirective.class);
    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;
    @Autowired
    private UserService userService;
    @Autowired
    private VariableSetService variableSetService;

    @Override
    protected Integer count(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected KpiSummarySheet field(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    public List<KpiSummarySheet> list(Map params, String filter, String order, String sort, boolean pageable, Page<KpiSummarySheet> pager, Map<String, Object> envParams) {
        String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
        String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");

        String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
        if(search==null){
        	Object valueObj = params.get("searchValue");
        	if(valueObj!=null){
        		search = String.valueOf(valueObj);
        	}
        }
        logger.info("Search KpiLib : " + search);
        String loginId = FreemarkerTemplateUtility.getStringValueFromParams(params, "loginId");
        logger.info("Current LoginId: " + loginId);
        KpiSummarySheet condition = null;
        if(CommonUtility.isNonEmpty(filter)) {
            condition = CommonUtility.toObject(KpiSummarySheet.class, filter,"yyyy-MM-dd");
        }else{
            condition = new KpiSummarySheet();
        }

        if (condition.getId() != null && condition.getId() == 0) {
            condition.setId(null);
        }
        if (CommonUtility.isNonEmpty(order)) {
            condition.setOrder(order);
        }
        if (CommonUtility.isNonEmpty(sort)) {
            condition.setSort(sort);
        }
        if (CommonUtility.isNonEmpty(custom)) {
        }

        condition.setDel(false);
        
        //当前登录人是否为部门负责人
        User currentLoginUser = null;
        boolean isDepartmentHead = false;
        if(CommonUtility.isNonEmpty(loginId)){
        	currentLoginUser = userService.findUserById(Integer.parseInt(loginId));        	
        }
        if(currentLoginUser != null){
        	isDepartmentHead = userService.isRoleOf(currentLoginUser, "DepartmentHead");        	
        }
        logger.info("Current Login User is DepartmentHead:"+isDepartmentHead);
        if(isDepartmentHead){
        	if(currentLoginUser.getDepartment() !=null ){
        		condition.setDepartmentName(currentLoginUser.getDepartment().getName());
        	}
        }
        
        // 查询列表
        if (pageable) {
            if (CommonUtility.isNonEmpty(search)){
                condition.setRealName(search);

                pager = kpiSummarySheetService.findPage(condition, pager);
            } else {
                pager = kpiSummarySheetService.findPage(condition, pager);
            }
            return pager.getDatas();
        } else {
        	if(CommonUtility.isNonEmpty(search)){
            	condition.setMonth(search);
        	}
            List<KpiSummarySheet> datas = kpiSummarySheetService.findAll(condition);
            logger.debug("==> KpiLib length [" + datas.size() + "]");
            return datas;
        }
    }

    @Override
    protected List<KpiSummarySheet> tree(Map params, Map<String, Object> envParams) {
        return null;
    }
}