package com.easycms.kpi.underlying.domain;

import com.easycms.core.form.BasicForm;

import java.io.Serializable;

/**
 * <b>项目名称：</b><br>
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>绩效待办列表		kpi_pending_sheet<br>
 * <b>创建时间：</b><br>
 * <b>@Copyright:</b>2019-爱特联
 */
public class KpiPendingSheet  extends BasicForm implements Serializable, Comparable<KpiPendingSheet> {
    /**
     * 默认ID
     */
    private Integer id;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 姓名
     */
    private String realName;
    /**
     * 月份
     */
    private String month;
    /**
     * 待领导审核
     */
    private String parentok1;
    /**
     * 默认ID
     */
    private String parentOk1Id;
    /**
     * 待自评分
     */
    private String selfscore2;
    /**
     * 默认ID
     */
    private String selfScore2Id;
    /**
     * 待领导评分
     */
    private String parentscore3;
    /**
     * 默认ID
     */
    private String parentScore3Id;
    /**
     * 待干系人评分
     */
    private String stakeholderscore3;
    /**
     * 默认ID
     */
    private String stakeholderScore3Id;
    /**
     * 待领导评分
     */
    private String parentscore4;
    /**
     * 默认ID
     */
    private String parentScore4Id;
    /**
     * 待部门经理评分
     */
    private String departmentmanagerscore4;
    /**
     * 默认ID
     */
    private String departmentManagerScore4Id;
    /**
     * 待分管领导评分
     */
    private String chargeleaderscore4;
    /**
     * 默认ID
     */
    private String chargeLeaderScore4Id;
    /**
     * 待总经理评分
     */
    private String generalscore5;
    /**
     * 默认ID
     */
    private String generalScore5Id;
    /**
     * 待总经理二次评分
     */
    private String generalscore26;
    /**
     * 默认ID
     */
    private String generalScore26Id;

    /**
     * 默认ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 默认ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 部门名称
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * 部门名称
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * 姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 姓名
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 月份
     */
    public String getMonth() {
        return month;
    }

    /**
     * 月份
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * 待领导审核
     */
    public String getParentok1() {
        return parentok1;
    }

    /**
     * 待领导审核
     */
    public void setParentok1(String parentok1) {
        this.parentok1 = parentok1;
    }

    /**
     * 默认ID
     */
    public String getParentOk1Id() {
        return parentOk1Id;
    }

    /**
     * 默认ID
     */
    public void setParentOk1Id(String parentOk1Id) {
        this.parentOk1Id = parentOk1Id;
    }

    /**
     * 待自评分
     */
    public String getSelfscore2() {
        return selfscore2;
    }

    /**
     * 待自评分
     */
    public void setSelfscore2(String selfscore2) {
        this.selfscore2 = selfscore2;
    }

    /**
     * 默认ID
     */
    public String getSelfScore2Id() {
        return selfScore2Id;
    }

    /**
     * 默认ID
     */
    public void setSelfScore2Id(String selfScore2Id) {
        this.selfScore2Id = selfScore2Id;
    }

    /**
     * 待领导评分
     */
    public String getParentscore3() {
        return parentscore3;
    }

    /**
     * 待领导评分
     */
    public void setParentscore3(String parentscore3) {
        this.parentscore3 = parentscore3;
    }

    /**
     * 默认ID
     */
    public String getParentScore3Id() {
        return parentScore3Id;
    }

    /**
     * 默认ID
     */
    public void setParentScore3Id(String parentScore3Id) {
        this.parentScore3Id = parentScore3Id;
    }

    /**
     * 待干系人评分
     */
    public String getStakeholderscore3() {
        return stakeholderscore3;
    }

    /**
     * 待干系人评分
     */
    public void setStakeholderscore3(String stakeholderscore3) {
        this.stakeholderscore3 = stakeholderscore3;
    }

    /**
     * 默认ID
     */
    public String getStakeholderScore3Id() {
        return stakeholderScore3Id;
    }

    /**
     * 默认ID
     */
    public void setStakeholderScore3Id(String stakeholderScore3Id) {
        this.stakeholderScore3Id = stakeholderScore3Id;
    }

    /**
     * 待领导评分
     */
    public String getParentscore4() {
        return parentscore4;
    }

    /**
     * 待领导评分
     */
    public void setParentscore4(String parentscore4) {
        this.parentscore4 = parentscore4;
    }

    /**
     * 默认ID
     */
    public String getParentScore4Id() {
        return parentScore4Id;
    }

    /**
     * 默认ID
     */
    public void setParentScore4Id(String parentScore4Id) {
        this.parentScore4Id = parentScore4Id;
    }

    /**
     * 待部门经理评分
     */
    public String getDepartmentmanagerscore4() {
        return departmentmanagerscore4;
    }

    /**
     * 待部门经理评分
     */
    public void setDepartmentmanagerscore4(String departmentmanagerscore4) {
        this.departmentmanagerscore4 = departmentmanagerscore4;
    }

    /**
     * 默认ID
     */
    public String getDepartmentManagerScore4Id() {
        return departmentManagerScore4Id;
    }

    /**
     * 默认ID
     */
    public void setDepartmentManagerScore4Id(String departmentManagerScore4Id) {
        this.departmentManagerScore4Id = departmentManagerScore4Id;
    }

    /**
     * 待分管领导评分
     */
    public String getChargeleaderscore4() {
        return chargeleaderscore4;
    }

    /**
     * 待分管领导评分
     */
    public void setChargeleaderscore4(String chargeleaderscore4) {
        this.chargeleaderscore4 = chargeleaderscore4;
    }

    /**
     * 默认ID
     */
    public String getChargeLeaderScore4Id() {
        return chargeLeaderScore4Id;
    }

    /**
     * 默认ID
     */
    public void setChargeLeaderScore4Id(String chargeLeaderScore4Id) {
        this.chargeLeaderScore4Id = chargeLeaderScore4Id;
    }

    /**
     * 待总经理评分
     */
    public String getGeneralscore5() {
        return generalscore5;
    }

    /**
     * 待总经理评分
     */
    public void setGeneralscore5(String generalscore5) {
        this.generalscore5 = generalscore5;
    }

    /**
     * 默认ID
     */
    public String getGeneralScore5Id() {
        return generalScore5Id;
    }

    /**
     * 默认ID
     */
    public void setGeneralScore5Id(String generalScore5Id) {
        this.generalScore5Id = generalScore5Id;
    }

    /**
     * 待总经理二次评分
     */
    public String getGeneralscore26() {
        return generalscore26;
    }

    /**
     * 待总经理二次评分
     */
    public void setGeneralscore26(String generalscore26) {
        this.generalscore26 = generalscore26;
    }

    /**
     * 默认ID
     */
    public String getGeneralScore26Id() {
        return generalScore26Id;
    }

    /**
     * 默认ID
     */
    public void setGeneralScore26Id(String generalScore26Id) {
        this.generalScore26Id = generalScore26Id;
    }
    @Override
    public int compareTo(KpiPendingSheet o) {
        return this.id-o.getId();
    }

}
