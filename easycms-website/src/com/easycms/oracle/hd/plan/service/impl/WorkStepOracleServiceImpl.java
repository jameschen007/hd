//package com.easycms.oracle.hd.plan.service.impl;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.easycms.common.util.CommonUtility;
//import com.easycms.core.util.Page;
//import com.easycms.hd.plan.domain.WorkStep;
//import com.easycms.oracle.hd.plan.mybatis.dao.WorkStepOracleDao;
//import com.easycms.oracle.hd.plan.service.WorkStepOracleService;
//
//@Service("workStepOracleService")
//public class WorkStepOracleServiceImpl implements WorkStepOracleService {
//
//	@Autowired
//	private WorkStepOracleDao workStepOracleDao;
//
//	@Override
//	public WorkStep getById(Integer id) {
//		return workStepOracleDao.getById(id);
//	}
//
//	@Override
//	public int getCount(WorkStep workStep) {
//		Long allCount = workStepOracleDao.getCount(workStep);
//
//		if (null == allCount) {
//			allCount = 0l;
//		}
//		
//		return new Long(allCount).intValue();
//	}
//
//	@Override
//	public Page<WorkStep> findByPage(WorkStep workStep, Page<WorkStep> page) {
//		if (CommonUtility.isNonEmpty(page.getPageNum() + "")
//				&& CommonUtility.isNonEmpty(page.getPagesize() + "")) {
//			workStep.setRowStart((page.getPageNum() - 1) * page.getPagesize()
//					+ 1);
//			workStep.setRowEnd(page.getPageNum() * page.getPagesize());
//
//			int count = 0;
//			List<WorkStep> result = workStepOracleDao.findByPage(workStep);
//			count = getCount(workStep);
//			page.execute(count, page.getPageNum(), result);
//			return page;
//		}
//		return null;
//	}
//
//	@Override
//	public List<WorkStep> getByRollingPlanDrawnoAndWeldno(String drawno, String weldno) {
//		WorkStep workStep = new WorkStep();
//		workStep.setDrawno(drawno);
//		workStep.setWeldno(weldno);
//		return workStepOracleDao.getByRollingPlanDrawnoAndWeldno(workStep);
//	}
//
//}
