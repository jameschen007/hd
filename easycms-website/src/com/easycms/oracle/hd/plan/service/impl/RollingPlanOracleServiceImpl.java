//package com.easycms.oracle.hd.plan.service.impl;
//
//import java.util.Date;
//import java.util.List;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.easycms.common.util.CommonUtility;
//import com.easycms.common.util.MessageType;
//import com.easycms.common.util.Messenger;
//import com.easycms.core.util.Page;
//import com.easycms.hd.plan.domain.Isend;
//import com.easycms.hd.plan.domain.QCSign;
//import com.easycms.hd.plan.domain.RollingPlan;
//import com.easycms.hd.plan.domain.StepFlag;
//import com.easycms.hd.plan.domain.WorkStep;
//import com.easycms.hd.plan.service.RollingPlanService;
//import com.easycms.hd.plan.service.WorkStepService;
//import com.easycms.oracle.hd.plan.mybatis.dao.RollingPlanOracleDao;
//import com.easycms.oracle.hd.plan.service.RollingPlanOracleService;
//import com.easycms.oracle.hd.plan.service.WorkStepOracleService;
//
//@Service("rollingPlanOracleService")
//public class RollingPlanOracleServiceImpl implements RollingPlanOracleService {
//	private static final Logger logger = Logger.getLogger(RollingPlanOracleServiceImpl.class);
//	@Autowired
//	private RollingPlanOracleDao rollingPlanOracleDao;
//	@Autowired
//	private WorkStepOracleService workStepOracleService;
//	@Autowired
//	private RollingPlanService rollingPlanService;
//	@Autowired
//	private WorkStepService workStepService;
//
//	@Override
//	public RollingPlan getById(Integer id) {
//		return rollingPlanOracleDao.getById(id);
//	}
//
//	@Override
//	public int getCount(RollingPlan rollingPlan) {
//		Long allCount = rollingPlanOracleDao.getCount(rollingPlan);
//
//		if (null == allCount) {
//			allCount = 0l;
//		}
//		
//		return new Long(allCount).intValue();
//	}
//
//	@Override
//	public Page<RollingPlan> findByPage(RollingPlan rollingPlan, Page<RollingPlan> page) {
//		if (CommonUtility.isNonEmpty(page.getPageNum() + "")
//				&& CommonUtility.isNonEmpty(page.getPagesize() + "")) {
//			rollingPlan.setRowStart((page.getPageNum() - 1) * page.getPagesize()
//					+ 1);
//			rollingPlan.setRowEnd(page.getPageNum() * page.getPagesize());
//
//			int count = 0;
//			List<RollingPlan> result = rollingPlanOracleDao.findByPage(rollingPlan);
//			count = getCount(rollingPlan);
//			page.execute(count, page.getPageNum(), result);
//			return page;
//		}
//		return null;
//	}
//
//	@Override
//	public RollingPlan getByDrawnoAndWeldno(String drawno, String weldno) {
//		RollingPlan rollingPlan = new RollingPlan();
//		rollingPlan.setDrawno(drawno);
//		rollingPlan.setWeldno(weldno);
//		return rollingPlanOracleDao.getByDrawnoAndWeldno(rollingPlan);
//	}
//	
//	@Override
//	public List<RollingPlan> addWorkStep(List<RollingPlan> rollingPlanList){
//		for (RollingPlan rp : rollingPlanList){
//			addWorkStep(rp);
//		}
//		return rollingPlanList;
//	}
//
//	@Override
//	public RollingPlan addWorkStep(RollingPlan rollingPlan) {
//		if (null != rollingPlan){
//			rollingPlan.setWorkSteps(workStepOracleService.getByRollingPlanDrawnoAndWeldno(rollingPlan.getDrawno(), rollingPlan.getWeldno()));
//			return rollingPlan;
//		}
//		return null;
//	}
//
//	@Override
//	public Messenger importToLocal(String[] sIds) {
//		Messenger msg = new Messenger();
//		if (sIds != null && sIds.length > 0) {
//			for (String ids : sIds){
//				try {
//					String drawno = ids.split(":")[0];
//					String weldno = ids.split(":")[1];
//					RollingPlan rollingPlan = getByDrawnoAndWeldno(drawno, weldno);
//					if (null != rollingPlan){
//			        	rollingPlan.setIsend(Isend.INITIAL);
//			        	rollingPlan.setQcsign(QCSign.NOT_CONFIRMED);
//			        	rollingPlan.setCreatedBy("SYSTEM");
//			        	rollingPlan.setCreatedOn(new Date());
//						RollingPlan rp = rollingPlanService.add(rollingPlan);
//						
//						for (WorkStep workStep : workStepOracleService.getByRollingPlanDrawnoAndWeldno(rollingPlan.getDrawno(), rollingPlan.getWeldno())){
//	        				workStep.setRollingPlan(rp);
//	        				workStep.setCreatedOn(new Date());
//	        				workStep.setCreatedBy("SYSTEM");
//	        				workStep.setStepflag(StepFlag.UNDO);
//	        				
//	        				workStepService.add(workStep);
//		        		}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//					logger.error(e.getMessage());
//					msg.addMessenger(MessageType.ERROR, e.getMessage());
//					continue;
//				}
//			}
//		}
//		return msg;
//	}
//
//}
