package com.easycms.common.logic.context.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author wz
 *20180118
 */
public class SysConstant {

	/**
	 * 卡项外网地址
	 */
	public static String K_WW_URL="http://116.236.114.61:9201";	/**
	/**
	 * 福清外网地址
	 */
	public static String FQ_WW_URL = "http://125.77.122.66:9201";
	 /** 卡项项目部名称
	 */
	public static String K_project_name="K2/K3核电项目部";
	 /** 福清项目部名称
	 */
	public static String FQ_project_name="福清核电项目部";

	public static Map<String,String> appWwUrlMap = new HashMap<String,String>();
	static {
		appWwUrlMap.put(EnpConstant.PROJ_CODE_K,K_WW_URL);
		appWwUrlMap.put(EnpConstant.PROJ_CODE_FQ,FQ_WW_URL);
	}
	public static Map<String,String> projectNameMap = new HashMap<String,String>();
	static {
		projectNameMap.put(EnpConstant.PROJ_CODE_K,K_project_name);
		projectNameMap.put(EnpConstant.PROJ_CODE_FQ,FQ_project_name);
	}

	/**
	 * 卡项　Enpower内网地址
	 */
	public static String K_Enp_NW_URL="http://10.1.1.81:8018";
	/**
	 * 福清　Enpower内网地址
	 */
	public static String FQ_Enp_NW_URL="http://10.2.1.51:8018";

	public static Map<String,String> enpNwUrlMap = new HashMap<String,String>();
	static {
		enpNwUrlMap.put(EnpConstant.PROJ_CODE_K,K_Enp_NW_URL);
		enpNwUrlMap.put(EnpConstant.PROJ_CODE_FQ,FQ_Enp_NW_URL);
	}

	
/**
 * 0:0:0:0:0:0:0:1
 */
	public static String ipFromHead0="0:0:0:0:0:0:0:1";
	/**五公司内内ip前两位  10.1. */
	public static String hd5InPrefix = "10.1.";
	/**五公司外内ip前两位  116.236. */
	public static String hd5OutPrefix = "116.236.";
	/**
	 * 测试环境39.108.165.
	 */
	public static String hd39Prefix = "39.108.165.";
	
	/**
	 * apptimezoneoffset
	 * */
	public static String apptimezoneoffset="apptimezoneoffset";
	

	public static String bigPattern="[A-Z]+";
	public static String smallPattern="[a-z]+";
	public static String digitPattern="\\d+";
}
