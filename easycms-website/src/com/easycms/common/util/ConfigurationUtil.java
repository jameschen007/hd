package com.easycms.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConfigurationUtil {
	public static Map<String, String> readFileAsMap(String fileName)
			throws IOException {
		InputStream inputStream = null;

		try {
			inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(fileName);
			if (inputStream != null) {
				BufferedReader bf = new BufferedReader(new InputStreamReader(
						inputStream));
				String line = "";
				LinkedHashMap<String, String> keys = new LinkedHashMap<String, String>();

				while ((line = bf.readLine()) != null) {
					String[] ss = line.split("=");
					String value = convert(ss[1]);
					keys.put(ss[0], value);
				}
				bf.close();
				return keys;
			} else {
				throw new NullPointerException(fileName + " cannot be found!");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	private static String convert(String utfString) {
		StringBuilder sb = new StringBuilder();
		int i = -1;
		int pos = 0;
//这里会把包含英文的中文字段中的英文忽略掉，需要改进。
		if (utfString.contains("\\u")) {
			while ((i = utfString.indexOf("\\u", pos)) != -1) {
				sb.append(utfString.substring(pos, i));
				if (i + 5 < utfString.length()) {
					pos = i + 6;
					sb.append((char) Integer.parseInt(utfString.substring(i + 2, i + 6), 16));
				}
			}
			return sb.toString();
		} else {
			return utfString;
		}

	}
}
