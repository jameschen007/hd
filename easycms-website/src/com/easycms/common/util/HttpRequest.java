package com.easycms.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

@Slf4j
public class HttpRequest {
	public static String sendGet(String url, String param, String authToken) throws Exception {
		log.info("----------------------HttpRequest Get Start--------------------------------");
		log.info("[URL] ==> " + url);
		log.info("[param] ==> " + param);
		log.info("[X-Auth-Token] ==> " + authToken);
		Long startTime = System.currentTimeMillis();
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url;
			if (null != param) {
				urlNameString = url + "?" + param;
			}
			URL realUrl = new URL(urlNameString);
			URLConnection connection = realUrl.openConnection();
			connection.setRequestProperty("Accept", "*/*");
			connection.setRequestProperty("Content-Type", "text/html; charset=utf-8");
			connection.setRequestProperty("X-Auth-Token", authToken);
			connection.connect();
			Map<String, List<String>> map = connection.getHeaderFields();
			for (String key : map.keySet()) {
				log.info(key + "--->" + map.get(key));
			}
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			log.info("Send Get Request Meet Error: " + e);
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
				throw e2;
			}
		}
		Long endTime = System.currentTimeMillis();
		log.info("Call Get Method cost(unit: ms): " + (endTime - startTime));
		log.info("----------------------HttpRequest Get End--------------------------------");
		return result;
	}
	public static String sendPostUsingRestTemplate(String url, String param, String contentType, String charSet) throws Exception {

		log.info("[url] ==> " + url);
		log.info("[param] ==> " + param);
		log.info("[contentType] ==> " + contentType);
		
		RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/x-www-form-urlencoded");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        
        HttpEntity<String> formEntity = new HttpEntity<String>(param, headers);

        String result = restTemplate.postForObject(url, formEntity, String.class);

		log.info("[param] ==> " + result);
		return result ;
	}
	public static String sendPost(String url, String param, String contentType, String charSet) throws Exception {
//		sendPostUsingRestTemplate(url,param,contentType,null);//这个方法在本机上是可以运行的，但是在五公司机器不能运行
		
//		HdHttpUtil.post(url, param);//这个方法在本机上是可以运行的，但是在五公司机器不能运行
		log.info("----------------------HttpRequest Post Start--------------------------------");
		log.info("[URL] ==> " + url);
		log.info("[param] ==> " + param);
		log.info("[content-type] ==> " + contentType);
		if (!CommonUtility.isNonEmpty(charSet)) {
			charSet = "UTF-8";
		}

		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		if (ContextPath.hostAddress.startsWith("10.1.")) {// 五公司内办公环境
			contentType = "application/x-www-form-urlencoded";
		} else if (ConstantVar.HOST_ADDRESS10.equals(ContextPath.hostAddress)) {

		} else {

		}

		log.info("ContextPath.hostAddress ==> " + ContextPath.hostAddress);

		log.info("[charSet] ==> " + charSet);
		log.info("[content-type] ==> " + contentType);

		Long startTime = System.currentTimeMillis();
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			URLConnection conn = realUrl.openConnection();
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Content-Type", contentType);

			conn.setDoOutput(true);
			conn.setDoInput(true);
			out = new PrintWriter(conn.getOutputStream());
			if (null != param) {
				out.print(param);
			}
			out.flush();
			Map<String, List<String>> map = conn.getHeaderFields();
			for (String key : map.keySet()) {
				log.info(key + "--->" + map.get(key));
			}
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charSet));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			log.info("Send POST Request Meet Error:" + e);
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		Long endTime = System.currentTimeMillis();
		log.info("Call Post Method cost(unit: ms): " + (endTime - startTime));
		log.info("----------------------HttpRequest Post End--------------------------------");

		return result;
	}
}