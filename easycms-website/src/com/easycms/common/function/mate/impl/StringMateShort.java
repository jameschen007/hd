package com.easycms.common.function.mate.impl;

import com.easycms.common.function.StringMate;
/**
 * 字符串，匹配出对应的数字
 * @author wz
 *
 */
public class StringMateShort implements StringMate {

	public static void main(String[] args) {
		StringMateShort tt = new StringMateShort();
		short t = tt.mateShort(StringMateShort.before);
		
		System.out.println(t);
	}

	@Override
	public short mateShort(String type) {
		return matingShort(type);
	}

}
