package com.easycms.core.strategy.authorization;


/**
 * 认证器
 * @author Administrator
 *
 */
public class Authenticator<T>{
	
	private Authorization<T> auth;
	
	public Authenticator(Authorization<T> auth){
		this.auth = auth;
	}
	
	
	public void setAuth(Authorization<T> auth) {
		this.auth = auth;
	}


	/**
	 * 认证器执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public T executeAuth(String username,String password, boolean isSession){
		return (T) this.auth.executeAuth(username, password, isSession);
	}
	public T executeAuthMd5(String username,String md5, boolean isSession){
		return (T) this.auth.executeAuthMd5(username, md5, isSession);
	}
}
