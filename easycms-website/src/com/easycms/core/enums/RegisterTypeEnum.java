package com.easycms.core.enums;

public enum RegisterTypeEnum {
	/**
	 * 系统类型
	 */
	SYSTEM,
	/**
	 * LDAP类型
	 */
	LDAP
}
