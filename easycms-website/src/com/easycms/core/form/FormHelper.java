package com.easycms.core.form;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.easycms.core.response.JsonResponse;


public class FormHelper {

	public static void setParams(Object obj,HttpServletRequest request) {
		try {
			Field[] fields = obj.getClass().getDeclaredFields();
			if(fields.length >0) {
				for(Field field : fields) {
					field.setAccessible(true);
					String name = field.getName();
					Object value = field.get(obj);
					request.setAttribute(name, value);
				}
			}
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static JsonResponse printErros(BindingResult errors,Logger log) {
		JsonResponse res = new JsonResponse("-1000","Validate failed");
		Map<String,String> errorsMap = new HashMap<String, String>();
		List<FieldError> errorList = errors.getFieldErrors();
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		for(FieldError error : errorList) {
			sb.append(error.getField());
			if(error.getCode() != null) {
				sb.append("["+error.getCode()+"]");
			}
			
			if(error.getDefaultMessage() != null) {
				sb.append(":" + error.getDefaultMessage());
			}
			sb.append(",");
			errorsMap.put(error.getField(), error.getDefaultMessage() == null?error.getDefaultMessage():error.getCode());
		}
		sb.substring(0,sb.length() - 1);
		sb.append("}");
		log.info("==> Validate failed.Cased by:" + sb.toString());
		
		res.setErrors(errorsMap);
		return res;
	}
	
}
