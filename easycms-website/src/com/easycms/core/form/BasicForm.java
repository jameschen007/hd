
package com.easycms.core.form;

import java.io.Serializable;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.common.util.CommonUtility;

import lombok.Data;



/** 共用表单对象
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
@Data
public class BasicForm implements Serializable{
	
	@JsonIgnore 
	@Transient
	public static final String FUNC_EXIST = "exist";
	@JsonIgnore 
	@Transient
	public static final String FUNC_EXIST_EXCEPT_SELF = "exceptSelfExist";
	
	@JsonIgnore 
	@Transient
	private static final long serialVersionUID = -2457961236194663445L;
	@JsonIgnore 
	@Transient
	private String formatter = "yyyy-MM-dd HH:mm:ss";	//时间格式
	@JsonIgnore 
	@Transient
	private String[] dateFields = null;	//时间类型字段名
	@JsonIgnore 
	@Transient
	private Integer[] ids;	//ID数组
	@JsonIgnore 
	@Transient
	private String[] sIds;	//ID数组
	@JsonIgnore 
	@Transient
	private String sId;	//ID数组
	@JsonIgnore 
	@Transient
	private String page;	//当前页数
	@JsonIgnore 
	@Transient
	private String rows;	//每页大小
	@JsonIgnore 
	@Transient
	private String order;	//升序或降序
	@JsonIgnore 
	@Transient
	private String sort;	//排序
	@JsonIgnore 
	@Transient
	private String queryType;	//查询类型
	@JsonIgnore 
	@Transient
	private String filter;	//过滤条件
	@JsonIgnore 
	@Transient
	private String func = "";
	@JsonIgnore 
	@Transient
	private String custom;
	@JsonIgnore 
	@Transient
	private Integer customId;
	//---------------------------
	@JsonIgnore 
	@Transient
	private String rollingPlanType; //滚动计划类型
	@JsonIgnore 
	@Transient
	private Integer pageNumber = 1;	//当前页数
	@JsonIgnore 
	@Transient
	private Integer pageSize = 10;	//每页大小
	@JsonIgnore 
	@Transient
	private Integer userId;	//用户ID
	//--------------------------------------
	@JsonIgnore 
	@Transient
	private String keyword;
	
	@JsonIgnore 
	public boolean isEmpty(){
		if(CommonUtility.isNonEmpty(page) && CommonUtility.isNonEmpty(rows)) {
			return false;
		}else{
			return true;
		}
	}
	@JsonIgnore 
	public boolean isOrderAsc(){
		return this.order != null && "asc".equalsIgnoreCase(order);
	}
	@JsonIgnore 
	public boolean isOrderDesc(){
		return this.order != null && "desc".equalsIgnoreCase(order);
	}
	@JsonIgnore 
	public boolean isIdsEmpty(){
		return ids == null || ids.length == 0;
	}
	
	public String[] getsIds() {
		return sIds;
	}
	public void setsIds(String[] sIds) {
		this.sIds = sIds;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
}
