package com.easycms.core.authorization;

import com.easycms.management.user.domain.User;

public interface Authorization {

	/**
	 * 执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public User executeAuth(String username,String password);
}
