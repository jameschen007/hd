package com.easycms.core.response;

import java.util.Map;

public class JsonResponse {
	private String code;
	private String message;
	private Map<String,String> errors;
	private Map<String,Object> response;
	
	public JsonResponse(){
		this.code = "1000";
		this.message = "success";
	}
	
	public JsonResponse(String code,String message){
		this.code = code;
		this.message = message;
	}
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String, String> getErrors() {
		return errors;
	}
	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}
	public Map<String, Object> getResponse() {
		return response;
	}
	public void setResponse(Map<String, Object> response) {
		this.response = response;
	}
	
}
