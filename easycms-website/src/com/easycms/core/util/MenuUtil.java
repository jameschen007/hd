package com.easycms.core.util;

import java.util.ArrayList;
import java.util.List;

import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Privilege;

public class MenuUtil {
	/**
	 * 将菜单的权限取出
	 * @param menus
	 * @return
	 */
	public static List<Privilege> getPrivileges(List<Menu> menus) {
		if(menus == null || menus.size() == 0) {
			return null;
		}
		menus = getAsList(menus);
		List<Privilege> privileges = new ArrayList<Privilege>();
		for(Menu m : menus) {
			if(m.getPrivilege() == null) {
				continue;
			}
			privileges.add(m.getPrivilege());
		}
		return privileges;
	}
	
	public static List<Menu> getAsList(List<Menu> menus) {
		if(menus == null || menus.size() == 0) {
			return new ArrayList<Menu>();
		}
		
		List<Menu> dest = new ArrayList<Menu>();
		getAllMenus(menus, dest);
		return dest;
	}
	private static void getAllMenus(List<Menu> target,List<Menu> dest) {
		for(Menu m : target) {
			if(m != null) {
				dest.add(m);
				if(m.getChildren() != null) {
					getAllMenus(m.getChildren(), dest);
				}
			}
		}
	}
}
