package com.easycms.core.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.easycms.management.settings.domain.Website;
import com.easycms.management.user.domain.User;

public class ContextUtil {
	
	public static final String LOCAL_WEBSITE = "LOCAL_WEBSITE";
	public static final String CURRENT_WEBSITE = "CURRENT_WEBSITE";
	public static final String ACTIVE_THEME_DIR = "ACTIVE_THEME_DIR";

	public static String getCurrentRootDir() {
		String rootDir = getCurrentWebsite().getRoot();
		if ("/".equals(rootDir))
			rootDir = getRealPath("");
		return rootDir;
	}
	
	
	
	/**
	 * 获得本地网站设置
	 * @return
	 */
	public static Website getLocalWebsite() {
		return (Website) getServletContext().getAttribute(LOCAL_WEBSITE);
	}
	
	/**
	 * 设置本地网站设置
	 * @param w
	 */
	public static void setLocalWebsite(Website w) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		request.getSession().getServletContext().setAttribute(LOCAL_WEBSITE, w);
	}
	
	public static String getActiveThemeDir() {
		return (String) getServletContext().getAttribute(ACTIVE_THEME_DIR);
	}
	
	/**
	 * 从SESSION中取当前网站设置<br/>
	 * 用于管理多站点
	 * @return
	 */
	public static Website getCurrentWebsite() {
		Website website = (Website) getSession().getAttribute(CURRENT_WEBSITE);
		return website;
	}
	
	/**
	 * 设置当前站点信息
	 * @param w
	 */
	public static void setCurrentWebsite(Website w) {
		getSession().setAttribute(CURRENT_WEBSITE,w);
	}
	
	/**
	 * 设置当前站点信息
	 * @param w
	 */
	public static User getCurrentLoginUser() {
		return (User)getSession().getAttribute("user");
	}
	
	/**
	 * 得到绝对路径
	 * @param path	
	 * @return
	 */
	public static String getRealPath(String path) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		String location = request.getSession().getServletContext()
		.getRealPath(path);
		return location;
	}
	
	/**
	 * 得到request
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request;
	}
	
	/**
	 * 得到session
	 * @return
	 */
	public static HttpSession getSession() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request.getSession();
	}
	
	/**
	 * 得到context
	 * @return
	 */
	public static ServletContext getServletContext() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request.getSession().getServletContext();
	}
}
