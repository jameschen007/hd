package com.easycms.core.freemarker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;

import com.easycms.common.util.CommonUtility;

public class QueryUtil {
	private static final Logger logger = Logger.getLogger(QueryUtil.class);
	
	public static final String TYPE_QUERY = "query";	//查询类型
	public static final String TYPE_TREE = "tree";	
	public static final String TYPE_SEARCH = "search";	
	public static final String TYPE_COUNT = "count";
	
	public static <T> Specification<T> queryConditions(final T t) {
		Specification<T> sepc =  createCondition(t);
		return sepc;
	}
	private static <T> Specification<T> createCondition(final T t) {
		return new Specification<T> () {
			public Predicate toPredicate(Root<T> root,  
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate pre = null;
				Path<String> sortPath = null;

				logger.debug("==> Find field.");
				if(t != null){
					Class<? extends Object> clazz = t.getClass();
					Field[] fields = getClassFields(clazz);
					for(Field field : fields) {
						try {
							String name = field.getName();
//							if("serialVersionUID".equals(name)
//									|| "sort".equals(name)
//									|| "order".equals(name)
//									) {
//								continue;
//							}
							Object result = getFieldValue(t, name);
							
							if(result != null) {
								
								javax.persistence.Transient tran = field.getAnnotation(javax.persistence.Transient.class);
								if(tran != null) {
									continue;
								}
								logger.debug("[name] = " + name);
								logger.debug("[value] = " + result);
								logger.debug("[sortPath] = " + sortPath);
								
								String typeName = field.getGenericType().toString();
								typeName = typeName.substring("class ".length(),typeName.length());
								Class<?> type = Class.forName(typeName);
								logger.debug("[class type] = " + type);
//								logger.debug("[clazz.newInstance() instanceof String] = " + (type.newInstance() instanceof String));
								logger.debug("result.equals(String[].class) = " + result.getClass().equals(String[].class));
								logger.debug("--------------");
								if(result.getClass().equals(String.class)) {
									String value = (String) result;
									Path<String> fieldPath = root.get(name);
//									logger.debug("[fieldPath] = " + fieldPath);
									
									if(CommonUtility.isNonEmpty(value)){
										pre = combinePre(cb, pre, cb.like(fieldPath,"%"+value+"%"));
									}
								}else if(result.getClass().equals(Date.class)) {
									Date value = (Date) result;
									Path<Date> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Integer.class)) {
									Integer value = (Integer) result;
									Path<Integer> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Long.class)) {
									Long value = (Long) result;
									Path<Long> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Short.class)) {
									Short value = (Short) result;
									Path<Short> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Float.class)) {
									Float value = (Float) result;
									Path<Float> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Double.class)) {
									Double value = (Double) result;
									Path<Double> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Boolean.class)) {
									Boolean value = (Boolean) result;
									Path<Boolean> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Byte.class)) {
									Byte value = (Byte) result;
									Path<Byte> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}
							}
							
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					// #########################################################
					// 日期范围
					// #########################################################
					String[] dateFields = (String[]) getFieldValue(t,"dateFields");
					if(dateFields != null && dateFields.length > 0) {
						String formatter = (String) getFieldValue(t, "formatter");
						String startTime = (String) getFieldValue(t, "startTime");
						String endTime = (String) getFieldValue(t, "endTime");
						logger.debug("[formatter] = " + formatter);
						logger.debug("[startTime] = " + startTime);
						logger.debug("[endTime] = " + endTime);
						if(CommonUtility.isNonEmpty(formatter)
								&&CommonUtility.isNonEmpty(startTime)
								&&CommonUtility.isNonEmpty(endTime)
								){
							Date start = CommonUtility.parseDate(startTime, formatter);
							Date end = CommonUtility.parseDate(endTime, formatter);
							
							for(String dateField : dateFields) {
								Path<Date> fieldPath = root.get(dateField);
								pre = combinePre(cb, pre, cb.between(fieldPath,start,getEndDate(end)));
							}
						}
					}
					// #########################################################
					// 排序
					// #########################################################
					String sort = (String) getFieldValue(t, "sort");
					
					if(CommonUtility.isNonEmpty(sort)) {
						sortPath = root.get(sort);
					}
					
					// #########################################################
					// 升降序
					// #########################################################
					Order orderBy = null;
					String order = (String) getFieldValue(t, "order");
					if(CommonUtility.isNonEmpty(order) && sortPath != null) {
						if("desc".equals(order)) {
							orderBy = cb.desc(sortPath);
						}else if("asc".equals(order)) {
							orderBy = cb.asc(sortPath);
						}
						query.orderBy(orderBy);
					}
					
					if(pre != null) {
						query.where(pre);
					}
				}
				
				return pre;  
			}  
		};
	}
	private static Predicate combinePre(CriteriaBuilder cb, Predicate pre,
			Predicate conditionPre) {
		if(pre == null) {
			pre = conditionPre;
		}else{
			pre=cb.and(pre,conditionPre);
		}
		return pre;
	}
	
	public static Field[] getClassFields(Class<?> clazz) {
		List<Field> list = new ArrayList<Field>();
		combineFields(clazz,list);
		return list.toArray(new Field[list.size() - 1]);
	}
	
	private static void combineFields(Class<?> clazz,List<Field> fieldList) {
		logger.debug(fieldList.size());
		Field[] fields = clazz.getDeclaredFields();
		fieldList.addAll(Arrays.asList(fields));
		Class<?> superClazz = clazz.getSuperclass();
		if(superClazz != null) {
			combineFields(superClazz,fieldList);
		}
	}
	
	public static Field getClassField(Class<?> clazz,String fieldName) {
		Field[] fields = getClassFields(clazz);
		for(Field field : fields) {
			if(field.getName().equals(fieldName)){
				return field;
			}
 		}
		return null;
	}
	
	public static Object getFieldValue(Object modal,String fieldName) {
		if(fieldName != null) {
		char[] buffer = fieldName.toCharArray();
		buffer[0] = Character.toUpperCase(buffer[0]);
		String methodName = "get" + new String(buffer);
		Class<?> clazz = modal.getClass();
		Method method = getFieldMethod(clazz,methodName);
		if(method != null) {
			try {
				return method.invoke(modal);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
		return null;
	}
	
	private static Method getFieldMethod(Class<?> clazz,String methodName) {
		try {
			return clazz.getDeclaredMethod(methodName);
		} catch (NoSuchMethodException e) {
			Class<?> superClazz = clazz.getSuperclass();
			if(superClazz != null) {
				return getFieldMethod(superClazz,methodName);
			}
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static Date getEndDate(Date endDate) {
		String time = CommonUtility.formateDate(endDate);
		return CommonUtility.parseDate(time.split(" ")[0] + " 23:59:59");
		
	}
}

