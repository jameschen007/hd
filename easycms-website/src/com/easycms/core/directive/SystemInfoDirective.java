package com.easycms.core.directive;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

/**
 * 当前位置标签
 * 
 * @author jiepeng
 * 
 */
public class SystemInfoDirective implements TemplateDirectiveModel {
	/**
	 * Logger for this class
	 */
	private static final String KEY = "SYS_INFO";
	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {

		Map<String, String> info = new HashMap<String, String>();
		
		Properties props = System.getProperties(); // 获得系统属性集
		String osName = props.getProperty("os.name"); // 操作系统名称
		String osArch = props.getProperty("os.arch"); // 操作系统构架
		String osVersion = props.getProperty("os.version"); // 操作系统版本
		String osUsername = props.getProperty("user.name"); // 用户的账户名称
		String osUserHome = props.getProperty("user.home"); // 用户的主目录 
		String osUserDir = props.getProperty("user.dir"); // 用户的当前工作目录
		String javaVersion = props.getProperty("java.version"); // JAVA版本

		info.put("osName", osName);
		info.put("osArch", osArch);
		info.put("osVersion", osVersion);
		info.put("osUsername", osUsername);
		info.put("osUserHome", osUserHome);
		info.put("osUserDir", osUserDir);
		info.put("javaVersion", javaVersion);
		
		env.setVariable(KEY, DEFAULT_WRAPER.wrap(info));
		body.render(env.getOut());

	}

}
