package com.easycms.management.user.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

import net.sf.json.JSONObject;

/**
 * 获取用户信息指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class UserDirective extends BaseDirective<User> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserDirective.class);
	@Autowired
	private UserService userService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private KpiUserBaseService kpiUserBaseService;

	@Override
	protected Integer count(Map params,Map<String,Object> envParams) {
		// TODO Auto-generated method stub
		return userService.count();
	}

	@Override
	protected List<User> tree(Map params,Map<String,Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected User field(Map params,Map<String,Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			User user = userService.findUserById(id);
			return user;
		}

		// 用户username信息
		String username = FreemarkerTemplateUtility.getStringValueFromParams(
				params, "username");
		logger.debug("[username] ==> " + username);
		// 查询username信息
		if (CommonUtility.isNonEmpty(username)) {
			User user = userService.findUser(username);
			return user;
		}
		return null;
	}

	@Override
	protected List<User> list(Map params, String filter, String order, String sort,
			boolean pageable, Page<User> pager,Map<String,Object> envParams) {
		
		String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
		if("findSupUser".equals(func)) {
			Integer roleId = FreemarkerTemplateUtility.getIntValueFromParams(params, "roleId");
			logger.debug("==> RoleId = " + roleId);
			Integer departmentId = FreemarkerTemplateUtility.getIntValueFromParams(params, "departmentId");
			logger.debug("==> DepartmentId = " + departmentId);
			if(roleId != null && departmentId != null){
				List<User> users = userService.findParentByDepartmentIdAndRoleId(departmentId, roleId);
				logger.debug("==> Find user size  " + (users != null?users.size():"0"));
				return users;
			}
			return null;
		}
		
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search User : " + search);
		
		User condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(User.class, filter,"yyyy-MM-dd");
			JSONObject json = JSONObject.fromObject(filter);
			if(json!=null && json.get("departmentId")!=null && CommonUtility.isNonEmpty(json.get("departmentId").toString()) && !"null".equals(json.get("departmentId").toString().trim())){
				Department dept = departmentService.findById(Integer.valueOf(json.get("departmentId").toString()));
				condition.setDepartment(dept);
			}
		}else{
			condition = new User();
		}
		//绩效考核共用人员表时
		boolean kpimodule = "kpimodule".equals(condition.getCustom());
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}

		condition.setDel(false);
		// 查询列表
		if (pageable) {
			if (CommonUtility.isNonEmpty(search)){
				condition.setRealname(search);
				pager = userService.findPage(condition, pager);
				if(pager.getDatas().isEmpty()){
					condition.setRealname(null);
					condition.setName(search);
					pager = userService.findPage(condition, pager);
				}
			} else {
				pager = userService.findPage(condition, pager);
			}
			
			if(kpimodule){//如果是绩效考核，将人员类别查出来放在custom字段
				List<User> list = pager.getDatas();
				if(list!=null && list.size()>0){
					list.stream().forEach(u->{
						com.easycms.kpi.underlying.domain.KpiUserBase ku = kpiUserBaseService.findKpiUserBaseByUserId(u.getId());
						if(ku!=null){
							u.setCustom(ku.getPersonnelCategory());
						}
					});
				}
			}
			
			return pager.getDatas();
		} else {
			List<User> datas = userService.findAll(condition);
			logger.debug("==> User length [" + datas.size() + "]");
			return datas;
		}
	}

}
