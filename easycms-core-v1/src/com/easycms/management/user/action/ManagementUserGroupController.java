package com.easycms.management.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.easycms.core.util.ForwardUtility;

@Controller
@RequestMapping("/management/usergroup")
public class ManagementUserGroupController {
	/**
	 * 用户管理说明指南
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("")
	public String usergroup(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility.forwardAdminView("/user-group/index_usergroup");
	}
}
