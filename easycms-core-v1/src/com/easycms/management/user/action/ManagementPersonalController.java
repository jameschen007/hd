package com.easycms.management.user.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.ValidateUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.response.AuthorizationResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.PersonalForm;
import com.easycms.management.user.form.validator.PersonalInfoFormValidator;
import com.easycms.management.user.form.validator.PersonalPwdFormValidator;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/management/personal")
public class ManagementPersonalController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ManagementPersonalController.class);

	@Autowired
	private UserService userService;

	/**
	 * 修改个人资料UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String editInfoUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return ForwardUtility.forwardAdminView("/user-group/user_profile");
	}

	/**
	 * 保存个人资料
	 * 
	 * @param request
	 * @param response
	 * @param personalForm
	 * @param errors
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public void modifyInfo(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("personalInfoForm") @Valid PersonalForm personalForm, BindingResult errors) {
		logger.debug("==> Start save personal information.");

		// 校验表单
		PersonalInfoFormValidator validator = new PersonalInfoFormValidator();
		validator.validate(personalForm, errors);
		if (errors.hasErrors()) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save Failed.");
			logger.debug("==> End save personal information.");
			return;
		}

		// 校验通过,更新用户资料
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		user.setRealname(personalForm.getRealname());
		if (personalForm.getEmail() != null) {
			user.setEmail(personalForm.getEmail());
		}
		userService.update(user);
		session.setAttribute("user", user);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> Save success.");
		logger.debug("==> End save personal information.");
		return;
	}

	/**
	 * 修改密码UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/password", method = RequestMethod.GET)
	public String personalPasswordView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return ForwardUtility.forwardAdminView("/user-group/user_pwd");
	}

	/**
	 * 保存修改密码
	 * 
	 * @param request
	 * @param response
	 * @param personalForm
	 * @param errors
	 * @return
	 */
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	public void resetPwd(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("personalPasswordForm") @Valid PersonalForm personalForm, BindingResult errors) {
		logger.debug("==> Start change password.");
		logger.debug(CommonUtility.toJson(personalForm));

		JsonResult<AuthorizationResult> result = new JsonResult<AuthorizationResult>();
		// 校验表单
		PersonalPwdFormValidator validator = new PersonalPwdFormValidator();
		validator.validate(personalForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Change password failed.");
			logger.debug("==> End change password.");
			result.setCode(ExceptionCode.E1);
			result.setMessage("修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}

		// 校验通过,更新用户密码
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		// 密码长度最长20位

		// 密码长度最长16位
		String newp = personalForm.getNewPWD();
		if (newp == null || "".equals(newp)) {
			result.setCode(ExceptionCode.E1);
			result.setMessage("修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}
		if (newp.length() < 8 || newp.length() > 16) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码长度需8-16,修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}

		List<String> ret = ValidateUtility.matchePattern(newp, SysConstant.bigPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含大写字母,修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}
		ret = ValidateUtility.matchePattern(newp, SysConstant.smallPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含小写字母,修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}
		ret = ValidateUtility.matchePattern(newp, SysConstant.digitPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含数字,修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}

		boolean resultb = userService.updatePassword(user.getId(), personalForm.getOldPWD(), personalForm.getNewPWD());
		session.setAttribute("user", user);
		if (resultb) {
			logger.debug("==> Change password success.");
			result.setMessage("修改成功！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		} else {
			logger.debug("==> Change password failed.");
			result.setCode(ExceptionCode.E2);
			result.setMessage("修改失败！");
			HttpUtility.writeToClient(response, CommonUtility.toJson(result));
			return;
		}
	}
}
