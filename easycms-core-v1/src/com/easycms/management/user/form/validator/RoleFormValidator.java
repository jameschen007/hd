package com.easycms.management.user.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.management.user.domain.Role;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class RoleFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(Role.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
		"form.roleName.empty");
	}
}
