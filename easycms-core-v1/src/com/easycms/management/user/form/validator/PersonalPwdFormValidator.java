package com.easycms.management.user.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.common.util.CommonUtility;
import com.easycms.management.user.form.PersonalForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class PersonalPwdFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PersonalForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPWD",
		 "form.oldPassword.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPWD",
		 "form.newPassword.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confPWD",
		 "form.confirmPassword.empty");
		 
		 PersonalForm form = (PersonalForm) target;
		 String newPassword = form.getNewPWD();
		 String confirmPassword = form.getConfPWD();
		 
		 if(CommonUtility.isNonEmpty(newPassword) && CommonUtility.isNonEmpty(confirmPassword)) {
			 if(!newPassword.equals(confirmPassword)) {
				 errors.rejectValue("confirmPassword", "form.password.compare");
			 }
		 }
	}
}
