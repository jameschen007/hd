
package com.easycms.management.user.form;

import java.io.Serializable;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.Department;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class DepartmentForm extends BasicForm implements Serializable,Validator{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5778232707296977882L;
	private Integer id;	
	private String name;
	private Integer parentId;
	private Integer topRoleId;
	
	private String searchValue;
	
	
	
	public String getSearchValue() {
		return searchValue;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public Integer getTopRoleId() {
		return topRoleId;
	}
	public void setTopRoleId(Integer topRoleId) {
		this.topRoleId = topRoleId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return clazz.equals(Department.class);
	}
	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
				 "form.verifyCode.empty");
		
	}
	
}
