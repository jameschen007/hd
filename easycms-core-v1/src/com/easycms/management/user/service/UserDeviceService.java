package com.easycms.management.user.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.management.user.domain.UserDevice;


public interface UserDeviceService {
	
	/**
	 * 保存
	 * @param userDevice
	 * @return
	 */
	public UserDevice add(UserDevice userDevice);
	
	/**
	 * 更新
	 * @param userDevice
	 * @return
	 */
	public UserDevice update(UserDevice userDevice);
	
	/**
	 * 删除
	 * @param id
	 */
	public void delete(Integer id);

	/**
	 * 批量删除
	 * @param ids
	 */
	public void delete(Integer[] ids);
	
	
	/**
	 * 查找
	 * @param id
	 * @return
	 */
	public UserDevice findUserDevice(Integer id);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	public Page<UserDevice> findPage(UserDevice condition,Page<UserDevice> page);
	
	public Page<UserDevice> findPage(Page<UserDevice> page);
	
	/**
	 * 查找所有
	 * @return
	 */
	public List<UserDevice> findAll(UserDevice condition);
	
	public List<UserDevice> findAll();
	
	public Integer count();
	
	public UserDevice getDeviceByDeviceIdAndUserId(Integer userID, String deviceId);
	
	List<UserDevice> getDeviceByUserId(Integer userId);
}
