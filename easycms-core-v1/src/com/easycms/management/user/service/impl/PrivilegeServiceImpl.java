package com.easycms.management.user.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.management.user.dao.PrivilegeDao;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.service.PrivilegeService;
import com.easycms.management.user.service.RoleService;

@Service("privilegeService")
public class PrivilegeServiceImpl implements PrivilegeService {
	private static final Logger logger = Logger
			.getLogger(PrivilegeServiceImpl.class);
	@Autowired
	private PrivilegeDao privilegeDao;

	@Autowired
	private RoleService roleService;

	@Override
	public Privilege save(Privilege p) {
		// TODO Auto-generated method stub
		return privilegeDao.save(p);
	}

	@Override
	public int delete(Integer id) {
		return privilegeDao.deleteById(id);
	}

	@Override
	public void deleteAll() {
		privilegeDao.deleteAll();
	}

	@Override
	public void deleteAll(Integer[] ids) {
		privilegeDao.deleteByIds(ids);

	}

//	@Override
//	public void updatePrivileges(List<Privilege> list) {
//		// 得到所有角色
//		List<Role> roleList = roleService.findAll();
//
//		// 若角色数据不为空，则将角色所包含权限重新赋予
//		if (roleList != null && roleList.size() != 0) {
//
//			List<Role> newRoles = new ArrayList<Role>();
//			for (Role role : roleList) {
//				Role r = new Role();
//				r.setId(role.getId());
//				r.setName(role.getName());
//				logger.debug("==> privilege length = "
//						+ role.getPrivileges().size());
//				r.setPrivileges(role.getPrivileges());
//				newRoles.add(r);
//				// role.getPrivileges().clear();
//			}
//
//			// 清空所有权限
//			logger.debug("==> Delete old privileges.");
//			deleteAll();
//
//			// 插入所有权限
//			logger.debug("==> Add all privileges.");
//			insertAll(list, null);
//
//			logger.debug("==> Recover role's privileges.");
//			// 获得所有新权限
//			List<Privilege> allPrivileges = findAll();
//			for (Role role : newRoles) {
//				List<Privilege> newRolePrivileges = new ArrayList<Privilege>();
//				List<Privilege> rolePrivileges = role.getPrivileges();
//
//				if (rolePrivileges != null && rolePrivileges.size() != 0) {
//					logger.debug("[" + role.getName()
//							+ "'s privilege length] = " + rolePrivileges.size());
//
//					for (Privilege rp : rolePrivileges) {
//						for (Privilege p : allPrivileges) {
//							String rpURI = rp.getUri();
//							String URI = p.getUri();
//
//							if (rpURI == null)
//								rpURI = rp.getName();
//							if (URI == null)
//								URI = p.getName();
//							if (rpURI.equals(URI)) {
//								newRolePrivileges.add(p);
//								break;
//							}
//						}
//					}
//
//					logger.debug("[newRolePrivileges] = "
//							+ CommonUtility.toJson(newRolePrivileges));
//					role.setPrivileges(newRolePrivileges);
//					roleService.update(role);
//				}
//
//			}
//
//		}
//
//	}

	public List<Privilege> findAll() {
		return privilegeDao.findAll(QueryUtil.queryConditions(new Privilege()));
	}

	@Override
	public List<Privilege> findAll(Privilege condition) {
		return privilegeDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public Privilege findById(Integer id) {
		// TODO Auto-generated method stub
		return privilegeDao.findById(id);
	}
	@Override
	public Privilege findByName(String name) {
		// TODO Auto-generated method stub
		return privilegeDao.findByName(name);
	}

	@Override
	public Page<Privilege> findPage(final Privilege p, Page<Privilege> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<Privilege> springPage = privilegeDao
				.findAll(QueryUtil.queryConditions(p), pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	/**
	 * 修改时判断名称是否重复
	 * 
	 * @param name
	 * @return
	 */
	@Override
	public boolean existByNameAndId(String name, int id) {
		// TODO Auto-generated method stub
		return privilegeDao.existByNameAndId(name, id) > 0 ? true : false;
	}
	
	@Override
	public boolean existByUriAndId(String uri, int id) {
		// TODO Auto-generated method stub
		return privilegeDao.existByUriAndId(uri, id) > 0 ? true : false;
	}

	@Override
	public Privilege findByUri(String uri) {
		// TODO Auto-generated method stub
		return privilegeDao.findByUri(uri);
	}

	@Override
	public void addIfNotExist(List<Privilege> list) {
		for (Privilege p : list) {
			if (this.privilegeDao.exist(p.getName(),p.getUri()) == 0) {
				this.privilegeDao.save(p);
				logger.debug("[privilege] = {name:" + p.getName() + ",id:" + p.getId() + "}");
			}
		}
	}

	@Override
	public boolean updateMenu(Integer id, String menuId) {
		// TODO Auto-generated method stub
		return privilegeDao.updateMenu(id, menuId) > 0;
	}

	@Override
	public List<Privilege> findFreePrivileges() {
		// TODO Auto-generated method stub
		return privilegeDao.findNoMenuPrivileges();
	}

	@Override
	public boolean existName(String name) {
		// TODO Auto-generated method stub
		return privilegeDao.existName(name) > 0;
	}

	@Override
	public boolean existUri(String uri) {
		// TODO Auto-generated method stub
		return privilegeDao.existURi(uri) > 0;
	}
}
