package com.easycms.management.user.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;

public interface UserService{

	public User findByNameIsDel1(String username);
	/**
	 * 查找所有
	 * @return
	 */
	List<User> findAll();
	
	List<User> findAll(User condition);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
//	Page<User> findPage(Specifications<User> sepc,Pageable pageable);
	
	
	Page<User> findPage(Page<User> page);
	
	Page<User> findPage(User condition,Page<User> page);
	
	/**
	 * 
	 * @param map 参数名为:username,pwd
	 * @return
	 */
	User userLogin(String username,String password, boolean isSession);
	
	/**
	 * 根据用户名查找用户
	 * @param username
	 * @return
	 */
	User findUser(String username);

	public User findByRealame(String username);
	/**
	 * 根据ID查找用户
	 * @param id
	 * @return
	 */
	User findUserById(Integer id);
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	User add(User user);
	
	boolean deleteByName(String name);
	
	
	boolean deleteByNames(String[] names);
	
	/**
	 * 删除
	 * @param id
	 */
	boolean delete(Integer id);
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	boolean delete(Integer[] ids);
	
	/**
	 * 保存
	 * @param user
	 * @return
	 */
	User update(User user);
	
	/**
	 * 更新密码
	 * @param user
	 * @param newPassword
	 */
	boolean updatePassword(Integer id,String oldPassword,String newPassword);
	
	/**
	 * 用户是否存在
	 * @param username
	 * @return
	 */
	boolean userExist(String username);
	
	boolean userExistExceptWithId(String username,Integer id);
	
	Integer count();
	
	/**
	 * 查找用户
	 * @param departmentId 部门ID
	 * @param roleId 职务ID
	 * @return
	 */
	List<User> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);
	
	/**
	 * 查找上级用户
	 * @param departmentId 部门ID
	 * @param roleId 职务ID
	 * @return
	 */
	List<User> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);
	
	/**
	 * 根据直接领导userid查询下级。
	 * @param userId 用户主键id
	 * @return 
	 */
	List<User> findUserByParentId(Integer userId);

	/**
	 * 根据直接领导userId查询下级，通过部门上下级去查询
	 * @param userId
	 * @return
	 */
	public List<User> findUserByDeptParentId(Integer userId);

	  /**
	   * 根据人员id 查人员的部门的下级部门下的人员，并且角色是指定的角色
	   */
	public List<User> findUserByDeptParentIdAndRoleName(Integer userId,String roleName);
	/**
	 * 根据部门id集合和，角色key查询人员
	 * @param userId
	 * @param roleNameKey
	 * @return
	 */
	public List<User> findUserByDepartmentAndRoleName(List<Department> departments,String roleNameKey);
	public List<User> findUserByDepartmentAndRoleNames(List<Department> departments,String[] roleNameKeys);
	/**
	 * 根据部门查询部门下的人员列表
	 * @param departmentId
	 * @return
	 */
	  public List<User> findUserByDepartmentId(Integer departmentId);
		/**
		 * 根据部门查询部门下的人员列表
		 * @param departmentIds
		 * @return
		 * wangz
		 */
	  public List<User> findUserByDepartmentIds(List<Integer> departmentIds);

	/**
	 * 根据user获取当解决人列表
	 * @param user 发起问题的user
	 * @return
	 */
    List<User> getSolvers(User user);


	/**
	 * 根据User判断 是否是 技术部门的。technologyManagement
	 */
	public boolean isDepartmentOf(User user, String technologyManagement);

	/**
	 * 根据User判断 是否是某个角色的,如 班长 monitor。
	 */
	public boolean isRoleOf(User user, String monitor) ;
	public boolean isRoleOf(User user, String[] monitor) ;

	/**
	 * 根据字符标示的变量到变量表中取出相应的部门id，查询该部门中的所有人员。
	 * @param variable
	 * @return
	 */
	public List<User> findUserByDepartmentVariable(String variableType,String variableKey);
	/**
	 * 查询作业问题待处理人列表,根据变量表配置和模块查询
	 * 
	 * */
	public List<User> findUserByDepartmentVariableAndModule(String variableType, String variableKey,String moduleType);
	  
	/**
	 * 根据字符标示的变量到变量表中取出相应角色，并关联滚动计划类型查询人员。做为供选择的问题指派人。
	 * @param variable
	 * @return
	 */
	public List<User> findUserByDepartmentVariable(String variableType, String variableKey,String moduleType);

	public List<User> findUserByRoleAndDepartmentVariable(String roleVariable, String departmentVariable);
	/**
	 * 根据enpowerid获取用户
	 * @param string
	 * @return
	 */
	User findUserEnpowerId(String string);
	String findRealnameById(Integer id);
	/**
	 * 根据部门名称和人员名称查人员
	 * @param departmentId
	 * @param moduleType
	 * @return
	 */
	User findDeptmentAndUser(String deptName,String realName);
}
