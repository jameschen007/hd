package com.easycms.management.user.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.Role;
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface RoleDao extends BasicDao<Role> ,Repository<Role,Integer>{
	/**
	 * 得到最顶层的父级权限
	 * @return
	 */
	@Query("FROM Role r WHERE r.parent IS NULL and isDel=0")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<Role> findTop();

	@Query("FROM Role r WHERE r.isDel=0")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<Role> findAll2();
	
	/**
	 * 得到子级菜单
	 * @param parentId
	 * @return
	 */
	@Query("FROM Role r WHERE r.parent.id =?1 and isDel=0")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="false") })
	List<Role> findChildren(Integer parentId);
	
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Role o WHERE o.id  IN (?1)")
	int deleteByIds(Integer[] ids);
	
	
	Role getByName(String name);

	@Query("FROM Role r WHERE r.enpowerId =?1")
	Role getByEnpowerId(String enpowerId);

	@Query("FROM Role r WHERE r.enpowerId =?1")
	List<Role> getListByEnpowerId(String enpowerId);

	@Query("FROM Role r WHERE r.roleNo =?1")
	Role findByRoleNo(String roleNo);
}
