package com.easycms.management.user.domain;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name="ec_menu")
@Cacheable
public class Menu extends BasicForm implements Comparable<Menu> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5386363271091316067L;
	
	@Id
	@GenericGenerator(name="idGenerator", strategy="uuid")
	@GeneratedValue(generator="idGenerator",strategy=GenerationType.AUTO)
	private String id;
	
	@Column(name="menu_name")
	private String name;
	
	private Integer seq = 0;
	
	@Column(name="icon_path")
	private String iconPath;
	
	private String style;
	
	@OneToOne(targetEntity = com.easycms.management.user.domain.Privilege.class,fetch=FetchType.EAGER)
	@JoinColumn(name="privilege_id")
	private Privilege privilege;
	
	@ManyToOne(targetEntity = com.easycms.management.user.domain.Menu.class,fetch=FetchType.LAZY)
	@JoinColumn(name="parent_id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Menu parent;
	
	@OneToMany(targetEntity =  com.easycms.management.user.domain.Menu.class,cascade = {CascadeType.REMOVE},mappedBy="parent",fetch=FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<Menu> children;
	
	@ManyToMany(targetEntity=com.easycms.management.user.domain.Role.class,mappedBy="menus",fetch=FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<Role> roles;

	@Column(name="is_del")
	private boolean isDel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Privilege getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}

	public Menu getParent() {
		return parent;
	}

	public void setParent(Menu parent) {
		this.parent = parent;
	}

	public List<Menu> getChildren() {
		return children;
	}

	public void setChildren(List<Menu> children) {
		this.children = children;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public int compareTo(Menu menu) {
		// TODO Auto-generated method stub
		return this.seq - menu.getSeq();
	}

	public boolean isDel() {
		return isDel;
	}

	public void setDel(boolean del) {
		isDel = del;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof Menu) {
        	Menu menu = (Menu) obj;
            return (id.equals(menu.getId()));
        }
        return super.equals(obj);
    }
	
	@Override 
    public int hashCode() {
        return id.hashCode();
    }
}
