package com.easycms.management.settings.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.management.settings.form.WebsiteForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class WebsiteFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(WebsiteForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sitename",
		"form.siteName.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "domain",
		"form.domain.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "keywords",
		"form.keyword.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "describe",
		"form.describe.empty");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maxSize",
		"form.maxSize.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "templUploadDir",
		"form.templUploadDir.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "templUploadType",
		"form.templUploadType.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "resUploadDir",
		"form.resUploadDir.empty");*/
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "root",
		"form.appPath.empty");
		
		/*WebsiteForm form = (WebsiteForm) target;
		String size = form.getMaxSize();
		if(!CommonUtility.isNumber(size)) {
			errors.rejectValue("maxSize", "form.maxSize.number");
		}*/
	}
}
