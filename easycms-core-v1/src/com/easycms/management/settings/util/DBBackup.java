package com.easycms.management.settings.util;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.easycms.common.util.CommonUtility;

/**
 * 数据库备份/恢复工具
 * 
 * @author Administrator
 * 
 */
public class DBBackup {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DBBackup.class);

	
	private static String database = "";
	private static String user = "";
	private static String pwd = "";
	private static DBBackup instance = new DBBackup();

	private DBBackup() {
		Properties p = CommonUtility.getPropertyFile("jdbc.properties");
		database = (String) p.get("jdbc.database");
		user = (String) p.get("jdbc.username");
		pwd = (String) p.get("jdbc.password");
	}

	public static DBBackup getInstance() {
		return instance;
	}

	/**
	 * 执行备份操作
	 * 
	 * @param path
	 *            保存目录
	 * @return true/false
	 */
	public boolean backup(String path) {
		//检查目标是否存在，若不存在，则创建
		File dir = new File(path);
		if(!dir.exists()) {
			dir.mkdirs();
		}
		
		StringBuffer sb = new StringBuffer();
		// 生成保存文件名和路径
		sb.append(database)
				.append(CommonUtility.formateDate(new Date(), "yyyyMMdd-HHmmss"))
				.append(".sql");
		String backupFilename = sb.toString();
		path = CommonUtility.assemblePath(path, backupFilename);
		sb.setLength(0);

//		if (isWindows()) {
//			sb.append("cmd.exe /c ");
//		}

		// 准备备份SQL;
		sb.append("mysqldump -u").append(user).append(" -p").append(pwd)
				.append(" " + database).append(" > ").append(path);
		String backupSQL = sb.toString();
		logger.debug("[backupSQL] = " + backupSQL);
		
		// 执行备份操作
		ProcessBuilder builder = null;
//		Runtime r = Runtime.getRuntime();
		int result = 0;
		try {
//			Process pro = null;
			// 检查平台类型，若是windows平台则添加cms.exe /c命令
			if (isWindows()) {
				builder = new ProcessBuilder("cmd.exe","/c",backupSQL);
//				pro = r.exec(new String[]{"cmd.exe","/c",backupSQL});
			}else{
				builder = new ProcessBuilder("sh","-c",backupSQL);
//				pro = r.exec(new String[]{"sh","-c",backupSQL});
			}
			
			builder.redirectErrorStream(true);
			Process pro = builder.start();
			result = pro.waitFor();
			pro.exitValue();
			if(result != 0) {
				InputStream in = pro.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line = "";
				sb.setLength(0);
				while((line=reader.readLine())!= null) {
					sb.append(line);
				}
				
				logger.error("==> Exe error:" + sb.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = -1;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = -1;
		}
		return result == 0;
	}

	/**
	 * 运行环境是否是windows
	 * @return
	 */
	private static boolean isWindows() {
		String systemName = System.getProperty("os.name");
		systemName = CommonUtility.isNonEmpty(systemName) ? systemName
				.toLowerCase() : systemName;
		return systemName.startsWith("windows");
	}

	/**
	 * 恢复数据库
	 * @param path 数据库文件存放位置
	 * @return true/false
	 */
	public boolean recover(String path) {
		//若为空，则返回false
		if(!CommonUtility.isNonEmpty(path)) return false;
		
		//文件不存在,返回false
		File file = new File(path);
		if (!file.exists()) return false;
		
		//执行恢复操作
		StringBuffer sb = new StringBuffer();
		
		//若是windows环境，加上命令cmd.exe /c
//		if(isWindows()) {
//			sb.append("cmd.exe /c ");
//		}
		
		sb.append("mysql -u").append(user).append(" -p").append(pwd).append(" " + database)
		.append(" < ").append(path);
		String recoverSQL = sb.toString();
		logger.debug("[recoverSQL] = " + recoverSQL);
		
		int result = 0;
		ProcessBuilder builder = null;
		try {
//			Runtime r = Runtime.getRuntime();
//			Process pro = null;
			if (isWindows()) {
				builder = new ProcessBuilder("cmd.exe","/c",recoverSQL);
//				pro = r.exec(new String[]{"cmd.exe","/c",backupSQL});
			}else{
				builder = new ProcessBuilder("sh","-c",recoverSQL);
//				pro = r.exec(new String[]{"sh","-c",backupSQL});
			}
			
			builder.redirectErrorStream(true);
			Process pro = builder.start();
//			pro = r.exec(recoverSQL);
			result = pro.waitFor();
			if(result != 0) {
				InputStream in = pro.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line = "";
				sb.setLength(0);
				while((line=reader.readLine())!= null) {
					sb.append(line);
				}
				logger.error("==> Exe error:" + sb.toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result == 0;
	}

	public static void main(String[] args) {
//		System.out.println(DBBackup.getInstance().backup("E:\\"));
		System.out.println(DBBackup.getInstance().recover("E:\\easycms20140121.sql"));
		// System.out.println(System.getProperty("os.name"));
		// System.out.println(System.getProperty("java.vm.name"));
		// Properties p = System.getProperties();
		// Enumeration<Object> e = p.keys();
		//
		// while(e.hasMoreElements()) {
		// System.out.println(e.nextElement() + " = " +
		// System.getProperty((String)e.nextElement()));
		// }

	}

}
