package com.easycms.management.settings.domain;

import java.io.Serializable;

public class Database implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8569023161831405051L;
	
	/**
	 * 文件名
	 */
	private String name;
	
	/**
	 * 大小
	 */
	private String size;
	
	/**
	 * 最后修改时间
	 */
	private String lastModifyTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}


	public String getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(String lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	} 
	
}
