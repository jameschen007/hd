package com.easycms.management.settings.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ec_site")
public class Website implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8569023161831405051L;
	
	/**
	 * 网站设置Id
	 */
	@Id
	private Integer id;
	
	/**
	 * 网站名称
	 */
	@Column(name="site_name")
	private String sitename;
	
	/**
	 * 网站域名
	 */
	@Column(name="site_domain")
	private String domain; 
	
	/**
	 * 关键字
	 */
	@Column(name="site_key")
	private String keywords; 
	
	/**
	 * 网站描述
	 */
	@Column(name="site_describe")
	private String describe; 
	
	/**
	 * 使用模板名称
	 */
	@Column(name="site_tpl_name")
	private String siteTplName; 
	
	
	/**
	 * 上传文件最大值
	 */
	@Column(name="upload_max_size")
	private Integer uploadMaxSize; 
	
	/**
	 * 模板上传目录
	 */
	@Column(name="upload_tpl_dir")
	private String uploadTplDir; 
	
	/**
	 * 模板上传类型
	 */
	@Column(name="tpl_file_type")
	private String tplFileType; 
	
	/**
	 * 资源上传目录
	 */
	@Column(name="upload_res_dir")
	private String uploadResDir;
	
	/**
	 * 是否本地站点
	 */
	@Column(name="is_local_site")
	private boolean isLocalSite = false;
	
	/**
	 * 是否为本地站点标识
	 */
	@Transient
	private Integer isLocalSiteFlag = 0;
	
	/**
	 * 站点根目录
	 */
	@Column(name="app_path")
	private String root;
	
	/**
	 * 页面大小选择
	 * 数组格式为[1,xxx]
	 */
	@Column(name="pagesize_list")
	private String pagesizeList;
	
	
	@Column(name="pagesize")
	private String defaultPagesize;




	public String getDefaultPagesize() {
		return defaultPagesize;
	}



	public void setDefaultPagesize(String defaultPagesize) {
		this.defaultPagesize = defaultPagesize;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getSitename() {
		return sitename;
	}



	public void setSitename(String sitename) {
		this.sitename = sitename;
	}



	public String getDomain() {
		return domain;
	}



	public void setDomain(String domain) {
		this.domain = domain;
	}



	public String getKeywords() {
		return keywords;
	}



	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}



	public String getDescribe() {
		return describe;
	}



	public void setDescribe(String describe) {
		this.describe = describe;
	}



	public String getSiteTplName() {
		return siteTplName;
	}



	public void setSiteTplName(String siteTplName) {
		this.siteTplName = siteTplName;
	}



	public Integer getUploadMaxSize() {
		return uploadMaxSize;
	}



	public void setUploadMaxSize(Integer uploadMaxSize) {
		this.uploadMaxSize = uploadMaxSize;
	}



	public String getUploadTplDir() {
		return uploadTplDir;
	}



	public void setUploadTplDir(String uploadTplDir) {
		this.uploadTplDir = uploadTplDir;
	}



	public String getTplFileType() {
		return tplFileType;
	}



	public void setTplFileType(String tplFileType) {
		this.tplFileType = tplFileType;
	}



	public String getUploadResDir() {
		return uploadResDir;
	}



	public void setUploadResDir(String uploadResDir) {
		this.uploadResDir = uploadResDir;
	}



	public boolean isLocalSite() {
		return isLocalSite;
	}



	public void setLocalSite(boolean isLocalSite) {
		this.isLocalSite = isLocalSite;
	}



	public String getRoot() {
		return root;
	}



	public void setRoot(String root) {
		this.root = root;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public void setIsLocalSiteFlag(Integer isLocalSiteFlag) {
		this.isLocalSiteFlag = isLocalSiteFlag;
	}



	public Integer getIsLocalSiteFlag() {
		if(this.isLocalSite) this.isLocalSiteFlag = 1;
		return isLocalSiteFlag;
	}



	public String getPagesizeList() {
		return pagesizeList;
	}



	public void setPagesizeList(String pagesizeList) {
		this.pagesizeList = pagesizeList;
	}
	
}
