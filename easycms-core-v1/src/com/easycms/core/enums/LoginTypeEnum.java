package com.easycms.core.enums;

public enum LoginTypeEnum {
	/**
	 * 系统类型
	 */
	SYSTEM,
	/**
	 * LDAP类型
	 */
	LDAP,
	/**
	 * Ajax类型
	 */
	AJAX
}
