package com.easycms.core.enums;

public enum UserTypeEnum {
	/**
	 * 系统类型
	 */
	SYSTEM,
	/**
	 * 前端类型
	 */
	FRONTEND,
	/**
	 * LDAP类型
	 */
	LDAP
}
