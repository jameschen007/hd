package com.easycms.core.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.CMSInit;
import com.easycms.core.util.HttpUtility;
import com.easycms.core.util.MenuUtil;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.domain.User;

public class BackendAuthenticationInterceptor extends HandlerInterceptorAdapter {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(BackendAuthenticationInterceptor.class);


	/**
	 * 过滤不需要进行自动跳转的URL 必须完全匹配才能通过，支持*通配符，更细粒度的校验
	 * 
	 * @param URI
	 * @return
	 */
//	private boolean returnURLCheck(String URI) {
//		boolean result = false;
//		for (String regx : CMSInit.RETURN_URI_LIST) {
//			if (CommonUtility.simpleWildcardMatch(regx, URI)) {
//				result = true;
//				break;
//			}
//		}
//		return result;
//	}

	/**
	 * URI级别 - 检查访问权限 必须完全匹配才能通过，支持*通配符，更细粒度的校验
	 * 
	 * @param URI
	 * @return
	 */
	private boolean permissionCheck(List<String> uris, String URI) {
		boolean result = false;
		for (String regx : uris) {
			if (CommonUtility.simpleWildcardMatch(regx, URI)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * 对已登录的用户权限检查
	 * 
	 * @param URI
	 * @return
	 */
	private boolean checkPrivilege(User user, String uri) {
		if (user == null || !CommonUtility.isNonEmpty(uri))
			return false;
		uri = uri.split(";")[0];
//		logger.debug("[checkURI] = " + uri);
		boolean result = false;
		if (user != null && CommonUtility.isNonEmpty(uri)) {
			if (permissionCheck(CMSInit.LOGIN_UNCHECK_LIST, uri)) {
				return true;
			}

			 List<Privilege> privileges = MenuUtil.getPrivileges(user.getMenus());
			 if (null != privileges && !privileges.isEmpty()) {
				for (Privilege p : privileges) {
					if(!CommonUtility.isNonEmpty(p.getUri())) {
						continue;
					}
					String ownURI = p.getUri();
	//				logger.debug("[ownURI] = " + ownURI);
					if (uri.equals(p.getUri())) {
						result = true;
						break;
					}
				}
			 }

		}
		return result;
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// ####################################################
		// 若有sessionId,则获取session
		// #########################################################
		HttpSession session = null;
		session = request.getSession();
 
		User user = null;
		user = (User) session.getAttribute("user");
		String basePath = request.getContextPath();
		String uri = request.getRequestURI();
		uri = uri.substring(basePath.length(), uri.length());
		//当前节点link
		request.setAttribute("menuLink", uri);
		
		//获取顶级节点
		String topLink = getTopLink(uri);
		request.setAttribute("topLink", topLink);
		
		logger.debug("==> The request URI is [" + uri + "]");
		logger.debug("==> The request topLink is [" + topLink + "]");
		
		if(!CMSInit.BACKEND_CHECK_PREFIX.equalsIgnoreCase(topLink)
				&& !"/".equals(CMSInit.BACKEND_CHECK_PREFIX)) {
			return true;
		}
		
		boolean result = false;
		// ####################################################
		// 未登录用户
		// #########################################################
		if (user == null) {
			//指定URL不进行校验
			if (permissionCheck(CMSInit.ALWAYS_UNCHECK_LIST, uri)) {
				result = true;
			} else {
//				if (!returnURLCheck(uri)) {
//					String reqURL = HttpUtility.getRequestURLWithQueryParrams(request);
//					reqURL = MyURLEncoder.encode(reqURL, "utf-8");
//					CMSInit.LOGIN_URI += "?returnURL=" + reqURL;
//				}
				response.sendRedirect(basePath + CMSInit.BACKEND_LOGIN_URI);
				result = false;
			}
		} 

		// ####################################################
		// 已登录用户
		// #########################################################
		if (user != null) {
			// 放行无需检查的URI
			if (checkPrivilege(user, uri)) {
				result = true;
			} else {
				// 没有权限，则跳转到提示页面
				logger.debug("==> No permission");
				
				if(CMSInit.NO_PERMISSION_URI.equals(uri)){//如果是
					return result;
				}else{
					response.sendRedirect(basePath + CMSInit.NO_PERMISSION_URI);
				}
			}
		}
		
		return result;
	}

	private String getTopLink(String uri) {
		if(uri.startsWith("/admin")) {
			String topLink = uri.replaceFirst("/admin/", "");
			String[] phases = topLink.split("/");
			topLink = "/admin/" + phases[0];
			return topLink;
		}else{
			String[] phases = uri.split("/");
			if(phases.length < 2) {
				return "/";
			}else{
				return "/" + phases[1];
			}
		}
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		request.setAttribute("host", HttpUtility.getHostURL(request));
		super.postHandle(request, response, handler, modelAndView);
	}

	/*
	 * @Override public void afterCompletion(HttpServletRequest request,
	 * HttpServletResponse response, Object handler, Exception ex) throws
	 * Exception { System.out.println("afterCompletion");
	 * super.afterCompletion(request, response, handler, ex); }
	 */
}
