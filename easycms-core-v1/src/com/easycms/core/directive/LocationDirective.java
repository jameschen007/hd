package com.easycms.core.directive;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.service.MenuService;
import com.easycms.management.user.service.PrivilegeService;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

/**
 * 当前位置标签 
 * 
 * @author jiepeng
 * 
 */
public class LocationDirective implements TemplateDirectiveModel {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(LocationDirective.class);

	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();
	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private MenuService menuService;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if(loopVars.length < 1) {
			throw new RuntimeException("Loop variable is required.");
		}
		
		
		
		HttpServletRequest req = ContextUtil.getRequest();
		String uri = req.getRequestURI();
		logger.debug("[uri] = " + uri );
		String contextPath = req.getContextPath();
		logger.debug("[context path] = " + contextPath );
		if(CommonUtility.isNonEmpty(contextPath) && !File.separator.equals(contextPath)) {
			uri = uri.substring(contextPath.length(),uri.length());
		}
		logger.debug("[privilege path] = " + uri );
		
		// #########################################################
		// 查询出菜单
		// #########################################################
		Menu m = menuService.findByUri(uri);
		
		List<Menu> list = new ArrayList<Menu>();
		getLocation(list,m);
		Collections.reverse(list);
		loopVars[0] = DEFAULT_WRAPER.wrap(list);
		body.render(env.getOut());

	}
	
	private void getLocation(List<Menu> list,Menu m) {
		if(m != null) {
			list.add(m);
			Menu parent = m.getParent();
			if(parent != null) {
				getLocation(list,parent);
			}else{
				return;
			}
		}
	}

}
