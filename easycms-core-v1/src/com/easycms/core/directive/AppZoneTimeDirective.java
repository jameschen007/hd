package com.easycms.core.directive;

import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

/**
 * 根据APP的zone输出时间
 * 
 * @author wz
 * 
 */
public class AppZoneTimeDirective implements TemplateDirectiveModel {
	private static Logger logger = Logger.getLogger(AppZoneTimeDirective.class);

//	private static final String KEY = SysConstant.apptimezoneoffset;
	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {

		Integer apptimezoneoffset = FreemarkerTemplateUtility.getIntValueFromParams(params,
				SysConstant.apptimezoneoffset);

		String ret = "";
		if (apptimezoneoffset == null) {
			ret = "";
		} else {
//	        env.setVariable("userList", getBeansWrapper().wrap(userlist));  
			
		}
		Writer out = null;
		try {
			if (body == null) {// 自定义标签必须有内容，即自定义 开始标签与结束标签之间必须有 内容
				throw new TemplateModelException("null body");
			} else {
				// TemplateNumberModel 数字类型， TemplateScalarModel 字符串类型，
				// TemplateBooleanModel boolean类型
				Object dateModel = params.get("date");
				SimpleDateFormat sdf_default = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT+"+apptimezoneoffset));
				String snow;
				try {
					logger.info("dateModel="+dateModel);
					snow = sdf.format(sdf_default.parse(String.valueOf(dateModel)));
					logger.info("DEFAULT_WRAPER.wrap(snow)="+DEFAULT_WRAPER.wrap(snow));
					
					env.setVariable(SysConstant.apptimezoneoffset, DEFAULT_WRAPER.wrap(snow));
					out = env.getOut();
					body.render(out);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (TemplateModelException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
