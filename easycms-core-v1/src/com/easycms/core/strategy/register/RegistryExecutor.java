package com.easycms.core.strategy.register;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindingResult;


/**
 * 认证器
 * @author Administrator
 *
 */
public class RegistryExecutor<T>{
	
	private RegisterStrategy<T> registerStrategy;
	
	public RegistryExecutor(RegisterStrategy<T> registerStrategy){
		this.registerStrategy = registerStrategy;
	}
	
	


	public void setRegisterStrategy(RegisterStrategy<T> registerStrategy) {
		this.registerStrategy = registerStrategy;
	}




	/**
	 * 认证器执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public void execute(T form,BindingResult errors,HttpServletRequest request, HttpServletResponse response){
		this.registerStrategy.execute(form,errors,request,response);
	}
}
