package com.easycms.core.strategy.authorization;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.SortEnum;
import com.easycms.core.util.ContextUtil;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.settings.domain.Website;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.action.ManagementAuthorizationController;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.MenuService;

/**
 * mysql的认证方式
 * @author Administrator
 *
 */
public class DefaultAuth implements Authorization<User> {
	private static final Logger logger = Logger
			.getLogger(DefaultAuth.class);

	private static final String disturb = "easycms.com";
	@Autowired
	private UserDao userDao;
	@Autowired
	private WebsiteService websiteService;
	@Autowired
	private MenuService menuService;
	@Autowired
	private VariableSetService variableSetService;

	@Override
	public User executeAuth(String username, String password, boolean isSession) {
		password = CommonUtility.MD5Digest(disturb, password);

		User user = userDao.findByNameAndPassword(username, password);
		if(ContextPath.WZ){
			logger.debug("王志本机测试，DefaultAuth不验证密码！！！");
			user = userDao.findByName(username);
		}
		if (user != null) {
			// user = userDao.getById(user.getId());
			// 默认将本地站点设置为当前站点
			Website website = websiteService.findLocalSite();
			ContextUtil.setCurrentWebsite(website);

			HttpServletRequest request = ContextUtil.getRequest();

			// 登录成功更新用户信息
			Date currentLoginTime = user.getCurrentLoginTime();
			user.setLastLoginTime(currentLoginTime);
			user.setCurrentLoginTime(new Date());
			String currentLoginIp = user.getCurrentLoginIp();
			user.setLastLoginIp(currentLoginIp);
			user.setCurrentLoginIp(request.getRemoteAddr());
			Integer loginTimes = user.getLoginTimes();
			loginTimes = loginTimes == null ? 0 : loginTimes;
			user.setLoginTimes(++loginTimes);
			userDao.save(user);

			//如果是默认管理员登录，则拥有所有菜单
			List<Menu> menus = new ArrayList<Menu>();
			if(user.isDefaultAdmin()) {
				menus = menuService.getTree(SortEnum.desc);
			}else{
				//将重复的权限去掉
				List<Role> roles = user.getRoles();
				
				if (null != roles) {
					Set<String> roleType = new HashSet<String>();
					
					for (Role r : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(r.getName(), "roleType");
						roleType.addAll(roleNameList);
					}
					user.setRoleType(roleType);
				}
				
				menus = menuService.getTree(roles,SortEnum.desc);
			}
			user.setMenus(menus);
			if (isSession) {
				HttpSession session = request.getSession();
				session.setAttribute("user", user);
				session.setAttribute("userId", "" + user.getId());
			}
		}
		return user;
	}

}
