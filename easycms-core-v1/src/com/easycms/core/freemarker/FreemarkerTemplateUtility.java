package com.easycms.core.freemarker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;

import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

/**
 * 模板工具类
 * @author Administrator
 *
 */
@Component
public class FreemarkerTemplateUtility extends ApplicationObjectSupport {
	private static ApplicationContext context;
	private static FreeMarkerConfigurer freemarkerConfig;
	private static Configuration conf;

	public static boolean createHtml(String templateName, String htmlName,
			Map<String, Object> rootMap) {
		String path = "E:\\" + htmlName;
		Writer out = null;
		boolean result = true;
		try {
			Template t = conf.getTemplate(templateName, "utf-8");
			out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(new File(path))));
			t.process(rootMap, out);
			out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}

	public static boolean write2Client(String templateName,
			HttpServletResponse response, Map<String, Object> rootMap) {
		return respone2client(templateName, response, rootMap, "UTF-8");
	}

	/**
	 * @param templateName
	 * @param response
	 * @param rootMap
	 * @param code
	 * @return
	 */
	public static boolean write2Client(String templateName,
			HttpServletResponse response, Map<String, Object> rootMap,
			String code) {
		return respone2client(templateName, response, rootMap, code);
	}

	private static boolean respone2client(String templateName,
			HttpServletResponse response, Map<String, Object> rootMap,
			String code) {
		Writer out = null;
		boolean result = true;
		try {
			Template t = conf.getTemplate(templateName, "utf-8");
			response.setCharacterEncoding(code);
			response.setContentType("text/html;charset=" + code);
			out = response.getWriter();
			t.process(rootMap, out);
			out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}

	public static String getTemplateAsString(String templateName,
			Map<String, Object> rootMap) {
		Writer writer = null;
		String result = null;
		try {
			Template t = conf.getTemplate(templateName, "utf-8");
			writer = new StringWriter();
			t.process(rootMap, writer);
			result = writer.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 从MAP中获取String类参数
	 * 
	 * @param params
	 * @param key
	 * @return
	 */
	public static String getStringValueFromParams(@SuppressWarnings("rawtypes") Map params, String key) {
		String value = null;
		try {
			if (params != null && CommonUtility.isNonEmpty(key)) {
				Object valueObj = params.get(key);
				if (valueObj != null) {
					if (valueObj instanceof TemplateScalarModel) {
						TemplateScalarModel valueModel = (TemplateScalarModel) valueObj;
						value = valueModel.getAsString();

					} else if(valueObj instanceof String){
						return (String) valueObj;
					} else {
						throw new TemplateModelException(key
								+ " must be string!");
					}
				}
			}
		} catch (TemplateModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}

	public static Integer getIntValueFromParams(@SuppressWarnings("rawtypes") Map params, String key) {
		Integer value = null;
		try {
			if (params != null && CommonUtility.isNonEmpty(key)) {
				Object valueObj = params.get(key);
				if (valueObj != null) {
					if (valueObj instanceof TemplateScalarModel) {
						TemplateScalarModel valueModel = (TemplateScalarModel) valueObj;
						String valueStr = valueModel.getAsString();
						if(CommonUtility.isNumber(valueStr)){
							value = Integer.parseInt(valueStr);
						}else{
							value = null;
						}
					} else if (valueObj instanceof TemplateNumberModel) {
						value = ((TemplateNumberModel) valueObj).getAsNumber()
								.intValue();
					} else {
						throw new TemplateModelException(key
								+ " must be a string or number!");
					}
				}
			}
		} catch (TemplateModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}

	@Override
	protected void initApplicationContext(ApplicationContext c)
			throws BeansException {
		// TODO Auto-generated method stubthi
		try {
			context = c;
			freemarkerConfig = (FreeMarkerConfigurer) context
					.getBean("freemarkerConfig");
			conf = freemarkerConfig.createConfiguration();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.initApplicationContext(c);
	}
	
	/**
	 * 设置分页变量到输出环境
	 * @param env
	 * @param page
	 * @param DEFAULT_WRAPER
	 * @throws TemplateModelException
	 */
	@Deprecated
	public static <T> void pageVariable(Environment env, Page<T> page,ObjectWrapper DEFAULT_WRAPER)
			throws TemplateModelException {
		env.setVariable("count", DEFAULT_WRAPER.wrap(page.getTotalCounts())); // 总记录数
		env.setVariable("pagesize", DEFAULT_WRAPER.wrap(page.getPagesize())); // 页面大小
		env.setVariable("start", DEFAULT_WRAPER.wrap(page.getStartPage())); // 开始页面
		env.setVariable("end", DEFAULT_WRAPER.wrap(page.getEndPage())); // 结束页面
		env.setVariable("current", DEFAULT_WRAPER.wrap(page.getCurrentPage())); // 当前页面
		env.setVariable("pagecount", DEFAULT_WRAPER.wrap(page.getPageCount())); // 页面总数
	}
}
