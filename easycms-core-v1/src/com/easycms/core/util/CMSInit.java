package com.easycms.core.util;

import java.util.List;

public class CMSInit {
	
	public static List<String> LOGIN_UNCHECK_LIST = null;
	public static List<String> ALWAYS_UNCHECK_LIST = null;
	public static List<String> RETURN_URI_LIST = null;
	public static String NO_PERMISSION_URI = null;
	public static String BACKEND_LOGIN_URI = null;
	public static String FRONTEND_LOGIN_URI = null;
	public static String BACKEND_CHECK_PREFIX = null;
	public static String FRONTEND_CHECK_PREFIX = null;
	
}
