package com.easycms.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.easycms.common.util.XMLHelper;
import com.easycms.management.user.domain.Privilege;

@SuppressWarnings("unchecked")
public class PrivilegeUtil {
	
	private static final String NAME_NODE = "name";
	private static final String URL_NODE = "url";
	private static final String PRIVILEGE_NODE = "privilege";
	
//	public static void main(String[] args) {
//		InputStream in = null;
//		try {
//			in = PrivilegeUtil.class.getClassLoader().getResourceAsStream("cms-privileges.xml");
//			XMLHelper xmlHelper = new XMLHelper(in);
//			Element rootElement = xmlHelper.getRootElement();
//			List<Element> privilegesElements = rootElement.elements(PRIVILEGE_NODE);
//			List<Privilege> list = new ArrayList<Privilege>();
//			if(privilegesElements != null) {
//				for(Element e : privilegesElements) {
//					list = injectAll(e,list);
//				}
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}finally{
//			try {
//				in.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
////		System.out.println(WebUtility.toJson(list));
////		System.out.println("name:" + p.getPrivilegeName());
////		System.out.println("url:" + p.getUri());
//	}
	
	/**
	 * 权限写入配置文件
	 * @param path	配置文件绝对路径
	 * @param privileges	具有层级关系的权限列表
	 * @return
	 */
	public static boolean writeToConf(String from,List<Privilege> privileges) {
		try {
			File fromFile = new File(from);
			if(!fromFile.exists()) {
				fromFile.mkdirs();
			}
			InputStream in = new FileInputStream(fromFile);
			XMLHelper xmlHelper = new XMLHelper(in);
			Element root = xmlHelper.getRootElement();
			root.clearContent();
			injectToConf(root,privileges);
			
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setEncoding("urf-8");
			XMLWriter writer = new XMLWriter(new FileWriter(fromFile),format);
			writer.write(root);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	private static void injectToConf(Element e,List<Privilege> privileges) {
		if(e==null 
				||privileges == null 
				|| privileges.size() == 0) {
			return;
		}
		
		for(Privilege p: privileges) {
			Element parent = e.addElement("privilege");
			
			String name = p.getName() == null?"":p.getName();
			String url = p.getUri() == null?"":p.getUri();
			
			parent.addElement("name").addText(name);
			parent.addElement("url").addText(url);
		}
	}
	
	
	public static List<Privilege> getPrivilegesFromConf(String path) {
		List<Privilege> list = null;
		try {
			InputStream in = new FileInputStream(new File(path)) ;
			list = parseInputStream(in);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	
	public static List<Privilege> getPrivilegesFromConf(InputStream in) {
		List<Privilege> list = null;
		try {
			list = parseInputStream(in);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	private static List<Privilege> parseInputStream(InputStream in)
			throws IOException {
		List<Privilege> list;
		XMLHelper xmlHelper = new XMLHelper(in);
		Element rootElement = xmlHelper.getRootElement();
		List<Element> privilegesElements = rootElement.elements(PRIVILEGE_NODE);
		list = new ArrayList<Privilege>();
		if(privilegesElements != null) {
			for(Element e : privilegesElements) {
				Privilege p = new Privilege();
				Element nameElement = e.element(NAME_NODE);
				if(nameElement !=null && nameElement.hasContent()) {
					String nameValue = nameElement.getText();
					p.setName(nameValue);
//						System.out.println("name==>" + nameValue);
				}
				Element urlElement = e.element(URL_NODE);
				if(urlElement !=null && urlElement.hasContent()) {
					String urlValue = urlElement.getText();
					p.setUri(urlValue);
//						System.out.println("url==>" + urlValue);
				}
				
				p.setCreateTime(new Date());
				list.add(p);
			}
		}
		in.close();
		return list;
	}
	
	
//	public static List<Privilege> injectAll(Element root,List<Privilege> list,Privilege parent) {
//		if (list == null) list = new ArrayList<Privilege>();
//		Privilege p = new Privilege();
//		Element nameElement = root.element(NAME_NODE);
//		if(nameElement !=null && nameElement.hasContent()) {
//			String nameValue = nameElement.getText();
//			p.setName(nameValue);
////			System.out.println("name==>" + nameValue);
//		}
//		Element urlElement = root.element(URL_NODE);
//		if(urlElement !=null && urlElement.hasContent()) {
//			String urlValue = urlElement.getText();
//			p.setUri(urlValue);
////			System.out.println("url==>" + urlValue);
//		}
//		list.add(p);
//		return list;
//	}
}
