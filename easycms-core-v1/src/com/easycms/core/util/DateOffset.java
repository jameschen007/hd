package com.easycms.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;

public class DateOffset {
	
	public static String convert(Map params, Object dateModel) {

		Integer apptimezoneoffset = FreemarkerTemplateUtility.getIntValueFromParams(params,
				SysConstant.apptimezoneoffset);

		SimpleDateFormat sdf_default = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + apptimezoneoffset));

		String snow = "";
		try {
			snow = sdf.format(sdf_default.parse(String.valueOf(dateModel)));
			return snow;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return snow;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
