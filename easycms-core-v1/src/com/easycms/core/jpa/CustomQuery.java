package com.easycms.core.jpa;

import javax.persistence.criteria.Predicate;

public interface CustomQuery {
	
	void execute(Predicate pre);
}
