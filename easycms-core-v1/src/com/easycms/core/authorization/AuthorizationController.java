package com.easycms.core.authorization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.LoginForm;
import com.easycms.management.user.form.validator.LoginFormValidator;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/authorization")
public class AuthorizationController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(AuthorizationController.class);
	private static final String AJAX_REQUEST = "XMLHttpRequest";

	@Autowired
	private UserService userService;

	/**
	 * 显示管理员登录页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String view(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String returnString = ForwardUtility.forwardAdminView("login");
		
		String requestType = request.getHeader("X-Requested-With");
		logger.debug("[requestType] = " + requestType);
		if(AJAX_REQUEST.equals(requestType)) {
			returnString = ForwardUtility.forwardAdminView("/login_ajax");
		}
		return returnString;
	}

	@RequestMapping(value="/login",method=RequestMethod.POST)
	public void login(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("loginForm") @Valid LoginForm loginForm,
			BindingResult errors) {
//		Properties p = CommonUtility.getPropertyFile("errors.properties");
		// 校验表单
		LoginFormValidator validator = new LoginFormValidator();
		validator.setRequest(request);
		validator.validate(loginForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return ;
		}
		
		//用户登录
		logger.info("==> User is logining");
		
		String username = loginForm.getUsername();
		logger.info("[username] = " + username);

		String pwd = loginForm.getPassword();
		logger.info("[pwd] = " + pwd);

		User user = userService.userLogin(username,pwd, true);
		if (user == null) {
			errors.rejectValue("username", "form.user.notExist");

			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return ;
		}
		
		logger.info("==> User [" + username + "] login success.");
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
	}
	
	/**
	 * 用户退出登录
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request,
			HttpServletResponse response) {

		User user = (User) request.getSession().getAttribute("user");
		String username = user == null?"":user.getName();
		logger.info("==> user [" + username + "] logout");
		request.getSession().removeAttribute("user");
		String returnString = ForwardUtility.redirectWithSpringMVC("/");
		return returnString;
	}
}
