package com.easycms.core.authorization;


import com.easycms.management.user.domain.User;
/**
 * 认证器
 * @author Administrator
 *
 */
public class Authenticator{
	
	private Authorization auth;
	
	public Authenticator(Authorization auth){
		this.auth = auth;
	}
	
	/**
	 * 认证器执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public User executeAuth(String username,String password){
		return this.auth.executeAuth(username, password);
	}
}
