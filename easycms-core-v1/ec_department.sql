/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : easycms_v10

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-22 10:32:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ec_department`
-- ----------------------------
DROP TABLE IF EXISTS `ec_department`;
CREATE TABLE `ec_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `department_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '部门名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父级部门ID',
  `top_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_fk` (`parent_id`),
  KEY `fk_role` (`top_role_id`),
  CONSTRAINT `ec_department_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `ec_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role` FOREIGN KEY (`top_role_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_department
-- ----------------------------
INSERT INTO `ec_department` VALUES ('81', '经理部', null, '130');
INSERT INTO `ec_department` VALUES ('82', '工程部', '81', '132');
INSERT INTO `ec_department` VALUES ('83', '技术部', '81', '133');
INSERT INTO `ec_department` VALUES ('84', '管道队', '81', '136');
INSERT INTO `ec_department` VALUES ('85', '技术管理室', '83', '134');
INSERT INTO `ec_department` VALUES ('86', '施工管理室', '84', '136');
INSERT INTO `ec_department` VALUES ('89', '质检部', '81', '145');
INSERT INTO `ec_department` VALUES ('91', 'A见证组', '89', '141');
INSERT INTO `ec_department` VALUES ('92', 'B见证组', '89', '141');

-- ----------------------------
-- Table structure for `ec_role`
-- ----------------------------
DROP TABLE IF EXISTS `ec_role`;
CREATE TABLE `ec_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名',
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ec_parent_fk` (`parent_id`),
  CONSTRAINT `ec_parent_fk` FOREIGN KEY (`parent_id`) REFERENCES `ec_role` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_role
-- ----------------------------
INSERT INTO `ec_role` VALUES ('130', '总经理', null, '1');
INSERT INTO `ec_role` VALUES ('131', '技术经理', '130', '2');
INSERT INTO `ec_role` VALUES ('132', '工程部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('133', '技术部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('134', '管理室主任', '133', '4');
INSERT INTO `ec_role` VALUES ('135', '工程师', '134', '5');
INSERT INTO `ec_role` VALUES ('136', '队长', '131', '3');
INSERT INTO `ec_role` VALUES ('137', '班长', '136', '4');
INSERT INTO `ec_role` VALUES ('138', '组长', '137', '5');
INSERT INTO `ec_role` VALUES ('139', '计划部经理', '131', '3');
INSERT INTO `ec_role` VALUES ('140', '计划员', '139', '4');
INSERT INTO `ec_role` VALUES ('141', '见证组组长', '145', '4');
INSERT INTO `ec_role` VALUES ('142', 'W级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('143', 'H级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('144', 'R级见证员', '141', '4');
INSERT INTO `ec_role` VALUES ('145', '质检部部长', '131', '3');

-- ----------------------------
-- Table structure for `ec_site`
-- ----------------------------
DROP TABLE IF EXISTS `ec_site`;
CREATE TABLE `ec_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站名称',
  `site_domain` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '域名',
  `site_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键字',
  `site_describe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站描述',
  `site_tpl_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板名称',
  `upload_max_size` int(11) DEFAULT '0' COMMENT '上传文件大小',
  `upload_tpl_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板上传目录',
  `tpl_file_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '模板上传类型',
  `upload_res_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源上传目录',
  `is_local_site` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否本地站点',
  `app_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '站点绝对路径',
  `i18n_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '国际化目录',
  `pagesize_list` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pagesize` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_site
-- ----------------------------
INSERT INTO `ec_site` VALUES ('9', '核岛安装实时动态监管系统', 'asdas', 'asda', 'sdsad', 'easycms', null, null, null, null, '1', '/', null, '[20,50,100]', '20');

-- ----------------------------
-- Table structure for `ec_user`
-- ----------------------------
DROP TABLE IF EXISTS `ec_user`;
CREATE TABLE `ec_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '默认ID',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名唯一',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `real_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '真是姓名',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `current_login_time` datetime DEFAULT NULL COMMENT '当前登录时间',
  `current_login_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT '127.0.0.1' COMMENT '当前登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT '127.0.0.1' COMMENT '上次登录IP',
  `login_times` int(11) DEFAULT NULL COMMENT '登录次数',
  `is_default_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认管理员',
  `is_del` tinyint(1) DEFAULT '0',
  `device` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(100) DEFAULT NULL,
  `department_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_parent` (`parent_id`),
  KEY `fk_department` (`department_id`),
  CONSTRAINT `fk_department` FOREIGN KEY (`department_id`) REFERENCES `ec_department` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_parent` FOREIGN KEY (`parent_id`) REFERENCES `ec_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_user
-- ----------------------------
INSERT INTO `ec_user` VALUES ('8', 'admin', '594afe5e1f8ac83827ca43434f7dae1a', '邓洁芃', '', '2014-01-01 18:20:59', '127.0.0.1', '2015-07-22 09:19:32', '192.168.2.153', '2015-07-22 09:18:55', '192.168.2.153', '1453', '1', '0', 'zzzzzz', null, null);
INSERT INTO `ec_user` VALUES ('89', 'zhangxiaolin', '594afe5e1f8ac83827ca43434f7dae1a', '张孝林', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-14 22:41:42', '192.168.2.119', '2015-06-14 21:40:46', '192.168.2.119', '76', '0', '0', null, null, '86');
INSERT INTO `ec_user` VALUES ('91', 'liheping', '594afe5e1f8ac83827ca43434f7dae1a', '李和平', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-14 21:40:08', '192.168.2.119', '2015-06-04 22:23:48', '192.168.2.119', '24', '0', '0', null, null, '86');
INSERT INTO `ec_user` VALUES ('92', 'maoguoqiang', 'a5100adcf95faed61d4e4ca1721399b0', '毛国强', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-05-29 15:56:41', '192.168.2.119', null, '127.0.0.1', '1', '0', '0', null, '89', '86');
INSERT INTO `ec_user` VALUES ('93', 'yezhu', 'a5100adcf95faed61d4e4ca1721399b0', '业主', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '1', null, null, null);
INSERT INTO `ec_user` VALUES ('94', 'gongchenggongsi', 'a5100adcf95faed61d4e4ca1721399b0', '工程公司', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-07 16:28:14', '192.168.2.119', '2015-06-07 16:06:05', '192.168.2.119', '186', '0', '1', null, null, null);
INSERT INTO `ec_user` VALUES ('95', 'jianli', 'a5100adcf95faed61d4e4ca1721399b0', '监理', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '91');
INSERT INTO `ec_user` VALUES ('101', 'gaobo', 'a5100adcf95faed61d4e4ca1721399b0', '高博', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, '92');
INSERT INTO `ec_user` VALUES ('106', 'maosan', 'a5100adcf95faed61d4e4ca1721399b0', '毛三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('107', 'maoer', 'a5100adcf95faed61d4e4ca1721399b0', '毛二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('108', 'maoyi', 'a5100adcf95faed61d4e4ca1721399b0', '毛一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('109', 'lisan', 'a5100adcf95faed61d4e4ca1721399b0', '李三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('110', 'lier', 'a5100adcf95faed61d4e4ca1721399b0', '李二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('111', 'liyi', 'a5100adcf95faed61d4e4ca1721399b0', '李一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('112', 'zhangsan', 'a5100adcf95faed61d4e4ca1721399b0', '张三', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('113', 'zhanger', 'a5100adcf95faed61d4e4ca1721399b0', '张二', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('114', 'zhangyi', 'a5100adcf95faed61d4e4ca1721399b0', '张一', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('116', 'zhoujuan', 'a5100adcf95faed61d4e4ca1721399b0', '周娟', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', null, '127.0.0.1', null, '127.0.0.1', null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('118', 'yangkaifeng', '594afe5e1f8ac83827ca43434f7dae1a', '杨凯峰', 'example@qq.com', '2015-04-21 00:00:00', '127.0.0.1', '2015-06-14 17:33:01', '192.168.2.119', '2015-06-11 20:33:23', '192.168.2.119', '11', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('131', 'banzhang', '594afe5e1f8ac83827ca43434f7dae1a', '班长', '', '2015-06-07 16:31:04', '192.168.2.119', '2015-06-14 21:06:11', '192.168.2.119', '2015-06-14 20:31:07', '192.168.2.119', '51', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('132', 'zuzhang', '594afe5e1f8ac83827ca43434f7dae1a', '施工组长', '', '2015-06-07 16:33:59', '192.168.2.119', '2015-06-14 22:40:00', '192.168.2.119', '2015-06-14 22:38:47', '192.168.2.119', '80', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('136', 'jhy', '594afe5e1f8ac83827ca43434f7dae1a', '计划员', 'jhy@hd.com', '2015-06-07 20:02:30', '192.168.2.119', '2015-06-14 19:34:24', '192.168.2.119', '2015-06-14 17:31:36', '192.168.2.119', '32', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('138', 'lindong1', '594afe5e1f8ac83827ca43434f7dae1a', '林动', '', '2015-06-09 17:53:27', '192.168.2.119', '2015-06-14 21:59:40', '192.168.2.119', '2015-06-14 21:44:46', '192.168.2.119', '17', '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('139', 'qc1', '594afe5e1f8ac83827ca43434f7dae1a', 'qc1见证人', '', '2015-06-11 21:27:51', '192.168.2.119', null, null, null, null, null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('140', 'qc2', '594afe5e1f8ac83827ca43434f7dae1a', 'qc2见证人', '', '2015-06-11 21:28:07', '192.168.2.119', null, null, null, null, null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('141', 'qca', '594afe5e1f8ac83827ca43434f7dae1a', 'qca见证人', '', '2015-06-11 21:28:27', '192.168.2.119', null, null, null, null, null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('142', 'rjzr', '594afe5e1f8ac83827ca43434f7dae1a', 'R点见证人', '', '2015-06-11 21:30:24', '192.168.2.119', null, null, null, null, null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('143', 'wjzr', '594afe5e1f8ac83827ca43434f7dae1a', 'W点见证人', 'admin@hfda.co', '2015-06-11 21:58:13', '192.168.2.119', null, null, null, null, null, '0', '0', null, null, null);
INSERT INTO `ec_user` VALUES ('144', '洁芃', '594afe5e1f8ac83827ca43434f7dae1a', '洁芃', 'jiepeng@globalroam.com', '2015-06-17 09:50:51', '192.168.2.153', '2015-06-17 09:53:17', '192.168.2.153', '2015-06-17 09:51:50', '192.168.2.153', '2', '0', '1', null, null, null);
