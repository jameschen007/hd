//package com.easycms.oracle.hd.plan.service;
//
//import java.util.List;
//
//import com.easycms.core.util.Page;
//import com.easycms.hd.plan.domain.WorkStep;
//
//public interface WorkStepOracleService {
//	int getCount(WorkStep workStep);
//	WorkStep getById(Integer id);
//	List<WorkStep> getByRollingPlanDrawnoAndWeldno(String drawno, String weldno);
//	Page<WorkStep> findByPage(WorkStep workStep, Page<WorkStep> page);
//}
