//package com.easycms.oracle.hd.plan.service;
//
//import java.util.List;
//
//import com.easycms.common.util.Messenger;
//import com.easycms.core.util.Page;
//import com.easycms.hd.plan.domain.RollingPlan;
//
//public interface RollingPlanOracleService {
//	int getCount(RollingPlan rollingPlan);
//	RollingPlan getById(Integer id);
//	RollingPlan getByDrawnoAndWeldno(String drawno, String weldno);
//	Page<RollingPlan> findByPage(RollingPlan rollingPlan, Page<RollingPlan> page);
//	Messenger importToLocal(String[] sIds);
//	List<RollingPlan> addWorkStep(List<RollingPlan> rollingPlanList);
//	RollingPlan addWorkStep(RollingPlan rollingPlan);
//}
