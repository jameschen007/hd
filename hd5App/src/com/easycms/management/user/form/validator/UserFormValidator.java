package com.easycms.management.user.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.common.util.CommonUtility;
import com.easycms.management.user.form.UserForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class UserFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(UserForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
		"form.username.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "realname",
		 "form.realname.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
		 "form.password.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword",
		 "form.confirmPassword.empty");
		 
		 UserForm form = (UserForm) target;
		 String email = form.getEmail();
		 if(CommonUtility.isNonEmpty(email)) {
			 if(!CommonUtility.isEmail(email)) {
				 errors.rejectValue("email", "form.email.error");
			 }
		 }
	}
}
