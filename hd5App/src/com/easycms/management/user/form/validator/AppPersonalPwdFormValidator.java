package com.easycms.management.user.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.request.ResetPasswordRequestForm;
import com.easycms.management.user.form.PersonalForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class AppPersonalPwdFormValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ResetPasswordRequestForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
		 "form.oldPassword.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword",
		 "form.newPassword.empty");
		 
	}
}
