package com.easycms.management.user.form.validator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.form.LoginForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class LoginFormValidator implements Validator {
	private HttpServletRequest request;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(LoginForm.class);
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	@Override
	public void validate(Object target, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username",
		 "form.username.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
		 "form.password.empty");
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "verifyCode",
		 "form.verifyCode.empty");
		 
		 if(!errors.hasErrors()) {
			 LoginForm form = (LoginForm) target;
			 String verifyCode = form.getVerifyCode();
			 if(CommonUtility.isNonEmpty(verifyCode)) {
				 String serverCode = HttpUtility.getVerifyCodeByCreated(request);
				 if(!verifyCode.equalsIgnoreCase(serverCode)) {
					 errors.rejectValue("verifyCode", "form.verifyCode.error");
				 }
			 }
			 
		 }
	}
}
