package com.easycms.management.user.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.easycms.common.util.CommonUtility;
import com.easycms.management.user.form.PersonalForm;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-22
 * 
 */
@Component
public class PersonalInfoFormValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PersonalForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "realname",
		 "form.realname.empty");
		 PersonalForm form = (PersonalForm) target;
		 String email = form.getEmail();
		 if(CommonUtility.isNonEmpty(email)) {
			 if(!CommonUtility.isEmail(email)) {
				 errors.rejectValue("email", "form.email.error");
			 }
		 }
	}
}
