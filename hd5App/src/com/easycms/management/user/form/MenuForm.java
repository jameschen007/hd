package com.easycms.management.user.form;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;

public class MenuForm extends BasicForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String name = "";
	private String iconPath = "";
	private String style = "";
	private String parentId = "";
	private Integer seq = 0;
	private Integer privilegeId;



	public Integer getSeq() {
		return seq == null?0:seq;
	}



	public void setSeq(Integer seq) {
		this.seq = seq;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getIconPath() {
		return iconPath;
	}



	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}



	public String getStyle() {
		return style;
	}



	public void setStyle(String style) {
		this.style = style;
	}



	public String getParentId() {
		return parentId;
	}



	public void setParentId(String parentId) {
		this.parentId = parentId;
	}






	public Integer getPrivilegeId() {
		return privilegeId;
	}



	public void setPrivilegeId(Integer privilegeId) {
		this.privilegeId = privilegeId;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public boolean isValidExist() {
		if ("exist".equals(super.getFunc())) {
			return true;
		} else {
			return false;
		}
	}
}
