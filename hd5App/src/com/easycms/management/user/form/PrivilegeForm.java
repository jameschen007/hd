package com.easycms.management.user.form;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;

public class PrivilegeForm extends BasicForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name = "";
	private String uri = "";


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public boolean isValidExist() {
		if ("exist".equals(super.getFunc())) {
			return true;
		} else {
			return false;
		}
	}
}
