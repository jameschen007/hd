
package com.easycms.management.user.form;


/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class LoginForm {
	private String username = "";
	private String password = "";
	private String verifyCode="";
	private String loginType;
	public String getUsername() {
		return username.trim();
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password.trim();
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerifyCode() {
		return verifyCode.trim();
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	
}
