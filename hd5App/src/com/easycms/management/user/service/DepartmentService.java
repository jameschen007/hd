package com.easycms.management.user.service;

import java.util.List;

import com.easycms.core.enums.SortEnum;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;

public interface DepartmentService{

	Department add(Department d);
	
	Department update(Department d);
	
	/**
	 * 删除
	 * @param id
	 */
	boolean delete(Integer id);
	
	/**
	 * 删除所有
	 */
	boolean deleteAll();
	
	/**
	 * 删除所以
	 * @param ids
	 */
	boolean deleteAll(Integer[] ids);
	
	Department findById(Integer id);
	
	Department findByName(String name);
	
	/**
	 * 查找所有
	 * @return
	 */
	List<Department> findAll();
	
	List<Department> findAll(Department condition);
	
	/**
	 * 得到最顶层的父级权限
	 * @return
	 */
	List<Department> findTop();
	
	/**
	 * 得到子级菜单
	 * @param parentId
	 * @return
	 */
	List<Department> findChildren(Integer parentId);
	public Page<Department> findPage(Department condition,Page<Department> page)throws Exception;
	
	public Page<Department> findPage(Page<Department> page)throws Exception;

	/**
	 * 根据enpowerid获取部门
	 * @param id
	 * @return
	 */
	Department findByEnpowerId(String enpowerId);

	List<Department> getTree(SortEnum sort);
	

	/**
	 * 获得责任部门
	 * @return
	 */
	public List<Department> responsibilityDept();
	/**
	 * 是否责任部门人员
	 * @param sort
	 * @return
	 */
	public boolean isFromDept(User user,List<Department> depts);
}
