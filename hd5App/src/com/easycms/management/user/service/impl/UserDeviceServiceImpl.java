package com.easycms.management.user.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.management.user.dao.UserDeviceDao;
import com.easycms.management.user.domain.UserDevice;
import com.easycms.management.user.service.UserDeviceService;

@Service("userDeviceService")
public class UserDeviceServiceImpl implements UserDeviceService {
	
	@Autowired
	private UserDeviceDao userDeviceDao;

	@Override
	public UserDevice add(UserDevice userDevice) {
		return userDeviceDao.save(userDevice);
	}

	@Override
	public UserDevice update(UserDevice userDevice) {
		return userDeviceDao.save(userDevice);
	}

	@Override
	public void delete(Integer id) {
		userDeviceDao.deleteById(id);
	}

	@Override
	public void delete(Integer[] ids) {
		userDeviceDao.deleteByIds(ids);
	}

	@Override
	public UserDevice findUserDevice(Integer id) {
		return userDeviceDao.findById(id);
	}

	@Override
	public Page<UserDevice> findPage(UserDevice condition,Page<UserDevice> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<UserDevice> springPage = userDeviceDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<UserDevice> findPage(Page<UserDevice> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<UserDevice> springPage = userDeviceDao.findAll(QueryUtil.queryConditions(new UserDevice()),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<UserDevice> findAll(UserDevice condition) {
		return userDeviceDao.findAll(queryConditions(condition));
	}
	
	@Override
	public List<UserDevice> findAll() {
		return userDeviceDao.findAll(queryConditions(null));
	}

	@Override
	public Integer count() {
		return userDeviceDao.count();
	}

	private Specification<UserDevice> queryConditions(final UserDevice userDevice) {
		return new Specification<UserDevice> () { 
			   public Predicate toPredicate(Root<UserDevice> root,  
			     CriteriaQuery<?> query, CriteriaBuilder cb) { 
				Path<String> idPath = root.get("id");  
			    Path<String> namePath = root.get("name");  
			    
			    Predicate pre = null;
			    
			    if(userDevice != null) {
			    	String sort = userDevice.getSort();	//排序条件  
			    	
			    	if(CommonUtility.isNonEmpty(userDevice.getDeviceid())){
			    		pre =cb.like(namePath,"%"+userDevice.getDeviceid()+"%");  
			    	}

			    	if(null != userDevice.getId()){
				    	pre=cb.and(pre,cb.equal(idPath,userDevice.getId()));
				    }
			    	//升降序
			    	//排序
			    	Path<String> sortPath = null;
			    	if(CommonUtility.isNonEmpty(sort)){
			    		sortPath = root.get(sort);
			    	}else{
			    		sortPath = idPath;
			    	}
			    	
			    	Order orderBy = null;
			    	if(userDevice.isOrderDesc()){
			    		orderBy = cb.desc(sortPath);
			    	}else{
			    		orderBy = cb.asc(sortPath);
			    	}
			    	
			    	query.orderBy(orderBy);
			    }
			    
			    if(pre != null) {
			    	query.where(pre);
			    }
			    
			    return null;  
			   }  
		};
	}

	@Override
	public UserDevice getDeviceByDeviceIdAndUserId(Integer userID, String deviceId) {
		
		return userDeviceDao.getDeviceByDeviceIdAndUserId(userID, deviceId);
	}

	@Override
	public List<UserDevice> getDeviceByUserId(Integer userId) {
		return userDeviceDao.getDeviceByUserId(userId);
	}
	

}
