package com.easycms.management.user.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.enums.SortEnum;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.management.user.dao.DepartmentDao;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;

@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {
	@Autowired
	private DepartmentDao departmentDao;
	@Autowired
	private VariableSetDao variableSetDao;
	@Override
	public Department add(Department d) {
		// TODO Auto-generated method stub
		return departmentDao.save(d);
	}
	@Override
	public Department update(Department d) {
		// TODO Auto-generated method stub
		return departmentDao.save(d);
	}
	@Override
	public boolean delete(Integer id) {
		// TODO Auto-generated method stub
		return departmentDao.deleteById(id) > 0;
	}
	@Override
	public boolean deleteAll() {
		// TODO Auto-generated method stub
		return departmentDao.deleteAll() > 0;
	}
	@Override
	public boolean deleteAll(Integer[] ids) {
		return departmentDao.deleteByIds(ids) > 0;
	}
	@Override
	public Department findById(Integer id) {
		// TODO Auto-generated method stub
		return departmentDao.findById(id);
	}
	@Override
	public List<Department> findAll() {
		// TODO Auto-generated method stub
		return departmentDao.findAll();
	}
	
	@Override
	public List<Department> findAll(Department condition) {
		
		return departmentDao.findAll(QueryUtil.queryConditions(condition));
	}
	@Override
	public List<Department> findTop() {
		// TODO Auto-generated method stub
		return departmentDao.findTop();
	}
	@Override
	public List<Department> findChildren(Integer parentId) {
		// TODO Auto-generated method stub
		return departmentDao.findChildren(parentId);
	}
	@Override
	public Department findByName(String name) {
		// TODO Auto-generated method stub
		return departmentDao.findByName(name);
	}
	@Override
	public Page<Department> findPage(Department condition, Page<Department> page)
			throws Exception {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<Department> springPage = departmentDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<Department> findPage(Page<Department> page) throws Exception {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<Department> springPage = departmentDao.findAll(QueryUtil.queryConditions(new Department()),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Department findByEnpowerId(String enpowerId) {
		return departmentDao.findByEnpowerId(enpowerId);
	}
	
	/**
	 * 获得责任部门
	 * @return
	 */
	public List<Department> responsibilityDept(){

		String variableType = "departmentId";
		String variableKey = "departmentRoot";//K2/K3核电项目部
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);
		if(set==null || set.size()>1){
			throw new RuntimeException("配置数据异常，应该仅有一个部门数据可以查询出来！！"+variableKey+" "+variableType+" "+set);
		}
		Integer deptId = Integer.valueOf(set.iterator().next());
		return departmentDao.findChildren(deptId);
	}
	
	/**
	 * 是否责任部门人员
	 * @param sort
	 * @return
	 */
	public boolean isFromDept(User user,List<Department> depts){
		boolean f = false ;
		
		if(depts.isEmpty() == false ){
			f = depts.stream().anyMatch(e->
				user.getDepartment().getId().intValue()== e.getId().intValue()
			);
		}
		
		return f;
	}

	@Override
	public List<Department> getTree(SortEnum sort){

		String variableType = "departmentId";
		String variableKey = "departmentRoot";//K2/K3核电项目部
		Set<String> set = variableSetDao.findSetValueBySetKeyAndType(variableKey, variableType);
		
		List<Department> departments = new ArrayList<Department>();
		
		List<Department> elements = departmentDao.findAll();
		List<Department> roots = new ArrayList<Department>();
		for(Department m : elements) {
//			if(m.getParent() == null) {
//				roots.add(m);
//			}
			for (String val : set) {//只取
				if(Integer.valueOf(val).intValue()==m.getId()){
					roots.add(m);
				}
			}
		}
		
		switch (sort) {
		case asc:
			Collections.sort(roots);
			break;
		case desc:
			Collections.sort(roots);
			Collections.reverse(roots);
			break;
		default:
			Collections.sort(roots);
			Collections.reverse(roots);
			break;
		}
		getDepartmentTree(roots,elements,sort);
		return roots;
	}
	
	private void getDepartmentTree(List<Department> roots,Collection<Department> elements,SortEnum sort){
		for(Department root : roots) {
			List<Department> children = new ArrayList<Department>();
			for(Department child : elements) {
				if(child.getParent() != null 
						&& child.getParent().getId()!=null
						&& root.getId().intValue()==child.getParent().getId().intValue()) {
					children.add(child);	
				}
			}
			
			if(children.size() > 0) {
				switch (sort) {
				case asc:
					Collections.sort(children);
					break;
				case desc:
					Collections.sort(children);
					Collections.reverse(children);
					break;
				default:
					Collections.sort(children);
					Collections.reverse(children);
					break;
				}
				getDepartmentTree(children,elements,sort);
			}
			
			root.setChildren(children);
		}
	}

}
