package com.easycms.management.user.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.management.user.dao.RoleDao;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	public static Logger logger = Logger.getLogger(RoleServiceImpl.class);
	
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserDao userDao;

	@Override
	public Role add(Role role) {
		// TODO Auto-generated method stub
		return roleDao.save(role);
	}

	@Override
	public Role update(Role role) {
		// TODO Auto-generated method stub
		return roleDao.save(role);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		roleDao.deleteById(id);
	}

	@Override
	public void delete(Integer[] ids) {
		roleDao.deleteByIds(ids);
	}

	@Override
	public Role findRole(Integer id) {
		// TODO Auto-generated method stub
		return roleDao.findById(id);
	}

	@Override
	public Page<Role> findPage(Role condition,Page<Role> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<Role> springPage = roleDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<Role> findPage(Page<Role> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<Role> springPage = roleDao.findAll(QueryUtil.queryConditions(new Role()),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<Role> findAll(Role condition) {
		// TODO Auto-generated method stub
		return roleDao.findAll(queryConditions(condition));
	}

	public List<Role> findAll2(){
		return roleDao.findAll2();
	}
	
	@Override
	public List<Role> findAll() {
		// TODO Auto-generated method stub
		return roleDao.findAll(queryConditions(null));
	}

	@Override
	public Integer count() {
		// TODO Auto-generated method stub
		return roleDao.count();
	}

	private Specification<Role> queryConditions(final Role role) {
		return new Specification<Role> () { 
			   public Predicate toPredicate(Root<Role> root,  
			     CriteriaQuery<?> query, CriteriaBuilder cb) { 
				Path<String> idPath = root.get("id");  
			    Path<String> namePath = root.get("name");  
			    
			    Predicate pre = null;
			    
			    if(role != null) {
			    	String sort = role.getSort();	//排序条件  
			    	
			    	if(CommonUtility.isNonEmpty(role.getName())){
			    		pre =cb.like(namePath,"%"+role.getName()+"%");  
			    	}

			    	if(null != role.getId()){
				    	pre=cb.and(pre,cb.equal(idPath,role.getId()));
				    }
			    	//升降序
			    	//排序
			    	Path<String> sortPath = null;
			    	if(CommonUtility.isNonEmpty(sort)){
			    		sortPath = root.get(sort);
			    	}else{
			    		sortPath = idPath;
			    	}
			    	
			    	Order orderBy = null;
			    	if(role.isOrderDesc()){
			    		orderBy = cb.desc(sortPath);
			    	}else{
			    		orderBy = cb.asc(sortPath);
			    	}
			    	
			    	query.orderBy(orderBy);
			    }
			    
			    if(pre != null) {
			    	query.where(pre);
			    }
			    
			    return null;  
			   }  
		};
	}
	@Override
	public List<Role> findTop() {
		// TODO Auto-generated method stub
		return roleDao.findTop();
	}
	@Override
	public List<Role> findChildren(Integer parentId) {
		// TODO Auto-generated method stub
		return roleDao.findChildren(parentId);
	}

	@Override
	public List<Role> findTopRoleByDepartmentId(Integer departmentId) {
		List<User> users = userDao.getByDepartmentIdAndParentIsNull(departmentId);
		List<Role> tempRoles = new ArrayList<Role>();
		for(User u : users) {
			List<Role> roles = u.getRoles();
			tempRoles.addAll(roles);
		}
		Collections.reverse(tempRoles);
		
		int index = 0;
		for(int i = 0;i< tempRoles.size();i++) {
			if(i == tempRoles.size() - 1 ) {
				index = i;
				break;
			}
			Integer pre = tempRoles.get(i).getLevel();
			Integer after = tempRoles.get(i+1).getLevel();
			
			if(pre != after) {
				index = i;
				break;
			}
			
		}
		
		return tempRoles.subList(0, index);
	}

	@Override
	public Role findByName(String name) {
		return roleDao.getByName(name);
	}
	
	@Override
	public Role findByEnpowerId(String enpowerId) {
		List<Role> roleList = roleDao.getListByEnpowerId(enpowerId);
		
		Role ret = null ;
		if(roleList.size()>1){
			//20180502 出现按enpowerid查询出来两条的情况　。　因为是由enpower同步。添加逻辑，如果再有这种数据，进行删除
//不知道如何产生的，岗位职务数据，按Enpower的标识查询出现多条数据。我临时把数据删除后，再同步的。程序代码我增加了逻辑，如果再有这种情况，只留一条数据。
			Integer[] ids = new Integer[roleList.size()-1];
			ret = roleList.get(roleList.size()-1);
			for(int i=0;i<roleList.size()-1;i++){
				ids[i]=roleList.get(i).getId();
			}
			roleDao.deleteByIds(ids);
		}else if(roleList==null || roleList.size()==0){
			logger.error("enpowerId="+enpowerId+" 没有记录！");
		}else{
			ret = roleList.get(0);
		}
//		roleDao.getByEnpowerId(enpowerId)
		
		return ret ;
	}
	
	@Override
	public Role findByRoleNo(String roleNo) {
		return roleDao.findByRoleNo(roleNo);
	}

	@Override
	public Role findById(Integer id) {
		return roleDao.findById(id);
	}

	/**
	 * 根据id获取角色,目的是为了兼容已有的业务方法，已有的业务方法是需要List
	 * @param enpowerId
	 * @return
	 */
	@Override
	public List<Role> findListById(List<Role> roles,Integer id) {
		if(roles!=null && roles.size()>0){
			if(roles.stream().noneMatch(b->b.getId().equals(id))){
				return roles ;
			}
		}
		
		List<Role> list = new ArrayList<Role>();
		Role r = roleDao.findById(id);
		list.add(r);
		return list;
	}

}
