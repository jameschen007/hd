package com.easycms.management.user.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.service.PrivilegeService;

/**
 * 菜单指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class PrivilegeDirective extends BaseDirective<Privilege> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(PrivilegeDirective.class);
	@Autowired
	private PrivilegeService privilegeService;

	@Override
	protected Integer count(Map params,Map<String,Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Privilege field(Map params,Map<String,Object> envParams) {
		// 根据ID查找，优先级最高
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		if (id != null) {
			Privilege privilege = privilegeService.findById(id);
			return privilege;
		}

		return null;
	}

	@Override
	protected List<Privilege> list(Map params, String filter, String order, String sort,
			boolean pageable, Page<Privilege> pager,Map<String,Object> envParams) {
		String type = FreemarkerTemplateUtility.getStringValueFromParams(params,
				"type");
		
		if("free".equals(type)) {
			List<Privilege> freePrivileges = privilegeService.findFreePrivileges();
			logger.debug("==> Find " + freePrivileges.size() + " free privilege");
			return freePrivileges;
		}
		
		Privilege condition = null;
		if (CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Privilege.class, filter,
					"yyyy-MM-dd");
		} else {
			condition = new Privilege();
		}
		condition.setOrder(order);
		condition.setSort(sort);

		if (pageable) {
			pager = privilegeService.findPage(condition, pager);
			return pager.getDatas();
		} else {
			return privilegeService.findAll(condition);
		}
	}

	@Override
	protected List<Privilege> tree(Map params,Map<String,Object> envParams) {
		return null;
	}
}
