package com.easycms.management.user.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.service.RoleService;

/**
 * 角色列表指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class RoleDirective extends BaseDirective<Role> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RoleDirective.class);

	@Autowired
	private RoleService roleService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params,Map<String,Object> envParams) {
		Integer count = roleService.count();
		return count;
	}

	@Override
	protected List<Role> tree(Map params,Map<String,Object> envParams) {
		Integer departmentId = FreemarkerTemplateUtility.getIntValueFromParams(params, "departmentId");
		List<Role> tree = null;
		if(departmentId == null) {
			tree = roleService.findTop();
		}else{
			tree = roleService.findTopRoleByDepartmentId(departmentId);
		}
		logger.debug("==> tree length [" + tree.size() + "]");
		return tree;
	}

	@Override
	protected Role field(Map params,Map<String,Object> envParams) {
		// ID
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		String name = FreemarkerTemplateUtility.getStringValueFromParams(params, "name");
		logger.debug("[id] ==> " + id);

		// 查询ID信息
		if (id != null) {
			Role role = roleService.findRole(id);
			return role;
		} else if (CommonUtility.isNonEmpty(name)){
			Role role = roleService.findByName(name);
			return role;
		}
		return null;
	}

	@Override
	protected List<Role> list(Map params, String filter, String order, String sort,
			boolean pageable, Page<Role> pager,Map<String,Object> envParams) {
		Role condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Role.class, filter,"yyyy-MM-dd");
		}else{
			condition = new Role();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}

		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search Role : " + search);
		
		// 查询列表
		if (pageable) {
			if (CommonUtility.isNonEmpty(search)){
				condition.setName(search);
				pager = roleService.findPage(condition, pager);
			}else{
				pager = roleService.findPage(condition, pager);				
			}
			return pager.getDatas();
		} else {
			List<Role> datas = roleService.findAll(condition);
			logger.debug("==> Role length [" + datas.size() + "]");
			return datas;
		}
	}

}
