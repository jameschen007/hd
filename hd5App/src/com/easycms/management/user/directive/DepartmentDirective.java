package com.easycms.management.user.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.SortEnum;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.service.DepartmentService;

/**
 * 角色列表指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class DepartmentDirective extends BaseDirective<Department> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(DepartmentDirective.class);

	@Autowired
	private DepartmentService departmentService;

	@Override
	protected Integer count(Map params,Map<String,Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Department field(Map params,Map<String,Object> envParams) {
		// ID
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,"id");
		logger.debug("[id] ==> " + id);
		if (id != null) {
			return departmentService.findById(id);
		}
		return null;
	}

	@Override
	protected List<Department> list(Map params, String filter, String order, String sort, boolean pageable, Page<Department> pager,Map<String,Object> envParams) {
		String name = FreemarkerTemplateUtility.getStringValueFromParams(params,"name");
		Department condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Department.class, filter,"yyyy-MM-dd");
		}else{
			condition = new Department();
		}
		
		if (CommonUtility.isNonEmpty(name)){
			condition.setName(name);
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		
		logger.info("Search value : " + params.get("searchValue"));
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search Department : " + search);
		
		List<Department> datas;
		try {
			if (pageable) {
				if (CommonUtility.isNonEmpty(search)){
					condition.setName(search);
					pager = departmentService.findPage(condition, pager);
				}else{
					pager = departmentService.findPage(condition, pager);					
				}
				return pager.getDatas();
			} else {
				datas = departmentService.findAll(condition);
				logger.debug("==> department length [" + datas.size() + "]");
				return datas;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected List<Department> tree(Map params,Map<String,Object> envParams) {
		String lefttree = FreemarkerTemplateUtility.getStringValueFromParams(params, "lefttree");
		if(CommonUtility.isNonEmpty(lefttree)){
			String sortStr = FreemarkerTemplateUtility.getStringValueFromParams(
					params, "sort");
			SortEnum sort = SortEnum.desc;
			try {
				sort = SortEnum.valueOf(sortStr);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			return departmentService.getTree(sort);
		}
		
		List<Department> tree = departmentService.findTop();
		logger.debug("==> tree length [" + tree.size() + "]");
		return tree;
	}

}
