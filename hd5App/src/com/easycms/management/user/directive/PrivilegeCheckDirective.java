package com.easycms.management.user.directive;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.MenuUtil;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.domain.User;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

/**
 * 角色列表指令
 * 
 * @author jiepeng
 * 
 */

@Component
public class PrivilegeCheckDirective extends BaseDirective<Privilege> {
	
	private static final Logger logger = Logger.getLogger(PrivilegeCheckDirective.class);
	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel
			.getDefaultObjectWrapper();

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		// HttpServletRequest request = requestContextHolder.getRequest();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		// 获取匹配参数,name,uri
		String name = FreemarkerTemplateUtility.getStringValueFromParams(
				params, "name");
//		logger.debug("==> name = " + name);
		String uri = FreemarkerTemplateUtility.getStringValueFromParams(params,
				"uri");
//		logger.debug("==> uri = " + uri);

		List<Menu> menus = user.getMenus();
		if(menus == null) {
			return;
		}
		
		menus = MenuUtil.getAsList(menus);
		if (CommonUtility.isNonEmpty(name)) {
			for (Menu m : menus) {
				if (name.equalsIgnoreCase(m.getName())) {
					env.setVariable("AUTH_MENU", DEFAULT_WRAPER.wrap(m));
					body.render(env.getOut());
					break;
				}
			}
		} else if (CommonUtility.isNonEmpty(uri)) {
			for (Menu m : menus) {
				if(m.getPrivilege() == null){
					continue;
				}
				if (uri.equalsIgnoreCase(m.getPrivilege().getUri())) {
					env.setVariable("AUTH_MENU", DEFAULT_WRAPER.wrap(m));
					body.render(env.getOut());
					break;
				}
			}
		}

	}

	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Privilege field(Map params, Map<String, Object> envParams) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		// HttpServletRequest request = requestContextHolder.getRequest();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		// 获取匹配参数,name,uri
		String name = FreemarkerTemplateUtility.getStringValueFromParams(
				params, "name");
		String uri = FreemarkerTemplateUtility.getStringValueFromParams(params,
				"uri");

		List<Menu> menus = user.getMenus();
		if(menus == null) {
			return null;
		}
		
		if (CommonUtility.isNonEmpty(name)) {
			for (Menu m : menus) {
				if (name.equalsIgnoreCase(m.getName())) {
					envParams.put("AUTH_MENU", m);
					break;
				}
			}
		} else if (CommonUtility.isNonEmpty(uri)) {
			for (Menu m : menus) {
				if (uri.equalsIgnoreCase(m.getPrivilege().getUri())) {
					envParams.put("AUTH_MENU", m);
					break;
				}
			}
		}
		return null;
	}

	@Override
	protected List<Privilege> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<Privilege> pager,
			Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<Privilege> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
