package com.easycms.management.user.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.SortEnum;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.service.MenuService;

/**
 * 菜单指令
 * 
 * @author jiepeng
 * 
 */
@Component
public class MenuDirective extends BaseDirective<Menu> {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MenuDirective.class);
	@Autowired
	private MenuService MenuService;

	@Override
	protected Integer count(Map params,Map<String,Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Menu field(Map params,Map<String,Object> envParams) {
		// 根据ID查找，优先级最高
		String id = FreemarkerTemplateUtility.getStringValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		if (id != null) {
			Menu Menu = MenuService.findById(id);
			return Menu;
		}

		return null;
	}

	@Override
	protected List<Menu> list(Map params, String filter, String order, String sort,
			boolean pageable, Page<Menu> pager,Map<String,Object> envParams) {
		
		// 查找父级菜单及子菜单
		String parentId = FreemarkerTemplateUtility.getStringValueFromParams(
				params, "parentId");
		logger.debug("[parentId] ==> " + parentId);
		if (CommonUtility.isNonEmpty(parentId)) {
			List<Menu> menus = MenuService.findChildren(parentId); // 子菜单
			
			menus.add(0,MenuService.findById(parentId));
			pager.execute(menus.size(), pager.getPageNum(), menus);
			
			return pager.getDatas();
		}
		Menu condition = null;
		if (CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(Menu.class, filter,
					"yyyy-MM-dd");
		} else {
			condition = new Menu();
		}
		condition.setOrder(order);
		condition.setSort(sort);

		if (pageable) {
			pager = MenuService.findPage(condition, pager);
			return pager.getDatas();
		} else {
			return MenuService.findAll(condition);
		}
	}

	@Override
	protected List<Menu> tree(Map params,Map<String,Object> envParams) {
		String sortStr = FreemarkerTemplateUtility.getStringValueFromParams(
				params, "sort");
		SortEnum sort = SortEnum.desc;
		try {
			sort = SortEnum.valueOf(sortStr);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return MenuService.getTree(sort);
	}
}
