package com.easycms.management.user.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.Privilege;

@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface PrivilegeDao extends BasicDao<Privilege>,Repository<Privilege,Integer> {
	
	/**
	 * 删除所有
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Privilege")
	int deleteAll();
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM Privilege o WHERE o.id  IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Privilege findByName(String name);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Privilege findByNameAndUri(String name,String uri);

	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Privilege findByUri(String uri);
	
	@Modifying
	@Query("UPDATE Privilege p SET p.menu.id = ?2 WHERE p.id =?1 and isDel=0")
	int updateMenu(Integer id,String menuId);
	
	int exists(Integer id);
	/**
	 * 新增时判断名称是否存在
	 * @param name
	 * @return
	 */
	@Query("select count(id) FROM Privilege p WHERE p.name =?1 AND p.uri = ?2 and isDel=0")
	int exist(String name,String uri);
	
	@Query("select count(id) FROM Privilege p WHERE p.uri =?1 and isDel=0")
	int existURi(String uri);
	
	@Query("select count(id) FROM Privilege p WHERE p.name =?1 and isDel=0")
	int existName(String name);
	/**
	 * 修改时判断名称是否存在
	 * @param name
	 * @return
	 */
	@Query("select count(id) FROM Privilege p WHERE p.name =?1 and p.id!=?2 and isDel=0")
	int existByNameAndId(String name,int id);
	
	@Query("select count(id) FROM Privilege p WHERE p.uri =?1 and p.id!=?2 and isDel=0")
	int existByUriAndId(String uri,int id);
	
	@Query("FROM Privilege p WHERE p.menu is NULL")
	List<Privilege> findNoMenuPrivileges();
}
