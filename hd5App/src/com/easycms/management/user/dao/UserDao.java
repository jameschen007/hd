package com.easycms.management.user.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.management.user.domain.User;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface UserDao extends BasicDao<User> ,Repository<User,Integer>{
	
	@Modifying
	@Query("UPDATE User u SET u.isDel = 1 WHERE u.name  = ?1")
	int deleteByName(String name);
	
	@Modifying
	@Query("UPDATE User u SET u.isDel = 1 WHERE u.name IN (?1)")
	int deleteByNames(String[] names);
	
	
	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	@Modifying
	@Query("UPDATE User u SET u.isDel = 1 WHERE u.id  = ?1 AND u.defaultAdmin = 0")
	int deleteById(Integer id);
	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	@Modifying
	@Query("delete from User u WHERE u.id  = ?1 AND u.defaultAdmin = 0")
	int deleteDbById(Integer id);
	
	@Query("from User u where u.isDel=0")
	public List<User> findAll();
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("UPDATE User u SET u.isDel = 1 WHERE u.id  IN (?1) AND u.defaultAdmin = 0")
	int deleteByIds(Integer[] ids);
	
	/**
	 * 更新密码
	 * 
	 * @param id
	 * @param oldPassword
	 * @param newPassword
	 */
	@Modifying
	@Query("UPDATE User u SET u.password =?3 WHERE u.id = ?1 AND u.password =?2")
	int updatePassword(Integer id, String oldPassword, String newPassword);

	@Query("FROM User u WHERE u.name = ?1 AND u.isDel = 0")
	User exsit(String username);

	@Query("FROM User u WHERE u.name = ?1 AND u.id != ?2 AND u.isDel = 0")
	User exsitExceptWithId(String username, Integer id);

	/**
	 * 
	 * @param map
	 *            参数名为:username,pwd
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.name = ?1 AND u.password = ?2 AND u.isDel = 0")
	User findByNameAndPassword(String username, String password);

	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.name = ?1 AND u.isDel = 0")
	User findByName(String username);
	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.realname = ?1 AND u.isDel = 0")
	User findByRealame(String username);
	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.name = ?1 AND u.isDel = 1")
	User findByNameIsDel1(String username);
	
	/**
	 * 查找部门下直接领导为空的人
	 * @param departmentId
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.department.id = ?1 AND u.parent IS NULL")
	List<User> getByDepartmentIdAndParentIsNull(Integer departmentId);
	
	//@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.department.id = ?1")
	List<User> getByDepartmentId(Integer departmentId);

	//@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
	@Query("FROM User u WHERE u.parent.id = ?1")
	List<User> findUserByParentId(Integer userId);
	/**
	 * 根据人员id查下级人员，根据当前部门查下级部门再查下级部门下的人员
	 * @param userId
	 * @return
	 */
	@Query(value="select uc.* from ec_user uc ,ec_department dc,ec_user u where uc.department_id=dc.id and dc.parent_id=u.department_id and u.id in (?1) and uc.is_del=0",nativeQuery=true)
	List<User> findUserByDeptParentId(Integer userId);
	/**
	 * 根据人员id查下级人员，根据当前部门查下级部门再查下级部门下的人员
	 * 根据人员id 查人员的部门的下级部门下的人员，并且角色是指定的角色
	 * @param userId
	 * @return
	 */
	@Query(value="select uc.* from ec_user uc ,ec_department dc,ec_user u where uc.department_id=dc.id and dc.parent_id=u.department_id and u.id in (?1) and uc.is_del=0 and uc.id in ( select ur.user_id from ec_user_role ur,ec_role role where role.id = ur.role_id and role.role_name in (?2)) ",nativeQuery=true)
	List<User> findUserByDeptParentIdAndRoleName(Integer userId,String roleName);	
	/**
	 *  根据部门id集合和人员的角色名字查询人员
	 * @return
	 */
	@Query(value="select u.* from ec_user u where u.department_id in (?1) and u.is_del=0 and u.id in ( select ur.user_id from ec_user_role ur,ec_role role where role.id = ur.role_id and role.role_name in (?2)) ",nativeQuery=true)
	List<User> findUserByDepartmentIdAndRoleName(List<Integer> departmentid,List<String> roleName);

	@Query(value = "select * from ec_user t where t.id in ( select ur.user_id from ec_user_role ur,ec_role role where role.id = ur.role_id and role.role_name in (?1)) and t.id in (select um.user_id from ec_user_module um,modules modu where modu.id = um.module_id and modu.type in (?2)) and t.is_del=0", nativeQuery = true)
	List<User> findRoleAndModule(Set<String> roleName,List<String> moduleType);
	/**
	 * 根据部门id和模块查询人员
	 * @param departmentId
	 * @param moduleType
	 * @return
	 */
	@Query(value = "select * from ec_user t where t.department_id in (?1) and t.is_del=0 and t.id in (select um.user_id from ec_user_module um,modules modu where modu.id = um.module_id and modu.type in (?2))", nativeQuery = true)
	List<User> findDeptmentAndModule(Set<Integer> departmentId,List<String> moduleType);

	@Query("FROM User u WHERE u.enpowerId = ?1")
	User findByEnpowerId(String enpowerId);
	/**
	 * 根据部门名称和人员名称查人员
	 * @param departmentId
	 * @param moduleType
	 * @return
	 */
	@Query(value = "select b.* from ec_department a,ec_user b where a.id=b.department_id and TRIM(a.department_name)=trim(?1) and TRIM(b.real_name)=trim(?2) and b.is_del=0", nativeQuery = true)
	User findDeptmentAndUser(String deptName,String realName);

	
}
