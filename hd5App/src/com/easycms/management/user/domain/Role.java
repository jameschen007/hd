package com.easycms.management.user.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name="ec_role")
@Cacheable
public class Role extends BasicForm implements Comparable<Role>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4486165151681270177L;
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="role_name")
	private String name;
	
	@Column(name="role_no")
	private String roleNo;
	
	@Column(name="enpower_id")
	private String enpowerId;
	
	@Column(name="enpower_department_id")
	private String enpowerDepartmentId;
	
	public String getEnpowerDepartmentId() {
		return enpowerDepartmentId;
	}

	public void setEnpowerDepartmentId(String enpowerDepartmentId) {
		this.enpowerDepartmentId = enpowerDepartmentId;
	}

	@ManyToMany(targetEntity=com.easycms.management.user.domain.User.class,mappedBy="roles",fetch=FetchType.LAZY,cascade={CascadeType.REMOVE, CascadeType.REFRESH})
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<User> users;
	
	@ManyToMany(targetEntity=com.easycms.management.user.domain.Menu.class,fetch=FetchType.LAZY)
	@JoinTable(
			name="ec_role_menu",
			joinColumns=@JoinColumn(name="role_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="menu_id",referencedColumnName="id")
			)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<Menu> menus;
	
	@ManyToOne(targetEntity = com.easycms.management.user.domain.Role.class,fetch=FetchType.LAZY)
	@JoinColumn(name="parent_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Role parent;
	@Column(name="is_del")
	private boolean isDel;

	@JsonIgnore
	@OneToMany(targetEntity=com.easycms.management.user.domain.Role.class,cascade={CascadeType.REMOVE, CascadeType.REFRESH},fetch=FetchType.LAZY,mappedBy="parent")
	private List<Role> children;
	
	private Integer level;
	
	private Boolean coexist;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		if (null != users){
			List<User> newList = new ArrayList<User>();
			for (User u : users){
				if (!u.isDel()){
					newList.add(u);
				}
			}
			return newList;
		}
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}


	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public Role getParent() {
		return parent;
	}

	public void setParent(Role parent) {
		this.parent = parent;
	}

	public List<Role> getChildren() {
		return children;
	}

	public void setChildren(List<Role> children) {
		this.children = children;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	public int compareTo(Role r) {
		// TODO Auto-generated method stub
		return r.getLevel() - this.level;
	}

	public Boolean getCoexist() {
		return coexist;
	}

	public void setCoexist(Boolean coexist) {
		this.coexist = coexist;
	}

	public boolean isDel() {
		return isDel;
	}

	public void setDel(boolean del) {
		isDel = del;
	}

	public String getRoleNo() {
		return roleNo;
	}

	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}

	public String getEnpowerId() {
		return enpowerId;
	}

	public void setEnpowerId(String enpowerId) {
		this.enpowerId = enpowerId;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof Role) {
        	Role role = (Role) obj;
            return (id.equals(role.getId()));
        }
        return super.equals(obj);
    }
	
	@Override 
    public int hashCode() {
        return id.hashCode();
    }
}
