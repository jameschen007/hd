package com.easycms.management.user.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name="ec_department")
@Cacheable
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) 
public class Department extends BasicForm implements Comparable<Department>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public Department(){}
	public Department(Integer id){
		this.id = id;
	}
	
	
	/**
	 * 序号
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	/**
	 * 部门名称
	 */
	@Column(name="department_name")
	private String name;
	
	/**
	 * 部门用户
	 */
	@JsonIgnore
	@OneToMany(targetEntity=com.easycms.management.user.domain.User.class,fetch=FetchType.LAZY,mappedBy="department")
	//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<User> users;
	
	/**
	 * 上级部门
	 */
	@JsonIgnore
	@ManyToOne(targetEntity=com.easycms.management.user.domain.Department.class,cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)
	@JoinColumn(name="parent_id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Department parent;
	
	@JsonIgnore
	@OneToMany(targetEntity=com.easycms.management.user.domain.Department.class,cascade={CascadeType.REMOVE},fetch=FetchType.LAZY,mappedBy="parent")
	private List<Department> children;
	
	@Column(name = "is_del")
	private boolean isDel;
	
	@Column(name = "org_code")
	private String orgCode;
	
	@Column(name = "org_name")
	private String orgName;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "other1")
	private String other1;
	
	@Column(name = "other2")
	private String other2;
	
	@Column(name = "other3")
	private String other3;
	
	@Column(name = "other4")
	private String other4;
	
	@Column(name = "other5")
	private String other5;
	
	@Column(name = "other6")
	private String other6;
	
	@Column(name = "other7")
	private String other7;
	
	@Column(name = "other8")
	private String other8;
	
	@Column(name = "other9")
	private String other9;
	
	@Column(name = "other10")
	private String other10;
	
	@Column(name = "org_appcode")
	private String orgAppcode;
	
	@Column(name = "end_date")
	private Date endDate;
	
	@Column(name = "delete_yn")
	private String deleteYn;
	
	@Column(name = "business_no")
	private String businessNo;
	
	@Column(name="enpower_id")
	private String enpowerId;
	
	@Column(name="enpower_parent_id")
	private String enpowerParentId;
	/**
	 * 顶级职务
	 */
	@JsonIgnore
	@ManyToOne(targetEntity=com.easycms.management.user.domain.Role.class,cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)
	@JoinColumn(name="top_role_id")
	//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private Role topRole;
	
	
	public List<User> getUsers() {
		if (null == users){
			return users;
		}
		List<User> newList = new ArrayList<User>();
		for (User u : users){
			if (!u.isDel()){
				newList.add(u);
			}
		}
		return newList;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public Role getTopRole() {
		return topRole;
	}
	
	/**
	 * 返回顶级职务列表
	 * 将topRole放进list作为外部查询使用
	 * @return
	 */
	@JsonIgnore
	public List<Role> getTopRoles() {
		List<Role> roles = null;
		if(this.topRole != null) {
			roles = new ArrayList<Role>();
			roles.add(this.topRole);
		}
		return roles;
	}
	public void setTopRole(Role topRole) {
		this.topRole = topRole;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getParent() {
		return parent;
	}

	public void setParent(Department parent) {
		this.parent = parent;
	}

	public List<Department> getChildren() {
		return children;
	}

	public void setChildren(List<Department> children) {
		this.children = children;
	}
	
	
	@JsonIgnore
	public List<User> getAllUsers(){
//		List<Role> roles = this.roles;
//		List<User> users = new ArrayList<User>();
//		if(roles.size()  == 0){
//			return users;
//		}
//	
//		List<Department> departments = this.children;
//		
//		if(departments != null && departments.size() >= 0){
//			for (Department department : departments) {
//				getChilrenRoles(department,roles);
//			}
//		}
//		
//		if(roles == null || roles.size() == 0){
//			return null;
//		}
//		
//		for (Role role : roles) {
//			List<User> tus = role.getUsers();
//			if(tus != null && tus.size() > 0){
//				users.addAll(tus);
//			}
//		}
		
		return users;
	}
//	
	@JsonIgnore
	private void getChilrenRoles(Department department, List<Role> roles) {
//		if(department == null){
//			return;
//		}
//		List<Department> deps = department.getChildren();
//		if(deps == null || deps.size() == 0 ){
//			return;
//		}
//		roles.addAll(department.getRoles());
//		for (Department department2 : deps) {
//			getChilrenRoles(department2, roles);
//		}
	}
	
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}

	public boolean isDel() {
		return isDel;
	}

	public void setDel(boolean del) {
		isDel = del;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOther1() {
		return other1;
	}
	public void setOther1(String other1) {
		this.other1 = other1;
	}
	public String getOther2() {
		return other2;
	}
	public void setOther2(String other2) {
		this.other2 = other2;
	}
	public String getOther3() {
		return other3;
	}
	public void setOther3(String other3) {
		this.other3 = other3;
	}
	public String getOther4() {
		return other4;
	}
	public void setOther4(String other4) {
		this.other4 = other4;
	}
	public String getOther5() {
		return other5;
	}
	public void setOther5(String other5) {
		this.other5 = other5;
	}
	public String getOther6() {
		return other6;
	}
	public void setOther6(String other6) {
		this.other6 = other6;
	}
	public String getOther7() {
		return other7;
	}
	public void setOther7(String other7) {
		this.other7 = other7;
	}
	public String getOther8() {
		return other8;
	}
	public void setOther8(String other8) {
		this.other8 = other8;
	}
	public String getOther9() {
		return other9;
	}
	public void setOther9(String other9) {
		this.other9 = other9;
	}
	public String getOther10() {
		return other10;
	}
	public void setOther10(String other10) {
		this.other10 = other10;
	}
	public String getOrgAppcode() {
		return orgAppcode;
	}
	public void setOrgAppcode(String orgAppcode) {
		this.orgAppcode = orgAppcode;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getDeleteYn() {
		return deleteYn;
	}
	public void setDeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}
	public String getBusinessNo() {
		return businessNo;
	}
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	public String getEnpowerId() {
		return enpowerId;
	}
	public void setEnpowerId(String enpowerId) {
		this.enpowerId = enpowerId;
	}
	public String getEnpowerParentId() {
		return enpowerParentId;
	}
	public void setEnpowerParentId(String enpowerParentId) {
		this.enpowerParentId = enpowerParentId;
	}
	@Override
	public int compareTo(Department arg0) {
		return this.id - arg0.getId();
	}
}
