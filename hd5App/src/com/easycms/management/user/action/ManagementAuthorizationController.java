package com.easycms.management.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.enums.LoginTypeEnum;
import com.easycms.core.form.FormHelper;
import com.easycms.core.response.JsonResponse;
import com.easycms.core.strategy.authorization.Authenticator;
import com.easycms.core.strategy.authorization.Authorization;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.LoginForm;
import com.easycms.management.user.form.validator.LoginFormValidator;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/management/authorization")
public class ManagementAuthorizationController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementAuthorizationController.class);
	private static final String AJAX_REQUEST = "XMLHttpRequest";

	@Autowired
	private UserService userService;
	@Autowired
	private Authenticator<User> authenticator;

	/**
	 * 显示管理员登录页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String view = ForwardUtility.forwardAdminView("login");
		String requestType = request.getHeader("X-Requested-With");
		if (AJAX_REQUEST.equals(requestType)) {
			view = ForwardUtility.forwardAdminView("/login_ajax");
		}
		return view;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public void login(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("loginForm") @Valid LoginForm loginForm,
			BindingResult errors) {
		//登录信息长度截取
		if(loginForm.getUsername()!=null && loginForm.getUsername().length()>50){
			loginForm.setUsername(loginForm.getUsername().substring(0,50));
		}
		if(loginForm.getPassword()!=null && loginForm.getPassword().length()>50){
			loginForm.setPassword(loginForm.getPassword().substring(0,50));
		}
		JsonResponse jres = new JsonResponse();
		// 校验表单
		LoginFormValidator validator = new LoginFormValidator();
		validator.setRequest(request);
		validator.validate(loginForm, errors);
		if(ContextPath.WZ){
			logger.debug("王志本机测试，ManagementAuthorizationController不验证密码！！！");
		}else{//通用验证
			
		if (errors.hasErrors()) {
			jres = FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(jres));
			return;
		}

		}
		
		// 用户登录
		logger.info("==> User is logining");

		String username = loginForm.getUsername();
		logger.info("[username] = " + username);

		String pwd = loginForm.getPassword();
		logger.info("[pwd] = " + pwd);

		ApplicationContext ac = WebApplicationContextUtils
				.getWebApplicationContext(HttpUtility.getServletContext());

		Authorization<User> auth = null;
		try {
			String loginType = loginForm.getLoginType();
			if(CommonUtility.isNonEmpty(loginType)) {
				LoginTypeEnum loginTypeEnum = LoginTypeEnum.valueOf(loginType);
				switch (loginTypeEnum) {
				case LDAP:
					auth = (Authorization<User>) ac.getBean("ldapAuth");
					authenticator.setAuth(auth);
					break;
				case SYSTEM:
					//auth = (Authorization) ac.getBean("frontendAuth");
					break;
				case AJAX:
					//auth = (Authorization) ac.getBean("frontendAuth");
					break;
				}
			}
			
			User user = authenticator.executeAuth(username, pwd, true);
			if(ContextPath.WZ){
				logger.debug("王志本机测试，ManagementAuthorizationController不验证密码！！！");
				user = userService.findUser(username);
			}
			if (user == null || !username.equals(user.getName())) {
				errors.rejectValue("username", "form.user.notExist","User is not exist");
				jres = FormHelper.printErros(errors, logger);
				HttpUtility.writeToClient(response, CommonUtility.toJson(jres));
				return;
			}
			
			logger.info("==> User [" + username + "] login success.");
			HttpUtility.writeToClient(response, CommonUtility.toJson(jres));
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			jres.setCode("-1010");
			jres.setMessage(e.getMessage());
			HttpUtility.writeToClient(response, CommonUtility.toJson(jres));
		}
		
	}

	/**
	 * 用户退出登录
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request,
			HttpServletResponse response) {

		User user = (User) request.getSession().getAttribute("user");
		String username = user == null ? "" : user.getName();
		logger.info("==> user [" + username + "] logout");
		request.getSession().removeAttribute("user");
		request.getSession().removeAttribute("type");
		String returnString = ForwardUtility.redirectWithSpringMVC("/");
		return returnString;
	}
}
