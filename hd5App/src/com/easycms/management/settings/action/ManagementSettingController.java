package com.easycms.management.settings.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.settings.domain.Website;
import com.easycms.management.settings.form.WebsiteForm;
import com.easycms.management.settings.form.validator.WebsiteFormValidator;
import com.easycms.management.settings.service.WebsiteService;
import com.easycms.management.user.service.PrivilegeService;

@Controller
@RequestMapping("/management/settings")
public class ManagementSettingController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(ManagementSettingController.class);
	@Autowired
	private WebsiteService websiteService;
	@Autowired
	private WebsiteFormValidator validator;
	@Autowired
	private PrivilegeService privilegeService;

	/**
	 * 系统设置指南
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("")
	public String settings(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		return ForwardUtility.forwardAdminView("/settings/settings_index");
	}
	
	/**
	 * 系统设置UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/website", method = RequestMethod.GET)
	public String settingUI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.debug("==> Show website setting UI.");
		return ForwardUtility.forwardAdminView("/settings/website_setting");
	}

	/**
	 * 更新系统设置
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/website", method = RequestMethod.POST)
	public void setting(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("websiteForm") @Valid WebsiteForm websiteForm,
			BindingResult errors) throws Exception {

		logger.info("[websiteForm] = " + CommonUtility.toJson(websiteForm));

		// 校验表单信息
		validator.validate(websiteForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save Failed.");
			logger.debug("==> End save setting.");
			return;
		}

		// 如果ID不为空,保存当前站点信息
		Integer id = websiteForm.getId();
		Website setting = new Website();
		if (id == null) {
			id = ContextUtil.getCurrentWebsite().getId();
		}

		setting.setId(id);
		setting.setSitename(websiteForm.getSitename());
		setting.setDomain(websiteForm.getDomain());
		setting.setKeywords(websiteForm.getKeywords());
		String appPath = websiteForm.getRoot();
		appPath = appPath.replace("\\", "/");
		setting.setRoot(appPath);
		setting.setDescribe(websiteForm.getDescribe());
		setting.setPagesizeList(websiteForm.getPagesizeList());
		setting.setDefaultPagesize(websiteForm.getDefaultPagesize());
		/*setting.setUploadMaxSize(Integer.parseInt(websiteForm.getMaxSize()));
		setting.setTplFileType(websiteForm.getTemplUploadType());
		String tplDir = websiteForm.getTemplUploadDir();
		tplDir = tplDir.replace("\\", "/");
		setting.setUploadTplDir(tplDir);
		String resDir = websiteForm.getResUploadDir();
		resDir = resDir.replace("\\", "/");
		setting.setUploadResDir(resDir);*/

		boolean result = websiteService.update(setting);

		HttpUtility.writeToClient(response, CommonUtility.toJson(result));

		ContextUtil.setLocalWebsite(websiteService.findLocalSite());
		// 如果是更新当前站点,则同步更新session
//		Website currentWebsite = ContextUtil.getCurrentWebsite();
//		if (result && id == currentWebsite.getId()) {
//			logger.debug("==> Synchron local setting.");
//			ContextUtil.setCurrentWebsite(setting);
//		}

		logger.debug("==> End save setting.");
		return;
	}
	
	/**
	 * 恢复初始化
	 * @param request
	 * @param response
	 * @return
	 */
//	@RequestMapping("/init")
//	public String init(HttpServletRequest request,
//			HttpServletResponse response) {
//		logger.info("==> To init privileges.");
//		User user = (User) request.getSession().getAttribute("user");
//		if(user != null && user.isDefaultAdmin()) {
//			//初始化所有权限
//			logger.info("==> Parse all privileges.");
//			List<Privilege> list = PrivilegeUtil.getPrivilegesFromConf(ContextUtil.getRealPath("/WEB-INF/conf/init/cms-init-privileges.xml"));
//			logger.info("==> Init all privileges.");
//			privilegeService.updatePrivileges(list);
//		}
//		request.getSession().removeAttribute("user");
//		String returnString = ForwardUtility.redirectWithSpringMVC("/");
//		return returnString;
//	}

}
