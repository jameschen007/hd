
package com.easycms.management.settings.form;

import com.easycms.core.form.BasicForm;



/** 
 * @author dengjiepeng: 
 * @areate Date:2012-3-21 
 * 
 */
public class DBForm extends BasicForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] names;
	public String[] getNames() {
		return names;
	}
	public void setNames(String[] names) {
		this.names = names;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public boolean isNamesEmpty(){
		return names == null || names.length == 0;
	}
}
