package com.easycms.management.settings.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.management.settings.domain.SystemLog;

@Transactional
public interface SystemLogDao extends Repository<SystemLog,Integer>,BasicDao<SystemLog>{

}
