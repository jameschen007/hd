package com.easycms.management.settings.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="system_log")
@Data
public class SystemLog implements Serializable{

	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue
		@Column(name = "id")
		private Integer id;

		@Column(name = "with_time")
		private BigDecimal withTime;
		
		@Column(name = "uri")
		private String uri;
		
}
