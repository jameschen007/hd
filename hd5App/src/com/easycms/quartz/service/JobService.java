package com.easycms.quartz.service;

public interface JobService {
	void doJob(Object jobDataMap);
}
