package com.easycms.core.util;

/**
 * CMS Theme工具类
 * @author Administrator
 *
 */
public class CMSThemeUtil {
	
	public static String DEFAULT_THEME = null;
	
	public static String getResPath(){
		return ForwardUtility.processSlash(ContextUtil.getServletContext().getContextPath())
				+ ForwardUtility.processSlash("theme")
				+ ForwardUtility.processSlash("deploy")
				+ ForwardUtility.processSlash(DEFAULT_THEME);
	}
}
