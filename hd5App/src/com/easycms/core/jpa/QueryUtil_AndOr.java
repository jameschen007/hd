package com.easycms.core.jpa;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.log4j.Logger;
import org.springframework.data.jpa.domain.Specification;

import com.easycms.common.util.CommonUtility;

public class QueryUtil_AndOr {
	private static final Logger logger = Logger.getLogger(QueryUtil_AndOr.class);
	
	public static final String TYPE_LIST = "list";	
	public static final String TYPE_TREE = "tree";	
	public static final String TYPE_COUNT = "count";
	public static final String TYPE_FIELD = "field";

	public static <T> Specification<T> queryConditions(final T t,final T andOr) {
		Specification<T> sepc =  createCondition(t,andOr,null,null,null);
		return sepc;
	}
	public static <T> Specification<T> queryConditions(final T t,final T andOr,final Map<String,String> cmethod) {
		Specification<T> sepc =  createCondition(t,andOr,cmethod,null,null);
		return sepc;
	}
//	public static <T> Specification<T> queryConditions(final T t,final T andOr, final List<Class<?>> clazzList) {
//		Specification<T> sepc =  createCondition(t,andOr,clazzList,null);
//		return sepc;
//	}
//	public static <T> Specification<T> queryConditions(final T t,final T andOr, final List<Class<?>> clazzList,final CustomQuery custom) {
//		Specification<T> sepc =  createCondition(t,andOr,clazzList,custom);
//		return sepc;
//	}
	public static <T> Specification<T> queryConditions(final T t,final T andOr,final Map<String,String> cmethod, final List<Class<?>> clazzList,final CustomQuery custom) {
		Specification<T> sepc =  createCondition(t,andOr,cmethod,clazzList,custom);
		return sepc;
	}
	private static <T> Specification<T> createCondition(final T t,final T andOr,final List<Class<?>> clazzList,final CustomQuery custom) {
		return new Specification<T> () {
			public Predicate toPredicate(Root<T> root,  
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate pre = null;
				Path<String> sortPath = null;
				
				logger.debug("==> Find field.");
				if(t != null){
					Class<? extends Object> clazz = t.getClass();
					Field[] fields = getClassFields(clazz);
					for(Field field : fields) {
						try {
							String name = field.getName();
//							if("serialVersionUID".equals(name)
//									|| "sort".equals(name)
//									|| "order".equals(name)
//									) {
//								continue;
//							}
//							logger.debug("[field.getType()] = " + field.getType());
//							logger.debug("[field.getType().getName()] = " + field.getType().getName());
//							logger.debug("[field.getType().getSimpleName()] = " + field.getType().getSimpleName());
//							logger.debug("[field.getType().getClass()] = " + field.getType().getClass());
							Object result = getFieldValue(t, field);
							
							if(result != null) {
								
								javax.persistence.Transient tran = field.getAnnotation(javax.persistence.Transient.class);
								if(tran != null) {
									continue;
								}
								logger.debug("[name] = " + name);
								logger.debug("[value] = " + result);
								logger.debug("[sortPath] = " + sortPath);
								
								String typeName = field.getType().getSimpleName();
								typeName = typeName.substring(0,1).toUpperCase() + typeName.substring(1);
								typeName = "java.lang." + typeName;
								try{
									if(result!=null){
										Class clazz2 = result.getClass();
										logger.debug("[class type] = " + clazz2.getName());
									}else{
										Class<?> type = Class.forName(typeName);
										logger.debug("[class type] = " + type);
									}
								}catch(java.lang.ClassNotFoundException e){
									e.printStackTrace();
								}
//								logger.debug("[clazz.newInstance() instanceof String] = " + (type.newInstance() instanceof String));
								logger.debug("result.equals(String[].class) = " + result.getClass().equals(String[].class));
								logger.debug("--------------");
								if(result.getClass().equals(String.class)) {
									String value = (String) result;
									Path<String> fieldPath = root.get(name);
//									logger.debug("[fieldPath] = " + fieldPath);
									
									if(CommonUtility.isNonEmpty(value)){
										pre = combinePre(cb, pre, cb.like(fieldPath,"%"+value+"%"));
									}
								}else if(result.getClass().equals(Date.class)) {
									Date value = (Date) result;
									Path<Date> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Integer.class)) {
									Integer value = (Integer) result;
									Path<Integer> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Long.class)) {
									Long value = (Long) result;
									Path<Long> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Short.class)) {
									Short value = (Short) result;
									Path<Short> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Float.class)) {
									Float value = (Float) result;
									Path<Float> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Double.class)) {
									Double value = (Double) result;
									Path<Double> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Boolean.class)) {
									Boolean value = (Boolean) result;
									Path<Boolean> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Byte.class)) {
									Byte value = (Byte) result;
									Path<Byte> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}
								if(null != clazzList){
									for(int m=0;m<clazzList.size();m++){
										if (result.getClass().equals(clazzList.get(m))) {
											// Object value = result;
											Path<Object> fieldPath = root.get(name);
											pre = combinePre(cb, pre,cb.equal(fieldPath, result));
										}
									}
								}
							}
							
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					// #########################################################
					// 日期范围
					// #########################################################
					String[] dateFields = (String[]) getNonBooleanFieldValue(t,"dateFields");
					if(dateFields != null && dateFields.length > 0) {
						String formatter = (String) getNonBooleanFieldValue(t, "formatter");
						String startTime = (String) getNonBooleanFieldValue(t, "startTime");
						String endTime = (String) getNonBooleanFieldValue(t, "endTime");
						logger.debug("[formatter] = " + formatter);
						logger.debug("[startTime] = " + startTime);
						logger.debug("[endTime] = " + endTime);
						//  修改bug #3163 start =============
						if(CommonUtility.isNonEmpty(formatter)
								&& (CommonUtility.isNonEmpty(startTime) || CommonUtility.isNonEmpty(endTime))
								){
							Date start = null;
							Date end = null;
							if(!CommonUtility.isNonEmpty(startTime)){
								Calendar _start=Calendar.getInstance();
								_start.set(1970, 0, 1);
								start=_start.getTime();
							}else{
								start = CommonUtility.parseDate(startTime, formatter);
							}
							
							if(!CommonUtility.isNonEmpty(endTime)){
								end=new Date();
							}else{
								end=CommonUtility.parseDate(endTime, formatter);
							}
							//  修改bug #3163 end =============
							for(String dateField : dateFields) {
								Path<Date> fieldPath = root.get(dateField);
								pre = combinePre(cb, pre, cb.between(fieldPath,start,getEndDate(end)));
							}
						}
					}
					
					// #########################################################
					// 自定义条件查询
					// #########################################################
					if(custom != null) {
						custom.execute(pre);
					}
					
					
					// #########################################################
					// 排序
					// #########################################################
					String sort = (String) getNonBooleanFieldValue(t, "sort");
					
					if(CommonUtility.isNonEmpty(sort)) {
						sortPath = root.get(sort);
					}
					
					// #########################################################
					// 升降序
					// #########################################################
					Order orderBy = null;
					String order = (String) getNonBooleanFieldValue(t, "order");
					if(CommonUtility.isNonEmpty(order) && sortPath != null) {
						if("desc".equals(order)) {
							orderBy = cb.desc(sortPath);
						}else if("asc".equals(order)) {
							orderBy = cb.asc(sortPath);
						}
						query.orderBy(orderBy);
					}
					
					if(pre != null) {
						query.where(pre);
					}
				}
				return pre;  
			}  
		};
	}
	
/**
 * 
 * @param t
 * @param andOr
 * @param cmethod 中放　beanid＝bean的id
 * @param clazzList
 * @param custom
 * @return
 */
	private static <T> Specification<T> createCondition(final T t,final T andOr,final Map<String,String> cmethod,final List<Class<?>> clazzList,final CustomQuery custom) {
		return new Specification<T> () {
			public Predicate toPredicate(Root<T> root,  
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate pre = null;
				Path<String> sortPath = null;
				
				logger.debug("==> Find field.");
				if(t != null){
					Class<? extends Object> clazz = t.getClass();
					Field[] fields = getClassFields(clazz);
					for(Field field : fields) {
						try {
							String name = field.getName();
//							if("serialVersionUID".equals(name)
//									|| "sort".equals(name)
//									|| "order".equals(name)
//									) {
//								continue;
//							}
//							logger.debug("[field.getType()] = " + field.getType());
//							logger.debug("[field.getType().getName()] = " + field.getType().getName());
//							logger.debug("[field.getType().getSimpleName()] = " + field.getType().getSimpleName());
//							logger.debug("[field.getType().getClass()] = " + field.getType().getClass());
							Object result = getFieldValue(t, field);
							
							if(result != null) {
								
								javax.persistence.Transient tran = field.getAnnotation(javax.persistence.Transient.class);
								if(tran != null) {
									continue;
								}
								logger.debug("[name] = " + name);
								logger.debug("[value] = " + result);
								logger.debug("[sortPath] = " + sortPath);
								
								String typeName = field.getType().getSimpleName();
								typeName = typeName.substring(0,1).toUpperCase() + typeName.substring(1);
								typeName = "java.lang." + typeName;
								try{
									if(result!=null){
										Class clazz2 = result.getClass();
										logger.debug("[class type] = " + clazz2.getName());
									}else{
										Class<?> type = Class.forName(typeName);
										logger.debug("[class type] = " + type);
									}
								}catch(java.lang.ClassNotFoundException e){
									e.printStackTrace();
								}
//								logger.debug("[clazz.newInstance() instanceof String] = " + (type.newInstance() instanceof String));
								logger.debug("result.equals(String[].class) = " + result.getClass().equals(String[].class));
								logger.debug("--------------");

								boolean xCompare = false;//not equal compare
								String compareTp = null;
								if(cmethod!=null && cmethod.get(name)!=null){
									xCompare = true ;
									compareTp = cmethod.get(name);
								}
								if(result.getClass().equals(String.class)) {
									String value = (String) result;
									Path<String> fieldPath = root.get(name);
//									logger.debug("[fieldPath] = " + fieldPath);
									
									if(xCompare&&JpaMethod.equals.equals(compareTp)){
										if(CommonUtility.isNonEmpty(value)){
											pre = combinePre(cb, pre, cb.equal(fieldPath,value));
										}
									}else if(xCompare&&JpaMethod.like.equals(compareTp)){
										if(CommonUtility.isNonEmpty(value)){
											pre = combinePre(cb, pre, cb.like(fieldPath,"%"+value+"%"));
										}
									}else{
										if(CommonUtility.isNonEmpty(value)){
											pre = combinePre(cb, pre, cb.like(fieldPath,"%"+value+"%"));
										}
									}
									
								}else if(result.getClass().equals(Date.class)) {
									Date value = (Date) result;
									Path<Date> fieldPath = root.get(name);
									if(xCompare && JpaMethod.greaterThanEquals.equals(compareTp)){
										pre = combinePre(cb, pre, cb.greaterThan(fieldPath,value));
									}else{
										pre = combinePre(cb, pre, cb.equal(fieldPath,value));
									}
								}else if(result.getClass().equals(Integer.class)) {
									Integer value = (Integer) result;
									Path<Integer> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Long.class)) {
									Long value = (Long) result;
									Path<Long> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Short.class)) {
									Short value = (Short) result;
									Path<Short> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Float.class)) {
									Float value = (Float) result;
									Path<Float> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Double.class)) {
									Double value = (Double) result;
									Path<Double> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Boolean.class)) {
									Boolean value = (Boolean) result;
									Path<Boolean> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}else if(result.getClass().equals(Byte.class)) {
									Byte value = (Byte) result;
									Path<Byte> fieldPath = root.get(name);
									pre = combinePre(cb, pre, cb.equal(fieldPath,value));
								}
								if(null != clazzList){
									for(int m=0;m<clazzList.size();m++){
										if (result.getClass().equals(clazzList.get(m))) {
											// Object value = result;
											Path<Object> fieldPath = root.get(name);
											pre = combinePre(cb, pre,cb.equal(fieldPath, result));
										}
									}
								}
							}
							
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					// #########################################################
					// 日期范围
					// #########################################################
					String[] dateFields = (String[]) getNonBooleanFieldValue(t,"dateFields");
					if(dateFields != null && dateFields.length > 0) {
						String formatter = (String) getNonBooleanFieldValue(t, "formatter");
						String startTime = (String) getNonBooleanFieldValue(t, "startTime");
						String endTime = (String) getNonBooleanFieldValue(t, "endTime");
						logger.debug("[formatter] = " + formatter);
						logger.debug("[startTime] = " + startTime);
						logger.debug("[endTime] = " + endTime);
						//  修改bug #3163 start =============
						if(CommonUtility.isNonEmpty(formatter)
								&& (CommonUtility.isNonEmpty(startTime) || CommonUtility.isNonEmpty(endTime))
								){
							Date start = null;
							Date end = null;
							if(!CommonUtility.isNonEmpty(startTime)){
								Calendar _start=Calendar.getInstance();
								_start.set(1970, 0, 1);
								start=_start.getTime();
							}else{
								start = CommonUtility.parseDate(startTime, formatter);
							}
							
							if(!CommonUtility.isNonEmpty(endTime)){
								end=new Date();
							}else{
								end=CommonUtility.parseDate(endTime, formatter);
							}
							//  修改bug #3163 end =============
							for(String dateField : dateFields) {
								Path<Date> fieldPath = root.get(dateField);
								pre = combinePre(cb, pre, cb.between(fieldPath,start,getEndDate(end)));
							}
						}
					}
					
					// #########################################################
					// 自定义条件查询
					// #########################################################
					if(custom != null) {
						custom.execute(pre);
					}
					
					
					// #########################################################
					// 排序
					// #########################################################
					String sort = (String) getNonBooleanFieldValue(t, "sort");
					
					if(CommonUtility.isNonEmpty(sort)) {
						sortPath = root.get(sort);
					}
					
					// #########################################################
					// 升降序
					// #########################################################
					Order orderBy = null;
					String order = (String) getNonBooleanFieldValue(t, "order");
					if(CommonUtility.isNonEmpty(order) && sortPath != null) {
						if("desc".equals(order)) {
							orderBy = cb.desc(sortPath);
						}else if("asc".equals(order)) {
							orderBy = cb.asc(sortPath);
						}
						query.orderBy(orderBy);
					}
					if(andOr!=null){

						Predicate pre2 = null;

						String beanid ="id";
						if(cmethod!=null && cmethod.get(JpaMethod.beanId)!=null){
							beanid = cmethod.get(JpaMethod.beanId);
						}
						Subquery<T> query2 = (Subquery<T>) query.subquery( andOr.getClass());
						Root<?> root2 = query2.from(andOr.getClass());
						query2.select(root2.<T>get(beanid));
						Path<T> path2 = root.get(beanid);
						
						logger.debug("==> Find field.");
						if(andOr != null){
							Class<? extends Object> clazz2 = andOr.getClass();
							Field[] fields2 = getClassFields(clazz);
							for(Field field2 : fields2) {
								try {
									String name = field2.getName();
//									if("serialVersionUID".equals(name)
//											|| "sort".equals(name)
//											|| "order".equals(name)
//											) {
//										continue;
//									}
//									logger.debug("[field.getType()] = " + field.getType());
//									logger.debug("[field.getType().getName()] = " + field.getType().getName());
//									logger.debug("[field.getType().getSimpleName()] = " + field.getType().getSimpleName());
//									logger.debug("[field.getType().getClass()] = " + field.getType().getClass());
									Object result = getFieldValue(andOr, field2);
									
									if(result != null) {
										
										javax.persistence.Transient tran = field2.getAnnotation(javax.persistence.Transient.class);
										if(tran != null) {
											continue;
										}
										logger.debug("[name] = " + name);
										logger.debug("[value] = " + result);
										logger.debug("[sortPath] = " + sortPath);
										
										String typeName = field2.getType().getSimpleName();
										typeName = typeName.substring(0,1).toUpperCase() + typeName.substring(1);
										typeName = "java.lang." + typeName;
										try{
											if(result!=null){
												Class clazz2_2 = result.getClass();
												logger.debug("[class type] = " + clazz2_2.getName());
											}else{
												Class<?> type = Class.forName(typeName);
												logger.debug("[class type] = " + type);
											}
										}catch(java.lang.ClassNotFoundException e){
											e.printStackTrace();
										}
//										logger.debug("[clazz.newInstance() instanceof String] = " + (type.newInstance() instanceof String));
										logger.debug("result.equals(String[].class) = " + result.getClass().equals(String[].class));
										logger.debug("--------------");

										boolean xCompare = false;//not equal compare
										String compareTp = null;
										if(cmethod!=null && cmethod.get(name)!=null){
											xCompare = true ;
											compareTp = cmethod.get(name);
										}
										if(result.getClass().equals(String.class)) {
											String value = (String) result;
											Path<String> fieldPath = root2.get(name);
//											logger.debug("[fieldPath] = " + fieldPath);
											
											if(xCompare&&JpaMethod.equals.equals(compareTp)){
												if(CommonUtility.isNonEmpty(value)){
													pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
												}
											}else if(xCompare&&JpaMethod.like.equals(compareTp)){
												if(CommonUtility.isNonEmpty(value)){
													pre2 = combinePreWithOr(cb, pre2, cb.like(fieldPath,"%"+value+"%"));
												}
											}else{
												if(CommonUtility.isNonEmpty(value)){
													pre2 = combinePreWithOr(cb, pre2, cb.like(fieldPath,"%"+value+"%"));
												}
											}
											
										}else if(result.getClass().equals(Date.class)) {
											Date value = (Date) result;
											Path<Date> fieldPath = root2.get(name);
											if(xCompare && JpaMethod.greaterThanEquals.equals(compareTp)){
												pre2 = combinePreWithOr(cb, pre2, cb.greaterThan(fieldPath,value));
											}else{
												pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
											}
										}else if(result.getClass().equals(Integer.class)) {
											if(!"isdel".equals(name) && !"isDel".equals(name) ){

												Integer value = (Integer) result;
												Path<Integer> fieldPath = root2.get(name);
												pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
											}
										}else if(result.getClass().equals(Long.class)) {
											Long value = (Long) result;
											Path<Long> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}else if(result.getClass().equals(Short.class)) {
											Short value = (Short) result;
											Path<Short> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}else if(result.getClass().equals(Float.class)) {
											Float value = (Float) result;
											Path<Float> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}else if(result.getClass().equals(Double.class)) {
											Double value = (Double) result;
											Path<Double> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}else if(result.getClass().equals(Boolean.class)) {
											Boolean value = (Boolean) result;
											Path<Boolean> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}else if(result.getClass().equals(Byte.class)) {
											Byte value = (Byte) result;
											Path<Byte> fieldPath = root2.get(name);
											pre2 = combinePreWithOr(cb, pre2, cb.equal(fieldPath,value));
										}
										if(null != clazzList){
											for(int m=0;m<clazzList.size();m++){
												if (result.getClass().equals(clazzList.get(m))) {
													// Object value = result;
													Path<Object> fieldPath = root2.get(name);
													pre2 = combinePreWithOr(cb, pre2,cb.equal(fieldPath, result));
												}
											}
										}
									}
									
								}catch (Exception e) {
									e.printStackTrace();
								}
							}
							
							// #########################################################
							// 日期范围
							// #########################################################
							String[] dateFields2 = (String[]) getNonBooleanFieldValue(t,"dateFields");
							if(dateFields2 != null && dateFields2.length > 0) {
								String formatter = (String) getNonBooleanFieldValue(t, "formatter");
								String startTime = (String) getNonBooleanFieldValue(t, "startTime");
								String endTime = (String) getNonBooleanFieldValue(t, "endTime");
								logger.debug("[formatter] = " + formatter);
								logger.debug("[startTime] = " + startTime);
								logger.debug("[endTime] = " + endTime);
								//  修改bug #3163 start =============
								if(CommonUtility.isNonEmpty(formatter)
										&& (CommonUtility.isNonEmpty(startTime) || CommonUtility.isNonEmpty(endTime))
										){
									Date start = null;
									Date end = null;
									if(!CommonUtility.isNonEmpty(startTime)){
										Calendar _start=Calendar.getInstance();
										_start.set(1970, 0, 1);
										start=_start.getTime();
									}else{
										start = CommonUtility.parseDate(startTime, formatter);
									}
									
									if(!CommonUtility.isNonEmpty(endTime)){
										end=new Date();
									}else{
										end=CommonUtility.parseDate(endTime, formatter);
									}
									//  修改bug #3163 end =============
									for(String dateField : dateFields2) {
										Path<Date> fieldPath = root2.get(dateField);
										pre2 = combinePreWithOr(cb, pre2, cb.between(fieldPath,start,getEndDate(end)));
									}
								}
							}
							
							if(pre2 != null) {
								query2.where(pre2);
								pre = combinePre(cb, pre, cb.in(path2).value(query2));
							}
						}
					
					}
					if(pre != null) {
						query.where(pre);
					}
				}
				return pre;  
			}
		};
	}

	private static Predicate combinePre(CriteriaBuilder cb, Predicate pre,
			Predicate conditionPre) {
		if(pre == null) {
			pre = conditionPre;
		}else{
			pre=cb.and(pre,conditionPre);
		}
		return pre;
	}

	private static Predicate combinePreWithOr(CriteriaBuilder cb, Predicate pre,
			Predicate conditionPre) {
		if(pre == null) {
			pre = conditionPre;
		}else{
			pre=cb.or(pre,conditionPre);
		}
		return pre;
	}
	public static Field[] getClassFields(Class<?> clazz) {
		List<Field> list = new ArrayList<Field>();
		combineFields(clazz,list);
		return list.toArray(new Field[list.size() - 1]);
	}
	
	private static void combineFields(Class<?> clazz,List<Field> fieldList) {
//		logger.debug(fieldList.size());
		Field[] fields = clazz.getDeclaredFields();
		fieldList.addAll(Arrays.asList(fields));
		Class<?> superClazz = clazz.getSuperclass();
		if(superClazz != null) {
			combineFields(superClazz,fieldList);
		}
	}
	
	public static Field getClassField(Class<?> clazz,String fieldName) {
		Field[] fields = getClassFields(clazz);
		for(Field field : fields) {
			if(field.getName().equals(fieldName)){
				return field;
			}
 		}
		return null;
	}
	
	public static Object getNonBooleanFieldValue(Object modal,String fieldName) {
		if(fieldName != null) {
		char[] buffer = fieldName.toCharArray();
		buffer[0] = Character.toUpperCase(buffer[0]);
		String methodName = "get" + new String(buffer);
		Class<?> clazz = modal.getClass();
		Method method = getFieldMethod(clazz,methodName);
		if(method == null) {
			methodName = "is" +  new String(buffer);
			method = getFieldMethod(clazz,methodName);
		}
		if(method != null) {
			try {
				return method.invoke(modal);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
		return null;
	}
	
	public static Object getFieldValue(Object modal,Field field) {
		if(field != null) {
			String fieldName = field.getName();
			char[] buffer = fieldName.toCharArray();
			String typeName = field.getType().getSimpleName();
			typeName = typeName.substring(0,1).toUpperCase() + typeName.substring(1);
			buffer[0] = Character.toUpperCase(buffer[0]);
			String methodName = "";
			if("Boolean".equals(typeName)){
				if(fieldName.startsWith("is")) {
					if( buffer.length > 2
						&& Character.isUpperCase(buffer[2])) {
						methodName = fieldName;
					}
				}else{
					buffer[0] = Character.toUpperCase(buffer[0]);
					methodName = "is" + new String(buffer);
				}
			}else{
				methodName = "get" + new String(buffer);
			}
//			logger.debug("[methodName] = " + methodName);
			if("department".equals(methodName)){
				logger.info(methodName);
			}
			Class<?> clazz = modal.getClass();
			Method method = getFieldMethod(clazz,methodName);
			if(method != null) {
				try {
					return method.invoke(modal);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	private static Method getFieldMethod(Class<?> clazz,String methodName) {
		try {
			return clazz.getDeclaredMethod(methodName);
		} catch (NoSuchMethodException e) {
			Class<?> superClazz = clazz.getSuperclass();
			if(superClazz != null) {
				return getFieldMethod(superClazz,methodName);
			}
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static Date getEndDate(Date endDate) {
		String time = CommonUtility.formateDate(endDate);
		return CommonUtility.parseDate(time.split(" ")[0] + " 23:59:59");
		
	}
}

