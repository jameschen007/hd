package com.easycms.core.enums;

public enum SortEnum {
	/**
	 * 降序
	 */
	desc,
	/**
	 * 升序
	 */
	asc
}
