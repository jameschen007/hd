package com.easycms.core.strategy.authorization;


public interface Authorization<T> {

	/**
	 * 执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public T executeAuth(String username,String password, boolean isSession);
	
}
