package com.easycms.core.strategy.register;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindingResult;


public interface RegisterStrategy<T> {

	/**
	 * 执行认证方法
	 * @param username
	 * @param password
	 * @return
	 */
	public void execute(T form,BindingResult errors,HttpServletRequest request, HttpServletResponse response);
	
}
