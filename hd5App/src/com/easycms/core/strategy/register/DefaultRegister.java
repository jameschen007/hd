package com.easycms.core.strategy.register;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.UserForm;
import com.easycms.management.user.form.validator.UserFormValidator;
import com.easycms.management.user.service.UserService;

/**
 * 默认的注册方式
 * @author Administrator
 *
 */
public class DefaultRegister implements RegisterStrategy<UserForm> {
	
	private static final Logger logger = Logger.getLogger(DefaultRegister.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ModulesService modulesService ;
	
	@Override
	public void execute(UserForm form, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response) {
		logger.debug("==> Start save new user.");

		// 校验新增表单信息
		UserFormValidator validator = new UserFormValidator();
		validator.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			logger.debug("==> End save new user.");
			return;
		}
		// 校验通过，保存新增
		User user = new User();
		user.setName(form.getName());
		user.setPassword(form.getPassword());
		user.setEmail(form.getEmail());
		user.setRealname(form.getRealname());
		user.setRegisterIp(request.getRemoteAddr());
		user.setRegisterTime(new Date());
		Integer[] roleIds = form.getRoleId();
		logger.debug("[roleId] = " + CommonUtility.toJson(roleIds));
		List<Role> roleList = new ArrayList<Role>();
		if (roleIds != null) {
			for (Integer roleId : roleIds) {
				Role role = new Role();
				role.setId(roleId);
				// Role role = roleService.findById(roleId);
				roleList.add(role);
			}
		}
		user.setRoles(roleList);
		//每个新增的用户，将模块全部初始录入进去
		user.setModules(
		modulesService.findByAll());
		user = userService.add(user);
		
		
		HttpUtility.writeToClient(response,
				CommonUtility.toJson(user.getId() == null));
		logger.debug("==> End save new user.");
	}


}
