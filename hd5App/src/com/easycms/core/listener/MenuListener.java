package com.easycms.core.listener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.easycms.core.util.PrivilegeUtil;
import com.easycms.management.user.domain.Menu;
import com.easycms.management.user.domain.Privilege;
import com.easycms.management.user.service.MenuService;
import com.easycms.management.user.service.PrivilegeService;

public class MenuListener implements ServletContextListener {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory
			.getLogger(MenuListener.class);

	/**
	 * 工程启动时需要处理的事情
	 */
	@Override
	@Transactional
	public void contextInitialized(ServletContextEvent event) {
		ServletContext sc = event.getServletContext();
		ApplicationContext ac = WebApplicationContextUtils
				.getWebApplicationContext(sc);
		
		// #########################################################
		// 初始化权限
		// #########################################################
		PrivilegeService privilegeService = ac.getBean(PrivilegeService.class);
		
//		File[] confList = scanMenuXML(sc, ac);
		List<InputStream> inList = ListenerHelper.getInstance().scanMenuXML("easycms-privilege.xml");


		List<InputStream> _core = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_core.xml");
		if (_core != null && _core.size() > 0) {
			inList.addAll(_core);
		}
		List<InputStream> _baseService = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_baseService.xml");
		if (_baseService != null && _baseService.size() > 0) {
			inList.addAll(_baseService);
		}
		List<InputStream> _conference = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_conference.xml");
		if (_conference != null && _conference.size() > 0) {
			inList.addAll(_conference);
		}
		List<InputStream> _qualityctrl = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_qualityctrl.xml");
		if (_qualityctrl != null && _qualityctrl.size() > 0) {
			inList.addAll(_qualityctrl);
		}

		List<InputStream> _witness = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_witness.xml");
		if (_witness != null && _witness.size() > 0) {
			inList.addAll(_witness);
		}

		List<InputStream> _hse = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_hse.xml");
		if (_hse != null && _hse.size() > 0) {
			inList.addAll(_hse);
		}


		List<InputStream> _kpi = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_kpi.xml");
		if (_kpi != null && _kpi.size() > 0) {
			inList.addAll(_kpi);
		}

		List<InputStream> _flow = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_flow.xml");
		if (_flow != null && _flow.size() > 0) {
			inList.addAll(_flow);
		}

		List<InputStream> _learn = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_learn.xml");
		if (_learn != null && _learn.size() > 0) {
			inList.addAll(_learn);
		}
		List<InputStream> _problem = ListenerHelper.getInstance()
				.scanMenuXML("easycms-privilege_problem.xml");
		if (_problem != null && _problem.size() > 0) {
			inList.addAll(_problem);
		}

		if(inList != null && inList.size() > 0) {
			List<Privilege> menuList = new ArrayList<Privilege>();
			for(InputStream in : inList) {
//				List<Privilege> list = PrivilegeUtil.getPrivilegesFromConf(conf.getAbsolutePath());
				List<Privilege> list = PrivilegeUtil.getPrivilegesFromConf(in);
				menuList.addAll(list);
			}
			privilegeService.addIfNotExist(menuList);
		}
		
		// #########################################################
		// 初始化菜单
		// #########################################################
		MenuService menuService = ac.getBean(MenuService.class);
		List<Menu> menus = menuService.findAll();
		if(menus == null || menus.size() == 0) {
			Privilege condition = new Privilege();
			condition.setUri("/management/menu");
			List<Privilege> privileges = privilegeService.findAll(condition);
			for(Privilege p : privileges) {
				Menu menu = new Menu();
				menu.setName(p.getName());
				menu.setPrivilege(p);
				
				menuService.save(menu);
			}
		}
		
		logger.info("<<<<<<<<<<<<<<<<< setting menus successful <<<<<<<<<<<<<<<<<");

	}

	/**
	 * 工程关闭时需要处理的事情
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
//		CacheManager.getInstance().
	}

}
