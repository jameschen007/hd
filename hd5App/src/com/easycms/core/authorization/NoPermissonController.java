package com.easycms.core.authorization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.LoginForm;
import com.easycms.management.user.form.validator.LoginFormValidator;
import com.easycms.management.user.service.UserService;

@Controller
@RequestMapping("/nopermisson")
public class NoPermissonController {
	private static final Logger logger = Logger.getLogger(NoPermissonController.class);


	/**
	 * 无权限访问提示，不是老是被浏览器请求
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String view(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String returnString = null;
		String requestType = request.getHeader("X-Requested-With");
		logger.debug("[requestType] = " + requestType);
		returnString = ForwardUtility.forwardAdminView("404");
		return returnString;
	}
}
