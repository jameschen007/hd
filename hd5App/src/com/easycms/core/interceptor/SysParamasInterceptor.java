package com.easycms.core.interceptor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.easycms.core.util.CMSThemeUtil;
import com.easycms.management.settings.dao.SystemLogDao;
import com.easycms.management.settings.domain.SystemLog;

public class SysParamasInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		return true;
	}
	@Autowired
	private SystemLogDao systemLogDao;

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	    long startTime=System.currentTimeMillis();//记录开始时间  
		SystemLog t = new SystemLog();


		//SESSION id
		request.setAttribute("session_id", request.getSession().getId());
		
		//当前URI
		String basePath = request.getContextPath();
		String uri = request.getRequestURI();
		uri = uri.substring(basePath.length(), uri.length());
		request.setAttribute("current_uri", uri);
		t.setUri(uri);
		
		//ID选择器
		if(uri.startsWith("/")) {
			uri = uri.substring(1,uri.length());
		}
		String[] array = uri.split("/");
		StringBuffer sb = new StringBuffer();
		for(int i = 0;i<array.length;i++) {
			if(StringUtils.isNotEmpty(array[i])) {
				if(i == 0) {
					sb.append(StringUtils.uncapitalize(array[i]));
				}else{
					sb.append(StringUtils.capitalize(array[i]));
				}
			}
		}
		request.setAttribute("selector", sb.toString());
		//上下文路径
		request.setAttribute("path", request.getContextPath());
		
		//时间戳
		request.setAttribute("timestamp", new Date().getTime());
		
		//主题资源路径
		request.setAttribute("res", CMSThemeUtil.getResPath());

		try{

			super.postHandle(request, response, handler, modelAndView);
		}catch(Exception e){
		    t.setWithTime(new BigDecimal("500.00"));
			systemLogDao.save(t);
			
			throw e;
		}
		

	    long endTime=System.currentTimeMillis();//记录结束时间  
	    t.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
	    if(t.getWithTime().floatValue()>0.3){
			systemLogDao.save(t);
	    }
	}
}
