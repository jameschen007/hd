package com.easycms.core.filter;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.easycms.common.logic.context.ContextPath;

/**
 * Servlet Filter implementation class CharactorFilter
 */
public class SpringCharactorFilter extends CharacterEncodingFilter {

	private static Logger logger = Logger.getLogger(SpringCharactorFilter.class);
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		//麻烦你帮个忙初始化一下访问上下文
		HttpServletRequest req = (HttpServletRequest)request;
		if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath==null){
//			getRequestURL()-getRequestURI()+getContextPath()
	 		logger.info("获得本机ip="+ContextPath.hostAddress);
			com.easycms.common.logic.context.ContextPath.selfProjectContextPath = req.getRequestURL().substring(0,req.getRequestURL().indexOf(req.getContextPath())+req.getContextPath().length())+"/";
			logger.info("selfProjectContextPath="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath);
			if( req.getContextPath()==null || "".equals( req.getContextPath().trim())){
				String u = req.getRequestURL().toString();
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath = u.substring(0,u.indexOf("/", 9)+1);
			}
			logger.info("selfProjectContextPath="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath);
			if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath.indexOf("localhost")!=-1){
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath = 
						com.easycms.common.logic.context.ContextPath.selfProjectContextPath.replace("localhost", ContextPath.hostAddress);
			}
			logger.info("selfProjectContextPath="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath);
			logger.info("wangz获取的selfProjectContextPath="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath+":8080");
			if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath.indexOf("127.0.0.1")!=-1){
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath = 
						com.easycms.common.logic.context.ContextPath.selfProjectContextPath.replace("127.0.0.1", ContextPath.hostAddress);
			}
		}
		//将文件路径加载
		if(com.easycms.common.logic.context.ContextPath.fileBasePath==null){
	        InputStream in = this.getClass().getResourceAsStream("/api.properties");  
	        Properties p = new Properties();
	        try {  
	            p.load(in);
	            com.easycms.common.logic.context.ContextPath.fileBasePath = p.getProperty("file_base_path");
	            com.easycms.common.logic.context.ContextPath.winfileBasePath = p.getProperty("windows_file_base_path");
				if( false == com.easycms.common.logic.context.ContextPath.Linux.equals(com.easycms.common.logic.context.ContextPath.os_name)){
					com.easycms.common.logic.context.ContextPath.fileBasePath = com.easycms.common.logic.context.ContextPath.winfileBasePath ;
				}
	        } catch (IOException e) {  
	            // TODO Auto-generated catch block  
	            e.printStackTrace();  
	        }  
		}
		
		super.doFilterInternal(new MyHttpServletRequest(request), response, filterChain);
	}
	

	private static class MyHttpServletRequest extends HttpServletRequestWrapper {
		private HttpServletRequest request;
		private String encode = "utf-8";
		
		public MyHttpServletRequest(HttpServletRequest request) {
			super(request);
			this.request = request;
//			this.encode = request.getCharacterEncoding();
		}

		@Override
		public String getParameter(String name) {
			// 对GET请求参数重新编码
			 String value = request.getParameter(name);
//			 System.out.println("SpringCharactorFilter before value:" + value);
//			 System.out.println("SpringCharactorFilter encoding:" + request.getCharacterEncoding());
//			 System.out.println("SpringCharactorFilter method:" + request.getMethod());
//			 System.out.println(encode);
			 
			 //app的安全模块的右上角搜索功能GET进来的中文传参，原本是正常的，在这里被转换为?了，注释取消
			try {
				if (value != null && "GET".equals(request.getMethod())) {
					value = new String(value.getBytes("iso-8859-1"),
							encode);
					logger.info("SpringCharactorFilter after value:" + value);
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return value;
		}

		@Override
		public String[] getParameterValues(String name) {
			String[] values = request.getParameterValues(name);
			List<String> list = new ArrayList<String>();
//			System.out.println(encode);
			
//			Object url = request.getRequestURL();
//			
//			String path = request.getContextPath();
//			logger.info("url"+url);
//			logger.info("path"+path);
			
			try {
//				 System.out.println(values.length);
//				 System.out.println("getParameterValues ==>" + request.getMethod());
				 //app的安全模块的右上角搜索功能GET进来的中文传参，原本是正常的，在这里被转换为?了，注释取消
				if (false && values != null && values.length > 0 && "GET".equals(request.getMethod())) {
					for (String value : values) {
//						 System.out.println("before value:" + value);
						value = new String(value.getBytes("iso-8859-1"),
								encode);
//						 System.out.println("after value:" + value);
						list.add(value);
					}
					values = list.toArray(new String[values.length]);
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return values;
		}

	}
	
}
