package com.easycms.hd.materialtype.service.imp;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.materialtype.dao.MaterialTypeDao;
import com.easycms.hd.materialtype.domain.MaterialType;
import com.easycms.hd.materialtype.service.MaterialTypeService;
import com.easycms.management.user.domain.User;
/**
 * 
 * @author zhangjian
 *
 */
@Service
public class MaterialTypeServiceImp implements MaterialTypeService {
	private Logger logger = Logger.getLogger(MaterialTypeServiceImp.class);
	@Autowired
	private MaterialTypeDao materialTypeDao;
	
	@Override
	public List<MaterialType> findAll() {
		return materialTypeDao.findAll();
	}

	@Override
	public MaterialType findById(Integer id) {
		return materialTypeDao.findById(id);
	}

	@Override
	public MaterialType add(MaterialType mt) {
		return materialTypeDao.save(mt);
	}

	@Override
	public Integer deleteById(Integer id) {
		return materialTypeDao.deleteById(id);
	}

	@Override
	public MaterialType update(MaterialType mt) {
		return materialTypeDao.save(mt);
	}

	@Override
	public Page<MaterialType> findPage(MaterialType condition,
			Page<MaterialType> page) {
		logger.debug("==> Find all MaterialType by condition.");
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<MaterialType> springPage = materialTypeDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<MaterialType> findAll(MaterialType condition) {
		return materialTypeDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public boolean materialTypeExists(String materialTypeId) {
		return materialTypeDao.exsits(materialTypeId) != null;
	}

	@Override
	public Integer delete(Integer[] ids) {
		return materialTypeDao.deleteByIds(ids);
		
	}
	
	
	
}
