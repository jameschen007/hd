package com.easycms.hd.materialtype.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.materialtype.domain.MaterialType;
import com.easycms.hd.materialtype.form.MaterialTypeForm;
import com.easycms.hd.materialtype.service.MaterialTypeService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.form.UserForm;

/**
 * 
 * @author zhangjian
 *
 */
@Controller
@RequestMapping("/baseservice")
public class MaterialTypeController {
	/** 
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MaterialTypeController.class);
	
	@Autowired
	private MaterialTypeService materialTypeService;

	@RequestMapping(value="/material_type",method = RequestMethod.GET)
	public String list(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("form") MaterialTypeForm form){
		logger.debug("request material type list data");
		
		String func = form.getFunc();
		logger.debug("[func] = " + func);
		// #########################################################
		// 校验用户是否存在
		// #########################################################
		if ("edit".equalsIgnoreCase(func)) {
			logger.debug("==> Valid materialTypeId is exist or not.");
			boolean avalible = false;
			avalible = !materialTypeService.materialTypeExists(form.getMaterialTypeId());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;

		}
		
		if ("exist".equalsIgnoreCase(func)) {
			logger.debug("==> Valid materialTypeId is exist or not.");
			boolean avalible = false;
			avalible = !materialTypeService.materialTypeExists(form.getMaterialTypeId());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;

		}
		return ForwardUtility.forwardAdminView("/hdService/materialType/list_material_type");
		
	}
	
	@RequestMapping(value="/material_type",method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("form") MaterialTypeForm form){
		logger.debug("request material type list page data");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/materialType/data/data_json_material_type");
		return returnString;
	}
	
	@RequestMapping(value="/material_type/edit",method = RequestMethod.GET)
	public String editUI(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("form") MaterialTypeForm form){
		logger.debug("request material type management edit ");
		return ForwardUtility.forwardAdminView("/hdService/materialType/modal_material_type_edit");
	}
	
	@RequestMapping(value="/material_type/edit",method = RequestMethod.POST)
	public void edit(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("form") MaterialTypeForm form){
		logger.debug("request material management  edit data ");
		if (form.getMaterialTypeId() == null) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save eidt material type failed.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		// #########################################################
		// 校验通过
		// #########################################################
		MaterialType materialType = new MaterialType();
		materialType.setId(form.getId());
		materialType.setMaterialTypeId(form.getMaterialTypeId());
		materialType.setMaterialTypeName(form.getMaterialTypeName());
		materialType.setUpdatedBy(loginUser.getName());
		materialType.setUpdatedOn(new Date());
		materialTypeService.update(materialType);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save eidt material type.");
		return;
	}
	
	@RequestMapping(value="/material_type/add",method = RequestMethod.GET)
	public String addUI(HttpServletRequest request,HttpServletResponse response){
		logger.debug("request material management add");
		return ForwardUtility.forwardAdminView("/hdService/materialType/modal_material_type_add");
	}
	
	@RequestMapping(value="/material_type/add",method = RequestMethod.POST)
	public void add(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("form") MaterialTypeForm form){
		logger.debug("request material management  add data ");
		if (form.getMaterialTypeId() == null) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save eidt material type failed.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		// #########################################################
		// 校验通过
		// #########################################################
		MaterialType materialType = new MaterialType();
		materialType.setMaterialTypeId(form.getMaterialTypeId());
		materialType.setMaterialTypeName(form.getMaterialTypeName());
		materialType.setCreatedBy(loginUser.getName());
		materialType.setCreatedOn(new Date());
		materialTypeService.add(materialType);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save eidt material type.");
		return;
	}
	
	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/material_type/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") MaterialTypeForm form) {
		logger.debug("==> Start delete material type.");

		Integer id = form.getId();
		if(id != null && id!=0){
			logger.debug("==> To delete material type[" + id + "]");
			materialTypeService.deleteById(id);
			HttpUtility.writeToClient(response, "true");
			return;
		}
		
		Integer[] ids = form.getIds();
		logger.debug("==> To delete material type[" + CommonUtility.toJson(ids) + "]");
		if (ids != null && ids.length > 0) {
			materialTypeService.delete(ids);
			HttpUtility.writeToClient(response, "true");
		}
		logger.debug("==> End delete material type.");
	}
}
