package com.easycms.hd.materialtype.dao;



import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.materialtype.domain.MaterialType;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface MaterialTypeDao extends BasicDao<MaterialType>,Repository<MaterialType,Integer>{
	
	
	
	/**
	 * 删除材质类型
	 * 
	 * @param materialTypeId
	 */
	@Modifying
	@Query("DELETE FROM MaterialType m WHERE m.id  = ?1")
	int deleteById(Integer id);

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM MaterialType m WHERE m.id  IN (?1) ")
	int deleteByIds(Integer[] ids);
	

	@Query("FROM MaterialType m WHERE m.materialTypeId = ?1")
	MaterialType exsits(String materialTypeId);

}
