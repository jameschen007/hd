package com.easycms.hd.exam.enums;

public enum ExamPagerItemEnum {
	SINGLE("单项选择"),
	MULTIPLE("多项选择"),
	YES_OR_NO("判断");
	
	private String name;

	private ExamPagerItemEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
