package com.easycms.hd.exam.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.exam.domain.ExamUserScore;

public interface ExamUserScoreDao extends Repository<ExamUserScore,Integer>,BasicDao<ExamUserScore>{
	
	@Query("FROM ExamUserScore e where e.userId = ?1")
	Page<ExamUserScore> findByUser(Integer userId, Pageable pageable);
	
	@Query("FROM ExamUserScore e where e.examStatus = ?1")
	Page<ExamUserScore> findByExamStatus(Pageable pageable);
	
	@Query("FROM ExamUserScore e where e.userId = ?1 AND e.examStatus = ?2")
	List<ExamUserScore> findByUserAndExamStatus(Integer userId,String examStatus);

	@Query("FROM ExamUserScore e where e.userId = ?1 AND e.examStatus = ?2")
	Page<ExamUserScore> findByUserAndExamStatus(Integer userId,String examStatus,Pageable pageable);
	@Query("FROM ExamUserScore e where e.examStatus = ?1 order by id desc")
	Page<ExamUserScore> findByExamStatus(String examStatus,Pageable pageable);
	@Query("FROM ExamUserScore e where e.examStatus = ?1")
	List<ExamUserScore> findByUserAndExamStatus(String examStatus);

}
