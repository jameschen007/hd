package com.easycms.hd.exam.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.exam.domain.ExamPaper;
import com.easycms.hd.exam.service.ExamPaperService;
import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-28
 */
@Slf4j
public class ExamDirective extends BaseDirective<ExamPaper>  {
	
	@Autowired
	private ExamPaperService examPaperService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected ExamPaper field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			ExamPaper examPaper = examPaperService.findById(id);
			return examPaper;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExamPaper> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<ExamPaper> page,
			Map<String, Object> envParams) {
		String section = FreemarkerTemplateUtility.getStringValueFromParams(params, "section");
		String module = FreemarkerTemplateUtility.getStringValueFromParams(params, "module");
		
		log.debug("Section : " + section + " ,Module: " + module);
		
		if (CommonUtility.isNonEmpty(module)) {
			page = examPaperService.findByModuleAndSection(module, section, page);
		} else {
			page = examPaperService.findBySection(section, page);
		}
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExamPaper> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
