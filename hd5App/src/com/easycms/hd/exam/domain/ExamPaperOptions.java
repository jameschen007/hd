package com.easycms.hd.exam.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 试题的选项
 * @author ChenYuMei-Refiny
 *
 */
@Entity
@Table(name = "exam_paper_options")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class ExamPaperOptions extends BasicForm implements Serializable {
	private static final long serialVersionUID = 3431371661094204461L;
	/**
	 * 选项唯一ID
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	/**
	 * 选项类型
	 */
	@Column(name = "type")
	private String type;
	/**
	 * 选项内容
	 */
	@Column(name = "content")
	private String content;
	/**
	 * 选项标号
	 */
	@Column(name = "number")
	private String number;
	/**
	 * 选项标号
	 */
	@Column(name = "correct")
	private String correct;
	
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
	
	/**
	 * 试题题目
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "paper_id", nullable = false)
	private ExamPaper examPaper;
}
