package com.easycms.hd.exam.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * 试题列表
 * @author ChenYuMei-Refiny
 *
 */
@Entity
@Table(name = "exam_paper")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class ExamPaper extends BasicForm implements Serializable {
	private static final long serialVersionUID = 2922567066593561951L;
	/**
	 * 唯一ID
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	/**
	 * 试题类型
	 */
	@Column(name = "number")
	private String number;
	/**
	 * 试题类型
	 */
	@Column(name = "type")
	private String type;
	/**
	 * 试题标题
	 */
	@Column(name = "subject")
	private String subject;
	/**
	 * 试题分数
	 */
	@Column(name = "score")
	private Double score;
	/**
	 * 试题状态
	 */
	@Column(name = "status")
	private String status;
	/**
	 * 所属版块
	 */
	@Column(name = "section")
	private String section;
	/**
	 * 所属模块
	 */
	@Column(name = "module")
	private String module;
	/**
	 * 所属子项
	 */
	@Column(name = "fold")
	private Integer fold;

	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
	
	/**
	 * 试题选项
	 */
	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "examPaper")
	private List<ExamPaperOptions> examPaperOptions;
}
