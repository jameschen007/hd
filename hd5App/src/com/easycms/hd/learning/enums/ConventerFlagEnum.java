package com.easycms.hd.learning.enums;

public enum ConventerFlagEnum {
	START("开始转换"),
	FAILED("失败"),
	COMPLETED("完成");
	
	private String name;

	private ConventerFlagEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
