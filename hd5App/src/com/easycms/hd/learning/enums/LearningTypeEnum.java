package com.easycms.hd.learning.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycms.hd.response.CommonMapResult;

public enum LearningTypeEnum {
	IMAGE_TEXT("图文"),
	VIDEO("视频");
	
	private String name;

	private LearningTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
    /**
     * enum lookup map
     */
    private static final Map<String, String> lookup = new HashMap<String, String>();
    /**
     * enum lookup list
     */
    private static final List<CommonMapResult> mapResult = new ArrayList<CommonMapResult>();

    static {
        for (LearningTypeEnum s : EnumSet.allOf(LearningTypeEnum.class)) {
            lookup.put(s.name(), s.getName());
            CommonMapResult commonMapResult = new CommonMapResult();
            commonMapResult.setKey(s.name());
            commonMapResult.setValue(s.getName());
            mapResult.add(commonMapResult);
        }
    }

    public static  Map<String, String> getMap(){
        return lookup;
    }
    
    public static  List<CommonMapResult> getList(){
    	return mapResult;
    }
}
