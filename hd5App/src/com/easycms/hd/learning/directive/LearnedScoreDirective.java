package com.easycms.hd.learning.directive;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.easycms.basic.BaseDirective;
import com.easycms.core.util.Page;
import com.easycms.hd.api.exam.response.ExamScoreResult;
import com.easycms.hd.api.exam.response.ExamUserScoreResult;
import com.easycms.hd.exam.domain.ExamUserScore;
import com.easycms.hd.exam.enums.ExamStatus;
import com.easycms.hd.exam.service.ExamUserScoreService;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearnedHistoryService;
import com.easycms.hd.learning.service.LearningFileService;
import com.easycms.hd.learning.service.LearningSectionService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 获取学习成绩列表
 * 
 * @author wz: 
 * @areate Date:2018 2 26 
 */
@Slf4j
public class LearnedScoreDirective extends BaseDirective<ExamUserScoreResult>  {
	
	@Autowired
	private LearnedHistoryService LearnedHistoryService;
	@Autowired
	private LearningFileService learningFileService;
	@Autowired
	private UserService userService;
	@Autowired
	private ModulesService modulesService;
	@Autowired
	private LearningSectionService learningSectionService;
	
	@Value("#{APP_SETTING['file_learning_path']}")
	private String fileLearningPath;
	
	@Value("#{APP_SETTING['file_learning_convert_path']}")
	private String fileLearningConvertPath;
	
	@Value("#{APP_SETTING['file_learning_cover_path']}")
	private String fileLearningCoverPath;

	@Autowired
	private ExamUserScoreService examUserScoreService;
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}
	protected ExamUserScoreResult field(Map params, Map<String, Object> envParams) {
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExamUserScoreResult> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<ExamUserScoreResult> page,
			Map<String, Object> envParams) {
		ExamScoreResult examScoreResult = new ExamScoreResult();
		Page<ExamUserScore> pager = new Page<ExamUserScore>();
		
		pager.setDefaultStartIndex(page.getDefaultStartIndex());
		pager.setEndPage(page.getEndPage());
		pager.setPageCount(page.getPageCount());
		pager.setPageNum(page.getPageNum());
		pager.setPagesize(page.getPagesize());
		pager.setStartIndex(page.getStartIndex());
		pager.setStartPage(page.getStartPage());

		pager = examUserScoreService.findByExamStatus(ExamStatus.COMPLETED.toString(), pager);
		
		examScoreResult.setPageCounts(pager.getPageCount());
		examScoreResult.setPageNum(pager.getPageNum());
		examScoreResult.setPageSize(pager.getPagesize());
		examScoreResult.setTotalCounts(pager.getTotalCounts());
		
		

		page.setDefaultStartIndex(pager.getDefaultStartIndex());
		page.setEndPage(pager.getEndPage());
		page.setPageCount(pager.getPageCount());
		page.setPageNum(pager.getPageNum());
		page.setPagesize(pager.getPagesize());
		page.setStartIndex(pager.getStartIndex());
		page.setStartPage(pager.getStartPage());
		page.setTotalCounts(pager.getTotalCounts());
		
		
		if (null != pager.getDatas()) {
			examScoreResult.setData(pager.getDatas().stream().map(exam -> {
				ExamUserScoreResult result = new ExamUserScoreResult();
				result.setExamDate(exam.getExamStartDate());
				
				//返回考试类型
				String type = null;
				Modules module = null;
				LearningSection learningSection = null;
				if(exam.getLearningFold()!=null){
					if(exam.getLearningFold().getModule()!=null){
						module = modulesService.findByType(exam.getLearningFold().getModule());
						type = module.getName();
					}else if(exam.getLearningFold().getSection()!=null){
						learningSection = learningSectionService.findByType(exam.getLearningFold().getSection());
					}
				}else if(exam.getExamType() != null){
					learningSection = learningSectionService.findByType(exam.getExamType());
				}
				if(learningSection != null){
					type = learningSection.getName();
				}else if(module != null){
					type = module.getName();
				}
				result.setExamType(type);
				result.setRealname(userService.findRealnameById(exam.getUserId()));

				result.setExamScore(String.valueOf(exam.getExamScore()));
				result.setExamResult(exam.getExamResult());
				return result;
			}).collect(Collectors.toList()));
		}
		
		return examScoreResult.getData();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<ExamUserScoreResult> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
