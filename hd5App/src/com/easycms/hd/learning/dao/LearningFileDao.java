package com.easycms.hd.learning.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.learning.domain.LearningFile;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface LearningFileDao extends Repository<LearningFile,Integer>,BasicDao<LearningFile>{
	@Query("From LearningFile lf WHERE lf.learningId = ?1")
	List<LearningFile> findByLearningId(Integer id);

	@Modifying
	@Query("DELETE FROM LearningFile lf WHERE lf.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM LearningFile lf WHERE lf.learningId IN (?1)")
	int batchRemoveLearning(Integer[] ids);

	@Query("From LearningFile lf WHERE lf.learningId IN (?1)")
	List<LearningFile> findFilesByLearningIds(Integer[] ids);

	@Modifying
	@Query("UPDATE LearningFile lf SET lf.learningId = ?2 WHERE lf.id IN (?1)")
	int batchUpdateLearning(Integer[] ids, Integer LearningId);
}
