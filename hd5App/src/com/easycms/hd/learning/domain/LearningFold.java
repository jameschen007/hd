package com.easycms.hd.learning.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 每个模块或者条目下的细分小类别，比如，基础培训下的入场质保
 * @author fangwei.ren
 *
 */
@Entity
@Table(name = "learning_fold")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class LearningFold extends BasicForm implements Serializable {
	private static final long serialVersionUID = 6014102300509526060L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "description", length = 500)
	private String description;
	@Column(name = "type", length = 50)
	private String type;
	@Column(name = "name", length = 50)
	private String name;
	@JsonIgnore
	@Column(name = "module", length = 10)
	private String module;
	@Column(name = "section", length = 10)
	private String section;
	@Column(name = "status", length = 10)
	private String status;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by")
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by")
	private Integer updateBy;
}