package com.easycms.hd.learning.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 培训管理中的历史记录信息
 * @author fangwei.ren
 *
 */
@Entity
@Table(name = "learning_history")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class LearnedHistory extends BasicForm implements Serializable {
	private static final long serialVersionUID = -2742233147442412233L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "user_id")
	private Integer userId;
	@Transient
	private String userName;
	
	/**
	 * 课程
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "learned_id",nullable = false)
	private Learning learning;
	
	/**
	 * 开始学习时间
	 */
	@Column(name = "start_date")
	private Date startDate;	
	
	/**
	 * 结束学习时间
	 */
	@Column(name = "end_date")
	private Date endDate;
	
	/**
	 * 学习时间
	 */
	@Column(name = "duration")
	private String duration;
	
	/**
	 * 唯一编码
	 */
	@Column(name = "unique_code")
	private String uniqueCode;
	
	@JsonIgnore
	@Column(name = "status", length = 10)
	private String status;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	
	@JsonIgnore
	@Column(name = "created_by")
	private Integer createBy;
}