package com.easycms.hd.learning.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearningSection;

public interface LearningSectionService {
	Page<LearningSection> findByPage(Page<LearningSection> page);

	List<LearningSection> findByAll();

	List<LearningSection> findByStatus(String status);

	LearningSection findByType(String type);

	LearningSection findById(Integer id);

	LearningSection add(LearningSection learningSection);

	boolean batchDelete(Integer[] ids);
}
