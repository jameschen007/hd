package com.easycms.hd.learning.service;

import java.util.List;

import com.easycms.hd.learning.domain.LearningFile;

public interface LearningFileService {
	LearningFile add(LearningFile pfile);
	
	LearningFile findById(Integer id);
	
	List<LearningFile> findFilesByLearningId(Integer id);

	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveLearning(Integer[] ids);
	
	boolean batchUpdateLearning(Integer[] ids, Integer learningId);

	List<LearningFile> findFilesByLearningIds(Integer[] ids);
}
