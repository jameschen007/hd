package com.easycms.hd.learning.service.impl;


import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.poi.excel.ExcelColumn;
import com.easycms.common.poi.excel.ExcelHead;
import com.easycms.common.poi.excel.ExcelHelperV2;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.learning.dao.LearnedHistoryDao;
import com.easycms.hd.learning.domain.LearnedHistory;
import com.easycms.hd.learning.service.LearnedHistoryService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;


/**
 * @author Mao.Zeng@MG
 *
 */
@Service("learnedHistoryService")
public class LearnedHistoryServiceImpl implements LearnedHistoryService {
	
	private static Logger log = Logger.getLogger(LearnedHistoryServiceImpl.class);

	@Autowired
	private LearnedHistoryDao learningHistoryDao;
	@Autowired
	private UserDao userDao;

	@Override
	public List<LearnedHistory> findByAll() {

		List<LearnedHistory> list = learningHistoryDao.findAll();

		if(list!=null){
			list.stream().forEach(b->{
				User user = userDao.findById(b.getUserId());
				if(user!=null){
					b.setUserName(user.getRealname());
				}
			});
		}
		return list;
	}

	@Override
	public List<LearnedHistory> findByStatus(String status) {
		return learningHistoryDao.findByStatus("ACTIVE");
	}

	@Override
	public LearnedHistory findById(Integer id) {
		return learningHistoryDao.findById(id);
	}

	@Override
	public LearnedHistory add(LearnedHistory learnedHistory) {
		return learningHistoryDao.save(learnedHistory);
	}

	@Override
	public LearnedHistory findByUniqueCode(String uniqueCode) {
		return learningHistoryDao.findByUniqueCode(uniqueCode);
	}

	@Override
	public Page<LearnedHistory> findByUser(Integer userId,Page<LearnedHistory> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<LearnedHistory> springPage = learningHistoryDao.findByUser(userId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<LearnedHistory> findPage(LearnedHistory condition,Page<LearnedHistory> page){
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<LearnedHistory> springPage = null;
		springPage = learningHistoryDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	/**
	 * 在线学习列表导出
	 */
	@Override
	public void statisticalReport(HttpServletRequest request, HttpServletResponse response) {

		try{
	        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
	        f.setTimeZone(TimeZone.getTimeZone("GMT+0"));
	        Date t = Calendar.getInstance().getTime();
			List<LearnedHistory> qualityControls = findByAll() ;
			for(int i=0;i<qualityControls.size();i++){
				LearnedHistory b = qualityControls.get(i);
				b.setId(i+1);
				User user = userDao.findById(b.getUserId());
				if(user!=null){
					b.setUserName(user.getRealname());
				}
				String d = b.getDuration();
				if(d!=null && d.matches("\\d+")){
			        t.setTime(Long.valueOf(d));
					b.setDuration(f.format(t));
				}
			}
			//获取模板文件
			String tempFlieName = "learnedTemplate.xls";
	    	InputStream in = this.getClass().getClassLoader().getResourceAsStream(tempFlieName);
			
			//设置Excel每列显示内容
			List<ExcelColumn> columns = new ArrayList<ExcelColumn>();
			

			columns.add(new ExcelColumn(0,"id","序号"));
			columns.add(new ExcelColumn(1,"userName","姓名"));
			columns.add(new ExcelColumn(2,"startDate","开始时间"));
			columns.add(new ExcelColumn(3,"endDate","结束时间"));
			columns.add(new ExcelColumn(4,"duration","时长(分钟)"));
			
			ExcelHead head = new ExcelHead();
			head.setColumns(columns);
			head.setColumnCount(columns.size());
			head.setRowCount(2);

			//写入数据
			ExcelHelperV2.getInstanse().exportExcelFile(head, in, response.getOutputStream(), qualityControls,"yyyy-MM-dd HH:mm:ss");
		}catch(Exception e){
			log.error("数据写入文件出错！"+e.getMessage());
			e.printStackTrace();
		}
	
		
	}
	
}
