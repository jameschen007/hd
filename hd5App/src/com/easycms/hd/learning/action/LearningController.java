package com.easycms.hd.learning.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.service.ExamApiService;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.service.LearnedHistoryService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/learning")
@Slf4j
public class LearningController {
	@Autowired
	private LearnedHistoryService learnedHistoryService;
	@Autowired
	private ExamApiService examApiService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("[Form] = " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/learning/page_learning");
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String showPagePost(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("==> Show learning data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_html_learning");
		return returnString;
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String showDetail(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("[Form] = " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/learning/page_learning_detail");
	}

/**
 * 在线学习列表
 * @param request
 * @param response
 * @param form
 * @return
 * @throws Exception
 */
	@RequestMapping(value = "/hisList", method = RequestMethod.GET)
	public String hisListUI(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("[Form] = " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/learning/page_learning_hisList");
	}

	/**
	 * 在线学习列表
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/hisList", method = RequestMethod.POST)
	public String hisList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("==> Show learning data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_html_learning_hisList");
		return returnString;
	}

	@RequestMapping(value = "/hisExport", method = RequestMethod.GET)
	public void hisExportUI(HttpServletRequest request, HttpServletResponse response){
    	response.setHeader("Content-Disposition", "attachment");
    	HttpUtility.writeToClient(response, "true");
	}
	/**
	 * 在线学习列表导出
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/hisExport", method = RequestMethod.POST)
	public void hisExport(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		String mimetype = "application/vnd.ms-excel;charset=utf-8";
    	response.setContentType(mimetype);
    	String filename = "参培人员学习记录";
    	filename += ".xls";
    	String headerValue = "attachment;";
    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
    	response.setHeader("Content-Disposition", headerValue);
    	learnedHistoryService.statisticalReport(request, response);
    	
	}

/**
 * 成绩列表
 * @param request
 * @param response
 * @param form
 * @return
 * @throws Exception
 */
	@RequestMapping(value = "/scoreList", method = RequestMethod.GET)
	public String scoreListUI(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("[Form] = " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/learning/page_learning_scoreList");
	}

	/**
	 * 成绩列表
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/scoreList", method = RequestMethod.POST)
	public String scoreList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		log.debug("==> Show learning data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_html_learning_scoreList");
		return returnString;
	}

	@RequestMapping(value = "/scoreExport", method = RequestMethod.GET)
	public void scoreExportUI(HttpServletRequest request, HttpServletResponse response){
    	response.setHeader("Content-Disposition", "attachment");
    	HttpUtility.writeToClient(response, "true");
	}
	/**
	 * 成绩列表导出
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/scoreExport", method = RequestMethod.POST)
	public void scoreExport(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") Learning form) throws Exception {
		String mimetype = "application/vnd.ms-excel;charset=utf-8";
    	response.setContentType(mimetype);
    	String filename = "参培人员成绩记录";
    	filename += ".xls";
    	String headerValue = "attachment;";
    	headerValue += " filename=\"" + WebUtility.encodeURIComponent(filename) +"\";";
    	headerValue += " filename*=utf-8''" + WebUtility.encodeURIComponent(filename);
    	response.setHeader("Content-Disposition", headerValue);
    	examApiService.statisticalReport(request, response);
    	
	}
	
	
}
