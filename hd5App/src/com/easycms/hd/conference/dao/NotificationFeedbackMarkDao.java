package com.easycms.hd.conference.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.NotificationFeedbackMark;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface NotificationFeedbackMarkDao extends Repository<NotificationFeedbackMark,Integer>,BasicDao<NotificationFeedbackMark>{
	@Modifying
	@Query("UPDATE NotificationFeedbackMark nfm SET nfm.markTime = ?2 where nfm.id = ?1") 
	int markRead(Integer markId, Date markTime);

	@Query("FROM NotificationFeedbackMark nfm where nfm.notificationid = ?1 AND nfm.userId = ?2")
	NotificationFeedbackMark findByNotificationIdAndUserId(Integer notificationId, Integer userId);
	
	@Modifying
	@Query("DELETE FROM NotificationFeedbackMark nfm where nfm.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM NotificationFeedbackMark nfm where nfm.notificationid IN (?1)")
	int batchRemoveNotification(Integer[] ids);
}
