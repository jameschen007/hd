package com.easycms.hd.conference.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.NotificationFeedback;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface NotificationFeedbackDao extends Repository<NotificationFeedback,Integer>,BasicDao<NotificationFeedback>{
	@Query("From NotificationFeedback nf where nf.notificationid = ?1 order by nf.createOn")
	List<NotificationFeedback> findByNotificationId(Integer id);
	
	@Modifying
	@Query("DELETE FROM NotificationFeedback nf where nf.id IN (?1)")
	int batchRemove(Integer[] ids);

	@Modifying
	@Query("DELETE FROM NotificationFeedback nf where nf.notificationid IN (?1)")
	int batchRemoveNotification(Integer[] ids);
}
