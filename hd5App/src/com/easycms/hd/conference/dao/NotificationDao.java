package com.easycms.hd.conference.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.Notification;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface NotificationDao extends Repository<Notification,Integer>,BasicDao<Notification>{
	@Query("FROM Notification n WHERE n.type = ?1 and n.createBy = ?2 ORDER BY n.createOn desc")
	Page<Notification> findByTypeAndCreater(String type, Integer creater, Pageable pageable);

	@Modifying
	@Query("DELETE FROM Notification n WHERE n.id IN (?1)")
	int deleteByIds(Integer[] ids);
	@Modifying
	@Query("update Notification n set status=?2 WHERE n.id IN (?1)")
	int updateById(Integer id,String statu);
	
//	update 
}
