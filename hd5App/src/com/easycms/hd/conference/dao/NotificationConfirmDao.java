package com.easycms.hd.conference.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.NotificationConfirm;

@Transactional
public interface NotificationConfirmDao extends Repository<NotificationConfirm,Integer>,BasicDao<NotificationConfirm>{
    @Query("From NotificationConfirm nc where nc.notificationid = ?1 and nc.userid = ?2")
    List<NotificationConfirm> findByNotificationId(Integer id, Integer userid);
}
