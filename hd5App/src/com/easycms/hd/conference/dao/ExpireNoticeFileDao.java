package com.easycms.hd.conference.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.domain.ExpireNoticeFile;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExpireNoticeFileDao extends Repository<ExpireNoticeFile,Integer>,BasicDao<ExpireNoticeFile>{
	@Query("FROM ExpireNoticeFile ")
	Page<ExpireNotice> findByPage(Pageable pageable);
}
