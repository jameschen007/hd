package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.ExpireNoticeFiles;

public interface ExpireNoticeFilesService {
	List<ExpireNoticeFiles> findByNoticeId(Integer noticeId);
	
	ExpireNoticeFiles add(ExpireNoticeFiles expireNoticeFiles);
}
