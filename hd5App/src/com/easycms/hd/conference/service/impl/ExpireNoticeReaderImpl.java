package com.easycms.hd.conference.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ExpireNoticeReaderDao;
import com.easycms.hd.conference.domain.ExpireNoticeReader;

@Service("expireNoticeReaderImpl")
public class ExpireNoticeReaderImpl {

	@Autowired
	private ExpireNoticeReaderDao expireNoticeReaderDao;
	
	public ExpireNoticeReader add(ExpireNoticeReader expireNotice) {
		return expireNoticeReaderDao.save(expireNotice);
	}

	public ExpireNoticeReader findById(Integer id) {
		return expireNoticeReaderDao.findById(id);
	}
}
