package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ConferenceFeedbackDao;
import com.easycms.hd.conference.domain.ConferenceFeedback;
import com.easycms.hd.conference.service.ConferenceFeedbackService;

@Service("conferenceFeedbackService")
public class ConferenceFeedbackServiceImpl implements ConferenceFeedbackService{

	@Autowired
	private ConferenceFeedbackDao conferenceFeedbackDao;
	@Override
	public ConferenceFeedback add(ConferenceFeedback pfile) {
		return conferenceFeedbackDao.save(pfile);
	}
	@Override
	public List<ConferenceFeedback> findByConferenceId(Integer id) {
		return conferenceFeedbackDao.findByConferenceId(id);
	}
	@Override
	public boolean batchRemove(Integer[] ids) {
		return conferenceFeedbackDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveConference(Integer[] ids) {
		return conferenceFeedbackDao.batchRemoveConference(ids) > 0;
	}

}
