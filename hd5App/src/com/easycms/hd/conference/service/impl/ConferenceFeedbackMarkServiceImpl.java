package com.easycms.hd.conference.service.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.ConferenceFeedbackMarkDao;
import com.easycms.hd.conference.domain.ConferenceFeedbackMark;
import com.easycms.hd.conference.service.ConferenceFeedbackMarkService;

@Service("conferenceFeedbackMarkService")
public class ConferenceFeedbackMarkServiceImpl implements ConferenceFeedbackMarkService{

	@Autowired
	private ConferenceFeedbackMarkDao conferenceFeedbackMarkDao;
	
	@Override
	public ConferenceFeedbackMark add(ConferenceFeedbackMark mark) {
		return conferenceFeedbackMarkDao.save(mark);
	}
	@Override
	public ConferenceFeedbackMark findByConferenceIdAndUserId(Integer conferenceId, Integer userId) {
		return conferenceFeedbackMarkDao.findByConferenceIdAndUserId(conferenceId, userId);
	}
	
	@Override
	public int markRead(Integer markId, Date markTime) {
		return conferenceFeedbackMarkDao.markRead(markId, markTime);
	}
	@Override
	public boolean batchRemove(Integer[] ids) {
		return conferenceFeedbackMarkDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveConference(Integer[] ids) {
		return conferenceFeedbackMarkDao.batchRemoveConference(ids) > 0;
	}

}
