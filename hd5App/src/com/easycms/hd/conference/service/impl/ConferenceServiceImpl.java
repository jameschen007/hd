package com.easycms.hd.conference.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.DateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceStatusEnum;
import com.easycms.hd.conference.dao.ConferenceDao;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.domain.ConferenceReceive;
import com.easycms.hd.conference.mybatis.dao.ConferenceMybatisDao;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;

@Service("conferenceService")
public class ConferenceServiceImpl implements ConferenceService {
	private static Logger logger = Logger.getLogger(ConferenceServiceImpl.class);

	@Autowired
	private ConferenceDao conferenceDao;
	@Autowired
	private ConferenceMybatisDao conferenceMybatisDao;
	@Autowired
	private JPushService jPushService;
	
	@Override
	public Conference add(Conference conference) {
		return conferenceDao.save(conference);
	}

	@Override
	public Page<Conference> findByTypeAndCreater(String type, Integer creater, Page<Conference> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Conference> springPage = conferenceDao.findByTypeAndCreater(type, creater, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<Conference> findConferenceSend(Integer userId, Page<Conference> page) {
		ConferenceReceive conferenceReceive = new ConferenceReceive();
		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		conferenceReceive.setPageSize(page.getPagesize());
		conferenceReceive.setUserId(userId);

		int count = 0;
		List<Integer> result = null;
		result = conferenceMybatisDao.findConferenceSend(conferenceReceive);
		count = conferenceMybatisDao.findConferenceSendCount(conferenceReceive);
		
		if (null != result && !result.isEmpty()) {
			List<Conference> conferences = result.stream().map(id -> {
				Conference conference = conferenceDao.findById(id);
				return conference;
			}).collect(Collectors.toList());
			
			page.execute(count, page.getPageNum(), conferences);
		}
		
		return page;
	}
	
	@Override
	public Page<Conference> findConferenceReceive(Integer userId, Page<Conference> page) {
		ConferenceReceive conferenceReceive = new ConferenceReceive();
		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		conferenceReceive.setPageSize(page.getPagesize());
		conferenceReceive.setUserId(userId);
		
		int count = 0;
		List<Integer> result = null;
		result = conferenceMybatisDao.findConferenceReceive(conferenceReceive);
		count = conferenceMybatisDao.findConferenceReceiveCount(conferenceReceive);
		
		if (null != result && !result.isEmpty()) {
			List<Conference> conferences = result.stream().map(id -> {
				Conference conference = conferenceDao.findById(id);
				return conference;
			}).collect(Collectors.toList());
			
			page.execute(count, page.getPageNum(), conferences);
		}
		
		return page;
	}

	@Override
	public Conference findById(Integer id) {
		return conferenceDao.findById(id);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != conferenceDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public boolean batchCancel(Integer[] ids) {
		if (ids!=null && 0 != ids.length){

			List<User> uList = new ArrayList<User>();
			for(Integer id:ids){
				Conference noti = conferenceDao.findById(id);
				Date now = DateUtility.now();
				if(noti.getStartTime().after(now)){
					conferenceDao.updateById(id,ConferenceStatusEnum.CANCEL.toString());
					List<User> tp = noti.getParticipants();
					if(tp!=null){
						uList.addAll(tp);
					}
				}else{//会议已经开始了，取消不成功
					logger.error("会议已经开始了，取消不成功");
					return false ;
				}
				
			}
			if(uList.size()>0){
				List<Integer> uList3 = uList.stream().mapToInt(b->b.getId()).boxed().collect(Collectors.toList());
				
				String message = " 接收到＂取消会议＂的消息，请打开应用APP查看";
				if (!uList3.isEmpty() ) {
			        JPushExtra extra = new JPushExtra();
					extra.setCategory(JPushCategoryEnums.CONFERENCE_CANCEL.name());
					jPushService.pushByUserId(extra, message, JPushCategoryEnums.CONFERENCE_CANCEL.getName(), uList3);
				}
			}
			
			return true;
		}
		return false;
	}

}
