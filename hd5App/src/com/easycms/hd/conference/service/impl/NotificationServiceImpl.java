package com.easycms.hd.conference.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.DateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceStatusEnum;
import com.easycms.hd.conference.dao.NotificationDao;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.domain.ConferenceReceive;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.mybatis.dao.ConferenceMybatisDao;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {
	private static Logger logger = Logger.getLogger(ConferenceServiceImpl.class);

	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private ConferenceMybatisDao conferenceMybatisDao;
	@Autowired
	private JPushService jPushService;

	@Override
	public Notification add(Notification notification) {
		return notificationDao.save(notification);
	}

	@Override
	public Page<Notification> findByTypeAndCreater(String type, Integer creater, Page<Notification> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Notification> springPage = notificationDao.findByTypeAndCreater(type, creater, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<Notification> findNotificationSend(Integer userId, Page<Notification> page) {
		ConferenceReceive conferenceReceive = new ConferenceReceive();
		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		conferenceReceive.setPageSize(page.getPagesize());
		conferenceReceive.setUserId(userId);

		int count = 0;
		List<Integer> result = null;
		result = conferenceMybatisDao.findNotificationSend(conferenceReceive);
		count = conferenceMybatisDao.findNotificationSendCount(conferenceReceive);
		
		if (null != result && !result.isEmpty()) {
			List<Notification> notifications = result.stream().map(id -> {
				Notification notification = notificationDao.findById(id);
				return notification;
			}).collect(Collectors.toList());
			
			page.execute(count, page.getPageNum(), notifications);
		}
		
		return page;
	}
	@Override
	public Page<Notification> findNotificationReceive(Integer userId, Page<Notification> page) {
		ConferenceReceive conferenceReceive = new ConferenceReceive();
		conferenceReceive.setPageNumber((page.getPageNum() - 1) * page.getPagesize());
		conferenceReceive.setPageSize(page.getPagesize());
		conferenceReceive.setUserId(userId);
		
		int count = 0;
		List<Integer> result = null;
		result = conferenceMybatisDao.findNotificationReceive(conferenceReceive);
		count = conferenceMybatisDao.findNotificationReceiveCount(conferenceReceive);
		
		if (null != result && !result.isEmpty()) {
			List<Notification> notifications = result.stream().map(id -> {
				Notification notification = notificationDao.findById(id);
				return notification;
			}).collect(Collectors.toList());
			
			page.execute(count, page.getPageNum(), notifications);
		}
		
		return page;
	}

	@Override
	public Notification findById(Integer id) {
		return notificationDao.findById(id);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != notificationDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}
	/**批量取消会议*/
	@Override
	public boolean batchCancel(Integer[] ids) {
		if (ids!=null && 0 != ids.length){

			List<User> uList = new ArrayList<User>();
			for(Integer id:ids){
				Notification noti = notificationDao.findById(id);
				Date now = DateUtility.now();
				if(noti.getStartTime().after(now)){
					notificationDao.updateById(id,ConferenceStatusEnum.CANCEL.toString());
					
					List<User> tp = noti.getParticipants();
					if(tp!=null){
						uList.addAll(tp);
					}
				}else{//会议已经开始了，取消不成功
					logger.error("通知已经开始了，取消不成功");
					return false ;
				}
			}
			if(uList.size()>0){
				List<Integer> uList3 = uList.stream().mapToInt(b->b.getId()).boxed().collect(Collectors.toList());
				String message = " 接收到＂取消通知＂的消息，请打开应用APP查看";
				if (!uList3.isEmpty() ) {
			        JPushExtra extra = new JPushExtra();
					extra.setCategory(JPushCategoryEnums.CONFERENCE_CANCEL.name());
					jPushService.pushByUserId(extra, message, JPushCategoryEnums.CONFERENCE_CANCEL.getName(), uList3);
				}
			}
			
			return true;
		}
		return false;
	
		
	}
}
