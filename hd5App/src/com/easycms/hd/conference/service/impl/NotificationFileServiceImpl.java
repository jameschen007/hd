package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.NotificationFileDao;
import com.easycms.hd.conference.domain.NotificationFile;
import com.easycms.hd.conference.service.NotificationFileService;


@Service("notificationFileService")
public class NotificationFileServiceImpl implements NotificationFileService{

	@Autowired
	private NotificationFileDao notificationFileDao;
	@Override
	public NotificationFile add(NotificationFile pfile) {
		return notificationFileDao.save(pfile);
	}
	@Override
	public List<NotificationFile> findFilesByNotificationId(Integer id) {
	
		return notificationFileDao.findByNotificationId(id);
	}
	@Override
	public boolean batchRemove(Integer[] ids) {
		return notificationFileDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveNotification(Integer[] ids) {
		return notificationFileDao.batchRemoveNotification(ids) > 0;
	}
	@Override
	public List<NotificationFile> findFilesByNotificationIds(Integer[] ids) {
		return notificationFileDao.findFilesByNotificationIds(ids);
	}
	@Override
	public NotificationFile findById(Integer id) {
		return notificationFileDao.findById(id);
	}
	@Override
	public boolean batchUpdateNotification(Integer[] ids, Integer notificationId) {
		return notificationFileDao.batchUpdateNotification(ids, notificationId) > 0;
	}

}
