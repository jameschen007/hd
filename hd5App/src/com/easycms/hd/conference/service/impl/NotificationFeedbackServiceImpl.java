package com.easycms.hd.conference.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.conference.dao.NotificationFeedbackDao;
import com.easycms.hd.conference.domain.NotificationFeedback;
import com.easycms.hd.conference.service.NotificationFeedbackService;

@Service("notificationFeedbackService")
public class NotificationFeedbackServiceImpl implements NotificationFeedbackService{

	@Autowired
	private NotificationFeedbackDao notificationFeedbackDao;
	@Override
	public NotificationFeedback add(NotificationFeedback feedback) {
		return notificationFeedbackDao.save(feedback);
	}
	@Override
	public List<NotificationFeedback> findByNotificationId(Integer id) {
		return notificationFeedbackDao.findByNotificationId(id);
	}
	@Override
	public boolean batchRemove(Integer[] ids) {
		return notificationFeedbackDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveNotification(Integer[] ids) {
		return notificationFeedbackDao.batchRemoveNotification(ids) > 0;
	}

}
