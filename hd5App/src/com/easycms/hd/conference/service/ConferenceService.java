package com.easycms.hd.conference.service;

import com.easycms.core.util.Page;
import com.easycms.hd.conference.domain.Conference;

public interface ConferenceService {
	Conference add(Conference conference);
	
	Conference findById(Integer id);
	
	Page<Conference> findByTypeAndCreater(String type, Integer creater, Page<Conference> page);

	Page<Conference> findConferenceReceive(Integer userId, Page<Conference> page);

	boolean batchDelete(Integer[] ids);
	boolean batchCancel(Integer[] ids);

	Page<Conference> findConferenceSend(Integer userId, Page<Conference> page);
}
