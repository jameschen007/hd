package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.NotificationConfirm;

public interface NotificationConfirmService {
	NotificationConfirm add(NotificationConfirm confirm);
    
    List<NotificationConfirm> findByNotificationId(Integer id, Integer userid);
}
