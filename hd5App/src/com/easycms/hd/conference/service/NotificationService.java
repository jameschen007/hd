package com.easycms.hd.conference.service;

import com.easycms.core.util.Page;
import com.easycms.hd.conference.domain.Notification;

public interface NotificationService {
	Notification add(Notification notification);
	
	Page<Notification> findByTypeAndCreater(String type, Integer creater, Page<Notification> page);

	Page<Notification> findNotificationReceive(Integer userId, Page<Notification> page);

	Notification findById(Integer id);

	boolean batchDelete(Integer[] ids);
	boolean batchCancel(Integer[] ids);

	Page<Notification> findNotificationSend(Integer userId, Page<Notification> page);
}
