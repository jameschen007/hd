package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.ConferenceFeedback;

public interface ConferenceFeedbackService {
	ConferenceFeedback add(ConferenceFeedback feedback);
	
	List<ConferenceFeedback> findByConferenceId(Integer id);
	
	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveConference(Integer[] ids);
}
