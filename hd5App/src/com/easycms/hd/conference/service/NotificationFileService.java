package com.easycms.hd.conference.service;

import java.util.List;

import com.easycms.hd.conference.domain.NotificationFile;

public interface NotificationFileService {
	NotificationFile add(NotificationFile pfile);
	
	List<NotificationFile> findFilesByNotificationId(Integer id);
	
	boolean batchRemove(Integer[] ids);
	
	boolean batchRemoveNotification(Integer[] ids);

	List<NotificationFile> findFilesByNotificationIds(Integer[] ids);

	NotificationFile findById(Integer id);

	boolean batchUpdateNotification(Integer[] ids, Integer notificationId);
}
