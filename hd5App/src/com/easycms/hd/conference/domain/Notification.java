package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "notification")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class Notification extends BasicForm implements Serializable {
	private static final long serialVersionUID = 1280545995308362123L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "subject")
	private String subject;
	@Column(name = "content")
	private String content;
	@Column(name = "category")
	private String category;
	@Column(name = "type")
	private String type;
	@Column(name = "department")
	private String department;
	@Column(name = "status")
	private String status;
	@Column(name = "start_time")
	private Date startTime;
	@Column(name = "end_time")
	private Date endTime;
	@Column(name = "alarm_time")
	private String alarmTime;
	@Column(name = "trigger_name")
	private String triggerName;
	@Column(name = "source")
	private String source;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
	
	@JsonIgnore
	@ManyToMany(targetEntity=com.easycms.management.user.domain.User.class,fetch=FetchType.EAGER,cascade={CascadeType.REMOVE, CascadeType.REFRESH})
	@JoinTable(
			name="notification_user",
			joinColumns=@JoinColumn(name="notification_id",referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="user_id",referencedColumnName="id")
			)
	private List<User> participants;
	
	@Transient
	private List<NotificationFile> notificationFiles;
}
