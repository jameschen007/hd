package com.easycms.hd.conference.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "notification_file")
@Data
public class NotificationFile {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "filepath")
	private String path;
	@Column(name = "uploadtime")
	private Date time;
	@Column(name = "notificationid")
	private Integer notificationid;
	@Column(name = "filename")
	private String fileName;
}
