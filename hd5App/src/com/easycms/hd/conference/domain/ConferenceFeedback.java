package com.easycms.hd.conference.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "conference_feedback")
@Data
public class ConferenceFeedback {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "conferenceid")
	private Integer conferenceid;
	@Column(name = "message")
	private String message;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	
}
