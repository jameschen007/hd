package com.easycms.hd.conference.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "expire_notice_file")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class ExpireNoticeFile implements Serializable {
	private static final long serialVersionUID = -4647617585942876366L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
//	@Column(name = "expire_id")
//	private Integer expireId;// '通知单ID',
	@Column(name = "canc_code")
	private String cancCode;// 文件失效通知单编号 主表 通知单ID
	@Column(name = "canc_id")
	private String cancId;// enpower 本表 主键 文件id
	@Column(name = "intercode")
	private String intercode;// 内部文件编号
	@Column(name = "c_title")
	private String cTitle;// 中文名称
	@Column(name = "revsion")
	private String revsion;// 版本
	@Column(name = "status ")
	private String status;// 状态
	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "file")
	private List<ExpireNoticeReader> userList;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expire_id", nullable = false)
	private EnpowerExpireNotice notice;

	public List<ExpireNoticeReader> getUserList() {
		return userList;
	}
	@JsonBackReference
	public void setUserList(List<ExpireNoticeReader> userList) {
		this.userList = userList;
	}
	public EnpowerExpireNotice getNotice() {
		return notice;
	}
	@JsonBackReference
	public void setNotice(EnpowerExpireNotice notice) {
		this.notice = notice;
	}
	
	
}
