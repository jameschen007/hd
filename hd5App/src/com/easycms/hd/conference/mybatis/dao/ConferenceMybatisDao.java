package com.easycms.hd.conference.mybatis.dao;

import java.util.List;

import com.easycms.hd.conference.domain.ConferenceReceive;

public interface ConferenceMybatisDao {
//Conference
	List<Integer> findConferenceSend(ConferenceReceive conference);
	Integer findConferenceSendCount(ConferenceReceive conference);
	List<Integer> findConferenceReceive(ConferenceReceive conference);
	Integer findConferenceReceiveCount(ConferenceReceive conference);
//Notificaiton	
	List<Integer> findNotificationSend(ConferenceReceive notification);
	Integer findNotificationSendCount(ConferenceReceive notification);
	List<Integer> findNotificationReceive(ConferenceReceive notification);
	Integer findNotificationReceiveCount(ConferenceReceive notification);
}
