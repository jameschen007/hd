package com.easycms.hd.conference.enums;

public enum ConferenceSource {
	/**
	 * 手机端调用
	 */
	API,
	/**
	 * 网页
	 */
	WEB;
}
