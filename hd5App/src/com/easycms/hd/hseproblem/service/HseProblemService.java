package com.easycms.hd.hseproblem.service;

import java.util.List;
import java.util.Set;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.hseproblem.Hse3mEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemDetils;

/**
 * @author ZengMao
 * @create Date:2017-10-12
 */
public interface HseProblemService {
	public HseProblem add(HseProblem hseProblem);
	public boolean delete(HseProblem hseProblem);
	public HseProblem update(HseProblem HseProblem);
	public HseProblem findById(Integer id);
	Integer count();
	public List<HseProblem> findAll();
	public Page<HseProblem> findAll(Page<HseProblem> page,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm);
	/**
	 * 获取指定状态的施工问题
	 * @param page
	 * @param problemStatus
	 * @return
	 */
	public Page<HseProblem> findByProblemStatus(Page<HseProblem> page, String problemStatus,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm);
	/**
	 * 获取指定部门的施工问题
	 * @param page
	 * @param problemStatus
	 * @param deptId
	 * @return
	 */
	public Page<HseProblem> findByDept(Page<HseProblem> page, String problemStatus,Integer deptId);
	/**
	 * 
	 * 获取指定班组的施工问题
	 * @param page
	 * @param problemStatus
	 * @param teamId
	 * @return
	 */
	public Page<HseProblem> findByTeam(Page<HseProblem> page, String problemStatus, Integer teamId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm);
	public Page<HseProblem> findByCaptain(Page<HseProblem> page, String problemStatus, Integer captainId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) ;
	/**
	 * 获取指定人创建的施工问题
	 * @param page
	 * @param problemStatus
	 * @param userId
	 * @return
	 */
	public Page<HseProblem> findByCreateUser(Page<HseProblem> page, String problemStatus,Integer userId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm);
	public Page<HseProblem> findByProblemAndSolveStatus(Page<HseProblem> page, String problemStatus, Set<String> solveStatus,String keyword);
	public Page<HseProblem> findByProblemAndSolveStatus2(Page<HseProblem> page, String problemStatus, String solveStatus, HseProblemExListRequestForm hseProblemExListRequestForm);
	
	public Page<HseProblemDetils> findDetilsAll(Page<HseProblemDetils> page,String problemStatus);


	/**
	 * 查询90天内安全文明问题分页列表
	 *
	 * @param page
	 * @param responsibleDept
	 * @param oneOf3Type
	 * @return
	 */
	public Page<HseProblem> getHseProblem3mResult(Page<HseProblem> page, Integer responsibleDept, Hse3mEnum oneOf3Type,HseProblemExListRequestForm hseProblemExListRequestForm, HseProblemStatusEnum problemStatus);

	/**
	 *删除不需处理的安全数据
	 *
	 */
	public boolean deleteNotHandle();
}
