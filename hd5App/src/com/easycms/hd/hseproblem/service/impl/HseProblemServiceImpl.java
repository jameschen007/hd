package com.easycms.hd.hseproblem.service.impl;

import java.util.List;
import java.util.Set;

import com.easycms.hd.api.enums.hseproblem.Hse3mEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.plan.mybatis.dao.HseMybatisDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.hseproblem.dao.HseProblemDao;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemDetils;
import com.easycms.hd.hseproblem.mybatis.dao.HseProblemMybatisDao;
import com.easycms.hd.hseproblem.service.HseProblemService;

@Service("hseProblemService")
public class HseProblemServiceImpl implements HseProblemService {
	@Autowired
	private HseProblemDao hseProblemDao;
	@Autowired
	private com.easycms.hd.plan.mybatis.dao.HseMybatisDao hseMybatisDao ;
	@Autowired
	private HseProblemMybatisDao hseProblemMybatisDao;
	@Autowired
	private HseProblemDynamicQueryServiceImpl hseProblemDynamicQueryService;

	@Override
	public HseProblem add(HseProblem hseProblem) {
		return hseProblemDao.save(hseProblem);
	}

	@Override
	public boolean delete(HseProblem hseProblem) {
		return hseProblemDao.deleteById(hseProblem.getId()) > 0;
	}

	@Override
	public HseProblem update(HseProblem HseProblem) {
		return hseProblemDao.save(HseProblem);
	}

	@Override
	public HseProblem findById(Integer id) {
		return hseProblemDao.findById(id);
	}

	@Override
	public Integer count() {
		return hseProblemDao.count();
	}
	
	@Override
	public List<HseProblem> findAll(){
		List<HseProblem> hseProblems = hseProblemDao.findAll();
		return hseProblems;
	}

	@Override
	public Page<HseProblem> findAll(Page<HseProblem> page,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(StringUtils.isBlank(keyword)){
			//springPage = hseProblemDao.findAll(pageable);
			springPage = hseProblemDynamicQueryService.findByStatusAndCreateUser(null, null, pageable,  hseProblemExListRequestForm);
		}else{
			springPage = hseProblemDao.findAll_keyword(CommonUtility.plusMi(keyword),pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<HseProblem> findByProblemAndSolveStatus(Page<HseProblem> page, String problemStatus, Set<String> solveStatus,String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
//		if(StringUtils.isBlank(keyword)){
//			springPage = hseProblemDao.findByHseProblemAndSolveStatus(solveStatus,problemStatus, pageable);
//
//			//TODO 增加提出时间和截止时间筛选
//
//			springPage = hseProblemDao.findByHseProblemAndSolveStatus(solveStatus,problemStatus, pageable);
//		}else{
			springPage = hseProblemDao.findByHseProblemAndSolveStatus_keyword(solveStatus,problemStatus,CommonUtility.plusMi(keyword), pageable);
//		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
	@Override
	public Page<HseProblem> findByProblemAndSolveStatus2(Page<HseProblem> page, String problemStatus, String solveStatus, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
//		if(StringUtils.isBlank(keyword)){
//			springPage = hseProblemDao.findByHseProblemAndSolveStatus(solveStatus,problemStatus, pageable);

			//TODO 增加提出时间和截止时间筛选
			springPage = hseProblemDynamicQueryService.findByHseProblemAndSolveStatus(solveStatus,problemStatus,pageable,  hseProblemExListRequestForm);
//		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<HseProblem> findByProblemStatus(Page<HseProblem> page, String problemStatus,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(StringUtils.isBlank(keyword)){
			//springPage = hseProblemDao.findByHseProblemStatus(problemStatus, pageable);
			springPage = hseProblemDynamicQueryService.findByStatusAndCreateUser(problemStatus,null,pageable,hseProblemExListRequestForm);
		}else{
			springPage = hseProblemDao.findByHseProblemStatus_keyword(problemStatus,CommonUtility.plusMi(keyword), pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<HseProblem> findByDept(Page<HseProblem> page, String problemStatus, Integer deptId) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(CommonUtility.isNonEmpty(problemStatus)){
			springPage = hseProblemDao.findByStatusAndDeptId(problemStatus, deptId, pageable);			
		}else{
			springPage = hseProblemDao.findAllHseProblemByDept(deptId, pageable);
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
	
	@Override
	public Page<HseProblem> findByTeam(Page<HseProblem> page, String problemStatus, Integer teamId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(CommonUtility.isNonEmpty(problemStatus)){
			if(StringUtils.isBlank(keyword)){

				//springPage = hseProblemDao.findByStatusAndTeamId(problemStatus, teamId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndTeamId(problemStatus, teamId,null, pageable, hseProblemExListRequestForm);
			}else{

				springPage = hseProblemDao.findByStatusAndTeamId_keyword(problemStatus, teamId,CommonUtility.plusMi(keyword), pageable);			
			}
		}else{

			if(StringUtils.isBlank(keyword)){
				//springPage = hseProblemDao.findAllHseProblemByTeam(teamId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndTeamId(null, teamId,null, pageable,  hseProblemExListRequestForm);
			}else{

				springPage = hseProblemDao.findAllHseProblemByTeam_keyword(teamId,CommonUtility.plusMi(keyword), pageable);
			}

				
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
	@Override
	public Page<HseProblem> findByCaptain(Page<HseProblem> page, String problemStatus, Integer captainId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(CommonUtility.isNonEmpty(problemStatus)){
			if(StringUtils.isBlank(keyword)){

				//springPage = hseProblemDao.findByStatusAndCaptainId(problemStatus, captainId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndTeamId(problemStatus, null,captainId, pageable, hseProblemExListRequestForm);
			}else{

				springPage = hseProblemDao.findByStatusAndCaptainId_keyword(problemStatus, captainId,CommonUtility.plusMi(keyword), pageable);			
			}
		}else{

			if(StringUtils.isBlank(keyword)){
				//springPage = hseProblemDao.findAllHseProblemByCaptain(captainId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndTeamId(null, null,captainId, pageable,  hseProblemExListRequestForm);
			}else{

				springPage = hseProblemDao.findAllHseProblemByCaptain_keyword(captainId,CommonUtility.plusMi(keyword), pageable);
			}

				
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<HseProblem> findByCreateUser(Page<HseProblem> page, String problemStatus, Integer userId,String keyword, HseProblemExListRequestForm hseProblemExListRequestForm) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<HseProblem> springPage = null;
		if(CommonUtility.isNonEmpty(problemStatus)){
			if(StringUtils.isNotBlank(keyword)){
				springPage = hseProblemDao.findByStatusAndCreateUser_keyword(problemStatus, userId,CommonUtility.plusMi(keyword), pageable);
			}else{
				//springPage = hseProblemDao.findByStatusAndCreateUser(problemStatus, userId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndCreateUser( problemStatus, userId,  pageable,  hseProblemExListRequestForm);
			}
			//TODO 增加提出时间和截止时间筛选
		}else{
			if(StringUtils.isNotBlank(keyword)){
				springPage = hseProblemDao.findByCreateUser_keyword(userId,CommonUtility.plusMi(keyword), pageable);		
			}else{
				//springPage = hseProblemDao.findByCreateUser(userId, pageable);
				springPage = hseProblemDynamicQueryService.findByStatusAndCreateUser( null, userId,  pageable,  hseProblemExListRequestForm);
			}
			//TODO 增加提出时间和截止时间筛选
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	/**
	 * 查询90天内安全文明问题分页列表
	 * @param page
	 * @param responsibleDept
	 * @param oneOf3Type
	 * @return
	 */
	@Override
	public Page getHseProblem3mResult(Page page, Integer responsibleDept, Hse3mEnum oneOf3Type,HseProblemExListRequestForm hseProblemExListRequestForm, HseProblemStatusEnum problemStatus){

		org.springframework.data.domain.Page springPage = hseProblemDynamicQueryService.getHseProblem3mResult(page, responsibleDept, oneOf3Type, hseProblemExListRequestForm,  problemStatus);
//		org.springframework.data.domain.Page<HseProblem> springPage = hseProblemDao.hse3mDelay(responsibleDept, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<HseProblemDetils> findDetilsAll(Page<HseProblemDetils> page,String problemStatus) {
		int pageNum = (page.getPageNum() - 1) * page.getPagesize();
		int pageSize = page.getPagesize();
		int count = 0;
		List<HseProblemDetils> results = null;
		
		if(CommonUtility.isNonEmpty(problemStatus)){
			count = hseProblemMybatisDao.getCountByStatus(problemStatus);
			results = hseProblemMybatisDao.findByStatus(problemStatus,pageNum,pageSize);			
		}else{
			count = hseProblemMybatisDao.getCount();
			results = hseProblemMybatisDao.findAll(pageNum, pageSize);	
		}
		page.execute(count, page.getPageNum(), results);
		
		return page;
	}

	/**
	 *删除不需处理的安全数据
	 */
	@Override
	public boolean deleteNotHandle(){
		try {

			hseMybatisDao.deleteNotHandle();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false ;
		}
	}
}
