package com.easycms.hd.hseproblem.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;

public interface HseProblemSolveDao extends Repository<HseProblemSolve,Integer>,BasicDao<HseProblemSolve> {
	
	@Query("From HseProblemSolve hps where hps.problemId = ?1")
	List<HseProblemSolve> findByHseProblemId(Integer id);
	
	@Query("From HseProblemSolve hps where hps.problemId = ?1 and hps.solveStep = ?2")
	List<HseProblemSolve> findByHseProblemIdAndSolveStep(Integer id, String solveRole);
	
	@Query("Select hps.problemId From HseProblemSolve hps where hps.solveStatus = ?1")
	Set<String> findProblemIdByStatus(String solveStatus);

}
