package com.easycms.hd.hseproblem.dao;

import com.easycms.basic.BasicDao;
import com.easycms.hd.hseproblem.domain.HseProblem3mDelay;
import org.springframework.data.repository.Repository;

public interface HseProblem3mDelayDao extends Repository<HseProblem3mDelay, Integer>, BasicDao<HseProblem3mDelay> {

}
