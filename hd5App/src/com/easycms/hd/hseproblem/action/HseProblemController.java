package com.easycms.hd.hseproblem.action;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.enums.hseproblem.HseProblemSolveStatusEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemAssignRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemRequestForm;
import com.easycms.hd.api.response.DepartmentUserResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult2;
import com.easycms.hd.api.response.hseproblem.HseProblemResult;
import com.easycms.hd.api.service.HseProblemApiService;
import com.easycms.hd.api.service.impl.DepartmentApiServiceImpl;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.easycms.hd.hseproblem.domain.HseProblemSolveStep;
import com.easycms.hd.hseproblem.domain.HseProblemStep;
import com.easycms.hd.hseproblem.service.HseProblemService;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/hseproblem")
@Slf4j
public class HseProblemController {
	
	@Autowired
	private HseProblemService hseProblemService;
	@Autowired
	private HseProblemSolveService hseProblemSolveService;
	@Autowired
	private HseProblemApiService hseProblemApiService;
	@Autowired
	@Qualifier("departmentApiService")
	private com.easycms.hd.api.service.DepartmentApiService departmentApiServiceImpl;
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showList(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("form") BasicForm form) throws Exception {
		String func = form.getFunc();
		log.debug("[func] = " + func + " : " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hseproblem/list_civilized_construction");
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("form") HseProblem form) throws Exception {
		log.debug("==> Show quality data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		return ForwardUtility.forwardAdminView("/hseproblem/data/data_json_civilized_construction");
	}
	
	/**
	 * 新增质量问题
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String id = request.getParameter("id");
		int deptId = ("".equals(id) || id == null) ? 0 : Integer.parseInt(id);
		log.debug("DeptId: "+ deptId);
		HseProblemCreatUiDataResult2 resultData = new HseProblemCreatUiDataResult2();
		resultData = hseProblemApiService.getHseProblemCreatUiResultForReport(deptId,"safe");
		request.setAttribute("resultData", resultData);
		return ForwardUtility.forwardAdminView("/hseproblem/page_civilized_construction_add");
	}
	
	/**
	 * 保存新增
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") HseProblemRequestForm form,
			@RequestParam(value="file") CommonsMultipartFile files[]) throws Exception{
		boolean status = false;
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String loginId = WebUtility.getUserIdFromSession(request);
		if(CommonUtility.isNonEmpty(loginId)){
			form.setLoginId(Integer.parseInt(loginId));			
		}else{
			HttpUtility.writeToClient(response, String.valueOf(status));
			return;
		}
		
		if(files == null){
			HttpUtility.writeToClient(response, String.valueOf(status));
			return;
		}
		status =hseProblemApiService.createHseProblem(form, files);
//		status = true;
		HttpUtility.writeToClient(response, String.valueOf(status));
	}
	
	/**
	 * 显示详情
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String showDetils(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id") Integer id){
		HseProblemResult hpResult = null;
		HseProblem hp = hseProblemService.findById(id);
		if(hp != null){
			hpResult = hseProblemApiService.transferForList(hp, null);
		}
		log.info("createUser------->  "+hpResult.getCreateUser());
		log.info("FilePath------->  "+hpResult.getFiles().get(0).getPath());
		request.setAttribute("hpResult", hpResult);
		
		return ForwardUtility.forwardAdminView("/hseproblem/page_civilized_construction_detils");
		
	}
	
	/**
	 * 关闭问题
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/close", method = RequestMethod.GET)
	public void close(HttpServletRequest request, HttpServletResponse response){
		String loginIdStr = WebUtility.getUserIdFromSession(request);
		int loginId = 1;
		String idStr = request.getParameter("id");
		int problemId = 1;
		if(CommonUtility.isNonEmpty(loginIdStr)){
			loginId = Integer.parseInt(loginIdStr);
		}
		if(!CommonUtility.isNonEmpty(idStr)){
			HttpUtility.writeToClient(response, "false");
			return;
		}else{
			problemId = Integer.parseInt(idStr);
		}
		
		HseProblem hseProblem = hseProblemService.findById(problemId);
		boolean isNeedHandle = false;
		if(hseProblem != null){
			isNeedHandle = hseProblem.getProblemStatus().equals(HseProblemStatusEnum.Need_Handle.toString());				
		}else{
			HttpUtility.writeToClient(response, "false");
			return;
		}
		if(!isNeedHandle){
			HttpUtility.writeToClient(response, "false");
			return;
		}
		hseProblem.setProblemStatus(HseProblemStatusEnum.None.toString());
		hseProblem.setProblemStep(HseProblemStep.isFinished.toString());//问题关闭
		hseProblem.setFinishDate(Calendar.getInstance().getTime());//问题关闭时间
		hseProblemService.update(hseProblem);

		//将HSE处理情况更新为已处理
		List<HseProblemSolve> hseProblemSolves =hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.hseConfirm.toString());
		if(hseProblemSolves != null && !hseProblemSolves.isEmpty()){
			HseProblemSolve hseProblemSolve = hseProblemSolves.get(0);
			hseProblemSolve.setSolveUser(loginId);
			hseProblemSolve.setSolveDate(Calendar.getInstance().getTime());
			hseProblemSolve.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
			hseProblemSolveService.update(hseProblemSolve);
		}
		HttpUtility.writeToClient(response, "true");
	}
	
	/**
	 * 核实问题
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public String check(HttpServletRequest request, HttpServletResponse response){
		String problemId = request.getParameter("id");
		Integer pId = 0;
		if(!CommonUtility.isNonEmpty(problemId)){
			HttpUtility.writeToClient(response, "false");
		}else{
			pId = Integer.parseInt(problemId);
		}

		HseProblem hp = hseProblemService.findById(pId);
		if(hp != null){
			log.debug("责任部门ID： "+hp.getResponsibleDept());
			List<DepartmentUserResult> teams = departmentApiServiceImpl.findDeptList(hp.getResponsibleDept());
			request.setAttribute("resultData", teams);
			request.setAttribute("teamId", hp.getResponsibleTeam());
			request.setAttribute("problemId", hp.getId());			
		}
		
		return ForwardUtility.forwardAdminView("/hseproblem/model_civilized_construction_check");
	}
	
	/**
	 * 核实并分派
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void assign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") HseProblem form) {
		boolean status = false;
		String targetDate = request.getParameter("targetdate");
		long date = 0;
		
		if(CommonUtility.isNonEmpty(targetDate)){
			targetDate += " 23:59:59";
			date = CommonUtility.convertDateStringToLong(targetDate);			
		}
		log.info("责任班组ID： "+form.getResponsibleTeam());
		log.info("整改期限： "+targetDate);
		
		HseProblemAssignRequestForm hsrForm = new HseProblemAssignRequestForm();
		hsrForm.setProblemId(form.getId());
		hsrForm.setResponsibleTeamId(form.getResponsibleTeam());
		hsrForm.setStartDate(date);

        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("user");
		status = hseProblemApiService.hseAssignTasks(hsrForm,loginUser);
//		status = true;
		HttpUtility.writeToClient(response, String.valueOf(status));
	}
	
	@RequestMapping(value = "/renovate", method = RequestMethod.GET)
	public String renovate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id") String problemId){
		if(!CommonUtility.isNonEmpty(problemId)){
			HttpUtility.writeToClient(response, "false");
		}
		request.setAttribute("problemId", problemId);
		
		return ForwardUtility.forwardAdminView("/hseproblem/model_civilized_construction_renovate");
	}
	
	/**
	 * 提交整改结果
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/renovate", method = RequestMethod.POST)
	public void renovateOk(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="problemId") Integer problemId,
			@RequestParam(value="description") String description,
			@RequestParam(value="file") CommonsMultipartFile[] files){
		boolean status = false;
		log.debug("问题ID： "+problemId);
		log.debug("整改描述： "+description);
		
		if(files == null){
			HttpUtility.writeToClient(response, String.valueOf(status));
		}
		HttpSession session = request.getSession();
 
		User user = null;
		user = (User) session.getAttribute("user");
		status = hseProblemApiService.processRenovateResult(problemId, description, files,user.getId());
//		status = true;
		HttpUtility.writeToClient(response, String.valueOf(status));
	}
	
	@RequestMapping(value = "/verify", method = RequestMethod.GET)
	public String verify(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id") String problemId){
		if(!CommonUtility.isNonEmpty(problemId)){
			HttpUtility.writeToClient(response, "false");
		}
		request.setAttribute("problemId", problemId);
		return ForwardUtility.forwardAdminView("/hseproblem/model_civilized_construction_verify");
	}
	
	/**
	 * 核实问题
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public void verifyOk(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id") Integer problemId,
			@RequestParam(value="checkResult") Integer checkResult){
		boolean status = false;
		int loginId = 1;
		String loginIdStr = WebUtility.getUserIdFromSession(request);
		if(CommonUtility.isNonEmpty(loginIdStr)){
			loginId = Integer.parseInt(loginIdStr);
		}
		
		log.debug("问题ID：" + problemId);
		log.debug("核实结果：" + checkResult);
		BaseRequestForm form = new BaseRequestForm();
		form.setLoginId(loginId);
		
        try{

    		status = hseProblemApiService.checkRenovateResult(form, problemId, checkResult);
        }catch(Exception e){
        	status = false ;
            e.printStackTrace();
        }
//		status = true;
		HttpUtility.writeToClient(response, String.valueOf(status));
		
	}

	/**
	 * 删除不需处理的安全数据
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/deleteNotHandle", method = RequestMethod.POST)
	public void deleteNotHandle(HttpServletRequest request, HttpServletResponse response) throws Exception{
		boolean f = hseProblemService.deleteNotHandle();
		HttpUtility.writeToClient(response, String.valueOf(f));
	}
}
