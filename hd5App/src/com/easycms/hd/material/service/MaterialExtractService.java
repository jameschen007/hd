package com.easycms.hd.material.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.material.domain.MaterialExtract;

public interface MaterialExtractService {
	MaterialExtract add(MaterialExtract materialExtract);

	boolean batchRemove(Integer[] ids);

	Page<MaterialExtract> findByPage(Integer departmentId, Page<MaterialExtract> page);

	MaterialExtract findById(Integer id);
	
	List<MaterialExtract> findByWarrantyNo(String warrantyNo);
}
