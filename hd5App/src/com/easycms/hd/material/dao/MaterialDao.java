package com.easycms.hd.material.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.material.domain.Material;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface MaterialDao extends Repository<Material,Integer>,BasicDao<Material>{
	@Query("FROM Material m ORDER BY m.createOn desc")
	Page<Material> findByPage(Pageable pageable);
	
	@Modifying
	@Query("UPDATE Material m SET m.exit = 1 WHERE m.id IN (?1)")
	int markDeleteByIds(Integer[] ids);
	
	@Modifying
	@Query("DELETE FROM Material m WHERE m.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Query("FROM Material m WHERE m.warrantyNo = ?1")
	Material findByWarrantyNo(String warrantyNo);
}
