package com.easycms.hd.question.directive;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.ArrayUtility;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.response.question.RollingQuestionPageDataResult;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

public class QuestionDirective extends BaseDirective<RollingQuestion> {

	@Autowired
	private RollingQuestionService questionService;
	@Autowired
	private UserService userService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private QuestionApiService questionApiService;

	// @Autowired
	// private WorkflowHelper workflowHelper;

	private Logger logger = Logger.getLogger(QuestionDirective.class);

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected RollingQuestion field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<RollingQuestion> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<RollingQuestion> pager,
			Map<String, Object> envParams) {
		String type = FreemarkerTemplateUtility.getStringValueFromParams(params, "rollingPlanType");
		String questionStatus = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		String userid = WebUtility.getUserIdFromSession(WebUtility.getRequest());
		Integer userId = null;
		String keyword = null;
		User he = userService.findUserById(Integer.parseInt(userid));
		List<Role> roles = he.getRoles();
		//rollingPlanType 如GDJH
		if(!CommonUtility.isNonEmpty(type)){
			type = (String) WebUtility.getSession().getAttribute("type");
		}
		if(!CommonUtility.isNonEmpty(questionStatus)){
			for (Role role : roles) {
				Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
				if (roleNameList.contains(ConstantVar.COORDINATOR)) {
					questionStatus = "NEED_ASSIGN";
				}else if(roleNameList.contains(ConstantVar.supervisor)) {
					questionStatus = "NEED_SOLVED";
				}else if(roleNameList.contains(ConstantVar.solver3)
						|| roleNameList.contains(ConstantVar.solver4)
						|| roleNameList.contains(ConstantVar.solver5)
						|| roleNameList.contains(ConstantVar.solver6)) {
					questionStatus = "PRE";
				}else{
					questionStatus = "NEED_SOLVE";					
				}
			}
		}

		RollingQuestion mainCondition = new RollingQuestion();
		RollingQuestionSolver childCondition = new RollingQuestionSolver();
		//是否是技术中心的领导，即是否是各主管，如管道主管、电气主管
		boolean isParent = false ;
		switch (questionStatus) {
		case "NEED_SOLVE"://QuestionStatusEnum_ZZ.NEED_SOLVE.toString():
				if (null != roles) {
					for (Role role : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
						if (roleNameList.contains("monitor")) {
							//如果是班长
							//组长： 待解决
							//待解决: 主表(pre待解决+undo待确认+unsolved仍未解决),子表(done已指派)
							mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
									+","+QuestionStatusDbEnum.undo.toString()
									+","+QuestionStatusDbEnum.unsolved.toString());
							//状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决

							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(1);
							//班长状态:pre待指派、assign已指派、reply已回执
							childCondition.setStatus(QuestionStatusDbEnum.assign.toString()
									+","+QuestionStatusDbEnum.reply.toString());

							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);

						} else if (roleNameList.contains("team")) {
							//如果是组长
							//组长： 待解决
							//待解决 查 pre+ unsolved两个状态
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
									+","+QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
						} else if (roleNameList.contains(ConstantVar.solver)) {
							//不是技术人员领导，是技术人员
							//如果是技术管理中心的 未能解决的问题
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(3);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
							
						} else if (roleNameList.contains(ConstantVar.supervisor)) {
							//技术人员领导
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(4);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver3)) {
							//技术人员领导 +1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(5);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver4)) {
							//技术人员领导 +1+1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(6);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver5)) {
							//技术人员领导 +1+1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(7);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
							
						}
						
					}
				}
//			}
			break;
		case "NEED_SOLVED":// 
			//队长列表， 
			//如果是队长 //队长 查询未解决  '状态:pre待解决v、undo待确认v、unsolved仍未解决v、solved已解决'
			mainCondition.setStatus(ArrayUtility.toCommaString(new Object[]{
					QuestionStatusDbEnum.pre,QuestionStatusDbEnum.undo,QuestionStatusDbEnum.unsolved}));
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
			pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			break;
		case "NEED_CONFIRM"://QuestionStatusEnum_ZZ.NEED_CONFIRM.toString():
				//组长待确认
				mainCondition.setOwner(he);
				mainCondition.setStatus(QuestionStatusDbEnum.undo.toString());
				pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
			
			break;
		case "SOLVED":// QuestionStatusEnum_ZZ.SOLVED.toString():
				if (null != roles) {
					for (Role role : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
						if (roleNameList.contains("monitor")) {
							//如果是班长 //班长已解决
							//已解决: 主表(solved已解决),并且带上组长userid即可
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
						} else if (roleNameList.contains("team")) {
							//如果是组长 //组长已解决
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
						}else if (roleNameList.contains("captain")) {
							//如果是队长 //队长已解决  '状态:pre待解决v、undo待确认v、unsolved仍未解决v、solved已解决'
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							mainCondition.setDescribe("captainList");
							pager = questionService.findPage(mainCondition,null,pager,type,userId,keyword);
						}else if (roleNameList.contains(ConstantVar.solver)) {
//							//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(3);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
							
						}else{
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
							
						}
					}
				}
//			}

			break;
		case "NEED_ASSIGN":// QuestionStatusEnum_BZ.NEED_ASSIGN.toString():
			boolean isCoordinate=false;
			//存在指派的角色有班长和协调
			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.COORDINATOR)) {
						isCoordinate=true;
//						//协调列表 待指派: 子表(pre待指派)
						mainCondition.setStatus(QuestionStatusDbEnum.pre.toString());
//						mainCondition.setCoordinate(he);
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(2);//'1班长，2队部协,3技术',
						childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
						pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
					}
				}
			}
			if(isCoordinate==false){

				//班长查列表 待指派: 主表(pre待解决),子表(pre待指派)
				mainCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(1);
				childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			}
			
			break;
		case "PRE":// QuestionStatusEnum_JS.PRE.toString():

			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.supervisor) ||roleNameList.contains(ConstantVar.solver3) ||roleNameList.contains(ConstantVar.solver4) ||roleNameList.contains(ConstantVar.solver5) ||roleNameList.contains(ConstantVar.solver) ) {
						isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
						int c = 3;
						if(roleNameList.contains(ConstantVar.solver)){
							c=3;
						}
						if(roleNameList.contains(ConstantVar.supervisor)){
							c=4;
						}
						if(roleNameList.contains(ConstantVar.solver3)){
							c=5;
						}
						if(roleNameList.contains(ConstantVar.solver4)){
							c=6;
						}
						if(roleNameList.contains(ConstantVar.solver5)){
							c=7;
						}
						//如果是技术管理中心的 未能解决的问题
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(c);
						mainCondition.setDescribe("solverParentList");
						childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
						pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
					} 
					
				}
			}
			if(isParent == false ){//不是技术人员领导，是技术人员

				//技术: 需要关联查询问题解决人表,要看子表属于自已的
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(3);
				childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
			}
			break;
		case "DONE":// QuestionStatusEnum_JS.PRE.toString():

			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.supervisor)) {
						isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
						//如果是技术管理中心的 未能解决的问题
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(3);
						mainCondition.setDescribe("solverParentList");
						childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
						pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
					} 
					
				}
			}
			if(isParent == false ){//不是技术人员领导，是技术人员

				//技术: 需要关联查询问题解决人表,要看子表属于自已的
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(3);
				childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
				pager = questionService.findPage(mainCondition,childCondition,pager,type,null,keyword);
			}
			break;
		case "ASSIGNED"://协调列表
//			//协调列表 已指派: 子表(assign已指派)
//			mainCondition.setCoordinate(he);
			childCondition.setSolver(he.getId());
			childCondition.setSolverRole(2);//'1班长，2队部协,3技术',
			childCondition.setStatus(QuestionStatusDbEnum.assign.toString());
			pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			break;
		//我的问题列表
		case "MYREPLY":
			//我已回复的问题
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
			pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			break;
		case "MYUNSOLVED":
			//我未解决的问题
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
			pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			break;
		case "MYSOLVED":
			//我已解决的问题
			mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
			pager = questionService.findPage(mainCondition,childCondition,pager,type,userId,keyword);
			break;
		default:
			//技术: 需要关联查询问题解决人表,要看子表属于自已的
			break;
		}


		return pager.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<RollingQuestion> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
