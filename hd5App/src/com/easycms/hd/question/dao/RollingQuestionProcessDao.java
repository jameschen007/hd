package com.easycms.hd.question.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionProcess;

@Transactional
public interface RollingQuestionProcessDao  extends Repository<RollingQuestionProcess,Integer>,BasicDao<RollingQuestionProcess>{

	@Query("From RollingQuestionProcess ps where ps.question.id = ?1")
	List<RollingQuestionProcess> findByQuestionId(Integer id);

	@Modifying
	@Query("UPDATE RollingQuestionProcess w SET w.status=?2 WHERE w.question.id=?1 and w.solver=?3 and w.status='pre'")
	int updateStatusByQuestionIdAndSolv(Integer qId,String s,Integer solv);
	
	List<RollingQuestionProcess> findTopByQuestionAndSolverOrderByCreateTimeDesc(RollingQuestion id,Integer solver);
}
