package com.easycms.hd.question.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.question.dao.RollingQuestionDao;
import com.easycms.hd.question.dao.RollingQuestionProcessDao;
import com.easycms.hd.question.dao.RollingQuestionSolverDao;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;

@Service
public class RollingQuestionSolverServiceImp implements RollingQuestionSolverService{
	public static final Logger logger = Logger.getLogger(RollingQuestionSolverServiceImp.class);

	@Autowired
	private RollingQuestionDao rollingQuestionDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private RollingQuestionSolverDao RollingQuestionSolverDao;
	@Autowired
	private RollingQuestionProcessDao RollingQuestionProcessDao;

	@Autowired
	private VariableSetDao variableSetDao;
	
	
	
	@Override
	public RollingQuestionSolver add(RollingQuestionSolver p) {
		// 先查询是否已有解决人记录，有就只是更新了。 这个表只用于存储每个人的最终状态了。
		List<RollingQuestionSolver> solvs = RollingQuestionSolverDao.findByQuestionIdAndUserId(p.getQuestion().getId(), p.getSolver());
		if(solvs!=null && solvs.size()>0){
			RollingQuestionSolver s = null;
			for(RollingQuestionSolver up:solvs){
				up.setStatus(p.getStatus());
				s = RollingQuestionSolverDao.save(up);
			}
			return s;
		}else{
			return RollingQuestionSolverDao.save(p);
		}
	}

	@Override
	public RollingQuestionSolver update(RollingQuestionSolver p) {
		return RollingQuestionSolverDao.save(p);
	}

	@Override
	public List<RollingQuestionSolver> findPreSolverById(Integer id) {
		logger.info("pre:"+QuestionStatusDbEnum.pre.toString());
		List<RollingQuestionSolver> soList = RollingQuestionSolverDao.findByIdAndStatus(id,QuestionStatusDbEnum.pre.toString());
		return soList;
	}
	@Override
	public String findPreSolverNameById(Integer id) {

		String prePerson = "";
		List<RollingQuestionSolver> soList = findPreSolverById(id);
		if(soList==null || soList.size()<1){
			throw new RuntimeException("操作不正确，当前问题没有相应的待处理人员！"+id);
		}
		
		for(RollingQuestionSolver b:soList){
			Integer solverId = b.getSolver() ;
			if(solverId==null){
				
			}else{

				User u = userDao.findById(solverId);
				if(u!=null && u.getRealname()!=null){

					prePerson+= " "+ u.getRealname()+" ";
				}
			}
			
			
		}
		
		return prePerson;
	}
	

	/**
	 *　根据问题id 查询　当前待处理人的　id 
	 */
	@Override
	public List<Integer> findPreSolverIdById(Integer id) {

		String prePerson = "";
		List<RollingQuestionSolver> soList = findPreSolverById(id);
		if(soList==null || soList.size()<1){
			throw new RuntimeException("操作不正确，当前问题没有相应的待处理人员！"+id);
		}
		
		List<Integer> userId = new ArrayList<Integer>();
		for(RollingQuestionSolver b:soList){
			Integer solverId = b.getSolver() ;
			if(solverId==null){
				
			}else{

				userId.add(solverId);
			}
			
			
		}
		
		return userId;
	}


	@Override
	public List<RollingQuestionSolver> findByQuestionId(Integer id) {
		return RollingQuestionSolverDao.findByQuestionId(id);
	}

	@Override
	public List<RollingQuestionSolver> findByQuestionIdAndUserId(int problemId,
			Integer userid) {
		return RollingQuestionSolverDao.findByQuestionIdAndUserId(problemId,userid);
	}

	@Override
	public List<RollingQuestionSolver> findByUserId(String id) {
		return RollingQuestionSolverDao.findByUserId(id);
	}

//	@Override
//	public List<RollingQuestionSolver> findByUserIdAndStatus(String userid, String status) {
//		return RollingQuestionSolverDao.findByUserIdAndStatus(userid,status);
//	}

	@Override
	public List<RollingQuestionSolver> findByQuestionIdAndType(int problemId,int solverType){
		return RollingQuestionSolverDao.findByQuestionIdAndType(problemId,solverType);
	}
	@Override
	public List<RollingQuestionSolver> findByQuestionIdAndStatus(int problemId,String status,Integer solverType){
		return RollingQuestionSolverDao.findByQuestionIdAndStatus(problemId,status,solverType);
	}
	@Override
	public List<RollingQuestionSolver> findByQuestionIdAndSolverType(int problemId,Integer solverType){
		return RollingQuestionSolverDao.findByQuestionIdAndSolverType(problemId,solverType);
	}

	/**
	 * 如果是技术主管改派，要将前一个技术人员的记录修改为未能解决的 状态
	 * @param ques
	 * @return
	 */
	@Override
	public boolean setOlderUnsolved(RollingQuestion ques){
		boolean f = false ;
		//找出原技术人员的记录，将他的记录状态置为未能解决的 状态
		
		//如果是技术主管改派，要将前一个技术人员的记录修改为未能解决的 状态
		User oldassign = ques.getAssign();
		if(oldassign!=null){
			List<RollingQuestionSolver> list = RollingQuestionSolverDao.findByQuestionIdAndUserId(ques.getId(),ques.getAssign().getId());
			if(list!=null && list.size()>0){
				list.stream().forEach(old ->{
					old.setStatus(QuestionStatusDbEnum.unsolved.toString());
					old.setSolverTime(Calendar.getInstance().getTime());

					//同步将过程表的状态修改
					RollingQuestionProcessDao.updateStatusByQuestionIdAndSolv(ques.getId(),QuestionStatusDbEnum.unsolved.toString(),old.getSolver());
					
					RollingQuestionSolverDao.save(old);
					
					
				});
				f = true ;
			}
			
		}else{
			f = true ;
		}
		

		return f ;
		
	}
	/**
	 * 延时接口:技术主管为技术人员延时
	 * @param ques
	 * @return
	 */
	@Override
	public boolean setTimeoutForSolver(Integer questionId,User solver,Integer hour){
		RollingQuestion ques = rollingQuestionDao.findById(questionId);
		boolean f = false ;
		if(ques==null){
			return f ;
		}
		List<RollingQuestionSolver> list = RollingQuestionSolverDao.findByQuestionIdAndUserId(ques.getId(),solver.getId());
		if(list!=null && list.size()>0){//获得当前待办人记录
			if(list.size()>1){
				List<RollingQuestionSolver> list2 = list.stream().filter(e-> QuestionStatusDbEnum.pre.toString().equals(e.getStatus())).collect(Collectors.toList());
				if(list2.size()>1){
					logger.error("技术主管为技术人员延时，当前被指派人应该有且仅有一个"+ques.getId());
					return false ;
				}
				list= list2;
			}
			RollingQuestionSolver bean = list.get(0);
			

			Calendar c = Calendar.getInstance();
			Date create = bean.getCreateTime();
			if(create==null){
				create = Calendar.getInstance().getTime();
			}
			
			Date delay = bean.getDelayTime();
			if(delay==null){
				c.setTime(create);
				
				//取出系统中 作业问题限时 时间
				VariableSet set = variableSetDao.findByKey("changeSolver", "problem");
				int day3 = 3;
				if(set.getSetvalue()!=null && set.getSetvalue().matches("\\d+")){
					day3 = Integer.valueOf(set.getSetvalue());
				}
				int sysHour = day3*24;
				c.add(Calendar.HOUR, hour+sysHour);//延长时间后，到什么时间是到期时间存到数据库中。
				bean.setDelayTime(c.getTime());
			}else{
				c.setTime(delay);
				c.add(Calendar.HOUR, hour);//延长时间后，到什么时间是到期时间存到数据库中。
				bean.setDelayTime(c.getTime());
			}
			
			RollingQuestionSolverDao.save(bean);

			//同步将过程表的状态修改? 不用了。
			
			f = true ;
		}
		return f ;
	}
	

	@Override
	public RollingQuestionSolver findById(Integer id) {
		return RollingQuestionSolverDao.findById(id);
	}
	
	
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		Calendar c = Calendar.getInstance();
		
		Date old = c.getTime();
		System.out.println(sdf.format(old));
		c.add(Calendar.HOUR,1);
		System.out.println(sdf.format(c.getTime()));
		
		Calendar c2 = Calendar.getInstance();
		c2.setTime(old);
		System.out.println(sdf.format(c2.getTime()));
	}

}
