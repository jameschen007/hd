package com.easycms.hd.question.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.question.QuestionAnswerStatusDbEnum;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.dao.RollingPlanDao;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.RollingPlanFlag;
import com.easycms.hd.plan.mybatis.dao.RollingQuestionMybatisDao;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.problem.dao.ProblemFileDao;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.question.dao.RollingQuestionDao;
import com.easycms.hd.question.dao.RollingQuestionProcessDao;
import com.easycms.hd.question.dao.RollingQuestionSolverDao;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionProcess;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.dao.VariableSetDao;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

@Service
public class RollingQuestionServiceImpl implements RollingQuestionService {
	
	public static Logger logger = Logger.getLogger(RollingQuestionServiceImpl.class);
	
	@Autowired
	private RollingQuestionDao rollingQuestionDao;
	@Autowired
	private RollingQuestionSolverDao rollingQuestionSolverDao;
	@Autowired
	private RollingQuestionSolverService rollingQuestionSolverService;
	@Autowired
	private RollingQuestionProcessDao rollingQuestionProcessDao;
	@Autowired
	private UserDao userDao ;
	@Autowired
	private RollingPlanDao rollingPlanDao;
	@Autowired
	private VariableSetDao variableSetDao;
	@Autowired
	private JPushService jPushService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	@Autowired
	private UserService userService;
	@Autowired
	private RollingQuestionMybatisDao rollingQuestionMybatisDao;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private ProblemFileDao problemFileDao;

	@Override
	public RollingQuestion add(RollingQuestion p) {
		return rollingQuestionDao.save(p);
	}

	@Override
	public RollingQuestion update(RollingQuestion p) {
		return rollingQuestionDao.save(p);
	}

	@Override
	public List<RollingQuestion> findByIds(Integer[] aids) {
		return rollingQuestionDao.findByIdS(aids);
	}

	@Override
	public RollingQuestion findById(Integer id) {
		RollingQuestion ques = rollingQuestionDao.findById(id);
		if(ques!=null){
			List<RollingQuestion> child = rollingQuestionDao.findByUpId(ques.getId());
			ques.setChildren(child);
		}
		return ques;
	}
	

	@Override
	public List<RollingQuestion> findByUpId(Integer id) {
		return rollingQuestionDao.findByUpId(id);
	}

	@Override
	public List<RollingQuestion> findByWorkstepId(int worstepid) {
//		return rollingQuestionDao.findByWorkstepId(worstepid);
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public List<RollingQuestion> findByUserId(String userid) {
		return rollingQuestionDao.findByUserId(userid);
	}

	@Override
	public List<RollingQuestion> findByIdsAndStatus(Integer[] ids, String status) {
		return rollingQuestionDao.findByIdsAndStatus(ids, status);
	}

	@Override
	public List<RollingQuestion> findByUserIdAndConfirm(String userid, String con) {
		return rollingQuestionDao.findByIdAndStatus(userid, con);
	}

	@Override
	public Page<RollingQuestion> findByIds(Integer[] ids, Page<RollingQuestion> page) {
//		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByIdInOrderByCreateOnDesc(ids,
//				pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public Page<RollingQuestion> findByIds(Integer[] ids, String keyword, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByIdInOrderByCreateOnDesc(ids,
				keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingQuestion> findByWorkstepId(int worstepid, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao
//				.findByWorstepidOrderByCreateOnDesc(worstepid, pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public Page<RollingQuestion> findByUserId(String userid, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByCreatedByOrderByCreateOnDesc(userid,
//				pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public Page<RollingQuestion> findByUserId(String userid, String keyword, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByCreatedByOrderByCreateOnDesc(userid,
				keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingQuestion> findByIdsAndStatus(Integer[] ids, QuestionStatusDbEnum status, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByIdInAndIsOkOrderByCreateOnDesc(ids,
				status.toString(), pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingQuestion> findByIdsAndStatus(Integer[] ids, String status, String keyword, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByIdInAndIsOkOrderByCreateOnDesc(ids,
				status, keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingQuestion> findByUserIdAndConfirm(String userid, String confirm, Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao
//				.findByCreatedByAndConfirmOrderByCreateOnDesc(userid, confirm, pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public boolean delete(RollingQuestion p) {
		return rollingQuestionDao.deleteById(p.getId()) > 0;
	}

	@Override
	public List<RollingQuestion> findByRollingPlanIdAndIsNotOk(int rolingPlanId) {
		return rollingQuestionDao.findByRollingPlanIdAndIsOk(rolingPlanId);
	}

	@Override
	public int countByUserId(String userid) {
		return rollingQuestionDao.countByUserId(userid);
	}

	@Override
	public int findCountByUserIdAndConfirm(String userid, String i) {
		return rollingQuestionDao.findCountByUserIdAndConfirm(userid, i);
	}

	@Override
	public int findCountConcernedNotSolved(Integer[] ids) {
		return rollingQuestionDao.findCountConcernedNotSolved(ids);
	}

	@Override
	public int findByNeedToSolveCount(Integer[] ids) {
		return rollingQuestionDao.findByNeedToSolveCount(ids);
	}

	@Override
	public List<RollingQuestion> findByRollingPlanId(int rid) {
		return rollingQuestionDao.findByRollingPlanId(rid);
	}

	@Override
	public Page<RollingQuestion> findByRollingPlanId(int id, Page<RollingQuestion> page) {
//		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findByRollingPlanIdOrderByCreateOnDesc(id,
//				pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public int findCountConcerned(Integer[] ids) {
		return rollingQuestionDao.findCountConcerned(ids);
	}

	@Override
	public List<RollingQuestion> findConfirmByIdsAndStatus(Integer[] ids, String i) {
		return rollingQuestionDao.findConfirmByIdsAndStatus(ids, i);
	}

	@Override
	public Page<RollingQuestion> findConfirmRollingQuestionsByIds(Integer[] ids, String i, Page<RollingQuestion> page) {
//		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao
//				.findByIdInAndConfirmOrderByCreateOnDesc(ids, i, pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public Page<RollingQuestion> findConfirmRollingQuestionsByIds(Integer[] ids, String i, String keyword, Page<RollingQuestion> page) {
//		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
//
//		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao
//				.findByIdInAndConfirmOrderByCreateOnDesc(ids, i, keyword, pageable);
//
//		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
//				springPage.getContent());
//		return page;
		logger.error("没有实现 的方法被调用！！");
		return null;
	}

	@Override
	public Page<RollingQuestion> findAll(Page<RollingQuestion> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findAll(null, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
	/**
	 * type 滚动计划的类型
	 * 
	 * @param keyword 问题列表搜索关键字
	 */
    @Override
    public Page<RollingQuestion> findPage(final RollingQuestion condition,final RollingQuestionSolver solver,Page<RollingQuestion> page,final String type, Integer userId,String keyword) {
        Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
        boolean captainList = ( condition!=null&&"captainList".equals(condition.getDescribe()));
        boolean solverParentList = ( condition!=null&&"solverParentList".equals(condition.getDescribe()));
        logger.debug("==> Find all RollingQuestion by condition.");
        if(condition!=null){

            logger.info(condition.getQuestionType());
            if(condition.getOwner()!=null){
            	logger.info("condition.getOwner().getId()="+condition.getOwner().getId());
            	logger.info(condition.getOwner().getRealname());
            }

            if(condition.getCoordinate()!=null){
            	logger.info("condition.getOwner().getId()="+condition.getCoordinate().getId());
            	logger.info(condition.getCoordinate().getRealname());
            }
            logger.info("condition.getStatus()="+condition.getStatus());
            logger.info(condition.getRollingPlanId());
        }
        if(solver!=null){
        	logger.info("solver.getSolver()="+solver.getSolver());
        	logger.info("solver.getSolverRole()="+solver.getSolverRole());
        	logger.info("solver.getStatus()="+solver.getStatus());
        	if(solver.getQuestion()!=null){
        		logger.info("solver.getQuestion().getId()="+solver.getQuestion().getId());
        	}
        }
        
        
        Specification<RollingQuestion> spc = new Specification<RollingQuestion>(){

			@Override
			public Predicate toPredicate(Root<RollingQuestion> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				Path<RollingQuestion> path= root.get("id");
				Path<RollingQuestion> path3= root.get("id");
				Path<Integer> path_type= root.get("rollingPlanId");
				Predicate predicates = null ;
				//需要关联子查询
				if(solver!=null && solver.getSolver()!=null){
					Subquery<RollingQuestion> queryTwo=query.subquery(RollingQuestion.class);
					//声明子查询的对象  
					Root<RollingQuestionSolver> rootTwo = ((Root<RollingQuestionSolver>)queryTwo.from(RollingQuestionSolver.class));
					queryTwo.select(rootTwo.<RollingQuestion>get("question"));

					predicates = cb.conjunction();
					Predicate preTwo = null;
					//技术人员的领导列表查询，需要关联查询
					if(solverParentList){
						Path<Integer> path_solver= rootTwo.get("solver");

						Subquery<Integer> querysolver=queryTwo.subquery(Integer.class);
						Root<User> rootsolver = ((Root<User>)querysolver.from(User.class));
//						Join<User,User> joinB = rootsolver.join("parent");//技术人员的领导
						Predicate presolver =cb.equal(rootsolver.get("id"),   solver.getSolver());
						logger.info("技术人员的领导列表查询，需要关联查询　ec_user.id="+solver.getSolver());
						querysolver.select(rootsolver.get("id"));
						querysolver.where(presolver);
						preTwo = cb.in(path_solver).value(querysolver);
					}else{
						//普通当前待处理人。
						preTwo = cb.equal(rootTwo.get("solver").as(Integer.class), solver.getSolver());
						logger.info("子表：普通当前待处理人　rolling_question_solver.solver="+solver.getSolver());
					}
					
					if(solver!=null && CommonUtility.isNonEmpty(solver.getStatus())){
						String st = solver.getStatus();
						if(st.indexOf(",")!=-1){
							In<String> in = cb.in(rootTwo.get("status").as(String.class));
							for(String x:st.split(",")){
								in.value(x);
							}
							preTwo = cb.and(preTwo,in);
							logger.info("子表：状态　rolling_question_solver.status in : "+st);
						}else{
							preTwo = cb.and(preTwo,cb.equal(rootTwo.get("status").as(String.class), solver.getStatus()));
							logger.info("子表：状态　rolling_question_solver.status = "+st);
						}
					}
					if(solver.getSolverRole()!=null){
						if(solver.getSolverRole().intValue()==1){
							preTwo = cb.and(preTwo,cb.equal(rootTwo.get("solverRole").as(Integer.class), solver.getSolverRole()));
							logger.info("子表：处理角色　rolling_question_solver.solver = "+solver.getSolverRole());
						}else{
							preTwo = cb.and(preTwo,cb.equal(rootTwo.get("solverRole").as(Integer.class), solver.getSolverRole()));
							logger.info("子表：处理角色　rolling_question_solver.solver = "+solver.getSolverRole());
						}
					}
					
					
					if(solver.getSolverTime()!=null){

//						preTwo = cb.and(preTwo,cb.gt(rootTwo.get("visitStatus").as(String.class), "Y"));
					}
					queryTwo.where(preTwo);
					
					predicates = cb.and(predicates,cb.in(path).value(queryTwo));
				}
				if(predicates==null){
					predicates =cb.gt(root.get("rollingPlanId").as(Integer.class), 0);
				}else{
					predicates =cb.and(predicates,cb.gt(root.get("rollingPlanId").as(Integer.class), 0));
				}
				
				if(condition!=null && condition.getOwner()!=null){
					
					if(captainList){
						//如果是队长列表查询，需要查到班长，再查到组长的问题。
//						Join<User,User> joinA = root.join("owner");//问题-组长
//						Join<User,User> joinB = joinA.join("parent");//组长-班长
//						Join<User,User> joinC = joinB.join("parent");//
//						predicates = cb.and(predicates,cb.equal(joinC,  condition.getOwner()));
					}else{
						//两张表关联查询 组长查自已的列表
		                Join<RollingQuestion,User> userJoin = root.join(
		                		root.getModel().getSingularAttribute("owner",User.class),JoinType.LEFT);
		                predicates = cb.and(predicates,cb.equal(userJoin, condition.getOwner()));
						logger.info("组长查自已的列表　rolling_question.owner_id= "+condition.getOwner().getId());
					}
				}

				if(condition!=null&&condition.getCoordinate()!=null){//如果是协调人员进来查看时,看班长的
					Predicate predicatesc1 = null ;
					Predicate predicatesc2 = null ;
					Subquery<RollingQuestion> queryTwo3 = query.subquery(RollingQuestion.class);
					// 声明子查询的对象  
					Root<RollingQuestionSolver> rootTwo3 = ((Root<RollingQuestionSolver>) queryTwo3.from(RollingQuestionSolver.class));
					queryTwo3.select(rootTwo3.<RollingQuestion> get("question"));
					predicatesc1 = cb.conjunction();
					Predicate preTwo3 = null;
					preTwo3 = cb.equal(rootTwo3.get("solver").as(Integer.class), condition.getCoordinate().getId());
					logger.info("协调 查子表：谁协调　rolling_question_solver.solver = "+ condition.getCoordinate().getId());
					preTwo3 =cb.and(preTwo3,cb.equal(rootTwo3.get("status").as(String.class), solver.getStatus()));
					logger.info("协调 查子表：状态　rolling_question_solver.status = "+solver.getStatus());
					preTwo3 =cb.and(preTwo3,cb.and(preTwo3, cb.equal(rootTwo3.get("solverRole").as(Integer.class), 2)));//协调人员进来，有自已的记录
					logger.info("协调 查子表：处理角色　rolling_question_solver.status = "+2);
					queryTwo3.where(preTwo3);
					predicates = cb.and(predicatesc1, cb.in(path3).value(queryTwo3));

					Subquery<RollingQuestion> queryTwo = query.subquery(RollingQuestion.class);
					// 声明子查询的对象  
					Root<RollingQuestionSolver> rootTwo = ((Root<RollingQuestionSolver>) queryTwo.from(RollingQuestionSolver.class));
					queryTwo.select(rootTwo.<RollingQuestion> get("question"));
					predicatesc2 = cb.conjunction();
					Predicate preTwo = null;
					preTwo = cb.equal(rootTwo.get("solver").as(Integer.class), userId);
					logger.info("协调进入时 查２子表：处理班长的id　rolling_question_solver.solver = "+userId);
					preTwo = cb.and(preTwo, cb.equal(rootTwo.get("solverRole").as(Integer.class), 1));//看班长的
					logger.info("协协调进入时 查２子表：处理角色　rolling_question_solver.status = "+1);
					queryTwo.where(preTwo);
					predicates = cb.and(predicates, cb.and(predicatesc2, cb.in(path).value(queryTwo)));

				}else{
					if(userId!=null && userId>0){//队长、班长 查看的是哪个组长的列表 
						predicates = cb.and(predicates,cb.equal(root.get("owner"), userDao.findById(userId)));
						logger.info("队长、班长 查看的是哪个组长的列表　rolling_question.owner = "+userId);
					}
				}
				
				//查询滚动计划类型
				if(CommonUtility.isNonEmpty(type)){
					Subquery<Integer> queryTwo=query.subquery(Integer.class);
					Root<RollingPlan> rootTwo = ((Root<RollingPlan>)queryTwo.from(RollingPlan.class));
					queryTwo.select(rootTwo.get("id"));
					Predicate preTwo =cb.equal(rootTwo.get("type").as(String.class), type);
					logger.info("查询滚动计划类型　rolling_question.type = "+type);
					preTwo = cb.and(preTwo,cb.equal(rootTwo.get("id"), root.get("rollingPlanId")));
					
					queryTwo.where(preTwo);
					predicates = cb.and(predicates,cb.in(path_type).value(queryTwo));
				}
				
				//查询问题状态
				if(condition!=null && CommonUtility.isNonEmpty(condition.getStatus())){
					String segment = condition.getStatus();
					if(segment.indexOf(",")!=-1){
						In<String> in = cb.in(root.get("status").as(String.class));
						for(String x:segment.split(",")){
							in.value(x);
						}
						predicates=cb.and(predicates,in);
						logger.info("状态　rolling_question.status in : "+segment);
					}else{
						predicates = cb.and(predicates,cb.equal(root.get("status").as(String.class), condition.getStatus()));
						logger.info("状态　rolling_question.status = "+condition.getStatus());
					}
				}
				//问题列表搜索关键字，keyword，所有字段都搜索？
				if(CommonUtility.isNonEmpty(keyword)){
					
					predicates = cb.and(predicates,cb.like(root.get("describe").as(String.class),"%"+keyword+"%"));
					logger.info("描述　rolling_question.describe = %"+keyword+"%");
				}
				if(condition!=null && condition.getCoordinate()!=null){//如果是协调人员查询列表时
					predicates = cb.and(predicates,cb.equal(root.get("coordinate").as(User.class), condition.getCoordinate()));
					logger.info("协调人员　rolling_question.coordinate = "+condition.getCoordinate());
				}
				
				return query.where(predicates).getRestriction();
				
			}
        	
        };
        
        
        org.springframework.data.domain.Page<RollingQuestion> springPage = rollingQuestionDao.findAll(spc,pageable);
        page.execute(Integer.valueOf(Long.toString(springPage
                .getTotalElements())), page.getPageNum(), springPage.getContent());
        return page;

    }
/**
 * 组长回答问题解决情况
 * @throws Exception 
 */
    @Transactional(propagation=Propagation.REQUIRED)
	@Override
	public boolean answer(Integer questionId, QuestionAnswerStatusDbEnum answer,String reason,CommonsMultipartFile[] files,HttpServletRequest request) throws Exception {
		
		boolean f = false ;
		try{
			// 
			//修改主表的状态
			RollingQuestion question = rollingQuestionDao.findById(questionId);
			
			
			//查询出处理人未处理的问题回执数据
			List<RollingQuestion> queFeedBacks = rollingQuestionDao.findByUpId(question.getId());
			RollingQuestion queFeedBack = queFeedBacks.stream().filter(q -> {
				return QuestionStatusDbEnum.pre.toString().equals(q.getStatus());
			}).collect(Collectors.toList()).get(0);


			List<RollingQuestionSolver> solvers = null;
//			solvers = rollingQuestionSolverDao.findByQuestionIdAndType(questionId, 3);//技术

			if(queFeedBack != null){				
				solvers = rollingQuestionSolverDao.findByQuestionIdAndUserId(questionId, queFeedBack.getOwner().getId());
			}
			
			if(solvers==null || solvers.size()==0){
				logger.error("组长回答问题解决情况出错,没有查出技术人员的记录"+questionId);
			}else{
				RollingQuestionSolver solver = null ;
				for(RollingQuestionSolver s:solvers){
					if(QuestionStatusDbEnum.reply.toString().equals(s.getStatus())){
						solver = s ;
						break;
					}
				}
				//TODO 如果原来是技术员待办的问题，其它人（如班长)回复了，这种情况怎么处理？？
				if(solver!=null){

					//修改当前解决人的状态
					solver.setStatus(answer.toString());
					//给出不通过原因
					solver.setReason(reason);
					rollingQuestionSolverDao.save(solver);
					//同步将过程表的状态修改
					updateProcess(question,solver.getSolver(),solver);
					
					if(answer==QuestionAnswerStatusDbEnum.unsolved){
						
						

						if(files!=null &&files.length>0 ){
							RollingQuestion quesTeam = null;
							for(RollingQuestion tea:queFeedBacks){
								if(QuestionApiService.teamAnswer.equals(tea.getStatus())){
									quesTeam = tea ;
								}
							}
							// 组长反馈不通过时，所需入参：问题id,问题反馈，附件
							// 将回执信息入问题表子记录
							// 入附件
							if(quesTeam==null){
								quesTeam = new RollingQuestion();
								quesTeam.setRollingPlanId(question.getRollingPlanId());
								quesTeam.setQuestionType(question.getQuestionType());
								quesTeam.setDescribe(reason);
								quesTeam.setStatus(QuestionApiService.teamAnswer);
								quesTeam.setOwner(question.getOwner());
								quesTeam.setQuestionTime(Calendar.getInstance().getTime());
								quesTeam.setUp(question);
								this.rollingQuestionDao.save(quesTeam);
							}

							//上传附件
							if (files!=null &&files.length>0 && !moreUpload(quesTeam.getId(), files,request)) {
								logger.debug("你上传了不通过问题的图片。。。。。。。。。");
							}
						}

						
						
						//如果组长不认同技术人员的解决方案,该问题反馈到技术领导
						if(solver.getSolverRole() == 3){
							
							
							addSolverParent(solver);
						}else{

							
							//如果问题仍未解决,在问题当前处理人处继续走流程
							//为当前处理人新加一条待处理记录
							RollingQuestionSolver solverAgain = new RollingQuestionSolver();
							solverAgain.setQuestion(question);
							solverAgain.setSolver(solver.getSolver());
							solverAgain.setSolverRole(solver.getSolverRole());
							solverAgain.setStatus(QuestionStatusDbEnum.pre.toString());
							

							RollingQuestionProcess process = new RollingQuestionProcess();
							BeanUtils.copyProperties(solverAgain, process);
							this.rollingQuestionProcessDao.save(process);//保存处理过程
							
							rollingQuestionSolverService.add(solverAgain);
						}
						
						//班长和协调回执的问题未被接受，更新主表状态为待解决
						if(solver.getSolverRole() == 1 || solver.getSolverRole() == 2){
							question.setAssign(null);
							question.setStatus(QuestionStatusDbEnum.pre.toString());
						}else{
							question.setStatus(answer.toString());						
						}
					}else{
						//修改主表状态
						question.setStatus(answer.toString());
						
						RollingPlan rp  = rollingPlanDao.findById(question.getRollingPlanId());
						//修改滚动计划状态
						rp.setDoissuedate(Calendar.getInstance().getTime());
						
						boolean rpFg = false;
						//查询 问题id 父问题id空，如果所有都是 solved已解决 状态，则更新滚动计划状态
						List<RollingQuestion> quesList = rollingQuestionDao.findByRollingPlanId(question.getRollingPlanId());
						rpFg =quesList.stream().filter(ques->{
							return false == QuestionAnswerStatusDbEnum.solved.toString().equals(ques.getStatus());
								}).collect(Collectors.toList()).isEmpty();
						if(rpFg){
							rp.setRollingplanflag(RollingPlanFlag.COMPLETED);
							executePushToEnpowerServiceImpl.updateEnpowerStatus(rp, null,"");
						}
//						if(0==rp.getIsend().intValue()){
//							rp.setIsend(1);
//						}
						rollingPlanDao.save(rp);
					}
					
					//修改主表的状态
					question.setReason(reason);
					rollingQuestionDao.save(question);
					
					//更新处理人回执数据状态为已处理
					queFeedBack.setStatus(QuestionStatusDbEnum.done.toString());
					rollingQuestionDao.save(queFeedBack);

					User solvering = userDao.findById(solver.getSolver());

					if (null != solvering) {
						String message = question.getOwner().getRealname() + " 回答了作业问题，请及时处理。";
				        JPushExtra extra = new JPushExtra();
						extra.setCategory(JPushCategoryEnums.QUESTION_ANSWER.name());
						jPushService.pushByUserId(extra, message, "作业问题回答", solver.getSolver());
					}
				}else{
					logger.error("组长回答问题解决情况时没有找到技术人员！questionId="+questionId);
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return f;
	}
	
	/**
	 * 对作业问题的超时状态变更
	 * 1 班长提出的问题，超过（3天+延时小时）时间，技术工程师的状态还是处于待处理状态的，
	 * 2 将技术工程师的记录状态标识为超时，且将作业问题主状态标识为unsolved,主记录的超时状态标识加上
	 */

    @Transactional(propagation=Propagation.REQUIRED)
	public boolean timeoutCompute(){
		boolean f = false ;

		Calendar c = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		Date sysdate = c.getTime();
		//取出系统中 作业问题限时 时间
		VariableSet set = variableSetDao.findByKey("changeSolver", "problem");
		int day3 = 3;
		if(set.getSetvalue()!=null && set.getSetvalue().matches("\\d+")){
			day3 = Integer.valueOf(set.getSetvalue());
		}
		int sysHour = day3*24;
		c2.add(Calendar.HOUR, -sysHour);//当前时间减去规定完成时间长度 大于创建时间则超时。
		Date sysdateMinus3 = c2.getTime();
		List<RollingQuestionSolver> solverList1 = rollingQuestionSolverDao.findTimeoutSolverPlus3(sysdateMinus3, QuestionStatusDbEnum.pre.toString(), 3);
		//当前时间大于超时限制结束时间则超时
		List<RollingQuestionSolver> solverList2 = rollingQuestionSolverDao.findTimeoutSolver(sysdate, QuestionStatusDbEnum.pre.toString(), 3);
    	//查询已超时的记录
		List<RollingQuestionSolver> solverList = new ArrayList<RollingQuestionSolver>();
		if(solverList1!=null && solverList1.size()>0){
			solverList.addAll(solverList1);
		}
		if(solverList2!=null && solverList2.size()>0){
			solverList.addAll(solverList2);
		}
		if(solverList.size()>0){
			try{
				solverList.stream().forEach(bn->{
			    	//修改子记录状态和超时状态
			    	bn.setStatus(QuestionStatusDbEnum.unsolved.toString());
			    	bn.setTimeout(true);
			    	bn.setUnableReason("超时未解决");
			    	rollingQuestionSolverDao.save(bn);

					//同步将过程表的状态修改
					updateProcess(bn.getQuestion(),bn.getSolver(),bn);
			    	
			    	//超时给解决人上级添加一条待解决记录
			    	if(!addSolverParent(bn)){
			    		logger.error("问题升级至解决人上级失败！解决人ID："+bn.getSolver());
			    	}
			    	
			    	//修改作业问题主记录状态和超时状态
			    	RollingQuestion que = bn.getQuestion();
			    	que.setStatus(QuestionStatusDbEnum.unsolved.toString());
			    	que.setTimeout(true);
			    	rollingQuestionDao.save(que);

				});
				return true ;
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}else{
			return true ;
		}
		
		return f ;
	}
    @Transactional(propagation=Propagation.REQUIRED)
	@Override
	public boolean unable(Integer questionId,String reason,BaseRequestForm baseRequestForm)  throws Exception {
		RollingQuestion question = rollingQuestionDao.findById(questionId);
		if(question == null){
			logger.error("查询问题数据为空，问题ID："+questionId);
			return false;
		}
		
		//修改状态为未解决
		question.setStatus(QuestionStatusDbEnum.unsolved.toString());
		
		//查询当前登录人的待处理数据
		List<RollingQuestionSolver> solvers = rollingQuestionSolverDao.findByQuestionIdAndUserId(question.getId(), baseRequestForm.getLoginId());
		List<RollingQuestionSolver> solverPre = solvers.stream().filter(s -> {
			return QuestionStatusDbEnum.pre.toString().equals(s.getStatus());
		}).collect(Collectors.toList());
		if(solverPre == null || solverPre.isEmpty()){
			logger.error("查询处理人数据为空，问题ID："+question.getId());
			return false;
		}
		RollingQuestionSolver solver = solverPre.get(0);

		//问题升级
		addSolverParent(solver);
		
		//更新问题处理人的状态为未解决，并注明原因
		solver.setStatus(QuestionStatusDbEnum.unsolved.toString());
		solver.setUnableReason(reason);
		rollingQuestionSolverDao.save(solver);

		//同步将过程表的状态修改
		updateProcess(question,solver.getSolver(),solver);
		
		rollingQuestionDao.save(question);
		
		
		return true;
	}
	
	/**
	 * 问题升级至解决人上级领导
	 * @param solver
	 * @return
	 */
	public boolean addSolverParent(RollingQuestionSolver solver){
		if(solver == null){
			logger.error("解决人为空");
			return false;
		}
		//问题升级
		//为解决人的上级添加一条待解决记录，状态为“pre待处理”
		RollingQuestionSolver solverParent = new RollingQuestionSolver();
		solverParent.setQuestion(solver.getQuestion());

		Integer solverParentId = null;
		Integer solverRole = solver.getSolverRole();
		
		RollingPlan rollingPlan = rollingPlanDao.findById(solver.getQuestion().getRollingPlanId());
		//查上级
		List<User> parents = null;
		User parent = null;

		if(solverRole == 3){
//			parents = userService.findUserByDepartmentAndRoleNames(departmentService.findById(358).getChildren(), new String[]{ConstantVar.supervisor});
			//[2017-12-27]技术领导按角色专业查询

			//直接上级首先取人员的直接领导
			User currentSoler = userService.findUserById(solver.getSolver());
			if(currentSoler!=null){
				parent = currentSoler.getParent();
			}
			if(parent!=null){
				solverParentId = parent.getId();
			}else{
				parents = userService.findUserByDepartmentVariable("roleType", ConstantVar.supervisor, rollingPlan.getType());
			}
		}else if(solverRole == 4){
//			parents = userService.findUserByDepartmentAndRoleNames(departmentService.findById(314).getChildren(), new String[]{ConstantVar.solver3});	
			parents = userService.findUserByDepartmentVariable("roleType", ConstantVar.solver3, rollingPlan.getType());
		}else if(solverRole == 5){
			//查经理部滚动计划作业问题副总经理（技术）
			parents = userService.findUserByRoleAndDepartmentVariable(ConstantVar.solver4,"ManagerDepartment");
		}else{
			//查经理部 项目总经理
			parents = userService.findUserByRoleAndDepartmentVariable(ConstantVar.solver5,"ManagerDepartment");
		}
		
		
		if(solverParentId==null && parents != null && parents.size()>0){
			parent = parents.get(0) ;//随便获取一个父级人员
			solverParentId = parent.getId();
		}
		
		
		if(solverParentId == null){
			logger.error("上级人员ID为空："+solverParentId);
			throw new RuntimeException("没有查询到解决人上级领导！");
		}
		solverParent.setSolver(solverParentId);
		RollingQuestion question = solver.getQuestion();
		question.setAssign(parent);
		//修改主表指派人
		rollingQuestionDao.save(question);
		
		//上级人员Role=当前解决人Role+1，层层升级
		solverParent.setSolverRole(solver.getSolverRole() + 1);			
		solverParent.setStatus(QuestionStatusDbEnum.pre.toString());
		logger.debug("上级人员ID:"+solverParentId+",solverRole："+solverParent.getSolverRole());
		RollingQuestionSolver solverUp = rollingQuestionSolverDao.save(solverParent);
		
		RollingQuestionProcess process = new RollingQuestionProcess();
		BeanUtils.copyProperties(solverParent, process);
		this.rollingQuestionProcessDao.save(process);//保存处理过程
		
		User solverOld = userService.findUserById(solver.getSolver());
		//推送消息
		if (null != solverUp && null != solverOld) {
			String message = solverOld.getRealname() + " 无法解决作业问题，请及时处理。";
	        JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.QUESTION_ANSWER.name());
			jPushService.pushByUserId(extra, message, "无法解决问题", solverUp.getSolver());
		}

		return true;
	}

	@Override
	public Integer getStatistics(String type, Integer userId, String... status) {
		if(status.length==1){
			return rollingQuestionMybatisDao.getStatistics(status[0], userId, type);
		}else{
			Map<String,Object> parm = new HashMap<String,Object>();
			parm.put("statuss", status);
			parm.put("userId", userId);
			parm.put("type", type);
			return rollingQuestionMybatisDao.getStatisticsByMap(parm);
		}
		
	}
	


	private RollingQuestionProcess updateProcess(RollingQuestion ques,Integer solverId,RollingQuestionSolver solver){
		//同步将过程表的状态修改
		List<RollingQuestionProcess> prs = rollingQuestionProcessDao.findTopByQuestionAndSolverOrderByCreateTimeDesc(ques, solverId);
		if(prs!=null && prs.size()>=1){
			RollingQuestionProcess pr = prs.get(0);
			pr.setReason(solver.getReason());
			pr.setStatus(solver.getStatus());
			pr.setUnableReason(solver.getUnableReason());
			
			return rollingQuestionProcessDao.save(pr);
		}
		return null; 
	}
	
	//以下是被上面的入口方法调用的方法
	private boolean moreUpload(Integer problemId, CommonsMultipartFile files[], HttpServletRequest request) {
		// 上传位置  设定文件保存的目录 
		String filePath = com.easycms.common.logic.context.ContextPath.fileBasePath+com.easycms.common.util.FileUtils.questionFilePath;
		List<FileInfo> fileInfos = UploadUtil.moreUpload(filePath, "utf-8", true, files);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				ProblemFile file = new ProblemFile();
				file.setPath(fileInfo.getNewFilename());
				file.setTime(Calendar.getInstance().getTime());
				file.setProblemid(problemId);
				if(fileInfo.getFilename()!=null && fileInfo.getFilename().length()>50){
					file.setFileName(fileInfo.getFilename().substring(0, 50));
				}else{
					file.setFileName(fileInfo.getFilename());
				}
				logger.debug("upload file success : " + file.toString());
				problemFileDao.save(file);
//				problemFileService.add(file);
			}
		}

		return true;
	}
}
