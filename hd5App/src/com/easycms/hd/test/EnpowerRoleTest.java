package com.easycms.hd.test;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.hd.api.enpower.domain.EnpowerRoleDomain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class EnpowerRoleTest {

	public static void main(String[] args) throws Exception {
		String roleTree = FileUtils.readFileContent("E:\\Git_Repository\\role_tree.json");
		
		Gson gson = new Gson();
		
		EnpowerRoleDomain enpowerRoleDomain = gson.fromJson(roleTree,
				new TypeToken<EnpowerRoleDomain>() {
				}.getType());
		
		System.out.println(CommonUtility.toJson(enpowerRoleDomain));
	}
}
