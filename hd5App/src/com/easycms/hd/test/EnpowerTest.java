package com.easycms.hd.test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.hd.api.enpower.domain.EnpowerRoleDomain;
import com.easycms.hd.api.enpower.domain.EnpowerRoleEntity;
import com.easycms.hd.api.enpower.response.EnpowerResponseDepartment;
import com.easycms.hd.api.enpower.response.EnpowerResponseRole;
import com.easycms.hd.api.enpower.response.EnpowerResponseUser;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.management.user.domain.Role;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class EnpowerTest {
	public static void main(String[] args) {
		Gson gson = new Gson();
		String userInfos = FileUtils.readFileContent("E:\\Git_Repository\\人员信息.json");
		String departmentInfos = FileUtils.readFileContent("E:\\Git_Repository\\部门信息.json");

		List<EnpowerResponseUser> enpowerResponseUsers = null;
		if (CommonUtility.isNonEmpty(userInfos)) {
			enpowerResponseUsers = gson.fromJson(userInfos, new TypeToken<List<EnpowerResponseUser>>() {
			}.getType());
		}
		
		Map<String, List<EnpowerResponseUser>> mapperEnpowerUsers = enpowerResponseUsers.stream().collect(Collectors.groupingBy(EnpowerResponseUser::getUserId));
		System.out.println(mapperEnpowerUsers.size());
		System.out.println(CommonUtility.toJson(mapperEnpowerUsers));
		List<EnpowerResponseUser> collectEnpowerUsers = mapperEnpowerUsers.entrySet().stream().map(mapper -> {
			List<EnpowerResponseUser> values = mapper.getValue();
			
			EnpowerResponseUser result = values.get(0);
			Set<Modules> modules = values.stream().map(value -> {
				Modules module = new Modules();
				module.setName(value.getSpecialty());
				return module;
			}).collect(Collectors.toSet());
			result.setModules(modules);
			
			return result;
		}).collect(Collectors.toList());
		System.out.println(collectEnpowerUsers.size());
		System.out.println(CommonUtility.toJson(collectEnpowerUsers));
		
		Set<Modules> modules = new HashSet<>();
		collectEnpowerUsers.stream().forEach(user -> {
			modules.addAll(user.getModules());
		});
		modules.stream().forEach(module -> {
			System.out.println(module.getName());
		});
		
		List<EnpowerResponseDepartment> enpowerResponseDepartments = null;
		if (CommonUtility.isNonEmpty(departmentInfos)) {
			enpowerResponseDepartments = gson.fromJson(departmentInfos,
					new TypeToken<List<EnpowerResponseDepartment>>() {
					}.getType());
		}

		if (null != enpowerResponseDepartments && null != enpowerResponseUsers) {
			List<EnpowerResponseDepartment> parentDepartments = enpowerResponseDepartments.stream()
					.filter(department -> {
						return department.getParentId().equals("0");
					}).collect(Collectors.toList());
			for (EnpowerResponseDepartment department : parentDepartments) {
				buildDepartmentTree(enpowerResponseDepartments, enpowerResponseUsers, department);
			}

			System.out.println("带有上下级关系的部门数量：" + parentDepartments.size());
			
			System.out.println(CommonUtility.toJson(parentDepartments));
			
			System.out.println("所有部门数量： " + enpowerResponseDepartments.size());
			
			System.out.println("所有人员数量： " + enpowerResponseUsers.size());
			
			Set<EnpowerResponseRole> roles = transferRole(enpowerResponseUsers);
			
			roles = buildEnpowerRoleTree(roles, "ALL");
			System.out.println("------------------上下级关系的角色列表-------------------" + roles.size());
			printRoles(roles, 0);
			
			System.out.println("------------------所有的角色列表-------------------" + roles.size());
			roles.stream().forEach(role -> {
				System.out.println(role.getRoleName());
			});
			
			System.out.println("------------------[K2/K3核电项目部] 的角色列表-------------------" + roles.size());
			roles = getRoles(parentDepartments, false);
			roles.stream().forEach(role -> {
				System.out.println(role.getRoleName());
			});
		}
	}
	
	private static void printRoles(Set<EnpowerResponseRole> roles, int level) {
		level++;
		String string = "";
		for (int i = 0; i < level; i++) {
			string += "-";
		}
		if (null != roles) {
			for (EnpowerResponseRole role : roles) {
				System.out.println(string + role.getRoleName());
				if (null != role.getChildren()) {
					printRoles(role.getChildren(), level);
				}
			}
		}
	}

	private static void buildDepartmentTree(List<EnpowerResponseDepartment> dataList, List<EnpowerResponseUser> enpowerResponseUsers,
			EnpowerResponseDepartment parent) {
		List<EnpowerResponseDepartment> children = dataList.stream().filter(data -> {
			if (null != data && CommonUtility.isNonEmpty(data.getParentId()) && null != parent && CommonUtility.isNonEmpty(parent.getId())) {
				if (data.getParentId().equals(parent.getId())) {
					return true;
				}
			}

			return false;
		}).collect(Collectors.toList());

		if (null != children && !children.isEmpty()) {
			parent.setChildren(children);
			
			for (EnpowerResponseDepartment department : children) {
				buildDepartmentTree(dataList, enpowerResponseUsers, department);
			}
		}
		
		List<EnpowerResponseUser> users = enpowerResponseUsers.stream().filter(user -> {
			if (CommonUtility.isNonEmpty(user.getDepartmentId()) && parent.getId().equals(user.getDepartmentId())) {
				return true;
			}
			return false;
		}).collect(Collectors.toList());
		
		if (null != users && !users.isEmpty()) {
			System.out.println(parent.getOrgName() + " 有用户 " + users.size() + " 个.");
			Set<EnpowerResponseRole> roles = users.stream().map(user -> {
				EnpowerResponseRole role = new EnpowerResponseRole();
				role.setRoleId(user.getRoleId());
				role.setRoleName(user.getRoleName());
				role.setRoleNo(user.getRoleNo());
				return role;
			}).collect(Collectors.toSet());
			
			//调用建树
			System.out.println(parent.getOrgName() + " - 角色列表：");
			roles.stream().forEach(role -> {
				System.out.println(role.getRoleId() + " : " + role.getRoleName());
			});
			
			parent.setRoles(roles);
		}
	}
	
	private static Set<EnpowerResponseRole> transferRole(List<EnpowerResponseUser> enpowerResponseUsers){
		if (null != enpowerResponseUsers && !enpowerResponseUsers.isEmpty()) {
			Set<EnpowerResponseRole> roles = enpowerResponseUsers.stream().map(user -> {
				EnpowerResponseRole role = new EnpowerResponseRole();
				role.setRoleId(user.getRoleId());
				role.setRoleName(user.getRoleName());
				role.setRoleNo(user.getRoleNo());
				return role;
			}).collect(Collectors.toSet());
			
			return roles;
		}
		
		return null;
	}
	
	private static Set<EnpowerResponseRole> getRoles(List<EnpowerResponseDepartment> departments, boolean getRoleFlag){
		Set<EnpowerResponseRole> results = new HashSet<EnpowerResponseRole>();
		
		if (null != departments) {
			for (EnpowerResponseDepartment department : departments) {
				if (department.getOrgName().equals("K2/K3核电项目部") || getRoleFlag) {
					if (null != department.getRoles()) {
						results.addAll(department.getRoles());
					}
					getRoleFlag = true;
				}
				if (null != department.getChildren() && !department.getChildren().isEmpty()) {
					results.addAll(getRoles(department.getChildren(), getRoleFlag));
				}
			}
		}
		
		return results;
	}
	
	//建立Roles的上下级关系结构树 -- 原生对象的处理工作
	private static Set<EnpowerResponseRole> buildEnpowerRoleTree(Set<EnpowerResponseRole> enpowerResponseRoles, String type) {
		String roleTree = FileUtils.readFileContent("E:\\Git_Repository\\role_tree.json");
		Gson gson = new Gson();
		EnpowerRoleDomain enpowerRoleDomain = gson.fromJson(roleTree,
				new TypeToken<EnpowerRoleDomain>() {
				}.getType());
		
		if (null != enpowerRoleDomain) {
			List<EnpowerRoleEntity> enpowerRoleEntities = new ArrayList<EnpowerRoleEntity>();
			if (type.equals("GDJH")) {
				EnpowerRoleEntity enpowerRoleEntity = enpowerRoleDomain.getGDJH();
				enpowerRoleEntities.add(enpowerRoleEntity);
			} else if (type.equals("ALL")) {
				enpowerRoleEntities = enpowerRoleDomain.getALL();
			}
			if (null != enpowerRoleEntities && !enpowerRoleEntities.isEmpty() && null != enpowerResponseRoles) {
				Set<EnpowerResponseRole> results = new HashSet<EnpowerResponseRole>(); 
				
				for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
					for (EnpowerRoleEntity enpowerRoleEntity : enpowerRoleEntities) {
						if (enpowerResponseRole.getRoleName().equals(enpowerRoleEntity.getName())) {
							enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, enpowerRoleEntity.getSubRole()));
							results.add(enpowerResponseRole);
						}
					}
				}
				
				return results;
			}
		}
		
		return null;
	}
	//递归处理角色成树的转换工作--重要
	private static Set<EnpowerResponseRole> buildEnpowerRoleTree(Set<EnpowerResponseRole> enpowerResponseRoles, EnpowerResponseRole parent, List<EnpowerRoleEntity> subRole) {
				
		Set<EnpowerResponseRole> results = new HashSet<EnpowerResponseRole>(); 
		
		if (null != enpowerResponseRoles && null != parent && null != subRole) {
			for (EnpowerRoleEntity role : subRole) {
				for (EnpowerResponseRole enpowerResponseRole : enpowerResponseRoles) {
					if (enpowerResponseRole.getRoleName().equals(role.getName()) && !enpowerResponseRole.getRoleName().equals(parent.getRoleName())) {
						enpowerResponseRole.setChildren(buildEnpowerRoleTree(enpowerResponseRoles, enpowerResponseRole, role.getSubRole()));
						results.add(enpowerResponseRole);
					}
				}
			}
			return results;
		}
		
		return null;
	}
}
