package com.easycms.hd.user.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.user.service.HDUserService;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;

@Service
public class HDUserServiceImpl implements HDUserService{
  @Autowired
  private VariableSetService variableSetService;
  @Autowired
  private UserDao userDao;
  public boolean isClasz(Integer userId){
    User user = userDao.findById(userId);
    VariableSet vs = variableSetService.findByKey(VariableSet.TEAM, VariableSet.ASSIGN_TYPE);
    List<Role> roles = user.getRoles();
    if(roles != null){
      for (Role role : roles) {
          if(vs.getSetvalue().equals(role.getName())){
            return true;
          }
      }
    }
    return false;
}

  public boolean isGroup(Integer userId){
    User user = userDao.findById(userId);
    VariableSet vs = variableSetService.findByKey(VariableSet.ENDMAN, VariableSet.ASSIGN_TYPE);
    List<Role> roles = user.getRoles();
    if(roles != null){
      for (Role role : roles) {
          if(vs.getSetvalue().equals(role.getName())){
            return true;
          }
      }
    }
    return false;
}

  @Override
  public List<User> getChildren(User user) {
    return userDao.findUserByParentId(user.getId());
  }

}
