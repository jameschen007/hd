package com.easycms.hd.qualityctrl.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.easycms.common.poi.FileInfo;

import lombok.Data;

@Data
public class QualityControlDetils implements Serializable {
	private static final long serialVersionUID = -6852977505117072061L;
	
	private Integer id;
	
	private String unit;
	
	private String subitem;
	
	private String area;
	
	private String floor;
	
	private String roomnum;
	
	private String system;
	
	private String responsibleDept;
	
	private String responsibleTeam;
	
	private String problemDescription;
	
	private String type;
	
	private String createUser;
	
	private Date createDate;
	
	private String status;
	
	private Integer qcUser;

	private String renovateTeam;

	private String renovateDescription;

	private String notes;

	private String qualityFlag;
	/**
	 * 相关图片附件
	 */
	private List<FileInfo> files;
}
