package com.easycms.hd.qualityctrl.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "hd_qc")
@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControl extends BasicForm implements Serializable {
	private static final long serialVersionUID = -1675495576097313083L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	/**
	 * 机组
	 */
	@Column(name="unit")
	private String unit;
	
	/**
	 * 子项
	 */
	@Column(name="subitem")
	private String subitem;
	
	/**
	 * 区域
	 */
	@Column(name="area")
	private String area;
	
	/**
	 * 楼层
	 */
	@Column(name="floor")
	private String floor;
	
	/**
	 * 房间号
	 */
	@Column(name="roomnum")
	private String roomnum;
	
	/**
	 * 系统
	 */
	@Column(name="system")
	private String system;
	
	/**
	 * 质量问题类别
	 */
	@Column(name="type")
	private Integer type;
	/**
	 * 责任部门
	 */
	@Column(name="responsible_dept")
	private Integer responsibleDept;
	
	/**
	 * 责任班组
	 */
	@Column(name="responsible_team")
	private Integer responsibleTeam;
	
	/**
	 * 问题描述
	 */
	@Column(name="problem_description")
	private String problemDescription;
	
	/**
	 * 创建人
	 */
	@Column(name="create_user")
	private Integer createUser;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_date")
	private Date createDate;
	
	/**
	 * 当前状态
	 */
	@Column(name="status")
	private String status;

	/**
	 * QC
	 */
	@Column(name="qc_user")
	private Integer qcUser;
	/**
	 * QC
	 */
	@Column(name="team_user")
	private Integer teamUser;
	
	/**
	 * 整改队伍
	 */
	@Column(name="renovate_team")
	private Integer renovateTeam;
	
	/**
	 * 整改描述
	 */
	@Column(name="renovate_description")
	private String renovateDescription;
	
	/**
	 * 备注
	 */
	@Column(name="notes")
	private String notes;
	
	/**
	 * 整改期限
	 */
	@Column(name="time_limit")
	private Date timeLimit;
	
	/**
	 * 是否质量问题单
	 */
	@Column(name="quality_flag")
	private boolean qualityFlag;

	@Transient
	private String responsibleDept2;

	@Transient
	private String responsibleTeam2;
}
