package com.easycms.hd.qualityctrl.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "hd_qc_step")
@Data
public class QualityControlStep implements Serializable {
	private static final long serialVersionUID = 520679745351984207L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	/**
	 * 问题ID
	 */
	@Column(name="qc_id")
    private Integer qcId;

	/**
	 * 当前处理人
	 */
	@Column(name="step_user")
    private Integer stepUser;
	
	/**
	 * 当前处理步骤
	 */
	@Column(name="step_details")
    private String stepDetails;

	/**
	 * 处理状态
	 */
	@Column(name="step_status")
    private String stepStatus;

	/**
	 * 处理时间
	 */
	@Column(name="step_date")
    private Date stepDate;

	/**
	 * 备注
	 */
	@Column(name="step_notes")
    private String stepNotes;

}
