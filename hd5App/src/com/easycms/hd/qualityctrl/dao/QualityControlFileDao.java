package com.easycms.hd.qualityctrl.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.qualityctrl.domain.QualityControlFile;

public interface QualityControlFileDao  extends Repository<QualityControlFile,Integer>,BasicDao<QualityControlFile>{
	
	@Query("FROM QualityControlFile q WHERE q.qcId= ?1")
	List<QualityControlFile> findByQctrlId(Integer qcId);
	
	@Query("From QualityControlFile q where q.qcId = ?1 and q.filetype= ?2")
	List<QualityControlFile> findByqcIdFileType(Integer qcId, String filetype);

}
