package com.easycms.hd.qualityctrl.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.poi.FileInfo;
import com.easycms.common.poi.excel.ExcelColumn;
import com.easycms.common.poi.excel.ExcelHead;
import com.easycms.common.poi.excel.ExcelHelper;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.request.qualityctrl.QualityReasonCodeEnum;
import com.easycms.hd.qualityctrl.dao.QualityControlDao;
import com.easycms.hd.qualityctrl.dao.QualityControlFileDao;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.domain.QualityControlDetils;
import com.easycms.hd.qualityctrl.domain.QualityControlFile;
import com.easycms.hd.qualityctrl.domain.QualityControlStatusMap;
import com.easycms.hd.qualityctrl.mybatis.dao.QualityControlMybatisDao;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("qualityControlService")
public class QualityControlServiceImpl implements QualityControlService {
	
	@Autowired
	private QualityControlDao qualityControlDao;
	@Autowired
	private QualityControlMybatisDao qualityControlMybatisDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private QualityControlFileDao qualityControlFileDao;
	

	@Override
	public QualityControl add(QualityControl qualityControl) {
		return qualityControlDao.save(qualityControl);
	}

	@Override
	public boolean delete(QualityControl qualityControl) {
		return qualityControlDao.deleteById(qualityControl.getId()) > 0;
	}

	@Override
	public QualityControl update(QualityControl qualityControl) {
		return qualityControlDao.save(qualityControl);
	}

	@Override
	public QualityControl findById(Integer id) {
		return qualityControlDao.findById(id);
	}

	@Override
	public Integer count() {
		return qualityControlDao.count();
	}

	@Override
	public Page<QualityControl> findByCreateUser(Page<QualityControl> page, Integer userId ,String status) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByCreateAndStatus(userId, status, pageable);
		}else{
			springPage = qualityControlDao.findByCreater(userId, pageable);			
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControl> findAll(Page<QualityControl> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = qualityControlDao.findAll(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControl> findByStatus(Page<QualityControl> page, String qcStatus) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(qcStatus)){
			springPage = qualityControlDao.findByStatus(qcStatus, pageable);
		}else{
			springPage = qualityControlDao.findAll(pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControl> findByDept(Page<QualityControl> page, Integer deptId,Integer userId,String status) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByDeptIdAndStatus(deptId,userId, status, pageable);
		}else{
			springPage = qualityControlDao.findByDeptId(deptId,userId, pageable);
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
	@Override
	public Page<QualityControl> findByTeam(Page<QualityControl> page, Integer teamId,Integer userId,String status) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByTeamIdAndStatus(teamId,userId, status, pageable);
		}else{
			springPage = qualityControlDao.findByTeamId(teamId,userId, pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControl> findByTeam(Page<QualityControl> page,List<Integer> teamId,Integer userId,String status){

		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByTeamIdAndStatus(teamId,userId, status, pageable);
		}else{
			springPage = qualityControlDao.findByTeamId(teamId,userId, pageable);
		}
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControl> findByQcUser(Page<QualityControl> page, Integer qcUserId,String status) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByQcUserAndStatus(qcUserId, status, pageable);
		}else{
			springPage = qualityControlDao.findByQcUserAll(qcUserId, pageable);			
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}


	@Override
	public Page<QualityControl> findByTeamUser(Page<QualityControl> page, Integer teamUserId,String status) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<QualityControl> springPage = null;
		List<User> teamList = userDao.findUserByDeptParentId(teamUserId);
		List<Integer> teamIds = teamList.stream().mapToInt(User::getId).boxed().collect(Collectors.toList());
		teamIds.add(teamUserId);
		
		if(CommonUtility.isNonEmpty(status)){
			springPage = qualityControlDao.findByTeamUserAndStatus(teamIds, status, pageable);
		}else{
			springPage = qualityControlDao.findByTeamUserAll(teamIds, pageable);			
		}
		
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<QualityControlDetils> findDetilsByStatus(Page<QualityControlDetils> page, String status) {
		int pageNum = (page.getPageNum() - 1) * page.getPagesize();
		int pageSize = page.getPagesize();
		int count = 0;
		List<QualityControlDetils> results = null;
		
		if(CommonUtility.isNonEmpty(status)){
			count = qualityControlMybatisDao.getCountByStatus(status);
			results = qualityControlMybatisDao.findByStatus(status,pageNum,pageSize);			
		}else{
			count = qualityControlMybatisDao.getCount();
			results = qualityControlMybatisDao.findAll(pageNum, pageSize);	
		}
		page.execute(count, page.getPageNum(), results);
		
		return page;
	}

	@Override
	public boolean statisticalReport(HttpServletRequest request, HttpServletResponse response) {
		boolean status = false;
		try{
			List<QualityControlDetils> qualityControl = qualityControlMybatisDao.findForReport();
			List<QualityControlDetils> qualityControls = qualityControl.stream().map(mapper ->{
				if(mapper.getType() != null){
					mapper.setType(QualityReasonCodeEnum.getName(Integer.parseInt(mapper.getType())));
				}
				mapper.setStatus(QualityControlStatusMap.map.get(mapper.getStatus()));
				mapper.setQualityFlag(QualityControlStatusMap.map.get(String.valueOf(mapper.getQualityFlag())));

				//处理返回的图片信息
				List<QualityControlFile> files = qualityControlFileDao.findByQctrlId(mapper.getId());
				List<FileInfo> r = new ArrayList<FileInfo>();
				if(files!=null && files.size()>0){
					String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath;
					for(QualityControlFile f:files){
						FileInfo r1 = new FileInfo();
//						String visitPath = com.easycms.common.logic.context.ContextPath.selfProjectContextPath;
						r1.setAbsolutePath(filePath+"/"+f.getFilepath());
						r1.setFilename(f.getFilename());
						r1.setType(f.getFiletype());
						r.add(r1);
					}
				}
				mapper.setFiles(r);
				
				return mapper;
			}).collect(Collectors.toList());

			
			//获取模板文件
			String tempFlieName = CommonUtility.getPropertyFile("qc.properties").getProperty("TemplateFile");
	    	InputStream in = this.getClass().getClassLoader().getResourceAsStream(tempFlieName);
			
			//设置Excel每列显示内容
			List<ExcelColumn> columns = new ArrayList<ExcelColumn>();
			
			columns.add(new ExcelColumn(0,"unit","机组"));
			columns.add(new ExcelColumn(1,"subitem","子项"));
			columns.add(new ExcelColumn(2,"area","区域"));
			columns.add(new ExcelColumn(3,"floor","楼层"));
			columns.add(new ExcelColumn(4,"roomnum","房间号"));
			columns.add(new ExcelColumn(5,"system","系统"));
			columns.add(new ExcelColumn(6,"responsibleDept","责任部门"));
			columns.add(new ExcelColumn(7,"responsibleTeam","责任班组"));
			columns.add(new ExcelColumn(8,"problemDescription","问题描述"));
			columns.add(new ExcelColumn(9,"type","问题类别"));
			columns.add(new ExcelColumn(10,"notes","备注"));
			columns.add(new ExcelColumn(11,"status","状态"));
			columns.add(new ExcelColumn(12,"qualityFlag","是否质量问题单"));
			columns.add(new ExcelColumn(13,"files","图片"));
			
			ExcelHead head = new ExcelHead();
			head.setColumns(columns);
			head.setColumnCount(columns.size());
			head.setRowCount(2);

			//写入数据
			ExcelHelper.getInstanse().exportExcelFile(head, in, response.getOutputStream(), qualityControls);
			status = true;
		}catch(Exception e){
			log.error("数据写入文件出错！"+e.getMessage());
			e.printStackTrace();
		}
		return status;
	}


}
