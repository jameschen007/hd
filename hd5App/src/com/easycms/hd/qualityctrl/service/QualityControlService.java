package com.easycms.hd.qualityctrl.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.core.util.Page;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.domain.QualityControlDetils;

public interface QualityControlService {
	
	public QualityControl add(QualityControl qualityControl);
	public boolean delete(QualityControl qualityControl);
	public QualityControl update(QualityControl qualityControl);
	public QualityControl findById(Integer id);
	public Integer count();
	
	/**
	 * 获取指定userID创建的质量问题分页信息
	 * @param page
	 * @param userId
	 * @return
	 */
	public Page<QualityControl> findByCreateUser(Page<QualityControl> page, Integer userId,String status);
	
	/**
	 * 获取所有
	 * @param page
	 * @return
	 */
	public Page<QualityControl> findAll(Page<QualityControl> page);
	
	/**
	 * 获取传入状态的质量问题分页信息
	 * @param page
	 * @param qcStatus
	 * @return
	 */
	public Page<QualityControl> findByStatus(Page<QualityControl> page, String qcStatus);
	
	/**
	 * 获取指定部门的质量问题分页信息
	 * @param page
	 * @param deptId
	 * @return
	 */
	public Page<QualityControl> findByDept(Page<QualityControl> page, Integer deptId,Integer userId,String status);
	
	/**
	 * 获取指定班组的质量问题
	 * @param page
	 * @param qcUserId
	 * @return
	 */
	public Page<QualityControl> findByTeam(Page<QualityControl> page,Integer teamId,Integer userId,String status);
	public Page<QualityControl> findByTeam(Page<QualityControl> page,List<Integer> teamId,Integer userId,String status);
	
	/**
	 * 
	 * 获取分派给qc1的质量问题
	 * @param page
	 * @param qcUserId
	 * @param status
	 * @return
	 */
	public Page<QualityControl> findByQcUser(Page<QualityControl> page,Integer qcUserId,String status);
	/**
	 * 
	 * 获取分派给组长或班长的质量问题，以及下属的问题。
	 * @param page
	 * @param qcUserId
	 * @param status
	 * @return
	 */
	public Page<QualityControl> findByTeamUser(Page<QualityControl> page, Integer teamUserId,String status);
	
	public Page<QualityControlDetils> findDetilsByStatus(Page<QualityControlDetils> page,String status);
	
	/**
	 * 质量问题统计报表
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean statisticalReport(HttpServletRequest request,HttpServletResponse response);

}
