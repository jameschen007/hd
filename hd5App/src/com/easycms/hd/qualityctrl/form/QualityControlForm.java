package com.easycms.hd.qualityctrl.form;

import java.io.Serializable;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControlForm extends BasicForm implements Serializable {
	private static final long serialVersionUID = -7816603643613235252L;
	
	private String unit;
	
	private String subitem;
	
	private String floor;
	
	private String roomnum;
	
	private String system;
	
	private Integer responsibleDept;
	
	private Integer responsibleTeam;
	
	private String problemDescription;
	
	private Integer qcUser;

	private Integer renovateTeam;

	private String renovateDescription;

	private String notes;

}
