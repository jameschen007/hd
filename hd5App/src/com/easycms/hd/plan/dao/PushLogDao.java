package com.easycms.hd.plan.dao;

import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.PushLog;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface PushLogDao extends BasicDao<PushLog> ,Repository<PushLog,Integer>{
}
