package com.easycms.hd.plan.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.RollingPlan;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Transactional(readOnly=true,propagation=Propagation.REQUIRED)
public interface StatisticsRollingPlanDao extends Repository<RollingPlan,Integer>,BasicDao<RollingPlan> {
//--------------------------------------task team--------------------------------------------	
	//统计指定班组，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where consteam=?1 and isend!=2 and speciality=?2")
	Long getRollingPlanSurplus(String id, String type);
	//统计指定班组，处于已完成状态的计划数量
	@Query("select count(*) from RollingPlan where consteam=?1 and isend=2 and speciality=?2")
	Long getRollingPlanComplate(String id, String type);
	//统计指定班组，全部计划数量
	@Query("select count(*) from RollingPlan where consteam=?1 and speciality=?2")
	Long getRollingPlanTotal(String id, String type);
//--------------------------------------task group--------------------------------------------	
	//统计指定组长，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where consendman=?1 and isend=2 and speciality=?2")
	Long getRollingPlanCompleteByConsEndmanAndType(String id, String type);
	//统计指定组长，处于已完成状态的计划数量
	@Query("select count(*) from RollingPlan where consendman=?1 and isend!=2 and speciality=?2")
	Long getRollingPlanUnCompleteByConsEndmanAndType(String id, String type);
	//统计指定组长，全部计划数量
	@Query("select count(*) from RollingPlan where consendman=?1")
	Long getRollingPlanTotalByConsEndman(String id);
	//统计指定组长，按类型全部计划数量
	@Query("select count(*) from RollingPlan where consendman=?1 and speciality=?2")
	Long getRollingPlanTotalByConsEndman(String id, String type);
//---------------------------------------task status--------------------------------------------------	
	//按天统计，处于初始状态的计划数量
	@Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus0(String type, String date);
	//按天统计，处于施工状态的计划数量
	@Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus1(String type, String date);
	//按天统计，处于完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus2(String type, String date);
	//按天统计，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus3(String type, String date);
	//按天统计，处于滞后状态的计划数量
	@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
	Long getRollingPlanStatusDayStatus6(String type, Date date, Integer diff);
	//按天统计，处于计划状态的计划数量
	@Query("select count(*) from RollingPlan where isend != 0 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus4(String type, String date);
	//按天统计，处于处理中状态的计划数量
	@Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m-%d')=?2")
	Long getRollingPlanStatusDayStatus5(String type, String date);
//----------------------------Week	
	//按周统计，处于初始状态的计划数量
	@Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and yearweek(date_format(createdOn,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus0(String type, Date date);
	//按周统计，处于施工状态的计划数量
	@Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus1(String type, Date date);
	//按周统计，处于完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and yearweek(date_format(enddate,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus2(String type, Date date);
	//按周统计，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus3(String type, Date date);
	//按周统计，处于滞后状态的计划数量
	@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
	Long getRollingPlanStatusWeekStatus6(String type, Date date, Integer diff);
	//按周统计，处于计划状态的计划数量
	@Query("select count(*) from RollingPlan where isend !=0 and speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus4(String type, Date date);
	//按周统计，处于处理中状态的计划数量
	@Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(doissuedate,'%Y-%m-%d'))=yearweek(?2)")
	Long getRollingPlanStatusWeekStatus5(String type, Date date);
//----------------------------Month	
	//按月统计，处于初始状态的计划数量
	@Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus0(String type, String date);
	//按月统计，处于施工状态的计划数量
	@Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus1(String type, String date);
	//按月统计，处于完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus2(String type, String date);
	//按月统计，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus3(String type, String date);
	//按月统计，处于滞后状态的计划数量
	@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
	Long getRollingPlanStatusMonthStatus6(String type, Date date, Integer diff);
	//按月统计，处于计划状态的计划数量
	@Query("select count(*) from RollingPlan where isend != 0 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus4(String type, String date);
	//按月统计，处于处理中状态的计划数量
	@Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m')=?2")
	Long getRollingPlanStatusMonthStatus5(String type, String date);
//----------------------------Year	
	//按年统计，处于初始状态的计划数量
	@Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y')=?2")
	Long getRollingPlanStatusYearStatus0(String type, String date);
	//按年统计，处于施工状态的计划数量
	@Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2")
	Long getRollingPlanStatusYearStatus1(String type, String date);
	//按年统计，处于完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y')=?2")
	Long getRollingPlanStatusYearStatus2(String type, String date);
	//按年统计，处于未完成状态的计划数量
	@Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2")
	Long getRollingPlanStatusYearStatus3(String type, String date);
	//按年统计，处于滞后状态的计划数量
	@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
	Long getRollingPlanStatusYearStatus6(String type, Date date, Integer diff);
	//按年统计，处于计划状态的计划数量
	@Query("select count(*) from RollingPlan where isend !=0 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2")
	Long getRollingPlanStatusYearStatus4(String type, String date);
	//按年统计，处于处理中状态的计划数量
	@Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y')=?2")
	Long getRollingPlanStatusYearStatus5(String type, String date);
	
	
//-----------------------------------------Task Hyperbola----------------------------------------------
	//任务完成情况的双曲线
	@Query(value="select MT.DAY_NUM day, ifnull(total_num, 0) as amount from MONTH_DAY MT left join(SELECT T.*, DAY(endtime)+0 M FROM (select count(autoid) TOTAL_NUM,DATE_FORMAT(enddate,'%Y-%m-%d') endtime from rolling_plan where DATE_FORMAT(enddate,'%Y-%m')=?1 and isend=2 and speciality=?2 group by endtime) T) t on t.m=MT.DAY_NUM order by MT.DAY_NUM;", nativeQuery=true)
	List<Object[]> getRollingPlanTaskHyperbola(String month, String type);
	
	//任务完成情况的双曲线,包含时间搜索条件
	@Query(value="select MT.select_date day, ifnull(total_num, 0) as amount from query_date MT left join(SELECT T.*, DATE_FORMAT(endtime,'%y-%m-%d') M FROM " +
					"(select count(autoid) TOTAL_NUM,DATE_FORMAT(enddate,'%y-%m-%d') " +
					"endtime from rolling_plan where DATE_FORMAT(enddate,'%y-%m-%d')>=DATE_FORMAT(?1,'%y-%m-%d')  " +
					"and   DATE_FORMAT(enddate,'%y-%m-%d')<=DATE_FORMAT(?2,'%y-%m-%d') and isend=2 and speciality=?3 group by endtime) T) t on t.m=MT.select_date order by MT.select_date;", nativeQuery=true)
	List<Object[]> getRollingPlanTaskHyperbolaByDate(String startTime, String endTime, String type);


//##########################################根据ban长查询统计#######################################################################
	
	@Query(value="select MT.DAY_NUM day, ifnull(total_num, 0) as amount from MONTH_DAY MT left join(SELECT T.*, DAY(endtime)+0 M FROM (select count(a.autoid) TOTAL_NUM,DATE_FORMAT(a.enddate,'%Y-%m-%d') endtime from rolling_plan a ,ec_department b  where  a.consteam =b.id and a.consteam in ?3 and DATE_FORMAT(a.enddate,'%Y-%m')=?1 and a.isend=2 and a.speciality=?2 group by endtime) T) t on t.m=MT.DAY_NUM order by MT.DAY_NUM;", nativeQuery=true)
    List<Object[]> getRollingPlanTaskHyperbola(String month, String weldHK, String[] ids);
    
    //任务完成情况的双曲线,包含时间搜索条件
  	@Query(value="select MT.select_date day, ifnull(total_num, 0) as amount from query_date MT left join(SELECT T.*, DATE_FORMAT(endtime,'%y-%m-%d') M FROM " +
  					"(select count(autoid) TOTAL_NUM,DATE_FORMAT(enddate,'%y-%m-%d') " +
  					"endtime from rolling_plan where DATE_FORMAT(enddate,'%y-%m-%d')>=DATE_FORMAT(?1,'%y-%m-%d')  " +
  					"and   DATE_FORMAT(enddate,'%y-%m-%d')<=DATE_FORMAT(?2,'%y-%m-%d') and isend=2 and speciality=?3 and consteam in ?4 group by endtime) T) t on t.m=MT.select_date order by MT.select_date;", nativeQuery=true)
    List<Object[]> getRollingPlanTaskHyperbolaByDuration(
			String fromDate, String toDate, String type, String[] ids);
    

    //rolling plan status by day and departmrnt id
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusDayStatus0(String type, String date, String[] ids);
    
    //查处至现在所有的未分配的
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and createdOn < ?2 and consteam in ?3")
    Long getRollingPlanStatusAllStatus0(String type, Date date, String[] ids);
    
    //根据班长查询未分配统计
    @Query("select count(*) from RollingPlan where isend=4 and speciality=?1  and consteam in ?2")
    Long getRollingPlanStatusAllStatus0ByClasz(String type,String[] ids);
    
    //根据计划员查询未分配统计
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1  and consteam is null")
    Long getRollingPlanStatusAllStatus0ByPlanner(String type);

    //按天与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusDayStatus1(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusDayStatus2(String type, String date, String[] ids);
    
    //按天统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2 and consteam in ?3")
    Long getRollingPlanStatusDayStatus3(String type, String date,String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL and consteam in ?4", nativeQuery=true)
	Long getRollingPlanStatusDayStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusDayStatus4(String type, String date, String[] ids);
    
    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m-%d')=?2 and consteam in ?3")
    Long getRollingPlanStatusDayStatus4ByClasz(String type, String date, String[] ids);

    //查询处理中的
    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusDayStatus5(String type, String date, String[] ids);
    
    //查询处理中的不按日期
    @Query("select count(*) from RollingPlan where speciality=?1 and doissuedate < ?2 and consteam in ?3")
    Long getRollingPlanStatusAllStatus5(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and yearweek(date_format(createdOn,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
	Long getRollingPlanStatusWeekStatus0(String type, Date date, String[] ids);

    //按周与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
	Long getRollingPlanStatusWeekStatus1(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and yearweek(date_format(enddate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
	Long getRollingPlanStatusWeekStatus2(String type, Date date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL and consteam in ?4", nativeQuery=true)
	Long getRollingPlanStatusWeekStatus6(String type, Date date,
			Integer hysteresis, String[] ids);
    //按周统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
    Long getRollingPlanStatusWeekStatus3(String type, Date date,String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
	Long getRollingPlanStatusWeekStatus4(String type, Date date, String[] ids);
    
    //根据班长按周查询计划中的统计
    @Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(planfinishdate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
    Long getRollingPlanStatusWeekStatus4ByClasz(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(doissuedate,'%Y-%m-%d'))=yearweek(?2) and consteam in ?3")
	Long getRollingPlanStatusWeekStatus5(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m-%d')=?2 and consteam in ?3")
	Long getRollingPlanStatusMonthStatus0(String type, String date, String[] ids);

    //按月与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2 and consteam in ?3")
	Long getRollingPlanStatusMonthStatus1(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m')=?2 and consteam in ?3")
	Long getRollingPlanStatusMonthStatus2(String type, String date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL and consteam in ?4", nativeQuery=true)
	Long getRollingPlanStatusMonthStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2 and consteam in ?3")
	Long getRollingPlanStatusMonthStatus4(String type, String date, String[] ids);
    
    //根据班长按月查询计划中的统计
    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2 and consteam in ?3")
    Long getRollingPlanStatusMonthStatus4ByClasz(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m')=?2 and consteam in ?3")
	Long getRollingPlanStatusMonthStatus5(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y')=?2 and consteam in ?3")
	Long getRollingPlanStatusYearStatus0(String type, String date, String[] ids);

    //按年与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2 and consteam in ?3")
	Long getRollingPlanStatusYearStatus1(String type, String date, String[] id);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y')=?2 and consteam in ?3")
	Long getRollingPlanStatusYearStatus2(String type, String date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, plan_finish_date)>=?3 and speciality=?1 and enddate is NULL and consteam=?4", nativeQuery=true)
	Long getRollingPlanStatusYearStatus6(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2 and consteam in ?3")
	Long getRollingPlanStatusYearStatus4(String type, String date, String[] ids);
    
    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2 and consteam in ?3")
    Long getRollingPlanStatusYearStatus4ByClasz(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y')=?2 and consteam in ?3")
	Long getRollingPlanStatusYearStatus5(String type, String date, String[] ids);
    
    //根据ids按年统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2 and consteam in ?3")
    Long getRollingPlanStatusYearStatus3(String type, String date,String[] ids);

    //按月统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y-%m')=?2 and consteam in ?3")
    Long getRollingPlanStatusMonthStatus3(String type, String date, String[] ids);
    
    //按天统计，处于初始状态的计划数量
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and createdOn < ?2")
    Long getRollingPlanStatusAllStatus0(String type, Date date);

    //查询处理中的
    @Query("select count(*) from RollingPlan where speciality=?1 and doissuedate < ?2")
    Long getRollingPlanStatusAllStatus5(String type, Date date);
    
    
    //#########################################ban长登陆查询开始#############################################
    
    
    //#########################################zu长登陆查询开始#############################################
    
  	
    @Query(value="select MT.DAY_NUM day, ifnull(total_num, 0) as amount from MONTH_DAY MT left join(SELECT T.*, DAY(endtime)+0 M FROM (select count(a.autoid) TOTAL_NUM,DATE_FORMAT(a.enddate,'%Y-%m-%d') endtime from rolling_plan a ,ec_department b  where  a.consteam =b.id and a.consendman in ?3 and DATE_FORMAT(a.enddate,'%Y-%m')=?1 and a.isend=2 and a.speciality=?2 group by endtime) T) t on t.m=MT.DAY_NUM order by MT.DAY_NUM;", nativeQuery=true)
    List<Object[]> getRollingPlanTaskHyperbolaByGroup(String month, String weldHK, String[] ids);

  //任务完成情况的双曲线,包含时间搜索条件
  	@Query(value="select MT.select_date day, ifnull(total_num, 0) as amount from query_date MT left join(SELECT T.*, DATE_FORMAT(endtime,'%y-%m-%d') M FROM " +
  					"(select count(autoid) TOTAL_NUM,DATE_FORMAT(enddate,'%y-%m-%d') " +
  					"endtime from rolling_plan where DATE_FORMAT(enddate,'%y-%m-%d')>=DATE_FORMAT(?1,'%y-%m-%d')  " +
  					"and   DATE_FORMAT(enddate,'%y-%m-%d')<=DATE_FORMAT(?2,'%y-%m-%d') and isend=2 and speciality=?3 and consendman in ?4 group by endtime) T) t on t.m=MT.select_date order by MT.select_date;", nativeQuery=true)
    List<Object[]> getRollingPlanTaskHyperbolaByGroupAndDuration(
			String fromDate, String toDate, String type, String[] ids);
    
    //rolling plan status by day and departmrnt id
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m-%d')=?2 and consendman in ?3")
	Long getRollingPlanStatusDayStatus0ByGroup(String type, String date, String[] ids);
    
    //查处至现在所有的未分配的
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and createdOn < ?2 and consendman in ?3")
    Long getRollingPlanStatusAllStatus0ByGroup(String type, Date date, String[] ids);

    //按天与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where doissuedate is null and isend=1 and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m-%d')=?2 and consendman in ?3")
	Long getRollingPlanStatusDayStatus1ByGroup(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m-%d')=?2 and consendman in ?3")
	Long getRollingPlanStatusDayStatus2ByGroup(String type, String date, String[] ids);
    
    //按天统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m-%d')=?2 and consendman in ?3")
    Long getRollingPlanStatusDayStatus3ByGroup(String type, String date,String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_FORMAT(SUBSTR(plandate, 12,21),'%Y-%m-%d'))>=?3 and speciality=?1 and enddate is NULL and consendman in ?4", nativeQuery=true)
	Long getRollingPlanStatusDayStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m-%d')=?2 and consendman in ?3")
	Long getRollingPlanStatusDayStatus4ByGroup(String type, String date, String[] ids);

    //查询处理中的
    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m-%d')=?2 and consendman in ?3")
	Long getRollingPlanStatusDayStatus5ByGroup(String type, String date, String[] ids);
    
    //查询处理中的不按日期
    @Query("select count(*) from RollingPlan where speciality=?1 and doissuedate < ?2 and consendman in ?3")
    Long getRollingPlanStatusAllStatus5ByGroup(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and yearweek(date_format(createdOn,'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
	Long getRollingPlanStatusWeekStatus0ByGroup(String type, Date date, String[] ids);

    //按周与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where  isend=1 and doissuedate is null and speciality=?1 and yearweek(date_format(SUBSTR(plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
	Long getRollingPlanStatusWeekStatus1ByGroup(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and yearweek(date_format(enddate,'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
	Long getRollingPlanStatusWeekStatus2ByGroup(String type, Date date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_FORMAT(SUBSTR(plandate, 12,21),'%Y-%m-%d'))>=?3 and speciality=?1 and enddate is NULL and consendman in ?4", nativeQuery=true)
	Long getRollingPlanStatusWeekStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);
    //按周统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and yearweek(date_format(SUBSTR(plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
    Long getRollingPlanStatusWeekStatus3ByGroup(String type, Date date,String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(SUBSTR(plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
	Long getRollingPlanStatusWeekStatus4ByGroup(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and yearweek(date_format(doissuedate,'%Y-%m-%d'))=yearweek(?2) and consendman in ?3")
	Long getRollingPlanStatusWeekStatus5ByGroup(String type, Date date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y-%m')=?2 and consendman in ?3")
	Long getRollingPlanStatusMonthStatus0ByGroup(String type, String date, String[] ids);

    //按月与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m')=?2 and consendman in ?3")
	Long getRollingPlanStatusMonthStatus1ByGroup(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(enddate,'%Y-%m')=?2 and consendman in ?3")
	Long getRollingPlanStatusMonthStatus2ByGroup(String type, String date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_FORMAT(SUBSTR(plandate, 12,21),'%Y-%m-%d'))>=?3 and speciality=?1 and enddate is NULL and consendman in ?4", nativeQuery=true)
	Long getRollingPlanStatusMonthStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m')=?2 and consendman in ?3")
	Long getRollingPlanStatusMonthStatus4ByGroup(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where  speciality=?1 and DATE_FORMAT(doissuedate,'%Y-%m')=?2 and consendman in ?3")
	Long getRollingPlanStatusMonthStatus5ByGroup(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and DATE_FORMAT(createdOn,'%Y')=?2 and consendman in ?3")
	Long getRollingPlanStatusYearStatus0ByGroup(String type, String date, String[] ids);

    //按年与部门id的施工中的数量
    @Query("select count(*) from RollingPlan where isend=1 and doissuedate is null and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y')=?2 and consendman in ?3")
	Long getRollingPlanStatusYearStatus1ByGroup(String type, String date, String[] id);

    @Query("select count(*) from RollingPlan where isend=2 and speciality=?1 and DATE_FORMAT(planfinishdate,'%Y')=?2 and consendman in ?3")
	Long getRollingPlanStatusYearStatus2ByGroup(String type, String date, String[] ids);

    @Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_FORMAT(SUBSTR(plandate, 12,21),'%Y-%m-%d'))>=?3 and speciality=?1 and enddate is NULL and consendman in ?4", nativeQuery=true)
	Long getRollingPlanStatusYearStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y')=?2 and consendman in ?3")
	Long getRollingPlanStatusYearStatus4ByGroup(String type, String date, String[] ids);

    @Query("select count(*) from RollingPlan where speciality=?1 and DATE_FORMAT(doissuedate,'%Y')=?2 and consendman in ?3")
	Long getRollingPlanStatusYearStatus5ByGroup(String type, String date, String[] ids);
    
    //根据ids按年统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y')=?2 and consendman in ?3")
    Long getRollingPlanStatusYearStatus3ByGroup(String type, String date,String[] ids);

    //按月统计，处于未完成状态的计划数量
    @Query("select count(*) from RollingPlan where isend!=2 and speciality=?1 and DATE_FORMAT(SUBSTR(plandate, 1,10),'%Y-%m')=?2 and consendman in ?3")
    Long getRollingPlanStatusMonthStatus3ByGroup(String type, String date, String[] ids);
    
    //按天统计，处于初始状态的计划数量
    @Query("select count(*) from RollingPlan where isend=0 and speciality=?1 and createdOn < ?2")
    Long getRollingPlanStatusAllStatus0ByGroup(String type, Date date);

    //查询处理中的
    @Query("select count(*) from RollingPlan where  speciality=?1 and doissuedate < ?2")
    Long getRollingPlanStatusAllStatus5ByGroup(String type, Date date);
    
    //#########################################组长登陆查询完#############################################
    
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //--------------------------------------------------------------Pagenation task status[start]----------------------------------------------------------------	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //---------------------------------------task status--------------------------------------------------	
  	//按天统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus0(Pageable pageable, String type, String date);
  	//按天统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus1(Pageable pageable, String type, String date);
  	//按天统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus2(Pageable pageable, String type, String date);
  	//按天统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus3(Pageable pageable, String type, String date);
  	//按天统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按天统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus4(Pageable pageable, String type, String date);
  	//按天统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2")
  	Page<RollingPlan> getRollingPlanStatusDayStatus5(Pageable pageable, String type, Date date);
  //----------------------------Week	
  	//按周统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus0(Pageable pageable, String type, Date date);
  	//按周统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus1(Pageable pageable, String type, Date date);
  	//按周统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus2(Pageable pageable, String type, Date date);
  	//按周统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus3(Pageable pageable, String type, Date date);
  	//按周统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按周统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus4(Pageable pageable, String type, Date date);
  	//按周统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus5(Pageable pageable, String type, Date date);
  //----------------------------Month	
  	//按月统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus0(Pageable pageable, String type, String date);
  	//按月统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus1(Pageable pageable, String type, String date);
  	//按月统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus2(Pageable pageable, String type, String date);
  	//按月统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus3(Pageable pageable, String type, String date);
  	//按月统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按月统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus4(Pageable pageable, String type, String date);
  	//按月统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus5(Pageable pageable, String type, Date date);
  //----------------------------Year	
  	//按年统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus0(Pageable pageable, String type, String date);
  	//按年统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus1(Pageable pageable, String type, String date);
  	//按年统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus2(Pageable pageable, String type, String date);
  	//按年统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus3(Pageable pageable, String type, String date);
  	//按年统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按年统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus4(Pageable pageable, String type, String date);
  	//按年统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2")
  	Page<RollingPlan> getRollingPlanStatusYearStatus5(Pageable pageable, String type, Date date);	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //--------------------------------------------------------------Pagenation task status[end]------------------------------------------------------------------	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------  
    
  //-----------------------------------------------------------------------------------------------------------------------------
  //------------------------------------[Start] Clasz pagenation---------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------task status--------------------------------------------------	
  	//按天统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus0(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus1(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus2(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus3(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按天统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus4(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus5(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Week	
  	//按周统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus0(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus1(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus2(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus3(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按周统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus4(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus5(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Month	
  	//按月统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus0(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus1(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus2(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus3(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按月统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus4(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus5(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Year	
  	//按年统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus0(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus1(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus2(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus3(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按年统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus4(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus5(Pageable pageable, String type, Date date, String[] ids);	
    //-------------------------------------------------------------------------------------------------------------------------
  	//------------------------------------[End] clasz pagenation---------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------
    
  	
    
  //-----------------------------------------------------------------------------------------------------------------------------
  //------------------------------------[Start] Group pagenation---------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------task status--------------------------------------------------	
  	//按天统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus0ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus1ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus2ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus3ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按天统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus4ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按天统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusDayStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Week	
  	//按周统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus0ByGroup(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus1ByGroup(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus2ByGroup(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus3ByGroup(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按周统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus4ByGroup(Pageable pageable, String type, Date date, String[] ids);
  	//按周统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Month	
  	//按月统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus0ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus1ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus2ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus3ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按月统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus4ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按月统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids);
  //----------------------------Year	
  	//按年统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus0ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus1ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus2ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus3ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按年统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus4ByGroup(Pageable pageable, String type, String date, String[] ids);
  	//按年统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3")
  	Page<RollingPlan> getRollingPlanStatusYearStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids);	
    //-------------------------------------------------------------------------------------------------------------------------
  	//------------------------------------[End] Group pagenation---------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------
 //****************************************************************************************[For Search Result]************************************************************************************************************************ 	
  
  	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //--------------------------------------------------------------Pagenation task search status[start]----------------------------------------------------------------	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //---------------------------------------task status--------------------------------------------------	
  	//按天统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus0(Pageable pageable, String type, String date, String keyword);
  	//按天统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus1(Pageable pageable, String type, String date, String keyword);
  	//按天统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus2(Pageable pageable, String type, String date, String keyword);
  	//按天统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus3(Pageable pageable, String type, String date, String keyword);
  	//按天统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按天统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus4(Pageable pageable, String type, String date, String keyword);
  	//按天统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusDayStatus5(Pageable pageable, String type, Date date, String keyword);
  //----------------------------Week	
  	//按周统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2) and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus0(Pageable pageable, String type, Date date, String keyword);
  	//按周统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus1(Pageable pageable, String type, Date date, String keyword);
  	//按周统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2) and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus2(Pageable pageable, String type, Date date, String keyword);
  	//按周统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus3(Pageable pageable, String type, Date date, String keyword);
  	//按周统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按周统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus4(Pageable pageable, String type, Date date, String keyword);
  	//按周统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusWeekStatus5(Pageable pageable, String type, Date date, String keyword);
  //----------------------------Month	
  	//按月统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus0(Pageable pageable, String type, String date, String keyword);
  	//按月统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus1(Pageable pageable, String type, String date, String keyword);
  	//按月统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus2(Pageable pageable, String type, String date, String keyword);
  	//按月统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus3(Pageable pageable, String type, String date, String keyword);
  	//按月统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按月统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus4(Pageable pageable, String type, String date, String keyword);
  	//按月统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusMonthStatus5(Pageable pageable, String type, Date date, String keyword);
  //----------------------------Year	
  	//按年统计，处于初始状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus0(Pageable pageable, String type, String date, String keyword);
  	//按年统计，处于施工状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus1(Pageable pageable, String type, String date, String keyword);
  	//按年统计，处于完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus2(Pageable pageable, String type, String date, String keyword);
  	//按年统计，处于未完成状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus3(Pageable pageable, String type, String date, String keyword);
  	//按年统计，处滞后状态的计划，分页数据。
  	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
  	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
  	//按年统计，处于计划状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.isend != 0 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus4(Pageable pageable, String type, String date, String keyword);
  	//按年统计，处于处理中状态的计划，分页数据。
  	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and (rp.drawno like ?3 or rp.weldno like ?3)")
  	Page<RollingPlan> getRollingPlanStatusYearStatus5(Pageable pageable, String type, Date date, String keyword);	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------	
  //--------------------------------------------------------------Pagenation task search status[end]------------------------------------------------------------------	
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------
  	
  	//-----------------------------------------------------------------------------------------------------------------------------
    //------------------------------------[Start] Clasz search pagenation---------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------
      //---------------------------------------task status--------------------------------------------------	
    	//按天统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus0(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus1(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus2(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus3(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按天统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m-%d')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus4(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus5(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Week	
    	//按周统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus0(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus1(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus2(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus3(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按周统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and yearweek(date_format(rp.planfinishdate,'%Y-%m-%d'))=yearweek(?2) and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus4(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus5(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Month	
    	//按月统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus0(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus1(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus2(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus3(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按月统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y-%m')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus4(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus5(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Year	
    	//按年统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus0(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus1(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus2(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus3(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按年统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(rp.planfinishdate,'%Y')=?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus4(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consteam in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus5(Pageable pageable, String type, Date date, String[] ids, String keyword);	
      //-------------------------------------------------------------------------------------------------------------------------
    	//------------------------------------[End] clasz search pagenation---------------------------------------------------------
      //-------------------------------------------------------------------------------------------------------------------------
      
    	
      
    //-----------------------------------------------------------------------------------------------------------------------------
    //------------------------------------[Start] Group search pagenation---------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------
      //---------------------------------------task status--------------------------------------------------	
    	//按天统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m-%d')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus0ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus1ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m-%d')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus2ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus3ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusDayStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按天统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus4ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按天统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusDayStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Week	
    	//按周统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and yearweek(date_format(rp.createdOn,'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus0ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus1ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and yearweek(date_format(rp.enddate,'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus2ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus3ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusWeekStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按周统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and yearweek(date_format(SUBSTR(rp.plandate, 1,10),'%Y-%m-%d'))=yearweek(?2) and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus4ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    	//按周统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where  rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusWeekStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Month	
    	//按月统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y-%m')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus0ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus1ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y-%m')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus2ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus3ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusMonthStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按月统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y-%m')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus4ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按月统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusMonthStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);
    //----------------------------Year	
    	//按年统计，处于初始状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=0 and rp.speciality=?1 and DATE_FORMAT(rp.createdOn,'%Y')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus0ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于施工状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=1 and doissuedate is null and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus1ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend=2 and rp.speciality=?1 and DATE_FORMAT(rp.enddate,'%Y')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus2ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于未完成状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.isend!=2 and rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus3ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处滞后状态的计划，分页数据。
    	//@Query(value="select count(*) from rolling_plan where DATEDIFF(?2, DATE_ADD(plan_finish_date, INTERVAL ?3 DAY))>=?3 and speciality=?1 and enddate is NULL", nativeQuery=true)
    	//Page<RollingPlan> getRollingPlanStatusYearStatus6(Pageable pageable, String type, Date date, Integer diff);
    	//按年统计，处于计划状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and DATE_FORMAT(SUBSTR(rp.plandate, 1,10),'%Y')=?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus4ByGroup(Pageable pageable, String type, String date, String[] ids, String keyword);
    	//按年统计，处于处理中状态的计划，分页数据。
    	@Query("FROM RollingPlan rp where rp.speciality=?1 and rp.doissuedate<?2 and rp.consendman in ?3 and (rp.drawno like ?4 or rp.weldno like ?4)")
    	Page<RollingPlan> getRollingPlanStatusYearStatus5ByGroup(Pageable pageable, String type, Date date, String[] ids, String keyword);	
      //-------------------------------------------------------------------------------------------------------------------------
    	//------------------------------------[End] Group search pagenation---------------------------------------------------------
      //-------------------------------------------------------------------------------------------------------------------------
}
