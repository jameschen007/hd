package com.easycms.hd.plan.dao;

import java.sql.*;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.plan.domain.Isend;
import com.easycms.hd.plan.domain.QCSign;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepenp;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.service.WorkStepenpService;


@Controller
@RequestMapping("/baseservice/import_from_oracle")
public class TestOracle {
	private static boolean IMPORTING = false;
	@Autowired
	private RollingPlanService rollingPlanService;

	@Autowired
	private WorkStepService workStepService;

	@Autowired
	private WorkStepenpService workStepenpService;

	@RequestMapping(value = "/workstep", method = RequestMethod.GET)
	public void loadOracleworkstep(HttpServletRequest request, HttpServletResponse response){
		//HttpServletRequest request, HttpServletResponse response
		try {
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			Connection oracle_con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@10.33.64.239:1521/enpower.fq23","app","app");  

			//step3 create the statement object  
			Statement stmt=oracle_con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("select * from work_step"); 

			//load data to workstep
			while(rs.next()){
				try {
					String drawno = rs.getString(1);
					String weldno = rs.getString(2);
					RollingPlan rp = rollingPlanService.findByDrawnoAndWeldno(drawno, weldno);
					if(rp == null) continue;
					/*
					RollingPlan rollingPlan = new RollingPlan();
					rollingPlan.setId(workStepForm.getAutoidup());

					 */
					WorkStep workStep = new WorkStep();
					workStep.setRollingPlan(rp);
					workStep.setStepno(rs.getInt(3));
					workStep.setStepname(rs.getString(4));
					workStep.setOperater("");
					workStep.setNoticeaqa(rs.getString(7));

					workStep.setNoticeaqc1(rs.getString(5));
					//					workStep.setWitnesseraqc1(workStepForm.getWitnesseraqc1());
					//					workStep.setWitnessdateaqc1(workStepForm.getWitnessdateaqc1());

					workStep.setNoticeaqc2(rs.getString(6));
					//					workStep.setWitnesseraqc2(workStepForm.getWitnesseraqc2());
					//					workStep.setWitnessdateaqc2(workStepForm.getWitnessdateaqc2());


					workStep.setNoticeb(rs.getString(8));
					//					workStep.setWitnesserb(workStepForm.getWitnesserb());
					//					workStep.setWitnessdateb(workStepForm.getWitnessdateb());
					workStep.setNoticec(rs.getString(9));
					//					workStep.setWitnesserc(workStepForm.getWitnesserc());
					//					workStep.setWitnessdatec(workStepForm.getWitnessdatec());
					workStep.setNoticed(rs.getString(10));
					//					workStep.setWitnesserd(workStepForm.getWitnesserd());
					//					workStep.setWitnessdated(workStepForm.getWitnessdated());
					workStep.setCreatedOn(new Date());
					workStep.setCreatedBy("");
					workStep.setStepflag(StepFlag.UNDO);

					if (!CommonUtility.isNonEmpty(workStep.getNoticeaqa()) && 
							!CommonUtility.isNonEmpty(workStep.getNoticeaqc1()) && 
							!CommonUtility.isNonEmpty(workStep.getNoticeaqc2()) && 
							!CommonUtility.isNonEmpty(workStep.getNoticeb()) && 
							!CommonUtility.isNonEmpty(workStep.getNoticec()) && 
							!CommonUtility.isNonEmpty(workStep.getNoticed())){
						workStep.setStepflag(StepFlag.UNDO);
					}

					workStepService.add(workStep);

				} catch(Exception e){
					System.out.println(e.getMessage());
				}

			}
			HttpUtility.writeToClient(response, "import workstep done");
			return;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}


	@RequestMapping(value = "/rollingplan", method = RequestMethod.GET)
	public void loadOracleRollingPlan(HttpServletRequest request, HttpServletResponse response, String fromDate, String toDate){
		try{  
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			Connection oracle_con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@10.33.64.239:1521/enpower.fq23","app","app");  

			//step3 create the statement object  
			Statement stmt=oracle_con.createStatement();  
			
			ResultSet rs = null;
			
			if (fromDate == "" || fromDate == null || toDate == null || toDate == ""){
				//step4 execute query  
				rs=stmt.executeQuery("select * from rolling_plan"); 
			}else {
				rs=stmt.executeQuery("select * from rolling_plan where FOUND_DATE between to_date("+fromDate+", 'YYYY-MM-DD') and to_date("+toDate+", 'YYYY-MM-DD')"); 
			}
			

			//load data to rollingPlan
			while(rs.next()){
				// 区域号
				String areano = rs.getString(4);
				// 计划施工日期
				Date consdate = rs.getDate(14);
				// 图纸号 
				String drawno = rs.getString(5);
				// 
				String materialtype = rs.getString(9);
				// RCCM
				String rccm = rs.getString(7);
				//
				String speciality = rs.getString(1); 
				// 机组号
				String unitno = rs.getString(3);
				// 
				String weldno = rs.getString(2);
				// 
				String weldlistno = rs.getString(6);
				// 
				String qualityplanno = rs.getString(8);

				Double qulitynum = rs.getDouble(11);

				Double workpoint = rs.getDouble(10);

				Double worktime = rs.getDouble(11);

				String createdby = ""; 

				String technologyask = rs.getString(14);
				String experiencefeedback = rs.getString(16);

				String worktool = rs.getString(17);

				String securityriskctl = rs.getString(15); 
				String qualityriskctl = rs.getString(15);

				export_rollingplan_to_mysql( areano,  consdate,  drawno,  materialtype,  rccm,  speciality,  unitno,  weldno,  weldlistno,  qualityplanno,  qulitynum,  workpoint,  worktime,  createdby,  technologyask,  experiencefeedback,  worktool,  securityriskctl,  qualityriskctl);
			}

			//step5 close the connection object  
			oracle_con.close();
			HttpUtility.writeToClient(response, "import rollingplan done");
			return;
		}catch(Exception e){ System.out.println(e);}  
	}

	public void export_rollingplan_to_mysql(String areano, Date consdate, String drawno, String materialtype, String rccm, String speciality, String unitno, String weldno, String weldlistno, String qualityplanno, Double qulitynum, Double workpoint, Double worktime, String createdby, String technologyask, String experiencefeedback, String worktool, String securityriskctl, String qualityriskctl){
		RollingPlan rollingPlan = new RollingPlan();
		rollingPlan.setIsend(Isend.INITIAL);
		rollingPlan.setQcsign(QCSign.NOT_CONFIRMED);
		rollingPlan.setAreano(areano);
		rollingPlan.setConsdate(consdate);
		rollingPlan.setDrawno(drawno);
		rollingPlan.setMaterialtype(materialtype);
		rollingPlan.setRccm(rccm);
		rollingPlan.setSpeciality(speciality);
		rollingPlan.setUnitno(unitno);
		rollingPlan.setWeldno(weldno);
		rollingPlan.setWeldlistno(weldlistno);
		rollingPlan.setQualityplanno(qualityplanno);
		rollingPlan.setQualitynum(qulitynum);
		rollingPlan.setWorkpoint(workpoint);
		rollingPlan.setWorktime(worktime);
		rollingPlan.setCreatedOn(new Date());
		rollingPlan.setCreatedBy(createdby);
		rollingPlan.setTechnologyAsk(technologyask);
		rollingPlan.setExperienceFeedback(experiencefeedback);
		rollingPlan.setWorkTool(worktool);
		rollingPlan.setSecurityRiskCtl(securityriskctl);
		rollingPlan.setQualityRiskCtl(qualityriskctl);
		try{
			rollingPlan = rollingPlanService.add(rollingPlan);
		}catch(Exception e){
			System.out.println(e);
		}
		return;
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void test(){
		try {
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			Connection oracle_con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@10.0.1.103:1521:grs","study","study");  

			//step3 create the statement object  
			Statement stmt=oracle_con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("select * from work_step"); 

			//load data to workstep
			while(rs.next()){
				try {
					String drawno = rs.getString(1);
					String weldno = rs.getString(2);
					int stepno = rs.getInt(3);
					String stepname = rs.getString(4);
					String noticeaqc1 = rs.getString(5);
					String noticeaqc2 = rs.getString(6);
					String noticeaqa = rs.getString(7);
					String noticeb = rs.getString(8);
					String noticec = rs.getString(9);
					String noticed = rs.getString(10);
					WorkStepenp workStep = new WorkStepenp();

					workStep.setDrawno(drawno);
					workStep.setWeldno(weldno);

					workStep.setStepno(stepno);
					workStep.setStepname(stepname);
					

					workStep.setNoticeaqc1(noticeaqc1);

					workStep.setNoticeaqc2(noticeaqc2);
					
					workStep.setNoticeaqa(noticeaqa);

					workStep.setNoticeb(noticeb);

					workStep.setNoticec(noticec);
					
					workStep.setNoticed(noticed);
					
					workStep.setCreatedOn(new Date());
					workStep.setCreatedBy("");
					workStep.setStepflag(StepFlag.UNDO);


					workStepenpService.add(workStep);

				} catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		}catch(Exception e){
		}
	}
	
	@RequestMapping(value = "/import", method = RequestMethod.GET)
	public void import_from_oracle(HttpServletRequest request, HttpServletResponse response){
		if(!TestOracle.IMPORTING){
			String fromDate = request.getParameter("fromDate");
    		String toDate = request.getParameter("toDate");
			TestOracle.IMPORTING = true;
			loadOracleRollingPlan(request, response, fromDate, toDate);
			loadOracleworkstep(request, response);
			TestOracle.IMPORTING = false;
		}
	}

	public static void main(String args[]){  

		//new TestOracle().loadOracleworkstep();

	}
}

