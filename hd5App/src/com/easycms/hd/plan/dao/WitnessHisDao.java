package com.easycms.hd.plan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.plan.domain.WorkStepWitnessHis;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface WitnessHisDao extends Repository<WorkStepWitnessHis,Integer>,BasicDao<WorkStepWitnessHis>{
	
	/**
	 * 取消时，将见证记录复制到这个历史表中来
	 */
	
	/**
	 * 恢复见证时，从这个历史表中获取历史分派数据。
	 */

//	WorkStepWitnessHis findByStepnoAndWitnessflagAndNoticePoint(Integer wokstepId,String witnessflag,String noticePoint);
	

	List<WorkStepWitnessHis> findByStepnoAndWitnessflagAndNoticePoint(Integer wokstepId,String witnessflag,String noticePoint);

	@Modifying
	@Query("UPDATE WorkStepWitnessHis wsw SET wsw.witnessflag='EXPIRED' WHERE wsw.stepno=?1")
	int expireByWorkStepId(Integer id);
}
