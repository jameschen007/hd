package com.easycms.hd.plan.enums;

public enum NoticePointType {
	QC1("QC1", 1),
	QC2("QC2", 2),
	CZEC_QC("CZEC QC", 3),
	CZEC_QA("CZEC QA", 4),
	PAEC("PAEC", 5);
	
	private String name;
	private Integer seq;

	private NoticePointType(String name, Integer seq) {
		this.name = name;
		this.seq = seq;
	}

	public String getName() {
		return name;
	}

	public Integer getSeq() {
		return seq;
	}
}
