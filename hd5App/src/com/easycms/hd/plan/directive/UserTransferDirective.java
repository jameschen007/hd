package com.easycms.hd.plan.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

/**
 * 用户transfer指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-09
 */
public class UserTransferDirective extends BaseDirective<UserResult>  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserTransferDirective.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiService userApiService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	protected UserResult field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			User user = userService.findUserById(id);
			if (null == user){
				return null;
			}

			return userApiService.userTransferToUserResult(user,true);//显示原始的角色，没有roleType显示
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<UserResult> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<UserResult> pager,
			Map<String, Object> envParams) {
		//不允许有任何的操作。
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<UserResult> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}
}
