//package com.easycms.hd.plan.directive;
//
//import java.util.List;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.easycms.basic.BaseDirective;
//import com.easycms.common.util.CommonUtility;
//import com.easycms.core.freemarker.FreemarkerTemplateUtility;
//import com.easycms.core.util.Page;
//import com.easycms.hd.plan.domain.RollingPlan;
//import com.easycms.oracle.hd.plan.service.RollingPlanOracleService;
//
///**
// * 获取滚动计划列表指令 
// * 
// * @author fangwei: 
// * @areate Date:2015-04-11 
// */
//public class RollingPlanOracleDirective extends BaseDirective<RollingPlan>  {
//	/**
//	 * Logger for this class
//	 */
//	private static final Logger logger = Logger.getLogger(RollingPlanOracleDirective.class);
//	
//	@Autowired
//	private RollingPlanOracleService rollingPlanOracleService;
//	
//	@SuppressWarnings("rawtypes")
//	@Override
//	protected Integer count(Map params, Map<String, Object> envParams) {
//		return null;
//	}
//
//	@SuppressWarnings("rawtypes")
//	@Override
//	protected RollingPlan field(Map params, Map<String, Object> envParams) {
//		String drawno = FreemarkerTemplateUtility.getStringValueFromParams(params, "drawno");
//		String weldno = FreemarkerTemplateUtility.getStringValueFromParams(params, "weldno");
//		logger.debug("[weldno : drawno] ==> " + weldno + " : " + drawno);
//		// 查询ID信息
//		if (CommonUtility.isNonEmpty(drawno) && CommonUtility.isNonEmpty(weldno)) {
//			RollingPlan rollingPlan = rollingPlanOracleService.getByDrawnoAndWeldno(drawno, weldno);
//			return rollingPlanOracleService.addWorkStep(rollingPlan);
//		}
//		return null;
//	}
//	
//	@SuppressWarnings({ "rawtypes"})
//	@Override
//	protected List<RollingPlan> list(Map params, String filter, String order,
//			String sort, boolean pageable, Page<RollingPlan> pager,
//			Map<String, Object> envParams) {
//		RollingPlan condition = null;
//		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
//		
//		if(CommonUtility.isNonEmpty(filter)) {
//			condition = CommonUtility.toObject(RollingPlan.class, filter,"yyyy-MM-dd");
//		}else{
//			condition = new RollingPlan();
//		}
//		
//		if (condition.getId() != null && condition.getId() == 0) {
//			condition.setId(null);
//		}
//		if (CommonUtility.isNonEmpty(order)) {
//			condition.setOrder(order);
//		}
//
//		if (CommonUtility.isNonEmpty(sort)) {
//			condition.setSort(sort);
//		}
//		
//		if (CommonUtility.isNonEmpty(search)) {
//			condition.setSearchValue(search);
//		}
//		
//		Page<RollingPlan> rollingPlanPage = rollingPlanOracleService.findByPage(condition, pager);
//			
//		logger.info("==> RollingPlan length [" + rollingPlanPage.getDatas().size() + "]");
//		return rollingPlanOracleService.addWorkStep(rollingPlanPage.getDatas());
//	}
//
//	@SuppressWarnings("rawtypes")
//	@Override
//	protected List<RollingPlan> tree(Map params, Map<String, Object> envParams) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//}
