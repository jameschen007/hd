package com.easycms.hd.plan.mybatis.dao;

import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;

import java.util.List;
import java.util.Map;

public interface KpiPendingSheetMybatisDao {
	List<KpiPendingSheet> findPageBySql(Map<String, Object> bean);
	Integer findPageBySqlCount(Map<String, Object> bean);
}
