package com.easycms.hd.plan.mybatis.dao;

import java.util.List;
import java.util.Map;

import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;

public interface WorkStepMybatisDao {
	
	//根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	/**
	 * 
	 * @param map [planIds 计划数组]
	 * @return
	 */
	public List<WorkStepForBatchWitness> getWorkStepByRollingIds(Map<String,Object> map);

}
