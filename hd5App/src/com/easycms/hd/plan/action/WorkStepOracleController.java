package com.easycms.hd.plan.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.plan.form.WorkStepForm;

@Controller
@RequestMapping("/baseservice/rollingplan/original/workstep")
public class WorkStepOracleController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WorkStepOracleController.class);
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response){
		String params = request.getParameter("drawno");
		String[] datas =	params.split("[?]");
		String weldno = request.getParameter("weldno");
		String[] weldnos =	weldno.split("[?]");
		request.setAttribute("drawno",datas[0]);
		request.setAttribute("weldno",weldnos[0]);
		return ForwardUtility.forwardAdminView("/hdService/workStep/list_workStep_original");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") WorkStepForm form) {
		logger.debug("==> Show orignial workStep data list.");
		String params = request.getParameter("_drawno");
		if (CommonUtility.isNonEmpty(params)){
			String[] datas = params.split("[?]");
			
			form.setDrawno(datas[0]);
		}
		String weldno = request.getParameter("_weldno");
		if (CommonUtility.isNonEmpty(weldno)){
			String[] weldnos = weldno.split("[?]");
			
			form.setWeldno(weldnos[0]);
		}
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/workStep/data/data_json_workStep_original");
		
		return returnString;
	}
}
