//package com.easycms.hd.plan.action;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.TreeMap;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.FileCopyUtils;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import com.easycms.common.poi.excel.ExcelColumn;
//import com.easycms.common.poi.excel.ExcelHead;
//import com.easycms.common.poi.excel.ExcelHelper;
//import com.easycms.common.upload.UploadUtil;
//import com.easycms.common.util.ConfigurationUtil;
//import com.easycms.common.util.MessageType;
//import com.easycms.common.util.Messenger;
//import com.easycms.common.util.WebUtility;
//import com.easycms.core.util.ForwardUtility;
//import com.easycms.hd.materialtype.domain.MaterialType;
//import com.easycms.hd.materialtype.service.MaterialTypeService;
//import com.easycms.hd.plan.domain.Isend;
//import com.easycms.hd.plan.domain.QCSign;
//import com.easycms.hd.plan.domain.RollingPlan;
//import com.easycms.hd.plan.domain.StepFlag;
//import com.easycms.hd.plan.domain.WorkStep;
//import com.easycms.hd.plan.form.RollingPlanForm;
//import com.easycms.hd.plan.form.WorkStepForm;
//import com.easycms.hd.plan.service.RollingPlanService;
//import com.easycms.hd.plan.service.WorkStepService;
//import com.easycms.hd.price.domain.Price;
//import com.easycms.hd.price.service.PriceService;
//import com.easycms.hd.variable.domain.VariableSet;
//import com.easycms.hd.variable.service.VariableSetService;
//import com.easycms.management.user.domain.User;
//
//@Controller
//@RequestMapping("/baseservice/rollingplan")
//public class PlanController {
//	private static String ROLLING_PLAN = "rollingPlan.properties";
//	private static String WORK_STEP = "workStep.properties";
//	private static String PLANNING_TABLE = "planningtable";
//	private static String PRICE = "price";
//	private static String MODEL = "model.xlsx";
//	/**
//	 * Logger for this class
//	 */
//	private static final Logger logger = Logger.getLogger(PlanController.class);
//	
//	@Autowired
//	private PriceService priceService;
//	@Autowired
//	private MaterialTypeService materialTypeService;
//	@Autowired
//	private RollingPlanService rollingPlanService;
//	@Autowired
//	private WorkStepService workStepService;
//	@Autowired
//	private VariableSetService variableSetService;
//
//	@RequestMapping(value = "/import", method = RequestMethod.GET)
//	public String importUI(HttpServletRequest request, HttpServletResponse response)
//	throws Exception {
//		logger.info("import GET");
//		
//		return ForwardUtility.forwardAdminView("/hdService/rollingPlan/modal_rollingPlan_import");
//	}
//	
//	@RequestMapping(value = "/download", method = RequestMethod.GET)
//	public void download(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		logger.info("download");
//		InputStream is = this.getClass().getClassLoader().getResourceAsStream(MODEL);
//		if(null != is) {
//			try {
//				String fileName = new String(MODEL.getBytes("utf-8"), "ISO8859-1");
//				response.setContentType("application/-excel");
//				response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
//				FileCopyUtils.copy(is, response.getOutputStream());
//			}catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	@RequestMapping(value = "/import", method = RequestMethod.POST)
//	public String importData(HttpServletRequest request, HttpServletResponse response){
//		Messenger msg = new Messenger();
//		User loginUser = (User) request.getSession().getAttribute("user");
//
//		Map<String, InputStream> fileMap = UploadUtil.upload("UTF-8", request);
//		
//		if (null != fileMap){
//			Iterator<Entry<String, InputStream>> fileMapIter = fileMap.entrySet().iterator();
//			while(fileMapIter.hasNext()) {
//				Entry<String, InputStream> fileEntry = fileMapIter.next();
//				InputStream is = (InputStream)fileEntry.getValue();
//				String modelName = fileEntry.getKey();
//						
//				//这里将inputstream缓存起来了，不是一个很好的做法。
//				
//				ByteArrayOutputStream baos = new ByteArrayOutputStream();  
//				byte[] buffer = new byte[1024];  
//				int len;  
//				try {
//					while ((len = is.read(buffer)) > -1 ) {  
//					    baos.write(buffer, 0, len);  
//					}  
//					baos.flush();
//				} catch (IOException e2) {
//					msg.addMessenger(MessageType.FATAL, "文件流解析出错");
//					WebUtility.writeToClient(response, WebUtility.toJson(msg));
//					e2.printStackTrace();
//					return null;
//				}                
//				  
//				InputStream iStream1 = new ByteArrayInputStream(baos.toByteArray());  
//				InputStream iStream2 = new ByteArrayInputStream(baos.toByteArray());
//				
//				
//				logger.info("----------------------------[Start to get rolling plan]------------------------");
//				
//				Map<String, String> rollingPlanMap = null;
//				try {
//					rollingPlanMap = ConfigurationUtil.readFileAsMap(ROLLING_PLAN);
//				} catch (IOException e2) {
//					msg.addMessenger(MessageType.FATAL, "读取滚动计划配置信息出错");
//					WebUtility.writeToClient(response, WebUtility.toJson(msg));
//					e2.printStackTrace();
//				}
//		    	
//		        List<ExcelColumn> rollingPlanExcelColumns = new ArrayList<ExcelColumn>();
//		        
//		        Iterator<Entry<String, String>> rollingPlanMapIter = rollingPlanMap.entrySet().iterator();
//				for(int i = 0; rollingPlanMapIter.hasNext(); i++) {
//					Entry<String, String> entry = rollingPlanMapIter.next();
//					rollingPlanExcelColumns.add(new ExcelColumn(i, entry.getKey(), entry.getValue()));
//				}
//		        
//		        ExcelHead rollingPlanHead = new ExcelHead();
//		        rollingPlanHead.setRowCount(1); 
//		        rollingPlanHead.setColumns(rollingPlanExcelColumns); 
//		         
//		        List<RollingPlanForm> rollingPlanList;
//				try {
//					rollingPlanList = ExcelHelper.getInstanse().importToObjectList(rollingPlanHead, iStream1, RollingPlanForm.class, variableSetService.findValueByKey("rollingPlan", PLANNING_TABLE));
//				} catch (Exception e1) {
//					msg.addMessenger(MessageType.FATAL, "解析滚动计划信息表时出错");
//					WebUtility.writeToClient(response, WebUtility.toJson(msg));
//					return null;
//				}
//				//得到所有滚动计划信息表中的图纸号和焊口号 
//		        HashSet rollingPlanDrawnoSet = new HashSet();
//		        HashSet rollingPlanWeldnoSet = new HashSet();
//		        
//		        for (RollingPlanForm rollingPlanForm : rollingPlanList) {
//		        	logger.info(rollingPlanForm);
//		        	rollingPlanDrawnoSet.add(rollingPlanForm.getDrawno());
//		        	rollingPlanWeldnoSet.add(rollingPlanForm.getWeldno());
//		        }
//		        
//		        List<String> rollingPlanDrawnoList = new ArrayList<String>(rollingPlanDrawnoSet);
//		        List<String> rollingPlanWeldnoList = new ArrayList<String>(rollingPlanWeldnoSet);
//		        
//		        logger.info("----------------------------[Get rolling plan -- success]------------------------");
//		        logger.info("----------------------------[Start to get work step]------------------------");
//		        
//		        Map<String, String> workStepMap = null;
//				try {
//					workStepMap = ConfigurationUtil.readFileAsMap(WORK_STEP);
//				} catch (IOException e2) {
//					msg.addMessenger(MessageType.FATAL, "读取工序步骤配置信息出错");
//					WebUtility.writeToClient(response, WebUtility.toJson(msg));
//					e2.printStackTrace();
//				}
//		    	
//		        List<ExcelColumn> workStepExcelColumns = new ArrayList<ExcelColumn>();
//		        
//		        Iterator<Entry<String, String>> workStepMapIter = workStepMap.entrySet().iterator();
//				for(int i = 0; workStepMapIter.hasNext(); i++) {
//					Entry<String, String> entry = workStepMapIter.next();
//					workStepExcelColumns.add(new ExcelColumn(i, entry.getKey(), entry.getValue()));
//				}
//		        
//		        ExcelHead workStepHead = new ExcelHead();
//		        workStepHead.setRowCount(1); 
//		        workStepHead.setColumns(workStepExcelColumns);
//		        
//		        List<WorkStepForm> workStepList;
//				try {
//					workStepList = ExcelHelper.getInstanse().importToObjectList(workStepHead, iStream2, WorkStepForm.class, variableSetService.findValueByKey("workStep", PLANNING_TABLE));
//				} catch (Exception e1) {
//					System.out.println(e1.getMessage());
//					msg.addMessenger(MessageType.FATAL, "解析工序步骤信息表时出错误");
//					WebUtility.writeToClient(response, WebUtility.toJson(msg));
//					return null;
//				}
//		        //得到所有工序步骤中的图纸号和焊口号 
//		        HashSet workStepDrawnoSet = new HashSet();
//		        HashSet workStepWeldnoSet = new HashSet();
//		        
//		        for (WorkStepForm workStepForm : workStepList) {
//		            logger.info(workStepForm);
//		            workStepDrawnoSet.add(workStepForm.getDrawno());
//		            workStepWeldnoSet.add(workStepForm.getWeldno());
//		        }
//		        
//		        List<String> workStepDrawnoList = new ArrayList<String>(workStepDrawnoSet);
//		        List<String> workStepWeldnoList = new ArrayList<String>(workStepWeldnoSet);
//		        
//		        logger.info("----------------------------[Get work step -- success]------------------------");
//		        logger.info("----------------------------[Start to validate rolling plan and work step key words]------------------------");
//		        
//		        if (rollingPlanDrawnoList.size() > workStepDrawnoList.size()){
//		        	logger.info(modelName + " -->> 滚动计划表中有未指定工序的计划"); //可忽略
//		        	msg.addMessenger(MessageType.WARN, modelName + " -->> 滚动计划表中有未指定工序的计划"); //可忽略
//		        }
//		        if (rollingPlanDrawnoList.size() < workStepDrawnoList.size()){
//		        	logger.info(modelName + " -->> 工序表中有找不到图纸号的计划"); //不可忽略
//		        	msg.addMessenger(MessageType.ERROR, modelName + " -->> 工序表中有找不到图纸号的计划"); //不可忽略
//		        	WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		        	return null;
//		        }
//		        
//		        for (String s : workStepDrawnoList){
//		        	if (!rollingPlanDrawnoList.contains(s)){
//		        		logger.info(modelName + " -->> 滚动计划表中没有找到图纸号为[ " + s + " ]的计划"); //不可忽略
//		        		msg.addMessenger(MessageType.ERROR, modelName + " -->> 滚动计划表中没有找到图纸号为[ " + s + " ]的计划"); //不可忽略
//		        		WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		        		return null;
//		        	}
//		        }
//		        
//		        if (rollingPlanWeldnoList.size() < workStepWeldnoList.size()){
//		        	logger.info(modelName + " -->> 工序表中有找不到焊口号的计划"); //不可忽略
//		        	msg.addMessenger(MessageType.ERROR, modelName + " -->> 工序表中有找不到焊口号的计划"); //不可忽略
//		        	WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		        	return null;
//		        }
//		        
//		        for (String s : workStepWeldnoList){
//		        	if (!rollingPlanWeldnoList.contains(s)){
//		        		logger.info(modelName + " -->> 滚动计划表中没有找到焊口号为[ " + s + " ]的计划"); //不可忽略
//		        		msg.addMessenger(MessageType.ERROR, modelName + " -->> 滚动计划表中没有找到焊口号为[ " + s + " ]的计划"); //不可忽略
//		        		WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		        		return null;
//		        	}
//		        }
//		        logger.info("----------------------------[End to validate rolling plan and work step key words]------------------------");
//		        
//		        logger.info("----------------------------[Start to validate materialType and price information]------------------------");
//		        
//		        for (RollingPlanForm rollingPlanForm : rollingPlanList) {
//		        	String materialtype = rollingPlanForm.getMaterialtype();
//		        	String speciality = rollingPlanForm.getSpeciality();
//		        	
//		        	if (!materialTypeService.materialTypeExists(materialtype)){
//		        		msg.addMessenger(MessageType.WARN, modelName + " -->> 材质管理中发现不存在的材质类型[ " + materialtype + " ], 已为你创建, 请自行修改");
//		        		MaterialType materialType = new MaterialType();
//		        		materialType.setMaterialTypeId(materialtype);
//		        		materialType.setMaterialTypeName("--未知--");
//		        		materialType.setCreatedBy(loginUser.getName());
//		        		materialType.setCreatedOn(new Date());
//		        		materialTypeService.add(materialType);
//		        	}
//		        	
//		        	List<VariableSet> priceTypeList = variableSetService.findByType(PRICE);
//		        	
//		        	for (VariableSet variableSet : priceTypeList){
//		        		String priceType = variableSet.getSetkey();
//		        		
//		        		if (!priceService.checkPricetypeExist(speciality, priceType)){
//		        			msg.addMessenger(MessageType.WARN, modelName + " -->> 单价管理中发现不存在 [ " + speciality + " -- " + priceType + " -- " + variableSetService.findValueByKey(priceType, PRICE) + " ], 已为你创建并设值为0, 请自行修改");
//		        			Price price = new Price();
//		        			price.setSpeciality(speciality);
//		        			price.setPricetype(priceType);
//		        			price.setUnitprice(0.0);
//		        			price.setRemark("--由导入文件生成--");
//		        			price.setCreatedOn(new Date());
//		        			price.setCreatedBy(loginUser.getName());
//		        			priceService.add(price);
//		        		}
//		        	}
//		        }
//		        
//		        logger.info("----------------------------[End to validate materialType and price information]------------------------");
//		        
//		        logger.info("----------------------------[Start to validate rolling plan drawno and weldno unique]------------------------");
//		        
//		        for (RollingPlanForm rollingPlanForm : rollingPlanList) {
//			        if (null != rollingPlanService.findByDrawnoAndWeldno(rollingPlanForm.getDrawno(), rollingPlanForm.getWeldno())){
//			        	msg.addMessenger(MessageType.WARN, rollingPlanForm.getDrawno() + " 的 [" + rollingPlanForm.getWeldno() + "] 已经存在,跳过该条记录");
//			        	logger.info(rollingPlanForm.getDrawno() + " 的 [" + rollingPlanForm.getWeldno() + "] 已经存在,跳过该条记录");
//			        	rollingPlanForm.setIgnore(true);
//			        }
//		        }
//		        
//		        logger.info("----------------------------[End to validate rolling plan drawno and weldno unique]------------------------");
//		        
//		        logger.info("----------------------------[Start to validate workstep stepno unique]------------------------");
//		        
//		        Map<String, ArrayList<WorkStepForm>> tm = sort(workStepList);
//		        Iterator it = tm.keySet().iterator();  
//		        while(it.hasNext()){  
//		        	String key = (String)it.next();  
//		        	logger.info("validate weldno : [" + key + "]."); 
//		        	ArrayList<WorkStepForm> list = tm.get(key);  
//		        	WorkStepForm temp;
//		            for (int i = 0; i < list.size() - 1; i++)
//		            {
//		                temp = list.get(i);
//		                for (int j = i + 1; j < list.size(); j++)
//		                {
//		                    if (temp.getStepno().equals(list.get(j).getStepno()))
//		                    {
//		                    	msg.addMessenger(MessageType.ERROR, "[" + key + "]中有重复步骤号，值为：" + temp.getStepno());
//		                    }
//		                }
//		            } 
//		        }
//		        
//		        if(msg.isContainError()){
//		        	WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		        	return null;
//		        }
//		        
//		        logger.info("----------------------------[End to validate workstep stepno unique]------------------------");
//
//		        logger.info("----------------------------[Start to store rolling plan and work steps]------------------------");
//		        
//		        for (RollingPlanForm rollingPlanForm : rollingPlanList){
//		        	if (rollingPlanForm.isIgnore()){
//		        		continue;
//		        	}
//		        	RollingPlan rollingPlan = new RollingPlan();
//		        	rollingPlan.setQualityplanno(rollingPlanForm.getQualityplanno());
//		        	rollingPlan.setWeldlistno(rollingPlanForm.getWeldlistno());
//		        	rollingPlan.setDrawno(rollingPlanForm.getDrawno());
//		        	rollingPlan.setRccm(rollingPlanForm.getRccm());
//		        	rollingPlan.setAreano(rollingPlanForm.getAreano());
//		        	
//		        	String unitno = rollingPlanForm.getUnitno();
//		        	try {
//						double d = Double.parseDouble(unitno);
//						if(d % 1.0 == 0){
//							unitno = String.valueOf((long)d);
//						}
//					} catch (Exception e) {
//						;
//					}
//		        	
//		        	rollingPlan.setUnitno(unitno);
//		        	rollingPlan.setWeldno(rollingPlanForm.getWeldno());
//		        	rollingPlan.setSpeciality(rollingPlanForm.getSpeciality());
//		        	rollingPlan.setMaterialtype(rollingPlanForm.getMaterialtype());
//		        	rollingPlan.setWorkpoint(rollingPlanForm.getWorkpoint());
//		        	rollingPlan.setQualitynum(rollingPlanForm.getQualitynum());
//		        	rollingPlan.setWorktime(rollingPlanForm.getWorktime());
//		        	rollingPlan.setConsdate(rollingPlanForm.getConsdate());
//		        	rollingPlan.setIsend(Isend.INITIAL);
//		        	rollingPlan.setQcsign(QCSign.NOT_CONFIRMED);
//		        	rollingPlan.setCreatedOn(new Date());
//		        	rollingPlan.setCreatedBy(loginUser.getName());
//		        	rollingPlan.setTechnologyAsk(rollingPlanForm.getTechnologyAsk());
//		        	rollingPlan.setSecurityRiskCtl(rollingPlanForm.getSecurityRiskCtl());
//		        	rollingPlan.setQualityRiskCtl(rollingPlanForm.getQualityRiskCtl());
//		        	rollingPlan.setExperienceFeedback(rollingPlanForm.getExperienceFeedback());
//		        	rollingPlan.setWorkTool(rollingPlanForm.getWorkTool());
//		        	
//		        	rollingPlan = rollingPlanService.add(rollingPlan);
//		        	
//		        	logger.info(rollingPlan);
//		        	
//		        	if (null != rollingPlan){
//		        		for (WorkStepForm workStepForm : workStepList){
//		        			if ((workStepForm.getDrawno().equals(rollingPlan.getDrawno())) && (workStepForm.getWeldno().equals(rollingPlan.getWeldno()))){
//		        				WorkStep workStep = new WorkStep();
//		        				workStep.setRollingPlan(rollingPlan);
//		        				workStep.setStepno(workStepForm.getStepno());
//		        				workStep.setStepname(workStepForm.getStepname());
//		        				workStep.setNoticeaqa(workStepForm.getNoticeaqa());
//		        				workStep.setNoticeaqc1(workStepForm.getNoticeaqc1());
//		        				workStep.setNoticeaqc2(workStepForm.getNoticeaqc2());
//		        				workStep.setNoticeb(workStepForm.getNoticeb());
//		        				workStep.setNoticec(workStepForm.getNoticec());
//		        				workStep.setNoticed(workStepForm.getNoticed());
//		        				workStep.setCreatedOn(new Date());
//		        				workStep.setCreatedBy(loginUser.getName());
//		        				workStep.setStepflag(StepFlag.UNDO);
//		        				
//		        				workStepService.add(workStep);
//		        			}
//		        		}
//		        	}
//		        }
//		        msg.addMessenger(MessageType.INFO, modelName + " -->> 正确解析并存储成功");
//		        logger.info("----------------------------[End to store rolling plan and work steps]------------------------");
//		        if (null != baos){
//		        	try {
//						baos.close();
//					} catch (IOException e) {
//						msg.addMessenger(MessageType.FATAL, "文件流关闭出错");
//						WebUtility.writeToClient(response, WebUtility.toJson(msg));
//						e.printStackTrace();
//					}
//		        }
//			}
//        } else {  
//        	msg = new Messenger();
//        	msg.addMessenger(MessageType.FATAL, "上传文件时发生系统错误");  
//        } 
//		
//		WebUtility.writeToClient(response, WebUtility.toJson(msg));
//		return null;
//	}
//	
//	private static Map<String, ArrayList<WorkStepForm>> sort(List<WorkStepForm> workStepList){  
//		 TreeMap<String, ArrayList<WorkStepForm>> tm = new TreeMap<String, ArrayList<WorkStepForm>>();  
//		 for(WorkStepForm ws : workStepList){  
//			 if(tm.containsKey(ws.getWeldno())){
//				 ArrayList<WorkStepForm> l11 = (ArrayList<WorkStepForm>)tm.get(ws.getWeldno());  
//				 l11.add(ws);  
//			 }else{  
//				 ArrayList<WorkStepForm> tem = new ArrayList<WorkStepForm>();  
//				 tem.add(ws);  
//				 tm.put(ws.getWeldno(), tem);  
//			 }        
//		 }  
//		 return tm;  
//	}
//}
