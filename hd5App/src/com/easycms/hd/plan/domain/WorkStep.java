package com.easycms.hd.plan.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.form.WorkStepWitnessFake;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-11 
 * 
 */
@Entity
@Table(name = "work_step")
//@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStep extends BasicForm implements java.io.Serializable ,Comparable<WorkStep>{
	private static final long serialVersionUID = 1487991188000135619L;
	
//	prvate static Logger logger = Logger.getLogger(WorkStep.class);
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "autoid", unique = true, nullable = false)
	private Integer id;
	
	@Transient
	private String drawno;
	@Transient
	private String weldno;
	
	@Column(name = "stepno")
	private Integer stepno;
	
	@Column(name = "stepflag")
	private String stepflag;
	
	@Column(name = "witnessflag")
	private String witnessflag;
	
	@Column(name = "stepname", length = 50)
	private String stepname;
	
	@Column(name = "operater", length = 50)
	private String operater;
	
	@Column(name = "operatedate", length = 0)
	private Date operatedate;
	
	@Column(name = "operatedesc", length = 1000)
	private String operatedesc;

	/**
	 *接口推送  enpower工序标识id
	 */
	@Column(name = "work_step_id", length = 50, nullable = false)
	private String workStepId;
	/**
	 *接口推送  工序编号 如2.1.2
	 */
	@Column(name = "step_identifier", length = 50, nullable = false)
	private String stepIdentifier;

	/**
	 *接口推送 质量计划id
	 */
	@Column(name = "qualityplanid", length = 50)
	private String qualityplanid;
	/**
	 *接口推送 质量计划号
	 */
	@Column(name = "qualityplanno", length = 50)
	private String qualityplanno;
	/**
	 *接口推送 子专业
	 */
	@Column(name = "sub_specialty", length = 50)
	private String subSpecialty ;
	/**
	 *接口推送 itp名称
	 */
	@Column(name = "itp_name", length = 50)
	private String itpName ;
	
	@Column(name = "noticeainfo", length = 100)
	private String noticeainfo;
	
	@Column(name = "noticeresult", length = 50)
	private String noticeresult;
	
	@Column(name = "noticeresultdesc", length = 100)
	private String noticeresultdesc;
	
	@Column(name = "noticeaqc1", length = 1)
	private String noticeaqc1;
	
	@Column(name = "witnesseraqc1", length = 50)
	private String witnesseraqc1;
	
	@Column(name = "witnessdateaqc1", length = 0)
	private Date witnessdateaqc1;
	
	@Column(name = "noticeaqc2", length = 1)
	private String noticeaqc2;
	
	@Column(name = "witnesseraqc2", length = 50)
	private String witnesseraqc2;
	
	@Column(name = "witnessdateaqc2", length = 0)
	private Date witnessdateaqc2;
	
	@Column(name = "noticeaqa", length = 1)
	private String noticeaqa;
	
	@Column(name = "witnesseraqa", length = 50)
	private String witnesseraqa;
	
	@Column(name = "witnessdateaqa", length = 0)
	private Date witnessdateaqa;

	@Column(name = "noticeb", length = 50)
	private String noticeb;
	
	@Column(name = "witnesserb", length = 50)
	private String witnesserb;
	
	@Column(name = "witnessdateb", length = 0)
	private Date witnessdateb;
	
	@Column(name = "noticec", length = 50)
	private String noticec;
	
	@Column(name = "witnesserc", length = 50)
	private String witnesserc;
	
	@Column(name = "witnessdatec", length = 0)
	private Date witnessdatec;
	
	@Column(name = "noticed", length = 50)
	private String noticed;
	
	@Column(name = "witnesserd", length = 50)
	private String witnesserd;
	
	@Column(name = "witnessdated", length = 0)
	private Date witnessdated;
	
	@Column(name = "created_on", length = 0)
	private Date createdOn;
	
	@Column(name = "created_by", length = 40)
	private String createdBy;
	
	@Column(name = "updated_on", length = 0)
	private Date updatedOn;
	
	@Column(name = "updated_by", length = 40)
	private String updatedBy;
	
	@Column(name = "noticeczecqc", length = 50)
	private String noticeczecqc;
	
	@Column(name = "witnesserczecqc")
	private Integer witnesserczecqc;
	
	@Column(name = "witnessdateczecqc", length = 0)
	private Date witnessdateczecqc;
	
	@Column(name = "noticeczecqa", length = 50)
	private String noticeczecqa;
	
	@Column(name = "witnesserczecqa")
	private Integer witnesserczecqa;
	
	@Column(name = "witnessdateczecqa", length = 0)
	private Date witnessdateczecqa;
	
	@Column(name = "noticepaec", length = 50)
	private String noticepaec;
	
	@Column(name = "witnesserpaec")
	private Integer witnesserpaec;
	
	@Column(name = "witnessdatepaec", length = 0)
	private Date witnessdatepaec;
	
	@Transient
	private boolean lastone = false;
	@Transient
	private Integer rowStart;
	@Transient
	private Integer rowEnd;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "autoidup", nullable = false)
	private RollingPlan rollingPlan;
	
	@JsonIgnore  
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "workStep",cascade={CascadeType.REMOVE})
	private List<WorkStepWitness> workStepWitnesses;
	
	@Transient
	private List<WorkStepWitnessFake> witnessInfo;
	@Transient
	private List<WorkStepWitnessFake> witnessesAssign;
	@Transient
	private Set<String> noticeType;
	@Override
	public int compareTo(WorkStep b) {
		String a1 = this.stepIdentifier;
		String b1 = b.getStepIdentifier();
		if(a1 == null){
			return -1;
		}
		
		if(b1 == null){
			return 1;
		}
//		System.out.println("be:"+a1);
//		System.out.println("be:"+b1);
		a1 = normaliseStr(a1.indexOf("."), a1);
		b1 = normaliseStr(b1.indexOf("."), b1);

//		System.out.println(a1);
//		System.out.println(b1);
		
		
		return a1.compareTo(b1);
	}
	
	private String normaliseStr(int pos1,String a1){
		String b = a1;
		if(pos1>-1){
			String data1[] = a1.split("\\.");
			for (int i = 0; i < data1.length; i++) {
			    if(i == 0){
			    	b = data1[i]+".";
			    }else{
			    	String digit = data1[i];
			    	if(digit.length() == 1){
			    		b += "00"+digit;
			    	}else if(digit.length() == 2){
			    		b +=  "0"+digit;
			    	}else{
			    		b += digit;
			    	}
			    	
			    }			
			}
		}
		return b ;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDrawno() {
		return drawno;
	}

	public void setDrawno(String drawno) {
		this.drawno = drawno;
	}

	public String getWeldno() {
		return weldno;
	}

	public void setWeldno(String weldno) {
		this.weldno = weldno;
	}

	public Integer getStepno() {
		return stepno;
	}

	public void setStepno(Integer stepno) {
		this.stepno = stepno;
	}

	public String getStepflag() {
		return stepflag;
	}

	public void setStepflag(String stepflag) {
		this.stepflag = stepflag;
	}

	public String getWitnessflag() {
		return witnessflag;
	}

	public void setWitnessflag(String witnessflag) {
		this.witnessflag = witnessflag;
	}

	public String getStepname() {
		return stepname;
	}

	public void setStepname(String stepname) {
		this.stepname = stepname;
	}

	public String getOperater() {
		return operater;
	}

	public void setOperater(String operater) {
		this.operater = operater;
	}

	public Date getOperatedate() {
		return operatedate;
	}

	public void setOperatedate(Date operatedate) {
		this.operatedate = operatedate;
	}

	public String getOperatedesc() {
		return operatedesc;
	}

	public void setOperatedesc(String operatedesc) {
		this.operatedesc = operatedesc;
	}

	public String getWorkStepId() {
		return workStepId;
	}

	public void setWorkStepId(String workStepId) {
		this.workStepId = workStepId;
	}

	public String getStepIdentifier() {
		return stepIdentifier;
	}

	public void setStepIdentifier(String stepIdentifier) {
		this.stepIdentifier = stepIdentifier;
	}

	public String getQualityplanid() {
		return qualityplanid;
	}

	public void setQualityplanid(String qualityplanid) {
		this.qualityplanid = qualityplanid;
	}

	public String getQualityplanno() {
		return qualityplanno;
	}

	public void setQualityplanno(String qualityplanno) {
		this.qualityplanno = qualityplanno;
	}

	public String getSubSpecialty() {
		return subSpecialty;
	}

	public void setSubSpecialty(String subSpecialty) {
		this.subSpecialty = subSpecialty;
	}

	public String getItpName() {
		return itpName;
	}

	public void setItpName(String itpName) {
		this.itpName = itpName;
	}

	public String getNoticeainfo() {
		return noticeainfo;
	}

	public void setNoticeainfo(String noticeainfo) {
		this.noticeainfo = noticeainfo;
	}

	public String getNoticeresult() {
		return noticeresult;
	}

	public void setNoticeresult(String noticeresult) {
		this.noticeresult = noticeresult;
	}

	public String getNoticeresultdesc() {
		return noticeresultdesc;
	}

	public void setNoticeresultdesc(String noticeresultdesc) {
		this.noticeresultdesc = noticeresultdesc;
	}

	public String getNoticeaqc1() {
		return noticeaqc1;
	}

	public void setNoticeaqc1(String noticeaqc1) {
		this.noticeaqc1 = noticeaqc1;
	}

	public String getWitnesseraqc1() {
		return witnesseraqc1;
	}

	public void setWitnesseraqc1(String witnesseraqc1) {
		this.witnesseraqc1 = witnesseraqc1;
	}

	public Date getWitnessdateaqc1() {
		return witnessdateaqc1;
	}

	public void setWitnessdateaqc1(Date witnessdateaqc1) {
		this.witnessdateaqc1 = witnessdateaqc1;
	}

	public String getNoticeaqc2() {
		return noticeaqc2;
	}

	public void setNoticeaqc2(String noticeaqc2) {
		this.noticeaqc2 = noticeaqc2;
	}

	public String getWitnesseraqc2() {
		return witnesseraqc2;
	}

	public void setWitnesseraqc2(String witnesseraqc2) {
		this.witnesseraqc2 = witnesseraqc2;
	}

	public Date getWitnessdateaqc2() {
		return witnessdateaqc2;
	}

	public void setWitnessdateaqc2(Date witnessdateaqc2) {
		this.witnessdateaqc2 = witnessdateaqc2;
	}

	public String getNoticeaqa() {
		return noticeaqa;
	}

	public void setNoticeaqa(String noticeaqa) {
		this.noticeaqa = noticeaqa;
	}

	public String getWitnesseraqa() {
		return witnesseraqa;
	}

	public void setWitnesseraqa(String witnesseraqa) {
		this.witnesseraqa = witnesseraqa;
	}

	public Date getWitnessdateaqa() {
		return witnessdateaqa;
	}

	public void setWitnessdateaqa(Date witnessdateaqa) {
		this.witnessdateaqa = witnessdateaqa;
	}

	public String getNoticeb() {
		return noticeb;
	}

	public void setNoticeb(String noticeb) {
		this.noticeb = noticeb;
	}

	public String getWitnesserb() {
		return witnesserb;
	}

	public void setWitnesserb(String witnesserb) {
		this.witnesserb = witnesserb;
	}

	public Date getWitnessdateb() {
		return witnessdateb;
	}

	public void setWitnessdateb(Date witnessdateb) {
		this.witnessdateb = witnessdateb;
	}

	public String getNoticec() {
		return noticec;
	}

	public void setNoticec(String noticec) {
		this.noticec = noticec;
	}

	public String getWitnesserc() {
		return witnesserc;
	}

	public void setWitnesserc(String witnesserc) {
		this.witnesserc = witnesserc;
	}

	public Date getWitnessdatec() {
		return witnessdatec;
	}

	public void setWitnessdatec(Date witnessdatec) {
		this.witnessdatec = witnessdatec;
	}

	public String getNoticed() {
		return noticed;
	}

	public void setNoticed(String noticed) {
		this.noticed = noticed;
	}

	public String getWitnesserd() {
		return witnesserd;
	}

	public void setWitnesserd(String witnesserd) {
		this.witnesserd = witnesserd;
	}

	public Date getWitnessdated() {
		return witnessdated;
	}

	public void setWitnessdated(Date witnessdated) {
		this.witnessdated = witnessdated;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getNoticeczecqc() {
		return noticeczecqc;
	}

	public void setNoticeczecqc(String noticeczecqc) {
		this.noticeczecqc = noticeczecqc;
	}

	public Integer getWitnesserczecqc() {
		return witnesserczecqc;
	}

	public void setWitnesserczecqc(Integer witnesserczecqc) {
		this.witnesserczecqc = witnesserczecqc;
	}

	public Date getWitnessdateczecqc() {
		return witnessdateczecqc;
	}

	public void setWitnessdateczecqc(Date witnessdateczecqc) {
		this.witnessdateczecqc = witnessdateczecqc;
	}

	public String getNoticeczecqa() {
		return noticeczecqa;
	}

	public void setNoticeczecqa(String noticeczecqa) {
		this.noticeczecqa = noticeczecqa;
	}

	public Integer getWitnesserczecqa() {
		return witnesserczecqa;
	}

	public void setWitnesserczecqa(Integer witnesserczecqa) {
		this.witnesserczecqa = witnesserczecqa;
	}

	public Date getWitnessdateczecqa() {
		return witnessdateczecqa;
	}

	public void setWitnessdateczecqa(Date witnessdateczecqa) {
		this.witnessdateczecqa = witnessdateczecqa;
	}

	public String getNoticepaec() {
		return noticepaec;
	}

	public void setNoticepaec(String noticepaec) {
		this.noticepaec = noticepaec;
	}

	public Integer getWitnesserpaec() {
		return witnesserpaec;
	}

	public void setWitnesserpaec(Integer witnesserpaec) {
		this.witnesserpaec = witnesserpaec;
	}

	public Date getWitnessdatepaec() {
		return witnessdatepaec;
	}

	public void setWitnessdatepaec(Date witnessdatepaec) {
		this.witnessdatepaec = witnessdatepaec;
	}

	public boolean isLastone() {
		return lastone;
	}

	public void setLastone(boolean lastone) {
		this.lastone = lastone;
	}

	public Integer getRowStart() {
		return rowStart;
	}

	public void setRowStart(Integer rowStart) {
		this.rowStart = rowStart;
	}

	public Integer getRowEnd() {
		return rowEnd;
	}

	public void setRowEnd(Integer rowEnd) {
		this.rowEnd = rowEnd;
	}

	public RollingPlan getRollingPlan() {
		return rollingPlan;
	}

	public void setRollingPlan(RollingPlan rollingPlan) {
		this.rollingPlan = rollingPlan;
	}

	public List<WorkStepWitness> getWorkStepWitnesses() {
		
//		List<WorkStepWitness> not = new ArrayList<WorkStepWitness>();
		
		if(workStepWitnesses!=null&& workStepWitnesses.size()>0){
			return workStepWitnesses.stream().filter(b->b!=null && !"EXPIRED".equals(b.getWitnessflag())).collect(Collectors.toList());
		}
		
		return workStepWitnesses;
	}

	public void setWorkStepWitnesses(List<WorkStepWitness> workStepWitnesses) {
		this.workStepWitnesses = workStepWitnesses;
	}

	public List<WorkStepWitnessFake> getWitnessInfo() {
		return witnessInfo;
	}

	public void setWitnessInfo(List<WorkStepWitnessFake> witnessInfo) {
		this.witnessInfo = witnessInfo;
	}

	public List<WorkStepWitnessFake> getWitnessesAssign() {
		return witnessesAssign;
	}

	public void setWitnessesAssign(List<WorkStepWitnessFake> witnessesAssign) {
		this.witnessesAssign = witnessesAssign;
	}

	public Set<String> getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(Set<String> noticeType) {
		this.noticeType = noticeType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}