package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>滚动计划附属材料信息表<br>
 * <b>创建时间：</b>2017-10-23 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 * 20180207会议纪要中提到，焊口作业条目不推送材料信息
 */
@Entity
@Table(name="enpower_material_info")
@Data
@EqualsAndHashCode(callSuper=false)
public class EnpowerMaterialInfo extends BasicForm implements Serializable {
	private static final long serialVersionUID = 7991371355432184827L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	/**
	 * 滚动计划id
	 */
	@Column(name = "rolling_plan_id", nullable = false)
	private Integer rollingPlanId;

	/**
	 * enpower材料主键id
	 */
	@Column(name = "enpower_material_id", nullable = false)
	private String enpowerMaterialId;
	/**
	 * enpower计划主键id
	 */
	@Column(name = "enpower_plan_id", nullable = false)
	private String enpowerPlanId;
	/**
	 * 物项名称
	 */
	@Column(name = "material_name", nullable = false)
	private String materialName;
	/**
	 * 物项编号
	 */
	@Column(name = "material_identifier", nullable = false)
	private String materialIdentifier;
	/**
	 * 规格型号
	 */
	@Column(name = "specification_model", nullable = false)
	private String specificationModel;
	/**
	 * 材质
	 */
	@Column(name = "material_quality", nullable = false)
	private String materialQuality;
	
	/**
	 * 计划用量
	 */
	@Column(name = "plan_dosage", nullable = false)
	private String planDosage;
	/**
	 * 实际用量
	 */
	@Column(name = "actual_dosage")
	private String actualDosage;
	/**
	 * 见证人id
	 */
	@Column(name = "recorder_id")
	private Integer recorderId;
	/**
	 * 见证人姓名
	 */
	@Column(name = "recorder_name")
	private String recorderName;
	/**
	 * 反填时间
	 */
	@Column(name = "recorder_time")
	private Date recorderTime;
	/**
	 * 材料状态:enpowered:已同步至enpower
	 */
	@Column(name = "status")
	private String status;
	/**单位 预留*/
	@Column(name = "unit")
	private String unit ;
	@Column(name = "is_del")
	private Boolean isDel;
}
