package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.form.WorkStepWitnessFake;
import com.easycms.hd.witness.domain.WitnessFile;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "work_step_witness")
@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepWitness extends BasicForm implements Serializable, Comparable<WorkStepWitness> {
	private static final long serialVersionUID = -2544231879904698079L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "witness", length = 50)
	private Integer witness;
	@Column(name = "witnessmonitor", length = 50)
	private Integer witnessmonitor;
	@Transient
	private String witnessMonitorName;
	@Column(name = "witnessteam", length = 50)
	private Integer witnessteam;
	@Transient
	private String witnessTeamName;
	
	@Transient
	private String witnessName;
	@Column(name = "witnesser")
	private Integer witnesser;
	@Column(name = "substitute")
	private String substitute;
	@Transient
	private String witnesserName;
	@Column(name = "witnessdes", length = 10000)
	private String witnessdes;
	@Column(name = "witnessaddress", length = 10000)
	private String witnessaddress;
	@Column(name = "witnessdate", length = 19)
	private Date witnessdate;
	@Column(name = "status")
	private Integer status;
	@Column(name = "isok")
	private Integer isok;//　２　自动见证　见证结果，1-不合格，3-合格
	@Column(name = "fail_type")
	private String failType;
	@Column(name = "remark", length = 50)
	private String remark;
	@Column(name = "triggerName", length = 50)
	private String triggerName;
	@Column(name = "notice_type", length = 10)
	private String noticeType;
	@Column(name = "notice_point", length = 10)
	private String noticePoint;
	@Column(name = "created_on", length = 19)
	private Date createdOn;
	@Column(name = "created_by", length = 40)
	private Integer createdBy;
	@Column(name = "updated_on", length = 19)
	private Date updatedOn;
	@Column(name = "updated_by", length = 40)
	private Integer updatedBy;
	@Column(name = "noticeresultdesc", length = 1000)
	private String noticeresultdesc;
	@Column(name = "witnessflag")
	private String witnessflag;
	
	@Column(name = "dosage", length = 50)
	private String dosage;
	@Column(name = "real_witnessaddress", length = 100)
	private String realwitnessaddress;
	@Column(name = "real_witnessdata")
	private Date realwitnessdata;
	@Transient
	@Column(name = "stepno")
	private Integer stepno;
	
	@Column(name = "batch_id")
	private String batchId;
	
	@Transient
	private Integer seq;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name = "stepno", nullable = false)
	private WorkStep workStep;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade={CascadeType.REMOVE})
	@JoinColumn(name="parent_id")
	private WorkStepWitness parent;
	
	@JsonIgnore
	@OneToMany(fetch=FetchType.EAGER,mappedBy="parent",cascade={CascadeType.REMOVE})
	private List<WorkStepWitness> children;
	
	@Transient
	private List<WorkStepWitnessFake> witnessesAssign;
	
	@Transient
	private List<WitnessFile> witnessFiles;

	@Override
	public int compareTo(WorkStepWitness wsw) {
		int flag = this.getCreatedOn().compareTo(wsw.getCreatedOn());
		if (flag == 0) {
			NoticePointType npt1 = NoticePointType.valueOf(noticePoint);
			NoticePointType npt2 = NoticePointType.valueOf(wsw.getNoticePoint());
			return npt1.compareTo(npt2);
		} else {
			return flag;
		}
	}
}