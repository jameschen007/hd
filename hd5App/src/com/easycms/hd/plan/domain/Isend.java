package com.easycms.hd.plan.domain;

public class Isend {
	public static int INITIAL = 0;
	public static int UNDER_CONSTRUCTION = 1;
	public static int COMPLETE = 2;
	public static int RETURN = 3;
	public static int ARRANGED = 4;
	public static int PROBLEM = 5;
}
