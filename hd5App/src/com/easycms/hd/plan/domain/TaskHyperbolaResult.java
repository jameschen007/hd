package com.easycms.hd.plan.domain;

import java.io.Serializable;
import java.math.BigInteger;

public class TaskHyperbolaResult implements Serializable{
	private static final long serialVersionUID = 6473289325391555839L;
	private String day;
	private BigInteger amount;
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public BigInteger getAmount() {
		return amount;
	}
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "TaskHyperbolaResult [day=" + day + ", amount=" + amount + "]";
	}
}
