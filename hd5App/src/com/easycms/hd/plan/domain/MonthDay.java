package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name = "MONTH_DAY")
@Cacheable
public class MonthDay extends BasicForm implements Serializable {
	private static final long serialVersionUID = 8067982615955057775L;
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "DAY_NUM", unique = true, nullable = false)
	private Integer dayNum;
	
	public Integer getDayNum() {
		return dayNum;
	}
	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}
}