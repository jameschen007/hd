package com.easycms.hd.plan.domain;

import java.io.Serializable;
import java.util.List;

import com.easycms.management.user.domain.Department;

public class TeamMap implements Serializable{
	private static final long serialVersionUID = 8484130702603347755L;
	
	private Department parent;
	private List<Department> children;
	
	public Department getParent() {
		return parent;
	}
	public void setParent(Department parent) {
		this.parent = parent;
	}
	public List<Department> getChildren() {
		return children;
	}
	public void setChildren(List<Department> children) {
		this.children = children;
	}
}
