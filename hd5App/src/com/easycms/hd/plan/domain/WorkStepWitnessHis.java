package com.easycms.hd.plan.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.enums.NoticePointType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "work_step_witness_his")
@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepWitnessHis extends BasicForm implements Serializable, Comparable<WorkStepWitnessHis> {
	private static final long serialVersionUID = -2544231879904698079L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "stepno")
	private Integer stepno;
	
	@Column(name = "witnessmonitor", length = 50)
	private Integer witnessmonitor;
	
	@Column(name = "witness", length = 50)
	private Integer witness;
	@Transient
	private String witnessMonitorName;
	@Column(name = "witnessteam", length = 50)
	private Integer witnessteam;
	@Transient
	private String witnessTeamName;
	
	@Transient
	private String witnessName;
	@Column(name = "witnesser")
	private Integer witnesser;
	@Transient
	private String witnesserName;
	
	
	@Column(name = "status")
	private Integer status;
	@Column(name = "isok")
	private Integer isok;
	
	@Column(name = "notice_type", length = 10)
	private String noticeType;
	@Column(name = "notice_point", length = 10)
	private String noticePoint;
	@Column(name = "witnessflag")
	private String witnessflag;
	
	@Override
	public int compareTo(WorkStepWitnessHis wsw) {
		int flag = this.getId().compareTo(wsw.getId());
		if (flag == 0) {
			NoticePointType npt1 = NoticePointType.valueOf(noticePoint);
			NoticePointType npt2 = NoticePointType.valueOf(wsw.getNoticePoint());
			return npt1.compareTo(npt2);
		} else {
			return flag;
		}
	}

}