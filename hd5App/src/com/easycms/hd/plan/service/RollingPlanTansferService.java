package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.hd.plan.domain.RollingPlan;

public interface RollingPlanTansferService {
	public List<RollingPlan> transferRollingPlan(List<RollingPlan> rollingPlans);
	public RollingPlan transferRollingPlan(RollingPlan rollingPlan);
	RollingPlan markNoticePoint(RollingPlan rollingPlan);
	RollingPlan markBackFill(RollingPlan rollingPlan);
	List<RollingPlan> markBackFill(List<RollingPlan> rollingPlans);
}
