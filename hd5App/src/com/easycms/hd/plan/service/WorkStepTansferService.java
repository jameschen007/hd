package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepenp;

public interface WorkStepTansferService {
	public List<WorkStep> transferWorkStep(List<WorkStep> workSteps);
	public WorkStep transferWorkStep(WorkStep workStep);
	public WorkStep addWitnessInfo(WorkStep workStep);
	public WorkStepenp addWitnessInfo(WorkStepenp workStepenp);
}
