package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStepenp;

public interface WorkStepenpService{
	
	List<WorkStepenp> findAll();
	
	List<WorkStepenp> findAll(WorkStepenp condition);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	
	Page<WorkStepenp> findPage(Page<WorkStepenp> page);
	
	Page<WorkStepenp> findPageByRollingPlanId(Page<WorkStepenp> page, Integer rollingPlanId);
	
	Page<WorkStepenp> findPage(WorkStepenp condition,Page<WorkStepenp> page);
	/**
	 * 根据ID查找
	 * @param id
	 * @return
	 */
	WorkStepenp findById(Integer id);
	
	WorkStepenp getById(Integer id);
	
	List<WorkStepenp> findByIDs(Integer[] ids);
	
	boolean delete(Integer id);
	
	boolean delete(Integer[] ids);
	
	boolean deleteByIDUP(Integer[] ids);
	
	
	/**
	 * 添加工序步骤
	 * @param workStep
	 * @return
	 */
	WorkStepenp add(WorkStepenp workStep);
}
