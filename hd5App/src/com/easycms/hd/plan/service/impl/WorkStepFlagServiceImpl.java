package com.easycms.hd.plan.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.hd.api.enums.WitnessFlagEnum;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.Isend;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WorkStepFlagService;
import com.easycms.hd.plan.service.WorkStepService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("workStepFlagService")
public class WorkStepFlagServiceImpl implements WorkStepFlagService {
	
	private static Logger logger = Logger.getLogger(WorkStepFlagServiceImpl.class);
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	@Autowired
	private JPushService jPushService;
	
	@SuppressWarnings("unchecked")
	@Override
	public void changeStepFlag(Integer rollingPlanId,String dosage) throws Exception {
		RollingPlan rollingPlan = rollingPlanService.findById(rollingPlanId);
		if (null != rollingPlan){
			List<WorkStep> workStepList = rollingPlan.getWorkSteps();
	
			if (null != workStepList && workStepList.size() > 0){
				Collections.sort(workStepList);
				  
				for (int i = 0; i < workStepList.size(); i++){
					WorkStep ws = workStepList.get(i);
					
					if (ws.getStepflag().equals(StepFlag.PREPARE) || ws.getStepflag().equals(StepFlag.PAUSE)){
						logger.info("rollingPlanId="+rollingPlanId+"  "+ws.getId()+"  stepNaeme="+ws.getStepname()+" stepflag="+ws.getStepflag());
						return;
					}
					
					if (ws.getStepflag().equals(StepFlag.UNDO)){
						logger.info("rollingPlanId="+rollingPlanId+"  "+ws.getId()+"  stepNaeme="+ws.getStepname()+" stepflag=UNDO");
						ws.setStepflag(StepFlag.PREPARE);
						workStepService.add(ws);
						return;
					}
				}
				
				long successCount = workStepList.stream().filter(workStep -> workStep.getStepflag().equals(StepFlag.DONE)).count();
				
				if (workStepList.size() == successCount){
					log.info("[" + rollingPlan.getDrawno() + " - " + rollingPlan.getWeldno() + "] All complate.");
					rollingPlan.setIsend(Isend.COMPLETE);
					if (null == rollingPlan.getQcdate()){
						rollingPlan.setQcdate(new Date());
					}
					if (null == rollingPlan.getQcsign()){
						rollingPlan.setQcsign(1);
					}
					rollingPlan.setWitnessflag(WitnessFlagEnum.COMPLETED.name());
					rollingPlan.setEnddate(new Date());
					rollingPlan.setIsReport("Y");//已报量
					if(dosage!=null && dosage.matches("\\d+[.]*\\d*")){
						rollingPlan.setRealProjectcost(dosage);
					}
					rollingPlanService.add(rollingPlan);
					executePushToEnpowerServiceImpl.updateEnpowerStatus(rollingPlan,null,dosage);
					String message = "滚动计划：" + rollingPlan.getDrawno() + " 所有工序已完成。";
			        JPushExtra extra = new JPushExtra();
					extra.setCategory(JPushCategoryEnums.ROLLINGPLAN_COMPLETE.name());
					jPushService.pushByUserId(extra, message, JPushCategoryEnums.ROLLINGPLAN_COMPLETE.getName(), rollingPlan.getConsteam());
					
					return;
				}
			}
		}
	}
}
