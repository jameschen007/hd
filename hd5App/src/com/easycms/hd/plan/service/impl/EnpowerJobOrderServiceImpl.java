package com.easycms.hd.plan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.dao.EnpowerJobOrderDao;
import com.easycms.hd.plan.domain.EnpowerJobOrder;
import com.easycms.hd.plan.service.EnpowerJobOrderService;
@Service("enpowerJobOrderService")
public class EnpowerJobOrderServiceImpl implements EnpowerJobOrderService{
	
	@Autowired
	private EnpowerJobOrderDao enpowerJobOrder;
	@Override
	public boolean update(EnpowerJobOrder rollingPlan) {
		return enpowerJobOrder.save(rollingPlan) != null;
	}

	@Override
	public EnpowerJobOrder add(EnpowerJobOrder rollingPlan) {
		return enpowerJobOrder.save(rollingPlan);
	}

	@Override
	public EnpowerJobOrder findById(Integer id) {
		return enpowerJobOrder.findById(id);
	}
	@Override
	public List<EnpowerJobOrder> findByRollingPlanId(Integer id) {
		return enpowerJobOrder.findByRollingPlanId(id);
	}
}
