package com.easycms.hd.plan.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.form.WorkStepWitnessFake;
import com.easycms.hd.plan.service.WorkStepTansferService;
import com.easycms.hd.plan.service.WorkStepWitnessTansferService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

@Service("workStepWitnessTansferService")
public class WorkStepWitnessTansferServiceImpl implements WorkStepWitnessTansferService{
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepTansferService workStepTansferService;
	
	@Override
	public List<WorkStepWitness> transferWorkStepWitness(List<WorkStepWitness> workStepWitnesses){
		if (null != workStepWitnesses){
			for(WorkStepWitness workStepWitness : workStepWitnesses){
				workStepWitness = transferWorkStepWitness(workStepWitness);
			}
		}
		return workStepWitnesses;
	}

	@Override
	public WorkStepWitness transferWorkStepWitness(WorkStepWitness workStepWitness) {
		if (null != workStepWitness){
			if (null != workStepWitness.getWorkStep()){
				WorkStep workStep = workStepWitness.getWorkStep();
				
				if (null != workStep.getRollingPlan()){
					RollingPlan rollingPlan = workStep.getRollingPlan();
					
					if (null != rollingPlan.getConsteam()){
						Department department = departmentService.findById(rollingPlan.getConsteam());
						
						if (null != department){
							rollingPlan.setConsteamName(department.getName());
						} else {
							rollingPlan.setConsteamName(null);
						}
					}
					
					if (null != rollingPlan.getConsendman() && !"".equals(rollingPlan.getConsendman())){
						User user = userService.findUserById(Integer.parseInt(rollingPlan.getConsendman()));
						
						if (null != user){
							rollingPlan.setConsendmanName(user.getRealname());
						} else {
							rollingPlan.setConsendmanName(null);
						}
					}
					
					List<WorkStep> wsList = rollingPlan.getWorkSteps();
					if (null != wsList && wsList.size() > 0){
						for (WorkStep ws : wsList){
							if (!ws.getStepflag().equals(StepFlag.UNDO) && !ws.getStepflag().equals(StepFlag.PREPARE)){
								rollingPlan.setStepStart(true);
								break;
							}
						}
					}
					workStep.setRollingPlan(rollingPlan);
				}
				
				workStepWitness.setWorkStep(workStep);
			}
			
		}
		return workStepWitness;
	}
	@Override
	public List<WorkStepWitness> transferChildren(List<WorkStepWitness> wswList){
		for (WorkStepWitness wsw : wswList){
			if (null != wsw.getChildren()){
				if (null != wsw.getWorkStep()){
					wsw.setNoticeresultdesc(wsw.getWorkStep().getNoticeresultdesc());
				} 
				
				wsw.setWitnessesAssign(toFake(transferWitness(wsw.getChildren())));
			}
			
			wsw = transferWitness(wsw);
		}
		return wswList;
	}
	@Override
	public List<WorkStepWitnessFake> toFake(List<WorkStepWitness> wswList){
		List<WorkStepWitnessFake> wswNewList = new ArrayList<WorkStepWitnessFake>();
		for (WorkStepWitness wsw : wswList){
			WorkStepWitnessFake wswFake = new WorkStepWitnessFake();
			try {
				BeanUtils.copyProperties(wswFake, wsw);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			wswNewList.add(wswFake);
		}
		
		return wswNewList;
	}
	@Override
	public List<WorkStepWitness> transferWitness(List<WorkStepWitness> datas){
		if (null != datas){
			for(WorkStepWitness workStepWitness : datas){
				workStepWitness = transferWitness(workStepWitness);
			}
		}
		return datas;
	}
	
	@Override
	public WorkStepWitness transferWitness(WorkStepWitness workStepWitness){
		if (null != workStepWitness.getWitness() && !"".equals(workStepWitness.getWitness())){
			Department department = departmentService.findById(workStepWitness.getWitness());
			
			if (null != department){
				workStepWitness.setWitnessName(department.getName());
			} else {
				workStepWitness.setWitnessName(null);
			}
		}
		
		if (null != workStepWitness.getWitnesser()){
			User user = userService.findUserById(workStepWitness.getWitnesser());
			
			if (null != user){
				workStepWitness.setWitnesserName(user.getRealname());
			} else {
				workStepWitness.setWitnesserName(null);
			}
		}
		
		if (null != workStepWitness.getWorkStep()){
			WorkStep workStep = workStepWitness.getWorkStep();
			workStep = workStepTansferService.transferWorkStep(workStepTansferService.addWitnessInfo(workStep));
			workStepWitness.setWorkStep(workStep);
		}
		
		return workStepWitness;
	}
}