package com.easycms.hd.plan.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.dao.StatisticsRollingPlanDao;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.StatisticsRollingPlanService;
import com.easycms.hd.statistics.util.StatisticsUtil;

@Service("statisticsRollingPlanService")
public class StatisticsRollingPlanServiceImpl implements StatisticsRollingPlanService {

	@Autowired
	private StatisticsRollingPlanDao statisticsRollingPlanDao;

	@Override
	public boolean update(RollingPlan rollingPlan) {
		return statisticsRollingPlanDao.save(rollingPlan) != null;
	}

	@Override
	public RollingPlan add(RollingPlan website) {
		return statisticsRollingPlanDao.save(website);
	}

	@Override
	public RollingPlan findById(Integer id) {
		return statisticsRollingPlanDao.findById(id);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != statisticsRollingPlanDao.deleteById(id)) {
			return true;
		}
		return false;
	}

	@Override
	public List<RollingPlan> findAll() {
		return statisticsRollingPlanDao.findAll();
	}

	@Override
	public List<RollingPlan> findAll(RollingPlan condition) {
		return statisticsRollingPlanDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public Page<RollingPlan> findPage(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = statisticsRollingPlanDao
				.findAll(null, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPage(RollingPlan condition,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = statisticsRollingPlanDao
				.findAll(QueryUtil.queryConditions(condition), pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPageByStatus(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = null;

		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			
			if (category.equals("dateAfter")){
				date = StatisticsUtil.getDate(1);
			} else if (category.equals("dateBefore")){
				date = StatisticsUtil.getDate(-1);
			}
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(
						pageable, type, date);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(
						pageable, type, date);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(
						pageable, type, date);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(
						pageable, type, date);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(
						pageable, type, new Date());
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(
						pageable, type, date);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(
						pageable, type, new Date());
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(
						pageable, type, new Date());
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(
						pageable, type, new Date());
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(
						pageable, type, new Date());
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(
						pageable, type, new Date());
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(
						pageable, type, new Date());
			}
		} else if (category.equals("dateMonth")) {
			date = date.substring(0, 7);
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(
						pageable, type, date);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(
						pageable, type, date);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(
						pageable, type, date);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(
						pageable, type, date);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(
						pageable, type, new Date());
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(
						pageable, type, date);
			}
		} else if (category.equals("dateYear")) {
			date = date.substring(0, 4);
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(
						pageable, type, date);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(
						pageable, type, date);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(
						pageable, type, date);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(
						pageable, type, date);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(
						pageable, type, new Date());
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(
						pageable, type, date);
			}
		} else {
			return null;
		}

		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPageByStatusClasz(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = null;

		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(
						pageable, type, date, ids);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(
						pageable, type, new Date(), ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(
						pageable, type, new Date(), ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(
						pageable, type, new Date(), ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(
						pageable, type, new Date(), ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(
						pageable, type, new Date(), ids);
			}
		} else if (category.equals("dateMonth")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(
						pageable, type, date, ids);
			}
		} else if (category.equals("dateYear")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(
						pageable, type, date, ids);
			}
		} else {
			return null;
		}

		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findPageByStatusGroup(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = null;

		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0ByGroup(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1ByGroup(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2ByGroup(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4ByGroup(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3ByGroup(
						pageable, type, date, ids);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3ByGroup(
						pageable, type, new Date(), ids);
			}
		} else if (category.equals("dateMonth")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0ByGroup(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1ByGroup(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2ByGroup(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4ByGroup(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3ByGroup(
						pageable, type, date, ids);
			}
		} else if (category.equals("dateYear")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0ByGroup(
						pageable, type, date, ids);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1ByGroup(
						pageable, type, date, ids);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2ByGroup(
						pageable, type, date, ids);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4ByGroup(
						pageable, type, date, ids);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5ByGroup(
						pageable, type, new Date(), ids);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3ByGroup(
						pageable, type, date, ids);
			}
		} else {
			return null;
		}

		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findPageByStatus(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = null;

		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			
			if (category.equals("dateAfter")){
				date = StatisticsUtil.getDate(1);
			} else if (category.equals("dateBefore")){
				date = StatisticsUtil.getDate(-1);
			}
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(
						pageable, type, date, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(
						pageable, type, date, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(
						pageable, type, date, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(
						pageable, type, date, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(
						pageable, type, new Date(), keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(
						pageable, type, date, keyword);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(
						pageable, type, new Date(), keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(
						pageable, type, new Date(), keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(
						pageable, type, new Date(), keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(
						pageable, type, new Date(), keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(
						pageable, type, new Date(), keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(
						pageable, type, new Date(), keyword);
			}
		} else if (category.equals("dateMonth")) {
			date = date.substring(0, 7);
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(
						pageable, type, date, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(
						pageable, type, date, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(
						pageable, type, date, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(
						pageable, type, date, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(
						pageable, type, new Date(), keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(
						pageable, type, date, keyword);
			}
		} else if (category.equals("dateYear")) {
			date = date.substring(0, 4);
			
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(
						pageable, type, date, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(
						pageable, type, date, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(
						pageable, type, date, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(
						pageable, type, date, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(
						pageable, type, new Date(), keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(
						pageable, type, date, keyword);
			}
		} else {
			return null;
		}

		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findPageByStatusClasz(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = null;
		
		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(
						pageable, type, date, ids, keyword);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(
						pageable, type, new Date(), ids, keyword);
			}
		} else if (category.equals("dateMonth")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(
						pageable, type, date, ids, keyword);
			}
		} else if (category.equals("dateYear")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(
						pageable, type, date, ids, keyword);
			}
		} else {
			return null;
		}
		
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findPageByStatusGroup(Page<RollingPlan> page,
			Integer status, String type, String date, String category,
			Integer hysteresis, String[] ids, String keyword) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = null;
		
		if (category.equals("dateAfter") || category.equals("dateCurrent")
				|| category.equals("dateBefore")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus0ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus1ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus2ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusDayStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus4ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus5ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusDayStatus3ByGroup(
						pageable, type, date, ids, keyword);
			}
		} else if (category.equals("dateWeek")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusWeekStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3ByGroup(
						pageable, type, new Date(), ids, keyword);
			}
		} else if (category.equals("dateMonth")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusMonthStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3ByGroup(
						pageable, type, date, ids, keyword);
			}
		} else if (category.equals("dateYear")) {
			if (status == 0) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus0ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 1) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus1ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 2) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus2ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 6) {
				// springPage =
				// rollingPlanDao.getRollingPlanStatusYearStatus6(pageable,
				// type, new Date(), hysteresis);
			} else if (status == 4) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus4ByGroup(
						pageable, type, date, ids, keyword);
			} else if (status == 5) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus5ByGroup(
						pageable, type, new Date(), ids, keyword);
			} else if (status == 3) {
				springPage = statisticsRollingPlanDao.getRollingPlanStatusYearStatus3ByGroup(
						pageable, type, date, ids, keyword);
			}
		} else {
			return null;
		}
		
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	// ------------------------------------task
	// team------------------------------------------------
	@Override
	public Long getRollingPlanSurplus(String id, String type) {
		return statisticsRollingPlanDao.getRollingPlanSurplus(id, type);
	}

	@Override
	public Long getRollingPlanComplate(String id, String type) {
		return statisticsRollingPlanDao.getRollingPlanComplate(id, type);
	}

	@Override
	public Long getRollingPlanTotal(String id, String type) {
		return statisticsRollingPlanDao.getRollingPlanTotal(id, type);
	}

	// -----------------------------------------------------------------------------------------------
	@Override
	public List<Object[]> getRollingPlanTaskHyperbola(String month, String type) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbola(month, type);
	}

	@Override
	public List<Object[]> getRollingPlanTaskHyperbolaByDate(String startTime,
			String endTime, String type) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbolaByDate(startTime,
				endTime, type);
	}

	// -----------------------------------------task
	// status------------------------------------------
	@Override
	public Long getRollingPlanStatusDayStatus0(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(type, date);
	}

	@Override
	public Long getRollingPlanStatusDayStatus1(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(type, date);
	}

	@Override
	public Long getRollingPlanStatusDayStatus2(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(type, date);
	}

	@Override
	public Long getRollingPlanStatusDayStatus6(String type, Date date,
			Integer diff) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus6(type, date, diff);
	}

	@Override
	public Long getRollingPlanStatusDayStatus4(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(type, date);
	}

	@Override
	public Long getRollingPlanStatusDayStatus5(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(type, date);
	}

	@Override
	public Long getRollingPlanStatusDayStatus3(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(type, date);
	}

	// --------------Week
	@Override
	public Long getRollingPlanStatusWeekStatus0(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(type, date);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus1(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(type, date);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus2(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(type, date);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus3(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(type, date);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus6(String type, Date date,
			Integer diff) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus6(type, date, diff);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus4(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(type, date);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus5(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(type, date);
	}

	// --------------Month
	@Override
	public Long getRollingPlanStatusMonthStatus0(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(type, date);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus1(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(type, date);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus2(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(type, date);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus3(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(type, date);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus6(String type, Date date,
			Integer diff) {
		return statisticsRollingPlanDao
				.getRollingPlanStatusMonthStatus6(type, date, diff);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus4(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(type, date);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus5(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(type, date);
	}

	// --------------Year
	@Override
	public Long getRollingPlanStatusYearStatus0(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(type, date);
	}

	@Override
	public Long getRollingPlanStatusYearStatus1(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(type, date);
	}

	@Override
	public Long getRollingPlanStatusYearStatus2(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(type, date);
	}

	@Override
	public Long getRollingPlanStatusYearStatus3(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(type, date);
	}

	@Override
	public Long getRollingPlanStatusYearStatus6(String type, Date date,
			Integer diff) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus6(type, date, diff);
	}

	@Override
	public Long getRollingPlanStatusYearStatus4(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(type, date);
	}

	@Override
	public Long getRollingPlanStatusYearStatus5(String type, String date) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(type, date);
	}

	// ------------------------------------task
	// group------------------------------------------------
	@Override
	public Long getRollingPlanCompleteByConsEndmanAndType(String id, String type) {
		return statisticsRollingPlanDao.getRollingPlanCompleteByConsEndmanAndType(id,
				type);
	}

	@Override
	public Long getRollingPlanUnCompleteByConsEndmanAndType(String id,
			String type) {
		return statisticsRollingPlanDao.getRollingPlanUnCompleteByConsEndmanAndType(id,
				type);
	}

	@Override
	public Long getRollingPlanTotalByConsEndman(String id) {
		return statisticsRollingPlanDao.getRollingPlanTotalByConsEndman(id);
	}
	
	@Override
	public Long getRollingPlanTotalByConsEndman(String id, String type) {
		return statisticsRollingPlanDao.getRollingPlanTotalByConsEndman(id, type);
	}

	@Override
	public List<Object[]> getRollingPlanTaskHyperbolaByGroup(String month,
			String weldHK, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbolaByGroup(month, weldHK, ids);
	}

	// ------------------------------------statistic by
	// department------------------------------------------------

	@Override
	public Long getRollingPlanStatusDayStatus0(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus0(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus1(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus1(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus2(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus2(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus6(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus6(type, date,
				hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus4(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus4(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus5(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus5(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus0(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus0(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus1(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus2(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus6(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus6(type, date,
				hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus4(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus5(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus5(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus0(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus0(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus1(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus2(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus6(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus6(type, date,
				hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus4(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus5(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus5(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus0(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus0(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus1(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus1(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus2(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus2(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus6(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus6(type, date,
				hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus4(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus4(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus5(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus5(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus3(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus3(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus3(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus3(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus3(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus3(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusAllStatus0(String type, Date date,
			String[] ids) {
		//
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus0(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusAllStatus5(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus5(type, date, ids);
	}

	@Override
	public Long getRollingPlanStatusAllStatus0(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus0(type, date);
	}

	@Override
	public Long getRollingPlanStatusAllStatus5(String type, Date date) {
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus5(type, date);
	}

	@Override
	public Long getRollingPlanStatusAllStatus0ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus0ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus1ByGroup(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus1ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus2ByGroup(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus2ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus6ByGroup(type, date,
				hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus4ByGroup(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus4ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusAllStatus5ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusAllStatus5ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusDayStatus3ByGroup(String type, String date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusDayStatus3ByGroup(type, date,
				ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus1ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus1ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus2ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus2ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus6ByGroup(type,
				date, hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus4ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusWeekStatus3ByGroup(String type, Date date,
			String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus3ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus1ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus1ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus2ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus2ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus6ByGroup(type,
				date, hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus4ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusMonthStatus3ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus3ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus1ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus1ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus2ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus2ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus6ByGroup(String type, Date date,
			Integer hysteresis, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus6ByGroup(type,
				date, hysteresis, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus4ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus4ByGroup(type,
				date, ids);
	}

	@Override
	public Long getRollingPlanStatusYearStatus3ByGroup(String type,
			String date, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanStatusYearStatus3ByGroup(type,
				date, ids);
	}

	@Override
	public List<Object[]> getRollingPlanTaskHyperbola(String month,
			String weldHK, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbola(month,weldHK,ids);
	}

	@Override
	public List<Object[]> getRollingPlanTaskHyperbolaByGroupAndDuration(
			String fromDate, String toDate, String weldHK, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbolaByGroupAndDuration(fromDate,  toDate,  weldHK,ids);
	}

	@Override
	public List<Object[]> getRollingPlanTaskHyperbolaByDuration(
			String fromDate, String toDate, String weldHK, String[] ids) {
		return statisticsRollingPlanDao.getRollingPlanTaskHyperbolaByDuration( fromDate,  toDate, weldHK, ids);
	}

  @Override
  public Long getRollingPlanStatusAllStatus0ByClasz(String type, String[] ids) {
    return statisticsRollingPlanDao.getRollingPlanStatusAllStatus0ByClasz(type,ids);
  }

  @Override
  public Long getRollingPlanStatusAllStatus0ByPlanner(String type) {
    return statisticsRollingPlanDao.getRollingPlanStatusAllStatus0ByPlanner(type);
  }

  @Override
  public Long getRollingPlanStatusDayStatus4ByClasz(String type, String date, String[] ids) {
    return statisticsRollingPlanDao.getRollingPlanStatusDayStatus4ByClasz(type,date,ids);
  }

  @Override
  public Long getRollingPlanStatusWeekStatus4ByClasz(String type, Date date, String[] ids) {
    return statisticsRollingPlanDao.getRollingPlanStatusWeekStatus4ByClasz(type,date,ids);
  }

  @Override
  public Long getRollingPlanStatusMonthStatus4ByClasz(String type, String date, String[] ids) {
    return statisticsRollingPlanDao.getRollingPlanStatusMonthStatus4ByClasz(type,date,ids);
  }

  @Override
  public Long getRollingPlanStatusYearStatus4ByClasz(String type, String date, String[] ids) {
    return statisticsRollingPlanDao.getRollingPlanStatusYearStatus4ByClasz(type,date,ids);
  }
}
