package com.easycms.hd.plan.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.enums.WitnessFlagEnum;
import com.easycms.hd.plan.dao.WitnessHisDao;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.domain.WorkStepWitnessHis;

@Service("witnessHisServiceImpl")
public class WitnessHisServiceImpl{
	@Autowired
	private WitnessHisDao witnessHisDao;
	

	public WorkStepWitnessHis findByStepnoAndWitnessflagAndNoticePoint(WorkStep wokstep,String status,String noticePoint){
		
		List<WorkStepWitnessHis> list = witnessHisDao.findByStepnoAndWitnessflagAndNoticePoint(wokstep.getId(), status,noticePoint);
		
		if(list!=null && list.size()>0){
			//先取有QC组员的，
			WorkStepWitnessHis wsw = list.stream().filter(w ->{
				//cancel 和expire的只取cancel的吗？
				return w.getWitnesser()!=null && w.getWitness()!=null && w.getWitnessteam()!=null && w.getWitnessmonitor()!=null;//见证组长，见证组员　，
			}).findFirst().orElse(null);
			
			
			//如果没有，再取有QC组长的。
			if(wsw!=null){
				return wsw ;
			}else{
				wsw = list.stream().filter(w ->{
					//cancel 和expire的只取cancel的吗？
					return  w.getWitness()!=null && w.getWitnessteam()!=null && w.getWitnessmonitor()!=null;//见证组长，见证组员　，
				}).findFirst().orElse(null);
				
				if(wsw!=null){
					return wsw ;
				}else{
					wsw = list.stream().filter(w ->{
						//cancel 和expire的只取cancel的吗？
						return  w.getWitnessteam()!=null && w.getWitnessmonitor()!=null;//见证组长，见证组员　，
					}).findFirst().orElse(null);
					
					if(wsw!=null){
						return wsw ;
					}else{
						wsw = list.stream().filter(w ->{
							//cancel 和expire的只取cancel的吗？
							return w.getWitnessmonitor()!=null;//见证组长，见证组员　，
						}).findFirst().orElse(null);
						
						
					}
				}
				
			}
			
		}
		return null;
//		return witnessHisDao.findByStepnoAndWitnessflagAndNoticePoint(wokstep.getId(), status,noticePoint);
	}
	
	public int expireByWorkStepId(Integer id){
		return witnessHisDao.expireByWorkStepId(id);
	}
	
	public WorkStepWitnessHis add(WorkStepWitnessHis his){
		return this.witnessHisDao.save(his);
	}
	
	public WorkStepWitnessHis update(WorkStepWitnessHis his){
		return this.witnessHisDao.save(his);
	}
	
	public WorkStepWitnessHis add(WorkStepWitness wit){
		WorkStepWitnessHis n = new WorkStepWitnessHis();
		n.setStepno(wit.getWorkStep().getId());
		n.setWitnessmonitor(wit.getWitnessmonitor());
		n.setWitnessteam(wit.getWitnessteam());
		n.setWitness(wit.getWitness());
		n.setWitnesser(wit.getWitnesser());
		n.setStatus(wit.getStatus());
		n.setIsok(wit.getIsok());
		n.setNoticePoint(wit.getNoticePoint());
		n.setNoticeType(wit.getNoticeType());
		n.setWitnessflag(WitnessFlagEnum.CANCEL.name());
		return this.witnessHisDao.save(n);
	}
}
