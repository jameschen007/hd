package com.easycms.hd.plan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.dao.EnpowerMaterialInfoDao;
import com.easycms.hd.plan.domain.EnpowerMaterialInfo;
import com.easycms.hd.plan.service.EnpowerMaterialInfoService;
@Service("enpowerMaterialInfoService")
public class EnpowerMaterialInfoServiceImpl implements EnpowerMaterialInfoService{
	
	@Autowired
	private EnpowerMaterialInfoDao materialInfoDao;

	@Override
	public boolean update(EnpowerMaterialInfo rollingPlan) {
		return materialInfoDao.save(rollingPlan) != null;
	}

	/**
	 * 如果涉及到实际用量原本有值，这里修改时，先将原来的值存储一个值为删除状态。以备后查
	 */
	@Override
	public boolean updateActualDosage(EnpowerMaterialInfo material) {
		if(material==null){
			return false ;
		}
		
		/**
		 * 如果原有的记录中，实际用量存在值，并且最新的值与原来的值不相等时，做一个数据记录保存
		 */
		EnpowerMaterialInfo old = findById(material.getId());
		if(old!=null && old.getActualDosage()!=null && !"".equals(old.getActualDosage()) 
				&& !old.getActualDosage().equals(material.getActualDosage())
				&& material.getActualDosage()!=null && !"".equals(material.getActualDosage())){
			EnpowerMaterialInfo del = new EnpowerMaterialInfo();
			del.setId(old.getId());
			del.setRollingPlanId(old.getRollingPlanId());
			del.setEnpowerMaterialId(old.getEnpowerMaterialId());
			del.setEnpowerPlanId(old.getEnpowerPlanId());
			del.setMaterialName(old.getMaterialName());
			del.setMaterialIdentifier(old.getMaterialIdentifier());
			del.setSpecificationModel(old.getSpecificationModel());
			del.setPlanDosage(old.getPlanDosage());
			del.setActualDosage(old.getActualDosage());
			del.setRecorderId(old.getRecorderId());
			del.setRecorderName(old.getRecorderName());
			del.setRecorderTime(old.getRecorderTime());
			del.setStatus(old.getStatus());
			del.setUnit(old.getUnit());
			del.setIsDel(true);
			add(del);
		}
		if(material.getActualDosage()!=null && !"".equals(material.getActualDosage())){

			return materialInfoDao.save(material) != null;
		}
		return false ;
		
	}

	@Override
	public EnpowerMaterialInfo add(EnpowerMaterialInfo rollingPlan) {
		return materialInfoDao.save(rollingPlan);
	}

	@Override
	public EnpowerMaterialInfo findById(Integer id) {
		return materialInfoDao.findById(id);
	}
	@Override
	public List<EnpowerMaterialInfo> findByRollingPlanId(Integer id) {
		return materialInfoDao.findByRollingPlanId(id);
	}
}
