package com.easycms.hd.plan.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.underlying.WitingPlan;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.jpa.JpaMethod;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.jpa.QueryUtil_AndOr;
import com.easycms.core.util.Page;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.dao.RollingPlanDao;
import com.easycms.hd.plan.dao.WitnessDao;
import com.easycms.hd.plan.dao.WorkStepDao;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.RollingPlanFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.User;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;

@Service("rollingPlanService")
public class RollingPlanServiceImpl implements RollingPlanService {
	
	public static Logger logger = Logger.getLogger(RollingPlanServiceImpl.class);

	@Autowired
	private RollingPlanDao rollingPlanDao;
	@Autowired
	private WorkStepDao workStepDao;
	@Autowired
	private WitnessDao witnessDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

	@Override
	public boolean updateWitnessflagNull(RollingPlan rollingPlan) {
		
		List<WorkStep> steps = workStepDao.findForCancelById(rollingPlan.getId());
		if(steps==null || steps.size()==0){
			//不存在　不为空的　工序，则修改计划的状态
			rollingPlan.setWitnessflag(null);
			return rollingPlanDao.save(rollingPlan) != null;
		}
		return false;
		
	}

	@Override
	public RollingPlan add(RollingPlan rollingPlan) {
		return rollingPlanDao.save(rollingPlan);
	}

	@Override
	public RollingPlan findById(Integer id) {
		return rollingPlanDao.findById(id);
	}

	@Override
	public List<RollingPlan> findByIds(Integer[] ids) {
		return rollingPlanDao.findByIds(ids);
	}

	@Override
	public List<RollingPlan> findConsteamByRPIds(Integer[] ids, String consteam) {
		return rollingPlanDao.findConsteamByRPIds(ids, consteam);
	}

	@Override
	public List<RollingPlan> findConsendmanByRPIds(Integer[] ids,
			String consendman) {
		return rollingPlanDao.findConsendmanByRPIds(ids, consendman);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != rollingPlanDao.deleteById(id)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Integer[] ids) {
		if (0 != rollingPlanDao.deleteByIds(ids)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean assign(Integer[] ids, String consteam, Integer isend,
			Date assigndate, Date planfinishdate) {
		if (0 != rollingPlanDao.assign(ids, consteam, isend, assigndate,
				planfinishdate)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean assignEndMan(Integer[] ids, String consendman,
			String plandate, Integer isend) {
		if (0 != rollingPlanDao.assignEndMan(ids, consendman, plandate, isend)) {
			return true;
		}
		return false;
	}

	@Override
	public List<RollingPlan> findAll() {
		return rollingPlanDao.findAll();
	}

	@Override
	public List<RollingPlan> findByIDs(Integer[] ids) {
		return rollingPlanDao.findByIDs(ids);
	}

	@Override
	public List<RollingPlan> findAll(RollingPlan condition) {
		return rollingPlanDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public RollingPlan findByDrawnoAndWeldno(String drawno, String weldno) {
		return rollingPlanDao.findByDrawnoAndWeldno(drawno, weldno);
	}

	@Override
	public Page<RollingPlan> findPage(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findAll(null, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPage(Page<RollingPlan> page,
			final String condition, final String key, final String[] value) {

		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = null;

		if (condition.equals("notequal")) {
			if (key.equals("consteam")) {
				if (null == value) {
					springPage = rollingPlanDao.findByConsteamIsNull(pageable);
				} else {
					springPage = rollingPlanDao.findByConsteamNotIn(
							Arrays.asList(value), pageable);
				}
			} else if (key.equals("mytask")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNull(pageable);
				} else {
					springPage = rollingPlanDao
							.findByConsteamIsNotNullAndConsendmanInAndWelderIsNull(
									Arrays.asList(value), pageable);
				}
			} else if (key.equals("consendman")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNull(pageable);
				} else {
					springPage = rollingPlanDao
							.findByConsteamInAndConsendmanIsNull(
									Arrays.asList(value), pageable);
				}
			}
		} else if (condition.equals("equal")) {
			if (key.equals("consteam")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsteamIsNotNullAndConsendmanIsNull(pageable);
				} else {
					springPage = rollingPlanDao.findByConsteamIn(Arrays.asList(value), pageable);
				}
			} else if (key.equals("mytask")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNotNull(pageable);
				} else {
					springPage = rollingPlanDao
							.findByConsendmanInAndWelderIsNotNull(
									Arrays.asList(value), pageable);
				}
			} else if (key.equals("consendman")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNotNull(pageable);
				} else {
					springPage = rollingPlanDao
							.findByConsteamInAndConsendmanIsNotNull(
									Arrays.asList(value), pageable);
				}
			}
		}

		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findPageByKeyWords(Page<RollingPlan> page, String condition,
			String key, String[] value, String keyword) {
		
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = null;
		
		if (condition.equals("notequal")) {
			if (key.equals("consteam")) {
				if (null == value) {
					springPage = rollingPlanDao.findByConsteamIsNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao.findByConsteamNotIn(pageable, 
							Arrays.asList(value), keyword);
				}
			} else if (key.equals("mytask")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao
							.findByConsteamIsNotNullAndConsendmanInAndWelderIsNull(
									Arrays.asList(value), keyword, pageable);
				}
			} else if (key.equals("consendman")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao
							.findByConsteamInAndConsendmanIsNull(
									Arrays.asList(value), keyword, pageable);
				}
			}
		} else if (condition.equals("equal")) {
			if (key.equals("consteam")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsteamIsNotNullAndConsendmanIsNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao.findByConsteamIn(Arrays.asList(value), keyword, pageable);
				}
			} else if (key.equals("mytask")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNotNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao
							.findByConsendmanInAndWelderIsNotNull(
									Arrays.asList(value), keyword, pageable);
				}
			} else if (key.equals("consendman")) {
				if (null == value) {
					springPage = rollingPlanDao
							.findByConsendmanIsNotNull(pageable, keyword);
				} else {
					springPage = rollingPlanDao
							.findByConsteamInAndConsendmanIsNotNull(
									Arrays.asList(value), keyword, pageable);
				}
			}
		}
		
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPage(RollingPlan condition,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findAll(QueryUtil.queryConditions(condition), pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findPage(RollingPlan condition,RollingPlan andOr,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,page.getPagesize());
		Map<String,String> cmethod = new HashMap<String,String>();
		cmethod.put(JpaMethod.beanId, "id");
		
		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findAll(QueryUtil_AndOr.queryConditions(condition,andOr,cmethod), pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<RollingPlan> findByConsteams(String[] ids) {
		return rollingPlanDao.findByConsteams(ids);
	}

	@Override
	public List<RollingPlan> findByConsteamsAndIsend(String[] ids, Integer isend) {
		return rollingPlanDao.findByConsteamsAndIsend(ids, isend);
	}

	@Override
	public List<RollingPlan> findByConsteamIsNull() {
		return rollingPlanDao.findByConsteamIsNull();
	}
	
	@Override
	public Page<RollingPlan> findByConsteamIsNull(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamIsNull(pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<RollingPlan> findByConsteamIsNotNull() {
		return rollingPlanDao.findByConsteamIsNotNull();
	}
	
	@Override
	public Page<RollingPlan> findByConsteamIsNotNull(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamIsNotNull(pageable);
		
		System.out.println("findByConsteamIsNotNull : " + springPage.getTotalElements());
		
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findByConsteamInAndConsendmanIsNull(
			List<String> ids, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamInAndConsendmanIsNull(ids, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<RollingPlan> findByConsteamInAndConsendmanIsNull(
			List<String> ids) {
		return rollingPlanDao.findByConsteamInAndConsendmanIsNull(ids);
	}

	@Override
	public List<RollingPlan> findByConsendmanInAndIsend(List<String> ids,
			Integer isend) {
		return rollingPlanDao.findByConsendmanInAndIsend(ids, isend);
	}
	
	@Override
	public Page<RollingPlan> findByConsendmanInAndIsend(List<String> ids,
			Integer isend, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsendmanInAndIsend(ids, isend, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findByConsendmanInAndIsendNot(List<String> ids,
			Integer isend, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsendmanInAndIsendNot(ids, isend, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<RollingPlan> findByConsendmanInAndIsendNot(List<String> ids,
			Integer isend) {
		return rollingPlanDao.findByConsendmanInAndIsendNot(ids, isend);
	}

	@Override
	public List<RollingPlan> findByConsteamInAndConsendmanIsNotNull(
			List<String> ids) {
		return rollingPlanDao.findByConsteamInAndConsendmanIsNotNull(ids);
	}
	
	@Override
	public Page<RollingPlan> findByConsteamInAndConsendmanIsNotNull(
			List<String> ids, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamInAndConsendmanIsNotNull(ids, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull() {
		return rollingPlanDao.findByConsteamIsNotNullAndConsendmanIsNotNull();
	}
	
	@Override
	public Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNotNull(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamIsNotNullAndConsendmanIsNotNull(pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull(Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsteamIsNotNullAndConsendmanIsNull(pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public List<RollingPlan> findByConsteamIsNotNullAndConsendmanIsNull() {
		return rollingPlanDao.findByConsteamIsNotNullAndConsendmanIsNull();
	}

	@Override
	public List<RollingPlan> findByConsendmanIn(List<String> consendman) {
		return rollingPlanDao.findByConsendmanIn(consendman);
	}

	@Override
	public List<RollingPlan> findByConsendmanIsNull() {
		return rollingPlanDao.findByConsendmanIsNull();
	}

	@Override
	public List<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend) {
		return rollingPlanDao.findByConsendmanIsNotNullAndIsend(isend);
	}
	
	@Override
	public Page<RollingPlan> findByConsendmanIsNotNullAndIsend(Integer isend, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsendmanIsNotNullAndIsend(isend, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend) {
		return rollingPlanDao.findByConsendmanIsNotNullAndIsendNot(isend);
	}
	
	@Override
	public Page<RollingPlan> findByConsendmanIsNotNullAndIsendNot(Integer isend, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByConsendmanIsNotNullAndIsendNot(isend, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<RollingPlan> findByConsEndman(String[] ids) {
		return rollingPlanDao.findByConsEndman(ids);
	}
//------------------------------------------------------[2017-09-11]---------------------------------------------------------------
	@Override
	public Page<RollingPlan> findByPageWithType(String type, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByType(type, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findByPageMonitorAssigned(String type, List<Integer> monitorId, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageMonitorAssigned(type, monitorId, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findByPageMonitorUnAssign(String type, List<Integer> monitorId, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageMonitorUnAssign(type, monitorId, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public boolean assignTeam(Integer[] ids, Integer consteam, Integer isend,
			Date consteamdate, Date planfinishdate, Integer operater) throws Exception {
		try{
			
			if (0 != rollingPlanDao.assignTeam(ids, consteam, isend, consteamdate,
					planfinishdate, operater)) {
				for(Integer id:ids){
					executePushToEnpowerServiceImpl.updateEnpowerStatus(null, id,"");
				}
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e ;
		}

		return false;
	}

	@Override
	public Page<RollingPlan> findTeamCompletedByPageWithType(List<Integer> teamIds, String type,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamCompletedByPageWithType(type, teamIds, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamProgressingByPageWithType(List<Integer> teamIds, String type,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamProgressingByPageWithType(type, teamIds, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamUnProgressByPageWithType(List<Integer> teamIds, String type,
			Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamUnProgressByPageWithType(type, teamIds, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamPausedByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamPausedByPageWithType(type, teamIds, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	
	@Override
	public Page<RollingPlan> findTeamUnCompleteByPageWithType(List<Integer> teamIds, String type, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamUnCompleteByPageWithType(type, teamIds, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public boolean backFill(Integer id, Integer welder, Date welddate) {
		if (0 != rollingPlanDao.backFill(id, welder, welddate)) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean backFillV2(Integer id, String welder, Date welddate) {
		if (0 != rollingPlanDao.backFillV2(id, welder, welddate)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean reAssignTeam(Integer[] ids, Integer userId, Date planfinishdate, Integer operater) {
		//如果当前作业组长有问题没有被解决，　不允许改派
		//ids　实际只会长度为１

		List<RollingPlan> list = rollingPlanDao.findByIds(ids);
		boolean f = list.stream().anyMatch(bn->RollingPlanFlag.PROBLEM.toString().equals(bn.getRollingplanflag()));
		if(f){
			return false ;
		}
		
		
		if (0 != rollingPlanDao.reAssignTeam(ids, userId, planfinishdate, operater)) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean reAssignTeam(Integer[] ids, Integer userId, Integer operater) {
		//如果当前作业组长有问题没有被解决，　不允许改派
		List<RollingPlan> list = rollingPlanDao.findByIds(ids);
		boolean f = list.stream().anyMatch(bn->RollingPlanFlag.PROBLEM.toString().equals(bn.getRollingplanflag()));
		if(f){
			return false ;
		}
		
		if (0 != rollingPlanDao.reAssignTeam(ids, userId, operater)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean releaseTeam(Integer[] ids, Integer operater) {
		if (0 != rollingPlanDao.releaseTeam(ids, operater)) {
			return true;
		}
		return false;
	}

	@Override
	public Page<RollingPlan> findByPageConsmonitor(String type, List<Integer> monitorId, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageConsmonitor(type, monitorId, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<RollingPlan> findByPageConsmonitorWithKeyword(String type, List<Integer> monitorId, String keyword, Page<RollingPlan> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageConsmonitorWithKeyword(type, monitorId,keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public boolean deleteRollingPlan(Integer[] ids) {
		return rollingPlanDao.deleteRollingPlan(ids) > 0;
	}
//-------------------------------------------------[2017-10-30为全局关键词搜索而添加的方法]-----------------------------------------------------------
	@Override
	public Page<RollingPlan> findByPageMonitorAssignedWithKeyword(String type, List<Integer> monitorId, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageMonitorAssignedWithKeyword(type, monitorId, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findByPageMonitorUnAssignWithKeyword(String type, List<Integer> monitorIds, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findByPageMonitorUnAssignWithKeyword(type, monitorIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamCompletedByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamCompletedByPageWithTypeAndKeyword(type, teamIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamProgressingByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamProgressingByPageWithTypeAndKeyword(type, teamIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamUnProgressByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamProgressingByPageWithTypeAndKeyword(type, teamIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamPausedByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());

		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamPausedByPageWithTypeAndKeyword(type, teamIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<RollingPlan> findTeamUnCompleteByPageWithTypeAndKeyword(List<Integer> teamIds, String type, String keyword,
			Page<RollingPlan> page) {
		if (CommonUtility.isNonEmpty(keyword)) {
			keyword = "%" + keyword + "%";
		}
		Pageable pageable = new PageRequest(page.getPageNum() - 1,
				page.getPagesize());
		
		org.springframework.data.domain.Page<RollingPlan> springPage = rollingPlanDao
				.findTeamUnCompleteByPageWithTypeAndKeyword(type, teamIds, keyword, pageable);
		page.execute(
				Integer.valueOf(Long.toString(springPage.getTotalElements())),
				page.getPageNum(), springPage.getContent());
		return page;
	}
	/**
	 * 获取滚计划的班长
	 * 首先取对象上面的值，
	 * 
	 * 首先按滚动计划查
	 * 其次按工序查，
	 * 最后使用见证查
	 * @return
	 */
	@Override
	public User getConsmoniterBy(RollingPlan plan,WorkStep workStep,WorkStepWitness witness,Integer rollingPlanId,Integer workStepId,Integer witnessId){
		Integer userId = null;
		try{
			if(plan!=null){
				userId = plan.getConsmonitor();
			}else if(workStep!=null){
				userId = workStep.getRollingPlan().getConsmonitor();
			}else if(witness!=null){
				userId = witness.getWorkStep().getRollingPlan().getConsmonitor();
			}else if(rollingPlanId!=null){
				userId = rollingPlanDao.findById(rollingPlanId).getConsmonitor();
			}else if(workStepId!=null){
				userId = workStepDao.findById(workStepId).getRollingPlan().getConsmonitor();
			}else if(witnessId!=null){
				userId = witnessDao.findById(witnessId).getWorkStep().getRollingPlan().getConsmonitor();
			}else{
				logger.error("获取滚动计划的班长参数异常：rollingPlanId="+rollingPlanId+" workStepId="+workStepId+" witnessId="+witnessId);
				throw new RuntimeException("获取滚动计划的班长参数异常");
			}
		}catch(Exception e){
			logger.error("获取滚动计划的班长出错：rollingPlanId="+rollingPlanId+" workStepId="+workStepId+" witnessId="+witnessId);
			e.printStackTrace();
			throw e;
		}
		return userDao.findById(userId);
	}
	/**
	 * 获取滚计划的班长id
	 * 首先取对象上面的值，
	 * 
	 * 首先按滚动计划查
	 * 其次按工序查，
	 * 最后使用见证查
	 * @return
	 */
	@Override
	public Integer getConsmoniterIdBy(RollingPlan plan,WorkStep workStep,WorkStepWitness witness,Integer rollingPlanId,Integer workStepId,Integer witnessId){
		Integer userId = null;
		try{
			if(plan!=null){
				userId = plan.getConsmonitor();
			}else if(workStep!=null){
				userId = workStep.getRollingPlan().getConsmonitor();
			}else if(witness!=null){
				userId = witness.getWorkStep().getRollingPlan().getConsmonitor();
			}else if(rollingPlanId!=null){
				userId = rollingPlanDao.findById(rollingPlanId).getConsmonitor();
			}else if(workStepId!=null){
				userId = workStepDao.findById(workStepId).getRollingPlan().getConsmonitor();
			}else if(witnessId!=null){
				userId = witnessDao.findById(witnessId).getWorkStep().getRollingPlan().getConsmonitor();
			}else{
				logger.error("获取滚动计划的班长参数异常：rollingPlanId="+rollingPlanId+" workStepId="+workStepId+" witnessId="+witnessId);
				throw new RuntimeException("获取滚动计划的班长参数异常");
			}
		}catch(Exception e){
			logger.error("获取滚动计划的班长出错：rollingPlanId="+rollingPlanId+" workStepId="+workStepId+" witnessId="+witnessId);
			e.printStackTrace();
			throw e;
		}
		return userId;
	}

	@Override
	public List<RollingPlan> findByWorkListId(String workListId){
		return rollingPlanDao.findByWorkListId(workListId);
	}

	@Override
	public List<RollingPlan> findByWorkListIdAndWeldno(String workListId,String weldno){
		return rollingPlanDao.findByWorkListIdAndWeldno(workListId,weldno);
	}
	

	public boolean update(RollingPlan rollingPlan) {
		return rollingPlanDao.save(rollingPlan) != null;
	}


	/**
	 * 根据其中的工序id查询出滚动计划
	 * @param stepForms
	 * @return
	 */
	public List<WitingPlan> findByWorkSteps(Set<WorkStepWitnessItemsForm> stepForms) {
		List<Integer> idList = new ArrayList<>();
		Map<Integer, WorkStepWitnessItemsForm> map = new TreeMap<>();

		for (WorkStepWitnessItemsForm oneform : stepForms) {
			String ids = oneform.getIds() ;
			if(CommonUtility.isNonEmpty(ids) && ids.matches("\\d+[[,]*\\d*[,]*]*")){
				String[] strarr = ids.split(",");
				for(String str:strarr){
					if (StringUtils.isEmpty(str) == false) {
						Integer id  = Integer.valueOf(str);
						oneform.setId(id);
						map.put(id, oneform);
						idList.add(id);
					}
				}
			}else if(oneform.getId()!=null){//兼容原来的逻辑
				Integer id = oneform.getId();
				oneform.setId(id);
				map.put(id, oneform);
				idList.add(id);
			}
		}
		Specification<RollingPlan> querySpeci = new Specification<RollingPlan>() {
			@Override
			public Predicate toPredicate(Root<RollingPlan> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				Path<RollingPlan> path = root.get("id");
				Subquery<RollingPlan> query2a = query.subquery(RollingPlan.class);
				Root<WorkStep> root2a = (Root<WorkStep>) query2a.from(WorkStep.class);
				query2a.select(root2a.<RollingPlan>get("rollingPlan"));
				Predicate predicates = builder.conjunction();
				
				CriteriaBuilder.In<Integer> idIn = builder.in(root2a.get("id").as(Integer.class));
				idList.stream().forEach(i->idIn.value(i));
				query2a.where(idIn);
				predicates = builder.and(predicates,builder.in(path).value(query2a));
				return query.where(predicates).getRestriction();
			}
		};

		List<RollingPlan> planList = rollingPlanDao.findAll(querySpeci);

		List<WitingPlan> ret = new ArrayList<>();

		planList.stream().forEach(b->{
			WitingPlan p = new WitingPlan();
			p.setPlan(b);
			p.setPlanId(b.getId());

			//设置原始参数集合
			Set<WorkStepWitnessItemsForm> oldStep = new HashSet<>();

			b.getWorkSteps().forEach(x -> {
				if (map.get(x.getId()) != null) {
					oldStep.add(map.get(x.getId()));
				}
			});

			p.setStep(oldStep);

			ret.add(p);
		});


		return ret;
	}
}
