package com.easycms.hd.plan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.plan.dao.EnpowerEquipmentDao;
import com.easycms.hd.plan.domain.EnpowerEquipment;
import com.easycms.hd.plan.service.EnpowerEquipmentService;
@Service("enpowerEquipmentService")
public class EnpowerEquipmentServiceImpl implements EnpowerEquipmentService{
	
	@Autowired
	private EnpowerEquipmentDao materialInfoDao;

	@Override
	public boolean update(EnpowerEquipment rollingPlan) {
		return materialInfoDao.save(rollingPlan) != null;
	}

	@Override
	public EnpowerEquipment add(EnpowerEquipment rollingPlan) {
		return materialInfoDao.save(rollingPlan);
	}

	@Override
	public EnpowerEquipment findById(Integer id) {
		return materialInfoDao.findById(id);
	}
	@Override
	public EnpowerEquipment findByRollingPlanId(Integer id) {
		return materialInfoDao.findByRollingPlanId(id);
	}
}
