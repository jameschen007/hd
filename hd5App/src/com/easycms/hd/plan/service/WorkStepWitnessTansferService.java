package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.form.WorkStepWitnessFake;

public interface WorkStepWitnessTansferService {
	public List<WorkStepWitness> transferWorkStepWitness(List<WorkStepWitness> workStepWitnesses);
	public WorkStepWitness transferWorkStepWitness(WorkStepWitness workStepWitness);
	List<WorkStepWitness> transferChildren(List<WorkStepWitness> wswList);
	List<WorkStepWitnessFake> toFake(List<WorkStepWitness> wswList);
	List<WorkStepWitness> transferWitness(List<WorkStepWitness> datas);
	WorkStepWitness transferWitness(WorkStepWitness workStepWitness);
}
