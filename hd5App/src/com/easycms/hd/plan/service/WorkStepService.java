package com.easycms.hd.plan.service;

import java.util.List;
import java.util.Map;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;

public interface WorkStepService{
	
	List<WorkStep> findAll();
	
	List<WorkStep> findAll(WorkStep condition);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
	
	Page<WorkStep> findPage(Page<WorkStep> page);
	
	Page<WorkStep> findPageByRollingPlanId(Page<WorkStep> page, Integer rollingPlanId);
	
	Page<WorkStep> findPage(WorkStep condition,Page<WorkStep> page);
	/**
	 * 根据ID查找
	 * @param id
	 * @return
	 */
	WorkStep findById(Integer id);
	List<WorkStep> findByIds(Integer[] ids);
	
	WorkStep getById(Integer id);
	
	List<WorkStep> findByRollingplanIds(Integer[] ids);
	
	boolean delete(Integer id);
	
	boolean delete(Integer[] ids);
	
	boolean deleteByIDUP(Integer[] ids);
	
	
	/**
	 * 添加工序步骤
	 * @param workStep
	 * @return
	 */
	WorkStep add(WorkStep workStep);
	//组长完成+没完成的工序
	Page<WorkStep> findByPageTeamAll(String type, Integer teamId, Page<WorkStep> page);
	//组长未完成的见证工序
	Page<WorkStep> findByPageTeamUnCompleted(String type, Integer teamId, Page<WorkStep> page);
	//组长未完成的见证工序 不判断计划状态
	Page<WorkStep> findByPageTeamUnCompletedNotPlan(String type, Integer teamId, Page<WorkStep> page);

	//组长完成的见证工序
	Page<WorkStep> findByPageTeamComplete(String type, Integer teamId, Page<WorkStep> page);
	//组长完成的见证工序：查询这条工序上的所有见证点都完成了
	Page<WorkStep> findByPageTeamCompleteNotPlanComplete(String type, Integer teamId, Page<WorkStep> page);
	//组长未完成的见证工序 -- 带搜索
	Page<WorkStep> findByPageTeamUnCompletedWithKeyword(String type, Integer teamId, String keyword,
			Page<WorkStep> page);
	//组长完成的见证工序 -- 带搜索
	Page<WorkStep> findByPageTeamCompleteWithKeyword(String type, Integer teamId, String keyword, Page<WorkStep> page);
	

	//根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	/**
	 * 
	 * @param map [planIds 计划数组]
	 * @return
	 */
	public List<WorkStepForBatchWitness> getWorkStep(Map<String,Object> map);
	/**
	 * 全部未完成的见证，用于修复应该完成却显示未完成的　都消点了，但管焊队队长点进去看，还在未完成界面
	 */
	public List<WorkStep> findByPageUnCompleted();
}
