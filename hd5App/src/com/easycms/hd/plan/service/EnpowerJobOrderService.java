package com.easycms.hd.plan.service;

import java.util.List;

import com.easycms.hd.plan.domain.EnpowerJobOrder;

public interface EnpowerJobOrderService {

	public boolean update(EnpowerJobOrder rollingPlan) ;

	public EnpowerJobOrder add(EnpowerJobOrder rollingPlan) ;


	public EnpowerJobOrder findById(Integer id) ;

	public List<EnpowerJobOrder> findByRollingPlanId(Integer id);
 
}
