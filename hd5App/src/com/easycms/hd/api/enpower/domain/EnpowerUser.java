package com.easycms.hd.api.enpower.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "enpower_user")
@Data
public class EnpowerUser {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "data")
	private String data;
	@Column(name = "createdate")
	private Date createdate;
	@Column(name = "status")
	private String status;
	@Column(name = "description")
	private String description;
}
