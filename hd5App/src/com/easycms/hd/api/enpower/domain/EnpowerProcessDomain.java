package com.easycms.hd.api.enpower.domain;

import java.util.List;

import com.easycms.hd.api.enpower.response.EnpowerResponseDepartment;
import com.easycms.hd.api.enpower.response.EnpowerResponseUser;

import lombok.Data;
@Data
public class EnpowerProcessDomain {
	private List<EnpowerResponseUser> enpowerResponseUsers;
	private List<EnpowerResponseDepartment> enpowerResponseDepartments;
}
