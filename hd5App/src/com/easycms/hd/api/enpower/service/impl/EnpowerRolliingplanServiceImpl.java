package com.easycms.hd.api.enpower.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.enpower.dao.EnpowerRollingplanDao;
import com.easycms.hd.api.enpower.domain.EnpowerRollingplan;
import com.easycms.hd.api.enpower.service.EnpowerRollingplanService;

@Service("enpowerRollingplanService")
public class EnpowerRolliingplanServiceImpl implements EnpowerRollingplanService {
	
	@Autowired
	private EnpowerRollingplanDao enpowerRollingplanDao;

	@Override
	public boolean insert(EnpowerRollingplan enpowerRollingplan) {
		return null != enpowerRollingplanDao.save(enpowerRollingplan);
	}
	
}
