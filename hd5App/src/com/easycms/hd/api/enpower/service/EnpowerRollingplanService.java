package com.easycms.hd.api.enpower.service;

import com.easycms.hd.api.enpower.domain.EnpowerRollingplan;

public interface EnpowerRollingplanService {
	boolean insert(EnpowerRollingplan enpowerRollingplan);
}
