package com.easycms.hd.api.enpower.service;

import java.util.List;
import java.util.Map;

import com.easycms.hd.api.enpower.domain.EnpowerUser;
import com.easycms.hd.api.enpower.response.EnpowerResponseDepartment;
import com.easycms.hd.api.enpower.response.EnpowerResponseUser;

public interface EnpowerUserService {
	/**
	 * 插入enpower的访问请求数据
	 * @param enpowerUser
	 * @return
	 */
	boolean insert(EnpowerUser enpowerUser);
	/**
	 * 批量插入部门和用户
	 * @param enpowerResponseDepartments
	 * @param enpowerResponseUsers
	 * @return
	 */
	boolean insertBatchDepartmentAndUser(List<EnpowerResponseDepartment> enpowerResponseDepartments, List<EnpowerResponseUser> enpowerResponseUsers);
	/**
	 * 转译enpower请求数据到本地对象
	 * @param dataList
	 * @return
	 */
	List<EnpowerResponseUser> transferResponseUser(List<Map<String, Object>> dataList);
	/**
	 * 验证用户
	 * @param enpowerResponseUsers
	 * @return
	 */
	boolean validataEnpowerResponseUser(List<EnpowerResponseUser> enpowerResponseUsers);
	/**
	 * 验证部门
	 * @param enpowerResponseDepartments
	 * @return
	 */
	boolean validataEnpowerResponseDepartment(List<EnpowerResponseDepartment> enpowerResponseDepartments);
	
	/**
	 * 菜单重建
	 */
	void buildEnpowerRoleMenu();
}
