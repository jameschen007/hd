package com.easycms.hd.api.enpower.response;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
/**
 * APP拿到的Enpower中的数据
 * @author ul-webdev
 */
@Data
public class EnpowerResponseHseCode {
	@SerializedName(value = "ID")
	private String id;
	@SerializedName(value = "PARENT_ID")
	private String parentId;
	@SerializedName(value = "CODE")
	private String code;
	@SerializedName(value = "CODE_DESC")
	private String codeDesc;
	@SerializedName(value = "STAND")
	private String stand ;
	@SerializedName(value = "PROJ_CODE")
	private String projCode = EnpConstant.PROJ_CODE;
	@SerializedName(value = "COMP_CODE")
	private String compCode = EnpConstant.COMP_CODE;
}
