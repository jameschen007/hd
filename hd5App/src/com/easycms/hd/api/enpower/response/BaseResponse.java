package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class BaseResponse implements Serializable {
	private static final long serialVersionUID = -2622687166750923385L;
	private Boolean IsSuccess;
    private String ErrorMsg;
    private String DataInfo;
}
