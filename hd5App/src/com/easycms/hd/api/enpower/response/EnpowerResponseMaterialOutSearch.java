package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EnpowerResponseMaterialOutSearch implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	// 出库ID
	private String ID;
	// 入库ID
	private String INSTK_ID;
	// 库存ID
	private String STK_ID;
	// 物项编码
	private String MATE_CODE;
	// 物项名称
	private String MATE_DESCR;
	// 出库单号
	private String ISSNO;
	// 出库类型
	private String ISSTYPE;
	// 开票日期
	private String ISSDATE;
	// 出库单位
	@SerializedName(value = "ISS_DEPT")
	private String Iss_Dept;
	// 领料员
	private String WHO_GET;
	// 出库数量
	private String ISSQTY;
	// 核实数量
	private String QTY_RELEASED;
	// 单价
	private String PRICE;
	// 出库金额
	private String CK_JE;
	// 核实金额
	private String CK_JE_HS;
	// 保管员
	private String KEEPER;
	// 核实日期
	private String WHEN_CNFMED;
	// 需求计划号
	@SerializedName(value = "MRP_NO")
	private String mrp_no;
	// 质保号
	private String GUARANTEENO;
	// 会计科目
	@SerializedName(value = "CST_CODE")
	private String Cst_Code;
	// 规格型号
	@SerializedName(value = "SPEC")
	private String Spec;
	// 材质
	@SerializedName(value = "TEXTURE")
	private String Texture;
	// 标准
	@SerializedName(value = "STAND_NO")
	private String Stand_No;
	// 核安全等级
	@SerializedName(value = "NS_CLASS")
	private String ns_class;
	// 质保等级
	@SerializedName(value = "QA_CLASS")
	private String qa_class;
	// 位号
	private String FUNC_NO;
	// 炉批号
	private String LOTNO;
	// 批号
	private String HEAT_NO;
	// 备注
	private String ISSUSEREMARK;
	/** 单位 **/
	private String UNIT;
	/** WAREH_PLACE 货位号 **/
	private String WAREH_PLACE;
	private String TOTAL_COUNT;
	private String ROW_NUM;

}
