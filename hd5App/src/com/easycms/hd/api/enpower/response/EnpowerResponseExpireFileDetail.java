package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;
/**
 * 
 * @author ul-webdev
 *
 */
@Data
public class EnpowerResponseExpireFileDetail  implements Serializable {
	
	private static final long serialVersionUID = 7068268681475989131L;
	/** 文件失效通知单编号 */
	private String CANC_CODE;// 1514
	/** 内部文件编号 */
	private String INTERCODE;// 07122A02BZS02-FM
	/** 中文名称 */
	private String C_TITLE;// 2A02区管道支吊架组装图(一)封面
	/** 版本 */
	private String REVSION;// A
	/** 状态 */
	private String STATUS;// INV
	/** 文件使用人 */
	private String BORROWER;// null
	/** 份数 */
	private String BOR_NUM;// null
	/** 发布时间 */
	private String SIGN_DATE;// 42970
	/** 编制人 */
	private String DRAFTER;// 王琳琳
	/** 编制日期 */
	private String DRAFT_DATE;// 2017-07-15 19
	/** 审批人 */
	private String SIGNER;// 陈官喜
	/** 项目代号 */
	private String PROJ_CODE;// K2K3
	/** 公司代码 */
	private String COMP_CODE;// 50812
	/** 文件借阅人 */
	private String Loginname;// 孙明光
	/** 借阅人所属分发组 */
	private String ORG_NAME;// 管焊队
	
	

	
	
}
