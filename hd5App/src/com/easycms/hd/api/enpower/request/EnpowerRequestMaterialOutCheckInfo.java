package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.wordnik.swagger.annotations.ApiModel;

import lombok.Data;

/***
 * 修改物项出库信息
 * 
 * @author ul-webdev
 *
 */
@Data
@ApiModel(value = "enpowerRequestMaterialOutCheckInfo")
public class EnpowerRequestMaterialOutCheckInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	/** id*/
	private String ID;
	/** 领料人 */
	private String WHO_GET;
	/** 核实量 */
	private String QTY_RELEASED;
	/** 备注 */
	private String ISSUSEREMARK;
	/** 保管员 */
	private String KEEPER;
	/** 核实日期 */
	private String WHEN_CNFMED;
	/** 状态 */
	private String STATUS;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
