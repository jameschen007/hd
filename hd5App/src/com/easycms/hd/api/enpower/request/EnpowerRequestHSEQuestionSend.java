package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 * 上报安全整改问题接口	需要接收的数据bean		
推送接口名: 上报安全整改问题接口
 */
@Data
public class EnpowerRequestHSEQuestionSend {
	
	/**状态值（必填项）（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'整改合格' ,'5' -'整改不合格' ） */
	private String FLAG ;
	private String TROU_TYPE ;
	private String JBMS ;
	private String JBMT ;
	/**整改单编号（必填项,唯一性）  */
	private String TROU_CODE ;
	/**机组  */
	private String UNIT ;
	/**厂房  */
	private String BUILDING ;
	/**标高  */
	private String LEVELS ;
	/**房间号 */
	private String ROOM ;
	/**问题描述    */
	private String BAKE  ;
	/**责任部门    */
	private String CHECKED_COM ;
	/**责任班组    */
	private String DUTY_COMP ;
	/**问题照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）    */
	private String ZGQ_PHOTO ;
	/**APP主键   */
	private String APPID ;
	/**上报人  */
	private String FOUND_MAN ;
	/**上报时间  */
	private String FOUND_DATE ;
	


   /**整改情况回复  */
   private String RE_ZG ;

   /**完成日期  */
   private String END_DATE ;

   /**验收日期  */
   private String RQ_YZ ;
   
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb.append("{");
		
		//
		sb.append("'FLAG':'");
		if(FLAG!=null)
		sb.append(FLAG);
		sb.append("',");
//整改单编号（
		sb.append("'TROU_CODE':'");
		if(TROU_CODE!=null)
		sb.append(TROU_CODE);
		sb.append("',");
//机组  
		sb.append("'UNIT':'");
		if(UNIT!=null)
		sb.append(UNIT);
		sb.append("',");
//厂房  
		sb.append("'BUILDING':'");
		if(BUILDING!=null)
		sb.append(BUILDING);
		sb.append("',");
//标高  
		sb.append("'LEVELS':'");
		if(LEVELS!=null)
		sb.append(LEVELS);
		sb.append("',");
//房间号 
		sb.append("'ROOM':'");
		if(ROOM!=null)
		sb.append(ROOM);
		sb.append("',");
//问题描述    
		sb.append("'BAKE ':'");
		if(BAKE!=null)
		sb.append(BAKE );
		sb.append("',");
//责任部门    
		sb.append("'CHECKED_COM':'");
		if(CHECKED_COM!=null)
		sb.append(CHECKED_COM);
		sb.append("',");
//责任班组    
		sb.append("'DUTY_COMP':'");
		if(DUTY_COMP!=null)
		sb.append(DUTY_COMP);
		sb.append("',");
//问题照片（Or
		sb.append("'ZGQ_PHOTO':'");
		if(ZGQ_PHOTO!=null)
		sb.append(ZGQ_PHOTO);
		sb.append("',");
//APP主键   
		sb.append("'APPID':'");
		if(APPID!=null)
		sb.append(APPID);
		sb.append("',");
//上报人  
		sb.append("'FOUND_MAN':'");
		if(FOUND_MAN!=null)
		sb.append(FOUND_MAN);
		sb.append("',");
//上报时间  
		sb.append("'FOUND_DATE':'");
		if(FOUND_DATE!=null)
		sb.append(FOUND_DATE);
		sb.append("',");
//项目代号（默
		sb.append("'PROJ_CODE':'");
		if(PROJ_CODE!=null)
		sb.append(PROJ_CODE);
		sb.append("',");
//项目代码（K
		sb.append("'COMP_CODE':'");
		if(COMP_CODE!=null)
		sb.append(COMP_CODE);
		sb.append("'");
		
		
		//TROU_TYPE
		sb.append(",");
		sb.append("'TROU_TYPE':'");
		if(TROU_TYPE!=null)
		sb.append(TROU_TYPE);
		sb.append("'");
		
		//
		sb.append(",");
		sb.append("'JBMS':'");
		if(JBMS!=null)
		sb.append(JBMS);
		sb.append("'");
		
		//
		sb.append(",");
		sb.append("'JBMT':'");
		if(JBMT!=null)
		sb.append(JBMT);
		sb.append("'");
		

		//整改情况回复
		sb.append(",");
		sb.append("'RE_ZG':'");
		if(RE_ZG!=null){
			sb.append(RE_ZG);
		}
		sb.append("'");
		 
		//完成日期
		sb.append(",");
		sb.append("'END_DATE':'");
		if(END_DATE!=null){
			sb.append(END_DATE);
		}
		sb.append("'");
		
		

		//验收日期
		sb.append(",");
		sb.append("'RQ_YZ':'");
		if(RQ_YZ!=null){
			sb.append(RQ_YZ);
		}
		sb.append("'");
		
		sb.append("}");
		
		
		return sb.toString();
	}

	public static void main(String[] args) {
		EnpowerRequestHSEQuestionSend t = new EnpowerRequestHSEQuestionSend();
		System.out.println(t.toString());
	}
}
