package com.easycms.hd.api.enpower.request;

import java.io.Serializable;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EnpowerRequestExpireNoticeFilesForm implements Serializable{
	private static final long serialVersionUID = 6752578720698910183L;
	@ApiParam(value = "编号", required = false)
	private String no;
	@ApiParam(value = "数量", required = false)
	private Integer number;
	@ApiParam(value = "版本号", required = false)
	private String version;
	@ApiParam(value = "领用人", required = false)
	private String receiver;
	@ApiParam(value = "状态", required = false)
	private String status;
	@ApiParam(value = "文件名", required = false)
	private String filename;
}
