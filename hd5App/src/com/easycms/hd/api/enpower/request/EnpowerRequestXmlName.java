package com.easycms.hd.api.enpower.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.MyURLEncoder;
import com.easycms.common.util.CommonUtility;

import lombok.Data;

@Data
public class EnpowerRequestXmlName {
	public static Logger logger = Logger.getLogger(EnpowerRequestXmlName.class);
	private String xml = ""; // 用户信息查询
	private String date_col ="";
	private String photo_col ="";
	private String _value = null; 
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(1024);
		sb.append("_value={\"xml\": \"");//_value:
		sb.append(MyURLEncoder.encode(xml));
		sb.append("\"");
		sb.append(",\"date_col\":\"");
		if(date_col!=null){
			sb.append(date_col);
		}
		sb.append("\"");//Enpower后来有添加一个这个。
//为了解决传送二进制图片问题，凡是调用update函数的接口，都必须需要按照说明或示例头部增加   "photo_col":""  后面引号中写入对应照片字段名。
//		if(photo_col!=null && !"".equals(photo_col.trim())){
			sb.append(",\"photo_col\":\"");
			if(photo_col!=null){
				sb.append(photo_col);
			}
			sb.append("\"");//Enpower后来有添加一个这个。
//		}
		
		sb.append(",\"_value\":\"");
		if(_value!=null){
			sb.append(MyURLEncoder.encode(_value.replace("\"","\'")));
			/*Enpower的格式：
			 * { "xml": "插入设备清单", "_value":" [{'TZD_ID':
' b36692fd93ee4ebd9141db78f8a33385',
'EQUI_CODE':'A32BSDE3',
'DRAW_NO':'423432333-001',
'S_ID':'ab23d1243afd3353w23r43rwtr4t3ttw333a3',
'FOUND_MAN':'张三','FOUND_DATE':'2017-10-29 00:00:00'}]"}
			 */
		}
		sb.append("\"}");
		return sb.toString();
	}
	public String toStringNoReplace() {
		StringBuffer sb = new StringBuffer(1024);
		sb.append("_value={\"xml\": \"");//_value:
		sb.append(MyURLEncoder.encode(xml));
		sb.append("\"");
		sb.append(",\"date_col\":\"");
		if(date_col!=null){
			sb.append(date_col);
		}
		sb.append("\"");//Enpower后来有添加一个这个。
//为了解决传送二进制图片问题，凡是调用update函数的接口，都必须需要按照说明或示例头部增加   "photo_col":""  后面引号中写入对应照片字段名。
//		if(photo_col!=null && !"".equals(photo_col.trim())){
			sb.append(",\"photo_col\":\"");
			if(photo_col!=null){
				sb.append(photo_col);
			}
			sb.append("\"");//Enpower后来有添加一个这个。
//		}
		
		sb.append(",\"_value\":\"");
		if(_value!=null){
			sb.append(MyURLEncoder.encode(_value));
			/*Enpower的格式：
			 * { "xml": "插入设备清单", "_value":" [{'TZD_ID':
' b36692fd93ee4ebd9141db78f8a33385',
'EQUI_CODE':'A32BSDE3',
'DRAW_NO':'423432333-001',
'S_ID':'ab23d1243afd3353w23r43rwtr4t3ttw333a3',
'FOUND_MAN':'张三','FOUND_DATE':'2017-10-29 00:00:00'}]"}
			 */
		}
		sb.append("\"}");
		return sb.toString();
	}
	
	public static void main(String[] args) {
		List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
		EnpowerRequestPlanStatusSyn t = new EnpowerRequestPlanStatusSyn();
		t.setStatus("已完成");
		
		list.add(t);

		
		EnpowerRequestMaterialStoreCheckInfo x = new EnpowerRequestMaterialStoreCheckInfo();
		x.setGUARANTEENO("GUARANTEENO");
		List<EnpowerRequestMaterialStoreCheckInfo> list2 = new ArrayList<EnpowerRequestMaterialStoreCheckInfo>();
		list2.add(x);
		EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
		bn.setXml("修改作业条目信息");
		String data = CommonUtility.toJsonStringNull(list2);
		
		data = data.replace("\"","\'");
		System.out.println(data);
		bn.set_value(data);
		logger.info(bn.toString());
//		logger.info(CommonUtility.toJson(bn));
		
		logger.info(bn.toString());
	}

	public String toStringFile() {
		StringBuffer sb = new StringBuffer(1024);
		sb.append("_value={\"xml\": \"");//_value:
		sb.append(xml);
		sb.append("\"");
		sb.append(",\"date_col\":\"");
		if(date_col!=null){
			sb.append(date_col);
		}
		sb.append("\"");//Enpower后来有添加一个这个。
//为了解决传送二进制图片问题，凡是调用update函数的接口，都必须需要按照说明或示例头部增加   "photo_col":""  后面引号中写入对应照片字段名。
//		if(photo_col!=null && !"".equals(photo_col.trim())){
			sb.append(",\"photo_col\":\"");
			if(photo_col!=null){
				sb.append(photo_col);
			}
			sb.append("\"");//Enpower后来有添加一个这个。
//		}
		
		sb.append(",\"_value\":\"");
		if(_value!=null){
			sb.append(_value.replace("\"","\'"));
			/*Enpower的格式：
			 * { "xml": "插入设备清单", "_value":" [{'TZD_ID':
' b36692fd93ee4ebd9141db78f8a33385',
'EQUI_CODE':'A32BSDE3',
'DRAW_NO':'423432333-001',
'S_ID':'ab23d1243afd3353w23r43rwtr4t3ttw333a3',
'FOUND_MAN':'张三','FOUND_DATE':'2017-10-29 00:00:00'}]"}
			 */
		}
		sb.append("\"}");
		return sb.toString();
	}
}
