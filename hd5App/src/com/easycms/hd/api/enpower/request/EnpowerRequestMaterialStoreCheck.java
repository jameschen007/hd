package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import lombok.Data;

/**
 * 物项入库核实接口		需要接收的数据bean	
 * 推送接口名: 物项入库核实接口
 */
@Data
@ApiModel(value="enpowerRequestMaterialStoreCheck")
public class EnpowerRequestMaterialStoreCheck  implements Serializable{
	private static final long serialVersionUID = 1L;
	//物项编码   
    private String MATE_CODE;
//质保编号   
    private String GUARANTEENO;
//存储仓位   
    private String STORAGES_CODE;
//存储货位   
    private String VESSEL_NO;

//	MATE_CODE：物项编码
//	GUARANTEENO：质保编号
//	STORAGES_CODE：存储仓位
//	VESSEL_NO：存储货位
//	PROJ_CODE：项目代号（默认值“K2K3”）
//	COMP_CODE：公司代码（K项默认值：“050812”)
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
