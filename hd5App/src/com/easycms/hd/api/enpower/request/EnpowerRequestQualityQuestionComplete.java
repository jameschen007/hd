package com.easycms.hd.api.enpower.request;

import lombok.Data;

/**
 * 质量整改完成接口		需要接收的数据bean
 * 推送接口名: 责任单位整改完成接口	
 */
@Data
public class EnpowerRequestQualityQuestionComplete {
//	：APP主键
	private String APPID;
//	：整改完成日期
	private String 纰_DATE ;

}
