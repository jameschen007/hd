package com.easycms.hd.api.enpower.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 见证消点接口
 * 推送接口名: 			

 * @author wz
 *
 */
@Data
public class EnpowerRequestDisappearPoint {
	/**
	 * 华辉游云亮 2018/5/14 15:39:09
在？
华辉游云亮 2018/5/14 15:41:07
你们调用 接口 “修改消点信息”
不应该改ID字段，以上是app里调用报错，经分析是调用对应字段错误，正确的格式也已发给你
华辉游云亮 2018/5/14 15:43:25
'NOTE_CODE':'F-7-CPN-11546' 这个字段值，应该就是通知单编号，对应enpower的字段名应该是NOTE_CODE，请注意调用时改为这个字段名，否则报错
华辉游云亮 2018/5/14 15:51:46


华辉游云亮 2018/5/14 16:13:56
另外，这个txt里的消点条目app_id=146168,对应的选点通知单ID的值也没有更新到enp，也就是 main_id
我的建议是

华辉游云亮撤回了一条消息
华辉游云亮 2018/5/14 16:15:33
_value={"xml": "修改消点信息","date_col":"A_DATE,B_DATE,C_DATE,D_DATE,H_DATE","photo_col":"","_value":"[{'NOTE_CODE':'F-7-CPN-11546','A_HG':'合格','A_MAN':'蔡德兵','A_DATE':'2018/05/14 14:17:00','MAIN_ID':'11547','APP_ID':'146168'}]"}
华辉游云亮 2018/5/14 16:16:33
因为你更新过来的消点信息，如果没有main_id，也就是通知单主信息id，enp里也无法显示，只是存留在数据库里

	 */

	// 通知单明细的主键 通知单编号（app生成
	@SerializedName(value = "APP_ID")
	private String noticeOrderNo;

	// QC1是否合格
	@SerializedName(value = "A_HG")
	private String isOkQc1;

	// QC1不合格原因
	@SerializedName(value = "A_YY")
	private String unqualifiedQc1Note;

	// 消点人（QC1）
	@SerializedName(value = "A_MAN")
	private String qc1Man;

	// QC1消点日期
	@SerializedName(value = "A_DATE")
	private String qc1Date;

	// CZEC QC是否合格
	@SerializedName(value = "B_HG")
	private String isOkCzecqc;

	// CZEC QC不合格原因
	@SerializedName(value = "B_YY")
	private String unqualifiedCzecqcNote;

	// 消点人（CZEC QC）
	@SerializedName(value = "B_MAN")
	private String czecqcMan;

	// CZEC QC消点日期
	@SerializedName(value = "B_DATE")
	private String czecqcDate;

	// CZEC QA是否合格
	@SerializedName(value = "C_HG")
	private String isOkCzecqa;

	// CZEC QA不合格原因
	@SerializedName(value = "C_YY")
	private String unqualifiedCzecqaNote;

	// 消点人（CZEC QA）
	@SerializedName(value = "C_MAN")
	private String czecqaMan;

	// CZEC QA消点日期
	@SerializedName(value = "C_DATE")
	private String czecqaDate;

	// PAEC是否合格
	@SerializedName(value = "D_HG")
	private String isOkPaec;

	// PAEC不合格原因
	@SerializedName(value = "D_YY")
	private String unqualifiedPaecNote;

	// 消点人（PAEC）
	@SerializedName(value = "D_MAN")
	private String paecMan;

	// PAEC消点日期
	@SerializedName(value = "D_DATE")
	private String paecDate;

	// QC2是否合格
	@SerializedName(value = "H_HG")
	private String isOkQc2;

	// QC2不合格原因
	@SerializedName(value = "H_YY")
	private String unqualifiedQc2Note;

	// 消点人（QC2）
	@SerializedName(value = "H_MAN")
	private String qc2Man;

	// QC2消点日期
	@SerializedName(value = "H_DATE")
	private String qc2Date;

	
	@SerializedName(value = "APP_MAIN_ID")
	private String mainId;
	
//	@SerializedName(value = "APP_ID")
//	private String appId;
}
