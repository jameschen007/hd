package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;
import com.wordnik.swagger.annotations.ApiModel;

import lombok.Data;

/**
 * 修改入库信息(按质保号)
		需要接收的数据bean	
 * 推送接口名:修改入库信息(按质保号)
 */
@Data
@ApiModel(value="enpowerRequestMaterialStoreCheckInfo")
public class EnpowerRequestMaterialStoreCheckInfo  implements Serializable{
	private static final long serialVersionUID = 1L;
	//enp标识
	private String ID ;
	//制造商
	private String MANUFACTURER ;
	//单价
	private String PRICE ;
	//生产日期
	private String YIELD_DATE ;
	//有效期
	private String WARRANT_START_DATE ;
	//保质期/月
	private String SHELIFE_MONTHS ;
	//保修期/月
	private String MONTHS_WARRANT ;
	//船次
	private String DELIV_LOT ;
	//换算率
	private String FACTRATE ;
	//不合格数量
	private String NOTPASSQTY ;
	//存储仓库
	private String STORAGES_CODE ;
	//存储级别
	private String STRG_CLASS ;
	//货位
	private String VESSEL_NO ;
	//验收时间
	private String CNFMED_DATE ;
	//仓储验收
	private String KEEPER ;
	//文件编号
	private String FILE_NO ;
	//状态
	private String STATUS ;
	//质保编号
	@SerializedName(value = "GUARANTEENO||MATE_CODE||FUNC_NO")
	private String GUARANTEENO ;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
