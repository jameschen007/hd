package com.easycms.hd.api.enpower;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.hd.api.enpower.response.EnpowerResponseHseCode;
import com.easycms.hd.api.request.LoginIdRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.service.HseProblemService;
import com.easycms.hd.qualityctrl.service.QualityControlService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower/hseQcFile",
	consumes = {MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(value = "enpowerHseQcFileController", description = "enpowerHseQcController相关api")
public class EnpowerHseQcFileController {
	private static Logger logger = Logger.getLogger(EnpowerHseQcFileController.class);

	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

	@Autowired
	private HseProblemService hseProblemService ;
	@Autowired
	private QualityControlService qualityControlService ;
	

	/***
	 * 第二批接口
	 * @param args
	 */

    
    /**
     * 上报安全整改问题接口 
     */
    @ResponseBody
    @ApiOperation(value = "上报安全整改问题接口", notes = "上报安全整改问题接口 ")
    @RequestMapping(value = "/hSEQuestionSend", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public JsonResult<Boolean> hSEQuestionSend(HttpServletRequest request, HttpServletResponse response, 
            @ApiParam(required = true, name = "hseId", value = "安全质量问题id") @RequestParam(value="hseId",required=true) Integer hseId) {

        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
        	HseProblem hp = hseProblemService.findById(hseId);
            result = executePushToEnpowerServiceImpl.hSEQuestionSend(hp);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(false);
            return result;
        }
        return result;
    }
    

//    /**
//     * 责任单位整改完成接口 
//     */
//    @ResponseBody
//    @ApiOperation(value = "责任单位整改完成接口", notes = "责任单位整改完成接口 ")
//    @RequestMapping(value = "/hSEQuestionComplete", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
//    public JsonResult<Boolean> hSEQuestionComplete(HttpServletRequest request, HttpServletResponse response, 
//            @ApiParam(required = true, name = "hseId", value = "安全质量问题id") @RequestParam(value="hseId",required=true) Integer hseId, 
//            @ApiParam(required = true, name = "FLAG", value = "状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）") @RequestParam(value="FLAG",required=true) String FLAG, 
//            @ApiParam(required = true, name = "solverId", value = "解决人id") @RequestParam(value="solverId",required=true) Integer solverId) {
//
//        JsonResult<Boolean> result = new JsonResult<Boolean>();
//        try{
//        	HseProblem hp = hseProblemService.findById(hseId);
//            result = executePushToEnpowerServiceImpl.hSEQuestionCompleteBy(hp,FLAG);
//        }catch(Exception e){
//            e.printStackTrace();
//			result.setCode(ExceptionCode.E4);
//            result.setMessage("操作异常:"+e.getMessage());
//            result.setResponseResult(false);
//            return result;
//        }
//        return result;
//    }

//
//    /**
//     * 质量问题整改完成接口 
//     */
//    @ResponseBody
//    @ApiOperation(value = "质量问题整改完成接口", notes = "质量问题整改完成接口 ")
//    @RequestMapping(value = "/qualityQuestionComplete", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
//    public JsonResult<Boolean> qualityQuestionComplete(HttpServletRequest request, HttpServletResponse response, 
//            @ApiParam(required = true, name = "qualityId", value = "质量质量问题id") @RequestParam(value="qualityId",required=true) Integer qualityId, 
//            @ApiParam(required = true, name = "FLAG", value = "状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）") @RequestParam(value="FLAG",required=true) String FLAG, 
//            @ApiParam(required = true, name = "solverId", value = "解决人id") @RequestParam(value="solverId",required=true) Integer solverId) {
//
//        JsonResult<Boolean> result = new JsonResult<Boolean>();
//        RollingPlan plan = new RollingPlan();
//        try{
////        	qualityProblem hp = qualityProblemService.findById(qualityId);
////            result = executePushToEnpowerServiceImpl.qualityQuestionCompleteBy(hp,FLAG,solverId);
//        }catch(Exception e){
//            e.printStackTrace();
//            result.setMessage("操作异常:"+e.getMessage());
//            result.setResponseResult(false);
//            return result;
//        }
//        return result;
//    }

    /**
     * 安全编码信息查询
     */
    @ResponseBody
    @ApiOperation(value = "安全编码信息查询", notes = "安全编码信息查询")
    @RequestMapping(value = "/hseCodeList", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public JsonResult<List<EnpowerResponseHseCode>> hseCodeList(HttpServletRequest request, HttpServletResponse response, 
            @ApiParam(required = false, name = "hseCodeParentId", value = "安全编码父级id") @RequestParam(value="hseCodeParentId",required=false) Integer hseCodeParentId, 
            @ApiParam(required = false, name = "getAll", value = "是否查询所有编码") @RequestParam(value="getAll",required=false,defaultValue="true") Boolean getAll, 
            @ModelAttribute(value="form") LoginIdRequestForm form) {

        JsonResult<List<EnpowerResponseHseCode>> result = new JsonResult<List<EnpowerResponseHseCode>>();
        
        try{
        	EnpowerResponseHseCode para = new EnpowerResponseHseCode();

    		if(getAll){//全部
            	para.setStand(null);
    		}else{
    			if(hseCodeParentId==null){

    				//查一级编码　默认
    				para.setStand(EnpConstant.HSE_STAND1 );
    				
            	}else{//父级

                	para.setParentId(hseCodeParentId+"");
                	para.setStand(null);
            	}
    			
    		}
    		
        	List<EnpowerResponseHseCode> resultStr = executePushToEnpowerServiceImpl.getHseCodeList(para);
            result.setResponseResult(resultStr);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(java.util.Collections.emptyList());
            return result;
        }
        return result;
    }
}
