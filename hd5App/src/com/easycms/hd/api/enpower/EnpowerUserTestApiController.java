package com.easycms.hd.api.enpower;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.enpower.domain.EnpowerUser;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.UserApiService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower",
	consumes = {MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(value = "EnpowerUserTestApiController", description = "EnpowerUserTestApiController相关api")
public class EnpowerUserTestApiController {
	
	@Autowired
	private UserApiService userApiService;
/*	 * @param request
	 * @param response
	 * @param domain
	 * @param timestamp
	 * @param signature
	 * @param userJson
	 * @return
	 */
	@ResponseBody
	@ApiOperation(value = "根据Enpower用户数据同步写入系统组织架构", notes = "enpower远程调用接口，传入一个json数据，最终解析为适配当前系统的组织架构。")
	@RequestMapping(value = "/testUser", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> teamAssign(HttpServletRequest request, HttpServletResponse response) {
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		
		
		return result;
	}
}
