package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.Set;

import com.easycms.hd.api.response.DepartmentResult;

import lombok.Data;

@Data
public class AuthRole implements Serializable{
	private static final long serialVersionUID = 2480177962782148936L;
	private Integer id;
	private String name;
	private Set<String> roleType;
	
	private DepartmentResult department;
}
