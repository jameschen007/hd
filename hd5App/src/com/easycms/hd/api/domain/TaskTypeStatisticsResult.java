package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TaskTypeStatisticsResult implements Serializable{
	private static final long serialVersionUID = -260186299789145683L;
	private String type;
	private List<Map<String, String>> result;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Map<String, String>> getResult() {
		return result;
	}
	public void setResult(List<Map<String, String>> result) {
		this.result = result;
	}
}
