package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.List;

import com.easycms.management.user.domain.User;

public class Witnesser implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	
	private String witnesserType;
	private String referenceType;
	private List<User> witnesser;
	
	public String getWitnesserType() {
		return witnesserType;
	}
	public void setWitnesserType(String witnesserType) {
		this.witnesserType = witnesserType;
	}
	public List<User> getWitnesser() {
		return witnesser;
	}
	public void setWitnesser(List<User> witnesser) {
		this.witnesser = witnesser;
	}
	public String getReferenceType() {
		return referenceType;
	}
	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}
}
