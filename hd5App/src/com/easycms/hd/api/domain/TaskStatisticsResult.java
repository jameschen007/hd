package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.statistics.domain.TaskStatistics;
import com.easycms.management.user.domain.Department;

public class TaskStatisticsResult implements Serializable{
	private static final long serialVersionUID = 6115091916001080215L;
	private Department department;
	private List<TaskStatistics> tasks;
	
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public List<TaskStatistics> getTasks() {
		return tasks;
	}
	public void setTasks(List<TaskStatistics> tasks) {
		this.tasks = tasks;
	}
}
