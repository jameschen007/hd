package com.easycms.hd.api.baseservice.hseproblem;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.hd.api.enums.hseproblem.DynamicFilterItems;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.hseproblem.HseProbelmTypeEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemSolveStatusEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemAssignRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemListRequestFormByStatus;
import com.easycms.hd.api.request.hseproblem.HseProblemRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemCreatUiDataResult2;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.api.response.hseproblem.HseProblemResult;
import com.easycms.hd.api.service.HseProblemApiService;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.easycms.hd.hseproblem.domain.HseProblemSolveStep;
import com.easycms.hd.hseproblem.domain.HseProblemStep;
import com.easycms.hd.hseproblem.service.HseProblemFileService;
import com.easycms.hd.hseproblem.service.HseProblemService;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.hd.plan.mybatis.dao.StatisticsHse3monthDao;
import com.easycms.hd.statistics.domain.StatisticsHse3month;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/hse")
@Api(value="HseProblemAPIController",description="文明施工问题模块相关的api")
@Slf4j
@javax.transaction.Transactional
public class HseProblemAPIController {
	@Autowired
	private HseProblemService hseProblemService;
	@Autowired
	private HseProblemApiService hseProblemApiService;
	@Autowired
	private HseProblemFileService hseProblemFileService;
	@Autowired
	private UserService userService;
	@Autowired
	private HseProblemSolveService hseProblemSolveService;
	@Autowired
	private StatisticsHse3monthDao statisticsHse3monthDao;

	@ResponseBody
	@RequestMapping(value = "createUI", method = RequestMethod.GET)
	@ApiOperation(value = "创建安全文明问题界面", notes = "用于获取机组、厂房、责任部门、责任班组")
	public JsonResult<HseProblemCreatUiDataResult> createUI (HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm,
			@ApiParam(required = false, name = "safe", value = "是否安全模块,如果是安全safe=safe") @RequestParam(value="safe",required=false) String safe,
			@ApiParam(required = false, name = "responsibleDeptId", value = "责任部门ID") @RequestParam(value="responsibleDeptId",required=false) Integer responsibleDeptId){
		JsonResult<HseProblemCreatUiDataResult> result = new JsonResult<HseProblemCreatUiDataResult>();
		HseProblemCreatUiDataResult hseProblemCreatUiResult = null;
		
		try{

			hseProblemCreatUiResult = hseProblemApiService.getHseProblemCreatUiResult(responsibleDeptId,safe);
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(hseProblemCreatUiResult);
			return result;
		}
		
		result.setResponseResult(hseProblemCreatUiResult);
		return result;

	}

	@ResponseBody
	@RequestMapping(value = "v2/createUI", method = RequestMethod.GET)
	@ApiOperation(value = "创建安全文明问题界面", notes = "用于获取机组、厂房、责任部门、责任班组")
	public JsonResult<HseProblemCreatUiDataResult2> createUI_v2 (HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm,
			@ApiParam(required = false, name = "safe", value = "是否安全模块") @RequestParam(value="safe",required=false) String safe,
			@ApiParam(required = false, name = "responsibleDeptId", value = "责任部门ID") @RequestParam(value="responsibleDeptId",required=false) Integer responsibleDeptId){
		JsonResult<HseProblemCreatUiDataResult2> result = new JsonResult<HseProblemCreatUiDataResult2>();
		HseProblemCreatUiDataResult2 hseProblemCreatUiResult = null;
		
		try{

			hseProblemCreatUiResult = hseProblemApiService.getHseProblemCreatUiResultForReport(responsibleDeptId,safe);
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(hseProblemCreatUiResult);
			return result;
		}
		
		result.setResponseResult(hseProblemCreatUiResult);
		return result;

	}
	
	@ResponseBody
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ApiOperation(value = "创建安全文明问题", notes = "用于创建安全文明问题")
	public JsonResult<Boolean> create(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") HseProblemRequestForm hseProblemRequestForm,@RequestParam(value="file",required=false) CommonsMultipartFile[] files){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		Boolean status = false ;
		Integer loginId = hseProblemRequestForm.getLoginId();
		if (null == loginId || loginId.intValue()==0) {
			result.setCode("-1001");
			result.setMessage("Error/s occurred, loginId is not available");
			return result;
		}
		if(!CommonUtility.isNonEmpty(hseProblemRequestForm.getUnit())){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, Unit is not available");
			return result;
		}
		if(!CommonUtility.isNonEmpty(hseProblemRequestForm.getWrokshop())){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, Wrokshop is not available");
			return result;
		}
//		if(!CommonUtility.isNonEmpty(hseProblemRequestForm.getEleration())){//标高 20180422 ａｐｐ选填
//			result.setCode("-1001");
//			result.setMessage("Error/s occurred, Eleration is not available");
//			return result;
//		}
//		if(!CommonUtility.isNonEmpty(hseProblemRequestForm.getRoomno())){//房间号 20180422 ａｐｐ选填
//			result.setCode("-1001");
//			result.setMessage("Error/s occurred, Roomno is not available");
//			return result;
//		}
		try{

			status = hseProblemApiService.createHseProblem(hseProblemRequestForm, files);
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}

		if(!status){
			result.setCode("-1001");
			result.setMessage("create problem error!");
			return result;
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "problemList", method = RequestMethod.GET)
	@ApiOperation(value = "查询安全文明问题分页列表", notes = "用于查询各状态安全文明问题列表，问题进度状态：need_handle:待处理;renovating:整改中;need_check:待审查;finish:已完成;none:不需要处理")
	public JsonResult<HseProblemPageDataResult> problemList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") HseProblemListRequestFormByStatus hseProblemRequestForm,
			@ModelAttribute("requestForm2") HseProblemExListRequestForm hseProblemExListRequestForm
	){
		if(true){//调试信息
			log.debug("问题进度状态problemStatus="+hseProblemRequestForm.getProblemStatus());
			log.debug("问题处理情况（pre:待处;done:已处理）："+hseProblemRequestForm.getProblemSolveStatus());
			log.debug("问题类型（all:所有已创建的问题;mine：登录用户创建的问题）："+hseProblemRequestForm.getProbelmType());
				if (hseProblemExListRequestForm!=null && hseProblemExListRequestForm.getCreateTime() != null) {
					System.out.println("提出时间筛选字段="+hseProblemExListRequestForm.getCreateTime());
				}
			if (hseProblemExListRequestForm!=null && hseProblemExListRequestForm.getTargetTime() != null) {
					System.out.println("截止时间筛选字段="+hseProblemExListRequestForm.getTargetTime());
				}
			}

		JsonResult<HseProblemPageDataResult> result = new JsonResult<HseProblemPageDataResult>();
		HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
		Page<HseProblem> page = new Page<HseProblem>();
		String problemStatus = null;
		String solveStatus = null;
		String problemType = null;
		//问题类型判断
		if(hseProblemRequestForm.getProbelmType()!=null){
			problemType = hseProblemRequestForm.getProbelmType().toString();
		}
		//问题状态判断
		if(hseProblemRequestForm.getProblemStatus()!=null){
			problemStatus = hseProblemRequestForm.getProblemStatus().toString();
		}
		//判断处理情况是否为空
		if(hseProblemRequestForm.getProblemSolveStatus()!=null){
			solveStatus = hseProblemRequestForm.getProblemSolveStatus().toString();
		}
		
		if (null != hseProblemRequestForm.getPagenum() && null != hseProblemRequestForm.getPagesize()) {
			page.setPageNum(hseProblemRequestForm.getPagenum());
			page.setPagesize(hseProblemRequestForm.getPagesize());
		}
		try{
			if(hseProblemRequestForm.getLoginId()!=null){
				User user = userService.findUserById(hseProblemRequestForm.getLoginId());
				if(user == null){
					result.setCode("-1001");
					result.setMessage("用户不存在！");
					return result;
				}
				
				if(HseProbelmTypeEnum.mine.toString().equals(problemType)){
					hseProblemPageDataResult = hseProblemApiService.getHseProblemResultByUser(page, problemStatus, user,hseProblemRequestForm.getKeyword(), hseProblemExListRequestForm);
				}else{
					hseProblemPageDataResult = hseProblemApiService.getHseProblemPageDataResult(page, problemStatus,solveStatus,user,hseProblemRequestForm.getKeyword(), hseProblemExListRequestForm);
				}
				
			}	
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-1001");
			result.setMessage("操作异常："+e.getMessage());
		}
		result.setResponseResult(hseProblemPageDataResult);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "problem/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "查询安全文明问题", notes = "用于查询单个安全文明问题")
	public JsonResult<HseProblemResult> problemListOne(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm hpRequestForm,@PathVariable("id") Integer id) throws Exception {
		JsonResult<HseProblemResult> result = new JsonResult<HseProblemResult>();
		HseProblemResult hpResult = null;
		HseProblem hp = hseProblemService.findById(id);
		if(hp != null){
			hpResult = hseProblemApiService.transferForList(hp, null);
			result.setResponseResult(hpResult);
		}else{
			result.setCode("-1001");
			result.setMessage("查询的安全问题不存在！");
		}

		return result;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "assign", method = RequestMethod.POST)
	@ApiOperation(value = "HSE执行分派", notes = "用于HSE分派整改任务,responsibleTeamId责任班组id，problemId问题id")
	public JsonResult<Boolean> assign (HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") HseProblemAssignRequestForm hseProblemAssignRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		Integer loginId = null;
		if(hseProblemAssignRequestForm.getLoginId() != null){
			loginId = hseProblemAssignRequestForm.getLoginId();
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser == null){
			result.setCode("-1000");
			result.setMessage("当前用户不存在！");
			return result;
		}
		try{
			//登录用户是否是HSE部门成员
			if(!userService.isDepartmentOf(loginUser, "HSEDepartment")){
				result.setCode("-1000");
				result.setMessage("当前用户不是HSE部门成员，无法执行该操作！");
				return result;
			}
		}catch(Exception e){
			log.error("something error");
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		if(hseProblemAssignRequestForm.getProblemId() == null){
			result.setCode("-1000");
			result.setMessage("必要参数problemID为空！");
			return result;
		}

		User logining = userService.findUserById(loginId);
		status = hseProblemApiService.hseAssignTasks(hseProblemAssignRequestForm,logining);
		if(!status){
			result.setCode("-1001");
			result.setMessage("执行分派出错！");
		}
		result.setResponseResult(status);			
		return result;

	}
	
	@ResponseBody
	@RequestMapping(value = "unAssign", method = RequestMethod.POST)
	@ApiOperation(value = "HSE执行不需要处理问题", notes = "用于HSE将问题设为不需要处理状态，problemId问题id")
	public JsonResult<Boolean> unAssign(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "problemId", value = "文明施工问题ID") @RequestParam(value="problemId",required=true) Integer problemId,@ModelAttribute("requestForm") BaseRequestForm baseRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		try{
			Integer loginId = null;
			if(baseRequestForm.getLoginId() != null){
				loginId = baseRequestForm.getLoginId();
			}
			User loginUser = userService.findUserById(loginId);
			if(loginUser == null){
				result.setCode("-1000");
				result.setMessage("当前用户不存在！");
				return result;
			}
			//登录用户是否是HSE部门成员
			if(!userService.isDepartmentOf(loginUser, "HSEDepartment")){
				result.setCode("-1000");
				result.setMessage("当前用户不是HSE部门成员，无法执行该操作！");
				return result;
			}
			
			//将问题状态改为不需要处理状态
			HseProblem hseProblem = hseProblemService.findById(problemId);
			boolean isNeedHandle = false;
			if(hseProblem != null){
				isNeedHandle = hseProblem.getProblemStatus().equals(HseProblemStatusEnum.Need_Handle.toString());				
			}else{
				result.setCode("-1001");
				result.setMessage("当前问题不存在！");
				return result;
			}
			if(!isNeedHandle){
				result.setCode("-1001");
				result.setMessage("当前状态不允许关闭问题！");
				return result;
			}
			hseProblem.setProblemStatus(HseProblemStatusEnum.None.toString());
			hseProblem.setProblemStep(HseProblemStep.isFinished.toString());//问题关闭
			hseProblem.setFinishDate(Calendar.getInstance().getTime());//问题关闭时间
			hseProblemService.update(hseProblem);

			//将HSE处理情况更新为已处理
			List<HseProblemSolve> hseProblemSolves =hseProblemSolveService.findByProblenIdAndSolveStep(problemId, HseProblemSolveStep.hseConfirm.toString());
			if(hseProblemSolves != null && !hseProblemSolves.isEmpty()){
				HseProblemSolve hseProblemSolve = hseProblemSolves.get(0);
				hseProblemSolve.setSolveUser(baseRequestForm.getLoginId());
				hseProblemSolve.setSolveDate(Calendar.getInstance().getTime());
				hseProblemSolve.setSolveStatus(HseProblemSolveStatusEnum.Done.toString());
				hseProblemSolveService.update(hseProblemSolve);
			}
			status=true;
		}catch(Exception e){
			log.error("something error");
			e.getMessage();
		}
		if(!status){
			result.setCode("-1001");
			result.setMessage("操作失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "submitRenovateResult", method = RequestMethod.POST)
	@ApiOperation(value = "提交整改结果", notes = "用于整改责任部门提交整改结果，problemId问题id")
	public JsonResult<Boolean> submitRenovateResult(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "problemId", value = "文明施工问题ID") @RequestParam(value="problemId",required=true) Integer problemId,
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId,
			@ApiParam(required = true, name = "description", value = "整改描述") @RequestParam(value="description",required=true) String description,
			@RequestParam(value="file") CommonsMultipartFile[] files){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		
		status = hseProblemApiService.processRenovateResult(problemId, description, files,loginId);
		if(!status){
			result.setCode("-1001");
			result.setMessage("提交结果失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "checkRenovateResult", method = RequestMethod.POST)
	@ApiOperation(value = "HSE审查整改结果", notes = "用于HSE审查整改结果，problemId问题id，checkResult：通过或者重新整改")
	public JsonResult<Boolean> checkRenovateResult(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "problemId", value = "文明施工问题ID") @RequestParam(value="problemId",required=true) Integer problemId,
			@ApiParam(required = true, name = "checkResult", value = "审查结果，1-通过，2-不通过") @RequestParam(value="checkResult",required=true) Integer checkResult,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm){
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		Integer loginId = null;
		if(baseRequestForm.getLoginId() != null){
			loginId = baseRequestForm.getLoginId();
		}
		User loginUser = userService.findUserById(loginId);
		if(loginUser == null){
			result.setCode("-1000");
			result.setMessage("当前用户不存在！");
			return result;
		}
		//登录用户是否是HSE部门成员
		if(!userService.isDepartmentOf(loginUser, "HSEDepartment")){
			result.setCode("-1000");
			result.setMessage("当前用户不是HSE部门成员，无法执行该操作！");
			return result;
		}
		
		boolean status = false;
		
        try{
        	status = hseProblemApiService.checkRenovateResult(baseRequestForm,problemId,checkResult);
    		
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(status);
            return result;
        }
        
		if(!status){
			result.setCode("-1001");
			result.setMessage("操作失败！");
		}

		result.setResponseResult(status);
		return result;
	}
	@ResponseBody
	@RequestMapping(value = "hseStatistics3Month", method = RequestMethod.GET)
	@ApiOperation(value = "近90天（三月）总体隐患数量", notes = "近90天（三月）总体隐患数量")
	public JsonResult<StatisticsHse3month> hseStatistics3Month(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {

		JsonResult<StatisticsHse3month> result = new JsonResult<StatisticsHse3month>();

		StatisticsHse3month map = new StatisticsHse3month();

		map = statisticsHse3monthDao.find();
		if(map!=null ){
			SimpleDateFormat sdf = new SimpleDateFormat("从yyyy年MM月dd日至今3月内");
//			map.setDay90agoTips(sdf.format(map.getDay90ago()));
		}
		result.setResponseResult(map);

		return result;
	}
	@ResponseBody
	@RequestMapping(value = "hseStatistics3MonthCaptain", method = RequestMethod.GET)
	@ApiOperation(value = "按责任部门近90天（三月）总体隐患数量", notes = "按责任部门近90天（三月）总体隐患数量")
	public JsonResult<List<StatisticsHse3month>> hseStatistics3MonthCaptain(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {

		JsonResult<List<StatisticsHse3month>> result = new JsonResult<List<StatisticsHse3month>>();

		List<StatisticsHse3month> map = statisticsHse3monthDao.findWithCaptain();
		result.setResponseResult(map);

		return result;
	}

}
