package com.easycms.hd.api.baseservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.request.JPushSingleRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/jpush")
@Api(value = "JPushAPIController", description = "极光推送相关的api")
public class JPushAPIController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private JPushService jPushService;
	
	/**
	 * 推送消息到指定用户的所有设备上
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ApiOperation(value = "推送到指定人", notes = "输入用户id，将指定消息推送到设备上。")
	public JsonResult<Boolean> pushSingle(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") JPushSingleRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		if (!CommonUtility.isNonEmpty(form.getMessage())) {
			result.setCode("-2001");
			result.setMessage("必须输入推送的消息内容");
			return result;
		}
		
		User user = null;
		if (null != form.getUserId()) {
			user = userService.findUserById(form.getUserId());
		} else {
			user = userService.findUserById(form.getLoginId());
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		
		JPushExtra extra = new JPushExtra();
		extra.setCategory(JPushCategoryEnums.API_CALL.name());
		jPushService.pushByUserId(extra, form.getMessage(), form.getTitle(), user.getId());
		
		result.setMessage("成功发起推送");
		return result;
	}
}
