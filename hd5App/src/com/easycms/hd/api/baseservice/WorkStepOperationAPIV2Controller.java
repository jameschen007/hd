package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.WorkStepWitnessItemsFormV2;
import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.WorkStepWitnessItemsResult;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/v2/workstep_op")
@Api(value = "WorkStepOperationAPIV2Controller", description = "针对工序操作相关的api -- V2")
@Transactional
public class WorkStepOperationAPIV2Controller {
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepApiService workStepApiService;
	@Autowired
	private RollingPlanService rollingPlanService;
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	/**
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/witness", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "工序上发起见证 - V2", notes = "对某道工序发起见证, ID为一串，其它为一项")
	public JsonResult<List<WorkStepWitnessItemsResult>> launch(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam @ApiParam(value = "登录用户ID",required=true) Integer loginId,
			@ModelAttribute("form") WorkStepWitnessItemsFormV2 workStepWitnessItemsForm
			){
		JsonResult<List<WorkStepWitnessItemsResult>> result = new JsonResult<List<WorkStepWitnessItemsResult>>();
		User user = userService.findUserById(loginId);
		
		if (null == workStepWitnessItemsForm.getIds() || workStepWitnessItemsForm.getIds().length == 0) {
			result.setCode("-1000");
			result.setMessage("工序ID号不能为空。");
			return result;
		}
		
		Set<WorkStepWitnessItemsForm> workStepWitnessItemsForms = new HashSet<WorkStepWitnessItemsForm>();
		for (Integer id : workStepWitnessItemsForm.getIds()) {
			WorkStepWitnessItemsForm wswf = new WorkStepWitnessItemsForm();
			wswf.setId(id);
			wswf.setWitnessaddress(workStepWitnessItemsForm.getWitnessaddress());
			wswf.setWitnessdate(workStepWitnessItemsForm.getWitnessdate());
			workStepWitnessItemsForms.add(wswf);
		}
		try{

			List<WitingPlan> planList = this.rollingPlanService.findByWorkSteps(workStepWitnessItemsForms);
			
			List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = workStepApiService.launchWitness(planList, user);

			if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().startsWith("-");}).count() > 0L) {
				result.setCode("-1000");
				result.setMessage("发起见证出现异常。");
			} else if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().equals("-8002");}).count() > 0L){
				result.setMessage("某些曾发起过的见证点被自动跳过，其余见证点已被成功发起");
			} else {
				result.setMessage("成功发起所有见证");
			}
			
			result.setResponseResult(workStepWitnessItemsResults);
			
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			return result;
		}
		return result;
	}
}
