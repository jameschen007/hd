package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.request.NotificationPageRequestForm;
import com.easycms.hd.api.request.NotificationRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.NotificationPageResult;
import com.easycms.hd.api.response.NotificationResult;
import com.easycms.hd.api.service.NotificationApiService;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.domain.NotificationFile;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.hd.conference.service.NotificationFileService;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/notification")
@Api(value = "NotificationAPIController", description = "通知通知相关的api")
@Slf4j
@javax.transaction.Transactional
public class NotificationAPIController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private NotificationApiService notificationApiService;
	
	@Autowired
	private NotificationFileService notificationFileService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_notification_path']}")
	private String fileNotificationPath;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	/**
	 * 发起通知
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(value = "发起通知", notes = "用户发起一次通知")
	public JsonResult<Boolean> createNotification(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") NotificationRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		User user = userService.findUserById(form.getLoginId());
		
		JsonResult<Integer> notificationResult = notificationApiService.insert(form, user, ConferenceSource.API);
		if (null != notificationResult && null != notificationResult.getResponseResult()) {
			String filePath = basePath + fileNotificationPath;
			List<Integer> removeIds = new ArrayList<Integer>();
			List<NotificationFile>  notificationFiles = notificationFileService.findFilesByNotificationId(notificationResult.getResponseResult());
			if (null != form.getFileIds() && form.getFileIds().size() > 0) {
				if (null != notificationFiles && !notificationFiles.isEmpty()) {
					for (NotificationFile notificationFile : notificationFiles) {
						if (!form.getFileIds().contains(notificationFile.getId())) {
							FileUtils.remove(filePath + notificationFile.getPath());
							removeIds.add(notificationFile.getId());
						}
					}
				}
			} else {
				if (null != notificationFiles && !notificationFiles.isEmpty()) {
					for (NotificationFile notificationFile : notificationFiles) {
						FileUtils.remove(filePath + notificationFile.getPath());
						removeIds.add(notificationFile.getId());
					}
				}
			}
			
			if (null != removeIds && !removeIds.isEmpty()) {
				notificationFileService.batchRemove(removeIds.toArray(new Integer[] {}));
			}
			
            List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
            if (fileInfos != null && fileInfos.size() > 0) {
                for (FileInfo fileInfo : fileInfos) {
                	NotificationFile file = new NotificationFile();
                    file.setPath(fileInfo.getNewFilename());
                    file.setTime(new Date());
                    file.setNotificationid(notificationResult.getResponseResult());
                    String fileName = fileInfo.getFilename();
                    if (fileName.length() > 90) {
                    	fileName = fileName.substring(0, 90);
                    }
                    file.setFileName(fileName);
                    log.debug("upload file success : " + file.toString());
                    notificationFileService.add(file);
                }
            }
		} else {
			result.setCode("-1000");
			result.setMessage("创建通知失败。");
			return result;
		}
		return result;
	}
	
	/**
	 * 获取通知
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取通知", notes = "获取不同类型的通知记录 -- 发送和草稿")
	public JsonResult<NotificationPageResult> witness(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") NotificationPageRequestForm form) throws Exception {
		JsonResult<NotificationPageResult> result = new JsonResult<NotificationPageResult>();
		if (null == form.getType()) {
			result.setCode("-1000");
			result.setMessage("通知请求类型不能为空。");
			return result;
		}
		
		User user = userService.findUserById(form.getLoginId());
		
		if (null != form.getPagenum() && null != form.getPagesize()) {
			Page<Notification> page = new Page<Notification>();

			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
			
			NotificationPageResult notificationPageResult = notificationApiService.getNotificationPageResult(form.getType(), user.getId(), page);
			result.setResponseResult(notificationPageResult);
			result.setMessage("成功获取分页信息");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 获取会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取通告-单个", notes = "获取单个的通告记录")
	public JsonResult<NotificationResult> getSingleConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form, 
			@PathVariable("id") Integer id) throws Exception {
		JsonResult<NotificationResult> result = new JsonResult<NotificationResult>();
		Notification notification = notificationService.findById(id);
		
		if (null != notification) {
			ConferenceApiRequestType type = ConferenceApiRequestType.valueOf(notification.getType());
			
			result.setResponseResult(notificationApiService.notificationTransferToNotificationResult(notification, form.getLoginId(), type, true));
		} else {
			result.setCode("-1001");
			result.setMessage("通告不存在");
			return result;
		}
		
		return result;
	}
}
