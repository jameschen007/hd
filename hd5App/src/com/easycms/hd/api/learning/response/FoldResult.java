package com.easycms.hd.api.learning.response;

import java.util.List;

import lombok.Data;

@Data
public class FoldResult {
	private Integer id;
	private Integer seq;
	private String name;
	List<LearningResult> learnings;
}
