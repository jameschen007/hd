package com.easycms.hd.api.learning.response;

import java.util.List;

import lombok.Data;

@Data
public class SectionDetailResult {
	private String type;
	private List<ModuleFoldResult> modules;
	private List<FoldResult> folds;
}
