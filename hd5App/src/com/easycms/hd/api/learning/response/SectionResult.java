package com.easycms.hd.api.learning.response;

import lombok.Data;

@Data
public class SectionResult {
	private Integer id;
	private String type;
	private String name;
}
