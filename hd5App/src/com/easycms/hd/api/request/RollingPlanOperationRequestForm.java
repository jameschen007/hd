package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.easycms.hd.api.enums.RollingPlanMethodEnum;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class RollingPlanOperationRequestForm implements Serializable{
	private static final long serialVersionUID = -437514136156419730L;
	@ApiParam(value = "滚动计划的类型", required = true)
	String type;
	@ApiParam(value = "请求操作的类型", required = true)
	RollingPlanMethodEnum method;
	//登录用户ID
	@ApiParam(value = "登录时的用户 ID", required = true)
	Integer loginId;
	
	@ApiParam(value = "分派，改派，解除等所操作的rollingplan ID(s)", required = false)
	Set<Integer> ids;
	
	//分派时的参数
	@ApiParam(value = "分派和改派时的用户 ID", required = false)
	Integer userId;
	@ApiParam(value = "施工日期", required = false)
	Date consDate;
	@ApiParam(value = "登录用户当前选择角色id",required=false,defaultValue="516")
	Integer roleId;
}
