package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class WorkStepWitnessItemsForm implements Serializable {
	private static final long serialVersionUID = 3632638504971317590L;
	@ApiParam(value = "workstep ID号", required = true)
	private Integer id;
	@ApiParam(value = "扩展：workstep ID号，以逗号分隔id字符串,当非空时，优先级高于id", required = false)
	private String ids;
	@ApiParam(value = "见证地址", required = true)
	private String witnessaddress;
	@ApiParam(value = "见证日期", required = true)
	private String witnessdate;
	
	public WorkStepWitnessItemsForm() {}
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof WorkStepWitnessItemsForm) {
        	WorkStepWitnessItemsForm form = (WorkStepWitnessItemsForm) obj;
            return (id.equals(form.getId()));
        }
        return super.equals(obj);
    }
	
	@Override 
    public int hashCode() {
		if(ids!=null){
			return ids.hashCode();
		}
        return id.hashCode();
    }
	
/**	
  	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private Integer qcsign;
	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private String qcman;
	**/
}
