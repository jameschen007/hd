package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RollingPlanWitnessRequestForm extends BaseRequestForm implements Serializable {
	private static final long serialVersionUID = 6687792075930773234L;
	@ApiParam(value = "workstep ID号", required = true)
	private Integer id;
	@ApiParam(value = "见证地址", required = true)
	private String witnessaddress;
	@ApiParam(value = "见证日期", required = true)
	private Date witnessdate;
	
/**	
  	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private Integer qcsign;
	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private String qcman;
	**/
}
