package com.easycms.hd.api.request.hseproblem;

import com.easycms.hd.api.enums.hseproblem.*;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * app下拉筛选条件增加
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemExListRequestForm implements Serializable {
	private static final long serialVersionUID = 608356712874057613L;
	@ApiParam(name = "createTime", value = "提出时间筛选字段。monthEarly0降序monthEarly1升序，以此类推", required = false)
	private Hse3mEnumCreateTime createTime;
	@ApiParam(name = "targetTime", value = "截止时间筛选字段.monthEarly0降序monthEarly1升序，以此类推", required = false)
	private Hse3mEnumCutOffTime targetTime;

}
