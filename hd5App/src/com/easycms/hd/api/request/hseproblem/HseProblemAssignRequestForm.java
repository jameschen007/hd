package com.easycms.hd.api.request.hseproblem;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemAssignRequestForm extends BasePagenationRequestForm implements Serializable {
	private static final long serialVersionUID = 2454165370789134497L;
	
	@ApiParam(required = true, name = "problemId", value = "文明施工问题ID")
	Integer problemId;
	@ApiParam(required = true, name = "responsibleCaptainId", value = "指派整改队")
	Integer responsibleCaptainId;
	@ApiParam(required = true, name = "responsibleTeamId", value = "指派整改班组")
	Integer responsibleTeamId;
	@ApiParam(required = true, name = "startDate", value = "要求整改日期",defaultValue="1508210311000")
	Long startDate;

}
