package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RollingPlanBackFillRequestFormV2 extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -6483967904125738836L;
	@ApiParam(value = "焊工的用户", required = true)
	String welder;
	@ApiParam(value = "焊接日期", required = true)
	Date weldDate;
	@ApiParam(value = "滚动计划ID", required = true)
	Integer id;
}
