package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class LoginRequestForm implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	
	@ApiParam(value = "用户登录的ID号,即用户名", required = true)
	String username;
	@ApiParam(value = "用户登录的密码", required = true)
	String password;
	@ApiParam(value = "用户当前所登录使用的设备自定义号", required = false)
	String uuid;
}
