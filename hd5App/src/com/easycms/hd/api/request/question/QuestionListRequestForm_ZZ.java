package com.easycms.hd.api.request.question;

import java.io.Serializable;

import com.easycms.hd.api.enums.question.QuestionStatusEnum_ZZ;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>     <br>
 * <b>创建时间：</b>2017-09-15 下午05:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class QuestionListRequestForm_ZZ extends BasePagenationRequestForm implements Serializable{

	private static final long serialVersionUID = -6892963500085316925L;

	@ApiParam(value = "问题状态", required = true)
	QuestionStatusEnum_ZZ questionStatus;
}
