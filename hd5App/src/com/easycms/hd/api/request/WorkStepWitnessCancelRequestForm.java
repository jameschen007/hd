package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepWitnessCancelRequestForm extends BaseRequestForm implements Serializable {
	private static final long serialVersionUID = 5475034259905987980L;
	
	@ApiParam(value = "工序上批量批量取消见证的IDS", required = true)
	List<Integer> ids;
}
