package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -4973603438582526873L;
	@ApiParam(value = "查询用户时的ID号", required = true)
	private Integer userId;
}
