package com.easycms.hd.api.request.base;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BaseIdsRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -3843225729688347406L;
	@ApiParam(value = "Ids",required=true)
	Integer[] ids;
}
