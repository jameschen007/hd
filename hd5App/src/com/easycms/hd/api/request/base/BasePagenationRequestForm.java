package com.easycms.hd.api.request.base;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BasePagenationRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -3843225729688347406L;
	@ApiParam(value = "每页返回数据大小",defaultValue="10")
	Integer pagesize = 10;
	@ApiParam(value = "分页页码号",defaultValue="1")
	Integer pagenum = 1;
//	@ApiParam(value = "搜索时的搜索类型")
//	String searchKey;
//	@ApiParam(value = "搜索的关键字")
//	String searchValue;
	@ApiParam(value = "搜索的关键字")
	String keyword;
}
