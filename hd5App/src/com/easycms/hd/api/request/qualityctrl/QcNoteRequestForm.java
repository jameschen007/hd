package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QcNoteRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 4311969097814274486L;

	@ApiParam(required = true, name = "qcProblrmId", value = "质量管理问题ID")
	private Integer qcProblrmId;
	@ApiParam(required = true, name = "note", value = "备注")
	private String note;
	
}
