package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.qualityctrl.domain.QualityControlStatus;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControlPageRequestForm extends BasePagenationRequestForm implements Serializable{
	private static final long serialVersionUID = 6345184410403623606L;
	
	@ApiParam(value = "问题状态", required = false)
	private QualityControlStatus status;

}
