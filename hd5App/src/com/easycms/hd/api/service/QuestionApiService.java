package com.easycms.hd.api.service;

import com.easycms.core.util.Page;
//import com.easycms.hd.api.response.question.RollingQuestionAssignResult;
import com.easycms.hd.api.response.question.RollingQuestionPageDataResult;
import com.easycms.hd.api.response.question.RollingQuestionResult;
import com.easycms.hd.question.domain.RollingQuestion;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>     <br>
 * <b>创建时间：</b>2017-09-15 下午05:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface QuestionApiService {
	
	public static final String teamAnswer = "teamAnswer";

	//获取所有跟question相关的分页信息
	RollingQuestionPageDataResult getQuestionPageDataResult(Page<RollingQuestion> page, Integer loginId, Integer userId, String questionStatus,String type,String keyword,Integer usingRoleId);
	
	/**
	 * 将实体类转换为接口输出,仅列表显示使用
	 * @param question
	 * @return
	 */
	public RollingQuestionResult transferForList(RollingQuestion question,boolean isShowChildAndFile,Integer loginId);
	
	
	/**
	 * 将实体类转换为接口输出 是指派需要的
	 * @param question
	 * @return
	 */
//	public RollingQuestionAssignResult transferTo2(RollingQuestion question);
	
	
}
