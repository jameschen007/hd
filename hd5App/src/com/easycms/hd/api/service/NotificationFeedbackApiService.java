package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.hd.api.request.NotificationFeedbackRequestForm;
import com.easycms.hd.api.request.NotificationMarkRequestForm;
import com.easycms.hd.api.response.NotificationFeedbackResult;
import com.easycms.hd.conference.domain.NotificationFeedback;
import com.easycms.management.user.domain.User;

public interface NotificationFeedbackApiService {
	NotificationFeedbackResult notificationFeedbackTransferToNotificationFeedbackResult(NotificationFeedback notificationFeedback);
	
	boolean insert(NotificationFeedbackRequestForm form, User user);

	List<NotificationFeedbackResult> notificationFeedbackTransferToNotificationFeedbackResult(List<NotificationFeedback> notificationFeedbacks);

	boolean markRead(NotificationMarkRequestForm form, User user);
}
