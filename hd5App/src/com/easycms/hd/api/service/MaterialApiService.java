package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.response.MaterialPageResult;
import com.easycms.hd.api.response.MaterialResult;
import com.easycms.hd.material.domain.Material;

public interface MaterialApiService {

	List<MaterialResult> materialTransferToMaterialResult(List<Material> materials, Integer userId);

	MaterialPageResult getMaterialPageResult(Integer userId, Page<Material> page);

	MaterialResult materialTransferToMaterialResult(Material material, Integer userId);
	
}
