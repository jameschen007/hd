package com.easycms.hd.api.service;

import java.util.Set;

import com.easycms.hd.api.domain.AuthModule;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.management.user.domain.User;

public interface RoleApiService {
	Set<AuthRole> getAuthRole(User user,boolean showOriginRole);
	Set<AuthModule> getAuthModule(User user);/**
	 * 判断人员是否具有某角色(经过变量表配置的)权限,,只要存在其中之一即返true
	 * */
	boolean checkRole(User user, String... roleType);

/**
 * 判断人员是否具有某角色(经过变量表配置的)权限,入参为变量表的常量
 * */
	public boolean checkAllRole(User user,String... roleType);
	/**
	 * 判断人员是否具有某角色(经过变量表配置的)权限,入参为变量表的常量
	 * */
		public boolean checkAllRole(Integer userId,String... roleType);
}
