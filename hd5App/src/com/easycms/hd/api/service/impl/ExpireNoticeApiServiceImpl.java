package com.easycms.hd.api.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.domain.EnpowerExpireNotice;
import com.easycms.hd.api.response.EnpowerPageResult;
import com.easycms.hd.api.response.ExpireNoticeExtraResult;
import com.easycms.hd.api.response.ExpireNoticeFilesResult;
import com.easycms.hd.api.response.ExpireNoticePageResult;
import com.easycms.hd.api.response.ExpireNoticeResult;
import com.easycms.hd.api.service.ExpireNoticeApiService;
import com.easycms.hd.conference.domain.ExpireNotice;
import com.easycms.hd.conference.domain.ExpireNoticeFiles;
import com.easycms.hd.conference.service.ExpireNoticeFilesService;
import com.easycms.hd.conference.service.ExpireNoticeService;

@Service("expireNoticeApiService")
public class ExpireNoticeApiServiceImpl implements ExpireNoticeApiService {
	@Autowired
	private ExpireNoticeService expireNoticeService;
	@Autowired
	private ExpireNoticeFilesService expireNoticeFilesService;

	@Override
	public ExpireNoticeResult tranferToResult(ExpireNotice expireNotices, boolean getFiles) {
		if (null != expireNotices) {
			ExpireNoticeResult expireNoticeResult = new ExpireNoticeResult();
			expireNoticeResult.setApprovelTime(expireNotices.getApprovelTime());
			expireNoticeResult.setContent(expireNotices.getContent());
			expireNoticeResult.setDepartment(expireNotices.getDepartment());
			expireNoticeResult.setId(expireNotices.getId());
			expireNoticeResult.setNo(expireNotices.getNo());
			expireNoticeResult.setPublishTime(expireNotices.getPublishTime());
			expireNoticeResult.setStatus(expireNotices.getStatus());
			expireNoticeResult.setWriteTime(expireNotices.getWriteTime());
			expireNoticeResult.setTitle(expireNotices.getTitle());
			if (getFiles) {
				List<ExpireNoticeFiles> expireNoticeFiles = expireNoticeFilesService.findByNoticeId(expireNotices.getId());
				if (null != expireNoticeFiles && !expireNoticeFiles.isEmpty()) {
					List<ExpireNoticeFilesResult> files = this.tranferToResult(expireNoticeFiles);
					expireNoticeResult.setFiles(files);
				}
			}
			ExpireNoticeExtraResult extra = new ExpireNoticeExtraResult();
			//TODO 统计所有文件领用情况的和
			expireNoticeResult.setExtra(extra);
			
			return expireNoticeResult;
		}
		return null;
	}

	@Override
	public EnpowerPageResult<EnpowerExpireNotice> getExpireNoticePageResult(EnpowerExpireNotice condition, Page<EnpowerExpireNotice> page) {
		EnpowerPageResult<EnpowerExpireNotice> expireNoticePageResult = new EnpowerPageResult<EnpowerExpireNotice>();
		
//		EnpowerExpireNotice condition = new EnpowerExpireNotice();
		Map<String,Object> additionalCondition = new HashMap<String,Object>();
		
		page = expireNoticeService.findByPage(condition, additionalCondition, page);
		if (null != page.getDatas()) {
			expireNoticePageResult.setPageCounts(page.getPageCount());
			expireNoticePageResult.setPageNum(page.getPageNum());
			expireNoticePageResult.setPageSize(page.getPagesize());
			expireNoticePageResult.setTotalCounts(page.getTotalCounts());
			expireNoticePageResult.setData(page.getDatas());
		}
		
		return expireNoticePageResult;
	}

	@Override
	public List<ExpireNoticeResult> tranferToResult(List<ExpireNotice> expireNotices, boolean getFiles) {
		if (null != expireNotices && !expireNotices.isEmpty()) {
			List<ExpireNoticeResult> expireNoticeResults = expireNotices.stream().map(expireNotice -> {
				return tranferToResult(expireNotice, getFiles);
			}).collect(Collectors.toList());
			
			return expireNoticeResults;
		}
		return null;
	}

	@Override
	public ExpireNoticeFilesResult tranferToResult(ExpireNoticeFiles expireNoticeFiles) {
		if (null != expireNoticeFiles) {
			ExpireNoticeFilesResult expireNoticeFilesResult = new ExpireNoticeFilesResult();
			expireNoticeFilesResult.setId(expireNoticeFiles.getId());
			expireNoticeFilesResult.setNo(expireNoticeFiles.getNo());
			expireNoticeFilesResult.setStatus(expireNoticeFiles.getStatus());
			expireNoticeFilesResult.setFilename(expireNoticeFiles.getFilename());
			expireNoticeFilesResult.setFilepath(expireNoticeFiles.getFilepath());
			expireNoticeFilesResult.setNumber(expireNoticeFiles.getNumber());
			expireNoticeFilesResult.setReceiver(expireNoticeFiles.getReceiver());
			expireNoticeFilesResult.setVersion(expireNoticeFiles.getVersion());
			return expireNoticeFilesResult;
		}
		return null;
	}

	@Override
	public List<ExpireNoticeFilesResult> tranferToResult(List<ExpireNoticeFiles> expireNoticeFiles) {
		if (null != expireNoticeFiles && !expireNoticeFiles.isEmpty()) {
			List<ExpireNoticeFilesResult> expireNoticeFilesResults = expireNoticeFiles.stream().map(expireNoticeFile -> {
				return tranferToResult(expireNoticeFile);
			}).collect(Collectors.toList());
			
			return expireNoticeFilesResults;
		}
		return null;
	}

	@Override
	public ExpireNoticeResult getExpireNoticeById(Integer id) {
		if (null != id) {
			return this.tranferToResult(expireNoticeService.findById(id), true);
		}
		return null;
	}
	
}
