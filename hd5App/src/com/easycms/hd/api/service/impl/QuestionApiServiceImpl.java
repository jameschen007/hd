package com.easycms.hd.api.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.ArrayUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.baseservice.question.QuestionTransForResult;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.enums.question.QuestionStatusDbMap;
import com.easycms.hd.api.response.UserResult;
//import com.easycms.hd.api.response.question.RollingQuestionAssignResult;
import com.easycms.hd.api.response.question.RollingQuestionPageDataResult;
import com.easycms.hd.api.response.question.RollingQuestionResult;
import com.easycms.hd.api.response.question.RollingQuestionSolverResult;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.plan.dao.RollingPlanDao;
import com.easycms.hd.problem.dao.ProblemFileDao;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionProcess;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionProcessService;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>     <br>
 * <b>创建时间：</b>2017-09-15 下午05:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Service("questionApiService")
public class QuestionApiServiceImpl implements QuestionApiService {
	
	public static final Logger logger = Logger.getLogger(QuestionApiServiceImpl.class);
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;
	@Autowired
	private RollingQuestionService questionService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private RollingQuestionSolverService questionSolverService;
	@Autowired
	private RollingQuestionProcessService questionProcessService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private ProblemFileDao problemFileDao;
	@Autowired
	private RollingPlanDao rollingPlanDao;
	@Autowired
	private RollingPlanApiService rollingPlanApiService;
	@Autowired
	private RoleService roleService;
	
	//获取所有跟question相关的分页信息
	@Override
	public RollingQuestionPageDataResult getQuestionPageDataResult(Page<RollingQuestion> page, Integer loginId, Integer userId,String questionStatus,String type,String keyword,Integer usingRoleId){
//		//主表:问题表, 子表 解决人表
////	主表	'状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决'
////	子表	'班长状态:pre待指派、done已指派，技术状态：pre待处理、done已处理、unsolved仍未解决、solved已解决',
//		if("zz".equals(roletype)){
//			//组长,直接查询 问题表即可,要主表是自已创建的
//			//状态:pre待解决、undo待确认、done已解决、unsolved仍未解决
//			//待解决 查 pre+ unsolved两个状态
//		}else if("bz".equals(roletype)){
//			//班长需要关联查询问题解决人表,要看子表属于自已的
//			
//			//待指派: 主表(pre待解决),子表(pre待指派)
//			//待解决: 主表(pre待解决,undo待确认+unsolved仍未解决),子表(done已指派)
//			//已解决: 主表(done已解决),子表(done已指派)
//		}else if("js".equals(roletype)){
//			
//			//技术: 需要关联查询问题解决人表,要看子表属于自已的
//			
//			//查主表
//			
//		}
		//协调列表
//		//待指派: 子表(pre待指派)
		//已指派: 子表(done已指派)
		
		RollingQuestionPageDataResult questionPageResult = new RollingQuestionPageDataResult();
		User he = userService.findUserById(loginId);
		List<Role> roles = he.getRoles();
//		 = baseRequestForm.getRoleId() ;
		if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
			roles = roleService.findListById(roles,usingRoleId);
		}
		List<Task> tasks = null;
		List<RollingQuestionSolver> solvers = new ArrayList<RollingQuestionSolver>();
		Page<RollingQuestion> questions = new Page<RollingQuestion>();
		RollingQuestion mainCondition = new RollingQuestion();
		RollingQuestionSolver childCondition = new RollingQuestionSolver();
		//是否是技术中心的领导，即是否是各主管，如管道主管、电气主管
		boolean isParent = false ;
		switch (questionStatus) {
		case "NEED_SOLVE"://QuestionStatusEnum_ZZ.NEED_SOLVE.toString():
//			Set<String> roleNameList0 = variableSetService.findKeyByValue(he.getDepartment().getName(), "departmentType");
//			if (roleNameList0.contains("technologyManagement")) {
//dsf
//				if (null != roles) {
//					for (Role role : roles) {
//						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
//						if (roleNameList.contains(ConstantVar.supervisor)) {
//							isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
//							//如果是技术管理中心的 未能解决的问题
//							childCondition.setSolver(he.getId());
//							childCondition.setSolverRole(3);
//							mainCondition.setDescribe("solverParentList");
//							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
//							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
//						} 
//						
//					}
//				}
//				if(isParent == false ){//不是技术人员领导，是技术人员
//					//如果是技术管理中心的 未能解决的问题
//					childCondition.setSolver(he.getId());
//					childCondition.setSolverRole(3);
//					childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
//					questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
//				}
//			} else{
				if (null != roles) {
					for (Role role : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
						if (roleNameList.contains("monitor")) {
							
							if(userId!=null){//如果是班长，根据组长的userId看组长的问题列表，仅查主表状态即可
								//待解决: 主表(pre待解决+undo待确认+unsolved仍未解决),子表(done已指派)
								mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
										+","+QuestionStatusDbEnum.undo.toString()
										+","+QuestionStatusDbEnum.unsolved.toString());
								//现在没有将已指派的过滤掉。
								questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
								
							}else{//以下可能是班长看自已的使用
								//如果是班长
								//组长： 待解决
								//待解决: 主表(pre待解决+undo待确认+unsolved仍未解决),子表(done已指派)
								mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
										+","+QuestionStatusDbEnum.undo.toString()
										+","+QuestionStatusDbEnum.unsolved.toString());
		//						状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决
								
								childCondition.setSolver(he.getId());
								childCondition.setSolverRole(1);
		//						班长状态:pre待指派、assign已指派、reply已回执
								childCondition.setStatus(QuestionStatusDbEnum.assign.toString()
										+","+QuestionStatusDbEnum.reply.toString());

//								//如果是班长
//								//组长： 待解决
//								//待解决: 主表(pre待解决+undo待确认+unsolved仍未解决),子表(done已指派)
//								mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
//										+","+QuestionStatusDbEnum.undo.toString()
//										+","+QuestionStatusDbEnum.unsolved.toString());
//		//						状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决
//								
//								childCondition.setSolver(he.getId());
//								childCondition.setSolverRole(1);
		//						班长状态:pre待指派、assign已指派
								childCondition.setStatus(QuestionStatusDbEnum.assign.toString());
								
								questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							}
						} else if (roleNameList.contains("team")) {
							//如果是组长
							//组长： 待解决
							//待解决 查 pre+ unsolved两个状态
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.pre.toString()
									+","+QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
						} else if (roleNameList.contains(ConstantVar.solver)) {
							//不是技术人员领导，是技术人员
							//如果是技术管理中心的 未能解决的问题
//							mainCondition.setStatus(QuestionStatusDbEnum.unsolved.toString()); 技术人员查　组长没有接受的问题，不用判断主表，这个时候有可能已经回到pre状态了。
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(3);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							
						} else if (roleNameList.contains(ConstantVar.supervisor)) {
							//技术人员领导
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(4);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver3)) {
							//技术人员领导 +1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(5);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver4)) {
							//技术人员领导 +1+1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(6);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver5)) {
							//技术人员领导 +1+1+1
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(7);
							childCondition.setStatus(QuestionStatusDbEnum.unsolved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
							
						}
						
					}
				}
//			}
			break;
		case "NEED_SOLVED":// 
			//队长列表， 
			//如果是队长 //队长 查询未解决  '状态:pre待解决v、undo待确认v、unsolved仍未解决v、solved已解决'
			mainCondition.setOwner(he);
			mainCondition.setStatus(ArrayUtility.toCommaString(new Object[]{
					QuestionStatusDbEnum.pre,QuestionStatusDbEnum.undo,QuestionStatusDbEnum.unsolved}));
			mainCondition.setDescribe("captainList");
			questions = questionService.findPage(mainCondition,null,page,type,userId,keyword);
			break;
		case "NEED_CONFIRM"://QuestionStatusEnum_ZZ.NEED_CONFIRM.toString():
				//组长待确认
				mainCondition.setOwner(he);
				mainCondition.setStatus(QuestionStatusDbEnum.undo.toString());
				questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
			
			break;
		case "SOLVED":// QuestionStatusEnum_ZZ.SOLVED.toString():
			//组长已解决
//			Set<String> roleNameList3 = variableSetService.findKeyByValue(he.getDepartment().getName(), "departmentType");
//			if (roleNameList3.contains("technologyManagement")) {
//				if (null != roles) {
//					for (Role role : roles) {
//						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
//						if (roleNameList.contains(ConstantVar.supervisor)) {
//							isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
//							//如果是技术管理中心的 未能解决的问题
//							childCondition.setSolver(he.getId());
//							childCondition.setSolverRole(3);
//							mainCondition.setDescribe("solverParentList");
//							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
//							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
//						} 
//					}
//				}
//				if(isParent == false ){//不是技术人员领导，是技术人员
//
//					//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
//					mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
//					
//					childCondition.setSolver(he.getId());
//					childCondition.setSolverRole(3);
//					childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
//					
//					questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
//					
//				}
//			} else{

				if (null != roles) {
					for (Role role : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
						if (roleNameList.contains("monitor")) {
							//如果是班长 //班长已解决
							//已解决: 主表(solved已解决),并且带上组长userid即可
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
//							childCondition.setSolver(he.getId());
//							childCondition.setSolverRole(1);
//							childCondition.setStatus(QuestionStatusDbEnum.assign.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
						} else if (roleNameList.contains("team")) {
							//如果是组长 //组长已解决
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
						}else if (roleNameList.contains("captain")) {
							//如果是队长 //队长已解决  '状态:pre待解决v、undo待确认v、unsolved仍未解决v、solved已解决'
							mainCondition.setOwner(he);
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							mainCondition.setDescribe("captainList");
							questions = questionService.findPage(mainCondition,null,page,type,userId,keyword);
						}else if (roleNameList.contains(ConstantVar.solver)) {
//							//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(3);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
							
						}else if (roleNameList.contains(ConstantVar.supervisor)) {
//							//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(4);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver3)) {
//							//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(5);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver4)) {
//							//如果是技术管理中心的 已解决的问题，暂时不考虑我没有解决，但是被别人解决这种情况
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(6);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
							
						}else if (roleNameList.contains(ConstantVar.solver5)) {
//							//
							mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							childCondition.setSolver(he.getId());
							childCondition.setSolverRole(7);
							childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
							
							questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
							
						}
					}
				}
//			}

			break;
		case "NEED_ASSIGN":// QuestionStatusEnum_BZ.NEED_ASSIGN.toString():
			boolean isCoordinate=false;
			//存在指派的角色有班长和协调
			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.COORDINATOR)) {
						isCoordinate=true;
//						//协调列表 待指派: 子表(pre待指派)
						mainCondition.setCoordinate(he);;
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(2);//'1班长，2队部协,3技术',
						childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
						questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
					}
				}
			}
			if(isCoordinate==false){

				//班长查列表 待指派: 主表(pre待解决),子表(pre待指派)
				mainCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(1);
				childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
			}
			
			break;
		case "PRE":// QuestionStatusEnum_JS.PRE.toString():

			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.supervisor) ||roleNameList.contains(ConstantVar.solver3) ||roleNameList.contains(ConstantVar.solver4)||roleNameList.contains(ConstantVar.solver5) ||roleNameList.contains(ConstantVar.solver) ) {
						isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
						int c = 3;
						if(roleNameList.contains(ConstantVar.solver)){
							c=3;
						}
						if(roleNameList.contains(ConstantVar.supervisor)){
							c=4;
						}
						if(roleNameList.contains(ConstantVar.solver3)){
							c=5;
						}
						if(roleNameList.contains(ConstantVar.solver4)){
							c=6;
						}
						if(roleNameList.contains(ConstantVar.solver5)){
							c=7;
						}
						//如果是技术管理中心的 未能解决的问题
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(c);
						mainCondition.setDescribe("solverParentList");
						childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
						questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
					} 
					
				}
			}
			if(isParent == false ){//不是技术人员领导，是技术人员

				//技术: 需要关联查询问题解决人表,要看子表属于自已的
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(3);
				childCondition.setStatus(QuestionStatusDbEnum.pre.toString());
				questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
			}
			break;
		case "DONE":// QuestionStatusEnum_JS.PRE.toString():

			if (null != roles) {
				for (Role role : roles) {
					Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
					if (roleNameList.contains(ConstantVar.supervisor)) {
						isParent = true ;//是技术人员的领导，即技术主管，如管道主管、机械主管等
						//如果是技术管理中心的 未能解决的问题
						childCondition.setSolver(he.getId());
						childCondition.setSolverRole(3);
						mainCondition.setDescribe("solverParentList");
						childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
						questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
					} 
					
				}
			}
			if(isParent == false ){//不是技术人员领导，是技术人员

				//技术: 需要关联查询问题解决人表,要看子表属于自已的
				childCondition.setSolver(he.getId());
				childCondition.setSolverRole(3);
				childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
				questions = questionService.findPage(mainCondition,childCondition,page,type,null,keyword);
			}
			break;
		case "ASSIGNED"://协调列表
//			//协调列表 已指派: 子表(assign已指派)
			mainCondition.setCoordinate(he);;
			childCondition.setSolver(he.getId());
			childCondition.setSolverRole(2);//'1班长，2队部协,3技术',
			childCondition.setStatus(QuestionStatusDbEnum.assign.toString());
			questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
			break;
		//我的问题列表
		case "MYREPLY":
			//我已回复的问题
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.reply.toString());
			questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
			break;
		case "MYUNSOLVED":
			//我未解决的问题
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.pre.toString()+","+QuestionStatusDbEnum.unsolved.toString());//增加pre状态　20180130
			questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
			break;
		case "MYSOLVED":
			//我已解决的问题
			mainCondition.setStatus(QuestionStatusDbEnum.solved.toString());
			childCondition.setSolver(he.getId());
			childCondition.setStatus(QuestionStatusDbEnum.solved.toString());
			questions = questionService.findPage(mainCondition,childCondition,page,type,userId,keyword);
			break;
		default:
			//技术: 需要关联查询问题解决人表,要看子表属于自已的
			break;
		}
		questionPageResult.setPageCounts(questions.getPageCount());
		questionPageResult.setPageNum(questions.getPageNum());
		questionPageResult.setPageSize(questions.getPagesize());
		questionPageResult.setTotalCounts(questions.getTotalCounts());
		if (null != questions.getDatas()) {
			questionPageResult.setData(questions.getDatas().stream().map(question -> {
				RollingQuestionResult questionResult = transferForList(question,false,loginId);
				return questionResult;
			}).collect(Collectors.toList()));
		}
		
		return questionPageResult;
	}
	
	private Page<RollingQuestion> getConfirmedProblems(String userId, Page<RollingQuestion> page) {
		List<Task> tasks;
		tasks = taskService.createTaskQuery().taskAssignee(userId).list();
		if (tasks != null && tasks.size() > 0) {
			Integer[] ids = new Integer[tasks.size()];
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				ProcessInstance pi = runtimeService
						.createProcessInstanceQuery()
						.processInstanceId(task.getProcessInstanceId())
						.singleResult();
				ids[i] = Integer.parseInt(pi.getBusinessKey());
			}
	
//			return questionService.findConfirmRollingQuestionsByIds(ids, -1, page);
		}
	
		return page;
	}
	
	/**
	 * 转换为接口返回数据结果
	 * @param question
	 * @return
	 */
	public RollingQuestionResult transferForList(RollingQuestion question,boolean isShowChildAndFile,Integer loginId){
		if(question==null){
			return null ;
		}
		QuestionTransForResult trans = new QuestionTransForResult();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		//查询 问题被指派给谁处理了。
		RollingQuestionSolver condi = new RollingQuestionSolver();
		condi.setSolverRole(3);
		condi.setQuestion(question);
		RollingQuestionSolver solv = null ;
		UserResult designee = new UserResult();
		UserResult coordinate = new UserResult();
		UserResult createUser = new UserResult();
		if(question.getAssign()==null){
			//之前 从子表中查被指派人，直接使用主表中的指派人字段
//			List<RollingQuestionSolver> solvs = questionSolverService.findByQuestionIdAndSolverType(question.getId(),3);
//			if(solvs==null || solvs.size()==0){
//				logger.error("查询问题时没有查到待解决人。");
//			}else 		
//			if( solvs.size()>1 ){
//				logger.error("查询问题时出现错误 ，待解决人存在多个吗？。");
//			}else{
//				solv = solvs.get(0);
//				User designeeUser= userService.findUserById(solv.getSolver());
//					designee.setId(designeeUser.getId());
//					designee.setUsername(designeeUser.getName());;
//					designee.setRealname(designeeUser.getRealname());
//			}
		}else{
			designee.setId(question.getAssign().getId());
			designee.setUsername(question.getAssign().getName());;
			designee.setRealname(question.getAssign().getRealname());
		}
		if(question.getCoordinate()!=null){
			User coordinateUser= question.getCoordinate();
			if(coordinateUser!=null && coordinateUser.getId()!=null){

				coordinate.setId(coordinateUser.getId());
				coordinate.setUsername(coordinateUser.getName());
				coordinate.setRealname(coordinateUser.getRealname());
			}
		}
		if(question.getOwner()!=null){
			User own= question.getOwner();
			createUser.setId(own.getId());
			createUser.setUsername(own.getName());
			createUser.setRealname(own.getRealname());
		}

		RollingQuestionResult questionResult = new RollingQuestionResult();
		questionResult.setId(question.getId());
		questionResult.setRollingPlanId(question.getRollingPlanId());
		questionResult.setQuestionType(question.getQuestionType());
		questionResult.setStatus(question.getStatus());
		questionResult.setDescribe(question.getDescribe());
		questionResult.setReason(question.getReason());
		questionResult.setTimeout(question.getTimeout());
		questionResult.setCoordinate(coordinate);//被指派协调人（班长指派的谁）
		//提问日期
		questionResult.setQuestionTime(sdf.format(question.getQuestionTime()));
		questionResult.setDesignee(designee);
		questionResult.setCreateUser(createUser);

		//返回当前登录人的小状态 loginId
		List<RollingQuestionSolver> solverList = questionSolverService.findByQuestionIdAndUserId(question.getId(),loginId);
		List<RollingQuestionSolverResult> solverStatusList = new ArrayList<RollingQuestionSolverResult>();
		for(RollingQuestionSolver solvb:solverList){
			RollingQuestionSolverResult result = transferTo3(solvb);
			solverStatusList.add(result);
			
			if(QuestionStatusDbEnum.pre.toString().equals(solvb.getStatus())
					&& (QuestionStatusDbEnum.pre.toString().equals(question.getStatus())||QuestionStatusDbEnum.unsolved.toString().equals(question.getStatus()))){
				
				
				//当前登录人是否能直接处理
				questionResult.setHandle(true);
			}
		}
		
		
		questionResult.setSolverList(solverStatusList);

		if(isShowChildAndFile == false){
			questionResult.setFileSize(problemFileDao.findCountByProblemId(question.getId()));
		}else{
			List<ProblemFile> files = problemFileDao.findByProblemId(question.getId());
			if(files!=null && files.size()>0){
				questionResult.setFiles(trans.problemFile(files));
				questionResult.setFileSize(files.size());
			}
			
			if(question.getChildren()!=null && question.getChildren().size()>0){
				List<RollingQuestionResult> newList = new ArrayList<RollingQuestionResult>();
				List<RollingQuestionResult> answerList = new ArrayList<RollingQuestionResult>();
				for(RollingQuestion ques:question.getChildren()){

					if(QuestionApiService.teamAnswer.equals(ques.getStatus())){

						RollingQuestionResult result = transferForList(ques,isShowChildAndFile,loginId);

						answerList.add(result);
					}else{

						RollingQuestionResult result = transferForList(ques,isShowChildAndFile,loginId);

						newList.add(result);
					}
				}
				questionResult.setFeedback(newList);
				questionResult.setTeamAnswer(answerList);
			} 
			
		}
		
		List<RollingQuestionProcess> opList = questionProcessService.findByQuestionId(question.getId());
		
		questionResult.setOpList(transferToList4(opList));
		
		questionResult.setRollingPlan(rollingPlanApiService.rollingPlanTransferToSixLevelRollingPlan(rollingPlanDao.findById(question.getRollingPlanId())));
		
		return questionResult;
	}

//	/**
//	 * 转换为接口返回数据结果  是指派需要的
//	 * RollingQuestionAssignResult 是指派需要的
//	 * @param question
//	 * @return
//	 */
//	public RollingQuestionAssignResult transferTo2(RollingQuestion question){
//
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//		//查询 问题被指派给谁处理了。
//		RollingQuestionSolver condi = new RollingQuestionSolver();
//		condi.setSolverRole(2);
//		condi.setQuestion(question);
//		RollingQuestionSolver solv = null;
//		List<RollingQuestionSolver> solvs = questionSolverService.findByQuestionIdAndType(question.getId(), 2);
//		User designeeUser=null;
//		UserResult designee = new UserResult();
//		if(solvs==null || solvs.size()==0){
//			
//		}else if(solvs.size()>1){
//			logger.error("待解决有怎么有多个呢？");
//		}
//		else{
//			solv= solvs.get(0);
//		}
//		if(solv!=null){
//			designeeUser = userService.findUserById(solv.getSolver());
//			designee.setId(designeeUser.getId());
//			designee.setUsername(designeeUser.getName());;
//			designee.setRealname(designeeUser.getRealname());
//		}
//
//		RollingQuestionAssignResult questionResult = new RollingQuestionAssignResult();
//		questionResult.setId(question.getId());
//		questionResult.setRollingPlanId(question.getRollingPlanId());
//		questionResult.setQuestionType(question.getQuestionType());
//		questionResult.setStatus(question.getStatus());
//		questionResult.setDescribe(question.getDescribe());
//		//提问日期
//		questionResult.setQuestionTime(sdf.format(question.getQuestionTime()));
//		questionResult.setDesignee(designee);
//		return questionResult;
//	}
//	

	/**
	 * 处理人小状态返回
	 * @param solver
	 * @return
	 */
	public RollingQuestionSolverResult transferTo3(RollingQuestionSolver solver){

		RollingQuestionSolverResult ret = new RollingQuestionSolverResult();
		ret.setId(solver.getId());
		ret.setStatus(solver.getStatus());
		ret.setReason(solver.getReason());
		
		ret.setCreateTime(solver.getCreateTime());;
		
		ret.setStatusName(QuestionStatusDbMap.map.get(solver.getStatus()));

		String reason = solver.getReason();
		String ureason = solver.getUnableReason();
		
		ret.setReason( (reason==null?"":reason) + (ureason==null?"":ureason) );
		
		String realname = "";
		if(solver.getSolver()!=null){
			Integer sol = solver.getSolver();
			User u = userService.findUserById(sol);
			if(u!=null){
				realname = u.getRealname();
			}
		}
		
		ret.setRealname(realname);
		
		return ret ;
	}
	/**
	 * 处理人小状态返回
	 * @param solver
	 * @return
	 */
	public RollingQuestionSolverResult transferTo3(RollingQuestionProcess solver){

		RollingQuestionSolverResult ret = new RollingQuestionSolverResult();
		ret.setId(solver.getId());
		ret.setStatus(solver.getStatus());
		ret.setReason(solver.getReason());
		
		ret.setCreateTime(solver.getCreateTime());;
		
		ret.setStatusName(QuestionStatusDbMap.map.get(solver.getStatus()));

		String reason = solver.getReason();
		String ureason = solver.getUnableReason();
		
		ret.setReason( (reason==null?"":reason) + (ureason==null?"":ureason) );
		
		String realname = "";
		if(solver.getSolver()!=null){
			Integer sol = solver.getSolver();
			User u = userService.findUserById(sol);
			if(u!=null){
				realname = u.getRealname();
			}
		}
		
		ret.setRealname(realname);
		
		return ret ;
	}

	/**处理列表　转换*/
	public List<RollingQuestionSolverResult> transferToList3(List<RollingQuestionSolver> solvers){
		
		List<RollingQuestionSolverResult> solverStatusList = new ArrayList<RollingQuestionSolverResult>();
		for(RollingQuestionSolver solvb:solvers){
			

			
			RollingQuestionSolverResult result = transferTo3(solvb);
			
			
			solverStatusList.add(result);
		}
		
		return solverStatusList ;
	}

	/**处理列表　转换*/
	public List<RollingQuestionSolverResult> transferToList4(List<RollingQuestionProcess> solvers){
		
		List<RollingQuestionSolverResult> solverStatusList = new ArrayList<RollingQuestionSolverResult>();
		for(RollingQuestionProcess solvb:solvers){
			

			
			RollingQuestionSolverResult result = transferTo3(solvb);
			
			
			solverStatusList.add(result);
		}
		
		return solverStatusList ;
	}
	
}
