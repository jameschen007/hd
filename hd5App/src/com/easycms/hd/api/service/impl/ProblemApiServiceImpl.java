package com.easycms.hd.api.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ProblemTypeEnum;
import com.easycms.hd.api.response.ProblemPageResult;
import com.easycms.hd.api.response.ProblemResult;
import com.easycms.hd.api.service.ProblemApiService;
import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.domain.ProblemConcernman;
import com.easycms.hd.problem.domain.ProblemData;
import com.easycms.hd.problem.domain.ProblemSolver;
import com.easycms.hd.problem.service.ProblemConcernmanService;
import com.easycms.hd.problem.service.ProblemService;
import com.easycms.hd.problem.service.ProblemSolverService;

@Service("problemApiService")
public class ProblemApiServiceImpl implements ProblemApiService {
	@Autowired
	private TaskService taskService;
	@Autowired
	private ProblemService problemService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private ProblemSolverService problemSolverService;
	@Autowired
	private ProblemConcernmanService problemConcernmanService;
	
	//获取所有跟Problem相关的分页信息
	@Override
	public ProblemPageResult getProblemPageDataResult(Page<Problem> page, Integer userId, ProblemTypeEnum problemType) {
		ProblemPageResult problemPageResult = new ProblemPageResult();
		List<Task> tasks = null;
		List<ProblemSolver> solvers = new ArrayList<ProblemSolver>();
		Page<Problem> problems = new Page<Problem>();
		switch (problemType) {
		//所有问题
		case ALL:
			problems = problemService.findAll(page);
			break;
		// 待解决问题
		case NEED_SOLVE:
//			tasks = taskService.createTaskQuery().taskCandidateUser(userId.toString()).list();
//			if (tasks != null && tasks.size() > 0) {
//				Integer[] ids = new Integer[tasks.size()];
//				for (int i = 0; i < tasks.size(); i++) {
//					Task task = tasks.get(i);
//					ProcessInstance pi = runtimeService
//							.createProcessInstanceQuery()
//							.processInstanceId(task.getProcessInstanceId())
//							.singleResult();
//					ids[i] = Integer.parseInt(pi.getBusinessKey());
//				}
//				problems = problemService.findByIdsAndStatus(ids, ProblemData.IS_NOT_OK, page);
//			}
			problems = problemService.findAll(page);
			break;
		// 未能解决
		case COULD_NOT_SOLVED:
			solvers = problemSolverService.findByUserId(userId.toString());
			if (solvers != null && solvers.size() > 0) {

				Integer[] ids = new Integer[solvers.size()];
				for (int i = 0; i < solvers.size(); i++) {
					ids[i] = solvers.get(i).getProblemid();
				}
				problems = problemService.findByIdsAndStatus(ids, ProblemData.IS_NOT_OK, page);
			}
			break;
		// 已经解决
		case SOLVED:
//			solvers = problemSolverService.findByUserId(userId.toString());
//			if (solvers != null && solvers.size() > 0) {
//
//				Integer[] ids = new Integer[solvers.size()];
//				for (int i = 0; i < solvers.size(); i++) {
//					ids[i] = solvers.get(i).getProblemid();
//				}
//				problems = problemService.findByIdsAndStatus(ids, ProblemData.IS_OK, page);
//			}
			problems = problemService.findAll(page);
			break;
		// 发起的问题
		case RAISED_PROBLEM:
			problems = problemService.findByUserId(userId.toString(), page);
			break;
		// 待确认的问题
		case NEED_CLEAR_PROBLEM:
			problems = this.getConfirmedProblems(userId.toString(), page);
			break;
		// 关注的问题
		case WATCHED_PROBLEM:
			List<ProblemConcernman> mans = problemConcernmanService.findByUserId(userId);
			if (mans != null && mans.size() > 0) {
				Integer[] ids = new Integer[mans.size()];
				for (int i = 0; i < ids.length; i++) {
					ids[i] = mans.get(i).getProblemid();
				}
				problems = problemService.findByIds(ids, page);
			}
			break;
		default:
			break;
		}
		problemPageResult.setPageCounts(problems.getPageCount());
		problemPageResult.setPageNum(problems.getPageNum());
		problemPageResult.setPageSize(problems.getPagesize());
		problemPageResult.setTotalCounts(problems.getTotalCounts());
		if (null != problems.getDatas()) {
			problemPageResult.setData(problems.getDatas().stream().map(problem -> {
				ProblemResult problemResult = new ProblemResult();
				try {
					BeanUtils.copyProperties(problemResult, problem);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				return problemResult;
			}).collect(Collectors.toList()));
		}
		
		return problemPageResult;
	}
	
	private Page<Problem> getConfirmedProblems(String userId, Page<Problem> page) {
		List<Task> tasks;
		tasks = taskService.createTaskQuery().taskAssignee(userId).list();
		if (tasks != null && tasks.size() > 0) {
			Integer[] ids = new Integer[tasks.size()];
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				ProcessInstance pi = runtimeService
						.createProcessInstanceQuery()
						.processInstanceId(task.getProcessInstanceId())
						.singleResult();
				ids[i] = Integer.parseInt(pi.getBusinessKey());
			}
	
			return problemService.findConfirmProblemsByIds(ids, -1, page);
		}
	
		return page;
	}
}
