package com.easycms.hd.api.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.DateUtility;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.JobService;

import lombok.extern.slf4j.Slf4j;

@Service("notificationAlarmService")
@Slf4j
public class NotificationAlarmServiceImpl implements JobService, Serializable {
	private static final long serialVersionUID = -5690901744850949508L;
	
	private int notificationId;
	private int userId;
	
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private JPushService jPushService;
	@Autowired
	private UserService userService;

	@SuppressWarnings("unchecked")
	@Override
	public void doJob(Object jobDataMap) {
		log.info("开始执行通知提醒任务.");
		Map<String, Object> jobMap = (Map<String, Object>) jobDataMap;
		
		this.userId = (Integer) jobMap.get("userId");
		this.notificationId = (Integer) jobMap.get("notificationId");
		Notification notification = notificationService.findById(notificationId);
		User user = userService.findUserById(userId);
		
		if (null != notification && null != user){
			if (notification.getType().equals("SEND")) {
				String message = user.getRealname() + " 向您发起通告提醒[" + notification.getSubject() + "], 通告时间为[" + DateUtility.getDateString(notification.getStartTime(), "yyyy-MM-dd HH:mm:ss") + "]";
	            
				List<User> participants = notification.getParticipants();
				participants = participants.stream().filter(u -> {
					return !u.getId().equals(user.getId());
				}).collect(Collectors.toList());
				
	            JPushExtra extra = new JPushExtra();
	    		extra.setCategory(JPushCategoryEnums.NOTIFICATION.name());
	    		jPushService.pushByUser(extra, message, "通知", participants);
			}
		}
	}
}
