package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.hd.api.enpower.domain.EnpowerConstantVar;
import com.easycms.hd.api.enpower.request.EnpowerRequestFileInvalidNotice;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionComplete;
//import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionComplete;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionSend;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelCheck;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutCheck;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialTodayOutList;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialTodayStoreList;
import com.easycms.hd.api.enpower.request.EnpowerRequestQualityQuestionCloseSend;
import com.easycms.hd.api.enpower.request.EnpowerRequestQualityQuestionSend;
import com.easycms.hd.api.enpower.request.EnpowerRequestQualityQuestionSending;
import com.easycms.hd.api.enums.enpower.EnpowerHSEStatus;
import com.easycms.hd.api.service.EnpowerApiService;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemFile;
import com.easycms.hd.hseproblem.service.HseProblemFileService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.qualityctrl.domain.QualityControlFile;
import com.easycms.hd.qualityctrl.service.QualityControlFileService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.UserService;

/**
 * 与Enpower交互的相关功能
 */
@Service("enpowerApiService")
public class EnpowerApiServiceImpl implements EnpowerApiService {
	
	private static Logger log = Logger.getLogger(EnpowerApiServiceImpl.class);
	@Autowired
	private DepartmentService departmentService ;
	@Autowired
	private UserService userService ;
	@Autowired
	private HseProblemFileService hseProblemFileService;
	@Autowired
	private QualityControlFileService qualityControlFileService;
	@Autowired
	private VariableSetService variableSetService;
	
    /**
     * 文件失效通知单接口 推送接口名: 文件失效通知单接口
     * 
     */
    public List<EnpowerRequestFileInvalidNotice> findEnpowerFileInvalidNoticeById(RollingPlan plan,Integer rollingPlanId) {

        return null;
    }

    /**
     * 责任单位整改完成接口 推送接口名: 责任单位整改完成接口 应该叫状态变更
     * 
     */
    @Override
    public List<EnpowerRequestHSEQuestionComplete> findEnpowerHSEQuestionCompleteBy(Map<String,Object> inMap,String enpFlag,Integer solveUser){

    	EnpowerRequestHSEQuestionComplete hse = new EnpowerRequestHSEQuestionComplete();

    	String APPID = "";
    	if(inMap==null){
    		throw new RuntimeException("入参为空错误！！");
    	}
    	if(inMap.get("APPID")!=null){
    		APPID = String.valueOf(inMap.get("APPID"));
    	}
    	
//		APP主键
		log.debug(APPID);
		hse.setAPPID(APPID);
//		状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
		/**
		 * 
 FLAG反填如下：  when '0' then '编制中'  when '1' then '未接收'  when '2' then '已接收'  when '3' then '整改反馈'  when '4' then '执行完成'  when '5' then '整改不合格'  else '未知状态'  
即反填 0,1,2,3,4,5，，即可
		 */
		log.debug(enpFlag);
		hse.setFLAG(enpFlag);
//		整改后照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）
		log.debug("");
		
		List<HseProblemFile> afters = hseProblemFileService.findByProblemIdAndFileType(Integer.valueOf(APPID), ConstantVar.after);
		

		String seting = variableSetService.findValueByKey("synchroEnpowerImage", "system");
		
    	
		if("ON".equalsIgnoreCase(seting) && afters!=null&& afters.size()>0){

			HseProblemFile fist = afters.get(0);

			// 上传位置  设定文件保存的目录 
			String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath+"/"+fist.getFilepath();
			
			hse.setZG_PHOTO(FileUtils.image2byteStr(filePath));
			
		}
		
//		整改人
		User user = userService.findUserById(solveUser);
		String usr = "";
		if(user!=null){
			usr= user.getRealname();
		}
		hse.setMODI_MAN(usr);
//		log.debug(hpsagain.getSolveUser());
//		整改完成日期
		Date MODI_DATE = null;

    	if(inMap==null){
    		throw new RuntimeException("入参为空错误！！");
    	}
    	if(inMap.get("MODI_DATE")!=null){
    		MODI_DATE = (Date) inMap.get("MODI_DATE");
    	}else{
    		MODI_DATE = Calendar.getInstance().getTime();
    	}
    	
		
		log.debug(DateUtility.sdf_yMdHms.format(MODI_DATE));
		hse.setMODI_DATE(DateUtility.sdf_yMdHms.format(MODI_DATE));
//		整改单的编号
//		hse.setTROU_CODE(getHSETRUE_CODE(APPID));
//		整改描述
//		log.debug("");
		List<EnpowerRequestHSEQuestionComplete> list = new ArrayList<EnpowerRequestHSEQuestionComplete>();
		list.add(hse);
		
        return list;
    }

    /**
     * 上报安全整改问题接口 推送接口名: 上报安全整改问题接口
     * 
     */
    public List<EnpowerRequestHSEQuestionSend> findEnpowerHSEQuestionSend(HseProblem hp) {
    	
    	EnpowerRequestHSEQuestionSend send = new EnpowerRequestHSEQuestionSend() ;
    	

//		整改单编号（必填项,唯一性）
    	send.setTROU_CODE(getHSETRUE_CODE(hp.getId()+""));
//		log.debug();
    	send.setFLAG(EnpowerHSEStatus.NOT_RECEIVE);
//      机组                  
        log.debug(hp.getUnit());                    
        send.setUNIT(hp.getUnit());
//      厂房                  
        log.debug(hp.getWrokshop());                    
        send.setBUILDING(hp.getWrokshop());
//      标高                  
        log.debug(hp.getEleration());                   
        send.setLEVELS(hp.getEleration());
//      房间号                 
        log.debug(hp.getRoomno());                  
        send.setROOM(hp.getRoomno());
//      问题描述                    
        log.debug(hp.getDescription());                 
        send.setBAKE (hp.getDescription());
        
//      责任部门                    
        log.debug(hp.getResponsibleDept());                 
        String deptName = "";
        Department dept = departmentService.findById(hp.getResponsibleDept());
        if(dept!=null){
        	deptName = dept.getName();
        }
        send.setCHECKED_COM(deptName);
//      责任班组                    
        log.debug(hp.getResponsibleTeam());      
        String deptName2 = "";
        Department dept2 = departmentService.findById(hp.getResponsibleTeam() );
        if(dept2!=null){
        	deptName2 = dept2.getName();
        }           
        send.setDUTY_COMP(deptName2);
//      问题照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）                    
//      log.debug();                    
		List<HseProblemFile> afters = hseProblemFileService.findByProblemIdAndFileType(hp.getId(), ConstantVar.before);

		String seting = variableSetService.findValueByKey("synchroEnpowerImage", "system");
		
    	
		if("ON".equalsIgnoreCase(seting) && afters!=null&& afters.size()>0){

			HseProblemFile fist = afters.get(0);

			// 上传位置  设定文件保存的目录 
			String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath+"/"+fist.getFilepath();
			
			
			send.setZGQ_PHOTO(FileUtils.image2byteStr(filePath));
			
		}
		
//      APP主键                   
        log.debug(hp.getId());                  
        send.setAPPID(String.valueOf(hp.getId()));
        
        send.setFOUND_DATE(DateUtility.sdfyMdHms.format(hp.getCreateDate()));
        
        User u = userService.findUserById(hp.getCreateUser());
        
        send.setFOUND_MAN(u.getRealname());
		
        List<EnpowerRequestHSEQuestionSend> list = new ArrayList<EnpowerRequestHSEQuestionSend>();
        //安全三级编码
        send.setTROU_TYPE(hp.getProblemTitle());
        send.setJBMS(hp.getCode2Desc());
        send.setJBMT(hp.getCode3Desc());
        
        
        list.add(send);
        return list;
    }

    /**
     * 修改安全整改完成情况 推送接口名: 修改安全整改完成情况
     * 
RE_ZG 整改情况回复
END_DATE 完成日期
RQ_YZ 验收日期
     */
    @Override
    public List<EnpowerRequestHSEQuestionSend> findEnpowerHSEQuestionSendUpdate(HseProblem hp,String RE_ZG,String END_DATE,String RQ_YZ,String enpFlag) {
    	
    	EnpowerRequestHSEQuestionSend send = new EnpowerRequestHSEQuestionSend() ;
    	
    	send.setRQ_YZ(RQ_YZ);
    	send.setEND_DATE(END_DATE);
    	send.setRE_ZG(RE_ZG);
    	//责任班组
      log.debug(hp.getResponsibleTeam());      
      String deptName2 = "";
      Department dept2 = departmentService.findById(hp.getResponsibleTeam() );
      if(dept2!=null){
      	deptName2 = dept2.getName();
      }
      send.setDUTY_COMP(deptName2);
      
  	//责任部门      
      String deptName = "";
      Department dept = departmentService.findById(hp.getResponsibleDept());
      if(dept!=null){
      	deptName = dept.getName();
      }
    //ZRDEPT--责任单位
      send.setCHECKED_COM(deptName);

//		整改单编号（必填项,唯一性）
    	send.setTROU_CODE(getHSETRUE_CODE(hp.getId()+""));
//		log.debug();
    	send.setFLAG(EnpowerHSEStatus.NOT_RECEIVE);
//      机组                  
        log.debug(hp.getUnit());                    
        send.setUNIT(hp.getUnit());
//      厂房                  
        log.debug(hp.getWrokshop());                    
        send.setBUILDING(hp.getWrokshop());
//      标高                  
        log.debug(hp.getEleration());                   
        send.setLEVELS(hp.getEleration());
//      房间号                 
        log.debug(hp.getRoomno());                  
        send.setROOM(hp.getRoomno());
//      问题描述                    
        log.debug(hp.getDescription());                 
        send.setBAKE (hp.getDescription()); 
//      问题照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）                    
//      log.debug();                    
		List<HseProblemFile> afters = hseProblemFileService.findByProblemIdAndFileType(hp.getId(), ConstantVar.before);

		String seting = variableSetService.findValueByKey("synchroEnpowerImage", "system");
		
    	
		if("ON".equalsIgnoreCase(seting) && afters!=null&& afters.size()>0){

			HseProblemFile fist = afters.get(0);

			// 上传位置  设定文件保存的目录 
			String filePath = ContextPath.fileBasePath+FileUtils.questionFilePath+"/"+fist.getFilepath();
			
			
			send.setZGQ_PHOTO(FileUtils.image2byteStr(filePath));
			
		}
		
//      APP主键                   
        log.debug(hp.getId());                  
        send.setAPPID(String.valueOf(hp.getId()));
        
        send.setFOUND_DATE(DateUtility.sdfyMdHms.format(hp.getCreateDate()));
         
		
        List<EnpowerRequestHSEQuestionSend> list = new ArrayList<EnpowerRequestHSEQuestionSend>();
        //安全三级编码
        send.setTROU_TYPE(hp.getProblemTitle());
        send.setJBMS(hp.getCode2Desc());
        send.setJBMT(hp.getCode3Desc());
        send.setFLAG(enpFlag);
        
        list.add(send);
        return list;
    }
    

    /**
     * 上报安全整改问题接口 推送接口名: 上传质量问题整改接口
     * 
     */
    public List<EnpowerRequestQualityQuestionSending> findEnpowerQualityQuestionSend(QualityControl qc,Integer zgr,Date zgDate,Date yzDate) {
    	
    	List<EnpowerRequestQualityQuestionSending> list = new ArrayList<EnpowerRequestQualityQuestionSending>();
    	EnpowerRequestQualityQuestionSending send = new EnpowerRequestQualityQuestionSending() ;
    	//责任班组
//        log.debug(qc.getResponsibleTeam());      
//        String deptName2 = "";
//        Department dept2 = departmentService.findById(qc.getResponsibleTeam() );
//        if(dept2!=null){
//        	deptName2 = dept2.getName();
//        }

    	//责任部门      
        String deptName = "";
        Department dept = departmentService.findById(qc.getResponsibleDept());
        if(dept!=null){
        	deptName = dept.getName();
        }
      //ZRDEPT--责任单位
        send.setZRDEPT(deptName);
        //STATUS--状态
        send.setSTATUS(EnpowerConstantVar.CLOSED);
        //ORIGIN--来源
        send.setORIGIN(EnpowerConstantVar.ENP_ORIGIN);
        //REASION_CODE--原因编码
        send.setREASION_CODE(qc.getType()+"");
        //DESCRIPTION--不符合描述
        send.setDESCRIPTION(qc.getProblemDescription());
//        //EDITER--编制人

        User createUser = userService.findUserById(qc.getCreateUser());
		if(createUser!=null){
			send.setEDITER(createUser.getRealname());
		}
//        //EDIT_DATE--编制日期
		if(qc.getCreateDate()!=null){
			send.setEDIT_DATE(DateUtility.sdf_yMdHms.format(qc.getCreateDate()));
		}
//        //TODO ZGR--整改人
		if(zgr!=null){
	        User zgrUser = userService.findUserById(zgr);
	        if(zgrUser!=null){
		        send.setZGR(zgrUser.getRealname());
	        }
		}
//        //ZG_DATE--整改日期
		if(zgDate!=null){
	        send.setZG_DATE(DateUtility.sdf_yMdHms.format(zgDate));
		}
//        //YZR--验证人
        User qcUser = userService.findUserById(qc.getQcUser());
		if(qcUser!=null){
	        send.setYZR(qcUser.getRealname());
		}
//        //TODO YZ_DATE--验证日期
		if(yzDate!=null){
	        send.setYZ_DATE(DateUtility.sdf_yMdHms.format(yzDate));
		}
		
		send.setAREA(qc.getArea());
		send.setSYSTEMS(qc.getSystem());
//		
//		private String AREA;//区域'
//		private String SYSTEMS;//系统'
//		private String ROOM;//房间'
		send.setROOM(qc.getRoomnum());
//		private String UNITS;//机组'
		send.setUNITS(qc.getUnit());
//		private String REMARK;//备注'
		send.setREMARK(qc.getProblemDescription());
//		private String TITLE;//问题单标题'
		send.setTITLE(qc.getType()+"");
//		private String TYPE;//问题单类型'}]"}
		send.setTYPE(qc.getType()+"");
    	
        list.add(send);
        return list;
    }

	/**
	 * 物项退库核实接口  推送接口名: 物项退库核实接口    
	 * @param MATE_CODE	物项编码
	 * @param ISSNO	退库单号
	 * @param ISSQTY	退库量
	 * @param QTY_RELEASED	核实量
	 */
    public List<EnpowerRequestMaterialCancelCheck> findEnpowerMaterialCancelCheckById(String MATE_CODE,String ISSNO,String ISSQTY,String QTY_RELEASED) {
    	List<EnpowerRequestMaterialCancelCheck> list = new ArrayList<EnpowerRequestMaterialCancelCheck>();
//    	if(forms!=null && forms.size()>0){

    	EnpowerRequestMaterialCancelCheck in = new EnpowerRequestMaterialCancelCheck();
    	in.setMATE_CODE(MATE_CODE);		//	物项编码
    	in.setISSNO(ISSNO);		//	退库单号
    	in.setISSQTY(ISSQTY);		//	退库量
    	in.setQTY_RELEASED(QTY_RELEASED);		//	核实量
    	
		list.add(in);
//    	}
        return list;
    }

    /**
     * 物项退库查询接口 推送接口名: 物项退库查询接口
     * 
     */
    public List<EnpowerRequestMaterialCancelSearch> findEnpowerMaterialCancelSearchById(String cancelNo) {
    	
    	EnpowerRequestMaterialCancelSearch send = new EnpowerRequestMaterialCancelSearch() ;
    	/**
    	 * 退库单号
    	 */
    	send.setCancelNo(cancelNo);
//      项目代号（默认值“K2K3”）                 
        send.setPROJ_CODE(EnpConstant.PROJ_CODE);
//      项目代码（K项默认值 “050812”)                    
        send.setCOMP_CODE(EnpConstant.COMP_CODE);
		
        List<EnpowerRequestMaterialCancelSearch> list = new ArrayList<EnpowerRequestMaterialCancelSearch>();
        list.add(send);
        return list;
    }

    /**
     * 物项出库核实接口 推送接口名: 物项出库核实接口     
     * @param MATE_CODE	物项编码
     * @param ISSNO	出库单号
     * @param ISSQTY	出库量
     * @param QTY_RELEASED	核实量
     */
    public List<EnpowerRequestMaterialOutCheck> findEnpowerMaterialOutCheckById(String MATE_CODE,String ISSNO,String ISSQTY,String QTY_RELEASED) {
    	List<EnpowerRequestMaterialOutCheck> list = new ArrayList<EnpowerRequestMaterialOutCheck>();
//    	if(forms!=null && forms.size()>0){

		EnpowerRequestMaterialOutCheck in = new EnpowerRequestMaterialOutCheck();
		in.setMATE_CODE(MATE_CODE); // 物项编码
		in.setISSNO(ISSNO); // 出库单号
		in.setISSQTY(ISSQTY); // 出库量
		in.setQTY_RELEASED(QTY_RELEASED); // 核实量

		list.add(in);
//    	}
        return list;
    }

    /**
     * 物项出库查询接口 推送接口名: 物项出库查询接口
     * 
     */
    public List<EnpowerRequestMaterialOutSearch> findEnpowerMaterialOutSearchById(String outNo) {
    	
    	EnpowerRequestMaterialOutSearch send = new EnpowerRequestMaterialOutSearch() ;

    	/**
    	 * 出库单号
    	 */
    	send.setOutNo(outNo);
//      项目代号（默认值“K2K3”）                 
        send.setPROJ_CODE(EnpConstant.PROJ_CODE);
//      项目代码（K项默认值 “050812”)                    
        send.setCOMP_CODE(EnpConstant.COMP_CODE);
		
        List<EnpowerRequestMaterialOutSearch> list = new ArrayList<EnpowerRequestMaterialOutSearch>();
        list.add(send);
        return list;
    }

    /**
     * 物项入库查询接口 推送接口名: 物项入库查询接口
     * 入参简单不需要在这里封装了。此方法废
     */
//    public List<EnpowerRequestMaterialStoreSearch> findEnpowerMaterialStoreSearchBywarrantyNo(String warrantyNo) {
//    	
//    	EnpowerRequestMaterialStoreSearch send = new EnpowerRequestMaterialStoreSearch() ;
//
//    	send.setWarrantyNo(warrantyNo);
////      项目代号（默认值“K2K3”）                 
//        send.setPROJ_CODE(EnpConstant.PROJ_CODE);
////      项目代码（K项默认值 “050812”)                    
//        send.setCOMP_CODE(EnpConstant.COMP_CODE);
//		
//        List<EnpowerRequestMaterialStoreSearch> list = new ArrayList<EnpowerRequestMaterialStoreSearch>();
//        list.add(send);
//        return list;
//    }

    /**
     * 查询今日出库量明细接口 推送接口名: 查询今日出库量明细接口
     */

    public List<EnpowerRequestMaterialTodayOutList> findEnpowerMaterialTodayOutListById(RollingPlan plan,Integer rollingPlanId) {

        return null;
    }

    /**
     * 查询今日入库量明细接口 推送接口名: 查询今日入库量明细接口
     * 
     */
    public List<EnpowerRequestMaterialTodayStoreList> findEnpowerMaterialTodayStoreList() {

        return null;
    }
    /**
     * 上报安全整改问题接口 整改单编号（必填项,唯一性）
     * @param aPPID
     * @return
     */
    private String getHSETRUE_CODE(String aPPID){
    	return EnpConstant.PROJ_CODE+"-HSE-YHZG-APP-"+aPPID;
    }
    private String getQualityTRUE_CODE(String aPPID){
    	return EnpConstant.PROJ_CODE+"-HSE-YHZG-APP-"+aPPID;
    }
}
