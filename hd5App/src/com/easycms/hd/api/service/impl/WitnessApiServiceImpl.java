package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.domain.Statistics;
import com.easycms.hd.api.enums.QC2MemberTypeEnum;
import com.easycms.hd.api.enums.StatusEnum;
import com.easycms.hd.api.enums.WitnessTypeEnum;
import com.easycms.hd.api.response.FileResult;
import com.easycms.hd.api.response.WitnessPageResult;
import com.easycms.hd.api.response.WitnessResult;
import com.easycms.hd.api.response.statistics.NoticePointStatisticsResult;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.mybatis.dao.WitnessStatisticsMybatisDao;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.hd.witness.domain.WitnessFile;
import com.easycms.hd.witness.service.WitnessFileService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

@Service("witnessApiService")
public class WitnessApiServiceImpl implements WitnessApiService {
	private static Logger logger = Logger.getLogger(WitnessApiServiceImpl.class);
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private WitnessFileService witnessFileService;
	@Autowired
	private WitnessStatisticsMybatisDao witnessStatisticsMybatisDao;
	
	//获取所有跟Problem相关的分页信息
	@Override
	public WitnessPageResult getWitnessPageResult(Page<WorkStepWitness> page, User user, WitnessTypeEnum witnessType, String rollingPlanType, NoticePointType noticePointType, String keyword, User loginUser,Integer usingRoleId) {
		logger.debug("user.getId()="+user.getId());
		logger.debug("witnessType="+witnessType);
		logger.debug("rollingPlanType="+rollingPlanType);
		logger.debug("noticePointType="+noticePointType);
		logger.debug("keyword="+keyword);
		logger.debug("loginUser="+loginUser.getId());
		WitnessPageResult witnessPageResult = new WitnessPageResult();
		Page<WorkStepWitness> workStepWitnesses = new Page<WorkStepWitness>();
		Integer loginId = null;
		if(loginUser!=null){
			loginId = loginUser.getId();
		}
		
		if (null != user) {
			List<Role> roles = user.getRoles();
			if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
				roles = roleService.findListById(roles,usingRoleId);
			}
			
			
			switch (witnessType) {
				//已发起的见证
				case LAUNCHED:
					workStepWitnesses = witnessService.findMyLaunchedWitnessByPage(user.getId(), rollingPlanType, page);
					break;
				// 未完成的见证
				case UNCOMPLETED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								NoticePointStatisticsResult statistics = getWitnessNoticePointStatisticsResult(user.getId(), roleNameList.toArray(new String[]{})[0], rollingPlanType);
								
								if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
									workStepWitnesses = witnessService.findQC2MemberUnCompleteWitnessByPage(user.getId(), rollingPlanType, keyword, page,loginId,user.getId(),ComputedConstantVar.noticePointQc2(roleNameList));
									
									if (!workStepWitnesses.getDatas().isEmpty()) {
										workStepWitnesses.setDatas(workStepWitnesses.getDatas().stream().map(mapper -> {
											if (!ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC2.name())) {
												return witnessService.findQC2SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}else if (ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC1.name())) {
												return witnessService.findQC1SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}
											return mapper;
										}).collect(Collectors.toList()));
									}
								} else if (roleNameList.contains("witness_member_qc1")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCMemberUnCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCMemberUnCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else if (roleNameList.contains("team")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findTeamUnCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findTeamUnCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else if (roleNameList.contains("witness_member_czecqa") || roleNameList.contains("witness_member_czecqc") || roleNameList.contains("witness_member_paec")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQC2DownLineMemberUnCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQC2DownLineMemberUnCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
								
								witnessPageResult.setStatistics(statistics);
							}
						}
					}
					break;
				// 已完成的见证
				case COMPLETED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if (roleNameList.contains("witness_member_qc1") || roleNameList.contains("witness_member_qc2")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCMemberCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCMemberCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else if (roleNameList.contains("team")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findTeamCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findTeamCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else if (roleNameList.contains("witness_member_czecqa") || roleNameList.contains("witness_member_czecqc") || roleNameList.contains("witness_member_paec")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQC2DownLineMemberCompleteWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQC2DownLineMemberCompleteWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
							}
						}
					}
					break;
				// 未分派的见证--见证组长--QC2
				case UNASSIGN: 
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if (roleNameList.contains("witness_team_qc1") || roleNameList.contains("witness_team_qc2")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCTeamUnassignWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCTeamUnassignWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findMemberUnassignQC2WitnessByPageWithKeyword(user.getId(), rollingPlanType, noticePointType == null ? null : noticePointType.name(), keyword, page);
									} else {
										workStepWitnesses = witnessService.findMemberUnassignQC2WitnessByPage(user.getId(), rollingPlanType, noticePointType == null ? null : noticePointType.name(), page);
									}
								}
							}
						}
					}
					break;
				// 待提交的见证
				case PENDING:
					if (CommonUtility.isNonEmpty(keyword)) {
						workStepWitnesses = witnessService.findMonitorPendingWitnessByPageWithKeyword(user.getId(), rollingPlanType, noticePointType == null ? null : noticePointType.name(), keyword, page);
					} else {
						workStepWitnesses = witnessService.findMonitorPendingWitnessByPage(user.getId(), rollingPlanType, noticePointType == null ? null : noticePointType.name(), page);
					}
					break;
				// 不合格的见证
				case UNQUALIFIED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
									workStepWitnesses = witnessService.findQC2MemberUnQulifiedWitnessByPage(user.getId(), rollingPlanType, keyword, page,loginId,user.getId(),ComputedConstantVar.noticePointQc2(roleNameList));
									
									if (!workStepWitnesses.getDatas().isEmpty()) {
										workStepWitnesses.setDatas(workStepWitnesses.getDatas().stream().map(mapper -> {
											if (!ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC2.name())) {
												return witnessService.findQC2SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}else if (ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC1.name())) {
												return witnessService.findQC1SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}
											return mapper;
										}).collect(Collectors.toList()));
									}
								} else if (roleNameList.contains("witness_member_czecqa") || roleNameList.contains("witness_member_czecqc") || roleNameList.contains("witness_member_paec")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQC2DownLineMemberUnQulifiedWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQC2DownLineMemberUnQulifiedWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCMemberUnQulifiedWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCMemberUnQulifiedWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
							}
						}
					}
					
					break;
				// 合格的见证
				case QUALIFIED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if (ComputedConstantVar.qc2OrFqQc1(roleNameList)) {
									workStepWitnesses = witnessService.findQC2MemberQulifiedWitnessByPage(user.getId(), rollingPlanType, keyword, page,loginId,user.getId(),ComputedConstantVar.noticePointQc2(roleNameList));
									
									if (!workStepWitnesses.getDatas().isEmpty()) {
										workStepWitnesses.setDatas(workStepWitnesses.getDatas().stream().map(mapper -> {
											if (!ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC2.name())) {
												return witnessService.findQC2SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}else if (ContextPath.qc1ReplaceQc2 && !mapper.getNoticePoint().equals(NoticePointType.QC1.name())) {
												return witnessService.findQC1SingleItem(user.getId(), rollingPlanType, mapper.getWorkStep().getId());
											}
											return mapper;
										}).collect(Collectors.toList()));
									}
								} else if (roleNameList.contains("witness_member_czecqa") || roleNameList.contains("witness_member_czecqc") || roleNameList.contains("witness_member_paec")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQC2DownLineMemberQulifiedWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQC2DownLineMemberQulifiedWitnessByPage(user.getId(), rollingPlanType, page);
									}
								} else {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCMemberQualifiedWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCMemberQualifiedWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
							}
						}
					}
					break;
				// 未处理的见证
				case UNHANDLED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
									workStepWitnesses = witnessService.findQC2TeamUnHandledWitnessByPage(user.getId(), rollingPlanType, keyword, page,loginId,user.getId(),ComputedConstantVar.noticePointQc2(roleNameList));
								} else if (roleNameList.contains("witness_member_qc1")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCTeamUnHandledWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCTeamUnHandledWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
							}
						}
					}
					break;
				// 已处理的见证
				case HANDLED:
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (null != roleNameList && !roleNameList.isEmpty()) {
								if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
									workStepWitnesses = witnessService.findQC2TeamHandledWitnessByPage(user.getId(), rollingPlanType, keyword, page,loginId,user.getId(),ComputedConstantVar.noticePointQc2(roleNameList));
								} else if (roleNameList.contains("witness_member_qc1")) {
									if (CommonUtility.isNonEmpty(keyword)) {
										workStepWitnesses = witnessService.findQCTeamHandledWitnessByPageWithKeyword(user.getId(), rollingPlanType, keyword, page);
									} else {
										workStepWitnesses = witnessService.findQCTeamHandledWitnessByPage(user.getId(), rollingPlanType, page);
									}
								}
							}
						}
					}
					break;
				default:
					break;
			}
		
			if (null != workStepWitnesses) {
				witnessPageResult.setPageCounts(workStepWitnesses.getPageCount());
				witnessPageResult.setPageNum(workStepWitnesses.getPageNum());
				witnessPageResult.setPageSize(workStepWitnesses.getPagesize());
				witnessPageResult.setTotalCounts(workStepWitnesses.getTotalCounts());
				if (null != workStepWitnesses.getDatas()) {
					witnessPageResult.setData(workStepWitnesses.getDatas().stream().map(workStepWitness -> {
						WitnessResult witnessResult = this.workStepWitnessTransferToWitnessResult(workStepWitness);
						
						if (workStepWitness.getNoticePoint().equals(NoticePointType.QC2.name())) {
							List<WorkStepWitness> subWitnesses = witnessService.findMemberPendingQC2WitnessAdvance(user.getId(), rollingPlanType, workStepWitness.getWorkStep().getId());
							
							if (!subWitnesses.isEmpty()) {
								List<WitnessResult> subs = subWitnesses.stream().map(sub -> {
									return this.workStepWitnessTransferToWitnessResult(sub);
								}).collect(Collectors.toList());
								witnessResult.setSubWitness(subs);
							
								if (subs.stream().filter(sub -> {
									return sub.getResult().equals(StatusEnum.UNQUALIFIED.name());
								}).count() > 0) {
									witnessResult.setResult(StatusEnum.UNQUALIFIED.name());
								} else if (subs.stream().filter(sub -> {
									return sub.getResult().equals(StatusEnum.UNWITNESS.name());
								}).count() > 0) {
									witnessResult.setStatus(StatusEnum.UNCOMPLETED.name());
									witnessResult.setResult(StatusEnum.UNWITNESS.name());
								} else {
									witnessResult.setResult(StatusEnum.QUALIFIED.name());
								}
							}
						}
						
						return witnessResult;
					}).collect(Collectors.toList()));
				}
			}
		}
		return witnessPageResult;
	}
	
	@Override
	public WitnessResult workStepWitnessTransferToWitnessResult(WorkStepWitness workStepWitness) {
		WitnessResult witnessResult = new WitnessResult();
		witnessResult.setId(workStepWitness.getId());
		witnessResult.setCreateDate(workStepWitness.getCreatedOn());
		
		User user = userService.findUserById(workStepWitness.getCreatedBy());
		if (null != user) {
			witnessResult.setLauncherId(workStepWitness.getCreatedBy());
			witnessResult.setLauncherName(user.getRealname());
		}
		witnessResult.setNoticePoint(workStepWitness.getNoticePoint());
		witnessResult.setNoticeType(workStepWitness.getNoticeType());
		witnessResult.setWitnessAddress(workStepWitness.getWitnessaddress());
		witnessResult.setWitnessDate(workStepWitness.getWitnessdate());
		witnessResult.setRealWitnessAddress(workStepWitness.getRealwitnessaddress());
		witnessResult.setRealWitnessDate(workStepWitness.getRealwitnessdata());
		witnessResult.setFailType(workStepWitness.getFailType());
		witnessResult.setRemark(workStepWitness.getRemark());
		if (null != workStepWitness.getWitnessteam()) {
			witnessResult.setWitnessTeam(userApiService.userTransferToUserResult(userService.findUserById(workStepWitness.getWitnessteam()),false));//rollingPlan等需要验证角色时，设置false
		}
		if (null == workStepWitness.getWitness()) {
			witnessResult.setStatus(StatusEnum.UNASSIGNED.name());
			witnessResult.setResult(StatusEnum.UNWITNESS.name());
		} else {
			if (null != workStepWitness.getWitnesser()) {
				witnessResult.setQc2Status(StatusEnum.ASSIGNED.name());
				witnessResult.setWitnesser(userApiService.userTransferToUserResult(userService.findUserById(workStepWitness.getWitnesser()),false));//rollingPlan等需要验证角色时，设置false
			}
			if (null != workStepWitness.getWitnesser()) {
				witnessResult.setQc2Status(StatusEnum.ASSIGNED.name());
				witnessResult.setWitnesser(userApiService.userTransferToUserResult(userService.findUserById(workStepWitness.getWitnesser()),false));//rollingPlan等需要验证角色时，设置false
			}
			if (workStepWitness.getIsok().equals(0)) {
				witnessResult.setStatus(StatusEnum.ASSIGNED.name());
				witnessResult.setResult(StatusEnum.UNWITNESS.name());
			} else {
				witnessResult.setStatus(StatusEnum.WITNESSED.name());
				if (workStepWitness.getIsok().equals(1)) {
					witnessResult.setResult(StatusEnum.UNQUALIFIED.name());
				} else if (workStepWitness.getIsok().equals(2)) {
					witnessResult.setResult(StatusEnum.AUTO_QUALIFIED.name());
				} else if (workStepWitness.getIsok().equals(3)) {
					witnessResult.setResult(StatusEnum.QUALIFIED.name());
				}
			}
		}
		WorkStep workStep = workStepWitness.getWorkStep();
		if (null != workStep) {
			witnessResult.setWorkStepNo(workStep.getStepno() + "");
			witnessResult.setStepIdentifier(workStep.getStepIdentifier());
			//统一　工序编号/名称绑在一起，APP就不用调整
			witnessResult.setWorkStepName(workStep.getStepIdentifier()+"/"+workStep.getStepname());
			witnessResult.setWorkStepId(workStep.getId());
			RollingPlan rollingPlan = workStep.getRollingPlan();
			if (null != rollingPlan) {
				witnessResult.setRollingPlanId(rollingPlan.getId());
				witnessResult.setItpNo(rollingPlan.getItpno());
				witnessResult.setProjectNo(rollingPlan.getProjectno());
				witnessResult.setSpeification(rollingPlan.getSpecification());
				witnessResult.setWorkListNo(rollingPlan.getWeldlistno());
				witnessResult.setWeldno(rollingPlan.getWeldno());
				witnessResult.setDrawingNo(rollingPlan.getDrawno());
			}
		}
		
		List<WitnessFile> witnessFiles = witnessFileService.findFilesByWitnessId(workStepWitness.getId());
		if (null != workStepWitness) {
			workStepWitness.setWitnessFiles(witnessFiles);
		}
		
		
		if (null != workStepWitness.getWitnessFiles() && !workStepWitness.getWitnessFiles().isEmpty()) {
			List<FileResult> witnessFileResults = workStepWitness.getWitnessFiles().stream().map(witnessFile -> {
				FileResult witnessFileResult = new FileResult();
				witnessFileResult.setId(witnessFile.getId());
				witnessFileResult.setUrl("/hdxt/api/files/witness/" + witnessFile.getPath());
				witnessFileResult.setFileName(witnessFile.getFileName());
				return witnessFileResult;
			}).collect(Collectors.toList());
			witnessResult.setWitnessFiles(witnessFileResults);
		}
		
		if (null != workStepWitness.getWitness() && null == witnessResult.getWitnesser()) {
			witnessResult.setWitnesser(userApiService.userTransferToUserResult(userService.findUserById(workStepWitness.getWitness()),false));//rollingPlan等需要验证角色时，设置false
		}
		
		witnessResult.setWitnessAddresses(this.getWitnessAddresses());
		witnessResult.setWitnessFlag(workStepWitness.getWitnessflag());
		witnessResult.setSubstitute(workStepWitness.getSubstitute());
		witnessResult.setLastOne(PlanUtil.lastOneWorkStepWitnessQC1(workStepWitness));
		witnessResult.setOrder(workStepWitness.getSeq());
		
		return witnessResult;
	}

	@Override
	public List<String> getWitnessAddresses() {
		List<String> witnessAddresses = new ArrayList<String>();
		witnessAddresses.add("Room 01");
		witnessAddresses.add("Conference 01");
		witnessAddresses.add("City 01");
		witnessAddresses.add("Country 01");
		witnessAddresses.add("Building 01");
		return witnessAddresses;
	}

	@Override
	public boolean assignToWitnessTeam(Integer[] ids, Integer teamId) {
		return witnessService.assignToWitnessTeam(ids, teamId);
	}

	@Override
	public boolean assignToWitnessMember(Integer[] ids, Integer teamId, Integer memberId) {
		boolean flag = witnessService.assignToWitnessMember(ids, teamId, memberId);
		if (flag) {
			User member = userService.findUserById(memberId);
			if (null != member) {
				List<Role> roles = member.getRoles();
				if (null != roles) {
					for (Role role : roles) {
						Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
						if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) { 
							if (null != ids) {
								for (Integer id : ids) {
									WorkStepWitness workStepWitness = witnessService.findById(id);
									if (null != workStepWitness) {
										witnessService.assignToWitnessMemberQC2(memberId, workStepWitness.getWorkStep().getId());
										if(ContextPath.qc1ReplaceQc2){//福清QC１组长分源组员时，附带打外部单位的也分配了。

											witnessService.assignToWbWitnessMemberFqQC1(memberId, workStepWitness.getWorkStep().getId(),teamId);
										}
									}
								}
							}
						}
					}
				}
			}
			return flag;
		}
		
		return false;
	}

	@Override
	public WitnessResult findByWitnessIdAndRollingPlanType(User user, Integer witnessId, String rollingPlanType, boolean ref) {
		WorkStepWitness workStepWitness = witnessService.findByWitnessIdAndRollingPlanType(witnessId, rollingPlanType);
		if (null != workStepWitness) {
			WitnessResult witnessResult = this.workStepWitnessTransferToWitnessResult(workStepWitness);
			
			if (workStepWitness.getNoticePoint().equals(NoticePointType.QC2.name())) {
				List<WorkStepWitness> subWitnesses = witnessService.findMemberPendingQC2WitnessAdvance(user.getId(), rollingPlanType, workStepWitness.getWorkStep().getId());
				
				if (!subWitnesses.isEmpty()) {
					List<WitnessResult> subs = subWitnesses.stream().map(sub -> {
						return this.workStepWitnessTransferToWitnessResult(sub);
					}).collect(Collectors.toList());
					witnessResult.setSubWitness(subs);
					
					if (ref) {
						if (subs.stream().filter(sub -> {
							return sub.getResult().equals(StatusEnum.UNQUALIFIED.name());
						}).count() > 0) {
							witnessResult.setResult(StatusEnum.UNQUALIFIED.name());
						} else if (subs.stream().filter(sub -> {
							return sub.getResult().equals(StatusEnum.UNWITNESS.name());
						}).count() > 0) {
							witnessResult.setStatus(StatusEnum.UNCOMPLETED.name());
							witnessResult.setResult(StatusEnum.UNWITNESS.name());
						} else {
							witnessResult.setResult(StatusEnum.QUALIFIED.name());
						}
					}
				}
			}
			return witnessResult;
		}
		return null;
	}

	/**
	 *适用于　福清qc1ReplaceQc2
	 * */
	@Override
	public boolean assignToWitnesserQC2(Integer[] ids, Integer qc2Id, Integer memberId) {
		return witnessService.assignToWitnesserQC2(ids, qc2Id, memberId);
	}
	
	private NoticePointStatisticsResult getWitnessNoticePointStatisticsResult(Integer userId, String role, String type) {
		NoticePointStatisticsResult noticePointStatisticsResult = new NoticePointStatisticsResult();
		
		Statistics statistics = new Statistics();
		statistics.setType(type);
		statistics.setUserId(userId);
		
		if (role.equals("witness_member_qc1")) {
			statistics.setPointType("H");
			noticePointStatisticsResult.setPointH(witnessStatisticsMybatisDao.qc1MemberNoticePointCount(statistics));
			statistics.setPointType("R");
			noticePointStatisticsResult.setPointR(witnessStatisticsMybatisDao.qc1MemberNoticePointCount(statistics));
			statistics.setPointType("W");
			noticePointStatisticsResult.setPointW(witnessStatisticsMybatisDao.qc1MemberNoticePointCount(statistics));
			noticePointStatisticsResult.setTotal(noticePointStatisticsResult.getPointH() + noticePointStatisticsResult.getPointW() +
					noticePointStatisticsResult.getPointR());
		} else if (role.equals("witness_member_qc2")) {
			statistics.setPointType("H");
			noticePointStatisticsResult.setPointH(witnessStatisticsMybatisDao.qc2MemberNoticePointCount(statistics));
			statistics.setPointType("R");
			noticePointStatisticsResult.setPointR(witnessStatisticsMybatisDao.qc2MemberNoticePointCount(statistics));
			statistics.setPointType("W");
			noticePointStatisticsResult.setPointW(witnessStatisticsMybatisDao.qc2MemberNoticePointCount(statistics));
			noticePointStatisticsResult.setTotal(noticePointStatisticsResult.getPointH() + noticePointStatisticsResult.getPointW() +
					noticePointStatisticsResult.getPointR());
		} else if (role.equals("witness_member_czecqc")) {
			statistics.setWitnessType(QC2MemberTypeEnum.CZEC_QC.name());
			
			statistics.setPointType("H");
			noticePointStatisticsResult.setPointH(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("R");
			noticePointStatisticsResult.setPointR(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("W");
			noticePointStatisticsResult.setPointW(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			noticePointStatisticsResult.setTotal(noticePointStatisticsResult.getPointH() + noticePointStatisticsResult.getPointW() +
					noticePointStatisticsResult.getPointR());
		} else if (role.equals("witness_member_czecqa")) {
			statistics.setWitnessType(QC2MemberTypeEnum.CZEC_QA.name());
			
			statistics.setPointType("H");
			noticePointStatisticsResult.setPointH(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("R");
			noticePointStatisticsResult.setPointR(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("W");
			noticePointStatisticsResult.setPointW(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			noticePointStatisticsResult.setTotal(noticePointStatisticsResult.getPointH() + noticePointStatisticsResult.getPointW() +
					noticePointStatisticsResult.getPointR());
		} else if (role.equals("witness_member_paec")) {
			statistics.setWitnessType(QC2MemberTypeEnum.PAEC.name());
			
			statistics.setPointType("H");
			noticePointStatisticsResult.setPointH(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("R");
			noticePointStatisticsResult.setPointR(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			statistics.setPointType("W");
			noticePointStatisticsResult.setPointW(witnessStatisticsMybatisDao.qc2MemberDownLineNoticePointCount(statistics));
			noticePointStatisticsResult.setTotal(noticePointStatisticsResult.getPointH() + noticePointStatisticsResult.getPointW() +
					noticePointStatisticsResult.getPointR());
		} else {
			return null;
		}
		noticePointStatisticsResult.setMark(role);
		return noticePointStatisticsResult;
	}
}
