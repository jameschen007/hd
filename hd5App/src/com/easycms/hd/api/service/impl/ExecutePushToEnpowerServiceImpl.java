package com.easycms.hd.api.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.easycms.hd.api.request.underlying.WitingPlan;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtility;
import com.easycms.common.util.HttpRequest;
import com.easycms.common.util.NumberUtility;
import com.easycms.hd.api.enpower.dao.EnpowerLogDao;
import com.easycms.hd.api.enpower.domain.EnpowerConstantVar;
import com.easycms.hd.api.enpower.domain.EnpowerLog;
import com.easycms.hd.api.enpower.request.EnpowerRequest;
import com.easycms.hd.api.enpower.request.EnpowerRequestDisappearPoint;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionComplete;
//import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionComplete;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionSend;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialBackfill;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelCheckInfo;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutCheckInfo;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutCheckStock;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheck;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckForm;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckInfo;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckStock;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanStatusSyn;
import com.easycms.hd.api.enpower.request.EnpowerRequestQualityQuestionSending;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendEquipment;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendMain;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendNotice;
import com.easycms.hd.api.enpower.request.EnpowerRequestXmlName;
import com.easycms.hd.api.enpower.request.build.BuildEnpowerRequestMaterialStoreCheckInfo;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enpower.response.EnpowerResponseHseCode;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialCancelSearch;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialOutCount;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialOutSearch;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialStoreSearch;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialTodayStore;
import com.easycms.hd.api.enpower.response.EnpowerResponseRoomLevel;
import com.easycms.hd.api.enums.enpower.EnpowerStatus;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.response.ExpireNoticeFileResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.MaterialOutCountResult;
import com.easycms.hd.api.response.MaterialResult;
import com.easycms.hd.api.service.EnpowerApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.easycms.hd.hseproblem.domain.HseProblemSolveStep;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.hd.plan.dao.RollingPlanDao;
import com.easycms.hd.plan.domain.EnpowerMaterialInfo;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.service.EnpowerMaterialInfoService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.qualityctrl.domain.QualityControl;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
 * 执行推送接口数据至Enpower系统
 * @author ul-webdev
 *
 */
@Service("executePushToEnpowerServiceImpl")
public class ExecutePushToEnpowerServiceImpl {
	private Logger logger = Logger.getLogger(ExecutePushToEnpowerServiceImpl.class);

	@Autowired
	private ComputedConstantVar constantVar ;
	@Value("#{APP_SETTING['enpower_api_updateData']}")
	private String enpowerupdateData;
	Gson gson = new Gson();
	
	@Autowired
	private WorkStepService workStepService ;
	@Autowired
	private RollingPlanApiService rollingPlanApiService ;
	@Autowired
	private RollingPlanService rollingPlanService ;
	@Autowired
	private RollingPlanDao rollingPlanDao ;
	@Autowired
	private EnpowerMaterialInfoService enpowerMaterialInfoService ;
	@Autowired
	private EnpowerApiService enpowerApiService ;
	@Autowired
	private VariableSetService variableSetService ;
	@Autowired
	private HseProblemSolveService hseProblemSolveService ;

	@Autowired
	private EnpowerLogDao enpowerLogDao;
	/**
	 * 调用Enpower系统进行消点,首选对象参数,否则使用id参数
	 * @param witnessList
	 * @param witnessIds
	 * @return
	 * @throws Exception 
	 */
	public JsonResult<Boolean> disappearPoint(List<WorkStepWitness> witnessList,Integer[] witnessIds) throws Exception{
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{
			List<EnpowerRequestDisappearPoint> disaList = null;
			disaList = rollingPlanApiService.findEnpowerDisappearPointByWitness(witnessList,witnessIds);
			logger.info("见证消点接口:推送接口名: 修改消点信息="+CommonUtility.toJsonStringNull(disaList));

			EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
			bn.setXml("修改消点信息");
			bn.setDate_col("A_DATE,B_DATE,C_DATE,D_DATE,H_DATE");
			String data = CommonUtility.toJsonStringNull(disaList);
			bn.set_value(data);
			
			updateEnpowerAndSetResult(result, bn,null);
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2010");
			result.setMessage("消点接口调用出现异常！"+e.getMessage()+" witnessIds=" +witnessIds);
			throw e;
		}

		return result;
	}
/**
 * 调用Enpower接口同步推送状态，首先采用对象参数，否则使用id参数
 * @param plan
 * @param rollingPlanId
 * @return
 */
	public JsonResult<Boolean> updateEnpowerStatus(RollingPlan plan,Integer rollingPlanId,String dosage) throws Exception{
		if(plan==null){
			if(rollingPlanId==null){
				throw new RuntimeException("参数异常错误！");
			}
			plan = rollingPlanService.findById(rollingPlanId);
		}
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
		try{
			EnpowerRequestPlanStatusSyn planStatus = rollingPlanApiService.findEnpowerStatusSynByRollingPlanId(plan,rollingPlanId);
			if(dosage==null || "".equals(dosage) || "null".equals(dosage)){
				if(plan.getRealProjectcost()!=null && !"".equals(plan.getRealProjectcost())){
					planStatus.setRealProjectcost(plan.getRealProjectcost());
				}
			}else{
				planStatus.setRealProjectcost(dosage);
			}
			list.add(planStatus);
			logger.info("修改作业条目信息:"+CommonUtility.toJsonStringNull(planStatus));
			
			if(StringUtils.isBlank(dosage)){
				
			}

			EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
			bn.setXml("修改作业条目信息");
			bn.setDate_col("START_DATE,END_DATE");
			String data = CommonUtility.toJsonStringNull(list);
			bn.set_value(data);
			
			updateEnpowerAndSetResult(result, bn,null);
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2010");
			result.setMessage("修改作业条目信息！"+e.getMessage()+" rollingPlanId=" +rollingPlanId +" plan="+(plan!=null?plan.getId():""));
			throw e;
		}

		return result;
	}
	
	/**
	 * 调用Enpower接口进行材料回填.
	 * @param rollingPlanId
	 * @return
	 */
	public JsonResult<Boolean> materialBackfill(String materialListStr,Integer rollingPlanId) throws Exception{
		logger.info("材料用量参数 ="+materialListStr+" rollingPlanId="+rollingPlanId);

		if(materialListStr==null ||"".equals(materialListStr)){
			//见证没有材料信息，不保存也不同步Enpower
//			JsonResult<Boolean> result = new JsonResult<Boolean>();
//			result.setCode("-2001");
//			result.setMessage("材料用量为必填项。");
//			return result;

			JsonResult<Boolean> result = new JsonResult<Boolean>();
			//取消材料限制后，这种情况是正常的了。
			result.setCode("1000");
			result.setMessage("操作执行成功");
			result.setResponseResult(true);
			return result;
		}else{
			String[] idMats = materialListStr.split(",");
			if(idMats!=null && idMats.length>0){
				for(String idMat:idMats){
					if(idMat!=null && !"".equals(idMat)){
						String [] idma = idMat.split("_");
						if(idma.length>1){
							Integer id = Integer.valueOf(idma[0]);
							String actual = idma[1];
							EnpowerMaterialInfo bean = enpowerMaterialInfoService.findById(id);
							bean.setActualDosage(actual);
							if(actual==null||CommonUtility.isNonEmpty(actual)==false){
								JsonResult<Boolean> result = new JsonResult<Boolean>();
								result.setResponseResult(false);
								return result ;
							}
							enpowerMaterialInfoService.updateActualDosage(bean);
						}else{
							//只有最后一次回填才会进来，所以要严格限制
							JsonResult<Boolean> result = new JsonResult<Boolean>();
							result.setResponseResult(false);
							return result ;
						}
						
					}
				}
			}
			
			JsonResult<Boolean> result = new JsonResult<Boolean>();
			result.setResponseResult(true);
			List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
			try{
				List<EnpowerRequestMaterialBackfill> materialList = null;
				try{
					materialList = rollingPlanApiService.findEnpowerMaterialInfoByRollingPlanId(rollingPlanId);
					logger.info("修改材料清单="+CommonUtility.toJsonStringNull(materialList));
				}catch(Exception e){
					e.printStackTrace();
					throw e;
				}
				if(materialList==null || materialList.size()==0){
//					result.setCode("-2001");
//					result.setMessage("材料信息未查到,"+rollingPlanId);
//					result.setResponseResult(false);

					//取消材料限制后，这种情况是正常的了。
					result.setCode("1000");
					result.setMessage("操作执行成功");
					result.setResponseResult(true);
					
					
					return result ;
				}

			    JsonParser parser = new JsonParser();
				EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
				bn.setXml("修改材料清单");
				bn.setDate_col("MATER_BACK_FILL_DATE");
				String data = CommonUtility.toJsonStringNull(materialList);
				bn.set_value(data);
				
				updateEnpowerAndSetResult(result, bn,null);
			}catch(Exception e){
				e.printStackTrace();
				result.setCode("-2010");
				result.setMessage("修改作业条目信息！"+e.getMessage()+" rollingPlanId=" +rollingPlanId);
				result.setResponseResult(false);
				throw e;
			}
			
			return result;
		}
	}

	/**
	 * 发点是针对一个滚动计划（作业条目下的）一或多个工序发起， 
	 * 最后再调用状态接口
	 * 20190708　优化数据结构
	 * @param planList
	 * @return
	 */
	public JsonResult<Boolean> insertPointToEnpower(List<WitingPlan> planList) throws Exception{
		JsonResult<Boolean> result = new JsonResult<Boolean>(); 
		for(WitingPlan wplan :planList){

			//这里取消了对工序数量的判断，以前的根据传入的工序条数判断没有意义。
			String workStepIds = "";
			try{
//				boolean isStatusSyn = false ;//状态只同步一次即可；
				
				RollingPlan plan = wplan.getPlan();
				Integer rollingPlanId = plan.getId();
				Boolean sendEnp = plan.getSendEnp();
				logger.info("当前发点滚动计划id="+rollingPlanId);
				//用于后面共用的 组长名字
				String consteamName = null;
				List<EnpowerRequestSendMain> mainList = null;
				mainList = rollingPlanApiService.findEnpowerSendPointMainByRollingPlanId(rollingPlanId);
				consteamName = mainList.get(0).getConsteamName();
				if(sendEnp==null || sendEnp == false){//发点通知单信息 与作业条目一对一
					EnpowerRequestXmlName bn1 = new EnpowerRequestXmlName();
					bn1.setXml("插入发点通知单信息");
//					bn.setDate_col("EDIT_DATE");
					String data1 = CommonUtility.toJson(mainList);
					bn1.set_value(data1);
					updateEnpowerAndSetResult(result, bn1,null);
					
//					插入发点通知单信息　和　插入发点通知单信息　这两个接口的调用次数是一样的
//					沙道磊  15:12:27
//					那就插一次吧
//					这个信息目前没见有啥用，先这样做
					List<EnpowerRequestSendEquipment> equipList = new ArrayList<EnpowerRequestSendEquipment>();
					equipList = rollingPlanApiService.findEnpowerSendPointEquipByRollingPlanId(rollingPlanId);

					EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
					bn.setXml("插入设备清单");
					bn.setDate_col("FOUND_DATE");
					String data = CommonUtility.toJson(equipList);
					bn.set_value(data);

					updateEnpowerAndSetResult(result, bn,null);

					//插入通知单明细 调一次就行了。20181028
					List<EnpowerRequestSendNotice> noticeList = new ArrayList<EnpowerRequestSendNotice>();
					noticeList = rollingPlanApiService.findEnpowerSendPointNoticeDetailByWorkStepIds(wplan,consteamName );

					EnpowerRequestXmlName bn2 = new EnpowerRequestXmlName();
					bn2.setXml("插入通知单明细");
					bn2.setDate_col("NOTE_DATE,WITN_DATE");
					String data2 = CommonUtility.toJson(noticeList);
					bn2.set_value(data2);
					updateEnpowerAndSetResult(result, bn2,null);
					
					plan.setSendEnp(true);
					rollingPlanDao.save(plan);
				}
				
//				if(isStatusSyn == false){//应该判断是同一个滚动计划的 20181116进和本方法的都是同一个滚动计划的了。
					List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
					EnpowerRequestPlanStatusSyn planStatus = rollingPlanApiService.findEnpowerStatusSynByRollingPlanId(null,rollingPlanId);
					list.add(planStatus);
					EnpowerRequestXmlName bn4 = new EnpowerRequestXmlName();
					bn4.setXml("修改作业条目信息");
					bn4.setDate_col("START_DATE,END_DATE");
					String data4 = CommonUtility.toJsonStringNull(list);
					bn4.set_value(data4);
					
					updateEnpowerAndSetResult(result, bn4,null);
//					isStatusSyn= true ;
//				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setCode("-2010");
				result.setMessage("发点接口调用出现异常！"+e.getMessage()+" workStepIds=" +workStepIds);
				throw e;
			}
		}

		return result ;

	}

	public void insertEnpowerAndSetResult(JsonResult<Boolean> result, EnpowerRequestXmlName bn) throws Exception {
		updateEnpowerAndSetResult(result,bn,null);
	}


    public List invalidFileList(String enpowerUserId) throws Exception{

    	List<MaterialResult> list = new ArrayList<MaterialResult>();
    	Map<String,Object> map = new HashMap<String,Object>();
    	if(enpowerUserId!=null){
    		map.put("xxxx", enpowerUserId);//当前登录人的enpowerid
    	}
    	if(map.size()==0){
    		return new ArrayList<Object>();
    	}
		//pageIndex 页码
		//pageSize 条数
        
		try {
			String xmlName = "xxx询接口";
			String conditionKey = "proj_code";
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			String requestResult = getEnpowerDBinfo(xmlName,map,EnpowerConstantVar.enpowerPageIndex,EnpowerConstantVar.enpowerPageSize);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					List<EnpowerResponseMaterialTodayStore> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialTodayStore>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){

						enpList.stream().forEach(ret->{
    						MaterialResult result = null;
							result = new MaterialResult();
							// 物项编码
							result.setMATE_CODE(ret.getMATE_CODE());// 物项编码

    						list.add(result);
    						
    					});
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    
    /**
     * 文件失效通知单接口 推送接口名: 文件失效通知单列表接口
     * @return
     */
    public List<ExpireNoticeFileResult> fileInvalidNoticeList(String cancelId,String enpowerUserId,int pagenum,int pagesize) throws Exception{
//        
//        JsonResult<Boolean> result = new JsonResult<Boolean>();
//        try{
//        	Map<String,Object> map = new HashMap<String,Object>();
//        	if(cancelId!=null){
//        		map.put("ID", cancelId);//失效单ID
//        	}
//        	StringBuffer sb = new StringBuffer();
//        	sb.append(" and BORROWER_ID in (select id from v_dept_user_sel where v_dept_user_sel.PARENT_ID in (")
//        			.append("select ID from send_group start with id in (select parent_id from v_dept_user_sel where")
//        			.append(" id='").append( enpowerUserId).append( "'")
//        			.append( " and proj_code='").append(EnpConstant.PROJ_CODE).append( "') connect by prior ID = PARENT_ID)) ");
//
//			String xmlName = "文件失效通知单接口";
//			String requestResult = getEnpowerDBinfo(xmlName,map,sb,pagenum,pagesize);
//
//			if (null != requestResult && !"".equals(requestResult)) {
//				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
//				}.getType());
//				if (baseResponse.getIsSuccess()) {
//					String dataInfo = baseResponse.getDataInfo();
//					logger.info(dataInfo);
//					List<EnpowerResponseExpireFileDetail> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseExpireFileDetail>>() {
//					}.getType());
//
//					List<ExpireNoticeFileResult> list = new ArrayList<ExpireNoticeFileResult>();
//					if(enpList.isEmpty()==false){
//
//						enpList.stream().forEach(ret->{
//							ExpireNoticeFileResult bn = new ExpireNoticeFileResult();
//							/* 文件失效通知单编号 */bn.setCancCode(ret.getCANC_CODE());
//							/* 内部文件编号 */bn.setIntercode(ret.getINTERCODE());
//							/* 中文名称 */bn.setCTitle(ret.getC_TITLE());
//							/* 版本 */bn.setRevsion(ret.getREVSION());
//							/* 状态 */bn.setStatus(ret.getSTATUS());
//							/* 文件使用人 */bn.setBorrower(ret.getBORROWER());
//							/* 份数 */bn.setBorNum(ret.getBOR_NUM());
//							/* 发布时间 */bn.setSignDate(ret.getSIGN_DATE());
//							/* 编制人 */bn.setDrafter(ret.getDRAFTER());
//							/* 编制日期 */bn.setDraftDate(ret.getDRAFT_DATE());
//							/* 审批人 */bn.setSigner(ret.getSIGNER());
//							/* 项目代号 */bn.setProjCode(ret.getPROJ_CODE());
//							/* 公司代码 */bn.setCompCode(ret.getCOMP_CODE());
//							/* 文件借阅人 */bn.setLoginname(ret.getLoginname());
//							/* 借阅人所属分发组 */bn.setOrgName(ret.getORG_NAME());
//    						list.add(bn);
//    						
//    					});
//						return list ;
//					}
//
//				} else {
//					logger.error(baseResponse.getDataInfo());
//				}
//			}
//        }catch(Exception e){
//            e.printStackTrace();
//            result.setCode("-2010");
//            result.setMessage(e.getMessage());
//            throw e;
//        }

        return new ArrayList<ExpireNoticeFileResult>();
    }

    /**
     * 供业务逻辑中修改安全状态时调用,目的是为了统一　调用Enpower的都放在此类中。
     * @param hp
     * @param enpFlag
     * @return
     * @throws Exception
     */
    public void findEnpowerHSEQuestionCompleteBy(Map<String,Object> map,String enpFlag,Integer solveUser) throws Exception{
    	enpowerApiService.findEnpowerHSEQuestionCompleteBy(map,enpFlag,solveUser);

        JsonResult<Boolean> result = new JsonResult<Boolean>();

        List<EnpowerRequestHSEQuestionComplete> disaList = null;
        disaList = enpowerApiService.findEnpowerHSEQuestionCompleteBy(map,enpFlag,solveUser);

        EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
        bn.setXml("修改安全整改完成情况");//责任单位整改完成接口");
        bn.setDate_col("MODI_DATE");//这个接口只有这一个日期
//            bn.setDate_col("FOUND_DATE,END_DATE,RQ_YZ");
        bn.setPhoto_col("ZG_PHOTO");
        String data = disaList.toString();
        bn.set_value(data);
        
        updateEnpowerAndSetResult(result, bn,false);
        
    }
    
    /**
     * 上报安全整改问题接口 推送接口名: 上报安全整改问题接口  
     * @return
     */
    public JsonResult<Boolean> hSEQuestionSend(HseProblem hp) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            List<EnpowerRequestHSEQuestionSend> disaList = null;
            disaList = enpowerApiService.findEnpowerHSEQuestionSend(hp);

            EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
            bn.setXml("上报安全整改问题接口");
            bn.setDate_col("FOUND_DATE");
            bn.setPhoto_col("ZGQ_PHOTO");
            String data = disaList.toString();
            bn.set_value(data);
            
            updateEnpowerAndSetResult(result, bn,true);
        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    

    /**
     * 责任单位整改完成接口 推送接口名: 责任单位整改完成接口  这里主要为了传图片
     * @return
     */
    public JsonResult<Boolean> hSEQuestionCompleteBy(HseProblem hp,String enpFlag) throws Exception{
    	
    	//整改人查询中
		HseProblemSolve hpsagain = null;
		Integer solveUser = null ;
    	{
			//是否存在二次整改
			List<HseProblemSolve> hseSolves = new ArrayList<>();
			hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(hp.getId(), HseProblemSolveStep.teamSolveAgain.toString());
			if(hseSolves == null || hseSolves.isEmpty()){
				hseSolves = hseProblemSolveService.findByProblenIdAndSolveStep(hp.getId(), HseProblemSolveStep.teamSolve.toString());				
			}
			
			if(hseSolves != null && !hseSolves.isEmpty()){
				hpsagain = hseSolves.get(0);
				if(hpsagain==null){
					throw new RuntimeException("安全文明施工没有查到整改人,问题id="+hp.getId());
				}
			}
			solveUser=hpsagain.getSolveUser();
    	}
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            List<EnpowerRequestHSEQuestionComplete> disaList = null;
            try{
            	Map<String,Object> map = new HashMap<String,Object>();
            	if(hp!=null && hp.getId()!=null){
            		map.put("APPID", hp.getId());
            	}
            	
                disaList = enpowerApiService.findEnpowerHSEQuestionCompleteBy(map,enpFlag,solveUser);
            }catch(Exception e){
                e.printStackTrace();
                result.setCode("-2001");
                result.setMessage(e.getMessage());
            }

            EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
            bn.setXml("修改安全整改完成情况");//责任单位整改完成接口");
            bn.setDate_col("MODI_DATE");
            bn.setPhoto_col("ZG_PHOTO");
            String data = disaList.toString();
            bn.set_value(data);
            
            updateEnpowerAndSetResult(result, bn,true);
        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    
    /**
     * 修改安全整改问题接口 推送接口名: 修改安全整改完成情况  
     * 
     * RE_ZG 整改情况回复
END_DATE 完成日期
RQ_YZ 验收日期

     * @return
     */
    public JsonResult<Boolean> hSEQuestionSendUpdate(HseProblem hp,String RE_ZG,String END_DATE,String RQ_YZ,String enpFlag) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            List<EnpowerRequestHSEQuestionSend> disaList = null;
            disaList = enpowerApiService.findEnpowerHSEQuestionSendUpdate(hp, RE_ZG, END_DATE, RQ_YZ, enpFlag);

            EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
            bn.setXml("修改安全整改完成情况");
            /**完成日期  END_DATE*/
            /**验收日期 RQ_YZ */   
            bn.setDate_col("FOUND_DATE,END_DATE,RQ_YZ");
            bn.setPhoto_col("ZGQ_PHOTO");
            
            String data = disaList.toString();
            bn.set_value(data);
            
            updateEnpowerAndSetResult(result, bn,true);
        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    /**
     * 上报质量整改问题接口 推送接口名: 
     * 
		TODO ZGR--整改人
		zgDate
		ZG_DATE--整改日期
		yzDate
		YZ_DATE--验证日期
		
     * @return
     */
    public JsonResult<Boolean> qualityQuestionSend(QualityControl hp,Integer zgr,Date zgDate,Date yzDate) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            List<EnpowerRequestQualityQuestionSending> disaList = null;
            disaList = enpowerApiService.findEnpowerQualityQuestionSend(hp, zgr, zgDate, yzDate);
            logger.info("上传质量问题整改接口="+CommonUtility.toJson(disaList));

            EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
            bn.setXml("上报质量问题整改接口");
            bn.setDate_col("EDIT_DATE,ZG_DATE,YZ_DATE");
            String data = CommonUtility.toJson(disaList);
            bn.set_value(data);
            
            updateEnpowerAndSetResult(result, bn,null);
        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    /**
     * 物项退库核实接口  推送接口名: 物项退库核实接口    
     * @param ISSNO	退库单号
     * @param ISSQTY	退库量
     * @param QTY_RELEASED	核实量
     * @param STK_QTY 库存数量
     */
    public JsonResult<Boolean> materialCancelCheck(String ID,String ISSNO,String ISSQTY,String QTY_RELEASED,String WAREH_CODE,String WAREH_PLACE) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            try{
    			List<EnpowerRequestMaterialCancelCheckInfo> infoList = new ArrayList<EnpowerRequestMaterialCancelCheckInfo>();
    			EnpowerRequestMaterialCancelCheckInfo toInfo = new EnpowerRequestMaterialCancelCheckInfo();
    			
    			toInfo.setID(ID);
    			//存储仓库
    			toInfo.setWAREH_CODE(WAREH_CODE);
    			//货位
    			toInfo.setWAREH_PLACE(WAREH_PLACE);
    			//退库数量
    			toInfo.setISSQTY(ISSQTY);
    			//核实量
    			toInfo.setQTY_RELEASED(QTY_RELEASED);
    			//状态
    			toInfo.setSTATUS(EnpowerConstantVar.alreadyCheck);

    			infoList.add(toInfo);
    			EnpowerRequestXmlName bn2 = new EnpowerRequestXmlName();
    			bn2.setXml("修改物项退库信息");
    			bn2.setDate_col("CNFMED_DATE");
    			String data2 = CommonUtility.toJsonStringNull(infoList);
    			bn2.set_value(data2);
    			updateEnpowerAndSetResult(result, bn2,null);

    			List<EnpowerRequestMaterialOutCheckStock> stockList = new ArrayList<EnpowerRequestMaterialOutCheckStock>();
    			EnpowerRequestMaterialOutCheckStock stock = new EnpowerRequestMaterialOutCheckStock();
    			stock.setID(ID);
    			stock.setSTK_QTY(QTY_RELEASED);//STK_QTY);
    			stockList.add(stock);

    			EnpowerRequestXmlName bn1 = new EnpowerRequestXmlName();
    			bn1.setXml("修改物项库存信息");
    			String data1 = CommonUtility.toJsonStringNull(stockList);
    			bn1.set_value(data1);
    			updateEnpowerAndSetResult(result, bn1,null);
                
            }catch(Exception e){
                e.printStackTrace();
                result.setCode("-2001");
                result.setMessage(e.getMessage());
            }

        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    /**
     * 物项退库(未核实)查询接口
     * @return
     * @throws Exception 
     */
    public List<MaterialResult> materialCancelSearchUnexamind(String cancelNo) throws Exception{
        
    	List<MaterialResult> list = new ArrayList<MaterialResult>();
        try{
            List<EnpowerRequestMaterialCancelSearch   > disaList = null;
            	String xmlName="物项退库(未核实)查询接口";
            	String conditionKey="ISSNO";//ISSNO：退库单号

    			String requestResult = getEnpowerDBinfo( xmlName, conditionKey,cancelNo);
    			
    			
    			if (null != requestResult && !"".equals(requestResult)){
    				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {}.getType());
    				if (baseResponse.getIsSuccess()){
    					String dataInfo = baseResponse.getDataInfo();

    					List<EnpowerResponseMaterialCancelSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialCancelSearch>>() {}.getType());
    					logger.info(dataInfo);
    					if(enpList.isEmpty()==false){
    						enpList.forEach(enp->{

        				    	MaterialResult result = new MaterialResult();
    							// 退库ID
    							result.setID(enp.getID());
    							// 入库ID
//    							result.setINSTK_ID(enp.getINSTK_ID());
    							// 库存ID
//    							result.setSTK_ID(enp.getSTK_ID());
    							// 出库单号
    							result.setIssNo(enp.getISSNO());
    							// 退库日期
    							String issDate = enp.getRETURNDATE();
    							result.setIssDate(DateUtility.getDateByYmdhms(issDate));
//    							result.setRETURNDATE(enp.getRETURNDATE());
    							// 物项编码xx
    							result.setWarrantyNo(enp.getMATE_CODE());
    							// 物项名称
    							result.setName(enp.getMATE_DESCR());
    							// 退库单位xx
    							result.setIssDept(enp.getRETURN_DEPT());
    							// 退库员xx
    							result.setKeeper(enp.getRETURNER());
    							// 退库数量xx
    							result.setNumber(enp.getISSQTY());
    							// 核实数量
    							result.setNumber(enp.getQTY_RELEASED());
    							// 单价
    							if(CommonUtility.isNonEmpty(enp.getPRICE())){
    								result.setPrice(new BigDecimal(enp.getPRICE()));
    							}else{
    								result.setPrice(new BigDecimal(0));
    							}
    							// 退库金额
    							result.setCkJe(enp.getCK_JE());
    							// 核实金额
    							result.setCkJeHs(enp.getCK_JE_HS());
    							// 需求计划号
    							result.setMrpNo(enp.getMRP_NO());
    							// 质保号
    							result.setWarrantyNo(enp.getGUARANTEENO());
    							// 会计科目
    							result.setCstCode(enp.getCST_CODE());
    							// 退库类型
//    							result.setIssType(enp.getISSTYPE());
    							// 状态
//    							result.setStatus(enp.getSTATUS());
    							// 规格型号
    							result.setSpecificationNo(enp.getSPEC());
    							// 材质
    							result.setMaterial(enp.getTEXTURE());
    							// 标准
    							result.setStandard(enp.getSTAND_NO());
    							// 核安全等级
    							result.setSecurityLevel(enp.getNS_CLASS());
    							// 质保等级
    							result.setWarrantyLevel(enp.getQA_CLASS());
    							// 位号
    							result.setPositionNo(enp.getFUNC_NO());
    							// 炉批号
    							result.setFurnaceNo(enp.getLOTNO());
    							// 批号
//    							result.setHEAT_NO(enp.getHEAT_NO());
    							// 备注
    							result.setRemark(enp.getRETURNREMARK());
    							// 存储仓库
    							result.setWarehouse(enp.getWAREH_CODE());
    							// 货位
    							result.setLocation(enp.getWAREH_PLACE());
    							// 单位
    							result.setUnit(enp.getUNIT());
        					
    							list.add(result);
    						});
    					}

    					return list;
    				} else {
    					logger.error(baseResponse.getDataInfo());
    				}
    			}
    			
                logger.info("物项退库(未核实)查询接口 ="+CommonUtility.toJsonStringNull(disaList));

        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    
    /**
     * 
     * 物项出库核实接口 推送接口名: 物项出库核实接口     
     * @param ISSNO 出库单号
     * @param QTY_RELEASED 核实量
     * @param WHO_GET  领料人
     * @param ISSUSEREMARK 备注
     * @param STK_QTY 库存数量
     * @param loginUser
     * @return
     * @throws Exception
     */
    public JsonResult<Boolean> materialOutCheck(String ISSNO,String QTY_RELEASED
    		,String WHO_GET,String ISSUSEREMARK,User loginUser
    		) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            try{
            	
                //再查一次获得修改入库信息的信息
                EnpowerResponseMaterialOutSearch enp = null;
                String xmlName="物项出库(未核实)查询接口";
            	String conditionKey="ISSNO";//出库单号

    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,ISSNO);
				if (null != requestResult && !"".equals(requestResult)) {
					BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>(){}.getType());
					if (baseResponse.getIsSuccess()) {
						String dataInfo = baseResponse.getDataInfo();
						List<EnpowerResponseMaterialOutSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialOutSearch>>() {}.getType());
						if(enpList.isEmpty()==false){
							enp = enpList.get(0);
						}
					}
				}
				if(enp==null){
					throw new RuntimeException("修改物项出库信息、修改物项库存信息时，二次查询【物项出库(未核实)查询接口】出错！");
				}
                
    			List<EnpowerRequestMaterialOutCheckInfo> infoList = new ArrayList<EnpowerRequestMaterialOutCheckInfo>();
    			EnpowerRequestMaterialOutCheckInfo toInfo = new EnpowerRequestMaterialOutCheckInfo();
    			toInfo.setID(enp.getID());
    			toInfo.setWHO_GET(WHO_GET);//领料人
    			toInfo.setQTY_RELEASED(QTY_RELEASED);//核实量
    			toInfo.setISSUSEREMARK(ISSUSEREMARK);//备注
    			toInfo.setKEEPER(loginUser.getRealname());//保管员
    			toInfo.setWHEN_CNFMED(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));//核实日期
    			toInfo.setSTATUS(EnpowerConstantVar.alreadyCheck);//状态
    			infoList.add(toInfo);
    			EnpowerRequestXmlName bn2 = new EnpowerRequestXmlName();
    			bn2.setXml("修改物项出库信息");
    			bn2.setDate_col("WHEN_CNFMED");
    			String data2 = CommonUtility.toJsonStringNull(infoList);
    			bn2.set_value(data2);
    			updateEnpowerAndSetResult(result, bn2,null);
    			

    			List<EnpowerRequestMaterialOutCheckStock> stockList = new ArrayList<EnpowerRequestMaterialOutCheckStock>();
    			EnpowerRequestMaterialOutCheckStock stock = new EnpowerRequestMaterialOutCheckStock();
    			stock.setID(enp.getID());
    			stock.setSTK_QTY(QTY_RELEASED);//STK_QTY);库存数量
    			stockList.add(stock);

    			EnpowerRequestXmlName bn1 = new EnpowerRequestXmlName();
    			bn1.setXml("修改物项库存信息");
    			String data1 = CommonUtility.toJsonStringNull(stockList);
    			bn1.set_value(data1);
    			updateEnpowerAndSetResult(result, bn1,null);
                
            }catch(Exception e){
                e.printStackTrace();
                result.setCode("-2001");
                result.setMessage(e.getMessage());
            }

        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    /**
     * 物项出库(未核实)查询接口
     * @return
     * @throws Exception 
     */
    public List<MaterialResult> materialOutSearchUnexamined(String outNo,User loginUser) throws Exception{
    	List<MaterialResult> list = new ArrayList<MaterialResult>();

		
        try{
            	String xmlName="物项出库(未核实)查询接口";
            	String conditionKey="ISSNO";

    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,outNo);

    			if (null != requestResult && !"".equals(requestResult)){
    				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {}.getType());
    				if (baseResponse.getIsSuccess()){
    					String dataInfo = baseResponse.getDataInfo();

    					List<EnpowerResponseMaterialOutSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialOutSearch>>() {}.getType());
    					logger.info(dataInfo);
    					if(enpList.isEmpty()==false){
	    					enpList.stream().forEach(ret->{
	    						MaterialResult result = null;
		    					result = new MaterialResult();
    							// 出库ID
    							result.setID(ret.getID());
    							// 入库ID
//    							result.setEnpHavAppNone(ret.getINSTK_ID());
    							// 库存ID
//    							result.setEnpHavAppNone(ret.getSTK_ID());
    							// 物项编码
//    							result.setEnpHavAppNone(ret.getMATE_CODE());
    							// 物项名称
    							result.setName(ret.getMATE_DESCR());
    							// 出库单号
    							result.setIssNo(ret.getISSNO());
    							// 出库类型
//    							result.setEnpHavAppNone(ret.getISSTYPE());
    							// 开票日期
    							result.setIssDate(DateUtility.getDateByYmdhms(ret.getISSDATE()));
    							// 出库单位
    							result.setIssDept(ret.getIss_Dept());
    							// 领料员
    							result.setWhoGet(ret.getWHO_GET());
    							// 出库数量
//    							if(CommonUtility.isNonEmpty(ret.getISSQTY())){
//    								result.setNumber(Integer.valueOf(ret.getISSQTY()));
//    							}else{
//    								result.setNumber(0);
//    							}
								result.setNumber(ret.getISSQTY());
    							// 核实数量
//    							result.setEnpHavAppNone(ret.getQTY_RELEASED());
    							// 单价
    							if(ret.getPRICE()!=null){
    								result.setPrice(new BigDecimal(ret.getPRICE()));
    							}else{
    								result.setPrice(new BigDecimal("0"));
    							}
    							// 出库金额
    							result.setCkJe(ret.getCK_JE());
    							// 核实金额
    							result.setCkJeHs(ret.getCK_JE_HS());
    							// 保管员
    							result.setKeeper(loginUser.getRealname());//ret.getKEEPER());
    							// 核实日期
    							result.setWhenCnfmed(DateUtility.now());
    							// 需求计划号
    							result.setMrpNo(ret.getMrp_no());
    							// 质保号
    							result.setWarrantyNo(ret.getGUARANTEENO());
    							// 会计科目
    							result.setCstCode(ret.getCst_Code());
    							// 规格型号
    							result.setSpecificationNo(ret.getSpec());
    							// 材质
    							result.setMaterial(ret.getTexture());
    							// 标准
    							result.setStandard(ret.getStand_No());
    							//核安全等级
    							result.setSecurityLevel(ret.getNs_class());
    							// 质保等级
    							result.setWarrantyLevel(ret.getQa_class());
    							// 位号
    							result.setPositionNo(ret.getFUNC_NO());
    							// 位号
    							result.setLocation(ret.getWAREH_PLACE());
    							// 炉批号
    							result.setFurnaceNo(ret.getLOTNO());
    							// 批号
    							// result.setEnpowerhavebutWeNone(ret.getHEAT_NO());
    							// 备注
    							result.setRemark(ret.getISSUSEREMARK());
    							// 单位
    							result.setUnit(ret.getUNIT());
	    						list.add(result);
	    						
	    					});
    					}

    					return list ;
    				} else {
    					logger.error(baseResponse.getDataInfo());
    				}
    			}

        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    /**
     * 物项入库核实接口 推送接口名: 修改入库信息(按质保号)				插入库存信息
     * @param 制造商 MANUFACTURER
     * @param 生产日期 YIELD_DATE
     * @param 有效期 WARRANT_START_DATE
     * @param 保质期/月 SHELIFE_MONTHS
     * @param 保修期/月 MONTHS_WARRANT
     * @param 船次 DELIV_LOT
     * @param 存储仓库 STORAGES_CODE
     * @param 存储级别 STRG_CLASS
     * @param 货位 VESSEL_NO
     * @param 文件编号 FILE_NO
     * @param 核实数量 CHECK_QTY
     * @param 储存区域 WAREH_AREA
     * @param 是否设备工机具 GJJSBFL
     */
    public JsonResult<Boolean> materialStoreCheck(EnpowerRequestMaterialStoreCheckForm form,User loginUser) throws Exception{
        
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        try{
            List<EnpowerRequestMaterialStoreCheck > disaList = null;
            try{
            	
                //再查一次获得修改入库信息的信息
                EnpowerResponseMaterialStoreSearch enp = null;
                String xmlName="物项入库(未核实)查询(汇总)接口";//"物项入库查询接口";
            	String conditionKey="Guaranteeno";//"质保编号";

    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,form.getGUARANTEENO());
				if (null != requestResult && !"".equals(requestResult)) {
					BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>(){}.getType());
					if (baseResponse.getIsSuccess()) {
						String dataInfo = baseResponse.getDataInfo();
						List<EnpowerResponseMaterialStoreSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialStoreSearch>>() {}.getType());
						if(enpList.isEmpty()==false){
							enp = enpList.get(0);
						}
					}
				}
				if(enp==null){
					throw new RuntimeException("修改入库信息(按质保号)	、插入库存信息时，二次查询【物项入库(未核实)查询(汇总)接口】出错！");
				}
                
				//数据格式验证
				/**
				 * 修改入库信息(按质保号)　
					'WARRANT_START_DATE':'有效期' 这个是时间类型   2017/7/3 14:36:20
					
					SHELIFE_MONTHS	保质期/月  ，是NUMBER，数字类型
					MONTHS_WARRANT	保修期/月 ，是NUMBER，数字类型
				 */
				String chec1 = null;
		        try{
					chec1 = form.getWARRANT_START_DATE();//有效期
					DateUtility.sdfyMdHms.parse(chec1);
		        }catch(Exception e){
		          throw new RuntimeException("有效期　时间格式不正确！格式yyyy/MM/dd HH:mm:ss　"+chec1);
		        }
				String SHELIFE_MONTHS = form.getSHELIFE_MONTHS();
		        if(SHELIFE_MONTHS!=null && NumberUtility.isNumber(SHELIFE_MONTHS)==false){
			          throw new RuntimeException("保质期/月  ，需要是NUMBER，数字类型　"+SHELIFE_MONTHS);
		        }
				String MONTHS_WARRANT = form.getMONTHS_WARRANT();
				if(NumberUtility.isNumber(MONTHS_WARRANT)==false ){
			          throw new RuntimeException("保修期/月 ，需要是NUMBER，数字类型　"+MONTHS_WARRANT);
				}
				
    			List<EnpowerRequestMaterialStoreCheckInfo> infoList = new ArrayList<EnpowerRequestMaterialStoreCheckInfo>();
    			EnpowerRequestMaterialStoreCheckInfo toInfo = new EnpowerRequestMaterialStoreCheckInfo();
    			
    			BuildEnpowerRequestMaterialStoreCheckInfo build = new BuildEnpowerRequestMaterialStoreCheckInfo();
    			toInfo = build.setMaterialStoreCheckInfo(toInfo, enp, form, loginUser);
    			infoList.add(toInfo);
    			EnpowerRequestXmlName bn2 = new EnpowerRequestXmlName();
    			bn2.setXml("修改入库信息(按质保号)");
    			bn2.setDate_col("YIELD_DATE,WARRANT_START_DATE,CNFMED_DATE");
    			String data2 = CommonUtility.toJsonStringNull(infoList);
    			bn2.set_value(data2);
    			updateEnpowerAndSetResult(result, bn2,null);
    			

    			List<EnpowerRequestMaterialStoreCheckStock> stockList = new ArrayList<EnpowerRequestMaterialStoreCheckStock>();
    			EnpowerRequestMaterialStoreCheckStock stock = new EnpowerRequestMaterialStoreCheckStock();
    			stock = build.setMaterialStoreCheckStock(stock, enp, form);
    			stockList.add(stock);

    			EnpowerRequestXmlName bn1 = new EnpowerRequestXmlName();
    			bn1.setXml("插入库存信息");
    			bn1.setDate_col("YIELD_DATE,WARRANT_START,ROWDATE");
    			String data1 = CommonUtility.toJson(stockList);
    			bn1.set_value(data1);
    			updateEnpowerAndSetResult(result, bn1,null);
                
            }catch(Exception e){
                e.printStackTrace();
                result.setCode("-2001");
                result.setMessage(e.getMessage());
            }

        }catch(Exception e){
            e.printStackTrace();
            result.setCode("-2010");
            result.setMessage(e.getMessage());
            throw e;
        }

        return result;
    }
    /**
     * 物项入库查询接口 推送接口名: 物项入库(未核实)查询接口 
     * @return
     * @throws Exception 
     */
    public MaterialResult materialStoreSearchUnexamined(String warrantyNo) throws Exception{
        try{
            	String xmlName="物项入库(未核实)查询(汇总)接口";//"物项入库查询接口";
            	String conditionKey="Guaranteeno";//"质保编号";

    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,warrantyNo);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					logger.debug("有了数据，就需要继续往下分析了。。。。");
					List<EnpowerResponseMaterialStoreSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialStoreSearch>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){
						//一个质保单号出来只会有一条记录。
						EnpowerResponseMaterialStoreSearch enp = enpList.get(0);
							MaterialResult ret = new MaterialResult();
							ret.setID(enp.getID());
							// 物项编码
							 ret.setMATE_CODE(enp.getMATE_CODE());
							// 物项名称
							ret.setName(enp.getMATE_DESCR());
							// 质保编号
							ret.setWarrantyNo(enp.getGUARANTEENO());
							// 数量
//							if (CommonUtility.isNonEmpty(enp.getQTY())) {
//								ret.setNumber(Integer.valueOf(enp.getQTY().trim()));
//							} else {
//								ret.setNumber(0);
//							}
							ret.setNumber(enp.getQTY());
							// 材质
							ret.setMaterial(enp.getTEXTURE());
							// 规格型号
							ret.setSpecificationNo(enp.getSPEC());
							// 炉批号
							ret.setFurnaceNo(enp.getHEAT_NO());
							// 批号
							// ret.setEnpowerhavebutWeNone (enp.getLotno());
							// 核安全等级
							ret.setSecurityLevel(enp.getNS_CLASS());
							// 质保等级
							ret.setWarrantyLevel(enp.getQA_CLASS());
							// 船次件号
							ret.setShipNo(enp.getDELIV_LOT());
							// 项目代号
							// ret.setEnpowerhavebutWeNone (enp.getProj_code());
							// 公司代号
							// ret.setEnpowerhavebutWeNone (enp.getComp_code());
//							标准
							ret.setStandard(enp.getSTAND_NO());
//							订单号
							ret.setCpoNo(enp.getCPO_NO());
//							价格
							if(CommonUtility.isNonEmpty(enp.getPRICE())){
								ret.setPrice(new BigDecimal(enp.getPRICE()));
							}

							// 单位
							ret.setUnit(enp.getUNIT());

							return ret ;
					}

				} else {
					logger.error(baseResponse.getDataInfo());
					logger.error(baseResponse.getErrorMsg());
					throw new RuntimeException(baseResponse.getErrorMsg());
				}
			}
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return null;
    }
    /**
     * 查询今日出库量明细接口 推送接口名: 查询今日出库量明细接口
     * @return
     */
    public List<MaterialResult> materialTodayOutList(String ISS_DEPT,Integer pagenum,Integer pagesize) throws Exception{

    	List<MaterialResult> outList = new ArrayList<MaterialResult>();
        
		try {
			String xmlName = "查询今日出库量明细接口";
			String conditionKey = "proj_code";
	    	Map<String,Object> map = new HashMap<String,Object>();
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			//出库单位
			if(CommonUtility.isNonEmpty(ISS_DEPT)){
				map.put("ISS_DEPT", ISS_DEPT);
			}

			String requestResult = getEnpowerDBinfo(xmlName, map,pagenum,pagesize);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					//复用出库的实体
					List<EnpowerResponseMaterialOutSearch> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialOutSearch>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){
						enpList.stream().forEach(ret -> {
							MaterialResult result = new MaterialResult();
							// 出库ID
							 result.setID(ret.getID());
							// 入库ID
							// result.setEnpHavAppNone(ret.getINSTK_ID());
							// 库存ID
							// result.setEnpHavAppNone(ret.getSTK_ID());
							// 物项编码
							// result.setEnpHavAppNone(ret.getMATE_CODE());
							// 物项名称
							result.setName(ret.getMATE_DESCR());
							// 出库单号
							result.setIssNo(ret.getISSNO());
							// 出库类型
							// result.setEnpHavAppNone(ret.getISSTYPE());
							// 开票日期
							result.setIssDate(DateUtility.getDateByYmdhms(ret.getISSDATE()));
							// 出库单位
							result.setIssDept(ret.getIss_Dept());
							// 领料员
							result.setWhoGet(ret.getWHO_GET());
							// 出库数量
							// if(CommonUtility.isNonEmpty(ret.getISSQTY())){
							// result.setNumber(Integer.valueOf(ret.getISSQTY()));
							// }else{
							// result.setNumber(0);
							// }
							result.setNumber(ret.getISSQTY());
							// 核实数量
							// result.setEnpHavAppNone(ret.getQTY_RELEASED());
							// 单价
							if (ret.getPRICE() != null) {
								result.setPrice(new BigDecimal(ret.getPRICE()));
							} else {
								result.setPrice(new BigDecimal("0"));
							}
							// 出库金额
							result.setCkJe(ret.getCK_JE());
							// 核实金额
							result.setCkJeHs(ret.getCK_JE_HS());
							// 保管员
							result.setKeeper(ret.getKEEPER());
							// 核实日期
							result.setWhenCnfmed(DateUtility.getDateByYmdhms(ret.getWHEN_CNFMED()));
							// 需求计划号
							result.setMrpNo(ret.getMrp_no());
							// 质保号
							result.setWarrantyNo(ret.getGUARANTEENO());
							// 会计科目
							result.setCstCode(ret.getCst_Code());
							// 规格型号
							result.setSpecificationNo(ret.getSpec());
							// 材质
							result.setMaterial(ret.getTexture());
							// 标准
							result.setStandard(ret.getStand_No());
							// 核安全等级
							result.setSecurityLevel(ret.getNs_class());
							// 质保等级
							result.setWarrantyLevel(ret.getQa_class());
							// 位号
							result.setPositionNo(ret.getFUNC_NO());
							// 炉批号
							result.setFurnaceNo(ret.getLOTNO());
							// 批号
							// result.setEnpowerhavebutWeNone(ret.getHEAT_NO());
							// 备注
							result.setRemark(ret.getISSUSEREMARK());
							result.setTotal_count(ret.getTOTAL_COUNT());
							if(ret.getROW_NUM()!=null && ret.getROW_NUM().matches("\\d+[.]*[0]*")){
								BigDecimal bi = new BigDecimal(ret.getROW_NUM());
								result.setNo(bi.intValue());
							}
							outList.add(result);
						});

					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return outList;
    }
    /**
     * 查询今日出库量（按部门）接口
     * @return
     */
    public List<MaterialOutCountResult> materialTodayOutCount() throws Exception{

    	List<MaterialOutCountResult> outList = new ArrayList<MaterialOutCountResult>();
        
		try {
			String xmlName = "查询今日出库量（按部门）接口";
			String conditionKey = "proj_code";

			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,EnpConstant.PROJ_CODE);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					//复用出库的实体
					List<EnpowerResponseMaterialOutCount> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialOutCount>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){
						enpList.stream().forEach(ret -> {
							MaterialOutCountResult result = new MaterialOutCountResult();
							
							// 出库数量
							result.setNumber(ret.getTS());

							/**
							 * Iss_Dept		出库单位
							 */
//							private String issDept;
							result.setIssDept(ret.getISS_DEPT());
							outList.add(result);
						});

					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return outList;
    }
    /**
     * 查询今日入库量明细接口  推送接口名:  查询今日入库量明细接口  
     * @return
     */
    public List<MaterialResult> materialTodayStoreList(Integer pagenum,Integer pagesize) throws Exception{

    	List<MaterialResult> list = new ArrayList<MaterialResult>();

    	Map<String,Object> map = new HashMap<String,Object>();
		try {
			String xmlName = "查询今日入库量明细接口";
			String conditionKey = "proj_code";
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			String requestResult = getEnpowerDBinfo(xmlName, map,pagenum, pagesize);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					List<EnpowerResponseMaterialTodayStore> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialTodayStore>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){

						enpList.stream().forEach(enp->{
							MaterialResult ret = new MaterialResult();

							ret.setID(enp.getID());
							// 物项编码
							ret.setMATE_CODE(enp.getMATE_CODE());
							// 物项名称
							ret.setName(enp.getMATE_DESCR());
							// 质保编号
							ret.setWarrantyNo(enp.getGUARANTEENO());
							// 入库数量
//							ret.setNumber(enp.getQTY());
							// 库存数量
							ret.setNumber(enp.getSTK_QTY());
							// 材质
							ret.setMaterial(enp.getTEXTURE());
							// 规格型号
							ret.setSpecificationNo(enp.getSPEC());
							// 炉批号
							ret.setFurnaceNo(enp.getHEAT_NO());
							// 批号
//							ret.setLOTNO(enp.getLOTNO());
							// 核安全等级
							ret.setSecurityLevel(enp.getNS_CLASS());
							// 质保等级
							ret.setWarrantyLevel(enp.getQA_CLASS());
							// 船次件号
							ret.setShipNo(enp.getDELIV_LOT());
							// 库存ID
//							ret.setID(enp.getID());
							// 存储仓库
							ret.setWarehouse(enp.getSTORAGES_CODE());
							// 货位
							ret.setLocation(enp.getVESSEL_NO());
							// 英文名称
							ret.setNameEn(enp.getMATE_DESCR_EN());
							// 标准
							ret.setStandard(enp.getSTAND_NO());
							// 位号
							ret.setPositionNo(enp.getFUNC_NO());
							// 备注
							ret.setRemark(enp.getREMARK());
							// 核实日期
							ret.setWhenCnfmed(DateUtility.getDateByYmdhms(enp.getCNFMED_DATE()));
							// 单位
							ret.setUnit(enp.getUNIT());
							ret.setTotal_count(enp.getTOTAL_COUNT());
							ret.setRow_num(enp.getROW_NUM());

							list.add(ret);
						});
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    

    /**
     * 物项库存查询接口 
     * @return
     */
    public List<MaterialResult> materialQueryList(String MATE_CODE,String MATE_DESCR,String SPEC,String TEXTURE,String GUARANTEENO,Integer pagenum,Integer pagesize) throws Exception{

    	List<MaterialResult> list = new ArrayList<MaterialResult>();
    	Map<String,Object> map = new HashMap<String,Object>();
    	if(MATE_CODE!=null){
    		map.put("mate_code", MATE_CODE);//物项编码
    	}
    	if(MATE_DESCR!=null){
    		map.put("MATE_DESCR", MATE_DESCR);//物项名称
    	}
    	if(SPEC!=null){
    		map.put("SPEC", SPEC);//规格型号
    	}
    	if(TEXTURE!=null){
    		map.put("TEXTURE", TEXTURE);//材质
    	}
    	if(GUARANTEENO!=null){
    		map.put("GUARANTEENO", GUARANTEENO);//质保编号
    	}
    	
    	if(map.size()==0){
    		return list;
    	}
		//pageIndex 页码
		//pageSize 条数
        
		try {
			String xmlName = "物项库存查询接口";
			String conditionKey = "proj_code";
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			String requestResult = getEnpowerDBinfoLikeCondition(xmlName,map,pagenum,pagesize);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					List<EnpowerResponseMaterialTodayStore> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseMaterialTodayStore>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){

						enpList.stream().forEach(ret->{
    						MaterialResult result = null;
							result = new MaterialResult();
							// 物项编码
							result.setMATE_CODE(ret.getMATE_CODE());// 物项编码
							// 唯一ID
							result.setID(ret.getID());// 库存ID
							// 名称
							result.setName(ret.getMATE_DESCR());// 物项名称
							// 仓库
							result.setWarehouse(ret.getSTORAGES_CODE());// 存储仓库
							// 货位 货位号
							result.setLocation(ret.getVESSEL_NO());// 货位
							// 数量
							result.setNumber(ret.getSTK_QTY());// 库存数量
							// 质保编号
							result.setWarrantyNo(ret.getGUARANTEENO());// 质保编号
							// 英文名称
							result.setNameEn(ret.getMATE_DESCR_EN());// 英文名称
							// 规格型号
							result.setSpecificationNo(ret.getSPEC());// 规格型号
							// 材质
							result.setMaterial(ret.getTEXTURE());// 材质
							// 安全等级
							result.setSecurityLevel(ret.getNS_CLASS());// 核安全等级
							// 质保等级
							result.setWarrantyLevel(ret.getQA_CLASS());// 质保等级
							// 标准
							result.setStandard(ret.getSTAND_NO());// 标准
							// 位号
							result.setPositionNo(ret.getFUNC_NO());// 位号
							// 炉批号
							result.setFurnaceNo(ret.getHEAT_NO());// 炉批号
							// 船次件号
							result.setShipNo(ret.getDELIV_LOT());// 船次件号
							// 核实日期
							result.setWhenCnfmed(DateUtility.getDateByYmdhms(ret.getCNFMED_DATE()));// 核实日期
							// 单位
							result.setUnit(ret.getUNIT());// 单位
							
							result.setTotal_count(ret.getTOTAL_COUNT());
							result.setRow_num(ret.getROW_NUM());

    						list.add(result);
    						
    					});
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    
    /**
     * 
     * @param xmlName 中文接口名
     * @param conditionKey　条件key
     * @param conditionNo　条件value
     * @return
     * @throws Exception 
     */
	public String getEnpowerDBinfo( String xmlName, String conditionKey,String conditionNo) throws Exception {
	    long startTime=System.currentTimeMillis();//记录开始时间  
		EnpowerLog elog = new EnpowerLog();
		EnpowerRequest enpowerRequest = new EnpowerRequest();
		enpowerRequest.setCondition(conditionKey+"='"+conditionNo+"'");
		enpowerRequest.setXmlname(xmlName);
		enpowerRequest.setIsPage(1);
		enpowerRequest.setPageIndex(1);
		enpowerRequest.setPageSize(20000);
		
		elog.setContent(conditionKey+"='"+conditionNo+"'");
		elog.setXmlName(xmlName);
		
		enpowerLogDao.save(elog);
		
		String parms = enpowerRequest.toString();
		logger.info("[EnpowerRequest] Info: " + constantVar.getEnpowerHost() + constantVar.enpowerApiInfo + " : " +  parms);
		
//		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
		String requestResult = null ;
		if(true){
			requestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + constantVar.enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
		}else{
			requestResult = "APP调用Enpower开关关闭了";
			logger.debug("====================APP调用Enpower开关关闭了=======================");
		}
		logger.info("[EnpowerRequest] result: \n" + requestResult);
		if(requestResult!=null ){
			if(requestResult.length()>30000){
				elog.setReturnResult(requestResult.substring(0,30000));
			}else{
				elog.setReturnResult(requestResult);
			}
		}

	    long endTime=System.currentTimeMillis();//记录结束时间  
	    float excTime=(float)(endTime-startTime)/1000;  
	    elog.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
		enpowerLogDao.save(elog);
		return requestResult;
	}
    /**
     * 
     * @param xmlName 中文接口名
     * @param conditionKey　条件key
     * @param conditionNo　条件value
     * @return
     * @throws Exception 
     */
	public String getEnpowerDBinfo(String xmlName,Map<String,Object> map,Integer pageIndex,Integer pageSize) throws Exception {
	    long startTime=System.currentTimeMillis();//记录开始时间  
		EnpowerLog elog = new EnpowerLog();
		EnpowerRequest enpowerRequest = new EnpowerRequest();
		String codi = computerCondition(map);
		enpowerRequest.setCondition(codi);
		enpowerRequest.setXmlname(xmlName);
		enpowerRequest.setIsPage(1);
		enpowerRequest.setPageIndex(pageIndex);
		enpowerRequest.setPageSize(pageSize);

		elog.setContent(codi);
		elog.setXmlName(xmlName);
		if(EnpowerLog.withoutSaving.contains(xmlName) == false){
			enpowerLogDao.save(elog);
		}
		
		String parms = enpowerRequest.toString();
		logger.info("[EnpowerRequest] Info: " + constantVar.getEnpowerHost() + constantVar.enpowerApiInfo + " : " +  enpowerRequest.toString());
		
//		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
		String requestResult = null ;
		if(true){
			requestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + constantVar.enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
		}else{
			requestResult = "APP调用Enpower开关关闭了";
			logger.debug("====================APP调用Enpower开关关闭了=======================");
		}
		if(EnpowerLog.withoutSaving.contains(xmlName) == false){
			logger.info("[EnpowerRequest] result: \n" + requestResult);
		}
		if(requestResult!=null ){
			if(requestResult.length()>30000){
				elog.setReturnResult(requestResult.substring(0,30000));
			}else{
				elog.setReturnResult(requestResult);
			}
		}
		if(EnpowerLog.withoutSaving.contains(xmlName) == false){
		    long endTime=System.currentTimeMillis();//记录结束时间  
		    float excTime=(float)(endTime-startTime)/1000;  
		    elog.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
			enpowerLogDao.save(elog);
		}

		return requestResult;
	}
    /**
     * 
     * @param xmlName 中文接口名
     * @param conditionKey　条件key
     * @param conditionNo　条件value
     * @return
     * @throws Exception 
     */
	public String getEnpowerDBinfoLikeCondition(String xmlName,Map<String,Object> map,Integer pageIndex,Integer pageSize) throws Exception {

	    long startTime=System.currentTimeMillis();//记录开始时间  
		EnpowerLog elog = new EnpowerLog();
		
		EnpowerRequest enpowerRequest = new EnpowerRequest();
		String condi = computerLikeCondition(map);
		enpowerRequest.setCondition(condi);
		enpowerRequest.setXmlname(xmlName);
		enpowerRequest.setIsPage(1);
		enpowerRequest.setPageIndex(pageIndex);
		enpowerRequest.setPageSize(pageSize);

		elog.setContent(condi);
		elog.setXmlName(xmlName);
		enpowerLogDao.save(elog);
		
		logger.info("[EnpowerRequest] Info : " + enpowerRequest.toString());
		String parms = enpowerRequest.toString();//MyURLEncoder.encode(enpowerRequest.toString(),"GBK");
		logger.info("[EnpowerRequest] Info encode: " + constantVar.getEnpowerHost() + constantVar.enpowerApiInfo + " : " +  parms);
		
//		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
		String requestResult = null ;
		if(true){
			requestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + constantVar.enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
		}else{
			requestResult = "APP调用Enpower开关关闭了";
			logger.debug("====================APP调用Enpower开关关闭了=======================");
		}
		logger.info("[EnpowerRequest] result: \n" + requestResult);
		if(requestResult!=null ){
			if(requestResult.length()>30000){
				elog.setReturnResult(requestResult.substring(0,30000));
			}else{
				elog.setReturnResult(requestResult);
			}
		}

	    long endTime=System.currentTimeMillis();//记录结束时间  
	    float excTime=(float)(endTime-startTime)/1000;  
	    elog.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
		enpowerLogDao.save(elog);
		return requestResult;
	}
    /**
     * 
     * @param xmlName 中文接口名
     * @param conditionKey　条件key
     * @param conditionNo　条件value
     * @return
     * @throws Exception 
     */
	public String getEnpowerDBinfo(String xmlName,Map<String,Object> map,StringBuffer condition,Integer pageIndex,Integer pageSize) throws Exception {

	    long startTime=System.currentTimeMillis();//记录开始时间  
		EnpowerLog elog = new EnpowerLog();
		EnpowerRequest enpowerRequest = new EnpowerRequest();
		String condi = computerCondition(map)+condition.toString();
		enpowerRequest.setCondition(condi);
		enpowerRequest.setXmlname(xmlName);
		enpowerRequest.setIsPage(1);
		enpowerRequest.setPageIndex(pageIndex);
		enpowerRequest.setPageSize(pageSize);

		elog.setContent(condi);
		elog.setXmlName(xmlName);
		enpowerLogDao.save(elog);
		String parms = enpowerRequest.toString();
		logger.info("[EnpowerRequest] Info: " + constantVar.getEnpowerHost() + constantVar.enpowerApiInfo + " : " +  parms);
		
//		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
		String requestResult = null ;
		if(true){
			requestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + constantVar.enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
		}else{
			requestResult = "APP调用Enpower开关关闭了";
			logger.debug("====================APP调用Enpower开关关闭了=======================");
		}
		logger.info("[EnpowerRequest] result: \n" + requestResult);
		if(requestResult!=null ){
			if(requestResult.length()>30000){
				elog.setReturnResult(requestResult.substring(0,30000));
			}else{
				elog.setReturnResult(requestResult);
			}
		}
	    long endTime=System.currentTimeMillis();//记录结束时间  
	    float excTime=(float)(endTime-startTime)/1000;  
	    elog.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
		enpowerLogDao.save(elog);
		return requestResult;
	}

	/**
	 * 公共方法 更新Enpower接口内容,并设置返回值.
	 * 其中会根据变量配置是否调用.
	 * @param result
	 * @param bn
	 * @throws Exception 
	 */
	public void updateEnpowerAndSetResult(JsonResult<Boolean> result, EnpowerRequestXmlName bn,Boolean notStore) throws Exception {
		String reqPara = bn.toString();
		if(notStore!=null && notStore){//代表是图片
			reqPara = bn.toStringNoReplace();
		}
		logger.info("[EnpowerRequestSendEquipment] Info: " + constantVar.getEnpowerHost() + enpowerupdateData + " : " +  reqPara);
		
		//存入文件中

	    long startTime=System.currentTimeMillis();//记录开始时间  
		EnpowerLog elog = new EnpowerLog();
		
		String userRequestResult = null;
		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
		elog.setContent(bn.toStringFile());
		elog.setOnOff(seting);
		elog.setXmlName(bn.getXml());
		if(notStore==null || notStore==false){
			enpowerLogDao.save(elog);
		}
//		
		if("ON".equals(seting)){
			userRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerupdateData, reqPara, "application/x-www-form-urlencoded", "GBK");
		}else{

			//记录原始内容
			FileUtility ut = new FileUtility();
			SimpleDateFormat sdf =new SimpleDateFormat("MM_dd_HHmmssS");
			String rollingfile = sdf.format(Calendar.getInstance().getTime())+"_enpOff.txt";
			String filePath_ = com.easycms.common.logic.context.ContextPath.fileBasePath+com.easycms.common.util.FileUtils.errorEnpowerImg;
	        String filenameTemp = filePath_+"/"+rollingfile;//文件路径+名称+文件类型
				StringBuffer sb = new StringBuffer(2048);
				sb.append(constantVar.getEnpowerHost()).append(enpowerupdateData).append("\r\n");
				sb.append(bn.toStringFile());
				
				ut.createFile(filenameTemp, sb.toString());
			
			userRequestResult = "APP调用Enpower开关关闭了";
			logger.debug("====================APP调用Enpower开关关闭了=======================");
			return;

		}
		elog.setReturnResult(userRequestResult);
		//
		if (null != userRequestResult && !"".equals(userRequestResult) ){
			BaseResponse baseResponse = gson.fromJson(userRequestResult, new TypeToken<BaseResponse>() {}.getType());
			elog.setIsSuccess(baseResponse.getIsSuccess()+"");
			if (baseResponse.getIsSuccess()){
				String returnInfo = baseResponse.getDataInfo();
				result.setMessage(returnInfo);
				result.setResponseResult(true);
				elog.setDataInfo(returnInfo);
				logger.info(CommonUtility.toJsonStringNull(returnInfo));
			} else {
				logger.error(baseResponse.getErrorMsg());
				logger.error(baseResponse.getDataInfo());
			}
			
			boolean failure = baseResponse.getIsSuccess()==false || baseResponse.getDataInfo()==null 
					|| (baseResponse.getDataInfo().indexOf(EnpowerStatus.SUCCESS)!=-1 || baseResponse.getDataInfo().indexOf(EnpowerStatus.SUCCESS2)!=-1)==false;
			elog.setUpdateState(failure);

			//记录原始内容
			FileUtility ut = new FileUtility();
			SimpleDateFormat sdf =new SimpleDateFormat("MM_dd_HHmmssS");
			String rollingfile = sdf.format(Calendar.getInstance().getTime())+(failure==false?"_ok":"_err")+".txt";
			String filePath_ = com.easycms.common.logic.context.ContextPath.fileBasePath+com.easycms.common.util.FileUtils.errorEnpowerImg;
	        String filenameTemp = filePath_+"/"+rollingfile;//文件路径+名称+文件类型
			if((notStore!=null && notStore) || failure){
				StringBuffer sb = new StringBuffer(2048);
				sb.append(constantVar.getEnpowerHost()).append(enpowerupdateData).append("\r\n");
				sb.append(bn.toStringFile());
				sb.append("\r\n");
				sb.append(userRequestResult);
				
				ut.createFile(filenameTemp, sb.toString());
			}
			
			if(failure){
				
				throw new RuntimeException("Enpower接口调用异常！！enpower invoke error with details in file :"+filenameTemp +"   "+baseResponse.getErrorMsg()+baseResponse.getDataInfo());
			}
			
		}else{
			throw new RuntimeException("Enpower接口调用没有返回！！");
		}
		if(notStore==null || notStore==false){

		    long endTime=System.currentTimeMillis();//记录结束时间  
		    float excTime=(float)(endTime-startTime)/1000;  
		    elog.setWithTime(new BigDecimal(endTime-startTime).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP));
			elog.setReturnResult(userRequestResult);
			enpowerLogDao.save(elog);
		}

	}
	public static String computerCondition(Map<String,Object> map){
		if(map!=null && map.size()>0){
			StringBuffer sb = new StringBuffer();
			
			Set<Map.Entry<String,Object>> set = map.entrySet();
			boolean first = true ;
			for(Map.Entry<String,Object> st:set){
			
				if(st.getValue()!=null && !"".equals(st.getValue())){
					if(first){
						first = false ;
					}else{
						sb.append(" and ");
					}
					sb.append(st.getKey());
					sb.append("='");
					sb.append(st.getValue());
					sb.append("'");
				}
			}
			
			return sb.toString();
		}
		return "";
	}
	public static String computerLikeCondition(Map<String,Object> map){
		if(map!=null && map.size()>0){
			StringBuffer sb = new StringBuffer();
			
			Set<Map.Entry<String,Object>> set = map.entrySet();
			boolean first = true ;
			for(Map.Entry<String,Object> st:set){
			
				if(st.getValue()!=null && !"".equals(st.getValue())){
					if(first){
						first = false ;
					}else{
						sb.append(" and ");
					}
					sb.append(st.getKey());
					sb.append(" like '%");
					sb.append(st.getValue());
					sb.append("%'");
				}
			}
			
			return sb.toString();
		}
		return "";
	}
	
	/**
	 * 调用Enpower接口同步推送状态，首先采用对象参数，否则使用id参数
	 * 插入焊口条目信息　更新焊口条目信 不需要了
	 * @param plan
	 * @param rollingPlanId
	 * @return
	 */
//		public JsonResult<Boolean> updateEnpowerStatus(RollingPlan plan,Integer rollingPlanId,String dosage) throws Exception{
//			if(plan==null){
//				if(rollingPlanId==null){
//					throw new RuntimeException("参数异常错误！");
//				}
//				plan = rollingPlanService.findById(rollingPlanId);
//			}
//			
//			JsonResult<Boolean> result = new JsonResult<Boolean>();
//			try{
//
//				/**
//				 * 如果不是管道的：
//					直接调用接口：“修改作业条目信息”
//					
//					如果是管道的：
//					一、班长刚分派组长时调用接口“插入焊口条目信息”，状态是已分派
//					二、其它调用接口“修改作业条目信息”的情况，改为调用接口“更新焊口条目信息”
//				 */
//				//如果是管道计划条目，则调用接口　插入焊口条目信息
//				if("GDJH".equals(plan.getType()) ){
//					Boolean insWeld = plan.getInsertWeld();
//
//					if(insWeld==null || insWeld == false){//发点通知单信息 与作业条目一对一
//
//						List<EnpowerRequestPlanInsertWeld> listweld = new ArrayList<EnpowerRequestPlanInsertWeld>();
//						EnpowerRequestPlanInsertWeld planStatus = null;
//						planStatus = rollingPlanApiService.findEnpowerStatusInsertWeldRollingPlanId(plan,rollingPlanId);
//						
//						if(dosage==null || "".equals(dosage)){
//							if(plan.getRealProjectcost()!=null && !"".equals(plan.getRealProjectcost())){
//								planStatus.setRealProjectcost(plan.getRealProjectcost());
//							}
//						}else{
//							planStatus.setRealProjectcost(dosage);
//						}
//						listweld.add(planStatus);
//						logger.info("插入焊口条目信息:"+CommonUtility.toJsonStringNull(planStatus));
//
//						EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
//						bn.setXml("插入焊口条目信息");
//						bn.setDate_col("START_DATE,END_DATE");
//						String data = CommonUtility.toJsonStringNull(listweld);
//						bn.set_value(data);
//						
//						updateEnpowerAndSetResult(result, bn,null);
//
//						plan.setInsertWeld(true);
//						rollingPlanDao.save(plan);
//						
//					}else{
//
//						List<EnpowerRequestPlanUpdateWeld> listweld = new ArrayList<EnpowerRequestPlanUpdateWeld>();
//						EnpowerRequestPlanUpdateWeld planStatus = null;
//						planStatus = rollingPlanApiService.findEnpowerStatusUpdateWeldRollingPlanId(plan,rollingPlanId);
//						
//						if(dosage==null || "".equals(dosage)){
//							if(plan.getRealProjectcost()!=null && !"".equals(plan.getRealProjectcost())){
//								planStatus.setRealProjectcost(plan.getRealProjectcost());
//							}
//						}else{
//							planStatus.setRealProjectcost(dosage);
//						}
//						listweld.add(planStatus);
//						logger.info("更新焊口条目信息:"+CommonUtility.toJsonStringNull(planStatus));
//
//						EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
//						bn.setXml("更新焊口条目信息");
//						bn.setDate_col("START_DATE,END_DATE");
//						String data = CommonUtility.toJsonStringNull(listweld);
//						bn.set_value(data);
//						
//						updateEnpowerAndSetResult(result, bn,null);
//					}
//				}else{
//
//					List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
//					EnpowerRequestPlanStatusSyn planStatus = rollingPlanApiService.findEnpowerStatusSynByRollingPlanId(plan,rollingPlanId);
//					
//					EnpowerRequestPlanInsertWeld addWeld = null;
//					
//					if(dosage==null || "".equals(dosage)){
//						if(plan.getRealProjectcost()!=null && !"".equals(plan.getRealProjectcost())){
//							planStatus.setRealProjectcost(plan.getRealProjectcost());
//						}
//					}else{
//						planStatus.setRealProjectcost(dosage);
//					}
//					list.add(planStatus);
//					logger.info("修改作业条目信息:"+CommonUtility.toJsonStringNull(planStatus));
//					
//					EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
//					bn.setXml("修改作业条目信息");
//					bn.setDate_col("START_DATE,END_DATE");
//					String data = CommonUtility.toJsonStringNull(list);
//					bn.set_value(data);
//					
//					updateEnpowerAndSetResult(result, bn,null);
//				}
//
//			}catch(Exception e){
//				e.printStackTrace();
//				result.setCode("-2010");
//				result.setMessage("修改作业条目信息！"+e.getMessage()+" rollingPlanId=" +rollingPlanId +" plan="+(plan!=null?plan.getId():""));
//				throw e;
//			}
//
//			return result;
//		}
	

    /**
     * 安全编码信息查询
     * @return
     */
    public List<EnpowerResponseHseCode> getHseCodeList(EnpowerResponseHseCode paramIn) throws Exception{

    	List<EnpowerResponseHseCode> outList = new ArrayList<EnpowerResponseHseCode>();
        
		try {
			String xmlName = "安全编码信息查询";
			String conditionKey = "proj_code";
	    	Map<String,Object> map = new HashMap<String,Object>();
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			//出库单位
			if(CommonUtility.isNonEmpty(paramIn.getStand())){
				map.put("STAND", paramIn.getStand());
			}
			if(CommonUtility.isNonEmpty(paramIn.getParentId())){
				map.put("PARENT_ID", paramIn.getParentId());
			}
			if(CommonUtility.isNonEmpty(paramIn.getId())){
				map.put("ID", paramIn.getId());
			}

			String requestResult = getEnpowerDBinfo(xmlName, map,1,100000);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
//					logger.info(dataInfo);
					List<EnpowerResponseHseCode> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseHseCode>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){
						
						outList.addAll(enpList);
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return outList;
    }
    /**
     * 区域房间标高信息查询
     * @return
     */
    public List<EnpowerResponseRoomLevel> getRoomLevelList(EnpowerResponseRoomLevel paramIn) throws Exception{
		try {
			String xmlName = "区域房间标高信息查询";
			String conditionKey = "proj_code";
	    	Map<String,Object> map = new HashMap<String,Object>();
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数

			String requestResult = getEnpowerDBinfo(xmlName, map,1,100000);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					List<EnpowerResponseRoomLevel> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseRoomLevel>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){
						return enpList;
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return Collections.emptyList();
    }
    
}
