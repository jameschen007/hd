package com.easycms.hd.api.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.api.response.MaterialExtractPageResult;
import com.easycms.hd.api.response.MaterialExtractResult;
import com.easycms.hd.api.response.MaterialResult;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.api.service.MaterialApiService;
import com.easycms.hd.api.service.MaterialExtractApiService;
import com.easycms.hd.material.domain.MaterialExtract;
import com.easycms.hd.material.service.MaterialExtractService;
import com.easycms.hd.material.service.MaterialService;
import com.easycms.management.user.service.DepartmentService;

@Service("materialExtractApiService")
public class MaterialExtractApiServiceImpl implements MaterialExtractApiService {
	
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private DepartmentApiService departmentApiService;
	@Autowired
	private MaterialService materialService;
	@Autowired
	private MaterialApiService materialApiService;
	@Autowired
	private MaterialExtractService materialExtractService;

	@Override
	public MaterialExtractResult materialExtractTransferToMaterialExtractResult(MaterialExtract materialExtract, Integer userId) {
		if (null != materialExtract) {
			MaterialExtractResult materialExtractResult = new MaterialExtractResult();
			materialExtractResult.setId(materialExtract.getId());
			materialExtractResult.setWarrantyNo(materialExtract.getWarrantyNo());
			materialExtractResult.setRemark(materialExtract.getRemark());
			materialExtractResult.setAccounting(materialExtract.getAccounting());
			materialExtractResult.setConfirmCount(materialExtract.getConfirmCount());
			if (null != materialExtract.getDepartmentId()) {
				materialExtractResult.setDepartment(departmentApiService.departmentTransferToDepartmentResult(departmentService.findById(materialExtract.getDepartmentId())));
			}
			materialExtractResult.setExtractCount(materialExtract.getExtractCount());
			materialExtractResult.setExtractNo(materialExtract.getExtractNo());
			materialExtractResult.setInvoiceDate(materialExtract.getInvoiceDate());
			materialExtractResult.setKeeper(materialExtract.getKeeper());
			materialExtractResult.setPlanNo(materialExtract.getPlanNo());
			materialExtractResult.setReceiver(materialExtract.getReceiver());
			materialExtractResult.setSignDate(materialExtract.getSignDate());
			if (null != materialExtract.getMaterialId()) {
				MaterialResult materialResult = materialApiService.materialTransferToMaterialResult(materialService.findById(materialExtract.getMaterialId()), userId);
				if (null != materialResult) {
					materialExtractResult.setMaterial(materialResult);
					BigDecimal totalPrice = materialResult.getPrice().multiply(new BigDecimal(materialExtract.getConfirmCount()));
					materialExtractResult.setTotalPrice(totalPrice);
				}
			}
			
			return materialExtractResult;
		}
		return null;
	}
	
	@Override
	public List<MaterialExtractResult> materialExtractTransferToMaterialExtractResult(List<MaterialExtract> materialExtracts, Integer userId) {
		if (null != materialExtracts && !materialExtracts.isEmpty()) {
			List<MaterialExtractResult> materialExtractResults = materialExtracts.stream().map(materialExtract -> {
				return materialExtractTransferToMaterialExtractResult(materialExtract, userId);
			}).collect(Collectors.toList());
			
			return materialExtractResults;
		}
		return null;
	}

	@Override
	public MaterialExtractPageResult getMaterialExtractPageResult(Integer userId, Integer departmentId, Page<MaterialExtract> page) {
		if (null != userId) {
			MaterialExtractPageResult materialExtractPageResult = new MaterialExtractPageResult();
			
			page = materialExtractService.findByPage(departmentId, page);
			
			if (null != page.getDatas()) {
				List<MaterialExtractResult> materialExtractResults = page.getDatas().stream().map(materialExtract -> {
					return this.materialExtractTransferToMaterialExtractResult(materialExtract, userId);
				}).collect(Collectors.toList());
				
				materialExtractPageResult.setPageCounts(page.getPageCount());
				materialExtractPageResult.setPageNum(page.getPageNum());
				materialExtractPageResult.setPageSize(page.getPagesize());
				materialExtractPageResult.setTotalCounts(page.getTotalCounts());
				materialExtractPageResult.setData(materialExtractResults);
			}
			
			return materialExtractPageResult;
		}
		return null;
	}
}
