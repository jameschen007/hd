package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.request.ConferenceFeedbackRequestForm;
import com.easycms.hd.api.request.ConferenceMarkRequestForm;
import com.easycms.hd.api.response.ConferenceFeedbackResult;
import com.easycms.hd.api.service.ConferenceFeedbackApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.conference.domain.ConferenceFeedback;
import com.easycms.hd.conference.domain.ConferenceFeedbackMark;
import com.easycms.hd.conference.service.ConferenceFeedbackMarkService;
import com.easycms.hd.conference.service.ConferenceFeedbackService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Service("conferenceFeedbackApiService")
public class ConferenceFeedbackApiServiceImpl implements ConferenceFeedbackApiService {
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private ConferenceFeedbackService conferenceFeedbackService;
	@Autowired
	private ConferenceFeedbackMarkService conferenceFeedbackMarkService;
	@Override
	public ConferenceFeedbackResult conferenceFeedbackTransferToConferenceFeedbackResult(
			ConferenceFeedback conferenceFeedback) {
		if (null != conferenceFeedback) {
			ConferenceFeedbackResult conferenceFeedbackResult = new ConferenceFeedbackResult();
			conferenceFeedbackResult.setId(conferenceFeedback.getId());
			conferenceFeedbackResult.setMessage(conferenceFeedback.getMessage());
			conferenceFeedbackResult.setFeedbackTime(conferenceFeedback.getCreateOn());
			User user = userService.findUserById(conferenceFeedback.getCreateBy());
			if (null != user) {
				conferenceFeedbackResult.setUser(userApiService.userTransferToUserResult(user,true));//显示原始的角色，没有roleType显示
			}
			return conferenceFeedbackResult;
		}
		return null;
	}
	@Override
	public boolean insert(ConferenceFeedbackRequestForm form, User user) {
		if (null != form && null != user) {
			ConferenceFeedback conferenceFeedback = new ConferenceFeedback();
			conferenceFeedback.setConferenceid(form.getConferenceId());
			conferenceFeedback.setCreateBy(user.getId());
			conferenceFeedback.setCreateOn(new Date());
			conferenceFeedback.setMessage(form.getMessage());
			if (null != conferenceFeedbackService.add(conferenceFeedback)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean markRead(ConferenceMarkRequestForm form, User user) {
		if (null != form) {
			ConferenceFeedbackMark conferenceFeedbackMark = conferenceFeedbackMarkService.findByConferenceIdAndUserId(form.getConferenceId(), user.getId());
			if (null != conferenceFeedbackMark) {
				conferenceFeedbackMarkService.markRead(conferenceFeedbackMark.getId(), new Date());
			} else {
				conferenceFeedbackMark = new ConferenceFeedbackMark();
				conferenceFeedbackMark.setConferenceid(form.getConferenceId());
				conferenceFeedbackMark.setUserId(user.getId());
				conferenceFeedbackMark.setMarkTime(new Date());
				conferenceFeedbackMark.setCreateBy(user.getId());
				conferenceFeedbackMark.setCreateOn(new Date());
				conferenceFeedbackMarkService.add(conferenceFeedbackMark);
			}
			
			return true;
		}
		return false;
	}
	
	@Override
	public List<ConferenceFeedbackResult> conferenceFeedbackTransferToConferenceFeedbackResult(
			List<ConferenceFeedback> conferenceFeedbacks) {
		if (null != conferenceFeedbacks && !conferenceFeedbacks.isEmpty()) {
			List<ConferenceFeedbackResult> conferenceFeedbackResults = new ArrayList<ConferenceFeedbackResult>();
			for (ConferenceFeedback conferenceFeedback : conferenceFeedbacks) {
				conferenceFeedbackResults.add(conferenceFeedbackTransferToConferenceFeedbackResult(conferenceFeedback));
			}
			return conferenceFeedbackResults;
		}
		return null;
	}
}
