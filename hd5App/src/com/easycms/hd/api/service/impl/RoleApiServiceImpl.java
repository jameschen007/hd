package com.easycms.hd.api.service.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.hd.api.domain.AuthModule;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.dao.UserDao;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;

@Service("roleApiService")
public class RoleApiServiceImpl implements RoleApiService {

	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private DepartmentApiService departmentApiService;
	@Autowired
	private UserDao userDao;

	@Override
	public Set<AuthRole> getAuthRole(User user,boolean showOriginRole) {//boolean showOriginRole
		Set<AuthRole> roles = new HashSet<AuthRole>();
		if (null != user) {
			if (user.getRoles() != null && user.getRoles().size() > 0) {
				for (Role r : user.getRoles()) {
					Set<String> roleNameList = variableSetService.findKeyByValue(r.getName(), "roleType");
					AuthRole authRole = new AuthRole();
					authRole.setId(r.getId());
					authRole.setName(r.getName());

					Set<String> roleNameList23 = new TreeSet<String>();
					if(showOriginRole){//显示原始的角色，没有roleType显示
						roles.add(authRole);
						authRole.setDepartment(departmentApiService.departmentTransferToDepartmentResult(user.getDepartment()));
//							authRole.setDepartment(departmentApiService.departmentTransferToDepartmentResult(departmentService.findByEnpowerId(r.getEnpowerDepartmentId())));
						
						roles.add(authRole);
					}else if (roleNameList != null && roleNameList.size() > 0) {
						// 多级问题解决人角色转换统一输出 - solver
						for (String rol : roleNameList) {
							if (rol.matches("solver\\d+")) {
								roleNameList23.add(ConstantVar.supervisor);
							} else {
								roleNameList23.add(rol);
							}
						}
						roleNameList = roleNameList23;
						
						//[2017-12-23]凡是没有找到角色类型的，都不再返回给客户端
						authRole.setRoleType(roleNameList);
						
						authRole.setDepartment(departmentApiService.departmentTransferToDepartmentResult(user.getDepartment()));
//							authRole.setDepartment(departmentApiService.departmentTransferToDepartmentResult(departmentService.findByEnpowerId(r.getEnpowerDepartmentId())));
						
						roles.add(authRole);
					}
				}
				return roles;
			}
		}
		return roles;
	}

	@Override
	public Set<AuthModule> getAuthModule(User user) {
		Set<AuthModule> module = new HashSet<AuthModule>();
		if (null != user) {
			// 管道问题提到“技术管理中心”，技术管理中心中心的人登录时，需要将岗位职务查询出来，如果是主任等通用的岗位职务，需要再判断一次部门
			// 管道主管plumbingSupervisor
			// 焊接主管weldSupervisor
			// 主任Director
			if (user.getModules() != null && user.getModules().size() > 0) {
				for (Modules m : user.getModules()) {
					AuthModule x = new AuthModule();
					x.setId(m.getId());
					x.setName(m.getName());
					x.setType(m.getType());
					module.add(x);
				}
			}
		}
		return module;
	}
	/**
	 * 判断人员是否具有某角色(经过变量表配置的)权限,只要存在其中之一即返true
	 * */
		@Override
		public boolean checkRole(User user,String... roleType){
			boolean f = false ;		
			
			Set<AuthRole> loginRoles = getAuthRole(user,false);
			if (null == loginRoles) {
				return f;
			}
			
			Set<String> loginRoleType = new HashSet<String>();
			for (AuthRole role : loginRoles) {
				loginRoleType.addAll(role.getRoleType());
			}
			
			for(String ab: roleType){
				if (loginRoleType.contains(ab)) {
					return true;
				}
			}
			
			
	//
//			if (loginRoleType.contains(roleType)) {
//				
//			}
//			
	//
//			if (loginRoleType.contains("witness_member_qc2")) {
	//
//			} else if (loginRoleType.contains("witness_team_qc1") || loginRoleType.contains("witness_team_qc2")) {
//				
//			}
			return f ;
		}
		

/**
 * 判断人员是否具有某角色(经过变量表配置的)权限,入参为变量表的常量
 * */
	@Override
	public boolean checkAllRole(User user,String... roleType){
		boolean f = false ;		
		
		Set<AuthRole> loginRoles = getAuthRole(user,false);
		if (null == loginRoles) {
			return f;
		}
		
		Set<String> loginRoleType = new HashSet<String>();
		for (AuthRole role : loginRoles) {
			loginRoleType.addAll(role.getRoleType());
		}
		
		for(String ab: roleType){
			if (loginRoleType.contains(ab)) {
				f = true;
			}else{
				f=false ;
			}
		}
		
		return f ;
	}

/**
 * 判断人员是否具有某角色(经过变量表配置的)权限,入参为变量表的常量
 * */
	@Override
	public boolean checkAllRole(Integer userId,String... roleType){
		return checkAllRole(userDao.findById(userId),roleType) ;
	}
	
}
