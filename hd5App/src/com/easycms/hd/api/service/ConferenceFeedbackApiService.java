package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.hd.api.request.ConferenceFeedbackRequestForm;
import com.easycms.hd.api.request.ConferenceMarkRequestForm;
import com.easycms.hd.api.response.ConferenceFeedbackResult;
import com.easycms.hd.conference.domain.ConferenceFeedback;
import com.easycms.management.user.domain.User;

public interface ConferenceFeedbackApiService {
	ConferenceFeedbackResult conferenceFeedbackTransferToConferenceFeedbackResult(ConferenceFeedback conferenceFeedback);
	
	boolean insert(ConferenceFeedbackRequestForm form, User user);

	List<ConferenceFeedbackResult> conferenceFeedbackTransferToConferenceFeedbackResult(List<ConferenceFeedback> conferenceFeedbacks);

	boolean markRead(ConferenceMarkRequestForm form, User user);
}
