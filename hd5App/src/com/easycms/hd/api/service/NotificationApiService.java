package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.request.NotificationRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.NotificationPageResult;
import com.easycms.hd.api.response.NotificationResult;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.management.user.domain.User;

public interface NotificationApiService {
	NotificationResult notificationTransferToNotificationResult(Notification notification, Integer userId, ConferenceApiRequestType type, boolean feedback);
	
	JsonResult<Integer> insert(NotificationRequestForm notificationRequestForm, User user, ConferenceSource source);
	
	NotificationPageResult getNotificationPageResult(ConferenceApiRequestType type, Integer userId, Page<Notification> page);

	List<NotificationResult> notificationTransferToNotificationResult(List<Notification> notifications, Integer userId, ConferenceApiRequestType type);

	boolean batchDelete(Integer[] ids);
	/**批量取消会议，并能参会人员发送推送消息*/
	boolean batchCancel(Integer[] ids);
}
