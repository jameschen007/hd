package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.hd.api.response.DepartmentResult;
import com.easycms.hd.api.response.DepartmentUserResult;
import com.easycms.management.user.domain.Department;

public interface DepartmentApiService {
	/**
	 * 获取所有的部门信息
	 * @return
	 */
	List<DepartmentUserResult> getAllDepartment();
	/**
	 * 
	获取下级部门的人员列表,不包含本部门下的人员
	 * @return
	 */
	List<DepartmentUserResult> memberOfChild(Integer departmentId);
	/**
	 * 获取所有的部门信息
	 * @return
	 */
	List<DepartmentUserResult> getDepartmentd(String departmentName);
	

	/**
	 * 根据部门id查部门列表，如果为空，查询根节点
	 * @param departmentId
	 * @return
	 */
	public List<DepartmentUserResult> findDeptList(Integer departmentId) ;
	/**
	 * 转换部门信息
	 * @param department
	 * @param getuser
	 * @return
	 */
	DepartmentUserResult departmentTransferToDepartmentUserResult(Department department, boolean getuser,boolean showOriginRole);
	/**
	 * 转换部门信息
	 * @param departments
	 * @param getuser
	 * @return
	 */
	List<DepartmentUserResult> departmentTransferToDepartmentUserResult(List<Department> departments, boolean getuser,boolean showOriginRole);
	/**
	 * 转换部门信息
	 * @param department
	 * @return
	 */
	DepartmentResult departmentTransferToDepartmentResult(Department department);
	/**
	 * 转换部门信息
	 * @param departments
	 * @return
	 */
	List<DepartmentResult> departmentTransferToDepartmentResult(List<Department> departments);
}
