package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.hd.api.enpower.domain.EnpowerUserEntity;
import com.easycms.hd.api.response.UserResult;
import com.easycms.management.user.domain.User;

public interface UserApiService {
	UserResult userTransferToUserResult(User user,boolean showOriginRole);
	
	List<UserResult> userTransferToUserResult(List<User> users,boolean showOriginRole);
	
	boolean insertBatch(List<EnpowerUserEntity> enpowerUsers);
}
