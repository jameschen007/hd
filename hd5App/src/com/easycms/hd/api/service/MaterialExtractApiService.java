package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.response.MaterialExtractPageResult;
import com.easycms.hd.api.response.MaterialExtractResult;
import com.easycms.hd.material.domain.MaterialExtract;

public interface MaterialExtractApiService {

	List<MaterialExtractResult> materialExtractTransferToMaterialExtractResult(List<MaterialExtract> materialExtracts, Integer userId);

	MaterialExtractPageResult getMaterialExtractPageResult(Integer userId, Integer departmentId, Page<MaterialExtract> page);

	MaterialExtractResult materialExtractTransferToMaterialExtractResult(MaterialExtract materialExtract, Integer userId);
	
}
