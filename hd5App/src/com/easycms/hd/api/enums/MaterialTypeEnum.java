package com.easycms.hd.api.enums;

public enum MaterialTypeEnum { 
	/**
	 * 出库
	 */
	OUT,
	/**
	 * 入库
	 */
	IN,
	/**
	 * 退库
	 */
	CANCEL
}
