package com.easycms.hd.api.enums;

public enum EnpowerDataStatusEnum { 
	/**
	 * 初始保存
	 */
	INIT,
	/**
	 * 成功处理
	 */
	SUCCESS,
	/**
	 * 处理失败
	 */
	FAILED
}
