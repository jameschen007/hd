package com.easycms.hd.api.enums;

public enum SecurityEnum {
	API_ENPOWER_SECURITY_KEY("enpower_147"),
	API_BASE_SECURITY_KEY("base_258");
	
	private String key;

	SecurityEnum(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}
