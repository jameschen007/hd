package com.easycms.hd.api.enums;

public enum RollingPlanTypeEnum {
	/**
	 * 已全部完成
	 */
	COMPLETED, 
	/**
	 * 正在施工中
	 */
	PROGRESSING, 
	/**
	 * 停滞中
	 */
	PAUSE, 
	/**
	 * 已分派
	 */
	ASSIGNED, 
	/**
	 * 未分派
	 */
	UNASSIGNED,
	/**
	 * 未施工
	 */
	UNPROGRESS,
	/**
	 * 未完成
	 */
	UNCOMPLETE
}
