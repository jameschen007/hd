package com.easycms.hd.api.enums;

public enum QuestionTypeEnum {

	/**
	 * 技术问题
	 */
	technicalMatters,
	/**
	 * 协调问题 
	 */
	coordinationProblem,
	/**
	 * 物项问题
	 */
	resourceMatters
}
