package com.easycms.hd.api.enums.hseproblem.underlying;

import com.easycms.hd.api.enums.hseproblem.DynamicFilterItemList;

import java.util.List;
import java.util.Map;

/**
 * @author wangzhi
 * @Description: test
 * @date 2019/7/3 10:52
 */
public class FilterItemTimeFrameTest {
	/**
	 * key=createdAll value=提出时间-全部 order=1 display=显示 
key=monthEarly value=这个月的早些时候 order=2 display=显示2019-07-01 00:00:00 2019-07-15 00:00:00
key=lastWeek value=上周 order=3 display=显示2019-07-15 00:00:00 2019-07-22 00:00:00
key=weekEarly value=本周早些时候 order=4 display=显示2019-07-22 00:00:00 2019-07-24 00:00:00
key=yesterday value=昨天 order=5 display=显示2019-07-24 00:00:00 2019-07-25 00:00:00
key=today value=今天 order=6 display=显示2019-07-25 00:00:00 2019-07-25 23:59:59




key=createdAll value=提出时间-全部 order=1 display=显示 
key=lastWeek value=上周 order=2 display=显示2019-07-01 00:00:00 2019-07-08 00:00:00
key=weekEarly value=本周早些时候 order=3 display=显示2019-07-08 00:00:00 2019-07-10 00:00:00
key=yesterday value=昨天 order=4 display=显示2019-07-10 00:00:00 2019-07-11 00:00:00
key=today value=今天 order=5 display=显示2019-07-11 00:00:00 2019-07-11 23:59:59


key=createdAll value=提出时间-全部 order=1 display=显示 
key=lastWeek value=上周 order=2 display=显示2019-06-24 00:00:00 2019-07-01 00:00:00
key=weekEarly value=本周早些时候 order=3 display=显示2019-07-01 00:00:00 2019-07-02 00:00:00
key=yesterday value=昨天 order=4 display=显示2019-07-02 00:00:00 2019-07-03 00:00:00
key=today value=今天 order=5 display=显示2019-07-03 00:00:00 2019-07-03 23:59:59


key=createdAll value=提出时间-全部 order=1 display=显示 
key=lastWeek value=上周 order=2 display=显示2019-06-24 00:00:00 2019-07-01 00:00:00
key=yesterday value=昨天 order=3 display=显示2019-07-01 00:00:00 2019-07-02 00:00:00
key=today value=今天 order=4 display=显示2019-07-02 00:00:00 2019-07-02 23:59:59

key=createdAll value=提出时间-全部 order=1 display=显示 
key=lastWeek value=上周 order=2 display=显示2019-06-24 00:00:00 2019-07-01 00:00:00
key=yesterday value=昨天 order=3 display=显示2019-06-30 00:00:00 2019-07-01 00:00:00
key=today value=今天 order=4 display=显示2019-07-01 00:00:00 2019-07-01 23:59:59

key=createdAll value=提出时间-全部 order=1 display=显示 
key=monthEarly value=这个月的早些时候 order=2 display=显示2019-06-01 00:00:00 2019-06-17 00:00:00
key=lastWeek value=上周 order=3 display=显示2019-06-17 00:00:00 2019-06-24 00:00:00
key=weekEarly value=本周早些时候 order=4 display=显示2019-06-24 00:00:00 2019-06-29 00:00:00
key=yesterday value=昨天 order=5 display=显示2019-06-29 00:00:00 2019-06-30 00:00:00
key=today value=今天 order=6 display=显示2019-06-30 00:00:00 2019-06-30 23:59:59
	 * @param args
	 */
    public static void main(String[] args) {

        DynamicFilterItemList t = new DynamicFilterItemList();
        Map<String, List<TheFilterItem>> dynamic = t.getDynamic();
    }
}
