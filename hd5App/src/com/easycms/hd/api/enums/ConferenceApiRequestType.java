package com.easycms.hd.api.enums;

public enum ConferenceApiRequestType {
	/**
	 * 已发送的会议
	 */
	SEND,
	/**
	 * 接收的会议
	 */
	RECEIVE,
	/**
	 * 保存为草稿的会议
	 */
	DRAFT
}
