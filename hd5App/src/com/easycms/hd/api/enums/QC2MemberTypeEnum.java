package com.easycms.hd.api.enums;

public enum QC2MemberTypeEnum { 
//统计信息中的额外类型枚举	
	/**
	 * CZEC QC
	 */
	CZEC_QC,
	/**
	 * CZEC QA
	 */
	CZEC_QA,
	/**
	 * PAEC
	 */
	PAEC
}
