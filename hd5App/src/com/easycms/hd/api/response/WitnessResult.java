package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class WitnessResult implements Serializable {
	private static final long serialVersionUID = -1509038306026349133L;
	//列表中显示的内容
	/**
	 * 唯一ID号
	 */
	private Integer id;
	/**
	 * 申请时间
	 */
	private Date createDate;
	/**
	 * 工序号
	 */
	private String workStepNo;
	/**
	 * 工序号
	 */
	private String stepIdentifier;
	/**
	 * 工序名称
	 */
	private String workStepName;
	/**
	 * 见证类型
	 */
	private String noticeType;
	/**
	 * 见证点
	 */
	private String noticePoint;
	/**
	 * 发起人名称
	 */
	private String launcherName;
	/**
	 * 发起人ID
	 */
	private Integer launcherId;
	
	/**
	 * 见证地点
	 */
	private String witnessAddress ;
	
	/**
	 * 见证时间
	 */
	private Date witnessDate;
	
	/**
	 * 真实见证地点
	 */
	private String realWitnessAddress ;
	
	/**
	 * 真实见证时间
	 */
	private Date realWitnessDate;
	
	/**
	 * 见证状态
	 */
	private String status ;
	/**
	 * QC2的见证状态
	 */
	private String qc2Status ;
	/**
	 * 见证结果
	 */
	private String result ;
	
//滚动计划和工序步骤详细信息
	/**
	 * 唯一ID号
	 */
	private Integer rollingPlanId;
	/**
	 * 唯一ID号
	 */
	private Integer workStepId;
	/**
	 * 作业条目编号
	 */
	private String workListNo;
	/**
	 * Itp编号
	 */
	private String itpNo;
	/**
	 * 工程量编号
	 */
	private String projectNo;
	/**
	 * 焊口支架号
	 */
	private String weldno;
	/**
	 * 规格型号
	 */
	private String speification;
	/**
	 * 工程量名称
	 */
	/**
	 * 物项名称
	 */
	/**
	 * 物项编号
	 */
	/**
	 * 计划用量
	 */
	/**
	 * 单位
	 */
	/**
	 * 图纸号
	 */
	private String drawingNo;
	/**
	 * 文件列表
	 */
	List<FileResult> witnessFiles;
	
	/**
	 * 见证地点列表
	 */
	private List<String> witnessAddresses;
	
	/**
	 * 见证人信息
	 */
	private UserResult witnesser;
	/**
	 * 见证组长信息
	 */
	private UserResult witnessTeam;
	/**
	 * 见证人信息
	 */
//	private UserResult qc2Witnesser;
	
	/**
	 * 不合格原因
	 */
	private String failType;
	/**
	 * 描述
	 */
	private String remark;
	/**
	 * 见证标记
	 */
	private String witnessFlag;
	
	/**
	 * 见证子项
	 */
	List<WitnessResult> subWitness;
	
	/**
	 * 被代替的见证者
	 */
	private String substitute;
	/**
	 * 是否是最后一个工序的最后一条见证信息
	 */
	private boolean lastOne;
	/**
	 * 是否是最后一个工序的最后一条见证信息
	 */
	private Integer order;
}
