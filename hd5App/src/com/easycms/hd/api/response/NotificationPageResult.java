package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotificationPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = 5356961789224770499L;
	private List<NotificationResult> data;
}
