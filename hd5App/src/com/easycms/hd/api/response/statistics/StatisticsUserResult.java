package com.easycms.hd.api.response.statistics;

import java.util.Set;

import com.easycms.hd.api.domain.AuthDept;
import com.easycms.hd.api.domain.AuthRole;

import lombok.Data;

@Data
public class StatisticsUserResult {
	private Integer id;
	private String name;
	private String realname;
	private Set<AuthRole> roles;
	private AuthDept dept;
}
