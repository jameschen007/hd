package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 每种不同角色下的统计信息
 * 班长和队长有这一项
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class WitnessStatisticsResult implements Serializable{
	private static final long serialVersionUID = -5313072274633196255L;
	private Long total = 0L;
	private Long launched = 0L;
	private Long completed = 0L;
	private Long uncomplete = 0L;
	private String mark;
}
