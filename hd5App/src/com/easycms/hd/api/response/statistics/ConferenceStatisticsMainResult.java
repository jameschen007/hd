package com.easycms.hd.api.response.statistics;

import java.io.Serializable;

import lombok.Data;

/**
 * 会议和通知返回的主类
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class ConferenceStatisticsMainResult<T> implements Serializable{
	private static final long serialVersionUID = -1160992367443317603L;
	private ConferenceStatisticsResult conference;
	private ConferenceStatisticsResult notification;
}
