package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 每种不同角色下的统计信息
 * QC2
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class WitnessQC2StatisticsResult implements Serializable{
	private static final long serialVersionUID = -5631071024130104085L;
	private Long completed = 0L;
	private Long uncomplete = 0L;
	private Long total = 0L;
	private String mark;
}
