package com.easycms.hd.api.response.statistics;

import com.easycms.hd.api.enums.ExtraTypeEnum;

import lombok.Data;

@Data
public class ExtraResult {
	private ExtraTypeEnum type;
	private Long result = 0L;
}
