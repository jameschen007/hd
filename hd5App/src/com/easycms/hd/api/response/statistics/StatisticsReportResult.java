package com.easycms.hd.api.response.statistics;

import java.io.Serializable;

import lombok.Data;

/**
 * 分类信息
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class StatisticsReportResult<T> implements Serializable{
	private static final long serialVersionUID = -4731407674708453037L;
	private StatisticsUserResult user;
	private T statistics;
}
