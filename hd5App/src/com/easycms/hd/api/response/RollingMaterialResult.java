package com.easycms.hd.api.response;

import java.io.Serializable;

import javax.persistence.Column;

import lombok.Data;

@Data
public class RollingMaterialResult implements Serializable {
	private static final long serialVersionUID = 7991371355432184827L;
	private Integer id;
	/**
	 * 物项名称
	 */
	private String materialName;
	/**
	 * 物项编号
	 */
	private String materialIdentifier;
	/**
	 * 规格型号
	 */
	private String specificationModel;
	/**
	 * 计划用量
	 */
	private String planDosage;
	/**
	 * 实际用量
	 */
	private String actualDosage;
	/**单位 预留*/
	private String unit ;
	/**
	 * 材质
	 */
	private String materialQuality;
}
