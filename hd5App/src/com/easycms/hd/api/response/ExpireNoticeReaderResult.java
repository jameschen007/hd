package com.easycms.hd.api.response;


import java.io.Serializable;

import lombok.Data;
/**
 * 借阅人
 */
@Data
public class ExpireNoticeReaderResult implements Serializable {
	private static final long serialVersionUID = -4647617585942876366L;
	private Integer id;
	private String cancId;// 文件id
	private String borNum;// 份数
	private String borrower;// 文件使用人
	private String loginname;// 文件借阅人
	private String loginId;// 文件借阅人id
	private String status;// 状态　N未还 Y已还
}