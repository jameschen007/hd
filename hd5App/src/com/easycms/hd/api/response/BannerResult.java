package com.easycms.hd.api.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class BannerResult implements Serializable{
	private static final long serialVersionUID = 7205330008466139066L;
	private Integer id;
	private String image;
	private String selfImage;
	private String alt;
	private String description;
	private Integer orderNumber;
}
