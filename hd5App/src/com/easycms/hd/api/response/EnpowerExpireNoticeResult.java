package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class EnpowerExpireNoticeResult implements Serializable {
	private static final long serialVersionUID = 7136267007264102616L;

	private Integer id;//

	private String cancCode;// 文件失效通知单编号

	private Date askReturnDate;// 要求归还时间

	private Date releaseDate;// 发布时间

	private String drafter;// 编制人id
	
	private String projName;// 项目名称

	private List<ExpireNoticeFileResult> cancFileList;
	
	
}
