package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class DepartmentResult {
	private Integer id;
	private String name;
	private DepartmentResult departmentResult;
}
