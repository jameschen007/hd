package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class ExpireNoticeExtraResult {
	private Long self = 0L;
	private Long downline = 0L;
}
