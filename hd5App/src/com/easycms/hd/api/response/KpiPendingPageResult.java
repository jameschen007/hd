package com.easycms.hd.api.response;

import com.easycms.kpi.underlying.domain.KpiPendingSheet;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
public class KpiPendingPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -1496091688855554319L;
	private List<KpiPendingSheet> data;
}
