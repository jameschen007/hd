package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class ProblemResult implements Serializable {
  private static final long serialVersionUID = -379714055691200798L;
  private String id;
  private String worstepid;
  private String weldno;
  private String drawno;
  private String describe;
  private String questionname;
  private String isOk;
  private String level;
  private String solvemethod;
  private String confirm;
  private String methodmanid;
  private String solvedate;
  private String concerman;
  private String currentsolver;
  private String createdBy;
  private String rollingPlanId;
  private List<ProblemFileResult> file;
  private String creator;
  private List<ProblemSolverResult> solvers;
}
