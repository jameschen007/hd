package com.easycms.hd.api.response.rollingplan;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Rollingplan的分页信息及期数据列表
 * @author ChenYuMei-Refiny
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class RollingPlanPageDataResult extends PagenationResult implements Serializable{
	private static final long serialVersionUID = -1035090443513483719L;
	private List<RollingPlanDataResult> data;
}
