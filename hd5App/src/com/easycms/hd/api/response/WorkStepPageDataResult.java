package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * WorkStep的分页信息及期数据列表
 * @author ChenYuMei-Refiny
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepPageDataResult extends PagenationResult implements Serializable{
	private static final long serialVersionUID = -1035090443513483719L;
	private List<WorkStepResult> data;
}
