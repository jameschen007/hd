package com.easycms.hd.api.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class ExpireNoticeFilesResult implements Serializable{
	private static final long serialVersionUID = 4761636470694942582L;
	/**
	 * 文件唯一标识ID
	 */
	private Integer id;
	/**
	 * 文件编号
	 */
	private String no;
	/**
	 * 数量
	 */
	private Integer number;
	/**
	 * 版本号
	 */
	private String version;
	/**
	 * 领用人
	 */
	private String receiver;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 文件名
	 */
	private String filename;
	/**
	 * 文件路径
	 */
	private String filepath;
}
