package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class ConferenceFeedbackResult implements Serializable{
	private static final long serialVersionUID = 765785624176552375L;
	/**
	 * 反馈唯一标识ID
	 */
	private Integer id;
	/**
	 * 反馈内容
	 */
	private String message;

	/**
	 * 反馈时间
	 */
	private Date feedbackTime;
	
	/**
	 * 反馈人信息
	 */
	private UserResult user;
}
