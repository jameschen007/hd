package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class MaterialExtractPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -7303962927655597272L;
	private List<MaterialExtractResult> data;
}
