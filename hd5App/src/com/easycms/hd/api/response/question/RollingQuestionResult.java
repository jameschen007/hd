package com.easycms.hd.api.response.question;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.ProblemFileResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanDataResult;

public class RollingQuestionResult implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;

	private int id;
//	/**
//	 * 作业条目号
//	 */ 
//	private String drawno;

//	/**
//	 * 焊口/支架 保留以前的，如果没有用，就删除
//	 */
//	private String weldno;
	private Integer rollingPlanId;
	/**
	 * 问题类型：tecknicalMatters技术问题,coordinationProblem协调问题
	 */
	private String questionType;
	/**
	 * '状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决'
	 */
	private String status;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 创建人id
	 */
//	private User owner;

	/** 创建时间 */
	private String questionTime;
	/*
	 * 不通过理由
	 */
	private String reason ;
	
	/**
	 * 解决人无法解决的原因
	 */
	private String unableReason;

	/** 被指派协调 */
	private UserResult coordinate;
	/** 被指派者，指派给 */
	private UserResult designee;
	/** 创建人 */
	private UserResult createUser;
	
	/**附件*/
	private List<ProblemFileResult> files;
	private Integer fileSize;
	/**
	 * 是否超时未解决
	 */
	private Boolean timeout;
	
	private RollingPlanDataResult rollingPlan;
	private List<UserResult> userList;

	private List<RollingQuestionResult> feedback;
	private List<RollingQuestionResult> teamAnswer;
	/**
	 * 处理人小状态返回
	 */
	private List<RollingQuestionSolverResult> solverList;

	/**
	 * 操作列表 
	 */
	private List<RollingQuestionSolverResult> opList;
	
	/**
	 * 当前登录人，能否直接处理，　handle=true时，可以直接处理。
	 */
	private Boolean handle ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getRollingPlanId() {
		return rollingPlanId;
	}

	public void setRollingPlanId(Integer rollingPlanId) {
		this.rollingPlanId = rollingPlanId;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getQuestionTime() {
		return questionTime;
	}

	public void setQuestionTime(String questionTime) {
		this.questionTime = questionTime;
	}

	public UserResult getDesignee() {
		return designee;
	}

	public void setDesignee(UserResult designee) {
		this.designee = designee;
	}

	public List<ProblemFileResult> getFiles() {
		return files;
	}

	public void setFiles(List<ProblemFileResult> files) {
		this.files = files;
	}

	public RollingPlanDataResult getRollingPlan() {
		return rollingPlan;
	}

	public void setRollingPlan(RollingPlanDataResult rollingPlan) {
		this.rollingPlan = rollingPlan;
	}

	public List<RollingQuestionResult> getFeedback() {
		return feedback;
	}

	public void setFeedback(List<RollingQuestionResult> feedback) {
		this.feedback = feedback;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<RollingQuestionSolverResult> getSolverList() {
		return solverList;
	}

	public void setSolverList(List<RollingQuestionSolverResult> solverList) {
		this.solverList = solverList;
	}

	public List<UserResult> getUserList() {
		return userList;
	}

	public void setUserList(List<UserResult> userList) {
		this.userList = userList;
	}

	public Boolean getTimeout() {
		return timeout;
	}

	public void setTimeout(Boolean timeout) {
		this.timeout = timeout;
	}

	public UserResult getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(UserResult coordinate) {
		this.coordinate = coordinate;
	}

	public String getUnableReason() {
		return unableReason;
	}

	public void setUnableReason(String unableReason) {
		this.unableReason = unableReason;
	}

	public List<RollingQuestionSolverResult> getOpList() {
		return opList;
	}

	public void setOpList(List<RollingQuestionSolverResult> opList) {
		this.opList = opList;
	}

	public UserResult getCreateUser() {
		return createUser;
	}

	public void setCreateUser(UserResult createUser) {
		this.createUser = createUser;
	}

	public Boolean getHandle() {
		return handle;
	}

	public void setHandle(Boolean handle) {
		this.handle = handle;
	}

	public List<RollingQuestionResult> getTeamAnswer() {
		return teamAnswer;
	}

	public void setTeamAnswer(List<RollingQuestionResult> teamAnswer) {
		this.teamAnswer = teamAnswer;
	}
	
	
}
