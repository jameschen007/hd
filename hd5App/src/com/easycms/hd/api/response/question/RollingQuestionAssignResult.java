//package com.easycms.hd.api.response.question;
//
//import java.util.List;
//
//import com.easycms.hd.api.response.UserResult;
//
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//
///**
// * 用于问题指派时使用，另一个多技术部人员列表
// * @author ul-webdev
// *
// */
//
//@Data
//@EqualsAndHashCode(callSuper=false)
//public class RollingQuestionAssignResult extends RollingQuestionResult{
//
//	private static final long serialVersionUID = -2221658589708419399L;
////
////	List<UserResult> userList;
//}
