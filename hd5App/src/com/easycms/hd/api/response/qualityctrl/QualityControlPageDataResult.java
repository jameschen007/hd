package com.easycms.hd.api.response.qualityctrl;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QualityControlPageDataResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -5886655252648133160L;
	
	private List<QualityControlResult> data;

}
