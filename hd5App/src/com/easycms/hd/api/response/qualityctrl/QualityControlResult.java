package com.easycms.hd.api.response.qualityctrl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.hseproblem.HseProblemFileResult;
import com.easycms.hd.api.response.hseproblem.HseResponsibleDeptResult;
import com.easycms.hd.qualityctrl.domain.QualityControlStep;

import lombok.Data;

@Data
public class QualityControlResult implements Serializable {
	private static final long serialVersionUID = 5363912691138076500L;

	/**
	 * 质量问题ID
	 */
	private Integer id;
	
	/**
	 * 机组
	 */
	private String unit;

	/**
	 * 子项
	 */
	private String subitem;

	/**
	 * 楼层
	 */
	private String floor;

	/**
	 * 房间号
	 */
	private String roomnum;

	/**
	 * 系统
	 */
	private String system;
	
	/**
	 * 区域
	 */
	private String area;
	
	/**
	 * 类别
	 */
	private String type;

	/**
	 * 责任部门
	 */
	private HseResponsibleDeptResult responsibleDept;

	/**
	 * 责任班组
	 */
	private HseResponsibleDeptResult responsibleTeam;

	/**
	 * 问题描述
	 */
	private String problemDescription;
	
	/**
	 * 相关图片附件
	 */
	private List<HseProblemFileResult> files;

	/**
	 * 问题创建人
	 */
	private String createUser;
	private String createUserName;

	/**
	 * 创建时间
	 */
	private Date createDate;

	/**
	 * 状态
	 */
	private String status;

	/**
	 * QC人员
	 */
	private Integer qcUserId;
	private String qcUser;

	/**
	 * 整改队伍
	 */
	private String renovateTeam;

	/**
	 * 整改描述
	 */
	private String renovateDescription;

	/**
	 * 备注
	 */
	private String notes;
	
	/**
	 * 处理步骤
	 */
	private List<QualityControlStep> qcStep;
	
	private List<UserResult> userList;


}
