package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class MaterialPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = 3240078483758008851L;
	private List<MaterialResult> data;
}
