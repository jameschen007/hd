package com.easycms.hd.api.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class ModulesResult implements Serializable{
	private static final long serialVersionUID = 7205330008466139066L;
	private Integer id;
	private String type;
	private String name;
	private boolean alert;
	private Integer orderNumber;
}
