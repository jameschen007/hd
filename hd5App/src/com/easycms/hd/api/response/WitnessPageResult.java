package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import com.easycms.hd.api.response.statistics.NoticePointStatisticsResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WitnessPageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -1496091688855554319L;
	private List<WitnessResult> data;
	private NoticePointStatisticsResult statistics;
}
