package com.easycms.hd.api.exam;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.util.WebUtility;
import com.easycms.hd.api.exam.request.ExamHandleRequestForm;
import com.easycms.hd.api.exam.request.ExamPaperSolution;
import com.easycms.hd.api.exam.response.ExamPaperHandInResult;
import com.easycms.hd.api.service.ExamApiService;
import com.easycms.hd.exam.domain.ExamUserScore;
import com.easycms.hd.exam.enums.ExamStatus;
import com.easycms.hd.exam.service.ExamUserScoreService;
import com.easycms.hd.response.JsonResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/hdxt/api/exam_op")
@Api(value = "ExamOperationAPIController", description = "考试操作相关的api")
public class ExamOperationAPIController {
	
	@Autowired
	private ExamApiService examApiService;
	@Autowired
	private ExamUserScoreService examUserScoreService;
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/handIn", method = RequestMethod.POST)
	@ApiOperation(value = "提交试卷", notes = "考试完成提交考试答案,返回考试结果")
	public JsonResult<ExamPaperHandInResult> handleExams(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ExamHandleRequestForm form,
			@ApiParam(required = true, name = "answers", value = "答案") @RequestParam(value="answers",required=true) String answers) throws Exception {
		JsonResult<ExamPaperHandInResult> result = new JsonResult<ExamPaperHandInResult>();
		Gson gson = new Gson();
		log.debug("UserAnswer:"+WebUtility.decodeURIComponent(answers));
		String data = WebUtility.decodeURIComponent(answers);
		if("".equals(data) || null == data){
			result.setCode("-1000");
			result.setMessage("数据异常,用户提交答案为空！");
			return result;
		}
		List<ExamPaperSolution> examPaperSolutions = null;
		try{
			examPaperSolutions = gson.fromJson(data, new TypeToken<List<ExamPaperSolution>>() {}.getType());
		}catch(Exception e){
			log.error("答案数据异常：" + data);
			result.setCode("-1000");
			result.setMessage("数据异常！");
			return result;
		}
		
		ExamPaperHandInResult examScoreResult = examApiService.checkExamResult(form, examPaperSolutions);
		List<ExamUserScore> examing = examUserScoreService.findByUserAndExamStatus(form.getLoginId(), ExamStatus.DOING.toString());
		//保存考试结果 
		if(examScoreResult != null){
			if(examing != null && examing.size()==1){
				ExamUserScore examUserScore = examing.get(0);
				examUserScore.setExamEndDate(new Date());
				examUserScore.setExamResult(examScoreResult.getResult());
				examUserScore.setExamScore(examScoreResult.getScore());
				examUserScore.setUserId(form.getLoginId());
				examUserScore.setExamStatus(ExamStatus.COMPLETED.toString());
				examUserScoreService.add(examUserScore);				
			}else{
				result.setCode("-1000");
				result.setMessage("交卷失败,当前用户考试信息异常！");
				return result;
			}
		}else{
			result.setCode("-1000");
			result.setMessage("交卷失败！");
			return result;
		}
	
		result.setResponseResult(examScoreResult);			

		return result;
	}
}
