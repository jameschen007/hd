package com.easycms.hd.api.exam.response;

import lombok.Data;

@Data
public class ExamPaperHandInResult {
	/**
	 * 总分数
	 */
	private Double score;
	/**
	 * 及格情况
	 */
	private String result;
	/**
	 * 考试时长
	 */
	private Long duration;
}
