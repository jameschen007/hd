package com.easycms.hd.api.exam;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.exam.request.ExamPaperRequestForm;
import com.easycms.hd.api.exam.response.ExamPaperResult;
import com.easycms.hd.api.exam.response.ExamScoreResult;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.service.ExamApiService;
import com.easycms.hd.exam.domain.ExamUserScore;
import com.easycms.hd.exam.enums.ExamStatus;
import com.easycms.hd.exam.service.ExamUserScoreService;
import com.easycms.hd.learning.domain.LearningFold;
import com.easycms.hd.learning.service.LearningFoldService;
import com.easycms.hd.response.JsonResult;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/hdxt/api/exam")
@Api(value = "ExamAPIController", description = "考试相关的api")
public class ExamAPIController {
	
	@Autowired
	private ExamApiService examApiService;
	@Autowired
	private ExamUserScoreService examUserScoreService;
	@Autowired
	private LearningFoldService learningFoldService;
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取考试题目", notes = "获取考试题目")
	public JsonResult<ExamPaperResult> getTestPaper(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ExamPaperRequestForm form){
		JsonResult<ExamPaperResult> result = new JsonResult<ExamPaperResult>();
		boolean status = false;
		if(!CommonUtility.isNonEmpty(form.getSection())){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, Section is not available");
			return result;	
		}
		if("JNPX".equals(form.getSection()) && !CommonUtility.isNonEmpty(form.getModule())){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, Module is not available");
			return result;	
		}
		Map<String,Object> res = new HashMap<>();
		ExamPaperResult examPaperResult = null;
		String msg = "";
		try {
			if("ZXPX".equals(form.getSection())){
				if(!CommonUtility.isNonEmpty(form.getModule())){
					result.setCode("-1001");
					result.setMessage("Error/s occurred, Module is not available");
					return result;
				}
				examPaperResult = examApiService.getAllExamPaper(form.getSection(), form.getModule());
			}else{
				res = examApiService.generateTestPaper(form);
				if(res.isEmpty()){
					result.setCode("-1000");
					result.setMessage("服务器繁忙，稍后再试");
					return result;
				}
				examPaperResult = (ExamPaperResult) res.get("data");
				msg = (String) res.get("msg");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			result.setCode("-1004");
			result.setMessage("服务器繁忙，稍后再试");
			return result;	
		}
		
		if(examPaperResult != null){
			status = true;
		}else{
			result.setCode("-1000");
			result.setMessage(msg);
			return result;	
		}
		
		//获取试题，考试开始，并记录考试基础信息，用户交卷时考试结束
		List<ExamUserScore> examing = examUserScoreService.findByUserAndExamStatus(form.getLoginId(), ExamStatus.DOING.toString());
		ExamUserScore examUserScore = null;
		
		if(examing.isEmpty() || examing.size()==0){
			examUserScore = new ExamUserScore();
			examUserScore.setExamStatus(ExamStatus.DOING.toString());
		}else if(examing != null && examing.size() < 2){
			examUserScore = examing.get(0);
		}else{
			status = false;
		}
		
		if(status){
			examUserScore.setExamStartDate(new Date());
			examUserScore.setExamType(form.getSection());
			examUserScore.setUserId(form.getLoginId());
			LearningFold learningFold = null;
			if(form.getFold() != null){
				Integer foldId = Integer.parseInt(form.getFold());
				learningFold = learningFoldService.findById(foldId);
			}
			examUserScore.setLearningFold(learningFold);
			examUserScoreService.add(examUserScore);						
		}else{
			result.setCode("-1000");
			result.setMessage("服务器繁忙，稍后再试");
			return result;			
		}

		result.setResponseResult(examPaperResult);			
		return result;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/examScore", method = RequestMethod.GET)
	@ApiOperation(value = "获取考试成绩", notes = "获取用户所有考试成绩")
	public JsonResult<ExamScoreResult> getTestScore(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BasePagenationRequestForm form) throws Exception {
		JsonResult<ExamScoreResult> result = new JsonResult<ExamScoreResult>();
		if(form.getLoginId() == null){
			result.setCode("-1001");
			result.setMessage("Error/s occurred, LoginId is not available");
			return result;
		}
		ExamScoreResult examScoreResult = examApiService.getExamScore(form);
		if(examScoreResult != null){
			result.setResponseResult(examScoreResult);			
		}else{
			result.setCode("-1000");
			result.setMessage("执行操作失败！");
		}
		return result;
	}
}
