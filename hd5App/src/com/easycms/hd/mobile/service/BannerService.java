package com.easycms.hd.mobile.service;

import java.util.List;

import com.easycms.hd.mobile.domain.Banner;


public interface BannerService {
	List<Banner> findByAll();
	List<Banner> findByStatus(String status);
}
