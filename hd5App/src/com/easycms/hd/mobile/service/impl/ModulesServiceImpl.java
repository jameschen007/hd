package com.easycms.hd.mobile.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.mobile.dao.ModulesDao;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;


@Service("modulesService")
public class ModulesServiceImpl implements ModulesService {

	@Autowired
	private ModulesDao modulesDao;

	@Override
	public List<Modules> findByAll() {
		return modulesDao.findAll();
	}

	@Override
	public List<Modules> findByStatus(String status) {
		return modulesDao.findByStatus("ACTIVE");
	}

	@Override
	public Modules findByType(String type) {
		return modulesDao.findByType(type);
	}

	@Override
	public Modules findById(Integer id) {
		return modulesDao.findById(id);
	}

	@Override
	public Modules findByEnpowerName(String enpowerName) {
		return modulesDao.findByEnpowerName(enpowerName);
	}

	@Override
	public Page<Modules> findByStatus(String status, Page<Modules> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Modules> springPage = modulesDao.findByStatus(status, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<Modules> findByStatusAndSection(String status, String section, Page<Modules> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Modules> springPage = modulesDao.findByStatusAndSection(status, section, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Modules add(Modules modules) {
		return modulesDao.save(modules);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != modulesDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}
}
