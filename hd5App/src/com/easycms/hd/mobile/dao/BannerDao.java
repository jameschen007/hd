package com.easycms.hd.mobile.dao;

import java.util.List;

import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.mobile.domain.Banner;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface BannerDao extends BasicDao<Banner> ,Repository<Banner,Integer>{
	List<Banner> findByStatus(String status);
}
