package com.easycms.hd.price.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.price.domain.Price;
import com.easycms.hd.price.form.PriceForm;
import com.easycms.hd.price.form.validator.PriceFormValidator;
import com.easycms.hd.price.service.PriceService;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.PrivilegeService;

@Controller
@RequestMapping("/baseservice/price")
public class PriceController {
	private static String PRICE = "price";
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PriceController.class);
	
	@Autowired
	private PriceService priceService;
	@Autowired
	@Qualifier("priceFormValidator")
	private org.springframework.validation.Validator validator;
	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private VariableSetService variableSetService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String settings(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("priceForm") @Valid PriceForm form) throws Exception {
		String func = form.getFunc();
		logger.debug("[func] = " + func + " : " + CommonUtility.toJson(form));
		if ("exist".equalsIgnoreCase(func)) {
			logger.debug("==> Valid price is exist or not.");
			boolean avalible = false;
			avalible = !priceService.checkPricetypeExist(form.getSpeciality(), form.getPricetype());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;

		}
		
		if ("edit".equalsIgnoreCase(func)) {
			logger.debug("==> Valid price is edit or not.");
			boolean avalible = false;
			avalible = !priceService.checkPricetypeExistWithId(form.getSpeciality(), form.getPricetype(), form.getId());
			HttpUtility.writeToClient(response, String.valueOf(avalible));
			return null;

		}
		
		return ForwardUtility.forwardAdminView("/hdService/price/list_price");
	}
	
	/**
	 * 数据片段
	 * 
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String dataList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") PriceForm form) {
		logger.debug("==> Show price data list.");
		form.setFilter(CommonUtility.toJson(form));
		logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility
				.forwardAdminView("/hdService/price/data/data_json_price");
		return returnString;
	}

	/**
	 * 新增单价信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response) {
		List<VariableSet> priceTypeList = variableSetService.findByType(PRICE);
		request.setAttribute("priceTypeList", priceTypeList);
		String returnString = ForwardUtility.forwardAdminView("/hdService/price/modal_price_add");
		return returnString;
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("priceForm") @Valid PriceForm priceForm,
			BindingResult errors) {
		logger.debug("==> Start save new price.");

		// 校验新增表单信息
		PriceFormValidator validator = new PriceFormValidator();
		validator.validate(priceForm, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> Save error.");
			logger.debug("==> End save new user.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");
		
		// 校验通过，保存新增
		Price price = new Price();
		price.setSpeciality(priceForm.getSpeciality());
		price.setPricetype(priceForm.getPricetype());
		price.setUnitprice(priceForm.getUnitprice());
		price.setRemark(priceForm.getRemark());
		price.setCreatedOn(new Date());
		price.setCreatedBy(loginUser.getName());
		
		if (!priceService.checkPricetypeExist(price.getSpeciality(), price.getPricetype())){
			price = priceService.add(price);
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			logger.debug("==> End save new price already exist.");
			return;
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(price.getId() == null));
		logger.debug("==> End save new price.");
		return;
	}


	/**
	 * 修改单价
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editUI(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("form") PriceForm form) {
		List<VariableSet> priceTypeList = variableSetService.findByType(PRICE);
		request.setAttribute("priceTypeList", priceTypeList);
		return ForwardUtility.forwardAdminView("/hdService/price/modal_price_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") PriceForm form) {
		if (logger.isDebugEnabled()) {
			logger.debug("==> Start save eidt price.");
		}

		// #########################################################
		// 校验提交信息
		// #########################################################
		if (form.getId() == null) {
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Save eidt price failed.");
			logger.debug("==> End save eidt price.");
			return;
		}
		
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		// #########################################################
		// 校验通过
		// #########################################################
		Integer id = form.getId();
		Price price = priceService.findById(id);
		price.setSpeciality(form.getSpeciality());
		price.setPricetype(form.getPricetype());
		price.setUnitprice(form.getUnitprice());
		price.setUpdatedBy(loginUser.getName());
		price.setRemark(form.getRemark());
		price.setUpdatedOn(new Date());
		priceService.update(price);
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End save eidt price.");
		return;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") PriceForm form) {
		logger.debug("==> Start delete price.");

		Integer[] ids = form.getIds();
		logger.debug("==> To delete price [" + CommonUtility.toJson(ids) + "]");
		if (ids != null && ids.length > 0) {
			priceService.delete(ids);
		}
		HttpUtility.writeToClient(response, "true");
		logger.debug("==> End delete price.");
	}

}
