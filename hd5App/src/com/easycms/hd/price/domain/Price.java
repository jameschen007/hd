package com.easycms.hd.price.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

/** 
 * @author fangwei: 
 * @areate Date:2015-04-08 
 * 
 */
@Entity
@Table(name = "price")
@Cacheable
public class Price extends BasicForm implements java.io.Serializable {
	private static final long serialVersionUID = 4334165446252943396L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "autoid", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "speciality", length = 20)
	private String speciality;
	
	@Column(name = "pricetype", length = 20)
	private String pricetype;
	
	@Column(name = "unitprice", precision = 10)
	private Double unitprice;
	
	@Column(name = "remark", length = 50)
	private String remark;
	
	@Column(name = "created_on", length = 0)
	private Date createdOn;
	
	@Column(name = "created_by", length = 40)
	private String createdBy;
	
	@Column(name = "updated_on", length = 0)
	private Date updatedOn;
	
	@Column(name = "updated_by", length = 40)
	private String updatedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getPricetype() {
		return pricetype;
	}

	public void setPricetype(String pricetype) {
		this.pricetype = pricetype;
	}

	public Double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(Double unitprice) {
		this.unitprice = unitprice;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}