package com.easycms.hd.price.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.price.dao.PriceDao;
import com.easycms.hd.price.domain.Price;
import com.easycms.hd.price.service.PriceService;

@Service("priceService")
public class PriceServiceImpl implements PriceService {

	@Autowired
	private PriceDao priceDao;


	@Override
	public boolean update(Price price) {
		// TODO Auto-generated method stub
		int result = priceDao.update(price.getSpeciality(),price.getPricetype(),price.getUnitprice(),price.getRemark(),price.getId());
		return result > 0;
	}

	@Override
	public Price add(Price price) {
		return priceDao.save(price);
	}

	@Override
	public Price findById(Integer id) {
		return priceDao.findById(id);
	}

	@Override
	public boolean delete(Integer id) {
		if (0 != priceDao.deleteById(id)){
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(Integer[] ids) {
		if (0 != priceDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<Price> findAll() {
		return priceDao.findAll();
	}

	@Override
	public List<Price> findAll(Price condition) {
		return priceDao.findAll(QueryUtil.queryConditions(condition));
	}

	@Override
	public Page<Price> findPage(Page<Price> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		
		org.springframework.data.domain.Page<Price> springPage = priceDao.findAll(null, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	@Override
	public Page<Price> findPage(Price condition,Page<Price> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		
		org.springframework.data.domain.Page<Price> springPage = priceDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public boolean checkPricetypeExist(String speciality, String pricetype) {
		if (null != priceDao.checkPricetypeExist(speciality, pricetype)){
			return true;
		}
		return false;
	}

	@Override
	public Price getPricetypeExist(String speciality, String pricetype) {
		return priceDao.checkPricetypeExist(speciality, pricetype);
	}

	@Override
	public boolean checkPricetypeExistWithId(String speciality, String pricetype, Integer id) {
		if (null != priceDao.checkPricetypeExistWithId(speciality, pricetype, id)){
			return true;
		}
		return false;
	}
}
