package com.easycms.hd.problem.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="problem_concernman")
public class ProblemConcernman {
	@Id
	@Column(name="id")
	@GeneratedValue
	private int id;
	@Column(name = "concernmanid")
	private int concermanid;
	@Column(name = "problemid")
	private int problemid;
	@Column(name = "concermanname")
	private String concernmanname;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getConcermanid() {
		return concermanid;
	}
	public void setConcermanid(int concermanid) {
		this.concermanid = concermanid;
	}
	public int getProblemid() {
		return problemid;
	}
	public void setProblemid(int problemid) {
		this.problemid = problemid;
	}
	public String getConcernmanname() {
		return concernmanname;
	}
	public void setConcernmanname(String concernmanname) {
		this.concernmanname = concernmanname;
	}
	
	
}
