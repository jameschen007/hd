package com.easycms.hd.problem.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workstep_problem_file")
public class ProblemFile {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "filepath")
	private String path;
	@Column(name = "uploadtime")
	private Date time;
	
	@Column(name = "problemid")
	private Integer problemid;
	
	@Column(name = "filename")
	private String fileName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}


	public Integer getProblemid() {
		return problemid;
	}

	public void setProblemid(Integer problemid) {
		this.problemid = problemid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "ProblemFile [id=" + id + ", path=" + path + ", time=" + time
				+ ", problemid=" + problemid + ", fileName=" + fileName + "]";
	}



	
}
