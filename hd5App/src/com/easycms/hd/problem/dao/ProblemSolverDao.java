package com.easycms.hd.problem.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.problem.domain.ProblemSolver;

@Transactional
public interface ProblemSolverDao  extends Repository<ProblemSolver,Integer>,BasicDao<ProblemSolver>{
	
	@Query("From ProblemSolver ps where ps.problemid = ?1")
	List<ProblemSolver> findByProblemId(Integer id);

	@Query("From ProblemSolver ps where ps.problemid = ?1 and ps.solver = ?2")
	List<ProblemSolver> findByProblemIdAndUserId(int problemId, String userid);

	@Query("From ProblemSolver ps where ps.solver = ?1")
	List<ProblemSolver> findByUserId(String id);

	@Query("From ProblemSolver ps where ps.solver = ?1 and ps.isSolve = ?2")
	List<ProblemSolver> findByUserIdAndStatus(String userid, int iS_OK);
	
}
