package com.easycms.hd.problem.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.domain.ProblemData;
import com.easycms.hd.problem.domain.ProblemSolver;
import com.easycms.hd.problem.service.ProblemService;
import com.easycms.hd.problem.service.ProblemSolverService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.JobService;
import com.easycms.quartz.service.SchedulerService;

@Service("problemAutoChangeSolverService")
public class ProblemAutoChangeSolverServiceImp implements JobService {

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private ProblemService problemService;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ProblemSolverService problemSolverService;

	@Autowired
	private UserService userService;

	private Logger logger = Logger
			.getLogger(ProblemAutoChangeSolverServiceImp.class);

	@Override
	public void doJob(Object jobDataMap) {

		logger.debug("=====问题未解决期限已到====");
		Map<String, Object> datas = (Map<String, Object>) jobDataMap;
		Integer problemid = (Integer) datas.get("problemid");
		User leader = (User) datas.get("leader");
		if (leader == null ) {
			logger.debug("=====没有领导解决问题了====");
			return;
		}
		logger.debug("problemid[" + problemid + "]   leaders ["
		    + leader.toString() + "]");
		Problem problem = problemService.findById(problemid);

		if (problem.getIsOk() == ProblemData.IS_OK) {
			logger.debug("=====问题是否已被解决:" + problem.getIsOk());
			logger.debug("=====问题是否已被解决:" + ProblemData.IS_OK);
			logger.debug("=====问题是否已被解决:"
					+ (problem.getIsOk() == ProblemData.IS_OK));
			return;
		}

		List<Task> tasks = taskService.createTaskQuery()
				.taskCandidateUser(problem.getCurrentsolverid() + "").list();
		logger.debug("当前问题解决人为:" + problem.getCurrentsolver());
		logger.debug("当前问题解决人所需解决问题数量为:" + tasks.size());
		Map<String, Object> variables = new HashMap<String, Object>();
		Task task = findTaskByKey(problem, tasks);

		if (task == null) {
			return;
		}

		String solvers = "";
		String solversIds = "";
		List<String> ids = new ArrayList<String>();
		ids.add(leader.getId() + "");
		
		logger.debug("自动升级给顶级领导解决:" + leader.getRealname());
		variables.put("works", false);
		taskService.complete(task.getId(), variables);
		variables.put("solved", false);
		variables.put("solver", "" + ids);

		Problem p = problemService.findById(problem.getId());
		p.setLevel(p.getLevel() + 1);
		p.setUpdateOn(new Date());
		problem.setCurrentsolver(solvers);
		problem.setCurrentsolverid(solversIds);
		problemService.update(p);
		taskService.complete(task.getId(), variables);
		ProblemSolver problemSolver = new ProblemSolver();
		problemSolver.setIsSolve(ProblemData.IS_NOT_OK);
		problemSolver.setCreateOn(new Date());
		problemSolver.setCreateBy("admin");
		problemSolver.setProblemid(problem.getId());
		problemSolver.setSolver(leader.getId() + "");

		
		ProblemSolver olPS = problemSolverService
				.findById(problem.getSolverId());
		ProblemSolver ps = problemSolverService
				.add(problemSolver);
		problem.setSolverId(ps.getId());

		olPS.setDescribe("规定时间未处理");
		problemSolverService.add(olPS);
	    problemService.update(problem);

	}

	private Task findTaskByKey(Problem problem, List<Task> tasks) {
		for (Task task : tasks) {
			ProcessInstance pi = runtimeService.createProcessInstanceQuery()
					.processInstanceId(task.getProcessInstanceId())
					.singleResult();
			if (pi.getBusinessKey().equals(problem.getId() + "")) {
				return task;
			}

		}
		return null;
	}

}
