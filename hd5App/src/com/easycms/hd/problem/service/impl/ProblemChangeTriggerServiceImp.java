package com.easycms.hd.problem.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.problem.dao.ProblemChangeTriggerDao;
import com.easycms.hd.problem.domain.ProblemChangeTrigger;
import com.easycms.hd.problem.service.ProblemChangeTriggerService;

@Service
public class ProblemChangeTriggerServiceImp implements ProblemChangeTriggerService{
	
	@Autowired
	private ProblemChangeTriggerDao problemChangeTriggerDao;
	
	public ProblemChangeTrigger findByName(String name){
		return problemChangeTriggerDao.findByName(name);
	}
	public ProblemChangeTrigger update(ProblemChangeTrigger problemChangeTrigger){
		return problemChangeTriggerDao.save(problemChangeTrigger);
	}
	@Override
	public ProblemChangeTrigger findById(int id) {
		return problemChangeTriggerDao.findById(id);
	}
}
