package com.easycms.hd.problem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.problem.dao.ProblemDao;
import com.easycms.hd.problem.domain.Problem;
import com.easycms.hd.problem.service.ProblemService;

@Service
public class ProblemServiceImpl implements ProblemService {

	@Autowired
	private ProblemDao problemDao;

	@Override
	public Problem add(Problem p) {
		return problemDao.save(p);
	}

	@Override
	public Problem update(Problem p) {
		return problemDao.save(p);
	}

	@Override
	public List<Problem> findByIds(Integer[] aids) {
		return problemDao.findByIdS(aids);
	}

	@Override
	public Problem findById(Integer id) {
		return problemDao.findById(id);
	}

	@Override
	public List<Problem> findByWorkstepId(int worstepid) {
		return problemDao.findByWorkstepId(worstepid);
	}

	@Override
	public List<Problem> findByUserId(String userid) {
		return problemDao.findByUserId(userid);
	}

	@Override
	public List<Problem> findByIdsAndStatus(Integer[] ids, int status) {
		return problemDao.findByIdsAndStatus(ids, status);
	}

	@Override
	public List<Problem> findByUserIdAndConfirm(String userid, int con) {
		return problemDao.findByIdAndStatus(userid, con);
	}

	@Override
	public Page<Problem> findByIds(Integer[] ids, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByIdInOrderByCreateOnDesc(ids,
				pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByIds(Integer[] ids, String keyword, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByIdInOrderByCreateOnDesc(ids,
				keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByWorkstepId(int worstepid, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao
				.findByWorstepidOrderByCreateOnDesc(worstepid, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByUserId(String userid, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByCreatedByOrderByCreateOnDesc(userid,
				pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByUserId(String userid, String keyword, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByCreatedByOrderByCreateOnDesc(userid,
				keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByIdsAndStatus(Integer[] ids, int status, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByIdInAndIsOkOrderByCreateOnDesc(ids,
				status, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByIdsAndStatus(Integer[] ids, int status, String keyword, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByIdInAndIsOkOrderByCreateOnDesc(ids,
				status, keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findByUserIdAndConfirm(String userid, int confirm, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao
				.findByCreatedByAndConfirmOrderByCreateOnDesc(userid, confirm, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public boolean delete(Problem p) {
		return problemDao.deleteById(p.getId()) > 0;
	}

	@Override
	public List<Problem> findByRollingPlanIdAndIsNotOk(int rolingPlanId) {
		return problemDao.findByRollingPlanIdAndIsOk(rolingPlanId);
	}

	@Override
	public int countByUserId(String userid) {
		return problemDao.countByUserId(userid);
	}

	@Override
	public int findCountByUserIdAndConfirm(String userid, int i) {
		return problemDao.findCountByUserIdAndConfirm(userid, i);
	}

	@Override
	public int findCountConcernedNotSolved(Integer[] ids) {
		return problemDao.findCountConcernedNotSolved(ids);
	}

	@Override
	public int findByNeedToSolveCount(Integer[] ids) {
		return problemDao.findByNeedToSolveCount(ids);
	}

	@Override
	public List<Problem> findByRollingPlanId(int rid) {
		return problemDao.findByRollingPlanId(rid);
	}

	@Override
	public Page<Problem> findByRollingPlanId(int id, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findByRollingPlanIdOrderByCreateOnDesc(id,
				pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public int findCountConcerned(Integer[] ids) {
		return problemDao.findCountConcerned(ids);
	}

	@Override
	public List<Problem> findConfirmByIdsAndStatus(Integer[] ids, int i) {
		return problemDao.findConfirmByIdsAndStatus(ids, i);
	}

	@Override
	public Page<Problem> findConfirmProblemsByIds(Integer[] ids, int i, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao
				.findByIdInAndConfirmOrderByCreateOnDesc(ids, i, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findConfirmProblemsByIds(Integer[] ids, int i, String keyword, Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao
				.findByIdInAndConfirmOrderByCreateOnDesc(ids, i, keyword, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public Page<Problem> findAll(Page<Problem> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Problem> springPage = problemDao.findAll(null, pageable);

		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}
}
