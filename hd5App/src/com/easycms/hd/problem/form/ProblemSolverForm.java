package com.easycms.hd.problem.form;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

public class ProblemSolverForm extends BasicForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int problemid;
	private String solver;
	private String describe;
	private Date solveDate;
	private int isSolve;
	private int currentSolver;
	private int stepno;
	private String solveLevel;
	private Date createOn;
	private String createBy;
	private Date updateOn;
	private String updateBy;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getSolver() {
		return solver;
	}
	public void setSolver(String solver) {
		this.solver = solver;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public Date getSolveDate() {
		return solveDate;
	}
	public void setSolveDate(Date solveDate) {
		this.solveDate = solveDate;
	}
	public int getIsSolve() {
		return isSolve;
	}
	public void setIsSolve(int isSolve) {
		this.isSolve = isSolve;
	}
	public int getCurrentSolver() {
		return currentSolver;
	}
	public void setCurrentSolver(int currentSolver) {
		this.currentSolver = currentSolver;
	}
	public String getSolveLevel() {
		return solveLevel;
	}
	public void setSolveLevel(String solveLevel) {
		this.solveLevel = solveLevel;
	}
	public Date getCreateOn() {
		return createOn;
	}
	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateOn() {
		return updateOn;
	}
	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public int getProblemid() {
		return problemid;
	}
	public void setProblemid(int problemid) {
		this.problemid = problemid;
	}
	
	public int getStepno() {
		return stepno;
	}
	public void setStepno(int stepno) {
		this.stepno = stepno;
	}
	@Override
	public String toString() {
		return "ProblemSolver [id=" + id + ", problemid=" + problemid
				+ ", solver=" + solver + ", describe=" + describe
				+ ", solveDate=" + solveDate + ", isSolve=" + isSolve
				+ ", currentSolver=" + currentSolver + ", solveLevel="
				+ solveLevel + ", createOn=" + createOn + ", createBy="
				+ createBy + ", updateOn=" + updateOn + ", updateBy="
				+ updateBy + "]";
	}

	
}
