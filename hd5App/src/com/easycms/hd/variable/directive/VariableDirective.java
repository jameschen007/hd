package com.easycms.hd.variable.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.price.domain.Price;
import com.easycms.hd.price.service.PriceService;
import com.easycms.hd.variable.domain.VariableSet;
import com.easycms.hd.variable.service.VariableSetService;

/**
 * 获取单价列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2015-04-08 
 */
public class VariableDirective extends BaseDirective<VariableSet>  {
	private static final Logger logger = Logger.getLogger(VariableDirective.class);
	
	@Autowired
	private VariableSetService variableSetService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected VariableSet field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		String key = FreemarkerTemplateUtility.getStringValueFromParams(params, "key");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			VariableSet variableSet = variableSetService.findById(id);
			return variableSet;
		}
		// 查询name信息
		if (CommonUtility.isNonEmpty(key)) {
			VariableSet variableSet = variableSetService.findByKey(key);
			return variableSet;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<VariableSet> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<VariableSet> pager,
			Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<VariableSet> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
