package com.easycms.hd.variable.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

@Entity
@Table(name = "variable_set")
@Cacheable
public class VariableSet extends BasicForm implements Serializable {
	private static final long serialVersionUID = 8067982615955057775L;
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "setkey", nullable = false, length = 50)
	private String setkey;
	@Column(name = "setvalue", nullable = false, length = 500)
	private String setvalue;
	@Column(name = "type", length = 50)
	private String type;
	@JsonIgnore
	@Column(name = "status", length = 10)
	private String status;
	@JsonIgnore
	@Column(name = "createon", length = 0)
	private Date createon;
	@JsonIgnore
	@Column(name = "createby", length = 50)
	private String createby;
	@JsonIgnore
	@Column(name = "updateon", length = 0)
	private Date updateon;
	@JsonIgnore
	@Column(name = "updateby", length = 50)
	private String updateby;
	@Column(name = "mark_explain", nullable = false, length = 500)
	private String markExplain;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getSetkey() {
		return this.setkey;
	}

	public void setSetkey(String setkey) {
		this.setkey = setkey;
	}

	public String getSetvalue() {
		return this.setvalue;
	}

	public void setSetvalue(String setvalue) {
		this.setvalue = setvalue;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getCreateon() {
		return this.createon;
	}

	public void setCreateon(Date createon) {
		this.createon = createon;
	}
	
	public String getCreateby() {
		return this.createby;
	}

	public void setCreateby(String createby) {
		this.createby = createby;
	}
	
	public Date getUpdateon() {
		return this.updateon;
	}

	public void setUpdateon(Date updateon) {
		this.updateon = updateon;
	}

	public String getUpdateby() {
		return this.updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	
	
	public String getMarkExplain() {
		return markExplain;
	}

	public void setMarkExplain(String markExplain) {
		this.markExplain = markExplain;
	}


	public static String  CONRACTOR = "consteam";
	public static String  ASSIGN_TYPE = "assigntype";
	public static String TEAM = "team";
	public static String ENDMAN = "endman";
	
}