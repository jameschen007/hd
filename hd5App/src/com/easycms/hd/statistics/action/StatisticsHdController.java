package com.easycms.hd.statistics.action;

import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.hd.api.baseservice.question.QuestionTransForResult;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.question.RollingQuestionResult;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.itextpdf.text.log.SysoCounter;

@Controller
@RequestMapping("/statistics/plan")
@Transactional
public class StatisticsHdController {
  private Logger logger = Logger.getLogger(StatisticsHdController.class);
  


  @RequestMapping(value = "", method = RequestMethod.GET)
  public String listUI(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page 打开滚动计划统计html");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_plan");
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public String listData(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  打开滚动计划统计data");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/data_json_statistics_plan");
  }


  @RequestMapping(value = "plan", method = RequestMethod.GET)
  public String listUIplan(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_plan_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_plan_situation");
  }

  @RequestMapping(value = "plan", method = RequestMethod.POST)
  public String listDataplan(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  list_json_statistics_plan_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_plan_situation");
  }


  @RequestMapping(value = "witness", method = RequestMethod.GET)
  public String listUIwitness(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_witness_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_witness_situation");
  }

  @RequestMapping(value = "witness", method = RequestMethod.POST)
  public String listDatawitness(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  list_json_statistics_witness_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_witness_situation");
  }


  @RequestMapping(value = "hse", method = RequestMethod.GET)
  public String listUIhse(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_hse_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_hse_situation");
  }

  @RequestMapping(value = "hse", method = RequestMethod.POST)
  public String listDatahse(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {

    logger.debug("request question list  data list_json_statistics_hse_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_hse_situation");
  }


  @RequestMapping(value = "qc", method = RequestMethod.GET)
  public String listUIqc(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_qc_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_qc_situation");
  }

  @RequestMapping(value = "qc", method = RequestMethod.POST)
  public String listDataqc(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  list_json_statistics_qc_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_qc_situation");
  }


  @RequestMapping(value = "expire", method = RequestMethod.GET)
  public String listUIexpire(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_expire_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_expire_situation");
  }

  @RequestMapping(value = "expire", method = RequestMethod.POST)
  public String listDataexpire(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  list_json_statistics_expire_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_expire_situation");
  }


  @RequestMapping(value = "conference", method = RequestMethod.GET)
  public String listUIconference(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_conference_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_conference_situation");
  }

  @RequestMapping(value = "conference", method = RequestMethod.POST)
  public String listDataconference(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data  list_json_statistics_conference_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_conference_situation");
  }


  @RequestMapping(value = "notification", method = RequestMethod.GET)
  public String listUInotification(HttpServletRequest request, HttpServletResponse response,
		  @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list page list_statistics_notification_situation");
    
    return ForwardUtility.forwardAdminView("/hdService/statistics/list_statistics_notification_situation");
  }

  @RequestMapping(value = "notification", method = RequestMethod.POST)
  public String listDatanotification(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute("form") BasicForm form) {
    logger.debug("request question list  data list_json_statistics_notification_situation");
    form.setFilter(CommonUtility.toJson(form));
    return ForwardUtility.forwardAdminView("/hdService/statistics/data/list_json_statistics_notification_situation");
  }


}
