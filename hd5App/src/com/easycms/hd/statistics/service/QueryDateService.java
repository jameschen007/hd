package com.easycms.hd.statistics.service;

import com.easycms.hd.statistics.domain.QueryDate;

public interface QueryDateService {
	int deleteAll();
	QueryDate save(QueryDate queryDate);
}
