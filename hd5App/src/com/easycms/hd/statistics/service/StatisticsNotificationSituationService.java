package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsNotificationSituationBean;

public interface StatisticsNotificationSituationService {

	public List<StatisticsNotificationSituationBean> findAll(StatisticsNotificationSituationBean condition);


	public Page<StatisticsNotificationSituationBean> findPage(StatisticsNotificationSituationBean condition,Page<StatisticsNotificationSituationBean> page) ;
}
