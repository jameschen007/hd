package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsHseSituationBean;

public interface StatisticsHseSituationService {

	public List<StatisticsHseSituationBean> findAll(StatisticsHseSituationBean condition);


	public Page<StatisticsHseSituationBean> findPage(StatisticsHseSituationBean condition,Page<StatisticsHseSituationBean> page) ;
}
