package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsQcSituationBean;

public interface StatisticsQcSituationService {

	public List<StatisticsQcSituationBean> findAll(StatisticsQcSituationBean condition);


	public Page<StatisticsQcSituationBean> findPage(StatisticsQcSituationBean condition,Page<StatisticsQcSituationBean> page) ;
}
