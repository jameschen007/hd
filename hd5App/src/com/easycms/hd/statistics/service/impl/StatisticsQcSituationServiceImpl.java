package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsQcSituationDao;
import com.easycms.hd.statistics.domain.StatisticsQcSituationBean;
import com.easycms.hd.statistics.service.StatisticsQcSituationService;

@Service
public class StatisticsQcSituationServiceImpl implements StatisticsQcSituationService  {
	@Autowired
	private StatisticsQcSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsQcSituationBean> findAll(StatisticsQcSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsQcSituationBean> findPage(StatisticsQcSituationBean condition,Page<StatisticsQcSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsQcSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
