package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsPlanSituationDao;
import com.easycms.hd.statistics.domain.StatisticsPlanSituationBean;
import com.easycms.hd.statistics.service.StatisticsPlanSituationService;

@Service
public class StatisticsPlanSituationServiceImpl implements StatisticsPlanSituationService  {
	@Autowired
	private StatisticsPlanSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsPlanSituationBean> findAll(StatisticsPlanSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsPlanSituationBean> findPage(StatisticsPlanSituationBean condition,Page<StatisticsPlanSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsPlanSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
