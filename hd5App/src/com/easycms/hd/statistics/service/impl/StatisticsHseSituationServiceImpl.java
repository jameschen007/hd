package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsHseSituationDao;
import com.easycms.hd.statistics.domain.StatisticsHseSituationBean;
import com.easycms.hd.statistics.service.StatisticsHseSituationService;

@Service
public class StatisticsHseSituationServiceImpl implements StatisticsHseSituationService  {
	@Autowired
	private StatisticsHseSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsHseSituationBean> findAll(StatisticsHseSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsHseSituationBean> findPage(StatisticsHseSituationBean condition,Page<StatisticsHseSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsHseSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
