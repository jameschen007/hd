package com.easycms.hd.statistics.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.freemarker.QueryUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.dao.StatisticsConferenceSituationDao;
import com.easycms.hd.statistics.domain.StatisticsConferenceSituationBean;
import com.easycms.hd.statistics.service.StatisticsConferenceSituationService;

@Service
public class StatisticsConferenceSituationServiceImpl implements StatisticsConferenceSituationService  {
	@Autowired
	private StatisticsConferenceSituationDao statisticsPlanDao;
	
	@Override
	public List<StatisticsConferenceSituationBean> findAll(StatisticsConferenceSituationBean condition) {
		return statisticsPlanDao.findAll(QueryUtil.queryConditions(condition));
	}


	@Override
	public Page<StatisticsConferenceSituationBean> findPage(StatisticsConferenceSituationBean condition,Page<StatisticsConferenceSituationBean> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<StatisticsConferenceSituationBean> springPage = statisticsPlanDao.findAll(QueryUtil.queryConditions(condition),pageable);
		page.execute(Integer.valueOf(Long.toString(springPage
				.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
	
}
