package com.easycms.hd.statistics.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsPlan;

public interface StatisticsPlanService {

	public List<StatisticsPlan> findAll(StatisticsPlan condition);


	public Page<StatisticsPlan> findPage(StatisticsPlan condition,Page<StatisticsPlan> page) ;
}
