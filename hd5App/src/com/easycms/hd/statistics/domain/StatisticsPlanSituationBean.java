package com.easycms.hd.statistics.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.BasicForm;

import lombok.Data;

@Entity
@Table(name = "statistics_plan_situation")
@Data
public class StatisticsPlanSituationBean extends BasicForm implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	/** 创建时间 */
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "total")
	private Integer total;
	
	
	
	

	@Column(name = "completed")
	private Integer completed;
	@Column(name = "stagnate")
	private Integer stagnate;
	@Column(name = "doing")
	private Integer doing;
	@Column(name = "notstarted")
	private Integer notstarted;
	//模块类型：管道计划GDJH
	private String type;
}
