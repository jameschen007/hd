package com.easycms.hd.statistics.directive;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.WebUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.TaskHyperbolaResult;
import com.easycms.hd.plan.domain.TaskHyperbolaResultMap;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.StatisticsRollingPlanService;
import com.easycms.hd.statistics.domain.QueryDate;
import com.easycms.hd.statistics.service.QueryDateService;
import com.easycms.hd.statistics.util.StatisticsUtil;
import com.easycms.hd.user.service.HDUserService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

public class TaskHyperbolaStatisticsDirective extends BaseDirective<TaskHyperbolaResultMap> {
  private static final Logger logger = Logger.getLogger(TaskHyperbolaStatisticsDirective.class);
  private static String WELD_DISTINGUISH = "welddistinguish";

  @Autowired
  private RollingPlanService rollingPlanService;
  @Autowired
  private VariableSetService variableSetService;
  @Autowired
  private UserService userService;
  @Autowired
  private HDUserService hdUserService;
  @Autowired
  private StatisticsRollingPlanService statisticsRollingPlanService;
  @Autowired
  private QueryDateService queryDateService;

  @SuppressWarnings("rawtypes")
  @Override
  protected Integer count(Map params, Map<String, Object> envParams) {
    return null;
  }

  @SuppressWarnings("rawtypes")
  @Override
  protected TaskHyperbolaResultMap field(Map params, Map<String, Object> envParams) {
    return null;
  }

  @SuppressWarnings({"rawtypes"})
  @Override
  protected List<TaskHyperbolaResultMap> list(Map params, String filter, String order, String sort,
      boolean pageable, Page<TaskHyperbolaResultMap> pager, Map<String, Object> envParams) {
    String isByUser = FreemarkerTemplateUtility.getStringValueFromParams(params, "isByUser");
    if ("true".equalsIgnoreCase(isByUser)) {
      return getAllStasticsByUser(params);
    }
    return getAllStatics(params);
  }

  @SuppressWarnings("rawtypes")
  private List<TaskHyperbolaResultMap> getAllStatics(Map params) {
    String category = FreemarkerTemplateUtility.getStringValueFromParams(params, "category");
    String weldHK = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
    String weldZJ = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);
    logger.debug("[Hyperbola category] ==> " + category);
    int dateOfMonth = StatisticsUtil.datesOfMonth(StatisticsUtil.getMonth());

    List<TaskHyperbolaResultMap> taskHyperbolaResultList = new ArrayList<TaskHyperbolaResultMap>();

    TaskHyperbolaResultMap taskHyperbolaResult = new TaskHyperbolaResultMap();
    if (category.equals("currentMonth")) {
      String month = StatisticsUtil.getMonth();
      logger.debug("[status] ==> " + month);

      List<TaskHyperbolaResult> weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldHK), dateOfMonth);
      taskHyperbolaResult.setType("weldHK");
      taskHyperbolaResult.setResult(weldHKResult);
      taskHyperbolaResultList.add(taskHyperbolaResult);

      List<TaskHyperbolaResult> weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldZJ), dateOfMonth);
      taskHyperbolaResult = new TaskHyperbolaResultMap();
      taskHyperbolaResult.setType("weldZJ");
      taskHyperbolaResult.setResult(weldZJResult);
      taskHyperbolaResultList.add(taskHyperbolaResult);
    } else if (category.equals("search")) {
      String startTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "startTime");
      String endTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "endTime");

      Date start = WebUtility.convertStringToDate(startTime, "yyyy-MM-dd");
      Date end = WebUtility.convertStringToDate(endTime, "yyyy-MM-dd");

      queryDateService.deleteAll();

      List<String> dateList = StatisticsUtil.datePeriodList(start, end);
      for (String date : dateList) {
        QueryDate queryDate = new QueryDate();
        queryDate.setId(date);
        queryDateService.save(queryDate);
      }

      List<TaskHyperbolaResult> weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(statisticsRollingPlanService
              .getRollingPlanTaskHyperbolaByDate(startTime, endTime, weldHK), dateList.size());
      taskHyperbolaResult.setType("weldHK");
      taskHyperbolaResult.setResult(weldHKResult);
      taskHyperbolaResultList.add(taskHyperbolaResult);

      List<TaskHyperbolaResult> weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(statisticsRollingPlanService
              .getRollingPlanTaskHyperbolaByDate(startTime, endTime, weldZJ), dateList.size());
      taskHyperbolaResult = new TaskHyperbolaResultMap();
      taskHyperbolaResult.setType("weldZJ");
      taskHyperbolaResult.setResult(weldZJResult);
      taskHyperbolaResultList.add(taskHyperbolaResult);
    }

    return taskHyperbolaResultList;
  }

  private List<TaskHyperbolaResultMap> getAllStasticsByUser(Map params) {
    String month = StatisticsUtil.getMonth();
    User user = (User) WebUtility.getSession().getAttribute("user");
    User loginUser = userService.findUserById(user.getId());



    if (hdUserService.isClasz(loginUser.getId())) {
      Department dept = loginUser.getDepartment();
      String[] ids = new String[1];
      if (dept != null) {
        ids[0] = dept.getId() + "";
      }
      return getAllStatisticByClasz(month, user, params, ids);
    } else if (hdUserService.isGroup(loginUser.getId())) {
      String[] ids = {loginUser.getId() + ""};
      return getAllStatisticByGroup(month, user, params, ids);
    } else {
      return getAllStatics(params);
    }
  }

  private List<TaskHyperbolaResultMap> getAllStatisticByClasz(String month, User user, Map params,
      String[] ids) {
    int dateOfMonth = StatisticsUtil.datesOfMonth(StatisticsUtil.getMonth());
    String weldHK = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
    String weldZJ = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);

    String category = FreemarkerTemplateUtility.getStringValueFromParams(params, "category");

    String startTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "startTime");
    String endTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "endTime");
    logger.debug("[status] ==> " + month);

    logger.debug("[startTime] ==> " + startTime);
    logger.debug("[toDate] ==> " + endTime);

    int size = 0;
    if (category.equals("search")) {
      Date start = WebUtility.convertStringToDate(startTime, "yyyy-MM-dd");
      Date end = WebUtility.convertStringToDate(endTime, "yyyy-MM-dd");

      queryDateService.deleteAll();

      List<String> dateList = StatisticsUtil.datePeriodList(start, end);
      size = dateList.size();
      for (String date : dateList) {
        QueryDate queryDate = new QueryDate();
        queryDate.setId(date);
        queryDateService.save(queryDate);
      }
    }

    List<TaskHyperbolaResultMap> taskHyperbolaResultList = new ArrayList<TaskHyperbolaResultMap>();

    TaskHyperbolaResultMap taskHyperbolaResult = new TaskHyperbolaResultMap();

    List<TaskHyperbolaResult> weldHKResult = null;
    if (user.isDefaultAdmin()) {
      weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldHK), dateOfMonth);
    } else if (category.equals("currentMonth")) {
      weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldHK, ids),
              dateOfMonth);
    } else {
      weldHKResult =
          StatisticsUtil
              .objectToTaskHyperbolaResult(statisticsRollingPlanService
                  .getRollingPlanTaskHyperbolaByGroupAndDuration(startTime, endTime, weldHK, ids),
                  size);
    }
    taskHyperbolaResult.setType("weldHK");
    taskHyperbolaResult.setResult(weldHKResult);
    taskHyperbolaResultList.add(taskHyperbolaResult);


    List<TaskHyperbolaResult> weldZJResult = null;
    if (user.isDefaultAdmin()) {
      weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldZJ), dateOfMonth);
    } else if (category.equals("currentMonth")) {
      weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldZJ, ids),
              dateOfMonth);
    } else {
      weldZJResult =
          StatisticsUtil
              .objectToTaskHyperbolaResult(statisticsRollingPlanService
                  .getRollingPlanTaskHyperbolaByGroupAndDuration(startTime, endTime, weldZJ, ids),
                  size);
    }
    taskHyperbolaResult = new TaskHyperbolaResultMap();
    taskHyperbolaResult.setType("weldZJ");
    taskHyperbolaResult.setResult(weldZJResult);
    taskHyperbolaResultList.add(taskHyperbolaResult);

    return taskHyperbolaResultList;
  }

  private List<TaskHyperbolaResultMap> getAllStatisticByGroup(String month, User user, Map params,
      String[] ids) {
    int dateOfMonth = StatisticsUtil.datesOfMonth(StatisticsUtil.getMonth());

    String weldHK = variableSetService.findValueByKey("weldHK", WELD_DISTINGUISH);
    String weldZJ = variableSetService.findValueByKey("weldZJ", WELD_DISTINGUISH);

    String startTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "startTime");
    String endTime = FreemarkerTemplateUtility.getStringValueFromParams(params, "endTime");
    String category = FreemarkerTemplateUtility.getStringValueFromParams(params, "category");

    logger.debug("[status] ==> " + month);
    logger.debug("[startTime] ==> " + startTime);
    logger.debug("[endTime] ==> " + endTime);

    int size = 0;

    if (category.equals("search")) {
      Date start = WebUtility.convertStringToDate(startTime, "yyyy-MM-dd");
      Date end = WebUtility.convertStringToDate(endTime, "yyyy-MM-dd");

      queryDateService.deleteAll();

      List<String> dateList = StatisticsUtil.datePeriodList(start, end);
      size = dateList.size();
      for (String date : dateList) {
        QueryDate queryDate = new QueryDate();
        queryDate.setId(date);
        queryDateService.save(queryDate);
      }
    }

    List<TaskHyperbolaResultMap> taskHyperbolaResultList = new ArrayList<TaskHyperbolaResultMap>();

    TaskHyperbolaResultMap taskHyperbolaResult = new TaskHyperbolaResultMap();

    List<TaskHyperbolaResult> weldHKResult = null;
    if (user.isDefaultAdmin()) {
      weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldHK), dateOfMonth);
    } else if (category.equals("currentMonth")) {
      weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbolaByGroup(month, weldHK, ids),
              dateOfMonth);
    } else {
      weldHKResult =
          StatisticsUtil.objectToTaskHyperbolaResult(statisticsRollingPlanService
              .getRollingPlanTaskHyperbolaByDuration(startTime, endTime, weldHK, ids), size);
    }
    taskHyperbolaResult.setType("weldHK");
    taskHyperbolaResult.setResult(weldHKResult);
    taskHyperbolaResultList.add(taskHyperbolaResult);

    List<TaskHyperbolaResult> weldZJResult = null;
    if (user.isDefaultAdmin()) {
      weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbola(month, weldZJ), dateOfMonth);
    } else if (category.equals("currentMonth")) {
      weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(
              statisticsRollingPlanService.getRollingPlanTaskHyperbolaByGroup(month, weldZJ, ids),
              dateOfMonth);
    } else {
      weldZJResult =
          StatisticsUtil.objectToTaskHyperbolaResult(statisticsRollingPlanService
              .getRollingPlanTaskHyperbolaByDuration(startTime, endTime, weldZJ, ids), size);
    }
    taskHyperbolaResult = new TaskHyperbolaResultMap();
    taskHyperbolaResult.setType("weldZJ");
    taskHyperbolaResult.setResult(weldZJResult);
    taskHyperbolaResultList.add(taskHyperbolaResult);

    return taskHyperbolaResultList;
  }



  @SuppressWarnings("rawtypes")
  @Override
  protected List<TaskHyperbolaResultMap> tree(Map params, Map<String, Object> envParams) {
    return null;
  }



}
