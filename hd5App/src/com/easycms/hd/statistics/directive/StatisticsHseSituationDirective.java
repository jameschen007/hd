package com.easycms.hd.statistics.directive;

import java.util.List;
import java.util.Map;

import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.statistics.domain.StatisticsHseSituationBean;
import com.easycms.hd.statistics.service.StatisticsHseSituationService;

public class StatisticsHseSituationDirective extends BaseDirective<StatisticsHseSituationBean> {
 
	@Autowired
	private StatisticsHseSituationService statisticsPlanService;
	
	

	private Logger logger = Logger.getLogger(StatisticsHseSituationDirective.class);

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected StatisticsHseSituationBean field(Map params, Map<String, Object> envParams) {
		// 用户ID信息
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<StatisticsHseSituationBean> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<StatisticsHseSituationBean> pager,
			Map<String, Object> envParams) {
		
		String func = FreemarkerTemplateUtility.getStringValueFromParams(params, "func");
		String rollingType = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		
		String search = FreemarkerTemplateUtility.getStringValueFromParams(params, "searchValue");
		search = CommonUtility.removeBlank(search);
		logger.info("Search rollingType : " + rollingType);
		
		StatisticsHseSituationBean condition = null;
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(StatisticsHseSituationBean.class, filter,"yyyy-MM-dd");
//			JSONObject json = JSONObject.fromObject(filter);
//			if(json!=null && json.get("departmentId")!=null && CommonUtility.isNonEmpty(json.get("departmentId").toString()) && !"null".equals(json.get("departmentId").toString().trim())){
//				Department dept = departmentService.findById(Integer.valueOf(json.get("departmentId").toString()));
//				condition.setDepartment(dept);
//			}
		}else{
			condition = new StatisticsHseSituationBean();
		}
		
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		// 查询列表
		if (pageable) {
			if (CommonUtility.isNonEmpty(search)){
				try {
					condition.setCreatedOn(DateUtils.parseDate(search));
				} catch (DateParseException e) {
					e.printStackTrace();
				}
				pager = statisticsPlanService.findPage(condition, pager);
			} else {
				pager = statisticsPlanService.findPage(condition, pager);
			}
			return pager.getDatas();
		} else {
			List<StatisticsHseSituationBean> datas = statisticsPlanService.findAll(condition);
			logger.debug("==> StatisticsHseSituationBean length [" + datas.size() + "]");
			return datas;
		}
	}

	@Override
	protected List<StatisticsHseSituationBean> tree(Map params, Map<String, Object> envParams) {
		return null;
	}


}
