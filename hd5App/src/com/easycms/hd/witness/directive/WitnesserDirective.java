package com.easycms.hd.witness.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.management.user.domain.User;

public class WitnesserDirective extends BaseDirective<WorkStepWitness>{
	private static final Logger logger = Logger.getLogger(WitnesserDirective.class);
	@Autowired
	private WitnessService witnessService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected WorkStepWitness field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		logger.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			WorkStepWitness workStepWitness = witnessService.findById(id);
			return workStepWitness;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepWitness> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<WorkStepWitness> pager,
			Map<String, Object> envParams) {
		User loginUser = ContextUtil.getCurrentLoginUser();
		String type =  (String) ContextUtil.getSession().getAttribute("type");
		
		String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
		WorkStepWitness condition = null;
		
		if(CommonUtility.isNonEmpty(filter)) {
			condition = CommonUtility.toObject(WorkStepWitness.class, filter,"yyyy-MM-dd HH:mm:ss");
		}else{
			condition = new WorkStepWitness();
		}
		
		if (condition.getId() != null && condition.getId() == 0) {
			condition.setId(null);
		}
		if (CommonUtility.isNonEmpty(order)) {
			condition.setOrder(order);
		}

		if (CommonUtility.isNonEmpty(sort)) {
			condition.setSort(sort);
		}
		
		boolean QC2Member = false;
		boolean QC1Member = false;
		if (null != loginUser.getRoleType() && !loginUser.getRoleType().isEmpty()) {
			for (String s : loginUser.getRoleType()) {
				if (s.equals("witness_member_qc2")) {
					QC2Member = true;
				}
				if (s.equals("witness_member_qc1")) {
					QC1Member = true;
				}
			}
		}
		//因已满足qc1ReplaceQc2要求，不用修改。

		// 查询列表
		if (pageable) {
			if (null != custom && custom.equals("myevent")){
				if (QC2Member || QC1Member) {
					pager = witnessService.findQCMemberUnCompleteWitnessByPage(loginUser.getId(), type, pager);
				} else {
					pager = witnessService.findQC2DownLineMemberUnCompleteWitnessByPage(loginUser.getId(), type, pager);
				}
				logger.info("==> Witnesser UnComplate length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			} else if (null != custom && custom.equals("myeventAlready")){
				if (QC2Member || QC1Member) {
					pager = witnessService.findQCMemberCompleteWitnessByPage(loginUser.getId(), type, pager);
				} else {
					pager = witnessService.findQC2DownLineMemberCompleteWitnessByPage(loginUser.getId(), type, pager);
				}
				logger.info("==> Witnesser Complate length [" + pager.getDatas().size() + "]");
				return pager.getDatas();
			}
			
			return null;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<WorkStepWitness> tree(Map params, Map<String, Object> envParams) {
		return null;
	}
}
