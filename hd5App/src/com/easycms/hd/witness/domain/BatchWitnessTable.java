package com.easycms.hd.witness.domain;

import java.io.Serializable;

public class BatchWitnessTable implements Serializable{
	private static final long serialVersionUID = 120342785008215510L;
	
	private boolean AQAW = false;
	private boolean AQAH = false;
	private boolean AQAR = false;
	private boolean AQC1W = false;
	private boolean AQC1H = false;
	private boolean AQC1R = false;
	private boolean AQC2W = false;
	private boolean AQC2H = false;
	private boolean AQC2R = false;
	private boolean BW = false;
	private boolean BH = false;
	private boolean BR = false;
	private boolean CW = false;
	private boolean CH = false;
	private boolean CR = false;
	private boolean DW = false;
	private boolean DH = false;
	private boolean DR = false;
	
	public boolean getAQAW() {
		return AQAW;
	}
	public void setAQAW(boolean aQAW) {
		AQAW = aQAW;
	}
	public boolean getAQAH() {
		return AQAH;
	}
	public void setAQAH(boolean aQAH) {
		AQAH = aQAH;
	}
	public boolean getAQAR() {
		return AQAR;
	}
	public void setAQAR(boolean aQAR) {
		AQAR = aQAR;
	}
	public boolean getAQC1W() {
		return AQC1W;
	}
	public void setAQC1W(boolean aQC1W) {
		AQC1W = aQC1W;
	}
	public boolean getAQC1H() {
		return AQC1H;
	}
	public void setAQC1H(boolean aQC1H) {
		AQC1H = aQC1H;
	}
	public boolean getAQC1R() {
		return AQC1R;
	}
	public void setAQC1R(boolean aQC1R) {
		AQC1R = aQC1R;
	}
	public boolean getAQC2W() {
		return AQC2W;
	}
	public void setAQC2W(boolean aQC2W) {
		AQC2W = aQC2W;
	}
	public boolean getAQC2H() {
		return AQC2H;
	}
	public void setAQC2H(boolean aQC2H) {
		AQC2H = aQC2H;
	}
	public boolean getAQC2R() {
		return AQC2R;
	}
	public void setAQC2R(boolean aQC2R) {
		AQC2R = aQC2R;
	}
	public boolean getBW() {
		return BW;
	}
	public void setBW(boolean bW) {
		BW = bW;
	}
	public boolean getBH() {
		return BH;
	}
	public void setBH(boolean bH) {
		BH = bH;
	}
	public boolean getBR() {
		return BR;
	}
	public void setBR(boolean bR) {
		BR = bR;
	}
	public boolean getCW() {
		return CW;
	}
	public void setCW(boolean cW) {
		CW = cW;
	}
	public boolean getCH() {
		return CH;
	}
	public void setCH(boolean cH) {
		CH = cH;
	}
	public boolean getCR() {
		return CR;
	}
	public void setCR(boolean cR) {
		CR = cR;
	}
	public boolean getDW() {
		return DW;
	}
	public void setDW(boolean dW) {
		DW = dW;
	}
	public boolean getDH() {
		return DH;
	}
	public void setDH(boolean dH) {
		DH = dH;
	}
	public boolean getDR() {
		return DR;
	}
	public void setDR(boolean dR) {
		DR = dR;
	}
}
