package com.easycms.hd.witness.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.witness.domain.WitnessFile;

@Transactional
public interface WitnessFileDao extends Repository<WitnessFile,Integer>,BasicDao<WitnessFile>{

	@Query("From WitnessFile wf where wf.witnessid = ?1")
	List<WitnessFile> findByWitnessId(Integer id);

}
