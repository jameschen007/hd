package com.easycms.workflow.exception;


public class NoTaskFoundError extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7043472125107236501L;

	public NoTaskFoundError() {
		super(" Do not find the task by process instance");
	}
	
	public NoTaskFoundError(String msg) {
		super(msg);
	}
	
}
