package com.easycms.workflow.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.workflow.domain.TraceTask;

public interface TraceTaskService{
	
	/**
	 * 添加一条跟踪任务
	 * @param traceTask
	 * @return
	 */
	TraceTask addTraceTask(TraceTask traceTask);
	
	/**
	 * 更新跟踪任务
	 * @param traceTask
	 * @return
	 */
	TraceTask updateTraceTask(TraceTask traceTask);
	
	boolean delete(String[] processInstanceIds);
	
	/**
	 * 根据任务ID获得跟踪任务
	 * @param taskId
	 * @return
	 */
	TraceTask getByTaskId(String taskId);
	
	/**
	 * 根据当前处理人ID查询所有跟踪任务
	 * @param currentHandleId
	 * @return
	 */
	List<TraceTask> getAllByCurrentHanldeId(String currentHandleId);
	
	/**
	 * 根据当前处理ID查询分页跟踪任务
	 * @param currentHandleId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getAllByCurrentHanldeId(String currentHandleId,Page<TraceTask> page);
	
	/**
	 * 根据当前处理ID查询已经完成的跟踪任务
	 * @param currentHandleId
	 * @return
	 */
	List<TraceTask> getFinishByCurrentHanldeId(String currentHandleId);
	
	/**
	 * 根据当前处理ID查询已经完成的分页跟踪任务
	 * @param currentHandleId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getFinishByCurrentHanldeId(String currentHandleId,Page<TraceTask> page);
	
	/**
	 * 根据当前处理ID查询正在进行的跟踪任务
	 * @param currentHandleId
	 * @return
	 */
	List<TraceTask> getProcessingByCurrentHanldeId(String currentHandleId);
	
	/**
	 * 根据当前处理ID查询正在进行的分页跟踪任务
	 * @param currentHandleId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getProcessingByCurrentHanldeId(String currentHandleId,Page<TraceTask> page);
	
	/**
	 * 根据发布ID查询所有跟踪任务
	 * @param deployId
	 * @return
	 */
	List<TraceTask> getAllByDeployId(String deployId);
	
	/**
	 * 根据发布ID查询分页跟踪任务
	 * @param deployId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getAllByDeployId(String deployId,Page<TraceTask> page);
	
	/**
	 * 根据发布ID查询已完成跟踪任务
	 * @param deployId
	 * @return
	 */
	List<TraceTask> getFinishByDeployId(String deployId);
	
	/**
	 * 根据发布ID查询分页已完成任务
	 * @param deployId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getFinishByDeployId(String deployId,Page<TraceTask> page);
	
	/**
	 * 根据发布ID查询正在进行任务
	 * @param deployId
	 * @return
	 */
	List<TraceTask> getProcessingByDeployId(String deployId);
	
	/**
	 * 根据发布ID查询正在进行任务的分页
	 * @param deployId
	 * @param page
	 * @return
	 */
	Page<TraceTask> getProcessingByDeployId(String deployId,Page<TraceTask> page);
}
