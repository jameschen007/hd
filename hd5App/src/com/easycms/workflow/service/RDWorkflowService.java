package com.easycms.workflow.service;

import java.util.List;

import com.easycms.workflow.domain.WorkflowZIP;

public interface RDWorkflowService{
	
	/**
	 * 根据角色ID查找用户ID
	 * @param roleId
	 * @return
	 */
	List<String> findUserId(Integer roleId);
	
	/**
	 * 查找部门下所有用户ID
	 * @param departmentId
	 * @return
	 */
	List<String> findUserIds(Integer departmentId);
	
	/**
	 * 查找部门下对应角色的所有用户ID
	 * @param departmentId
	 * @param roleId
	 * @return
	 */
	List<String> findUserIds(Integer departmentId,List<Integer> roleIds);
	
	
	
	WorkflowZIP findFlowZip(Integer id);
	
	List<WorkflowZIP> findFlowZip(Integer[] ids);
	
	List<WorkflowZIP> findFlowZip();
	
	WorkflowZIP addFlowZip(WorkflowZIP flowZip);
	
	WorkflowZIP updateFlowZip(WorkflowZIP flowZip);
	
	boolean deleteFlowZip(Integer[] ids);
	
	
}
