package com.easycms.workflow.service.impl;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.workflow.service.WorkflowService;

public class WorkflowServiceImpl implements WorkflowService{

	@Autowired
	private RoleService roleService;
	@Autowired
	private DepartmentService departmentService;

	@Override
	public List<String> findUserId(Integer roleId) {
		Role role = roleService.findRole(roleId);
		if(role == null) {
			return null;
		}
		
		List<User> users = role.getUsers();
		if(users == null || users.size() == 0) {
			return null;
		}
		
		List<String> userIds = new ArrayList<String>();
		for(User user : users) {
			userIds.add(user.getId().toString());
		}
		return userIds;
	}

	@Override
	public List<String> findUserIds(Integer departmentId) {
//		Department department = departmentService.findById(departmentId);
//		if(department == null) {
//			return null;
//		}
//		
//		List<Role> roles = department.getRoles();
//		if(roles == null || roles.size() == 0) {
//			return null;
//		}
//		
//		Set<String> userIds = new HashSet<String>();
//		for(Role r : roles) {
//			List<User> users = r.getUsers();
//			if(users == null || users.size() == 0) {
//				continue;
//			}
//			for(User u : users) {
//				userIds.add(u.getId().toString());
//			}
//		}
//		return new ArrayList<String>(userIds);
		
		return null;
	}

	@Override
	public List<String> findUserIds(Integer departmentId, Integer roleId) {
		// TODO Auto-generated method stub
		return null;
	}
}
