package com.easycms.workflow.domain;

import java.io.Serializable;

public class WorkflowNode implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1677689130385357159L;
	
	private String processDefId;	//流程定义ID
	private Integer x;	//X坐标
	private Integer y;	//Y坐标
	private Integer width;	//宽
	private Integer height;	//高
	
	public String getProcessDefId() {
		return processDefId;
	}
	public void setProcessDefId(String processDefId) {
		this.processDefId = processDefId;
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
