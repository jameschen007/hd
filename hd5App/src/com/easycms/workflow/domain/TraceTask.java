package com.easycms.workflow.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="ec_workflow_trace_task")
public class TraceTask {
	@Id
	@GenericGenerator(name="idGenerator", strategy="uuid")
	@GeneratedValue(generator="idGenerator",strategy=GenerationType.AUTO)
	@Column(name="trace_id")
	private String traceId;	//跟踪ID
	
	private String pid;	//流程实例ID
	private String pdid;	//流程定义ID
	
	@Column(name="deploy_id")
	private String deployId;	//发布人ID
	@Column(name="current_handle_id")
	private String currentHandleId;	//当前处理人Id
	@Column(name="pre_handle_id")
	private String preHandleId;	//前处理人Id
	@Column(name="task_id")
	private String taskId;	//节点Id
	@Column(name="pre_task_id")
	private String preTaskId;	//前节点Id
	@Column(name="handle_date")	//处理日期
	private Date handleDate;	//开始日期
	@Column(name="start_date")
	private Date startDate;	//开始日期
	@Column(name="end_date")
	private Date endDate;	//结束日期
	private Integer duration;	//持续时间
	@Column(name="task_status")
	private String taskStatus;	//任务状态
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getPdid() {
		return pdid;
	}
	public void setPdid(String pdid) {
		this.pdid = pdid;
	}
	public String getDeployId() {
		return deployId;
	}
	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}
	public String getCurrentHandleId() {
		return currentHandleId;
	}
	public void setCurrentHandleId(String currentHandleId) {
		this.currentHandleId = currentHandleId;
	}
	public String getPreHandleId() {
		return preHandleId;
	}
	public void setPreHandleId(String preHandleId) {
		this.preHandleId = preHandleId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getPreTaskId() {
		return preTaskId;
	}
	public void setPreTaskId(String preTaskId) {
		this.preTaskId = preTaskId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	public Date getHandleDate() {
		return handleDate;
	}
	public void setHandleDate(Date handleDate) {
		this.handleDate = handleDate;
	}
	
	
}
