package com.easycms.workflow.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.workflow.domain.TraceTask;
import com.easycms.workflow.domain.WorkflowNode;
import com.easycms.workflow.exception.NoSignTaskFoundError;
import com.easycms.workflow.exception.NoTaskFoundError;
import com.easycms.workflow.exception.StartProcessInstanceError;
import com.easycms.workflow.service.RDWorkflowService;
import com.easycms.workflow.service.TraceTaskService;

@Component("workflowHelper")
public class WorkflowHelper {

	private static final Logger logger = Logger.getLogger(WorkflowHelper.class);
	public static final String TASK_STATUS_PROCESSING = "processing";
	public static final String TASK_STATUS_FINISH = "finish";

	@Autowired
	private RDWorkflowService workflowService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private TraceTaskService traceTaskService;
	@Autowired
	private HistoryService historyService;

	/**
	 * 开始一个流程
	 * 
	 * @param processDefKey
	 *            流程定义key
	 * @param businessKey
	 *            业务系统关联数据
	 * @param variables
	 *            传递给xml的变量
	 * @return
	 * @throws StartProcessInstanceError
	 */
	public Task startProcessInstance(String userId, String processDefKey,
			String businessKey, Map<String, Object> variables)
			throws StartProcessInstanceError {
		ProcessInstance pi = runtimeService.startProcessInstanceByKey(
				processDefKey, businessKey, variables);

		if (pi.getId() == null) {
			logger.debug("==> Start process instance faild.");
			throw new StartProcessInstanceError("start process instance ["
					+ processDefKey + "] failed");
		}

		Task t = taskService.createTaskQuery().processInstanceId(pi.getId())
				.singleResult();

		// 任务跟踪表插入数据
		TraceTask traceTask = new TraceTask();
		traceTask.setPid(t.getProcessInstanceId());
		traceTask.setPdid(t.getProcessDefinitionId());
		traceTask.setDeployId(userId);
		traceTask.setTaskId(t.getId());
		traceTask.setCurrentHandleId(t.getAssignee());
		traceTask.setPreHandleId(userId);
		traceTask.setStartDate(t.getCreateTime());
		traceTask.setTaskStatus(TASK_STATUS_PROCESSING);

		traceTaskService.addTraceTask(traceTask);

		return t;
	}

	/**
	 * 签收任务 具有此任务处理权限的人都可以看到，需要签收此任务，才能将任务纳为该用户的任务
	 * 
	 * @param taskId
	 *            任务ID
	 * @param userId
	 *            用户ID
	 * @param processInstanceId
	 *            流程实例ID，需要根据该ID查询出任务
	 * @return
	 * @throws NoTaskFoundError
	 */
	public Task toSignTask(String taskId, String userId)
			throws NoTaskFoundError {
		// 签收任务
		taskService.claim(taskId, userId);

		// 从跟踪表里查找流程实例ID
		TraceTask traceTask = traceTaskService.getByTaskId(taskId);
		// 返回此签收的任务
		Task t = taskService.createTaskQuery()
				.processInstanceId(traceTask.getPid()).singleResult();
		if (t == null) {
			throw new NoTaskFoundError(
					" Do not find the task by process instance id :"
							+ traceTask.getPid());
		}

		// 更新跟踪表
		traceTask.setPreTaskId(taskId);
		traceTask.setTaskId(t.getId());
		traceTask.setCurrentHandleId(userId);

		traceTaskService.updateTraceTask(traceTask);
		return t;
	}

	public void finishTask(String userId, String taskId,
			Map<String, Object> variables) {
		taskService.complete(taskId, variables);

		// 更新跟踪任务
		TraceTask traceTask = traceTaskService.getByTaskId(taskId);
		Task task = taskService.createTaskQuery()
				.processInstanceId(traceTask.getPid()).singleResult();
		if (task != null) {
			traceTask.setTaskId(task.getId());
			traceTask.setCurrentHandleId(task.getAssignee());
			traceTask.setPreHandleId(userId);
			traceTask.setPreTaskId(taskId);
			traceTask.setHandleDate(task.getCreateTime());
		} else {
			// 若任务完成，去历史记录查找
			HistoricTaskInstance hti = historyService
					.createHistoricTaskInstanceQuery().taskId(taskId)
					.finished().singleResult();
			traceTask.setCurrentHandleId(null);
			traceTask.setHandleDate(hti.getEndTime());
			traceTask.setEndDate(hti.getEndTime());
			traceTask.setTaskStatus(TASK_STATUS_FINISH);
			traceTask.setDuration(hti.getDurationInMillis().intValue());
		}

		traceTaskService.updateTraceTask(traceTask);
	}

	/**
	 * 返回待签收的任务
	 * 
	 * @param userId
	 *            用户ID
	 * @return
	 * @throws NoSignTaskFoundError
	 */
	public List<Task> getToSignTasks(String userId) throws NoSignTaskFoundError {
		List<Task> tasks = taskService.createTaskQuery().active()
				.taskCandidateUser(userId).list();
		if (tasks == null || tasks.size() == 0) {
			throw new NoSignTaskFoundError("No sign tasks found by userId:"
					+ userId);
		}
		return tasks;
	}

	/**
	 * 分页获得待签收的任务
	 * 
	 * @param userId
	 *            用户ID
	 * @param pagenumber
	 *            当前页数
	 * @param pagesize
	 *            分页大小
	 * @return
	 * @throws NoSignTaskFoundError
	 */
	public List<Task> getToSignTasks(String userId, Integer pagenumber,
			Integer pagesize) throws NoSignTaskFoundError {
		TaskQuery query = taskService.createTaskQuery().active()
				.taskCandidateUser(userId);
		List<Task> tasks = query
				.listPage((pagenumber - 1) * pagesize, pagesize);
		if (tasks == null || tasks.size() == 0) {
			throw new NoSignTaskFoundError("No sign tasks found by userId:"
					+ userId);
		}
		return tasks;
	}

	/**
	 * 根据userID获得待处理的任务
	 * 
	 * @param userId
	 * @return
	 * @throws NoTaskFoundError
	 */
	public List<Task> getTotoTasks(String userId) throws NoTaskFoundError {
		List<Task> tasks = taskService.createTaskQuery().taskAssignee(userId)
				.active().list();
		if (tasks == null || tasks.size() == 0) {
			throw new NoTaskFoundError(" Do not find the tasks by user id :"
					+ userId);
		}

		// List<TraceTask> traceTasks =
		// traceTaskService.getProcessingByCurrentHanldeId(userId);
		//
		// if(traceTasks != null && traceTasks.size() != 0) {
		// for(TraceTask traceTask : traceTasks) {
		// }
		// }
		return tasks;

	}

	/**
	 * 根据userID获得待处理的分页任务
	 * 
	 * @param userId
	 * @param page
	 * @return
	 * @throws NoTaskFoundError
	 */
	public Page<Task> getTotoTasks(String userId, Integer pageNum,
			Integer pagesize) throws NoTaskFoundError {
		Integer totalCounts = (int) taskService.createTaskQuery()
				.taskAssignee(userId).active().count();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee(userId)
				.active().listPage(pageNum - 1, pagesize);
		Page<Task> page = new Page<Task>(pagesize, totalCounts, pageNum, tasks);
		if (tasks == null || tasks.size() == 0) {
			throw new NoTaskFoundError(" Do not find the tasks by user id :"
					+ userId);
		}
		return page;
	}

	/**
	 * 根据userId获得已发布的任务
	 * 
	 * @param userId
	 * @return
	 * @throws NoTaskFoundError
	 */
	public List<Task> getDeployedTasks(String userId) throws NoTaskFoundError {

		List<TraceTask> traceTasks = traceTaskService.getAllByDeployId(userId);
		List<Task> tasks = null;
		if (traceTasks != null && traceTasks.size() != 0) {
			tasks = new ArrayList<Task>();
			for (TraceTask traceTask : traceTasks) {
				Task task = taskService.createTaskQuery()
						.taskId(traceTask.getTaskId()).singleResult();
				if (task != null) {
					tasks.add(task);
				}
			}
		}
		return tasks;
	}

	public Page<Task> getDeployedTasks(String userId, Integer pagenum,
			Integer pagesize) throws NoTaskFoundError {
		Page<Task> page = null;
		Page<TraceTask> traceTaskPage = new Page<TraceTask>(pagenum, pagesize);
		traceTaskPage = traceTaskService
				.getAllByDeployId(userId, traceTaskPage);
		List<Task> tasks = null;
		if (traceTaskPage.getDatas() != null
				&& traceTaskPage.getDatas().size() != 0) {
			tasks = new ArrayList<Task>();
			for (TraceTask traceTask : traceTaskPage.getDatas()) {
				Task task = taskService.createTaskQuery()
						.taskId(traceTask.getTaskId()).singleResult();
				if (task != null) {
					tasks.add(task);
				}
			}
			page = new Page<Task>(pagesize, traceTaskPage.getTotalCounts(),
					pagenum, tasks);
		}
		return page;
	}

	/**
	 * 根据流程定义ID获得该流程所有节点信息
	 * 
	 * @param processDefId
	 * @return
	 */
	public List<WorkflowNode> getWorkflowNodesByProcessDefId(String processDefId) {
		List<WorkflowNode> nodes = new ArrayList<WorkflowNode>();
		ProcessDefinitionEntity proDef = (ProcessDefinitionEntity) repositoryService
				.getProcessDefinition(processDefId);
		List<ActivityImpl> activityImplList = proDef.getActivities();
		if (activityImplList != null && activityImplList.size() != 0) {
			for (ActivityImpl activityImpl : activityImplList) {
				WorkflowNode workflowNode = new WorkflowNode();
				workflowNode.setProcessDefId(proDef.getDeploymentId());
				workflowNode.setX(activityImpl.getX());
				workflowNode.setY(activityImpl.getY());
				workflowNode.setWidth(activityImpl.getWidth());
				workflowNode.setHeight(activityImpl.getHeight());
				nodes.add(workflowNode);
			}
		}
		return nodes;
	}

	public WorkflowNode getActiveWorkflowNodeByTaskId(String taskId) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		String proDefId = task.getProcessDefinitionId();
		ProcessDefinitionEntity proDef = (ProcessDefinitionEntity) repositoryService
				.getProcessDefinition(proDefId);
		List<ActivityImpl> activityImplList = proDef.getActivities();

		// 获取当前节点ID
		Execution execution = runtimeService.createExecutionQuery()
				.executionId(task.getProcessInstanceId()).singleResult();
		String activitiId = execution.getActivityId();
		WorkflowNode workflowNode = null;
		for(ActivityImpl activityImpl : activityImplList) {
			if(activitiId.equals(activityImpl.getId())) {
				workflowNode = new WorkflowNode();
				workflowNode.setProcessDefId(proDef.getDeploymentId());
				workflowNode.setX(activityImpl.getX());
				workflowNode.setY(activityImpl.getY());
				workflowNode.setWidth(activityImpl.getWidth());
				workflowNode.setHeight(activityImpl.getHeight());
				break;
			}
		}
		return workflowNode;
	}

	public List<String> findUserIds(String departmentIdStr, String[] roleIds) {
		logger.debug("==> Come in findUserIds(String departmentIdStr,String[] roleIds)");
		logger.debug("[roleIds] = " + CommonUtility.toJson(roleIds));

		Integer departmentId = Integer.parseInt(departmentIdStr);
		List<Integer> list = new ArrayList<Integer>();
		for (String roleId : roleIds) {
			list.add(Integer.parseInt(roleId));
		}
		return workflowService.findUserIds(departmentId, list);
	}

	/**
	 * 生成图片
	 * 
	 * @param response
	 * @param deploymentId
	 * @throws IOException
	 */
	public void createImg(HttpServletResponse response, String deploymentId)
			throws IOException {
		ProcessDefinition processDefinition = repositoryService
				.createProcessDefinitionQuery().deploymentId(deploymentId)
				.singleResult();

		String diagramResourceName = processDefinition.getDiagramResourceName();

		logger.debug("==> Write img to client.");
		InputStream in = repositoryService.getResourceAsStream(
				processDefinition.getDeploymentId(), diagramResourceName);
		response.setContentType("image/*");
		OutputStream out = response.getOutputStream();
		IOUtils.copy(in, out);
		IOUtils.closeQuietly(in);
		IOUtils.closeQuietly(out);
	}
}
