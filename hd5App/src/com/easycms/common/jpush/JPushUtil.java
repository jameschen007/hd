package com.easycms.common.jpush;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


public class JPushUtil {
	private static final Logger logger = Logger.getLogger(JPushUtil.class);
	
	public static Map<String, String> transBean2Map(JPushExtra extra) {
		if (extra == null) {
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(extra.getClass());
			PropertyDescriptor[] propertyDescriptors = beanInfo
					.getPropertyDescriptors();
			for (PropertyDescriptor property : propertyDescriptors) {
				String key = property.getName();

				if (!key.equals("class")) {
					Method getter = property.getReadMethod();
					String value = (String) getter.invoke(extra);
					map.put(key, value);
				}

			}
		} catch (Exception e) {
			logger.info("transBean2Map Error " + e);
		}
		return map;
	}
}
