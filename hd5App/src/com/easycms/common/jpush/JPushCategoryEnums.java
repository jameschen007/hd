package com.easycms.common.jpush;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum JPushCategoryEnums {
    CONS_ASSIGN("施工分派"), 
    CONS_REASSIGN("施工改派"), 
    CONS_RELEASE("施工解派"), 
    WITNESS_LAUNCH("发起见证"), 
    WITNESS_RELAUNCH("再次发起见证"), 
    WITNESS_CANCEL("取消见证"), 
    WITNESS_ASSIGN("见证分派"), 
    WITNESS_RESULT("见证结果"), 
    WITNESS_QC2_ASSIGN("QC2见证分派"),
    API_CALL("api调用"),
	CONFERENCE("发起会议"),
	CONFERENCE_CANCEL("取消会议"),
	NOTIFICATION("通知"),
	QUESTION_CREATE("作业问题创建"),
	QUESTION_ASSIGN("作业问题指派"),
	QUESTION_FEEDBACK("作业问题回执"),
	QUESTION_ANSWER("作业问题回答"),
	EXPIRE_NOTICE("失效文件通知"),
	
	WORKSTEP_COMPLETE("工序完成"),
	ROLLINGPLAN_FILLBACK_COMPLETE("滚动计划回填完成"),
	ROLLINGPLAN_COMPLETE("滚动计划完成"),
	HSE_ASSIGN("安全问题分派");

    private String name;
    JPushCategoryEnums(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    /**
     * enum lookup map
     */
    private static final Map<String, String> lookup = new HashMap<String, String>();

    static {
        for (JPushCategoryEnums s : EnumSet.allOf(JPushCategoryEnums.class)) {
            lookup.put(s.name(), s.getName());
        }
    }

    public static  Map<String, String> getMap(){
        return lookup;
    }
}
