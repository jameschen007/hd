package com.easycms.common.logic.context;

import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 通过计算之后获得的常量值
 * @author wangz
 *
 */
@Service("computedConstantVar")
public class ComputedConstantVar {
	public static final Logger logger = Logger.getLogger(ComputedConstantVar.class);
	@Value("#{APP_SETTING['enpower_host']}")
	private String enpowerHost;
	@Value("#{APP_SETTING['enpower_host10']}")
	private String enpowerHost10;
	@Value("#{APP_SETTING['enpower_host_FQ']}")
	private String enpowerHost_FQ;

	@Value("#{APP_SETTING['enpower_host_HY']}")
	private String enpowerHost_HY;
	@Value("#{APP_SETTING['enpower_host_SM']}")
	private String enpowerHost_SM;
	@Value("#{APP_SETTING['enpower_host_XP']}")
	private String enpowerHost_XP;

	@Value("#{APP_SETTING['enpower_host_K2K3']}")
	private String enpowerHost_K2K3;

	@Value("#{APP_SETTING['enpower_api_getdbinfo']}")
	public String enpowerApiInfo;
	
	//本类 类部计算变量
	private static String enpowerHost_out = null ;

	/**通过计算输出 访问Enpower接口的地址*/
	public String getEnpowerHost(){
		if(enpowerHost_out==null){

			if(ConstantVar.HOST_ADDRESS_HY.equals(ContextPath.hostAddress)){
				enpowerHost_out = enpowerHost_HY ;
			}else if(ConstantVar.HOST_ADDRESS_K2K3.equals(ContextPath.hostAddress)){//卡项
				enpowerHost_out = enpowerHost_K2K3 ;
			}else if(ConstantVar.HOST_ADDRESS_FQ.equals(ContextPath.hostAddress)){//福清
				enpowerHost_out = enpowerHost_FQ ;
			}else if(ConstantVar.HOST_ADDRESS_XP.equals(ContextPath.hostAddress)){//霞浦
				enpowerHost_out = enpowerHost_XP ;
			}else if(ConstantVar.HOST_ADDRESS_SM.equals(ContextPath.hostAddress)){//三门
				enpowerHost_out = enpowerHost_SM ;
			}else if(ConstantVar.HOST_ADDRESS10.equals(ContextPath.hostAddress)){
				enpowerHost_out = enpowerHost10;
			}else if(ContextPath.hostAddress.startsWith("10.1.")){//五公司内办公环境
				enpowerHost_out = enpowerHost10;
			}else{
				enpowerHost_out= enpowerHost ;
			}
			
			logger.error("hostAddressAndEnpowerAddress关键信息确认：本机="+ContextPath.hostAddress+" Enpower接口="+enpowerHost_out);
		}
		return enpowerHost_out ;
	}

	/**
	 * 是否QC２。
	 * 或福清QC1ReplaceQc2返回true
	 * */
	public static boolean qc2OrFqQc1(Set<String> roleNameList){
		synchronized(roleNameList) {
			if(roleNameList==null||roleNameList.size()==0){
				return false ;
			}
			return  roleNameList.contains(ConstantVar.witness_member_qc2) || ( ContextPath.qc1ReplaceQc2 && roleNameList.contains(ConstantVar.witness_member_qc1) ) ;
		}
	}
	/**
	 * 即是QC１组长又是QC１组员
	 * */
	public static boolean qc1TeamAndMumber(Set<String> roleNameList){
		synchronized(roleNameList) {
			if(roleNameList==null||roleNameList.size()==0){
				return false ;
			}
			return  roleNameList.contains(ConstantVar.witness_member_qc1) &&  roleNameList.contains(ConstantVar.witness_team_qc1) ;
		}
	}
	/**
	 * 即是QC2组长又是QC2组员
	 * */
	public static boolean qc2TeamAndMumber(Set<String> roleNameList){
		synchronized(roleNameList) {
			if(roleNameList==null||roleNameList.size()==0){
				return false ;
			}
			return  roleNameList.contains(ConstantVar.witness_member_qc2) &&  roleNameList.contains(ConstantVar.witness_team_qc2) ;
		}
		
	}
	/**
	 * noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
	 * 根据当前登录账号的角色情况，及是否福清qc1ReplaceQc2返回。
	 * 通用：直接返回QC２
	 * 福清qc1ReplaceQc2:返回qc1
	 * @param roleNameList
	 * @return
	 */
	public static String noticePointQc2(Set<String> roleNameList){
		synchronized(roleNameList) {
			String ret = "QC2";
			if(roleNameList==null||roleNameList.size()==0){
				return ret ;
			}
			//noticePoint　原本写死QC2，因福清qc1ReplaceQc2改为参数
			if (roleNameList.contains(ConstantVar.witness_member_qc2)) {
				return ret ;
			}else if(ContextPath.qc1ReplaceQc2 && roleNameList.contains(ConstantVar.witness_member_qc1)){
				ret = "QC1";
			}
			return ret ;
		}
	}
}
