package com.easycms.common.logic.context.constant;
/**
 * Enpower接口使用 常量值
 * @author ul-webdev
 *
 */
public class EnpConstant {
/**	程序引用　　项目代号（默认值“K2K3”） 因为要接入福清，在启动后有可能被修改为FQ*/
	public static String PROJ_CODE="K2K3";//默认K项　K2K3
	public final static String PROJ_CODE_K="K2K3";
	public final static String PROJ_CODE_FQ="FQ";
/**程序引用　　COMP_CODE：项目代码（K项默认值：“050812”)*/
	public static String COMP_CODE="050812";//默认K项　050812
	public final static String COMP_CODE_K="050812";
	public final static String COMP_CODE_FQ="050805";
	public final static String PROJ_CODE_HY="HY34"; 
	public final static String COMP_CODE_HY="050804";

	/**项目代号（霞浦） */
	public final static String PROJ_CODE_XP="XP"; 
	/**项目代码（霞浦) */
	public final static String COMP_CODE_XP="050814";

	/**项目代号（三门） */
	public final static String PROJ_CODE_SM="SM"; 
	/**项目代码（三门) */
	public final static String COMP_CODE_SM="050803";
	/***
	 * 171206的临时文档中，对物项入库的查询有些增加，最后放在查询中的．
	 */
//	public final static String PROJ_CODE_AND_COMP_CODE="and proj_code='K2K3' and comp_code='050812'";
	

	/**
	 * 安全文明施工　一级安全编码
	 */
	public final static String HSE_STAND1="一级编码";
	public final static String HSE_STAND2="二级编码";
	public final static String HSE_STAND3="三级编码";
	
}
