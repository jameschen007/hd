package com.easycms.common.util;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;

import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class FileUtils {
	
	public static Logger logger = Logger.getLogger(FileUtils.class);
	public static final String questionFilePath = "problem_files";
	public static final String rollingPlanFilePath = "rollingPlan_files";
	public static final String errorEnpowerImg = "errorEnpowerImg";
    static BASE64Encoder encoder = new sun.misc.BASE64Encoder(); 
    static BASE64Decoder decoder = new sun.misc.BASE64Decoder();  

	/**
	 * 读取某个文件夹下的所有文件
	 */
	public static List<String> readFileNames(String folderPath) {
		List<String> files = new ArrayList<String>();

		try {
			File file = new File(folderPath);
			if (!file.isDirectory()) {
				files.add(file.getAbsolutePath());
			} else if (file.isDirectory()) {
				String[] filelist = file.list();
				for (int i = 0; i < filelist.length; i++) {
					File readfile = new File(folderPath + "\\" + filelist[i]);
					if (!readfile.isDirectory()) {
						files.add(readfile.getAbsolutePath());
					} else if (readfile.isDirectory()) {
						readFileNames(folderPath + "\\" + filelist[i]);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return files;
	}

	public static String readFileContent(String filePath) {
		String output = "";
		File file = new File(filePath);
		if (file.exists()) {
			if (file.isFile()) {
				try {
					BufferedReader input = new BufferedReader(new FileReader(file));
					StringBuffer buffer = new StringBuffer();
					String text;
					while ((text = input.readLine()) != null)
						buffer.append(text);
					output = buffer.toString();
				} catch (IOException ioException) {
					System.err.println("File Error!");
				}
			} else if (file.isDirectory()) {
				String[] dir = file.list();
				output += "Directory contents:/n";

				for (int i = 0; i < dir.length; i++) {
					output += dir[i] + "/n";
				}
			}
		} else {
			System.err.println("Does not exist!");
		}
		return output;
	}
	
	public static boolean remove(String path) {
		if (CommonUtility.isNonEmpty(path)) {
			File file = new File(path);
			if (file.exists()) {
				return file.delete();
			}
		}
		return false;
	}
    /**
     * 将图片转换成二进制
     * @param filePaht 文件路径
     * @param fileType 文件后缀名称
     * @return
     */ 
    public static byte[] image2byte(String filePath) {
    	
    	  //图片到byte数组
    	    byte[] data = null;
    	    FileImageInputStream input = null;
    	    try {
    	      input = new FileImageInputStream(new File(filePath));
    	      ByteArrayOutputStream output = new ByteArrayOutputStream();
    	      byte[] buf = new byte[1024];
    	      int numBytesRead = 0;
    	      while ((numBytesRead = input.read(buf)) != -1) {
    	      output.write(buf, 0, numBytesRead);
    	      }
    	      data = output.toByteArray();
    	      output.close();
    	      input.close();
    	    }
    	    catch (FileNotFoundException ex1) {
    	      ex1.printStackTrace();
    	    }
    	    catch (IOException ex1) {
    	      ex1.printStackTrace();
    	    }
    	    return data;
    }
    

    public static String image2byteStr(String filePath) {
    	
    	String out = null;
    	  //图片到byte数组
//    	    byte[] data = null;
    	    InputStream input = null;
    	    try {
    	      input = new FileInputStream(new File(filePath));
    	      BufferedInputStream  binput = new BufferedInputStream(input); 
    	      
              BufferedImage bm = ImageIO.read(binput);     
              ByteArrayOutputStream bos = new ByteArrayOutputStream();   
              ImageIO.write(bm, filePath.substring(filePath.lastIndexOf(".")+1), bos);     
              bos.flush();     
              byte[] data = bos.toByteArray();     
      
              
//              out = byte2hex(data);     
              

              StringBuffer result = new StringBuffer();  
              for(int i = 0;i<data.length;i++)  
              {  
                  result.append(Long.toString(data[i] & 0xff, 10)+" ");  
              }  


              out = result.toString().substring(0, result.length()-1);  
              
              bos.close();
    	      input.close();
    	    }
    	    catch (FileNotFoundException ex1) {
    	      ex1.printStackTrace();
    	    }
    	    catch (IOException ex1) {
    	      ex1.printStackTrace();
    	    }
    	    return out;
    }


    public static String image2byteString(String filePath) {
    	return new String(image2byte(filePath));
    }
    
    
    
    /**   
     * 反格式化byte   
     *    
     * @param s   
     * @return   
     */    
    public static byte[] hex2byte(String s) {     
        byte[] src = s.toLowerCase().getBytes();     
        byte[] ret = new byte[src.length / 2];     
        for (int i = 0; i < src.length; i += 2) {     
            byte hi = src[i];     
            byte low = src[i + 1];     
            hi = (byte) ((hi >= 'a' && hi <= 'f') ? 0x0a + (hi - 'a')     
                    : hi - '0');     
            low = (byte) ((low >= 'a' && low <= 'f') ? 0x0a + (low - 'a')     
                    : low - '0');     
            ret[i / 2] = (byte) (hi << 4 | low);     
        }     
        return ret;     
    }     
        
    /**   
     * 格式化byte   
     *    
     * @param b   
     * @return   
     */    
    public static String byte2hex(byte[] b) {     
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',     
                'B', 'C', 'D', 'E', 'F' };     
        char[] out = new char[b.length * 2];     
        for (int i = 0; i < b.length; i++) {     
            byte c = b[i];     
            out[i * 2] = Digit[(c >>> 4) & 0X0F];     
            out[i * 2 + 1] = Digit[c & 0X0F];     
        }     
        
        return new String(out);     
    }      //byte数组到图片
    public static void byte2image(byte[] data,String path){
        if(data.length<3||path.equals("")) return;
        try{
        FileImageOutputStream imageOutput = new FileImageOutputStream(new File(path));
        imageOutput.write(data, 0, data.length);
        imageOutput.close();
        System.out.println("Make Picture success,Please find image in " + path);
        } catch(Exception ex) {
          System.out.println("Exception: " + ex);
          ex.printStackTrace();
        }
      }
      //byte数组到16进制字符串
      public String byte2string(byte[] data){
        if(data==null||data.length<=1) return "0x";
        if(data.length>200000) return "0x";
        StringBuffer sb = new StringBuffer();
        int buf[] = new int[data.length];
        //byte数组转化成十进制
        for(int k=0;k<data.length;k++){
          buf[k] = data[k]<0?(data[k]+256):(data[k]);
        }
        //十进制转化成十六进制
        for(int k=0;k<buf.length;k++){
          if(buf[k]<16) sb.append("0"+Integer.toHexString(buf[k]));
          else sb.append(Integer.toHexString(buf[k]));
        }
        return "0x"+sb.toString().toUpperCase();
      }
    
	public static void main(String[] args) {
		System.out.println(new String(image2byte("D:\\tmp\\UserFiles\\problem_files/_201711291101561511924516456.jpg")));
		byte2image(image2byte("D:\\tmp\\UserFiles\\problem_files/_201711291101561511924516456.jpg"),"D:\\tmp\\UserFiles/wz2");
	}
}
