package com.easycms.common.util;

import com.easycms.common.util.underlying.FileUnderlying;

public class FileUtility {

	private FileUnderlying fileUnderlying = FileUnderlying.getInstance(this);;

    @SuppressWarnings("unused")
	public boolean createFile(String filepath,String filecontent){
    	return fileUnderlying.createFile(filepath, filecontent);
    }
	
}
