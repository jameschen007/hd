package com.easycms.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtility {

	/**
	 * 格式yyyy/MM/dd HH:mm:ss
	 */
	public static SimpleDateFormat sdfyMdHms = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	/**
	 * 格式yyyy-MM-dd HH:mm:ss
	 */
	public static SimpleDateFormat sdf_yMdHms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * 格式yyyyMM
	 */
	public static SimpleDateFormat sdfyyyyMM = new SimpleDateFormat("yyyyMM");
	public static final int[] getDateInt(Date dateObj) {
		int[] date = new int[3];

		if (dateObj != null) {
			GregorianCalendar calender = new GregorianCalendar();
			calender.setTime(dateObj);

			date[0] = calender.get(5);
			date[1] = (calender.get(2) + 1);
			date[2] = calender.get(1);
		}
		return date;
	}

	public static final int getDayOfMonth(Date dateObj) {
		if (dateObj != null) {
			GregorianCalendar calender = new GregorianCalendar();
			calender.setTime(dateObj);

			return calender.get(5);
		}
		return -1;
	}

	public static final int getDateYear(Date dateObj) {
		if (dateObj != null) {
			GregorianCalendar calender = new GregorianCalendar();
			calender.setTime(dateObj);

			return calender.get(1);
		}
		return -1;
	}

	public static final int getDateMonth(Date dateObj) {
		if (dateObj != null) {
			GregorianCalendar calender = new GregorianCalendar();
			calender.setTime(dateObj);

			return calender.get(2);
		}
		return -1;
	}

	public static final int getLastDayOfMonth(int month, int year) {
		Date date = generateDate(1, month, year);

		if (date != null) {
			date = addDate(addDate(date, 0, 1, 0), -1, 0, 0);

			GregorianCalendar calender = new GregorianCalendar();
			calender.setTime(date);

			return calender.get(5);
		}
		return -1;
	}

	public static final Date addDate(Date date, int day, int month, int year) {
		if (date == null) {
			return null;
		}

		GregorianCalendar calender = new GregorianCalendar();
		calender.setTime(date);
		calender.add(5, day);
		calender.add(2, month);
		calender.add(1, year);
		return calender.getTime();
	}

	public static final Date generateDate(int day, int month, int year) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			dateFormat.setLenient(false);
			return dateFormat
					.parse(Integer.toString(day) + "-" + Integer.toString(month) + "-" + Integer.toString(year));
		} catch (ParseException pException) {
		}
		return null;
	}

	public static final Date generateDateTime(int day, int month, int year, String time) {
		try {
			if (time == "") {
				time = "00:00:00";
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			dateFormat.setLenient(false);
			return dateFormat.parse(
					Integer.toString(day) + "-" + Integer.toString(month) + "-" + Integer.toString(year) + " " + time);
		} catch (ParseException pException) {
			pException.printStackTrace();
		}
		return null;
	}

	public static final boolean isValidDate(int day, int month, int year) {
		return generateDate(day, month, year) != null;
	}

	public static final boolean isValidDateTime(int day, int month, int year, int hour, int minute, int second) {
		return generateDateTime(day, month, year, hour + ":" + minute + ":" + second) != null;
	}

	public static final String getDisplayDate(int day, int month, int year) {
		return getDisplayDate(generateDate(day, month, year));
	}

	public static final String getDisplayDateTime(int day, int month, int year, String time) {
		if (time == "")
			time = "00:00:00";
		return getDisplayDateTime(generateDateTime(day, month, year, time));
	}

	public static final String getDisplayDateTime(Date date) {
		return getDateString(date, "dd-MM-yyyy HH:mm:ss");
	}

	public static final String getDisplayDate(Date date) {
		return getDateString(date, "d MMM yyyy");
	}

	public static final String getNumericDate(Date date) {
		return getDateString(date, "dd-MM-yyyy");
	}

	public static final String getMonthYear(Date date) {
		return getDateString(date, "MMMM yyyy");
	}

	public static final String roundToHourMinFromSec(long seconds) {
		long minutes = seconds / 60L;

		if (minutes * 60L < seconds) {
			minutes += 1L;
		}
		long hour = minutes / 60L;
		minutes -= hour * 60L;

		return hour + ":" + (minutes < 10L ? "0" : "") + minutes;
	}

	public static final String getTime(Date date) {
		return getDateString(date, "HH:mm:ss");
	}

	public static final String getDateString(Date date, String format) {
		if ((date == null) || (format == null)) {
			return null;
		}
		return new SimpleDateFormat(format).format(date);
	}

	public static final Date getDateObjFromSQL(String dateStr) {
		GregorianCalendar calender = getDateFromSQL(dateStr);

		if (calender == null) {
			return null;
		}
		return calender.getTime();
	}

	public static final GregorianCalendar getDateFromSQL(String dateStr) {
		if (dateStr != null) {
			try {
				GregorianCalendar calender = new GregorianCalendar();

				calender.setTime(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(dateStr));

				return calender;
			} catch (ParseException pException) {
				pException.printStackTrace();
			}
		}
		return null;
	}

	public static Date getFormattedDate(String date, String format) {
		GregorianCalendar calender = new GregorianCalendar();
		Date myDate = null;
		try {
			calender.setTime(new SimpleDateFormat(format).parse(date));
			myDate = calender.getTime();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return myDate;
	}
	


	public static Date getDateByYmdhms(String date) {
		GregorianCalendar calender = new GregorianCalendar();
		Date myDate = null;
		try {
			

			if(date!=null && date.indexOf("-")!=-1){
				date = date.replaceAll("-", "/");
			}
			
			calender.setTime(sdfyMdHms.parse(date));
			myDate = calender.getTime();
		} catch (ParseException ex) {
			ex.printStackTrace();
			throw new RuntimeException("时间转换为Date出错1："+date);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("时间转换为Date出错2："+date);
		}

		return myDate;
	}
	
	

	public static Date dateMinus(Date date, long millis) {
		return new Date(date.getTime() - millis);
	}

	public static Date now(){
		return Calendar.getInstance().getTime();
	}
	/**
	 * 获得当前年月 yyyyMM
	 * @return
	 */
	public static String getYearMonth(){
		return sdfyyyyMM.format(now());
	}
	
	public static void main(String[] args) {
		System.out.println(getYearMonth());
	}

	  
    public static String sumTime(long ms) {
        int ss = 1000;
        long mi = ss * 60;
        long hh = mi * 60;
        long dd = hh * 24;
  
        long day = ms / dd;
        long hour = (ms - day * dd) / hh;
        long minute = (ms - day * dd - hour * hh) / mi;
        long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        long milliSecond = ms - day * dd - hour * hh - minute * mi - second
                * ss;
  
        String strDay = day < 10 ? "0" + day + "天" : "" + day + "天";
        String strHour = hour < 10 ? "0" + hour + "小时" : "" + hour + "小时";
        String strMinute = minute < 10 ? "0" + minute + "分" : "" + minute + "分";
        String strSecond = second < 10 ? "0" + second + "秒" : "" + second + "秒";
        String strMilliSecond = milliSecond < 10 ? "0" + milliSecond : ""
                + milliSecond;
        strMilliSecond = milliSecond < 100 ? "0" + strMilliSecond + "毫秒" : ""
                + strMilliSecond + " 毫秒";
        return strDay + " " + strHour + ":" + strMinute + ":" + strSecond + " "
                + strMilliSecond;
  
    }
    
}