package com.easycms.common.util;

public class StringUtility {
/**
 * 将纯包含数字（不含小数）的字符串转换
 * @param p
 * @return
 */
	public static Integer parseInt(String p){
		if(p==null){
			return null;
		}else if(p.matches("\\d+")){
			return Integer.parseInt(p);
		}
		return null;
	}
	public static void main(String[] args) {
		StringUtility t = new StringUtility();

		System.out.println(t.parseInt("a"));
		System.out.println(t.parseInt("123"));
		System.out.println(t.parseInt("13.3"));
	}

}
