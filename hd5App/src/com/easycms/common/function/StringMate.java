package com.easycms.common.function;


public interface StringMate {

	/**
	 *质量导出时，上报图片　输出到１３列，整改图片输出到１４列 
	 * */
	public short mateShort(String type);

    /**
     * 因为质量模块的导出图片，１３列是导出上报图片，１４列导出整改图片使用
     */
    static String before= "before";
    static String after= "after";
	default short matingShort(String type){
		if(before.equals(type)){
			return (short)13;
		}else if(after.equals(type)){
			return (short)14;
		}
		return 0;
	}

}
