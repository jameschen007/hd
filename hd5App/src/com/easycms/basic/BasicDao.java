package com.easycms.basic;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.QueryHints;

public interface BasicDao<T> {
	
	/**
	 * 查找所有
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<T> findAll();
	
//	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<T> findAll(Specification<T> sepc);
	
	/**
	 * 查找分页
	 * @param pageable
	 * @return
	 */
//	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Page<T> findAll(Specification<T> sepc,Pageable pageable);
	

	/**
	 * 根据用户ID查找
	 * 
	 * @param id
	 * @return
	 */
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	T findById(Integer id);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	T findById(String id);

	/**
	 * 保存用户
	 * 
	 * @param user
	 * @return
	 */
	@Modifying(clearAutomatically = true)
	T save(T t);
	/**
	 * 保存用户
	 * 
	 * @param user
	 * @return
	 */
	@Modifying(clearAutomatically = true)
	T saveAndFlush(T t);

//	/**
//	 * 删除用户
//	 * 
//	 * @param id
//	 */
	int deleteById(Integer id);
	
	int deleteById(String id);
	/**
	 * 删除所有
	 * @return
	 */
//	@Modifying
//	@Query("DELETE FROM Department")
//	int deleteAll();
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
//	@Modifying
//	@Query("DELETE FROM User u WHERE u.id  IN (?1)")
//	int deleteByIds(Integer[] ids);

	/**
	 * 统计
	 * @return
	 */
	Integer count();
}
