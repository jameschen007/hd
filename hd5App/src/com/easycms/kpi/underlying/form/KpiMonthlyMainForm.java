package com.easycms.kpi.underlying.form;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class KpiMonthlyMainForm extends BasicForm implements Serializable{
    private static final long serialVersionUID = 5778232707296977882L;

    private String searchValue;

    /** 默认ID */
    private Integer id;
    /** 被考核人id */
    private User owner;
    /** 月份 */
    private String month;
    /** 考核周期 */
    private String kpiCycle;
    /** 自评分 */
    private Integer selfScoring;
    /** 干系人评分 */
    private Integer stakeholderScoring;
    /** 总经理评分 */
    private Integer generalManagerScoring;
    /** 直接上级评分 */
    private Integer directSuperiorScoring;
    /** 直接上级意见 */
    private String directSuperiorOpinion;
    /** 综合得分 */
    private Integer compositeScoring;
    /** 二次处理得分 */
    private Integer twoProcessingScoring;
    /** 考核系数 */
    private BigDecimal kpiCoefficient;
    /** 考核状态 */
    private String kpiStatus;
    /** 当前状态 */
    private String currentStatus;
    /** 类型 */
    private String kpiType;
    /** 备注 */
    private String mark;
    /** 删除状态 */
    private boolean isDel;
    /** 创建时间 */
    private Date createTime;


    /**获取  默认ID */
    public Integer getId() {
        return id;
    }

    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }


    /**获取  被考核人id */
    public User getOwner() {
        return owner;
    }

    /**设置  被考核人id */
    public void setOwner(User owner) {
        this.owner = owner;
    }


    /**获取  月份 */
    public String getMonth() {
        return month;
    }

    /**设置  月份 */
    public void setMonth(String month) {
        this.month = month;
    }


    /**获取  考核周期 */
    public String getKpiCycle() {
        return kpiCycle;
    }

    /**设置  考核周期 */
    public void setKpiCycle(String kpiCycle) {
        this.kpiCycle = kpiCycle;
    }


    /**获取  自评分 */
    public Integer getSelfScoring() {
        return selfScoring;
    }

    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }


    /**获取  干系人评分 */
    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }

    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }


    /**获取  总经理评分 */
    public Integer getGeneralManagerScoring() {
        return generalManagerScoring;
    }

    /**设置  总经理评分 */
    public void setGeneralManagerScoring(Integer generalManagerScoring) {
        this.generalManagerScoring = generalManagerScoring;
    }


    /**获取  直接上级评分 */
    public Integer getDirectSuperiorScoring() {
        return directSuperiorScoring;
    }

    /**设置  直接上级评分 */
    public void setDirectSuperiorScoring(Integer directSuperiorScoring) {
        this.directSuperiorScoring = directSuperiorScoring;
    }


    /**获取  直接上级意见 */
    public String getDirectSuperiorOpinion() {
        return directSuperiorOpinion;
    }

    /**设置  直接上级意见 */
    public void setDirectSuperiorOpinion(String directSuperiorOpinion) {
        this.directSuperiorOpinion = directSuperiorOpinion;
    }


    /**获取  综合得分 */
    public Integer getCompositeScoring() {
        return compositeScoring;
    }

    /**设置  综合得分 */
    public void setCompositeScoring(Integer compositeScoring) {
        this.compositeScoring = compositeScoring;
    }


    /**获取  二次处理得分 */
    public Integer getTwoProcessingScoring() {
        return twoProcessingScoring;
    }

    /**设置  二次处理得分 */
    public void setTwoProcessingScoring(Integer twoProcessingScoring) {
        this.twoProcessingScoring = twoProcessingScoring;
    }


    /**获取  考核系数 */
    public BigDecimal getKpiCoefficient() {
        return kpiCoefficient;
    }

    /**设置  考核系数 */
    public void setKpiCoefficient(BigDecimal kpiCoefficient) {
        this.kpiCoefficient = kpiCoefficient;
    }


    /**获取  考核状态 */
    public String getKpiStatus() {
        return kpiStatus;
    }

    /**设置  考核状态 */
    public void setKpiStatus(String kpiStatus) {
        this.kpiStatus = kpiStatus;
    }


    /**获取  当前状态 */
    public String getCurrentStatus() {
        return currentStatus;
    }

    /**设置  当前状态 */
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }


    /**获取  类型 */
    public String getKpiType() {
        return kpiType;
    }

    /**设置  类型 */
    public void setKpiType(String kpiType) {
        this.kpiType = kpiType;
    }


    /**获取  备注 */
    public String getMark() {
        return mark;
    }

    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }


    /**获取  删除状态 */
    public boolean getIsDel() {
        return isDel;
    }

    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }


    /**获取  创建时间 */
    public Date getCreateTime() {
        return createTime;
    }

    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchValue() {
        return searchValue;
    }
}

