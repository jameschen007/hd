package com.easycms.kpi.underlying.domain;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.easycms.core.form.NoUserIdBasicForm;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_summary_sheet")
//@Cacheable
public class KpiSummarySheet extends NoUserIdBasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;

    /** 用户id */
    @Column(name="user_id")
    private Integer userId;
    /** 月份 */
    @Column(name="month")
    private String month;
    /** 部门 */
    @Column(name="department_name")
    private String departmentName;
    /** 姓名 */
    @Column(name="real_name")
    private String realName;
    /** 自评分 */
    @Column(name="self_scoring")
    private Integer selfScoring;
    /** 干系人评分 */
    @Column(name="stakeholder_scoring")
    private Integer stakeholderScoring;
    /** 直接上级评分 */
    @Column(name="direct_superior_scoring")
    private Integer directSuperiorScoring;
    /** 总经理评分 */
    @Column(name="general_manager_scoring")
    private Integer generalManagerScoring;
    /** 综合得分 */
    @Column(name="composite_scoring")
    private BigDecimal compositeScoring;
    /** 删除状态 */
    @Column(name="is_del")
    private boolean isDel;
    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;
    /** 主表id */
    @Column(name="monthly_main_id")
    private Integer monthlyMainId;

    public Integer getUserId() {
        return userId;
    }
    /**设置  用户id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMonth() {
        return month;
    }
    /**设置  月份 */
    public void setMonth(String month) {
        this.month = month;
    }

    public String getDepartmentName() {
        return departmentName;
    }
    /**设置  部门 */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getRealName() {
        return realName;
    }
    /**设置  姓名 */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getSelfScoring() {
        return selfScoring;
    }
    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }

    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }
    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }

    public Integer getDirectSuperiorScoring() {
        return directSuperiorScoring;
    }
    /**设置  直接上级评分 */
    public void setDirectSuperiorScoring(Integer directSuperiorScoring) {
        this.directSuperiorScoring = directSuperiorScoring;
    }

    public Integer getGeneralManagerScoring() {
        return generalManagerScoring;
    }
    /**设置  总经理评分 */
    public void setGeneralManagerScoring(Integer generalManagerScoring) {
        this.generalManagerScoring = generalManagerScoring;
    }

    public BigDecimal getCompositeScoring() {
        return compositeScoring;
    }
    /**设置  综合得分 */
    public void setCompositeScoring(BigDecimal compositeScoring) {
        this.compositeScoring = compositeScoring;
    }

    public boolean getIsDel() {
        return isDel;
    }
    /**设置  删除状态 */
    public void setIsDel(boolean isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }
    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDel() {
        return isDel;
    }

    public void setDel(boolean del) {
        isDel = del;
    }
	public Integer getMonthlyMainId() {
		return monthlyMainId;
	}
	public void setMonthlyMainId(Integer monthlyMainId) {
		this.monthlyMainId = monthlyMainId;
	}
    
    
}
