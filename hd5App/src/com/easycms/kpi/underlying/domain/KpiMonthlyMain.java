package com.easycms.kpi.underlying.domain;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_monthly_main")
//@Cacheable
public class KpiMonthlyMain extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;
    /** 被考核人id */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private User owner;
    /** 月份 */
    @Column(name="month")
    private String month;
    /** 考核周期 */
    @Column(name="kpi_cycle")
    private String kpiCycle;
    @Transient
    private String cycleStart;
    @Transient
    private String cycleEnd;
    /** 自评分 */
    @Column(name="self_scoring")
    private Integer selfScoring;
    /** 干系人评分 */
    @Column(name="stakeholder_scoring")
    private Integer stakeholderScoring;
    /** 总经理评分 */
    @Column(name="general_manager_scoring")
    private Integer generalManagerScoring;
    /** 直接上级评分 */
    @Column(name="direct_superior_scoring")
    private Integer directSuperiorScoring;
    /** 直接上级意见 */
    @Column(name="direct_superior_opinion")
    private String directSuperiorOpinion;
    /** 综合得分 */
    @Column(name="composite_scoring")
    private BigDecimal compositeScoring;
    /** 二次处理得分 */
    @Column(name="two_processing_scoring")
    private Integer twoProcessingScoring;
    /** 考核系数 */
    @Column(name="kpi_coefficient")
    private BigDecimal kpiCoefficient;
    /** 考核状态 */
    @Column(name="kpi_status")
    private String kpiStatus;
    /** 当前状态 */
    @Column(name="current_status")
    private String currentStatus;
    /** 类型 */
    @Column(name="kpi_type")
    private String kpiType;
    /** 备注 */
    @Column(name="mark")
    private String mark;
    /** 删除状态 */
    @Column(name="is_del")
    private boolean isDel;
    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;

    public Integer getId() {
        return id;
    }
    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }
    /**设置  被考核人id */
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getMonth() {
        return month;
    }
    /**设置  月份 */
    public void setMonth(String month) {
        this.month = month;
    }

    public String getKpiCycle() {
        return kpiCycle;
    }
    /**设置  考核周期 */
    public void setKpiCycle(String kpiCycle) {
        this.kpiCycle = kpiCycle;
    }

    public Integer getSelfScoring() {
        return selfScoring;
    }
    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }

    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }
    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }

    public Integer getGeneralManagerScoring() {
        return generalManagerScoring;
    }
    /**设置  总经理评分 */
    public void setGeneralManagerScoring(Integer generalManagerScoring) {
        this.generalManagerScoring = generalManagerScoring;
    }

    public Integer getDirectSuperiorScoring() {
        return directSuperiorScoring;
    }
    /**设置  直接上级评分 */
    public void setDirectSuperiorScoring(Integer directSuperiorScoring) {
        this.directSuperiorScoring = directSuperiorScoring;
    }

    public String getDirectSuperiorOpinion() {
        return directSuperiorOpinion;
    }
    /**设置  直接上级意见 */
    public void setDirectSuperiorOpinion(String directSuperiorOpinion) {
        this.directSuperiorOpinion = directSuperiorOpinion;
    }

    public BigDecimal getCompositeScoring() {
        return compositeScoring;
    }
    /**设置  综合得分 */
    public void setCompositeScoring(BigDecimal compositeScoring) {
        this.compositeScoring = compositeScoring;
    }

    public Integer getTwoProcessingScoring() {
        return twoProcessingScoring;
    }
    /**设置  二次处理得分 */
    public void setTwoProcessingScoring(Integer twoProcessingScoring) {
        this.twoProcessingScoring = twoProcessingScoring;
    }

    public BigDecimal getKpiCoefficient() {
        return kpiCoefficient;
    }
    /**设置  考核系数 */
    public void setKpiCoefficient(BigDecimal kpiCoefficient) {
        this.kpiCoefficient = kpiCoefficient;
    }

    public String getKpiStatus() {
        return kpiStatus;
    }
    /**设置  考核状态 */
    public void setKpiStatus(String kpiStatus) {
        this.kpiStatus = kpiStatus;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }
    /**设置  当前状态 */
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getKpiType() {
        return kpiType;
    }
    /**设置  类型 */
    public void setKpiType(String kpiType) {
        this.kpiType = kpiType;
    }

    public String getMark() {
        return mark;
    }
    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }

    public boolean getDel() {
        return isDel;
    }
    /**设置  删除状态 */
    public void setDel(boolean isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }
    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public String getCycleStart() {
		return cycleStart;
	}
	public void setCycleStart(String cycleStart) {
		this.cycleStart = cycleStart;
	}
	public String getCycleEnd() {
		return cycleEnd;
	}
	public void setCycleEnd(String cycleEnd) {
		this.cycleEnd = cycleEnd;
	}

}