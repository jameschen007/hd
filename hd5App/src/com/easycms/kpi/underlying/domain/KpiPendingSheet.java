package com.easycms.kpi.underlying.domain;
import com.easycms.core.form.BasicForm;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.NotEmpty;
import java.math.BigDecimal;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
/**
 * <b>项目名称：</b><br>
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>绩效待办列表	 <br>
 * <b>创建时间：</b><br>
 * <b>@Copyright:</b>2019-爱特联
 */
public class KpiPendingSheet extends BasicForm {
	/** 默认ID */
	private String id;
	/** 部门名称 */
	private String departmentName;
	/** 真是姓名 */
	private String realName;
	/** 月份 */
	private String month;
	/** 待领导审核 */
	private String parentok1;
	/** 待自评分 */
	private String selfscore2;
	/** 待领导评分 */
	private String parentscore3;
	/** 待干系人评分 */
	private String stakeholderscore3;
	/** 待领导评分 */
	private String parentscore4;
	/** 待部门经理评分 */
	private String departmentmanagerscore4;
	/** 待分管领导评分 */
	private String chargeleaderscore4;
	/** 待总经理评分 */
	private String generalscore5;
	/** 待总经理二次评分 */
	private String generalscore26;
	/** 默认ID */
	public String getId() {
		return id;
	}
	/** 默认ID */
	public void setId(String id) {
		this.id = id;
	}
	/** 部门名称 */
	public String getDepartmentName() {
		return departmentName;
	}
	/** 部门名称 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	/** 真是姓名 */
	public String getRealName() {
		return realName;
	}
	/** 真是姓名 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/** 月份 */
	public String getMonth() {
		return month;
	}
	/** 月份 */
	public void setMonth(String month) {
		this.month = month;
	}
	/** 待领导审核 */
	public String getParentok1() {
		return parentok1;
	}
	/** 待领导审核 */
	public void setParentok1(String parentok1) {
		this.parentok1 = parentok1;
	}
	/** 待自评分 */
	public String getSelfscore2() {
		return selfscore2;
	}
	/** 待自评分 */
	public void setSelfscore2(String selfscore2) {
		this.selfscore2 = selfscore2;
	}
	/** 待领导评分 */
	public String getParentscore3() {
		return parentscore3;
	}
	/** 待领导评分 */
	public void setParentscore3(String parentscore3) {
		this.parentscore3 = parentscore3;
	}
	/** 待干系人评分 */
	public String getStakeholderscore3() {
		return stakeholderscore3;
	}
	/** 待干系人评分 */
	public void setStakeholderscore3(String stakeholderscore3) {
		this.stakeholderscore3 = stakeholderscore3;
	}
	/** 待领导评分 */
	public String getParentscore4() {
		return parentscore4;
	}
	/** 待领导评分 */
	public void setParentscore4(String parentscore4) {
		this.parentscore4 = parentscore4;
	}
	/** 待部门经理评分 */
	public String getDepartmentmanagerscore4() {
		return departmentmanagerscore4;
	}
	/** 待部门经理评分 */
	public void setDepartmentmanagerscore4(String departmentmanagerscore4) {
		this.departmentmanagerscore4 = departmentmanagerscore4;
	}
	/** 待分管领导评分 */
	public String getChargeleaderscore4() {
		return chargeleaderscore4;
	}
	/** 待分管领导评分 */
	public void setChargeleaderscore4(String chargeleaderscore4) {
		this.chargeleaderscore4 = chargeleaderscore4;
	}
	/** 待总经理评分 */
	public String getGeneralscore5() {
		return generalscore5;
	}
	/** 待总经理评分 */
	public void setGeneralscore5(String generalscore5) {
		this.generalscore5 = generalscore5;
	}
	/** 待总经理二次评分 */
	public String getGeneralscore26() {
		return generalscore26;
	}
	/** 待总经理二次评分 */
	public void setGeneralscore26(String generalscore26) {
		this.generalscore26 = generalscore26;
	}
}
