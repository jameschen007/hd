package com.easycms.kpi.underlying.domain;

import lombok.Data;

public class KpiLibDetails {
	
	//部门名称
	private String departmentName;
	
	//用户名
	private String userName;
	
	//用户姓名
	private String userRealName;
	
	//指标名称
	private String kpiName;
	
	//指标类型
	private String kpiType;
	
	//干系人
	private String stakeholderName;

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	/**
	 * 获取指标类型
	 * @return
	 */
	public String getKpiType() {
		if("general".equals(kpiType) || kpiType=="general"){
			return "通用指标";
		}else if("purpose".equals(kpiType) || kpiType=="purpose"){
			return "岗位指标";
		}else{
			return kpiType;
		}
	}

	public void setKpiType(String kpiType) {
		this.kpiType = kpiType;
	}

	public String getStakeholderName() {
		return stakeholderName;
	}

	public void setStakeholderName(String stakeholderName) {
		this.stakeholderName = stakeholderName;
	}


}
