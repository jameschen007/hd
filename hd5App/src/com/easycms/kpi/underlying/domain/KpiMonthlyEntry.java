package com.easycms.kpi.underlying.domain;
import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.User;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:21<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Entity
@Table(name="kpi_monthly_entry")
//@Cacheable
public class KpiMonthlyEntry extends BasicForm {

    private static final long serialVersionUID = 2660217706536201507L;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    private Integer id;
    /** 被考核人 */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
    private User owner;
    /** 指标库id */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.kpi.underlying.domain.KpiLib.class,fetch=FetchType.LAZY)
	@JoinColumn(name="lib_id")
    private KpiLib lib;
    /** 权重 */
    @Column(name="weight")
    private Integer weight;
    /** 完成时间 */
    @Column(name="finish_time")
    private Date finishTime;
    /** 自评分 */
    @Column(name="self_scoring")
    private Integer selfScoring;
    /** 干系人 */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class,fetch=FetchType.LAZY)
	@JoinColumn(name="stakeholder_id")
    private User stakeholder;
    /** 干系人评分 */
    @Column(name="stakeholder_scoring")
    private Integer stakeholderScoring;
    /** 状态 */
    @Column(name="status")
    private String status;
    /** 备注 */
    @Column(name="mark")
    private String mark;
    /** 自评理由 */
    @Column(name="self_reason")
    private String selfReason;
    /** 干系人打分打分理由 */
    @Column(name="stakeholder_scoring_reason")
    private String stakeholderScoringReason;
    /** 一票否决理由 */
    @Column(name="one_ticket_veto_reason")
    private String oneTicketVetoReason;
    /** 删除状态 */
    @Column(name="is_del")
    private boolean isDel;
    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;
    /** 主表id */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.kpi.underlying.domain.KpiMonthlyMain.class,fetch=FetchType.LAZY)
	@JoinColumn(name="monthly_main_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private KpiMonthlyMain monthlyMain;

    public Integer getId() {
        return id;
    }
    /**设置  默认ID */
    public void setId(Integer id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }
    /**设置  被考核人 */
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public KpiLib getLib() {
        return lib;
    }
    /**设置  指标库id */
    public void setLib(KpiLib lib) {
        this.lib = lib;
    }

    public Integer getWeight() {
        return weight;
    }
    /**设置  权重 */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Date getFinishTime() {
        return finishTime;
    }
    /**设置  完成时间 */
    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getSelfScoring() {
        return selfScoring;
    }
    /**设置  自评分 */
    public void setSelfScoring(Integer selfScoring) {
        this.selfScoring = selfScoring;
    }

    public User getStakeholder() {
        return stakeholder;
    }
    /**设置  干系人 */
    public void setStakeholder(User stakeholder) {
        this.stakeholder = stakeholder;
    }

    public Integer getStakeholderScoring() {
        return stakeholderScoring;
    }
    /**设置  干系人评分 */
    public void setStakeholderScoring(Integer stakeholderScoring) {
        this.stakeholderScoring = stakeholderScoring;
    }

    public String getStatus() {
        return status;
    }
    /**设置  状态 */
    public void setStatus(String status) {
        this.status = status;
    }

    public String getMark() {
        return mark;
    }
    /**设置  备注 */
    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getSelfReason() {
        return selfReason;
    }
    /**设置  自评理由 */
    public void setSelfReason(String selfReason) {
        this.selfReason = selfReason;
    }

    public String getStakeholderScoringReason() {
        return stakeholderScoringReason;
    }
    /**设置  干系人打分打分理由 */
    public void setStakeholderScoringReason(String stakeholderScoringReason) {
        this.stakeholderScoringReason = stakeholderScoringReason;
    }

    public String getOneTicketVetoReason() {
        return oneTicketVetoReason;
    }
    /**设置  一票否决理由 */
    public void setOneTicketVetoReason(String oneTicketVetoReason) {
        this.oneTicketVetoReason = oneTicketVetoReason;
    }

    public boolean getDel() {
        return isDel;
    }
    /**设置  删除状态 */
    public void setDel(boolean isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }
    /**设置  创建时间 */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public KpiMonthlyMain getMonthlyMain() {
        return monthlyMain;
    }
    /**设置  主表id */
    public void setMonthlyMain(KpiMonthlyMain monthlyMainId) {
        this.monthlyMain = monthlyMainId;
    }

}
