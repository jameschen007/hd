package com.easycms.kpi.underlying.directive;

import com.easycms.basic.BaseDirective;
import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiUserBase;
import com.easycms.kpi.underlying.service.KpiUserBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>获取指令-用户绩效基础信息		kpi_user_base<br>
 * <b>创建时间：</b>2017-08-16 下午10:01:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Component
public class KpiUserBaseDirective extends BaseDirective<KpiUserBase> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiUserBaseDirective.class);
    @Autowired
    private KpiUserBaseService kpiUserBaseService;

    @Override
    protected Integer count(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected KpiUserBase field(Map params, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected List<KpiUserBase> list(Map params, String filter, String order, String sort, boolean pageable, Page<KpiUserBase> pager, Map<String, Object> envParams) {
        return null;
    }

    @Override
    protected List<KpiUserBase> tree(Map params, Map<String, Object> envParams) {
        return null;
    }
}