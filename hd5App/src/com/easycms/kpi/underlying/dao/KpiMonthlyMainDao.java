package com.easycms.kpi.underlying.dao;
import com.easycms.basic.BasicDao;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;
import com.easycms.kpi.underlying.domain.KpiUserBase;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>员工月度考核主表		kpi_monthly_main<br>
 * <b>创建时间：</b>2017-08-20 下午05:58:50<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface KpiMonthlyMainDao extends BasicDao<KpiMonthlyMain> ,Repository<KpiMonthlyMain,Integer>{


	
	@Query(value="from KpiMonthlyMain u where u.owner.id=?1 and u.isDel=0 and u.month=?2")
	KpiMonthlyMain findKpiMonthlyMainByUserIdMonth(Integer userId,String month);
	

	@Modifying
	@Query(value="delete from KpiMonthlyMain u where u.id=?1")
	 public int deleteByMainId(Integer mainId);
}
