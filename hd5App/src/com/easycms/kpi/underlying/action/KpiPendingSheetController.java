package com.easycms.kpi.underlying.action;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.StatusEnum;
import com.easycms.hd.api.request.WitnessRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.KpiPendingPageResult;
import com.easycms.hd.api.response.WitnessPageResult;
import com.easycms.hd.api.response.WitnessResult;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;
import com.easycms.kpi.underlying.form.KpiSummarySheetForm;
import com.easycms.kpi.underlying.service.IKpiPendingSheetService;
import com.easycms.kpi.underlying.service.KpiHandleExchangeService;
import com.easycms.kpi.underlying.service.KpiSummarySheetService;
import com.easycms.management.user.domain.User;
import com.wordnik.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>查询绩效待办列表<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Controller
@RequestMapping("/kpi/summary/sheet")
public class KpiPendingSheetController {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(KpiPendingSheetController.class);

    @Autowired
    private KpiSummarySheetService kpiSummarySheetService;
    @Autowired
    private IKpiPendingSheetService kpiPendingSheetService;
    @Autowired
    private KpiHandleExchangeService kpiHandleExchangeService;


    /**
     * 查询绩效待办列表UI
     * @param request
     * @param response
     * @param kpiSummarySheetForm
     * @return
     */
    @RequestMapping(value = "/pendingList", method = RequestMethod.GET)
    public String pendingList(HttpServletRequest request,
                              HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
        logger.debug("==> Show KpiSummarySheet data list.");
        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));

        List<KpiSummarySheet> kpiCurrentSheet =  kpiSummarySheetService.getCurrentSheet();
        request.setAttribute("kpiCurrentSheetList", kpiCurrentSheet);


        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
        return returnString;
    }
    /**
     * 查询绩效待办列表
     * @param request
     * @param response
     * @return
     */
//    @RequestMapping(value = "/pendingList", method = RequestMethod.POST)
//    public String pendingListData(HttpServletRequest request,
//                                  HttpServletResponse response, @ModelAttribute("form") KpiSummarySheetForm kpiSummarySheetForm) {
//        kpiSummarySheetForm.setSearchValue(request.getParameter("search[value]"));
//        logger.debug("==> Show KpiSummarySheet data list.");
//        kpiSummarySheetForm.setFilter(CommonUtility.toJson(kpiSummarySheetForm));
//        logger.debug("[easyuiForm] ==> " + CommonUtility.toJson(kpiSummarySheetForm));
//
//        List<KpiSummarySheet> kpiCurrentSheet =  kpiSummarySheetService.getCurrentSheet();
//        request.setAttribute("kpiCurrentSheetList", kpiCurrentSheet);
//
//
//        String returnString = ForwardUtility
//                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
//        return returnString;
//    }


    @ResponseBody
    @RequestMapping(value = "/pendingList", method = RequestMethod.POST)
    @ApiOperation(value = "查询绩效待办列表", notes = "查询绩效待办列表")
    public JsonResult<KpiPendingPageResult> witness(HttpServletRequest request, HttpServletResponse response,
                                                    @ModelAttribute("requestForm") Map<String, Object> map) {
        //KpiPendingSheet kpiPendingSheet
        JsonResult result = new JsonResult();


        if (map!=null && null  != map.get("pageNumber") && null  != map.get("pageSize") ) {
            Page<KpiPendingSheet> page = new Page<KpiPendingSheet>();

            page.setPageNum((Integer) map.get("pageNumber"));
            page.setPagesize((Integer) map.get("pageSize"));


            Page<KpiPendingSheet> pageBySql = kpiPendingSheetService.findPageBySql(page, map);


//WorkStepWitness
//            loginUser = userService.findUserById(witnessRequestForm.getLoginId());
//            if(witnessRequestForm.getUserId()!=null){
//                user = userService.findUserById(witnessRequestForm.getUserId());
//            }
//
//            WitnessPageResult witnessPageDataResult = witnessApiService.getWitnessPageResult(page, user, witnessRequestForm.getStatus(), witnessRequestForm.getType() , witnessRequestForm.getNoticePointType(), witnessRequestForm.getKeyword(),loginUser,witnessRequestForm.getRoleId());
            result.setResponseResult(pageBySql);
            result.setMessage("成功获取绩效待办列表");
            return result;
        } else {
            logger.debug("分页信息有缺失");
            result.setCode("-1002");
            result.setMessage("分页信息有缺失.");
            return result;
        }
    }

    /**
     * 绩效待办详情UI
     * @param request
     * @param response
     * @param kpiMainId
     * @return
     */
    @RequestMapping(value = "/pendingListUI", method = RequestMethod.GET)
    public String pendingListUI(HttpServletRequest request,
                                HttpServletResponse response, @NotNull String kpiMainId) {
        logger.debug("[kpiMainId] ==> " + kpiMainId);
        request.setAttribute("kpiMainId", kpiMainId);
        String returnString = ForwardUtility
                .forwardAdminView("/kpi/score/list_kpi_currentSheet");
        return returnString;
    }
    /**
     * 绩效待办详情
     * @param request
     * @param response
     * @param kpiMainId
     * @return
     */
    @RequestMapping(value = "/pendingDetail", method = RequestMethod.POST)
    public JsonResult<KpiPendingPageResult> pendingDetail(HttpServletRequest request,
                                                          HttpServletResponse response, @NotNull String kpiMainId) throws Exception {
        KpiPendingSheet byId = kpiPendingSheetService.findById(kpiMainId);
        JsonResult result = new JsonResult();
        result.setResponseResult(byId);
        result.setMessage("成功获取绩效待办详情");
        return result;
    }

    /**
     * 修改待办人
     * @param request
     * @param response
     * @param exchangeId
     * @return
     */
    @RequestMapping(value = "/updateOnePending", method = RequestMethod.POST)
    public void updateOnePending(HttpServletRequest request,HttpServletResponse response, @NotNull String exchangeId,@NotNull Integer handleUserId) {
        boolean update = kpiHandleExchangeService.update(exchangeId, handleUserId);

        HttpUtility.writeToClient(response, String.valueOf(update));
    }

}