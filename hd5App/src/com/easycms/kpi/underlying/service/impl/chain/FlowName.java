package com.easycms.kpi.underlying.service.impl.chain;

public enum FlowName {
	
	selfShowInit("selfShowInit","待提交规划"),//待规划确认　状态　在岐义，修改为“待提交规划”

	parentOk("1.parentOk","待领导审核"),
	selfScore("2.selfScore","待自评分"),
	parentScore("3.parentScore","待领导评分"),
	stakeholderScore("3.stakeholderScore","待干系人评分"),
	parentScore4("4.parentScore","待领导评分"),
	departmentManagerScore("4.departmentManagerScore","待部门经理评分"),
	chargeLeaderScore("4.chargeLeaderScore","待分管领导评分"),
	generalScore("5.generalScore","待总经理评分"),
	generalScore2("6.generalScore2","待总经理二次评分");
	

	String code;
	String msg ;
	FlowName(String code){
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	FlowName(String code,String msg){
		this.code = code;
		this.msg = msg ;
	}
	
	public static void main(String[] args) {
		System.out.println(FlowName.parentOk.getCode());
		System.out.println(FlowName.parentOk.getMsg());
	}
}
