package com.easycms.kpi.underlying.service.impl.sys;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MonthTool {

	
	/**
	 * 当前月加减参数
	 * @param thisMonth
	 * @return
	 */
	public static String getMonthStr(int thisMonth){
        String month = "201709";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, thisMonth);
        month =new SimpleDateFormat("yyyyMM").format(c.getTime());
    	
		return month ;
	}
	
	public static void main(String [] str ){
		MonthTool t = new MonthTool();
        String month = "201709";
        month = t.getMonthStr(0);
    	System.out.println("月:"+month);
	}
}
