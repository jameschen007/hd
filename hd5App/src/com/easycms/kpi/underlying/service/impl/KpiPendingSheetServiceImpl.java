package com.easycms.kpi.underlying.service.impl;

import com.easycms.core.util.Page;
import com.easycms.hd.plan.mybatis.dao.KpiPendingSheetMybatisDao;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;
import com.easycms.kpi.underlying.service.IKpiPendingSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>项目名称：</b><br>
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>绩效待办列表		kpi_pending_sheet<br>
 * <b>创建时间：</b><br>
 * <b>@Copyright:</b>2019-爱特联
 */
@Service
public class KpiPendingSheetServiceImpl implements IKpiPendingSheetService {
    @Autowired
    private KpiPendingSheetMybatisDao kpiPendingSheetMybatisDao;
    @Override
    public KpiPendingSheet findById(String id) throws Exception {

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);

        List<KpiPendingSheet> result = null;
        List<KpiPendingSheet> r = kpiPendingSheetMybatisDao.findPageBySql(map);
        if (r != null && r.size() > 0) {
            return r.get(0);
        }
        return null;
    }

    @Override
    public Page<KpiPendingSheet> findPageBySql(Page<KpiPendingSheet> page,Map<String, Object> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        map.put("pageNumber",(page.getPageNum() - 1) * page.getPagesize());
        map.put("pageSize", page.getPagesize());
        int count = 0;
        List<KpiPendingSheet> result = null;
        result = kpiPendingSheetMybatisDao.findPageBySql(map);
        count = kpiPendingSheetMybatisDao.findPageBySqlCount(map);
        page.execute(count, page.getPageNum(), result);

        return page;
    }
}
