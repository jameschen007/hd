package com.easycms.kpi.underlying.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.JsonFormatTool;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiLibDetails;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KpiLibExcelUtil {
	public static void main(String[] args) throws FileNotFoundException {
		String filePath = "D:\\wangz\\ProjectA\\KPI指标库汇总1tst.xlsx"; 
		List<KpiLibDetails> examPapers = readExcelToObj(filePath);
		System.out.println(JsonFormatTool.formatJson(CommonUtility.toJson(examPapers),"\t"));
	}

	/**
	 * 读取excel数据
	 * 
	 * @param path
	 */
	public static List<KpiLibDetails> readExcelToObj(String filePath) {
		Workbook wb = null;
		try {
			if (StringUtils.isNotBlank(filePath)) {
				wb = WorkbookFactory.create(new File(filePath));
				return readExcel(wb, 0, 1, 0);
			}
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 读取excel文件
	 * 
	 * @param wb
	 * @param sheetIndex
	 *            sheet页下标：从0开始
	 * @param startReadLine
	 *            开始读取的行:从0开始
	 * @param tailLine
	 *            去除最后读取的行
	 */
	private static List<KpiLibDetails> readExcel(Workbook wb, int sheetIndex, int startReadLine, int tailLine) {
		Sheet sheet = wb.getSheetAt(sheetIndex);
		Row row = null;
		List<KpiLibDetails> examPapers = new ArrayList<>();
		for (int i = startReadLine; i < sheet.getLastRowNum() - tailLine + 1; i++) {
			row = sheet.getRow(i); 
			
			
			if(row.getCell(0)!=null){
				KpiLibDetails lb = new KpiLibDetails();
				String departmentName = row.getCell(0).getStringCellValue();
				lb.setDepartmentName(departmentName);
				//姓名
				String name = row.getCell(1).getStringCellValue();
				lb.setUserName(name);
				//KPI指标名称  遵守公司和事业部的规章制度，不发生违规违纪活动
				String kpiName = row.getCell(2).getStringCellValue();
				if(StringUtils.isNotBlank(kpiName)){
					kpiName=StringEscapeUtils.escapeHtml4(kpiName);
					kpiName = kpiName.replace("\"", "&quot;");
				}
				String kpiType = row.getCell(3).getStringCellValue();
				String s = row.getCell(4).getStringCellValue();
				lb.setKpiName(kpiName);
				lb.setKpiType(kpiType);
				lb.setStakeholderName(s);
				examPapers.add(lb);
			}
		}
		return examPapers;
	}
 

	/**
	 * 获取单元格的值
	 * 
	 * @param cell
	 * @return
	 */
	private static String getCellValue(Cell cell) {
		if (cell == null)
			return "";
		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			return cell.getStringCellValue();
		} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			return String.valueOf(cell.getBooleanCellValue());
		} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			return cell.getCellFormula();
		} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			return String.valueOf(cell.getNumericCellValue());
		}else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return null;
		}
		return "";
	}
}
