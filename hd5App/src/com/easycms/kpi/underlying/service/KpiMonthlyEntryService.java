package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiMonthlyEntry;
import com.easycms.kpi.underlying.domain.KpiMonthlyMain;

import java.util.Date;
import java.util.List;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电员工月度考核子表		kpi_monthly_entry<br>
 * <b>创建时间：</b>2017-08-20 下午06:01:22<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiMonthlyEntryService{


public KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer userId,Integer mainId,Integer libId);
	KpiMonthlyEntry findKpiMonthlyEntryByUserIdMonth(Integer mainId,Integer libId);
	/**
	 * 如果没有,则进行添加后返回
	 * @param entry
	 * @return
	 */
	KpiMonthlyEntry findOrAdd(KpiMonthlyEntry entry);
	public List<KpiMonthlyEntry> findOrAdd(List<KpiMonthlyEntry> entrys,boolean isaddNewLlib);
    /**
     * 查找所有
     * @return
     */
    List<KpiMonthlyEntry> findAll();
    List<KpiMonthlyEntry> findByMainId(Integer mainId);
    List<KpiMonthlyEntry> findByMainId(Integer mainId,String kpiType);
    List<KpiMonthlyEntry> findByMainIdAndStak(Integer mainId,Integer stakId);

    List<KpiMonthlyEntry> findAll(KpiMonthlyEntry condition);
    
	public boolean updateAll(List<KpiMonthlyEntry> entrys);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiMonthlyEntry> findPage(Specifications<KpiMonthlyEntry> sepc,Pageable pageable);


    Page<KpiMonthlyEntry> findPage(Page<KpiMonthlyEntry> page);

    Page<KpiMonthlyEntry> findPage(KpiMonthlyEntry condition,Page<KpiMonthlyEntry> page);

    /**
     *
     * @return
     */
    KpiMonthlyEntry userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiMonthlyEntry findKpiMonthlyEntry(String username);

    /**
     * 根据ID查找用户
     * @param id
     * @return
     */
    KpiMonthlyEntry findKpiMonthlyEntryById(Integer id);

    /**
     * 添加用户
     * @param kpiMonthlyEntry
     * @return
     */
    KpiMonthlyEntry add(KpiMonthlyEntry kpiMonthlyEntry);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiMonthlyEntry
     * @return
     */
    KpiMonthlyEntry update(KpiMonthlyEntry kpiMonthlyEntry);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiMonthlyEntry> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiMonthlyEntry> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiMonthlyEntry> findKpiMonthlyEntryByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiMonthlyEntry 发起问题的kpiMonthlyEntry
     * @return
     */
    List<KpiMonthlyEntry> getSolvers(KpiMonthlyEntry kpiMonthlyEntry);
	/**
	 * 根据用户 、主表id、和指标库id,实时更新指标规划
	 * @param userId
	 * @param mainId
	 * @param libId
	 * @return
	 */
    public boolean realLib(Integer userId,Integer mainId,Integer libId,Integer weight,Date finishDate);
    
    public boolean deleteByMainIdAndLibId(Integer mainId,Integer libId);

}
