package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiLib;
import com.easycms.kpi.underlying.domain.KpiSummarySheet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>核电人员月度绩效结果汇总表		kpi_summary_sheet<br>
 * <b>创建时间：</b>2017-08-20 下午05:17:43<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public interface KpiSummarySheetService{

    /**
     * 查找所有
     * @return
     */
    List<KpiSummarySheet> findAll();

    List<KpiSummarySheet> findAll(KpiSummarySheet condition);

    /**
     * 查找分页
     * @return
     */
//	Page<KpiSummarySheet> findPage(Specifications<KpiSummarySheet> sepc,Pageable pageable);


    Page<KpiSummarySheet> findPage(Page<KpiSummarySheet> page);

    Page<KpiSummarySheet> findPage(KpiSummarySheet condition,Page<KpiSummarySheet> page);

    /**
     *
     * @return
     */
    KpiSummarySheet userLogin(String username,String password);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    KpiSummarySheet findKpiSummarySheet(String username);

    /**
     * 根据ID查找用户
     * @param id
     * @return
     */
    KpiSummarySheet findKpiSummarySheetById(Integer id);

    /**
     * 添加用户
     * @param kpiSummarySheet
     * @return
     */
    KpiSummarySheet add(KpiSummarySheet kpiSummarySheet);

    boolean deleteByName(String name);


    boolean deleteByNames(String[] names);

    /**
     * 删除
     * @param id
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean delete(Integer[] ids);

    /**
     * 保存
     * @param kpiSummarySheet
     * @return
     */
    KpiSummarySheet update(KpiSummarySheet kpiSummarySheet);

    /**
     * 更新密码
     * @param newPassword
     */
    boolean updatePassword(Integer id,String oldPassword,String newPassword);

    /**
     * 用户是否存在
     * @param username
     * @return
     */
    boolean userExist(String username);

    boolean userExistExceptWithId(String username,Integer id);

    Integer count();

    /**
     * 查找用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiSummarySheet> findByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 查找上级用户
     * @param departmentId 部门ID
     * @param roleId 职务ID
     * @return
     */
    List<KpiSummarySheet> findParentByDepartmentIdAndRoleId(Integer departmentId,Integer roleId);

    /**
     * 根据直接领导userid查询下级。
     * @param userId 用户主键id
     * @return
     */
    List<KpiSummarySheet> findKpiSummarySheetByParentId(Integer userId);

    /**
     * 根据user获取当解决人列表
     * @param kpiSummarySheet 发起问题的kpiSummarySheet
     * @return
     */
    List<KpiSummarySheet> getSolvers(KpiSummarySheet kpiSummarySheet);
    
    /**
     * 获取当前实时考核数据
     * @return
     */
    List<KpiSummarySheet> getCurrentSheet();

    boolean deleteByMainId(Integer mainId);
    boolean deleteHisByMainId(Integer mainId);
/**导出历史绩效考核*/
	void statisticalReport(HttpServletRequest request, HttpServletResponse response);

}