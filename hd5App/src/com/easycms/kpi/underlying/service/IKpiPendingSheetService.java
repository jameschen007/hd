package com.easycms.kpi.underlying.service;

import com.easycms.core.util.Page;
import com.easycms.kpi.underlying.domain.KpiPendingSheet;

import java.util.Map;
/**
 * <b>项目名称：</b><br>
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>绩效待办列表		kpi_pending_sheet<br>
 * <b>创建时间：</b><br>
 * <b>@Copyright:</b>2019-爱特联
 */
public interface IKpiPendingSheetService {
    KpiPendingSheet findById(String id)throws Exception;
    public Page<KpiPendingSheet> findPageBySql(Page<KpiPendingSheet> page, Map<String, Object> map);
}
