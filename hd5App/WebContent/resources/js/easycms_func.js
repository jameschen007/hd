$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

/** *********生成随机密码工具**************** */
/** *************************************** */
/** *************************************** */
/**
 * 得到指定长度中的随机数，例如：从0～10，得到0到10之间的数
 * 
 * @param {Object}
 *            lbound 下标
 * @param {Object}
 *            ubound 长度
 */
function getRandomNum(lbound, ubound) {
	return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
}

/**
 * 
 * @param {Object}
 *            num 密码长度
 */
function makeRandomPwd(num) {
	var numberChars = "0123456789";
	var lowerChars = "abcdefghijklmnopqrstuvwxyz";
	var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var charSet = numberChars + lowerChars + upperChars;
	if (typeof num == "undefined")
		num = 8;
	var result = "";
	for (var i = 0; i < num; i++) {
		result += charSet.charAt(getRandomNum(0, charSet.length));
	}
	return result;
}

/**
 * 显示验证码
 * 
 * @param element
 * @param path
 */
function verifycode(element, path) {
	var $e = $(element);
	path += '?' + Math.floor(Math.random() * 100);
	var $img = $('<img alt="正在拉取验证码" src="'
			+ path
			+ '" height="30" style="vertical-align:middle;cursor: pointer;" onclick="verifycode(this,\''
			+ path + '\');"/>');
	$e.siblings("img").remove();
	$e.after($img.fadeIn());
	if ("A" == $e[0].tagName || "IMG" == $e[0].tagName) {
		$e.remove();
	}
}
/**
 * 全选
 */
function checkAll(e) {
	var $operator = $(e);
	var $items = $operator.parents("table").find("[type='checkbox']").not($operator);
	if ($operator.prop("checked")) {
		$items.each(function(){
			$(this).prop("checked", true);
		})
	} else {
		$items.each(function(){
			$(this).prop("checked", false);
		})
	}
}

/**
 * 检查表单值是否为空
 */
function isFormEmpty($form) {
	var params = $form.serialize();
	if (params == "") {
		return true;
	}

	var entries = params.split("&");
	for (var i = 0; i < entries.length; i++) {
		var entry = entries[i].split("=");
		if ($.trim(entry[entry.length - 1]) != "" && entry[i] != "queryType") {
			return false;
		}
	}
	return true;
}

function getJsonLength(jsonData) {

	var jsonLength = 0;

	for ( var item in jsonData) {

		jsonLength++;

	}

	return jsonLength;

}

var formatter = {
	isNull : function(string) {
		if (string == null || string == "" || typeof string == "undefined") {
			return true;
		}

		return false;
	},
	combinURL : function(url, params) {
		if (formatter.isNull(url)) {
			return null;
		}
		params = formatter.isNull(params) == true ? "" : params;

		var array = url.split("?");
		if (array.length > 1) {
			url = array[0];
			params += "&" + array[1];
		}
		url += "?" + params;
		return url;
	}
};
// 全选反选
// 参数:selected:传入this,表示当前点击的组件
// treeMenu:要操作的tree的id；如：id="userTree"
function treeChecked(selectAll, tree) {
	var checkAll = function($e, $children) {
		$e.on("change", function() {
			if ($e.prop("checked")) {
				$children.prop("checked", true);
			} else {
				$children.prop("checked", false);
			}
		})
	}

	// 子菜单选中则选中父菜单，所有子菜单都不选中则父菜单不选中
	var uncheckRoot = function(e) {
		$e = $(e);
		// 父菜单
		var $parent = $e.closest("li").parent("ul").prev("label").find(
				"[type='checkbox']");

		// 同级菜单
		var $brothers = $e.closest("li").siblings("li");

		$e.on("change", function() {
			var i = 0;
			// 子菜单选中则选中父菜单
			if ($(this).prop("checked")) {
				if (typeof $parent.attr("id") == "undefined") {
					$parent.prop("checked", true);
				}
			} else {
				// 所有子菜单不选中，则父菜单不选中
				$brothers.each(function() {
					$brother = $(this).find("[type='checkbox']");
					if (!$brother.prop("checked")) {
						i++;
					}
				});
				if (i == $brothers.length) {
					$parent.prop("checked", false);
				}
			}
		});
	}

	var $selectAll = $(selectAll);
	var $tree = $(tree);
	var $items = $tree.find("[type='checkbox']");
	checkAll($selectAll, $items);

	$items.each(function(i) {
		uncheckRoot(this);
		var $self = $(this);

		var $childRoot = $self.closest("label").next("ul");
		if ($childRoot.length == 1) {
			var $children = $childRoot.find("[type='checkbox']");
			checkAll($self, $children);
		}
	});

	// var roots = $(treeMenu).tree('getRoots');// 返回tree的所有根节点数组
	// if (selected.checked) {
	// for (var i = 0; i < roots.length; i++) {
	// var node = $(treeMenu).tree('find', roots[i].id);// 查找节点
	// $(treeMenu).tree('check', node.target);// 将得到的节点选中
	// }
	// } else {
	// for (var i = 0; i < roots.length; i++) {
	// var node = $(treeMenu).tree('find', roots[i].id);
	// $(treeMenu).tree('uncheck', node.target);
	// }
	// }
}

/**
 * 显示时间面板
 * 
 * @param setting
 */
function timePannel(setting) {
	var defaultSetting = {
		max : "",
		min : "",
		lang : 'en',
		now : "%y-%M-%d",
		dateFmt : "yyyy-MM-dd"
	};
	setting = $.extend({}, defaultSetting, setting || {});
	var minDate = setting.min == "" ? "" : "#F{$dp.$D(\'" + setting.min
			+ "\')}";
	var maxDate = setting.max == "" ? setting.now : "#F{$dp.$D(\'"
			+ setting.max + "\')}";
	WdatePicker({
		dateFmt : setting.dateFmt,
		lang : setting.lang,
		readOnly : true,
		minDate : minDate,
		maxDate : maxDate
	});
}

function datePannel(setting) {
	var defaultSetting = {
			max : "",
			min : "",
			lang : 'en',
			now : "%y-%M-%d",
			dateFmt : "yyyy-MM-dd"
	};
	setting = $.extend({}, defaultSetting, setting || {});
	var minDate = setting.min == "" ? setting.now : "#F{$dp.$D(\'" + setting.min
			+ "\')}";
	var maxDate = setting.max == "" ? "" : "#F{$dp.$D(\'"
		+ setting.max + "\')}";
	WdatePicker({
		dateFmt : setting.dateFmt,
		lang : setting.lang,
		readOnly : true,
		minDate : minDate,
		maxDate : maxDate
	});
}

function dateTimePannel(setting) {
	var defaultSetting = {
			max : "",
			min : "",
			formatter : "%y-%M-%d %H:%m:%s"
	};
	setting = $.extend({}, defaultSetting, setting || {});
	var minDate = setting.min == "" ? "" : "#F{$dp.$D(\'" + setting.min + "\')}";
	var maxDate = setting.max == "" ? "" : "#F{$dp.$D(\'"+ setting.max + "\')}";
	
	WdatePicker({
		readOnly : true,
		dateFmt:'yyyy-MM-dd HH:mm:ss',
		minDate : minDate,
		maxDate : maxDate
	});
}
/**
 * 撤销事件，当按下删除<--键，清空该表单字段所有内容
 */
function repeal(event) {
	var target = event.srcElement || event.target;
	var code = event.keyCode;
	if (code == 8) {
		$(target).val("");
	}
}

/** **************************************************************** */
/** **********************ACE STYLE 方法************************* */
/** **********************Date : 2015.03.26***************************** */
/** **********************Author:jiepeng*************************************** */
var getTime = function() {
	var date = new Date();
	date = date.getTime();
	return date;
}

/**
 * 加载文件
 */
var loadFile = function(path, selector) {
	dataTable.initToOldPageBack();
	// $("#main-content").load(path + "?_" + getTime());
	if(path.indexOf("?")==-1){
        path = path + "?_" + getTime();
	}else{
        path = path + "&_" + getTime();
	}
	$.ajax({
				url : path ,
				type : "get",
				beforeSend : function(XMLHttpRequest) {
					$("#main-content")
							.empty()
							.append(
									'<i class="icon-spinner icon-spin orange bigger-125"></i>');

				},
				success : function(data) {
					$("#main-content").empty().append(data);
				}
			});
}
var appendFile = function(path) {
	$.get(path, function(data) {
		$("body").append(data);
	});
}

var confirm = function(path, msg) {
	dataTable.initToOldPageBack();
	
	var tip = typeof msg == "undefined" ? "您确定要删除这些项?" : msg;
	bootbox.confirm(tip, "取消", "确定", function(result) {
		if (result) {
			$.ajax({
				url : path,
				type : "get",
				dataType:"json",
				success : function(data) {
					console.log(data);
					if (data) {

			            if(dataTable.pageNum!=null && dataTable.pagesize!=null && 
			            		((typeof(dataTable.pageNum)=='number')&&dataTable.pageNum>1)){
							var table = $(dataTable.tableID).DataTable();
							table.page(dataTable.pageNum-1 ).draw( false );
							dataTable.pagesize=null;
							dataTable.pageNum=null;
						}else{
							dataTable.refresh();
						}
						
						tips.success("操作成功");
					} else {
						tips.error("操作失败,ERROR:" + data.message);
					}
				}
			});
			// bootbox.alert("You are sure!");
		}
	});
}

var confirmFunc = function(msg,$_func,_param) {
	var tip = typeof msg == "undefined" ? "您确定要删除这些项?" : msg;
	bootbox.confirm(tip, "取消", "确定", function(result) {
		if (result) {
			$_func(_param);
		}
	});
}

var confirm_loadFile = function(path, msg,loadFileParam) {
	var tip = typeof msg == "undefined" ? "您确定要删除这些项?" : msg;
	bootbox.confirm(tip, "取消", "确定", function(result) {
		if (result) {
			$.ajax({
				url : path,
				type : "get",
				dataType:"json",
				success : function(data) {
					console.log(data);
					if (data) {
						loadFile(loadFileParam);
					} else {
						tips.error("操作失败,ERROR:" + data.message);
					}
				}
			});
			// bootbox.alert("You are sure!");
		}
	});
}
var batchConfirm = function(e, path,tipTxt, msg) {
	var tip = typeof tipTxt == "undefined" ? "请选择要删除的项!" : tipTxt;
	var checkboxes = $(e).find(":checked");
	if (checkboxes.length == 0) {
		tips.warning(tip);
	} else {
		var ids = "";
		checkboxes.each(function(i) {
			var key = $(this).attr("name");
			ids += key + "=" + $(this).val() + "&";
		});
		if(path.indexOf("?") != -1) {
			path = path + "&" + ids;
		}else{
			path = path + "?" + ids;
		}
		confirm(path,msg);
	}
}
var tips = {
	createDom : function() {
		if (typeof $("#tipsBox").html() == "undefined") {
			var $wrap = $('<div id="tipsBox"></div>');
			$wrap.css("position", "absolute").css("width", "80%").css("left",
					"200px").css("top", "-5px");
			$("div.page-header").prepend($wrap.fadeIn("fast"));
		}
	},
	success : function(msg) {
		Messenger().post({
			id : "Only-one-message",
			message : msg,
			type : 'success',
			showCloseButton : true
		});
		// tips.createDom();
		// var e = "#tipsBox";
		// var string = '<div class="alert alert-block alert-success">'
		// + '<button type="button" class="close" data-dismiss="alert"><i
		// class="icon-remove"></i></button>'
		// + '<p>' + '<strong><i class="icon-ok"></i></strong>' + msg
		// + '</p></div>';
		// $(e).empty().append(string);
		// setTimeout(function() {
		// $(e).fadeOut("slow",function(){$(e).remove();});
		// }, 5000)
	},
	error : function(msg) {
		Messenger().post({
			id : "Only-one-message",
			message : msg,
			type : 'error',
			showCloseButton : true
		});
		// tips.createDom();
		// var e = "#tipsBox";
		// var string = '<div class="alert alert-block alert-error">'
		// + '<button type="button" class="close" data-dismiss="alert"><i
		// class="icon-remove"></i></button>'
		// + '<p>' + '<strong><i class="icon-remove"></i></strong>' + msg
		// + '</p></div>';
		// $(e).empty().append(string);
		// setTimeout(function() {
		// $(e).fadeOut("slow",function(){$(e).remove();});
		// }, 5000)
	},
	warning : function(msg) {
		Messenger().post({
			id : "Only-one-message",
			message : msg,
			type : 'error',
			showCloseButton : true
		});
		// tips.createDom();
		// var e = "#tipsBox";
		// var string = '<div class="alert alert-block alert-warning">'
		// + '<button type="button" class="close" data-dismiss="alert"><i
		// class="icon-remove"></i></button>'
		// + '<p>' + '<strong>!</strong>' + msg + '</p></div>';
		// $(e).empty().append(string);
		// setTimeout(function() {
		// $(e).fadeOut("slow",function(){$(e).remove();});
		// }, 5000)
	}
}

/**
 * 校验规则封装对象
 */
var formValidtor = {
	// 模态校验规则
	modalValid : function(setting) {
		dataTable.initToOldPageBack();
		var valid = {
			handler : "",
			rules : {},
			messages : {},
			callback: function(){},
			submitHandler : function(form) {
				var $form = $(form);
				var url = $form.attr("action");
				var method = $form.attr("method");
				var nvp = $form.serialize();
				$.ajax({
					url : url,
					type : method,
					data : nvp,
					success : function(data) {
						if (data) {
							$modal.modal('hide');

				            if(dataTable.pageNum!=null && dataTable.pagesize!=null && 
				            		((typeof(dataTable.pageNum)=='number')&&dataTable.pageNum>1)){
								var table = $(dataTable.tableID).DataTable();
								table.page(dataTable.pageNum-1 ).draw( false );
								dataTable.pagesize=null;
								dataTable.pageNum=null;
							}else{
								dataTable.refresh();
							}
							if((typeof(data)=='string'&&(data=='true'||data=='"true"'))||(typeof(data)=='boolean'&&data)){
								tips.success("操作成功");
							}else if((typeof(data)=='string'&&(data=='false'||data=='"false"'))||(typeof(data)=='boolean'&&(data==false))){
								tips.success("操作失败");
							}
							setting.callback();
						} else {
							tips.error("操作失败");
						}
						if(setting.callbackFunc != undefined){
							setting.callbackFunc(data);
						}
					}
				});
			}
		}

		var settings = $.extend({}, valid, setting);
		var $form = $(settings.handler);
		var $modal = $form.closest("[role='dialog']");
//		$modal.modal({
//			show : true
//		});
//		$modal.on('hidden.bs.modal', function(e) {
//			$modal.remove();
//		})
		// 进行表单验证
		var url = $form.attr("action");

		$form.validate({
			rules : settings.rules,
			messages : settings.messages,
			submitHandler : settings.submitHandler
		});
	},
	valid : function(setting) {
		var valid = {
			handler : "",
			rules : {},
			messages : {},
			submitHandler : function(form) {
				var $form = $(form);
				var url = $form.attr("action");
				var method = $form.attr("method");
				var nvp = $form.serialize();
				$.ajax({
					url : url,
					type : method,
					data : nvp,
					success : function(data) {
						if (data) {
							tips.success("操作成功");
						} else {
							tips.error("操作失败");
						}
						if(setting.callbackFunc != undefined){
							setting.callbackFunc(data);
						}
					}
				});
			}
		}

		var settings = $.extend({}, valid, setting);
		// 进行表单验证
		var $form = $(settings.handler);
		var url = $form.attr("action");

		$form.validate({
			rules : settings.rules,
			messages : settings.messages,
			submitHandler : settings.submitHandler
		});
	}
}
/**
 * 数据表格
 */
var dataTable = {
		pageNum:null,pagesize:null,//服务器分页参数，如果需要记住上次页数和条数，设置此值
	tableID : "",
	showType : "list",// 表格类型，list为普通数据表格，tree为树形表格
	url : "",
	callback : function() {
	},
	remote : function() {
		$.ajax({
			url : dataTable.url,
			type : "post",
			success : function(data) {
				if (data) {
					// alert(dataTable.tableID)
					$(dataTable.tableID).find("tbody").empty().append(data);
					$(dataTable.tableID).find("tbody").treetable("loadBranch",
							data);
					$(dataTable.tableID).treetable({
						expandable : true,
						force : true
					});
					dataTable.callback();
				}
			}
		});
	},
	initTable : function(options) {
		dataTable.tableID = options.tableID;
		dataTable.url = options.url;
		dataTable.showType = options.showType;
		dataTable.callback = typeof options.callback == "undefined" ? dataTable.callback
				: options.callback;
		switch (dataTable.showType) {
		case "tree":
			$
					.ajax({
						url : dataTable.url,
						type : "post",
						success : function(data) {
							if (data) {
								$(dataTable.tableID).find("tbody").empty()
										.append(data);
								$(dataTable.tableID).treetable({
									expandable : true,
									force : false
								});
								$(dataTable.tableID).treetable();
								dataTable.callback();
							}
						}
					});
			break;
		default:
			var remote = {
				"ajax" : {
					"url" : dataTable.url,
					"type" : options.type == null ? "POST" : options.type,
					"data" : function(d) {
						var start = Number(d.start);
						var pagesize = Number(d.length);
						var page = (start / pagesize) + 1;
						d.page = (start / pagesize) + 1;
						d.rows = pagesize;
						d.columns = "";
						d.order = "";
					}
				}
			}
			options = $.extend({}, options, remote);
			$(dataTable.tableID).dataTable(options);
			dataTable.callback();
			break;
		}
	},
	refresh : function() {
		switch (dataTable.showType) {
		case "tree":
			$.ajax({
				url : dataTable.url,
				type : "post",
				success : function(data) {
					if (data) {
						$(dataTable.tableID).find("tbody").empty();
						$(dataTable.tableID)
								.treetable("loadBranch", null, data);
						dataTable.callback();
					}
				}
			});
			break;
		default:
			console.log("refresh.");
			$(dataTable.tableID).DataTable().ajax.reload();
			dataTable.callback();
			break;
		}
	},
	reload : function(params, clean) {
		var array = dataTable.url.split("?");
		var url = '';
		if (array.length > 1) {
			url = array[0];
			if (!clean){
				params += "&" + array[1];
			}
		} else {
			url = dataTable.url;
		}
		
		dataTable.url = url + "?" + params;
		
		switch (dataTable.showType) {
		case "tree":
			$.ajax({
				url : dataTable.url,
				type : "post",
				success : function(data) {
					if (data) {
						$(dataTable.tableID).find("tbody").empty();
						$(dataTable.tableID)
						.treetable("loadBranch", null, data);
						dataTable.callback();
					}
				}
			});
			break;
		default:
			console.log("Reload.");
		$(dataTable.tableID).DataTable().ajax.url(dataTable.url).load();
		dataTable.callback();
		break;
		}
	}
	,initToOldPageBack:function (){
		var table = $(dataTable.tableID).dataTable();
	    var oSettings = table.fnSettings();
	    if(oSettings){
		    dataTable.pageNum=(((oSettings._iDisplayStart)/(oSettings._iDisplayLength))+1);
		    dataTable.pagesize=oSettings._iDisplayLength;
	    }
	},initPageBack:function (){
//      //停止
        timesRunPage += 1;
        if(timesRunPage === 8){
            clearInterval(intervalPageBack);
        }

            if(dataTable.pageNum!=null && dataTable.pagesize!=null && 
            		((typeof(dataTable.pageNum)=='number')&&dataTable.pageNum>1)){
                    var table = $(dataTable.tableID).DataTable();
                    table.page(dataTable.pageNum-1 ).draw( false );
                    dataTable.pagesize=null;
                    dataTable.pageNum=null;
            }
    }

}

/**
 * 模态框动作 2015.04.28
 */
var modal = {
	id : null,
	open : function(setting) {
		if(setting.notExistDatatable){
			
		}else{
			dataTable.initToOldPageBack();			
		}
		$("#holdon").modal({show:true,backdrop:'static'});
		var initModal = function($modal, isRemove) {
			$modal.on('show.bs.modal', function (e) {
				$("#holdon").modal("hide");
			})
			var width = $modal.width();
			width = width ? width : "auto";
			$modal.modal({
				show : true
			}).css({
				width : width,
				'margin-left' : function() {
					return -($(this).width() / 2);
				}
			});
			if (true == isRemove) {
				$modal.on('hidden.bs.modal', function(e) {
					$modal.remove();
					modal.id = null;
				});
			}
		}

		var defaultSetting = {
			id : null,
			url : null
		}

		var conf = $.extend({}, defaultSetting, setting);
		var $modal = null;

		// 若URL不为空，则进行AJAX获取远程页面
		if (conf.url != null) {
				if (conf.datatable != null){
					var checkboxes = $(conf.datatable).find("[name='ids']:checked");
					if (checkboxes.length == 0) {
						tips.warning("请选择要操作的信息!");
						$("#holdon").modal("hide");
						return;
					} else {
						var ids = "";
						checkboxes.each(function(i) {
							ids += "ids=" + $(this).val() + "&";
						});
						var array = conf.url.split("?");
						var url = '';
						if (array.length > 1) {
							url = array[0];
							ids += array[1];
						} else {
							url = conf.url;
						}
						conf.url = url + "?" + ids;
					}
				}
				$.ajax({
				   type: "get",
				   url: conf.url,
				   dataType:"html",
				   success: function(data) {
						var id = "modal_" + getTime();
						modal.id = id;
						$("body").append($(data).attr("id", modal.id));
						$modal = $("#" + modal.id);
						initModal($modal, true);
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						$("#holdon").modal("hide");
						resetBtn();
						if(XMLHttpRequest.status == 200 || XMLHttpRequest.status == "200") {
							return false;
						}
						bootbox.alert('发生['+XMLHttpRequest.status+']错误,' + XMLHttpRequest.statusText);
					}
				});
		} else if (conf.id != null) {
			modal.id = conf.id;
			$modal = $("#" + modal.id);
			initModal($modal, false);
		}
	},
	close : function() {
		if (modal.id != null) {
			$("#" + modal.id).modal('hide');
		}
	}
}

var refreshMenu = function(url) {
	$.ajax({
		url : url,
		type : "get",
		dataType:"json",
		success : function(record) {
			if(record.code == "1000") {
				alert("刷新成功!");
				location.reload();
			}else{
				alert("刷新失败!");
			}
		}
	});
}

var startTimeSearch=new Date().getTime();
var isSearch=function(_activ){
	var endTimeSearch = new Date().getTime();
	var searchFlag=endTimeSearch-startTimeSearch>500;
	if(searchFlag){
		startTimeSearch = new Date().getTime();
		_activ();
	}
};

function formatFullDateWithChina(time){//转东８区时间
    if (!time) {
        return ''
    }
    console.log('time=='+time);
    //东8区，东时区记做正数
    var zoneOffset = 8;
    //算出时差,并转换为毫秒：
    var date = new Date()
    var offset2 = date.getTimezoneOffset()* 60 * 1000;
    //算出现在的时间：
    var nowDate2 = time;
    //此时东2区的时间
    var currentZoneDate = new Date(nowDate2 + offset2 + zoneOffset*60*60*1000);

    console.log("东8区现在是："+currentZoneDate.getTime());
    console.log('当地时间是：'+nowDate2);
    var curTime = new Date(currentZoneDate.getTime()).format("yyyy-MM-dd hh:mm:ss");
    return curTime
}
function download(url, data, method) {
	if (url) {
		data = typeof data == 'string' ? data : jQuery.param(data);
		var inputs = '';
		jQuery.each(data.split('&'), function() {
			var pair = this.split('=');
			inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
		});
		jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
	};
};