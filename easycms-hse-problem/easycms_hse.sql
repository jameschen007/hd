/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.1.73 : Database - easycms_v2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`easycms_v2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `easycms_v2`;

/*Table structure for table `hse_problem` */

DROP TABLE IF EXISTS `hse_problem`;

CREATE TABLE `hse_problem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_user` int(11) NOT NULL,
  `problem_title` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '问题名称',
  `unit` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '机组',
  `wrokshop` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '厂房',
  `eleration` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '标高',
  `roomno` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '房间号',
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '问题描述',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `problem_status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '执行状态',
  `responsible_dept` int(11) DEFAULT NULL COMMENT '责任部门',
  `responsible_team` int(11) DEFAULT NULL COMMENT '责任班组',
  `target_date` timestamp NULL DEFAULT NULL COMMENT '整改期限',
  `finish_date` timestamp NULL DEFAULT NULL COMMENT '问题关闭时间',
  `problem_step` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '当前步骤',
  PRIMARY KEY (`id`),
  KEY `FK_SP_EU` (`create_user`),
  CONSTRAINT `FK_SP_EU` FOREIGN KEY (`create_user`) REFERENCES `ec_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hse_problem_file` */

DROP TABLE IF EXISTS `hse_problem_file`;

CREATE TABLE `hse_problem_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `filepath` varchar(100) COLLATE utf8_bin NOT NULL,
  `uploadtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `filename` varchar(100) COLLATE utf8_bin NOT NULL,
  `filetype` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'before:故障照片，after:整改照片',
  PRIMARY KEY (`id`),
  KEY `FK_SPF_SP` (`problem_id`),
  CONSTRAINT `FK_SPF_SP` FOREIGN KEY (`problem_id`) REFERENCES `hse_problem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `hse_problem_solve` */

DROP TABLE IF EXISTS `hse_problem_solve`;

CREATE TABLE `hse_problem_solve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL COMMENT '问题编号',
  `solve_user` int(11) DEFAULT NULL COMMENT '处理人ID',
  `solve_step` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `solve_status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '处理状态',
  `solve_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '处理时间',
  `solve_description` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SPS_SP` (`problem_id`),
  KEY `FK_SPS_EU` (`solve_user`),
  CONSTRAINT `FK_SPS_EU` FOREIGN KEY (`solve_user`) REFERENCES `ec_user` (`id`),
  CONSTRAINT `FK_SPS_SP` FOREIGN KEY (`problem_id`) REFERENCES `hse_problem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
