package com.easycms.hd.hseproblem.dao;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.hseproblem.domain.HseProblem;

public interface HseProblemDao extends Repository<HseProblem,Integer>,BasicDao<HseProblem> {

	String condition3 = " and ( date_format(h.createDate,'%Y-%m-%d') like ?3 or h.problemTitle like ?3 or exists (select 1 from User u where u.id=h.createUser and u.realname like ?3)) ";
	String condition2 = " and ( date_format(h.createDate,'%Y-%m-%d') like ?2 or h.problemTitle like ?2 or exists (select 1 from User u where u.id=h.createUser and u.realname like ?2) ) ";
	String condition1 = " and ( date_format(h.createDate,'%Y-%m-%d') like ?1 or h.problemTitle like ?1 or exists (select 1 from User u where u.id=h.createUser and u.realname like ?1)) ";

//	String condition3 = "";
//	String condition2 = "";
//	String condition1 = "";

	@Query("FROM HseProblem h ORDER BY h.createDate desc")
	Page<HseProblem> findAll(Pageable page);
	@Query("FROM HseProblem h WHERE 1=1 "+condition1+"ORDER BY h.createDate desc")
	Page<HseProblem> findAll_keyword(String keyword,Pageable page);

	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 ORDER BY h.createDate desc")
	Page<HseProblem> findByHseProblemStatus( String status,Pageable page);
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 "+condition2+" ORDER BY h.createDate desc")
	Page<HseProblem> findByHseProblemStatus_keyword( String status,String keyword,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.responsibleDept= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findAllHseProblemByDept(Integer deptId,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.responsibleDept= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndDeptId( String status,Integer deptId,Pageable page);

	@Query("FROM HseProblem h WHERE h.responsibleTeam= ?1 ORDER BY h.createDate desc")
	Page<HseProblem> findAllHseProblemByTeam(Integer teamId,Pageable page);
	@Query("FROM HseProblem h WHERE h.responsibleTeam= ?1"+condition2+" ORDER BY h.createDate desc")
	Page<HseProblem> findAllHseProblemByTeam_keyword(Integer teamId,String keyword,Pageable page);

	@Query("FROM HseProblem h WHERE h.responsibleTeam= ?1 ORDER BY h.createDate desc")
	Page<HseProblem> findAllHseProblemByCaptain(Integer captainId,Pageable page);
	@Query("FROM HseProblem h WHERE h.responsibleTeam= ?1"+condition2+" ORDER BY h.createDate desc")
	Page<HseProblem> findAllHseProblemByCaptain_keyword(Integer captainId,String keyword,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.responsibleTeam= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndTeamId( String status,Integer teamId,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.responsibleTeam= ?2 "+condition3+" ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndTeamId_keyword( String status,Integer teamId,String keyword,Pageable page);

	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.responsibleDept= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndCaptainId( String status,Integer captainId,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.responsibleDept= ?2 "+condition3+" ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndCaptainId_keyword( String status,Integer captainId,String keyword,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.createUser= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndCreateUser( String status,Integer userId,Pageable page);
	
	@Query("FROM HseProblem h WHERE h.problemStatus= ?1 AND h.createUser= ?2 "+condition3+" ORDER BY h.createDate desc")
	Page<HseProblem> findByStatusAndCreateUser_keyword( String status,Integer userId,String keyword,Pageable page);

	@Query("FROM HseProblem h WHERE h.createUser= ?1 ORDER BY h.createDate desc")
	Page<HseProblem> findByCreateUser(Integer userId,Pageable page);
	@Query("FROM HseProblem h WHERE h.createUser= ?1 "+condition2+" ORDER BY h.createDate desc")
	Page<HseProblem> findByCreateUser_keyword(Integer userId,String keyword,Pageable page);

	@Query("FROM HseProblem h WHERE h.id IN (?1) AND h.problemStatus= ?2 ORDER BY h.createDate desc")
	Page<HseProblem> findByHseProblemAndSolveStatus(Set<String> solveStatus, String status,Pageable page);
	@Query("FROM HseProblem h WHERE h.id IN (?1) AND h.problemStatus= ?2"+condition3+" ORDER BY h.createDate desc")
	Page<HseProblem> findByHseProblemAndSolveStatus_keyword(Set<String> solveStatus, String status,String keyword,Pageable page);

}
