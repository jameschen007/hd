package com.easycms.hd.hseproblem.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.hseproblem.dao.HseProblemSolveDao;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.domain.HseProblemSolve;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;

@Service("hseProblemSolveService")
public class HseProblemSolveServiceImpl implements HseProblemSolveService {
	@Autowired
	private HseProblemSolveDao hseProblemSolveDao;

	@Override
	public HseProblemSolve add(HseProblemSolve hseProblemSolve) {
		return hseProblemSolveDao.save(hseProblemSolve);
	}

	@Override
	public boolean delete(HseProblem hseProblemSolve) {
		return hseProblemSolveDao.deleteById(hseProblemSolve.getId()) > 0;
	}

	@Override
	public HseProblemSolve update(HseProblemSolve hseProblemSolve) {
		return hseProblemSolveDao.save(hseProblemSolve);
	}

	@Override
	public HseProblemSolve findById(Integer id) {
		return hseProblemSolveDao.findById(id);
	}

	@Override
	public List<HseProblemSolve> findByProblemId(Integer hseProblemId) {
		return hseProblemSolveDao.findByHseProblemId(hseProblemId);
	}
	
	@Override
	public List<HseProblemSolve> findByProblenIdAndSolveStep(Integer id, String solveRole) {
		return hseProblemSolveDao.findByHseProblemIdAndSolveStep(id, solveRole);
	}

	@Override
	public Set<String> findByStatus(String status) {
		return hseProblemSolveDao.findProblemIdByStatus(status);
	}


}
