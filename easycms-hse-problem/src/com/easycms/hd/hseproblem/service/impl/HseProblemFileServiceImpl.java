package com.easycms.hd.hseproblem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.hseproblem.dao.HseProblemFileDao;
import com.easycms.hd.hseproblem.domain.HseProblemFile;
import com.easycms.hd.hseproblem.service.HseProblemFileService;

@Service("hseProblemFileService")
public class HseProblemFileServiceImpl implements HseProblemFileService {
	@Autowired
	private HseProblemFileDao hseProblemFileDao;

	@Override
	public HseProblemFile add(HseProblemFile hseProblem) {
		return hseProblemFileDao.save(hseProblem);
	}

	@Override
	public boolean delete(HseProblemFile hseProblem) {
		return hseProblemFileDao.deleteById(hseProblem.getId()) > 0;
	}

	@Override
	public HseProblemFile update(HseProblemFile HseProblemFile) {
		return hseProblemFileDao.save(HseProblemFile);
	}

	@Override
	public HseProblemFile findById(Integer id) {
		return hseProblemFileDao.findById(id);
	}

	@Override
	public List<HseProblemFile> findByProblemId(Integer id) {
		return hseProblemFileDao.findByHseProblemId(id);
	}

	@Override
	public List<HseProblemFile> findByProblemIdAndFileType(Integer id, String type) {
		return hseProblemFileDao.findByHseProblemIdAndFileType(id, type);
	}

}
