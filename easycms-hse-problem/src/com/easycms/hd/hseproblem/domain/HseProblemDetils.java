package com.easycms.hd.hseproblem.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class HseProblemDetils implements Serializable {

	private static final long serialVersionUID = -7230844045130213429L;

	private Integer id;
    
	/**
	 * 标题
	 */
	private String problemTitle;

    /**
     * 机组
     */
    private String unit;

    /**
     * 厂房
     */
    private String wrokshop;

    /**
     * 标高
     */
    private String eleration;

    /**
     * 房间号
     */
    private String roomno;

    /**
     * 责任部门
     */
    private String responsibleDept;
    
    /**
     * 责任班组
     */
    private String responsibleTeam;
    
    /**
     * 问题描述
     */
    private String description;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 当前状态
     */
    private String problemStatus;


}
