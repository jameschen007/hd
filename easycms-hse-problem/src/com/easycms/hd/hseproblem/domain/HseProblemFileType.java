package com.easycms.hd.hseproblem.domain;

/**
 * 文件类型
 * @author ZengMao
 * @create Date:2017年11月23日
 */
public enum HseProblemFileType {
	
	/**
	 * 问题照片
	 */
	before,
	
	/**
	 * 整改照片
	 */
	after,
	
	/**
	 * 再次整改照片
	 */
	again

}
