package com.easycms.hd.hseproblem.domain;

public enum HseProblemStep {
	
	/**
	 * 新问题
	 */
	isNew,
	/**
	 * 待整改
	 */
	isNeedRenovate,
	
	/**
	 * 待审核
	 */
	isNeedCheck,
	
	/**
	 *重新整改 
	 */
	isRenovateAgain,
	
	/**
	 * 二次审核
	 */
	isCheckAgain,
	
	/**
	 *完成 
	 */
	isFinished,

}
