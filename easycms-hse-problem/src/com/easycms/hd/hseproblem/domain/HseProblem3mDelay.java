package com.easycms.hd.hseproblem.domain;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 仅连续三个月(90天)内的安全hse_problem　数据记录,其中的已超期的记录
 */
@Entity
@Table(name = "hse_problem_3m_delay")
@PrimaryKeyJoinColumn(name="id")
public class HseProblem3mDelay extends HseProblem implements Serializable {
	private static final long serialVersionUID = 900105837436361256L;

}