package com.easycms.hd.hseproblem.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.service.HseProblemService;

@Component
public class HseProblemDirective extends BaseDirective<HseProblem> {
	private static final Logger logger = Logger.getLogger(HseProblemDirective.class);
	@Autowired
	private HseProblemService hseProblemservice;

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return hseProblemservice.count();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected HseProblem field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		logger.debug("[id] ==> " + id);
		if (id != null) {
			HseProblem hseProblem = hseProblemservice.findById(id);
			return hseProblem;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<HseProblem> list(Map params, String filter, String order, String sort, boolean pageable,
			Page<HseProblem> pager, Map<String, Object> envParams) {
		return hseProblemservice.findAll(pager,null,null).getDatas();//TODO hse keyword add
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<HseProblem> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
