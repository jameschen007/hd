package com.easycms.hd.hseproblem.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.hseproblem.domain.HseProblemDetils;
import com.easycms.hd.hseproblem.service.HseProblemService;

@Component
public class HseProblemDetilsDirective extends BaseDirective<HseProblemDetils> {
	private static final Logger logger = Logger.getLogger(HseProblemDetilsDirective.class);
	@Autowired
	private HseProblemService hseProblemservice;

	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return hseProblemservice.count();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected HseProblemDetils field(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<HseProblemDetils> list(Map params, String filter, String order, String sort, boolean pageable,
			Page<HseProblemDetils> pager, Map<String, Object> envParams) {
		String status = FreemarkerTemplateUtility.getStringValueFromParams(params, "problemStatus");
		logger.debug("status------->"+status);

		pager = hseProblemservice.findDetilsAll(pager, status);

		return pager.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<HseProblemDetils> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
