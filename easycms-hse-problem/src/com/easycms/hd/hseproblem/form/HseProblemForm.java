package com.easycms.hd.hseproblem.form;

import java.io.Serializable;
import java.util.Date;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemForm extends BasicForm implements Serializable {
	private static final long serialVersionUID = 5953137798429269067L;
	
	private Integer id;

    private Integer createUser;
    
	private String problemTitle;

    private String unit;

    private String wrokshop;

    private String eleration;

    private String roomno;

    private String description;

    private Date createTime;

    private String problemStatus;

    private Integer responsibleDept;
    
    private Integer responsibleTeam;
    
    private Date startDate;
    
    private Date endDate;

}
