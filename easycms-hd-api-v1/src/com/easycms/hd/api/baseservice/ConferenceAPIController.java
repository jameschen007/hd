package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.request.ConferencePageRequestForm;
import com.easycms.hd.api.request.ConferenceRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.ConferencePageResult;
import com.easycms.hd.api.response.ConferenceResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.ConferenceApiService;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.domain.ConferenceFile;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.hd.conference.service.ConferenceFileService;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/conference")
@Api(value = "ConferenceAPIController", description = "会议通知相关的api")
@Slf4j
@javax.transaction.Transactional
public class ConferenceAPIController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConferenceApiService conferenceApiService;
	
	@Autowired
	private ConferenceService conferenceService;
	
	@Autowired
	private ConferenceFileService conferenceFileService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_conference_path']}")
	private String fileConferencePath;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	/**
	 * 发起会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(value = "发起会议", notes = "用户发起一次会议")
	public JsonResult<Boolean> createConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ConferenceRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		User user = userService.findUserById(form.getLoginId());
		
		if (null == form.getParticipants() || form.getParticipants().length < 1) {
			result.setCode("-1000");
			result.setMessage("请选择参会人。");
			return result;
		}
		
		JsonResult<Integer> conferenceResult = conferenceApiService.insert(form, user, ConferenceSource.API);
		if (null != conferenceResult && null != conferenceResult.getResponseResult()) {
			String filePath = basePath + fileConferencePath;
			List<Integer> removeIds = new ArrayList<Integer>();
			List<ConferenceFile>  conferenceFiles = conferenceFileService.findFilesByConferenceId(conferenceResult.getResponseResult());
			if (null != form.getFileIds() && form.getFileIds().size() > 0) {
				if (null != conferenceFiles && !conferenceFiles.isEmpty()) {
					for (ConferenceFile conferenceFile : conferenceFiles) {
						if (!form.getFileIds().contains(conferenceFile.getId())) {
							FileUtils.remove(filePath + conferenceFile.getPath());
							removeIds.add(conferenceFile.getId());
						}
					}
				}
			} else {
				if (null != conferenceFiles && !conferenceFiles.isEmpty()) {
					for (ConferenceFile conferenceFile : conferenceFiles) {
						FileUtils.remove(filePath + conferenceFile.getPath());
						removeIds.add(conferenceFile.getId());
					}
				}
			}
			
			if (null != removeIds && !removeIds.isEmpty()) {
				conferenceFileService.batchRemove(removeIds.toArray(new Integer[] {}));
			}
			
            List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
            if (fileInfos != null && fileInfos.size() > 0) {
                for (FileInfo fileInfo : fileInfos) {
                	ConferenceFile file = new ConferenceFile();
                    file.setPath(fileInfo.getNewFilename());
                    file.setTime(new Date());
                    file.setConferenceid(conferenceResult.getResponseResult());
                    String fileName = fileInfo.getFilename();
                    if (fileName.length() > 90) {
                    	fileName = fileName.substring(0, 90);
                    }
                    file.setFileName(fileName);
                    log.debug("upload file success : " + file.toString());
                    conferenceFileService.add(file);
                }
            }
		} else {
			result.setCode("-1000");
			result.setMessage("会议操作失败。");
			return result;
		}
		return result;
	}
	
	/**
	 * 获取会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取会议", notes = "获取不同类型的会议记录 -- 发送和草稿")
	public JsonResult<ConferencePageResult> getConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ConferencePageRequestForm form) throws Exception {
		JsonResult<ConferencePageResult> result = new JsonResult<ConferencePageResult>();
		if (null == form.getType()) {
			result.setCode("-1000");
			result.setMessage("会议请求类型不能为空。");
			return result;
		}
		
		User user = userService.findUserById(form.getLoginId());
		
		if (null != form.getPagenum() && null != form.getPagesize()) {
			Page<Conference> page = new Page<Conference>();

			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
			
			ConferencePageResult conferencePageResult = conferenceApiService.getConferencePageResult(form.getType(), user.getId(), page);
			result.setResponseResult(conferencePageResult);
			result.setMessage("成功获取分页信息");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 获取会议
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取会议-单个", notes = "获取单个的会议记录")
	public JsonResult<ConferenceResult> getSingleConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form, 
			@PathVariable("id") Integer id) throws Exception {
		JsonResult<ConferenceResult> result = new JsonResult<ConferenceResult>();
		Conference conference = conferenceService.findById(id);
		
		if (null != conference) {
			ConferenceApiRequestType type = ConferenceApiRequestType.valueOf(conference.getType());
			
			result.setResponseResult(conferenceApiService.conferenceTransferToConferenceResult(conference, form.getLoginId(), type, true));
		} else {
			result.setCode("-1001");
			result.setMessage("会议不存在");
			return result;
		}
		
		return result;
	}
}
