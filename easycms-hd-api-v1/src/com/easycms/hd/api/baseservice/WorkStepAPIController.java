package com.easycms.hd.api.baseservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.WorkStepRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.WorkStepPageDataResult;
import com.easycms.hd.api.response.WorkStepResult;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.mybatis.dao.bean.WorkStepForBatchWitness;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/workstep")
@Api(value = "WorkStepAPIController", description = "六级滚动计划实际工序步骤相关的api")
@Slf4j
@javax.transaction.Transactional
public class WorkStepAPIController {
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private WorkStepApiService workStepApiService;

	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "工序计划分页信息", notes = "用于返回工序步骤计划中的各类不同情况")
	public JsonResult<WorkStepPageDataResult> workStepList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") WorkStepRequestForm form) throws Exception {
		log.info("Get workstep with pagenation");
		JsonResult<WorkStepPageDataResult> result = new JsonResult<WorkStepPageDataResult>();
		
		User user = null;
		if (null != form.getUserId()) {
			user = userService.findUserById(form.getUserId());
		} else {
			user = userService.findUserById(form.getLoginId());
		}
		if (null == user) {
			result.setCode("-2001");
			result.setMessage("用户信息为空。");
			return result;
		}
		
		if (null != form.getPagenum() && null != form.getPagesize()) {
			Page<WorkStep> page = new Page<WorkStep>();

			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
			
			WorkStepPageDataResult workStepPageDataResult = workStepApiService.getWorkStepPageDataResult(page, user, form.getStatus(), form.getType(), form.getKeyword());
			result.setResponseResult(workStepPageDataResult);
			result.setMessage("成功获取工序列表");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 按滚动计划ID获取工序
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/rollingplan/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "得到工序步骤列表", notes = "给定rollingplan id得到其所有的工序步骤")
	public JsonResult<List<WorkStepResult>> workStepListByRollingPlanId(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable @ApiParam(value = "请求的rollingplan id号", required = true) Integer id,
			@RequestParam @ApiParam(value = "登录用户的ID", required = true) Integer loginId){
		log.info("Find WorkStep by RollingPlan id [" + id + "]");
		JsonResult<List<WorkStepResult>> result = new JsonResult<List<WorkStepResult>>();
		
		Integer[] ids = new Integer[] {id};
		
		List<WorkStep> workSteps = workStepService.findByRollingplanIds(ids);
		
		PlanUtil.lastOneMark(workSteps);
		
		result.setResponseResult(workStepApiService.workStepTransferForMobile(workSteps));
		result.setMessage("成功获取指定滚动计划下的所有工序");
		return result;
	}
	
	/**
	 * 根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/workStepList", method = RequestMethod.GET)
	@ApiOperation(value = "根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序", notes = "根据滚动计划【即作业条目】ｉｄ查询出相同的工序列表，供批量发起　多条计划的相同工序")
	public JsonResult<List<WorkStepForBatchWitness>> workStepListByRollingPlanIds(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam @ApiParam(value = "请求的rollingplan id号集合", required = true) List<Integer> ids,
			@RequestParam @ApiParam(value = "登录用户的ID", required = true) Integer loginId){
		log.info("Find WorkStep by RollingPlan id [" + ids + "]");
		JsonResult<List<WorkStepForBatchWitness>> result = new JsonResult<List<WorkStepForBatchWitness>>();
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		map.put("planIds", ids);
		List<WorkStepForBatchWitness> workSteps = workStepService.getWorkStep(map);
		
		result.setResponseResult(workSteps);
		result.setMessage("成功获取指定滚动计划下的工序并集");
		return result;
	}
	
	/**
	 * 按工序ID获取工序
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "工序步骤详情", notes = "给定工序步骤 id得到其详细信息")
	public JsonResult<WorkStepResult> workStepListById(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable @ApiParam(value = "请求的工序步骤 id号", required = true) Integer id,
			@RequestParam @ApiParam(value = "登录用户的ID", required = true) Integer loginId){
		JsonResult<WorkStepResult> result = new JsonResult<WorkStepResult>();
		WorkStep workStep = workStepService.getById(id);
		log.info("Find WorkStep by id [" + id + "]");
		result.setResponseResult(workStepApiService.workStepTransferForMobile(workStep));
		result.setMessage("成功得到指定工序的详细信息");
		return result;
	}
}
