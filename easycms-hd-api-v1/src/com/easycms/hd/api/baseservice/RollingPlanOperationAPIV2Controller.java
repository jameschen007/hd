package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.request.RollingPlanBackFillRequestFormV2;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/v2/rollingplan_op")
@Api(value = "RollingPlanOperationAPIV2Controller", description = "六级滚动计划操作相关的api -- V2")
@javax.transaction.Transactional
public class RollingPlanOperationAPIV2Controller {
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private JPushService jPushService;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	@ResponseBody
	@RequestMapping(value = "/backfill", method = RequestMethod.POST)
	@ApiOperation(value = "六级滚动计划回填", notes = "用于六级滚动计划的回填")
	public JsonResult<Boolean> rollingPlanBackFill(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") RollingPlanBackFillRequestFormV2 form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		RollingPlan rollingplan = rollingPlanService.findById(form.getId());
		if (null == rollingplan) {
			result.setCode("-1001");
			result.setMessage("滚动计划不存在");
			return result;
		}
		
		if (!CommonUtility.isNonEmpty(form.getWelder())) {
			result.setCode("-1001");
			result.setMessage("焊工信息必填");
			return result;
		}
		
		if (CommonUtility.isNonEmpty(rollingplan.getWelder2()) && null != rollingplan.getWelddate()) {
			result.setCode("-1001");
			result.setMessage("已经完成过回填的滚动计划，本次提交失败。");
			return result;
		}
		
		if (!rollingPlanService.backFillV2(form.getId(), form.getWelder(), form.getWeldDate())) {
			result.setCode("-1001");
			result.setMessage("回填失败");
			return result;
		}
		
		String message = "滚动计划：" + rollingplan.getDrawno() + " 已回填完成。";
        JPushExtra extra = new JPushExtra();
		extra.setCategory(JPushCategoryEnums.ROLLINGPLAN_FILLBACK_COMPLETE.name());
		jPushService.pushByUserId(extra, message, JPushCategoryEnums.ROLLINGPLAN_FILLBACK_COMPLETE.getName(), rollingplan.getConsmonitor());
		
		result.setMessage("成功回填滚动计划");
		return result;
	}
}
