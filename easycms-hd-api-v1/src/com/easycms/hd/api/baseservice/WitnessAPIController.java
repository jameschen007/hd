package com.easycms.hd.api.baseservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.WitnessRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.WitnessPageResult;
import com.easycms.hd.api.response.WitnessResult;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/witness")
@Api(value = "WitnessAPIController", description = "见证相关的api")
@Slf4j
public class WitnessAPIController {
	private static Logger logger = Logger.getLogger(WitnessAPIController.class);
	@Autowired
	private WitnessApiService witnessApiService;
	@Autowired
	private UserService userService;
	
	/**
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "见证列表", notes = "得到各类不同状态的见证列表")
	public JsonResult<WitnessPageResult> witness(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("requestForm") WitnessRequestForm witnessRequestForm){
		JsonResult<WitnessPageResult> result = new JsonResult<WitnessPageResult>();

		User user;
		User loginUser;
		if (null != witnessRequestForm.getUserId()) {
			logger.debug("witnessRequestForm.getUserId()="+witnessRequestForm.getUserId());
			user = userService.findUserById(witnessRequestForm.getUserId());
		} else {
			logger.debug("witnessRequestForm.getLoginId()="+witnessRequestForm.getLoginId());
			user = userService.findUserById(witnessRequestForm.getLoginId());
		}
		if (null == user) {
			result.setCode("-1001");
			result.setMessage("指定用户不存在");
			return result;
		}
		
		if (null == witnessRequestForm.getStatus()) {
			result.setCode("-1001");
			result.setMessage("查询问题类型必须填写");
			return result;
		}
		if (null == witnessRequestForm.getType()) {
			result.setCode("-1002");
			result.setMessage("滚动计划类型必须填写");
			return result;
		}
		
		if (null != witnessRequestForm.getPagenum() && null != witnessRequestForm.getPagesize()) {
			Page<WorkStepWitness> page = new Page<WorkStepWitness>();
			page.setPageNum(witnessRequestForm.getPagenum());
			page.setPagesize(witnessRequestForm.getPagesize());

			loginUser = userService.findUserById(witnessRequestForm.getLoginId());
			if(witnessRequestForm.getUserId()!=null){
				user = userService.findUserById(witnessRequestForm.getUserId());
			}
			
			WitnessPageResult witnessPageDataResult = witnessApiService.getWitnessPageResult(page, user, witnessRequestForm.getStatus(), witnessRequestForm.getType() , witnessRequestForm.getNoticePointType(), witnessRequestForm.getKeyword(),loginUser,witnessRequestForm.getRoleId());
			result.setResponseResult(witnessPageDataResult);
			result.setMessage("成功获取见证列表");
			return result;
		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "见证详细信息", notes = "得到指定ID的见证详细信息")
	public JsonResult<WitnessResult> witnessDetail(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") BaseRequestForm form,
			@RequestParam("type") @ApiParam(value = "请求的滚动计划类型", required = true) String type,
			@PathVariable("id") Integer witnessId){
		JsonResult<WitnessResult> result = new JsonResult<WitnessResult>();
		User user = userService.findUserById(form.getLoginId());
		
		result.setResponseResult(witnessApiService.findByWitnessIdAndRollingPlanType(user, witnessId, type, true));
		result.setMessage("成功获取指定见证信息");
		logger.info("得到指定ID的见证详细信息"+witnessId+"成功获取指定见证信息!");
		return result;
	}
}