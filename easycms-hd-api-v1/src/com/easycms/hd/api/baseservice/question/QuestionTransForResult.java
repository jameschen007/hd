package com.easycms.hd.api.baseservice.question;

import java.util.ArrayList;
import java.util.List;

import com.easycms.hd.api.response.ProblemFileResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.management.user.domain.User;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>将实体类转换为结果类输出接口<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public class QuestionTransForResult {
	

	public ProblemFileResult problemFile(ProblemFile file){
		ProblemFileResult r = new ProblemFileResult();
		r.setPath(file.getPath());
		r.setFileName(file.getFileName());
		return r ;
	}
	
	public List<ProblemFileResult> problemFile(List<ProblemFile> files){
		List<ProblemFileResult> r = new ArrayList<ProblemFileResult>();
		if(files!=null && files.size()>0){
			for(ProblemFile f:files){
				ProblemFileResult r1 = new ProblemFileResult();
				//http://39.108.165.171/hdxt/api/files/problem/4.jpg?loginId=258
				r1.setPath("/hdxt/api/files/problem/"+f.getPath());
				r1.setFileName(f.getFileName());
				r.add(r1);
			}
		}
		return r ;
	}
	

//RollingQuestionResult com.easycms.hd.api.service.impl.QuestionApiServiceImpl.transferTo(RollingQuestion question)
	
	public List<UserResult> user(List<User> users){
		List<UserResult> ret = new ArrayList<UserResult>();
		if(users!=null && users.size()>0){
			for(User u:users){
				UserResult r = new UserResult();
				r.setId(u.getId());
				r.setUsername(u.getName());
				r.setRealname(u.getRealname());
				ret.add(r);
			}
		}
		return ret ;
	}
}
