package com.easycms.hd.api.baseservice.question;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.upload.UploadUtilToday;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.question.QuestionAnswerStatusDbEnum;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.question.QuestionFeedbackRequestForm;
import com.easycms.hd.api.request.question.QuestionListRequestForm_BZ;
import com.easycms.hd.api.request.question.QuestionListRequestForm_Coordinate;
import com.easycms.hd.api.request.question.QuestionListRequestForm_DZ;
import com.easycms.hd.api.request.question.QuestionListRequestForm_JS;
import com.easycms.hd.api.request.question.QuestionListRequestForm_MY;
import com.easycms.hd.api.request.question.QuestionListRequestForm_ZZ;
import com.easycms.hd.api.request.question.QuestionRequestForm;
import com.easycms.hd.api.response.JsonResult;
//import com.easycms.hd.api.response.question.RollingQuestionAssignResult;
import com.easycms.hd.api.response.question.RollingQuestionPageDataResult;
import com.easycms.hd.api.response.question.RollingQuestionResult;
import com.easycms.hd.api.response.question.RollingQuestionStatisticsResult;
import com.easycms.hd.api.service.QuestionApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.RollingPlanFlag;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.problem.domain.ProblemFile;
import com.easycms.hd.problem.service.ProblemFileService;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionProcess;
import com.easycms.hd.question.domain.RollingQuestionSolver;
import com.easycms.hd.question.service.RollingQuestionProcessService;
import com.easycms.hd.question.service.RollingQuestionService;
import com.easycms.hd.question.service.RollingQuestionSolverService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/question")
@Api(value = "QuestionAPIController", description = "问题模块相关的api")
@Slf4j
@javax.transaction.Transactional
public class QuestionAPIContrller {

	@Autowired
	private QuestionApiService questionApiService;
	@Autowired
	private ProblemFileService problemFileService;
	@Autowired
	private RollingQuestionService rollingQuestionService;
	@Autowired
	private RollingQuestionSolverService rollingQuestionSolverService;
	@Autowired
	private RollingQuestionProcessService rollingQuestionProcessService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private RollingPlanApiService rollingPlanApiService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	@Autowired
	private UserService userService;

	@Autowired
	private JPushService jPushService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private RoleService roleService;

	@ResponseBody
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ApiOperation(value = "组长创建问题", notes = "用于组长创建问题")
	public JsonResult<Boolean> create(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QuestionRequestForm questionRequestForm,
			@RequestParam(value = "file", required = false) CommonsMultipartFile[] files) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		Boolean f = false;
		try {
			Integer loginId = questionRequestForm.getLoginId();
			User he = userService.findUserById(loginId);
			RollingPlan rp = null;
			if (null == loginId || loginId.intValue() == 0) {
				result.setCode("-1001");
				result.setMessage("Error/s occurred, loginId is not available");
				return result;
			}
			// 判断滚动计划非空
			if (null == questionRequestForm.getRollingPlanId()) {
				result.setCode("-1001");
				result.setMessage("Error/s occurred, rollingPlanId is not available");
				return result;
			} else {
				rp = rollingPlanService.findById(questionRequestForm.getRollingPlanId());
				if (rp == null) {
					result.setCode("-1001");
					result.setMessage("Error/s occurred, work step id is not exists");
					return result;
				}
			}
			if (!CommonUtility.isNonEmpty(questionRequestForm.getQuestionType().toString())) {
				result.setCode("-1001");
				result.setMessage("Error/s occurred, questionType is not available");
				return result;
			}
			if (!CommonUtility.isNonEmpty(questionRequestForm.getDescribe())) {
				result.setCode("-1001");
				result.setMessage("Error/s occurred, describe is not available");
				return result;
			}
			Integer monitorId = rollingPlanService.getConsmoniterIdBy(rp, null, null, null, null, null);
			// 创建问题
			RollingQuestion ques = new RollingQuestion();
			ques.setRollingPlanId(questionRequestForm.getRollingPlanId());
			ques.setQuestionType(questionRequestForm.getQuestionType().toString());
			ques.setDescribe(questionRequestForm.getDescribe());
			ques.setStatus(QuestionStatusDbEnum.pre.toString());
			ques.setOwner(he);
			ques.setQuestionTime(Calendar.getInstance().getTime());

			this.rollingQuestionService.add(ques);
			log.info("问题已经创建 id=" + ques.getId());
			if (ques.getId() == 0) {
				log.error("创建问题后id没有，需要检查");
				return null;
			}

			if (files != null && files.length > 0) {
				// 上传附件
				if (files != null && !moreUpload(ques.getId(), files, request)) {
					log.info("开始上传" + files);
					result.setCode("-1002");
					result.setMessage("upload files error!");
					return result;

				}
			}
			// 入处理人表，将班长取出入到处理人表中 //组长的上级就是班长
			RollingQuestionSolver solver = new RollingQuestionSolver();
			solver.setQuestion(ques);
			solver.setSolver(monitorId);
			solver.setSolverRole(1);
			solver.setStatus(QuestionStatusDbEnum.pre.toString());
			RollingQuestionProcess process = new RollingQuestionProcess();
			BeanUtils.copyProperties(solver, process);
			this.rollingQuestionProcessService.add(process);// 保存处理过程
			this.rollingQuestionSolverService.add(solver);

			if (solver.getId() == 0) {
				result.setCode("-1002");
				result.setMessage("create question solver error!");
				return result;
			}

			// 修改滚动计划状态
			rp.setDoissuedate(new Date());
			rp.setRollingplanflag(RollingPlanFlag.PROBLEM);

			result = executePushToEnpowerServiceImpl.updateEnpowerStatus(rp, null, "");

			rollingPlanService.update(rp);

			// 维班长 推送信息
			if (null != monitorId) {
				String message = he.getRealname() + " 创建了作业问题，请及时处理。";
				JPushExtra extra = new JPushExtra();
				extra.setCategory(JPushCategoryEnums.QUESTION_CREATE.name());
				jPushService.pushByUserId(extra, message, "作业问题创建", monitorId);
			}
			f = true;
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:" + e.getMessage());
			result.setResponseResult(false);
			return result;
		}

		// 问题创建完返回状态
		result.setResponseResult(f);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "teamList", method = RequestMethod.GET)
	@ApiOperation(value = "组长问题分页列表", notes = "用于组长查询问题列表")
	public JsonResult<RollingQuestionPageDataResult> teamList(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QuestionListRequestForm_ZZ questionRequestForm,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type) {

		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.info("组长问题分页列表");
		// 入参验证
		if (questionRequestForm == null) {
			result.setCode("-2001");
			result.setMessage("入参为空！！");
			return result;
		}
		if (questionRequestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (questionRequestForm == null || questionRequestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != questionRequestForm.getPagenum() && null != questionRequestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(questionRequestForm.getPagenum());
			page.setPagesize(questionRequestForm.getPagesize());
			// teamList
			RollingQuestionPageDataResult problemPageDataResult = questionApiService.getQuestionPageDataResult(page,
					questionRequestForm.getLoginId(), null, questionRequestForm.getQuestionStatus().toString(), type,
					questionRequestForm.getKeyword(),roleId);

			log.debug("查询出问题的条数：" + problemPageDataResult.getData().size() + "");
			result.setResponseResult(problemPageDataResult);
			return result;
		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}

	@ResponseBody
	@RequestMapping(value = "monitorList", method = RequestMethod.GET)
	@ApiOperation(value = "班长问题分页列表", notes = "用于班长查询问题列表")
	public JsonResult<RollingQuestionPageDataResult> monitorList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("requestForm") QuestionListRequestForm_BZ questionRequestForm,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = true, name = "userId", value = "问题提出人id") @RequestParam(value = "userId", required = true) Integer userId) {

		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.info("班长问题分页列表");
		// 入参验证
		if (questionRequestForm == null) {
			result.setCode("-2001");
			result.setMessage("入参为空！！");
			return result;
		}
		if (questionRequestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (questionRequestForm == null || questionRequestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != questionRequestForm.getPagenum() && null != questionRequestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(questionRequestForm.getPagenum());
			page.setPagesize(questionRequestForm.getPagesize());
			// 班长查询列表
			RollingQuestionPageDataResult questionPageDataResult = questionApiService.getQuestionPageDataResult(page,
					questionRequestForm.getLoginId(), userId, questionRequestForm.getQuestionStatus().toString(), type,
					questionRequestForm.getKeyword(),roleId);
			result.setResponseResult(questionPageDataResult);
			return result;

		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}

	@ResponseBody
	@RequestMapping(value = "coordinateList", method = RequestMethod.GET)
	@ApiOperation(value = "协调问题分页列表", notes = "用于班长查询问题列表")
	public JsonResult<RollingQuestionPageDataResult> coordinateList(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("requestForm") QuestionListRequestForm_Coordinate questionRequestForm,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = false, name = "userId", value = "问题提出人id") @RequestParam(value = "userId", required = false) Integer userId) {

		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.info("协调问题分页列表");
		// 入参验证
		if (questionRequestForm == null) {
			result.setCode("-2001");
			result.setMessage("入参为空！！");
			return result;
		}
		if (questionRequestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (questionRequestForm == null || questionRequestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("协调查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != questionRequestForm.getPagenum() && null != questionRequestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(questionRequestForm.getPagenum());
			page.setPagesize(questionRequestForm.getPagesize());
			// 协调查询列表
			RollingQuestionPageDataResult questionPageDataResult = questionApiService.getQuestionPageDataResult(page,
					questionRequestForm.getLoginId(), userId, questionRequestForm.getQuestionStatus().toString(), type,
					questionRequestForm.getKeyword(),roleId);
			result.setResponseResult(questionPageDataResult);
			return result;

		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("协调分页信息有缺失.");
			return result;
		}
	}

	@ResponseBody
	@RequestMapping(value = "captainList", method = RequestMethod.GET)
	@ApiOperation(value = "队长问题分页列表", notes = "用于队长查询问题列表")
	public JsonResult<RollingQuestionPageDataResult> captainList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("requestForm") QuestionListRequestForm_DZ questionRequestForm,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = true, name = "userId", value = "问题提出人id") @RequestParam(value = "userId", required = true) Integer userId) {

		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.info("队长问题分页列表");
		// 入参验证
		if (questionRequestForm == null) {
			result.setCode("-2001");
			result.setMessage("入参为空！！");
			return result;
		}
		if (questionRequestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (questionRequestForm == null || questionRequestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != questionRequestForm.getPagenum() && null != questionRequestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(questionRequestForm.getPagenum());
			page.setPagesize(questionRequestForm.getPagesize());
			// 队长 查询列表
			RollingQuestionPageDataResult questionPageDataResult = questionApiService.getQuestionPageDataResult(page,
					questionRequestForm.getLoginId(), userId, questionRequestForm.getQuestionStatus().toString(), type,
					questionRequestForm.getKeyword(),roleId);
			result.setResponseResult(questionPageDataResult);
			return result;

		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}

	@ResponseBody
	@RequestMapping(value = " technicianList", method = RequestMethod.GET)
	@ApiOperation(value = "技术员问题分页列表", notes = "用于技术员查询问题列表,或者技术主管查某个技术userId的列表")
	public JsonResult<RollingQuestionPageDataResult> technicianList(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("requestForm") QuestionListRequestForm_JS questionRequestForm,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = false, name = "userId", value = "某技术userId") @RequestParam(value = "userId", required = false) Integer userId) {

		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.info("技术员问题分页列表");
		// 入参验证
		if (questionRequestForm == null) {
			result.setCode("-2001");
			result.setMessage("入参为空！！");
			return result;
		}
		if (questionRequestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (questionRequestForm == null || questionRequestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != questionRequestForm.getPagenum() && null != questionRequestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(questionRequestForm.getPagenum());
			page.setPagesize(questionRequestForm.getPagesize());
			RollingQuestionPageDataResult questionPageDataResult = null;

			if (userId != null) {// 当技术主管查询某个技术人员的问题列表时
				questionPageDataResult = questionApiService.getQuestionPageDataResult(page, userId, null,
						questionRequestForm.getQuestionStatus().toString(), type, questionRequestForm.getKeyword(),roleId);
			} else {
				questionPageDataResult = questionApiService.getQuestionPageDataResult(page,
						questionRequestForm.getLoginId(), null, questionRequestForm.getQuestionStatus().toString(),
						type, questionRequestForm.getKeyword(),roleId);
			}
			result.setResponseResult(questionPageDataResult);
			return result;

		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}

	// @ResponseBody
	// @RequestMapping(value = "assignUI", method = RequestMethod.GET)
	// @ApiOperation(value = "班长指派页面显示（包括了技术人员列表）", notes = "用于班长指派页面显示")
	// public JsonResult<RollingQuestionAssignResult>
	// assignUI(HttpServletRequest request, HttpServletResponse response,
	// @RequestParam(value="questionId",required=true) Integer
	// questionId,@ModelAttribute("requestForm") BaseRequestForm
	// baseRequestForm){
	//
	// JsonResult<RollingQuestionAssignResult> result = new
	// JsonResult<RollingQuestionAssignResult>();
	// QuestionTransForResult trans = new QuestionTransForResult();
	//
	//// 查问题主表
	// RollingQuestion ques = rollingQuestionService.findById(questionId);
	//
	//// 查附件表
	// List<ProblemFile> files =
	// problemFileService.findFilesByProblemId(questionId);
	//
	// List<ProblemFileResult> bFiles = trans.problemFile(files);
	//
	// RollingPlan rollingPlan =
	// rollingPlanService.findById(ques.getRollingPlanId());
	//
	// RollingQuestionAssignResult aQues = questionApiService.transferTo2(ques);
	// aQues.setRollingPlan(rollingPlanApiService.rollingPlanTransferToSixLevelRollingPlan(rollingPlan));
	//
	// aQues.setFiles(bFiles);
	//
	// //查询技术管理中心下的所有人员是与需求不符的
	//// userService.findUserByDepartmentVariable("departmentId","technologyManagement");
	// //根据滚动计划类型 及字符串 solver 查询 人员列表
	//
	// List<User> userList =
	// userService.findUserByDepartmentVariable("roleType",
	// "solver",rollingPlan.getType());
	//
	// //查询技术人员列表
	// aQues.setUserList(trans.user(userList));
	//
	// result.setResponseResult(aQues);
	//
	// return result ;
	// }

	@ResponseBody
	@RequestMapping(value = "feedbackUI", method = RequestMethod.GET)
	@ApiOperation(value = "技术人员回执问题、组长回答页面显示（不是班长指派的情况，详情页使用）", notes = "用于技术人员回执问题、组长回答页面显示")
	public JsonResult<RollingQuestionResult> feedbackUI(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "questionId", required = true) Integer questionId,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {
		JsonResult<RollingQuestionResult> result = new JsonResult<RollingQuestionResult>();

		try {
			Integer loginId = baseRequestForm.getLoginId();
			log.info("问题" + questionId + "的当前登录处理人=" + loginId);
			// 班长或协调人员时，查询人员列表出来
			boolean moniterCoordinateShowUserList = true;

			// 查问题主表
			RollingQuestion ques = rollingQuestionService.findById(questionId);

			// 查附件表
			RollingQuestionResult aQues = null;

			aQues = questionApiService.transferForList(ques, true, loginId);

			aQues.setRollingPlan(rollingPlanApiService
					.rollingPlanTransferToSixLevelRollingPlan(rollingPlanService.findById(aQues.getRollingPlanId())));

			// 根据当前登录人查出是哪种角色：1班长,2队部协，3技术，如果是班长时，显示协调人员列表；如果是协调时，显示处理人列表
			// TODO 打开详情页面。
			Integer solverRole = null;
			List<RollingQuestionSolver> oldSolver = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId,
					loginId);
			// if(oldSolver==null || oldSolver.size()==0 || oldSolver.size()>1){
			if (oldSolver == null || oldSolver.size() == 0) {
				moniterCoordinateShowUserList = false;
			} else {
				RollingQuestionSolver loginSolver = oldSolver.get(0);
				if (loginSolver == null) {
					throw new RuntimeException("问题" + questionId + "的当前登录处理人记录为空异常！");
				}
				solverRole = loginSolver.getSolverRole();
				if (solverRole == null || solverRole.intValue() == 0) {
					throw new RuntimeException("问题" + questionId + "的当前登录处理人记录为空0异常！");
				}
			}

			// 获取不能处理的原因
			List<RollingQuestionSolver> solvers = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId,
					loginId);
			List<RollingQuestionSolver> solver = solvers.stream().filter(s -> {
				return QuestionStatusDbEnum.unsolved.toString().equals(s.getStatus());
			}).collect(Collectors.toList());
			if (solver != null && solver.size() > 0) {
				aQues.setUnableReason(solver.get(0).getUnableReason());
			}

			// 查询技术人员列表
			boolean isSupervisor = false;
			User loginUser = userService.findUserById(loginId);
			List<Role> roles = loginUser.getRoles();
			Integer usingRoleId = baseRequestForm.getRoleId() ;
			if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
				roles = roleService.findListById(roles,usingRoleId);
			}
			for (Role role : roles) {
				Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
				if (roleNameList.contains(ConstantVar.supervisor)) {
					isSupervisor = true;
					break;
				}
			}

			String nextSolver = null;
			List<User> userList = null;
			RollingPlan rollingPlan = rollingPlanService.findById(ques.getRollingPlanId());
			QuestionTransForResult trans = new QuestionTransForResult();
			if (isSupervisor) {
				moniterCoordinateShowUserList = true;
				nextSolver = "solver";
				userList = userService.findUserByDepartmentVariable("roleType", nextSolver, rollingPlan.getType());
			}
			if (solverRole != null && solverRole.intValue() == 1) {
				nextSolver = "coordinationDepartment";
				// userList =
				// userService.findUserByDepartmentVariableAndModule("departmentId",
				// nextSolver,rollingPlan.getType());
				// 查询协调人员不分专业
				userList = userService.findUserByRoleAndDepartmentVariable("coordinator", nextSolver);
			} else if (solverRole != null && solverRole.intValue() == 2) {
				nextSolver = "solver";
				userList = userService.findUserByDepartmentVariable("roleType", nextSolver, rollingPlan.getType());
			} else {
				// 技术角色本身，可组长角色本身（组长本身不应该报错）
			}

			if (moniterCoordinateShowUserList) {
				aQues.setUserList(trans.user(userList));
			}

			result.setResponseResult(aQues);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "assign", method = RequestMethod.POST)
	@ApiOperation(value = "班长执行指派,协调执行指派,技术主管改派", notes = "用于执行班长指派,协调执行指派,技术主管改派,designatedUserId被指派人id，questionId问题id")
	public JsonResult<Boolean> assign(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "questionId", required = true) Integer questionId,
			@ApiParam(required = true, name = "designatedUserId", value = "被指派人id") @RequestParam(value = "designatedUserId", required = true) Integer designatedUserId,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {
		// 指派 是针对pre ??
		// 改派 是针对unsolved ??

		// 当技术主管对 一 （1 超时、2 未解决 ）unsolved 进行改派，改为 组长刚创建问题的主状态 pre，新的技术人员的子状态是pre
		// 此时，组长来看在 待解决，班长->未解决 新技术人员本人是 -> 待处理 ， 原技术人员->未能解决的问题。
		// 技术领导查列表按主表状态查询（即待处理）
		// ques.setStatus(QuestionStatusDbEnum.pre.toString());

		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User assign = userService.findUserById(designatedUserId);
		User logining = userService.findUserById(baseRequestForm.getLoginId());
		if (null == assign) {
			result.setCode("-1001");
			result.setMessage("Error/s occurred, user is not available");
			return result;
		}
		Boolean ret = false;
		try {

			RollingQuestion ques = rollingQuestionService.findById(questionId);

			Integer loginSolverRole = null;

			boolean isSupervisorAssgin = false;
			List<Role> roles = logining.getRoles();
			Integer usingRoleId = baseRequestForm.getRoleId() ;
			if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
				roles = roleService.findListById(roles,usingRoleId);
			}
			for (Role role : roles) {
				Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
				if (roleNameList.contains(ConstantVar.supervisor)) {
					isSupervisorAssgin = true;
					break;
				}
			}

			// 将自已的解决状态由待指派变为”done已指派“，
			List<RollingQuestionSolver> allSolver = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId,
					baseRequestForm.getLoginId());
			List<RollingQuestionSolver> loginSolvers = allSolver.stream().filter(s -> {
				return QuestionStatusDbEnum.pre.toString().equals(s.getStatus());
			}).collect(Collectors.toList());
			if (loginSolvers == null || loginSolvers.size() == 0) {
				if (isSupervisorAssgin) {
					// 如果是技术主管改派，要将前一个技术人员的记录修改为未能解决的 状态
					rollingQuestionSolverService.setOlderUnsolved(ques);
					loginSolverRole = 4;
				} else {
					log.error("班长执行指派存在数据异常：questionId=" + questionId);
					throw new RuntimeException("问题执行指派，你不是当前待处理人员异常！");
				}
			} else {
				// 最基本的需求：班长指派技术人员后，班长记录标记为已指派
				// RollingQuestionSolver loginSolver = allSolver.get(0);

				RollingQuestionSolver loginSolver = loginSolvers.get(0);
				loginSolverRole = loginSolver.getSolverRole();
				loginSolver.setStatus(QuestionStatusDbEnum.assign.toString());
				loginSolver.setSolverTime(Calendar.getInstance().getTime());
				rollingQuestionSolverService.update(loginSolver);
				// 同步将过程表的状态修改
				rollingQuestionProcessService.updateStatusByQuestionIdAndSolv(questionId,
						QuestionStatusDbEnum.assign.toString(), baseRequestForm.getLoginId());
			}

			// 为技术人员添加一条解决记录，状态为“pre待处理” --追加逻辑，如果原处理人被再次改派上，则修改原记录
			// 查询原处理人记录
			List<RollingQuestionSolver> oldSolver = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId,
					designatedUserId);
			if (oldSolver != null && oldSolver.size() > 0) {
				// 正常的应该是只有一条 下面另建表后，尝试可以多次处理。
				oldSolver.stream().forEach(bn -> {
					bn.setStatus(QuestionStatusDbEnum.pre.toString());

					RollingQuestionProcess process = new RollingQuestionProcess();
					BeanUtils.copyProperties(bn, process);
					this.rollingQuestionProcessService.add(process);// 保存处理过程

					this.rollingQuestionSolverService.add(bn);
				});

				// throw new RuntimeException(assign.getRealname()+
				// "已经处理过一次本问题，请指定其它人员！");
			} else {
				RollingQuestionSolver solver2 = new RollingQuestionSolver();
				solver2.setQuestion(ques);
				solver2.setSolver(designatedUserId);
				if (loginSolverRole == null) {
					throw new RuntimeException("问题" + questionId + "无人员处理数据存在异常！");
				} else if (loginSolverRole.intValue() == 1) {// 当前登录人员是班长时,指派给协调
					solver2.setSolverRole(2);
					ques.setCoordinate(assign);
				} else if (loginSolverRole.intValue() == 2 || loginSolverRole.intValue() == 4) {// 当协调人员登录时，指派给技术工程师
					solver2.setSolverRole(3);
					ques.setAssign(assign);
				} else {
					throw new RuntimeException("问题" + questionId + "数据存在异常！");
				}
				solver2.setStatus(QuestionStatusDbEnum.pre.toString());

				RollingQuestionProcess process = new RollingQuestionProcess();
				BeanUtils.copyProperties(solver2, process);
				this.rollingQuestionProcessService.add(process);// 保存处理过程
				this.rollingQuestionSolverService.add(solver2);
			}

			// 组长指派前，主状态是创建时的状态 pre，保持pre状态,

			// 技术主管改派前，主状态是unsolved 改为pre状态
			if (QuestionStatusDbEnum.unsolved.toString().equals(ques.getStatus())) {
				ques.setStatus(QuestionStatusDbEnum.pre.toString());
			}
			rollingQuestionService.add(ques);
			// 推送信息
			String message = logining.getRealname() + " 指派了作业问题，请及时处理。";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.QUESTION_ASSIGN.name());
			jPushService.pushByUserId(extra, message, "作业问题指派", designatedUserId);

			ret = true;
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		// TODO 班长执行指派

		result.setResponseResult(ret);

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "feedback", method = RequestMethod.POST)
	@ApiOperation(value = "技术人员问题回执", notes = "用于技术人员问题回执")
	public JsonResult<Boolean> feedback(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") QuestionFeedbackRequestForm questionRequestForm,
			@RequestParam(value = "file", required = false) CommonsMultipartFile[] files) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		Boolean f = false;
		int questionId = questionRequestForm.getQuestionId();
		log.info("questionId" + questionId);
		int loginId = questionRequestForm.getLoginId();
		User logining = userService.findUserById(loginId);
		try {

			// 所需入参：问题id,问题反馈，附件
			RollingQuestion up = rollingQuestionService.findById(questionRequestForm.getQuestionId());
			// 将回执信息入问题表子记录
			// 入附件
			RollingQuestion ques = new RollingQuestion();
			ques.setRollingPlanId(up.getRollingPlanId());
			ques.setQuestionType(up.getQuestionType());
			ques.setDescribe(questionRequestForm.getDescribe());
			ques.setStatus(QuestionStatusDbEnum.pre.toString());
			ques.setOwner(userService.findUserById(questionRequestForm.getLoginId()));
			ques.setQuestionTime(Calendar.getInstance().getTime());
			ques.setUp(up);

			// 修改自已的解决状态
			List<RollingQuestionSolver> allSolvers = rollingQuestionSolverService.findByQuestionIdAndUserId(questionId,
					loginId);
			// 查询那条待处理数据
			List<RollingQuestionSolver> allSolver = allSolvers.stream().filter(q -> {
				return QuestionStatusDbEnum.pre.toString().equals(q.getStatus());
			}).collect(Collectors.toList());

			// 20180127
			// 当问题有人待办的时候，其它非待办的人进行了回复处理，系统报“问题回执人异常”，现在将这个条件放在，保留原有的待办，并且为非待办的人（现在是班长）录入一条新的处理数据。
			// 并置为已回复reply，而且将主问题状态置为待确认。原来的待办人数据不动，原来的待办人应该可以继续回复。
			// 如果作业组长接受方案时，将主问题置为解决，原来的待办人数据怎么办呢？删除还是修改为未解决呢？

			if (allSolver == null || allSolver.size() == 0 || allSolver.size() > 1) {
				log.error("技术人员问题回执存在数据异常：questionId=" + questionId);
				result.setCode("-1002");
				String prePerson = rollingQuestionSolverService.findPreSolverNameById(questionId);
				result.setMessage("该问题已经提交给【" + prePerson + "】，你不能处理!");// 问题回执人员异常!
				return result;
			} else {
				RollingQuestionSolver loginSolver = allSolver.get(0);
				loginSolver.setStatus(QuestionStatusDbEnum.reply.toString());
				loginSolver.setSolverTime(Calendar.getInstance().getTime());
				rollingQuestionSolverService.update(loginSolver);
				// 同步将过程表的状态修改
				rollingQuestionProcessService.updateStatusByQuestionIdAndSolv(questionId,
						QuestionStatusDbEnum.reply.toString(), loginId);
				// 班长或协调人员直接回执问题，则将指派人设置为自己
				if (loginSolver.getSolverRole() == 1 || loginSolver.getSolverRole() == 2) {
					up.setAssign(logining);
				}
			}

			this.rollingQuestionService.add(ques);
			log.info("问题已经创建 id=" + ques.getId());
			if (ques.getId() == 0) {
				log.error("创建问题后id没有，需要检查");
				return null;
			}

			// 上传附件
			if (files != null && files.length > 0 && !moreUpload(ques.getId(), files, request)) {
				log.info("开始上传" + files);
				result.setCode("-1002");
				result.setMessage("upload files error!");
				result.setResponseResult(f);
				return result;

			}

			// 修改问题表状态 为待确认
			up.setStatus(QuestionStatusDbEnum.undo.toString());
			this.rollingQuestionService.update(up);

			if (null != logining) {
				String message = logining.getRealname() + " 回执了作业问题，请及时处理。";
				JPushExtra extra = new JPushExtra();
				extra.setCategory(JPushCategoryEnums.QUESTION_FEEDBACK.name());
				jPushService.pushByUserId(extra, message, "作业问题回执", up.getOwner().getId());
			}
			f = true;
		} catch (Exception e) {
			e.printStackTrace();
			f = false;
		}

		result.setResponseResult(f);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "answer", method = RequestMethod.POST)
	@ApiOperation(value = "组长回答问题是否解决", notes = "用于组长回答问题是否解决,designatedUserId被指派人id，questionId问题id")
	public JsonResult<Boolean> answer(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "questionId", value = "问题id,主记录id") @RequestParam(value = "questionId", required = true) Integer questionId,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm,
			@ApiParam(required = true, name = "answer", value = "回答解决还是未解决") @RequestParam(value = "answer", required = true) QuestionAnswerStatusDbEnum answer,
			@ApiParam(required = false, name = "reason", value = "不通过理由") @RequestParam(value = "reason", required = false) String reason,
			@RequestParam(value = "file", required = false) CommonsMultipartFile[] files) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try {
			// 修改主表的状态
			Boolean ret = rollingQuestionService.answer(questionId, answer, reason, files, request);
			result.setResponseResult(ret);
		} catch (Exception e) {
			// e.printStackTrace();
			result.setCode("-1000");
			result.setMessage("操作异常:" + e.getMessage());
			result.setResponseResult(false);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return result;
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "timeDelay", method = RequestMethod.POST)
	@ApiOperation(value = "技术主管对问题延时，单位小时", notes = "技术主管对问题延时，单位小时,designatedUserId被指派人id，questionId问题id")
	public JsonResult<Boolean> timeDelay(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "questionId", value = "问题标识id") @RequestParam(value = "questionId", required = true) Integer questionId,
			@ApiParam(required = true, name = "designatedUserId", value = "被指派人id,即assignUI接口designee中的id") @RequestParam(value = "designatedUserId", required = true) Integer designatedUserId,
			@ApiParam(required = true, name = "delayHour", value = "延时小时数") @RequestParam(value = "delayHour", required = true) Integer delayHour,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {
		// 延时接口提供入参：问题标识、designatedUserId、延时小时数。登录人
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User assign = userService.findUserById(designatedUserId);
		if (null == assign) {
			result.setCode("-1001");
			result.setMessage("Error/s occurred, user is not available");
			return result;
		}
		Boolean ret = false;
		try {

			ret = rollingQuestionSolverService.setTimeoutForSolver(questionId, assign, delayHour);
		} catch (Exception e) {
			e.printStackTrace();
		}
		result.setResponseResult(ret);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "unable", method = RequestMethod.POST)
	@ApiOperation(value = "无法解决问题", notes = "用于问题处理人无法处理当前问题时调用")
	public JsonResult<Boolean> unable(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "questionId", value = "问题标识id") @RequestParam(value = "questionId", required = true) Integer questionId,
			@ApiParam(required = false, name = "reason", value = "无法解决理由") @RequestParam(value = "reason", required = false) String reason,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {
		log.info("用户" + baseRequestForm.getLoginId() + "无法解决问题：" + questionId);
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		if (baseRequestForm.getLoginId() == null) {
			result.setCode("-1001");
			result.setMessage("Error/s occurred, loginID is not available");
			return result;
		}

		try {

			List<Integer> preId = rollingQuestionSolverService.findPreSolverIdById(questionId);
			if (preId == null || preId.size() == 0 || !preId.contains(baseRequestForm.getLoginId())) {
				throw new RuntimeException("问题已经被指派给其它人处理！！" + questionId);
			}

			status = rollingQuestionService.unable(questionId, reason, baseRequestForm);
		} catch (Exception e) {
			result.setCode("-1000");
			result.setMessage(e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		if (!status) {
			result.setCode("-1001");
			result.setMessage("执行操作失败！");
		}
		result.setResponseResult(status);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "statistics", method = RequestMethod.GET)
	@ApiOperation(value = "我的问题统计", notes = "用于统计我的已反馈、未解决、已解决的问题")
	public JsonResult<RollingQuestionStatisticsResult> statistics(HttpServletRequest request,
			HttpServletResponse response,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@ModelAttribute("requestForm") BaseRequestForm baseRequestForm) {
		JsonResult<RollingQuestionStatisticsResult> result = new JsonResult<RollingQuestionStatisticsResult>();
		log.debug("LoginId: " + baseRequestForm.getLoginId());
		RollingQuestionStatisticsResult r = new RollingQuestionStatisticsResult();

		r.setReply(rollingQuestionService.getStatistics(type, baseRequestForm.getLoginId(),
				QuestionStatusDbEnum.reply.toString()));
		r.setUnsolved(rollingQuestionService.getStatistics(type, baseRequestForm.getLoginId(),
				QuestionStatusDbEnum.unsolved.toString(), QuestionStatusDbEnum.pre.toString()));
		r.setSolved(rollingQuestionService.getStatistics(type, baseRequestForm.getLoginId(),
				QuestionStatusDbEnum.solved.toString()));

		result.setResponseResult(r);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "myQuestionList", method = RequestMethod.GET)
	@ApiOperation(value = "我的问题分页列表", notes = "用于班长查询我的问题分页列表")
	public JsonResult<RollingQuestionPageDataResult> myQuestion(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("requestForm") QuestionListRequestForm_MY requestForm,
			@ApiParam(required = true, name = "type", value = "滚动计划的类型") @RequestParam(value = "type", required = true) String type,
			@RequestParam(required = false) @ApiParam(value = "登录用户的角色ID") Integer roleId,
			@ApiParam(required = false, name = "userId", value = "userId") @RequestParam(value = "userId", required = false) Integer userId) {
		JsonResult<RollingQuestionPageDataResult> result = new JsonResult<RollingQuestionPageDataResult>();
		log.debug("我的问题分页列表,LoginId: " + requestForm.getLoginId());

		// 入参验证
		if (requestForm.getLoginId() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题时，登录人id为空。");
			return result;
		}

		if (requestForm == null || requestForm.getQuestionStatus() == null) {
			result.setCode("-2001");
			result.setMessage("查询问题状态为空。查询哪个状态的问题必须填写");
			return result;
		}

		if (null != requestForm.getPagenum() && null != requestForm.getPagesize()) {
			Page<RollingQuestion> page = new Page<RollingQuestion>();

			page.setPageNum(requestForm.getPagenum());
			page.setPagesize(requestForm.getPagesize());
			RollingQuestionPageDataResult questionPageDataResult = null;

			questionPageDataResult = questionApiService.getQuestionPageDataResult(page, requestForm.getLoginId(), null,
					requestForm.getQuestionStatus().toString(), type, requestForm.getKeyword(),roleId);

			result.setResponseResult(questionPageDataResult);
			return result;

		} else {
			log.debug("分页信息有缺失");
			result.setCode("-1002");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}

	// 以下是被上面的入口方法调用的方法
	private boolean moreUpload(Integer problemId, CommonsMultipartFile files[], HttpServletRequest request) {
		// 上传位置 设定文件保存的目录
		String filePath = com.easycms.common.logic.context.ContextPath.fileBasePath
				+ com.easycms.common.util.FileUtils.questionFilePath;
		List<FileInfo> fileInfos = UploadUtilToday.moreUpload(filePath, "utf-8", true, files,true);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				ProblemFile file = new ProblemFile();
				file.setPath(fileInfo.getNewFilename());
				file.setTime(Calendar.getInstance().getTime());
				file.setProblemid(problemId);
				if (fileInfo.getFilename() != null && fileInfo.getFilename().length() > 50) {
					file.setFileName(fileInfo.getFilename().substring(0, 50));
				} else {
					file.setFileName(fileInfo.getFilename());
				}
				log.debug("upload file success : " + file.toString());
				problemFileService.add(file);
			}
		}

		return true;
	}

	private boolean upload(Integer problemId, HttpServletRequest request) {
		String filePath = com.easycms.common.util.FileUtils.questionFilePath;
		List<FileInfo> fileInfos = UploadUtilToday.upload(filePath, "utf-8", true, request,true);
		if (fileInfos != null && fileInfos.size() > 0) {
			for (FileInfo fileInfo : fileInfos) {
				ProblemFile file = new ProblemFile();
				file.setPath(fileInfo.getNewFilename());
				file.setTime(Calendar.getInstance().getTime());
				file.setProblemid(problemId);
				file.setFileName(fileInfo.getFilename());
				log.debug("upload file success : " + file.toString());
				problemFileService.add(file);
			}
		}

		return true;
		// WebUtility.writeToClient(response, "true");
	}

}
