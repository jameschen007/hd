package com.easycms.hd.api.baseservice;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.request.WitnessAssignForm;
import com.easycms.hd.api.request.WitnessSubmitForm;
import com.easycms.hd.api.request.WorkStepWitnessResultForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.hd.plan.enums.NoticePointType;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.util.PlanUtil;
import com.easycms.hd.witness.domain.WitnessFile;
import com.easycms.hd.witness.service.WitnessFileService;
import com.easycms.hd.witness.service.WitnessFlagService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/witness_op")
@Api(value = "WitnessOperationAPIController", description = "见证操作相关的api")
@Slf4j
@javax.transaction.Transactional
public class WitnessOperationAPIController {

	public static Logger logger = Logger.getLogger(WitnessOperationAPIController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private WitnessApiService witnessApiService;
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private RoleApiService roleApiService;
	@Autowired
	private WitnessFlagService witnessFlagService;
	@Autowired
	private WitnessFileService witnessFileService;
	@Autowired
	private JPushService jPushService;
	@Autowired
	private RollingPlanService rollingPlanService;

	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;

	@Value("#{APP_SETTING['file_witness_path']}")
	private String fileWitnessPath;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(value = "见证组长或QC2分派见证", notes = "见证组长分派见证到相应的见证员,QC2(或福清QC1)分派见证到CZEC QC, CZEC QA, PAEC")
	public JsonResult<Boolean> witnessAssign(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") WitnessAssignForm form) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();

		User loginUser = userService.findUserById(form.getLoginId());
		boolean assignSelf = form.getLoginId().intValue() == form.getMemberId().intValue();// 分派给自已
		Set<AuthRole> loginRoles = roleApiService.getAuthRole(loginUser, false);
		if (null == loginRoles) {
			result.setCode("-1002");
			result.setMessage("当前登录没有找到任何职位信息.");
			return result;
		}

		Set<String> loginRoleType = new HashSet<String>();
		for (AuthRole role : loginRoles) {
			loginRoleType.addAll(role.getRoleType());
		}

		if (null == form.getIds() || form.getIds().isEmpty()) {
			result.setCode("-1002");
			result.setMessage("待分派的ID号为必填项.");
			return result;
		}
		if (null == form.getMemberId()) {
			result.setCode("-1002");
			result.setMessage("见证组组员的id号是必填项.");
			return result;
		}
		User user = userService.findUserById(form.getMemberId());
		if (null == user) {
			result.setCode("-1002");
			result.setMessage("未找到见证组组员的信息.");
			return result;
		}
		boolean memberIsQc = roleApiService.checkRole(user, ConstantVar.witness_member_qc1,
				ConstantVar.witness_member_qc2);// 被分派人是QC组员角色

		// if (null == user.getParent() ||
		// !user.getParent().getId().equals(form.getLoginId())) {
		// result.setCode("-1002");
		// result.setMessage("当前登录用户与指定见证员不存在上下级关系。");
		// return result;
		// }

		Set<AuthRole> roles = roleApiService.getAuthRole(user, false);
		if (null == roles) {
			result.setCode("-1002");
			result.setMessage("当前给定的见证组员没有找到任何职位信息.");
			return result;
		}

		Set<String> roleType = new HashSet<String>();
		for (AuthRole role : roles) {
			roleType.addAll(role.getRoleType());
		}

		if (null == roleType || roleType.isEmpty()) {
			result.setCode("-1002");
			result.setMessage("当前给定的见证组组员没有任何权限.");
			return result;
		}

		// 分派外部单位
		if (ComputedConstantVar.qc2OrFqQc1(loginRoleType) && !assignSelf && !memberIsQc) {// 福清的，一个人即是QC１组长，又是QC１组员，还分派给自已，你说这个怎么个弄法？

			logger.debug(
					ContextPath.qc1ReplaceQc2 ? "当前用户角色包含“QC1组员”" + loginRoleType : "当前用户角色包含“QC2组员”" + loginRoleType);
			for (Integer id : form.getIds()) {
				WorkStepWitness workStepWitness = witnessService.findById(id);
				if (null != workStepWitness) {
					if (null != workStepWitness.getWitness()) {
						if (!workStepWitness.getWitness().equals(loginUser.getId())) {
							log.info("不是当前登录用户的条目，略过。");
							continue;
						}
					}
					if (null != workStepWitness.getWitnesser()) {
						log.info("已经被提交,略过此条记录.");
						continue;
					}

					if (workStepWitness.getNoticePoint().equals(NoticePointType.CZEC_QA.name())) {
						if (!roleType.contains("witness_member_czecqa")) {
							result.setCode("-1002");
							result.setMessage("指定[" + user.getId() + "]见证员不匹配的CZEC_QA见证点类型");
							return result;
						}
					}
					if (workStepWitness.getNoticePoint().equals(NoticePointType.CZEC_QC.name())) {
						if (!roleType.contains("witness_member_czecqc")) {
							result.setCode("-1002");
							result.setMessage("指定[" + user.getId() + "]见证员不匹配的CZEC_QC见证点类型");
							return result;
						}
					}
					if (workStepWitness.getNoticePoint().equals(NoticePointType.PAEC.name())) {
						if (!roleType.contains("witness_member_paec")) {
							result.setCode("-1002");
							result.setMessage("指定[" + user.getId() + "]见证员不匹配的PAEC见证点类型");
							return result;
						}
					}
				}
			}

			result.setResponseResult(witnessApiService.assignToWitnesserQC2(form.getIds().toArray(new Integer[] {}),
					form.getLoginId(), form.getMemberId()));

			String message = loginUser.getRealname() + " 将[" + form.getIds().size() + "]条见证分派给了您。";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_ASSIGN.name());
			jPushService.pushByUserId(extra, message, "见证分派", form.getMemberId());
		} else if (loginRoleType.contains("witness_team_qc1") || loginRoleType.contains("witness_team_qc2")) {
			logger.debug("当前用户角色包含QC1组长或QC2组长" + loginRoleType);
			for (Integer id : form.getIds()) {
				WorkStepWitness workStepWitness = witnessService.findById(id);
				if (null != workStepWitness) {
					if (null != workStepWitness.getWitness()) {
						log.info("已经被提交,略过此条记录.");
						continue;
					}
					if (workStepWitness.getNoticePoint().equals(NoticePointType.QC1.name())) {
						if (!roleType.contains("witness_member_qc1")) {
							result.setCode("-1002");
							result.setMessage("指定[" + user.getId() + "]见证员不匹配的QC1见证点类型");
							return result;
						}
					}
					if (workStepWitness.getNoticePoint().equals(NoticePointType.QC2.name())) {
						if (!roleType.contains("witness_member_qc2")) {
							result.setCode("-1002");
							result.setMessage("指定[" + user.getId() + "]见证员不匹配的QC2见证点类型");
							return result;
						}
					}
				}
			}

			result.setResponseResult(witnessApiService.assignToWitnessMember(form.getIds().toArray(new Integer[] {}),
					form.getLoginId(), form.getMemberId()));

			String message = loginUser.getRealname() + " 将[" + form.getIds().size() + "]条见证分派给了您。";
			JPushExtra extra = new JPushExtra();
			extra.setCategory(JPushCategoryEnums.WITNESS_ASSIGN.name());
			jPushService.pushByUserId(extra, message, "见证分派", form.getMemberId());
		} else {
			result.setCode("-1002");
			result.setMessage("登录用户不具备分派见证的权限。");
			return result;
		}
		result.setMessage("成功分派见证");
		return result;
	}

	/**
	 * 见证结果的填写
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/result", method = RequestMethod.POST)
	@ApiOperation(value = "填写见证结果", notes = "用于见证组员填写见证结果，所填写的结果将影响到工序状态")
	public JsonResult<Boolean> result(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") WorkStepWitnessResultForm form) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean loginRoleTypeQC1 = false;
		try {

			User loginUser = userService.findUserById(form.getLoginId());
			Set<AuthRole> loginRoles = roleApiService.getAuthRole(loginUser, false);
			if (null == loginRoles) {
				result.setCode("-1002");
				result.setMessage("当前登录没有找到任何职位信息.");
				return result;
			}

			Set<String> loginRoleType = new HashSet<String>();
			for (AuthRole role : loginRoles) {
				loginRoleType.addAll(role.getRoleType());
			}

			Integer isok = form.getIsok();
			List<Integer> isokList = new ArrayList<Integer>();
			isokList.add(1);
			isokList.add(3);

			if (!isokList.contains(isok)) {
				result.setCode("-1002");
				result.setMessage("见证结果只允许[1, 3]");
				return result;
			}

			WorkStepWitness wsw = witnessService.findById(form.getId());
			RollingPlan rollingPlan = null;
			if (null != wsw) {
				WorkStep workStep = wsw.getWorkStep();
				if (null != workStep) {
					rollingPlan = workStep.getRollingPlan();
					if (null != rollingPlan) {
						if (CommonUtility.isNonEmpty(rollingPlan.getRollingplanflag())
								&& rollingPlan.getRollingplanflag().equals("PROBLEM")) {
							result.setCode("-1002");
							result.setMessage("见证对应的滚动计划存在没有解决的问题。");
							return result;
						}
					} else {
						result.setCode("-1002");
						result.setMessage("见证对应的滚动计划不存在。");
						return result;
					}
				} else {
					result.setCode("-1002");
					result.setMessage("见证对应的工序不存在。");
					return result;
				}

				if (loginRoleType.contains("witness_member_czecqa") || loginRoleType.contains("witness_member_czecqc")
						|| loginRoleType.contains("witness_member_paec")) {
					if (null == wsw.getWitnesser() || !wsw.getWitnesser().equals(loginUser.getId())) {
						result.setCode("-1002");
						result.setMessage("这不是你的见证条目。");
						return result;
					}
				} else if (loginRoleType.contains("witness_member_qc1")
						|| loginRoleType.contains("witness_member_qc2")) {
					if (null == wsw.getWitness() || !wsw.getWitness().equals(loginUser.getId())) {
						result.setCode("-1002");
						result.setMessage("这不是你的见证条目。");
						return result;
					} else if (loginRoleType.contains("witness_member_qc1")) {
						loginRoleTypeQC1 = true;
					}
				} else {
					result.setCode("-1002");
					result.setMessage("当前登录用户不具备填写见证结果的权限。");
					return result;
				}

				if (3 == wsw.getIsok() || 2 == wsw.getIsok()) {
					result.setCode("-1002");
					result.setMessage("当前已经存在见证结果[" + wsw.getIsok() + "] , 且合格");
					return result;
				}
				// 不为空就是焊口条目
				boolean weldFlag = rollingPlan.getWeldno() != null && !"".equals(rollingPlan.getWeldno().trim());

				String realQty = form.getDosage();
				if (loginRoleTypeQC1 && form.getIsok() == 3) {// 只是QC１时才验证
					// 合格时，必须填写 实际完成工程量
					if (realQty == null || !realQty.matches("\\d+[.]*\\d*")) {
						result.setCode("-1002");
						result.setMessage("[实际完成工程量 必须填写" + rollingPlan.getId());
						return result;
					}
				}
				if (realQty != null && realQty.matches("\\d+[.]*\\d*")) {
					rollingPlan.setRealProjectcost(realQty);// 保存一下实际工程量
					this.rollingPlanService.add(rollingPlan);
				}

				if (PlanUtil.lastOneWorkStepWitnessQC1(wsw) && form.getIsok() == 3) {
					logger.info("材料用量：" + CommonUtility.toJson(form.getMateriaJsonlList()));
					// 材料回填
					JsonResult<Boolean> fillResult = executePushToEnpowerServiceImpl
							.materialBackfill(form.getMateriaJsonlList(), wsw.getWorkStep().getRollingPlan().getId());

					Boolean fillRes = fillResult.getResponseResult();
					if (fillRes == null || fillRes == false) {
						result.setCode("-1002");
						result.setMessage("当前为最后一条见证，如果选择合格，则用量为必填项。");
						return result;
					}
				} else {// 其它情况如果有写就保存了。
					JsonResult<Boolean> fillResult = executePushToEnpowerServiceImpl
							.materialBackfill(form.getMateriaJsonlList(), wsw.getWorkStep().getRollingPlan().getId());
				}

				// 上传附件
				// if (files!=null && !moreUpload(form.getId(), files,request))
				// {
				// log.info("开始上传"+files);
				// result.setCode("-1002");
				// result.setMessage("upload files error!");
				// return result;
				//
				// }
				String filePath = com.easycms.common.logic.context.ContextPath.fileBasePath + fileWitnessPath;
				List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
				if (fileInfos != null && fileInfos.size() > 0) {
					for (FileInfo fileInfo : fileInfos) {
						WitnessFile file = new WitnessFile();
						file.setPath(fileInfo.getNewFilename());
						file.setTime(new Date());
						file.setWitnessid(form.getId());
						String fileName = fileInfo.getFilename();
						if (fileName.length() > 90) {
							fileName = fileName.substring(0, 90);
						}
						file.setFileName(fileName);
						log.debug("upload file success : " + file.toString());
						witnessFileService.add(file);
					}
				}

				logger.info("见证结果：" + form.getWitnessdesc());
				logger.info("见证结果：" + form.getWitnessaddress());

				wsw.setIsok(form.getIsok());
				wsw.setNoticeresultdesc(form.getWitnessdesc());
				wsw.setUpdatedBy(loginUser.getId());
				wsw.setUpdatedOn(new Date());
				wsw.setDosage(form.getDosage());
				wsw.setRealwitnessaddress(form.getWitnessaddress());
				wsw.setRealwitnessdata(form.getWitnessdate());
				wsw.setFailType(form.getFailType());
				wsw.setRemark(form.getRemark());

				/**
				 * QC2见证组员可以代替别个填写见证结果，但需要记录被代替者是谁 TODO ?? wsw.setSubstitute
				 * 就没有保存到数据库中
				 */
				List<String> substitutePoints = new ArrayList<String>();
				substitutePoints.add("CZEC_QC");
				substitutePoints.add("CZEC_QA");
				substitutePoints.add("PAEC");
				if ((ComputedConstantVar.qc2OrFqQc1(loginRoleType))
						&& substitutePoints.contains(wsw.getNoticePoint())) {
					wsw.setSubstitute(form.getSubstitute());
				}

				// 材料保存

				// 开始处理前，调用Enpower接口推送消点
				ArrayList<WorkStepWitness> enpParam = new ArrayList<WorkStepWitness>();
				enpParam.add(wsw);
				executePushToEnpowerServiceImpl.disappearPoint(enpParam, null);

				wsw = witnessService.add(wsw);
				if (null != wsw) {
					String message = "通知： " + loginUser.getRealname() + " 填写了见证结果:" + form.getWitnessaddress();
					JPushExtra extra = new JPushExtra();
					extra.setCategory(JPushCategoryEnums.WITNESS_RESULT.name());
					List<Integer> userIdList = new ArrayList<Integer>();
					// 所有情况，QC1，Qc2都不会收到push消息
					if (null != rollingPlan.getConsteam()) {
						userIdList.add(rollingPlan.getConsteam());
					}
					if (!userIdList.isEmpty() && 1 == form.getIsok().intValue()) {// 只有见证结果不合格时，才给组长推送消息
						jPushService.pushByUserId(extra, message, JPushCategoryEnums.WITNESS_RESULT.getName(),
								userIdList);
					}

					List<WorkStepWitness> workStepWitnesses = witnessService.getByWorkStepId(wsw.getWorkStep().getId());

					if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
						workStepWitnesses = workStepWitnesses.stream().filter(list -> {
							return (!CommonUtility.isNonEmpty(list.getWitnessflag()));
						}).collect(Collectors.toList());

						if (null != workStepWitnesses && !workStepWitnesses.isEmpty()) {
							List<Integer> okList = new ArrayList<Integer>();
							okList.add(2);
							okList.add(3);

							long successCount = workStepWitnesses.stream()
									.filter(item -> okList.contains(item.getIsok())).count();
							if (successCount == Long.parseLong(workStepWitnesses.size() + "")) {
								witnessFlagService.changeWitnessFlagSingle(wsw.getId(), form.getDosage());
							}
						}
					}
				}
				result.setMessage("成功填写见证结果");
				return result;
			} else {
				result.setCode("-1002");
				result.setMessage("不存在的见证条目[" + form.getId() + "]");
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:" + e.getMessage());
			result.setResponseResult(false);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return result;
		}
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/monitor", method = RequestMethod.POST)
	@ApiOperation(value = "班长提交见证", notes = "班长将指定的见证信息提交到见证组长")
	public JsonResult<Boolean> witnesserType(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") WitnessSubmitForm witnessSubmitForm) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User loginUser = userService.findUserById(witnessSubmitForm.getLoginId());

		if (null == witnessSubmitForm.getIds() || witnessSubmitForm.getIds().isEmpty()) {
			result.setCode("-1002");
			result.setMessage("待提交的ID号为必填项.");
			return result;
		}
		if (null == witnessSubmitForm.getTeamId()) {
			result.setCode("-1002");
			result.setMessage("见证组长的id号是必填项.");
			return result;
		}
		User user = userService.findUserById(witnessSubmitForm.getTeamId());
		if (null == user) {
			result.setCode("-1002");
			result.setMessage("未找到见证组长信息.");
			return result;
		}

		Set<AuthRole> roles = roleApiService.getAuthRole(user, false);
		if (null == roles) {
			result.setCode("-1002");
			result.setMessage("当前给定的见证组长没有找到任何职位信息.");
			return result;
		}

		Set<String> roleType = new HashSet<String>();
		for (AuthRole role : roles) {
			roleType.addAll(role.getRoleType());
		}

		if (null == roleType || roleType.isEmpty()) {
			result.setCode("-1002");
			result.setMessage("当前给定的见证组长没有任何权限.");
			return result;
		}

		for (Integer id : witnessSubmitForm.getIds()) {
			WorkStepWitness workStepWitness = witnessService.findById(id);
			if (null != workStepWitness) {
				if (null != workStepWitness.getWitnessteam()) {
					log.info("已经被提交,略过此条记录.");
					continue;
				}
				if (workStepWitness.getNoticePoint().equals(NoticePointType.QC1.name())) {
					if (!roleType.contains("witness_team_qc1")) {
						result.setCode("-1002");
						result.setMessage("不匹配的QC1见证点类型");
						return result;
					}
				}
				if (workStepWitness.getNoticePoint().equals(NoticePointType.QC2.name())) {
					if (!roleType.contains("witness_team_qc2")) {
						result.setCode("-1002");
						result.setMessage("不匹配的QC2见证点类型");
						return result;
					}
				}
			}
		}

		result.setResponseResult(witnessApiService.assignToWitnessTeam(
				witnessSubmitForm.getIds().toArray(new Integer[] {}), witnessSubmitForm.getTeamId()));

		String message = loginUser.getRealname() + " 将[" + witnessSubmitForm.getIds().size() + "]条见证提交给了您。";
		JPushExtra extra = new JPushExtra();
		extra.setCategory(JPushCategoryEnums.WITNESS_ASSIGN.name());
		jPushService.pushByUserId(extra, message, "见证提交", witnessSubmitForm.getTeamId());
		result.setMessage("成功提交见证");
		return result;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/repairChangeWitnessFlagSingle", method = RequestMethod.POST)
	@ApiOperation(value = "修改应该是工序完成了的，但在待　未见证的见证列表中的数据。", notes = "修改应该是工序完成了的，但在待　未见证的见证列表中的数据。")
	public JsonResult<String> repairChangeWitnessFlagSingle(HttpServletRequest request, HttpServletResponse response,

			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value = "loginId", required = true) Integer loginId) {
		JsonResult<String> ret = new JsonResult<String>();
		try {
			witnessFlagService.repairChangeWitnessFlagSingle();
			ret.setResponseResult("操作成功");
		} catch (Exception e) {
			ret.setResponseResult("操作失败" + e.getMessage());
			e.printStackTrace();
		}
		return ret;
	}

}