package com.easycms.hd.api.baseservice;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.CommonMapResult;
import com.easycms.hd.api.response.JsonResult;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;


@Controller
@RequestMapping("/hdxt/api/extra")
@Api(value = "ExtraAPIController", description = "额外属性数据相关的api")
public class ExtraAPIController {
	
	/**
	 * 会议提醒类型
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/conference_alarm", method = RequestMethod.GET)
	@ApiOperation(value = "会议提醒类型", notes = "获取会议提醒类型的下拉列表")
	public JsonResult<List<CommonMapResult>> pushSingle(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<List<CommonMapResult>> result = new JsonResult<List<CommonMapResult>>();
		result.setResponseResult(ConferenceAlarmTypsEnums.getList());
		result.setMessage("成功获取会议提醒类型");
		return result;
	}
}
