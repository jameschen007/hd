package com.easycms.hd.api.baseservice;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.MaterialTypeEnum;
import com.easycms.hd.api.request.MaterialPageRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.request.base.SimpleBasePagenationRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.MaterialExtractPageResult;
import com.easycms.hd.api.response.MaterialExtractResult;
import com.easycms.hd.api.response.MaterialOutCountResult;
import com.easycms.hd.api.response.MaterialPageResult;
import com.easycms.hd.api.response.MaterialResult;
import com.easycms.hd.api.service.MaterialApiService;
import com.easycms.hd.api.service.MaterialExtractApiService;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.material.domain.Material;
import com.easycms.hd.material.domain.MaterialExtract;
import com.easycms.hd.material.service.MaterialExtractService;
import com.easycms.hd.material.service.MaterialService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/material")
@Api(value = "MaterialAPIController", description = "物资管理相关的api")
public class MaterialAPIController {
	
	@Autowired
	private MaterialApiService materialApiService;
	
	@Autowired
	private MaterialExtractApiService materialExtractApiService;

	@Autowired
	private MaterialService materialService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private MaterialExtractService materialExtractService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;

	@Autowired
	private ComputedConstantVar constantVar ;
	
	@Value("#{APP_SETTING['enpower_api_materialData']}")
	private String enpowerApiInfo;
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}

	/**
	 * 获取物资
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "获取物料列表", notes = "获取物料列表")
	public JsonResult<Object> getMaterial(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") MaterialPageRequestForm form) throws Exception {
		JsonResult<Object> result = new JsonResult<Object>();
		
		if (null != form.getPagenum() && null != form.getPagesize()) {
			Page<Material> page = new Page<Material>();

			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
			
			Page<MaterialExtract> pager = new Page<MaterialExtract>();
			
			pager.setPageNum(form.getPagenum());
			pager.setPagesize(form.getPagesize());
			
			Integer startNo = (form.getPagenum() - 1) * form.getPagesize() + 1;
			if (form.getType().equals(MaterialTypeEnum.IN)) {
				MaterialPageResult materialPageResult = materialApiService.getMaterialPageResult(form.getLoginId(), page);
				if (null != materialPageResult.getData() && !materialPageResult.getData().isEmpty()) {
					for (int i = 0; i < materialPageResult.getData().size(); i++) {
						materialPageResult.getData().get(i).setNo(startNo + i);;
					}
				}
				result.setResponseResult(materialPageResult);
			} else if (form.getType().equals(MaterialTypeEnum.OUT)) {
				if (null != form.getDepartmentId()) {
					MaterialExtractPageResult materialExtractPageResult = materialExtractApiService.getMaterialExtractPageResult(form.getLoginId(), form.getDepartmentId(), pager);
					
					if (null != materialExtractPageResult.getData() && !materialExtractPageResult.getData().isEmpty()) {
						for (int i = 0; i < materialExtractPageResult.getData().size(); i++) {
							materialExtractPageResult.getData().get(i).setNo(startNo + i);;
						}
					}
					
					result.setResponseResult(materialExtractPageResult);
				}
			}
			result.setMessage("成功获取分页信息");
			return result;
		} else {
			result.setCode("-1001");
			result.setMessage("分页信息有缺失.");
			return result;
		}
	}
	
	/**
	 * 获取物资
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/enpower/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取物料列表", notes = "获取物料列表")
	public JsonResult<Object> getMaterialEnpower(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "质保号",required=true) @PathVariable("id") String id,
			@ApiParam(value = "出入库类型",required=true) @RequestParam("type") MaterialTypeEnum type,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<Object> result = new JsonResult<Object>();
		
//		EnpowerRequest enpowerRequest = new EnpowerRequest();
//		enpowerRequest.setCondition("Material='XXX'");
//		enpowerRequest.setXmlname("物资信息查询");
//		enpowerRequest.setIsPage(1);
//		enpowerRequest.setPageIndex(1);
//		enpowerRequest.setPageSize(20000);
//		
//		String parms = enpowerRequest.toString();
//		
//		String materialRequestResult = HttpRequest.sendPost(enpowerHost + enpowerApiInfo, parms, "application/x-www-form-urlencoded", "GBK");
//		if (null != materialRequestResult && !"".equals(materialRequestResult)){
//			//TODO 入库查询接口 出库 in out区分 查询都是这个 获取物资
//		}
		
		try{

			if (type.equals(MaterialTypeEnum.IN)) {//入库
				
				MaterialResult materialResult = executePushToEnpowerServiceImpl.materialStoreSearchUnexamined(id);
				
//				Material material = materialService.findByWarrantyNo(id);
//				MaterialResult materialResult = materialApiService.materialTransferToMaterialResult(material, form.getLoginId());
				result.setResponseResult(materialResult);
			} else if (type.equals(MaterialTypeEnum.OUT)) {//出库
		        HttpSession session = request.getSession();
		        User loginUser = (User) session.getAttribute("user");
		        if(loginUser==null){
		        	loginUser = userService.findUserById(form.getLoginId());
		        }
				

		    	List<MaterialResult> materialExtractResults = executePushToEnpowerServiceImpl.materialOutSearchUnexamined(id,loginUser);
//				List<MaterialExtract> materialExtracts = materialExtractService.findByWarrantyNo(id);
//				List<MaterialExtractResult> materialExtractResults = materialExtractApiService.materialExtractTransferToMaterialExtractResult(materialExtracts, form.getLoginId());
//				
//				int startNo = 1;
//				if (null != materialExtractResults && !materialExtractResults.isEmpty()) {
//					for (int i = 0; i < materialExtractResults.size(); i++) {
//						materialExtractResults.get(i).setNo(startNo + i);;
//					}
//				}
				result.setResponseResult(materialExtractResults);
			} else if (type.equals(MaterialTypeEnum.CANCEL)) {//出库//退库
				
		    	List<MaterialResult> materialExtractResults = executePushToEnpowerServiceImpl.materialCancelSearchUnexamind(id);
				result.setResponseResult(materialExtractResults);
				
			}
					
		}catch(Exception e){

			result.setCode(ExceptionCode.E4);
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(Collections.emptyList());
			return result;
		}
		return result;
	}
	
	/**
	 * 获取物资
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "获取物料列表--单个", notes = "获取物料列表--单个")
	public JsonResult<Object> getMaterialSingle(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "内部物料号ID",required=true) @PathVariable("id") Integer id,
			@ApiParam(value = "出入库类型",required=true) @RequestParam("type") MaterialTypeEnum type,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<Object> result = new JsonResult<Object>();
		if (type.equals(MaterialTypeEnum.IN)) {
			MaterialResult materialResult = materialApiService.materialTransferToMaterialResult(materialService.findById(id), form.getLoginId());
			result.setResponseResult(materialResult);
		} else if (type.equals(MaterialTypeEnum.OUT)) {
			MaterialExtractResult materialExtractResult = materialExtractApiService.materialExtractTransferToMaterialExtractResult(materialExtractService.findById(id), form.getLoginId()); 
			result.setResponseResult(materialExtractResult);
		}
		return result;
	}
	

    /**
     * 查询今日入库量明细接口
     */
    @ResponseBody
    @ApiOperation(value = "查询今日入库量明细接口", notes = "查询今日入库量明细接口")
    @RequestMapping(value = "/materialTodayStoreList", method = RequestMethod.GET)
    public JsonResult<Object> materialTodayStoreList (HttpServletRequest request, HttpServletResponse response, 
    		@ModelAttribute("form") SimpleBasePagenationRequestForm form
           ) {

        JsonResult<Object> result = new JsonResult<Object>();
        try{
            List<MaterialResult> list = executePushToEnpowerServiceImpl.materialTodayStoreList(form.getPagenum(),form.getPagesize());

			Page<MaterialResult> page = new Page<MaterialResult>();
        	if(list==null || list.size()==0){
        		page.setDatas(new ArrayList<MaterialResult>());
				result.setResponseResult(page);

        	} else {
				int totalCounts = new BigDecimal(list.get(0).getTotal_count()).intValue();
				page.setPagesize(form.getPagesize());
				page.execute(totalCounts, form.getPagenum(), list);
			}
			result.setResponseResult(page);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(false);
            return result;
        }
        return result;
    }

    /**
     * 查询今日出库量明细接口
     */
    @ResponseBody
    @ApiOperation(value = "查询今日出库量明细接口", notes = "查询今日出库量明细接口")
    @RequestMapping(value = "/materialTodayOutList", method = RequestMethod.POST)
    public JsonResult<Object> materialTodayOutList(HttpServletRequest request, HttpServletResponse response, 
			@ApiParam(required = true, name = "ISS_DEPT", value = "出库单位,中文，故本接口POST") @RequestParam(value="ISS_DEPT",required=true) String ISS_DEPT,
			@ModelAttribute("form") SimpleBasePagenationRequestForm form) {

        JsonResult<Object> result = new JsonResult<Object>();
        List<MaterialResult> list = null;
        try{
        	list = executePushToEnpowerServiceImpl.materialTodayOutList(ISS_DEPT,form.getPagenum(),form.getPagesize());

			Page<MaterialResult> page = new Page<MaterialResult>();
        	if(list==null || list.size()==0){
        		page.setDatas(new ArrayList<MaterialResult>());
				result.setResponseResult(page);

        	} else {
				int totalCounts = new BigDecimal(list.get(0).getTotal_count()).intValue();
				page.setPagesize(form.getPagesize());
				page.execute(totalCounts, form.getPagenum(), list);
			}
			result.setResponseResult(page);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            return result;
        }
        return result;
    }

    /**
     * 查询今日出库量（按部门）接口
     */
    @ResponseBody
    @ApiOperation(value = "查询今日出库量（按部门）接口", notes = "查询今日出库量（按部门）接口")
    @RequestMapping(value = "/materialTodayOutCount", method = RequestMethod.GET)
    public JsonResult<List<MaterialOutCountResult>> materialTodayOutCount(HttpServletRequest request, HttpServletResponse response, 
			@ApiParam(required = true, name = "loginId", value = "loginId") @RequestParam(value="loginId",required=true) Integer loginId) {

        JsonResult<List<MaterialOutCountResult>> result = new JsonResult<List<MaterialOutCountResult>>();
        List<MaterialOutCountResult> list = null;
        try{
        	list = executePushToEnpowerServiceImpl.materialTodayOutCount();
            result.setResponseResult(list);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            result.setResponseResult(new ArrayList<MaterialOutCountResult>());
            return result;
        }
        return result;
    }
    /**
     * 物项库存查询接口
     */
    @ResponseBody
    @ApiOperation(value = "物项库存查询接口", notes = "物项库存查询接口")
    @RequestMapping(value = "/materialQueryList", method = RequestMethod.POST)
    public JsonResult<Object> materialQueryList(
			@ApiParam(required = false, name = "MATE_CODE", value = "物项编码") @RequestParam(value="MATE_CODE",required=false) String MATE_CODE,
			@ApiParam(required = false, name = "MATE_DESCR", value = "物项名称") @RequestParam(value="MATE_DESCR",required=false) String MATE_DESCR,
			@ApiParam(required = false, name = "SPEC", value = "规格型号") @RequestParam(value="SPEC",required=false) String SPEC,
			@ApiParam(required = false, name = "TEXTURE", value = "材质") @RequestParam(value="TEXTURE",required=false) String TEXTURE,
			@ApiParam(required = false, name = "GUARANTEENO", value = "质保编号") @RequestParam(value="GUARANTEENO",required=false) String GUARANTEENO,
			@ModelAttribute("form") SimpleBasePagenationRequestForm form
			) {

        JsonResult<Object> result = new JsonResult<Object>();
        List<MaterialResult> list = null;
        try{
        	list = executePushToEnpowerServiceImpl.materialQueryList(MATE_CODE,MATE_DESCR,SPEC,TEXTURE,GUARANTEENO,form.getPagenum(),form.getPagesize());

			Page<MaterialResult> page = new Page<MaterialResult>();
        	if(list==null || list.size()==0){
        		page.setDatas(new ArrayList<MaterialResult>());
				result.setResponseResult(page);

        	} else {
				int totalCounts = new BigDecimal(list.get(0).getTotal_count()).intValue();
				page.setPagesize(form.getPagesize());
				page.execute(totalCounts, form.getPagenum(), list);
			}
			result.setResponseResult(page);
        }catch(Exception e){
            e.printStackTrace();
			result.setCode(ExceptionCode.E4);
            result.setMessage("操作异常:"+e.getMessage());
            return result;
        }
        return result;
    }

}
