package com.easycms.hd.api.baseservice.hseproblem;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.hseproblem.DynamicFilterItemList;
import com.easycms.hd.api.enums.hseproblem.Hse3mEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.enums.hseproblem.underlying.TheFilterItem;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.request.base.SimpleBasePagenationRequestForm;
import com.easycms.hd.api.request.hseproblem.HseProblemExListRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.hseproblem.HseProblemPageDataResult;
import com.easycms.hd.api.service.HseProblemApiService;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.hseproblem.service.HseProblemFileService;
import com.easycms.hd.hseproblem.service.HseProblemService;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.hd.plan.mybatis.dao.StatisticsHse3monthDao;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 手机APP软件修改需求单15-04-011-[19] 需求相关内容
 */
@Controller
@RequestMapping("/hdxt/api/hse")
@Api(value = "HseProblemAPIController", description = "文明施工问题模块相关的api")
@Slf4j
@javax.transaction.Transactional
public class HseProblemAPIExController {
    @Autowired
    private HseProblemService hseProblemService;
    @Autowired
    private HseProblemApiService hseProblemApiService;
    @Autowired
    private HseProblemFileService hseProblemFileService;
    @Autowired
    private UserService userService;
    @Autowired
    private HseProblemSolveService hseProblemSolveService;
    @Autowired
    private StatisticsHse3monthDao statisticsHse3monthDao;

    /**
     * 手机APP软件修改需求单15-04-011-[19]　问题二，
     * /hdxt/api/hse/dynamicFilterItems
     *
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "dynamicFilterItems", method = RequestMethod.GET)
    @ApiOperation(value = "需求单15-04-011-[19]标题下拉菜单进行筛选项", notes = "需求单15-04-011-[19]标题下拉菜单进行筛选项")
    public JsonResult<Map<String, List<TheFilterItem>>> hseStatistics3MonthCaptain(
            @ApiParam(required = true, name = "loginId", value = "loginId", defaultValue = "1") @RequestParam(value = "loginId", required = true) Integer loginId, HttpServletRequest request, HttpServletResponse response) {

        JsonResult<Map<String, List<TheFilterItem>>> result = new JsonResult<>();

        result.setResponseResult(new DynamicFilterItemList().getDynamic());

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "problemList3m", method = RequestMethod.GET)
    @ApiOperation(value = "查询90天内安全文明问题分页列表", notes = "用于查询90天内(隐患总数　待整改数　超期未整改)安全文明问题列表，问题进度状态：need_handle:待处理;renovating:整改中;need_check:待审查;finish:已完成;none:不需要处理;隐患总数total　待整改数waitingModify　超期未整改delay之一；")
    public JsonResult<HseProblemPageDataResult> problemList(HttpServletRequest request, HttpServletResponse response,
                                                            @ModelAttribute("requestForm") SimpleBasePagenationRequestForm basePagenationRequestForm,
                                                            @ModelAttribute("requestForm2") HseProblemExListRequestForm hseProblemExListRequestForm,
                                                            @ApiParam(value = "问题进度状态", name = "problemStatus", required = false) @RequestParam(value = "problemStatus", required = false) HseProblemStatusEnum problemStatus,
                                                            @ApiParam(required = true, name = "responsibleDept", value = "责任部门", defaultValue = "550") @RequestParam(value = "responsibleDept", required = true) @NotNull Integer responsibleDept,
                                                            @ApiParam(required = true, name = "oneOf3Type", value = "隐患总数　待整改数　超期未整改之一") @RequestParam(value = "oneOf3Type", required = true) @NotNull Hse3mEnum oneOf3Type
    ) {
        JsonResult<HseProblemPageDataResult> result = new JsonResult<HseProblemPageDataResult>();
        if (true) {//调试信息
            log.debug("责任部门responsibleDept=" + responsibleDept);
            log.debug("oneOf3Type：" + oneOf3Type);
            if (hseProblemExListRequestForm!=null && hseProblemExListRequestForm.getCreateTime() != null) {
                System.out.println("提出时间筛选字段="+hseProblemExListRequestForm.getCreateTime());
            }
            if (hseProblemExListRequestForm!=null && hseProblemExListRequestForm.getTargetTime() != null) {
                System.out.println("截止时间筛选字段="+hseProblemExListRequestForm.getTargetTime());
            }
            System.out.println("问题进度状态="+problemStatus);
        }

        HseProblemPageDataResult hseProblemPageDataResult = new HseProblemPageDataResult();
        Page<HseProblem> page = new Page<HseProblem>();
        try {
            if (basePagenationRequestForm.getLoginId() != null) {
                User user = userService.findUserById(basePagenationRequestForm.getLoginId());
                if (user == null) {
                    result.setCode("-1001");
                    result.setMessage("用户不存在！");
                    return result;
                }
                if (null != basePagenationRequestForm.getPagenum() && null != basePagenationRequestForm.getPagesize()) {
                    page.setPageNum(basePagenationRequestForm.getPagenum());
                    page.setPagesize(basePagenationRequestForm.getPagesize());
                }

                hseProblemPageDataResult = hseProblemApiService.getHseProblem3mResult(page, responsibleDept, oneOf3Type, hseProblemExListRequestForm,  problemStatus);

            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode("-1001");
            result.setMessage("操作异常：" + e.getMessage());
        }
        result.setResponseResult(hseProblemPageDataResult);
        return result;
    }
}
