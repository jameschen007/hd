package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.service.RollingPlanService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.request.WorkStepWitnessCancelRequestForm;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.WorkStepWitnessItemsResult;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/workstep_op")
@Api(value = "WorkStepOperationAPIController", description = "针对工序操作相关的api")
@Transactional
public class WorkStepOperationAPIController {
	public static Logger logger = Logger.getLogger(WorkStepOperationAPIController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private WorkStepApiService workStepApiService;
	@Autowired
	private RollingPlanApiService rollingPlanApiService;
	@Autowired
	private RollingPlanService rollingPlanService;

	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @return
	 * @throws Exception
	 */
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	/**
	 * 发起见证
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/witness", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "工序上发起见证", notes = "对某道工序发起见证，各个工序独立发起")
	public JsonResult<List<WorkStepWitnessItemsResult>> launch(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") BaseRequestForm base, 
			@RequestBody String form
			){

		JsonResult<List<WorkStepWitnessItemsResult>> result = new JsonResult<List<WorkStepWitnessItemsResult>>();
		try{

			User user = userService.findUserById(base.getLoginId());

            Gson gson = new Gson();
			Set<WorkStepWitnessItemsForm> workStepWitnessItemsForms = gson.fromJson(form, new TypeToken<Set<WorkStepWitnessItemsForm>>() {}.getType());

			List<WitingPlan> planList = this.rollingPlanService.findByWorkSteps(workStepWitnessItemsForms);
			
			List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = workStepApiService.launchWitness(planList, user);
			
			if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().startsWith("-");}).count() > 0L) {
				result.setCode("-1000");
				result.setMessage("发起见证出现异常。");
			} else if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().equals("-8002");}).count() > 0L){
				result.setMessage("某些曾发起过的见证点被自动跳过，其余见证点已被成功发起");
			} else {
				result.setMessage("成功发起见证");
			}

			result.setResponseResult(workStepWitnessItemsResults);
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(null);
			return result;
		}
		
		return result;
	}
	
	/**
	 * @param request
	 * @param response
	 * @param form
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/witness/cancel", method = RequestMethod.POST)
	@ApiOperation(value = "工序上取消见证", notes = "对某些工序上发起的见证进行取消操作")
	public JsonResult<List<WorkStepWitnessItemsResult>> cancel(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") WorkStepWitnessCancelRequestForm form){
		JsonResult<List<WorkStepWitnessItemsResult>> result = new JsonResult<List<WorkStepWitnessItemsResult>>();
		
		User user = userService.findUserById(form.getLoginId());
		
		List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = workStepApiService.cancelWitness(form.getIds(), user);
		
		if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().equals("1000");}).count() == 0L) {
			result.setCode("-1001");
			result.setMessage("取消见证出现异常。");
		} else {
			result.setMessage("成功取消见证");
		}
		
		result.setResponseResult(workStepWitnessItemsResults);
		
		return result;
	}

}
