package com.easycms.hd.api.baseservice;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.request.NotificationConfirmRequestForm;
import com.easycms.hd.api.request.NotificationFeedbackRequestForm;
import com.easycms.hd.api.request.NotificationMarkRequestForm;
import com.easycms.hd.api.request.base.BaseIdsRequestForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.NotificationApiService;
import com.easycms.hd.api.service.NotificationFeedbackApiService;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.domain.NotificationConfirm;
import com.easycms.hd.conference.enums.NotificationConfirmType;
import com.easycms.hd.conference.service.NotificationConfirmService;
import com.easycms.hd.conference.service.NotificationService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/hdxt/api/notification_op")
@Api(value = "NotificationOperationAPIController", description = "通告操作相关的api")
@javax.transaction.Transactional
public class NotificationOperationAPIController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private NotificationApiService notificationApiService;
	
	@Autowired
	private NotificationConfirmService notificationConfirmService;
	
	@Autowired
	private NotificationFeedbackApiService notificationFeedbackApiService;
	
    /**
     * 确认通知
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    @ApiOperation(value = "确认会议", notes = "用户发起一次通告的确认")
    public JsonResult<Boolean> confirm(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("form") NotificationConfirmRequestForm form) throws Exception {
        JsonResult<Boolean> result = new JsonResult<Boolean>();
        
        User user = userService.findUserById(form.getLoginId());
        
        Notification notification = notificationService.findById(form.getNotificationId());
    	
    	if (null != notification) {
    		List<User> participants = notification.getParticipants();
    		if (null != participants && !participants.isEmpty()) {
    			boolean flag = false;
    			for (User u : participants) {
    				if (u.getId().equals(user.getId())) {
    					flag = true;
    					break;
    				}
    			}
    			
    			if (!flag) {
    				result.setCode("-1000");
	                result.setMessage("你不是参会人之一，不允许通告确认。");
	                return result;
    			}
    			
		        List<NotificationConfirm> notificationConfirms = notificationConfirmService.findByNotificationId(form.getNotificationId(), form.getLoginId());
		        
		        if (null != notificationConfirms && !notificationConfirms.isEmpty()) {
		            result.setCode("-1000");
		            result.setMessage("通告已经发起过确认,当前请求失败。");
		            return result;
		        } else {
			        	NotificationConfirm confirm = new NotificationConfirm();
			        	confirm.setUserid(user.getId());
			        	confirm.setCreateBy(user.getId());
			        	confirm.setCreateOn(new Date());
			        	confirm.setNotificationid(form.getNotificationId());
			        	confirm.setStatus(NotificationConfirmType.CONFIRM.name());
			            
			            if (null != notificationConfirmService.add(confirm)) {
			                result.setMessage("通告确认成功。");
			            } else {
			                result.setCode("-1000");
			                result.setMessage("通告确认失败。");
			                return result;
			            }
		        }
    		} else {
    			result.setCode("-1000");
                result.setMessage("没有任何参会人，不允许通告确认。");
                return result;
    		}
    	} else {
    		result.setCode("-1000");
            result.setMessage("通告不存在。");
            return result;
    	}
        return result;
    }


	/**
	 * 反馈通知
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	@ApiOperation(value = "反馈通知", notes = "用户发起一次通告的反馈")
	public JsonResult<Boolean> createNotification(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") NotificationFeedbackRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		User user = userService.findUserById(form.getLoginId());
		
        Notification notification = notificationService.findById(form.getNotificationId());
    	
    	if (null != notification) {
    		List<User> participants = notification.getParticipants();
    		if (null != participants && !participants.isEmpty()) {
    			boolean flag = false;
    			for (User u : participants) {
    				if (u.getId().equals(user.getId())) {
    					flag = true;
    					break;
    				}
    			}
    			
    			if (!flag) {
    				result.setCode("-1000");
	                result.setMessage("你不是参会人之一，不允许通告反馈。");
	                return result;
    			}
		
				if (notificationFeedbackApiService.insert(form, user)) {
					NotificationMarkRequestForm notificationMarkRequestForm = new NotificationMarkRequestForm();
					notificationMarkRequestForm.setNotificationId(form.getNotificationId());
					notificationFeedbackApiService.markRead(notificationMarkRequestForm, user);
					result.setMessage("通告反馈成功。");
				} else {
					result.setCode("-1000");
					result.setMessage("通告反馈失败。");
					return result;
				}
    		} else {
    			result.setCode("-1000");
                result.setMessage("没有任何参会人，不允许通告反馈。");
                return result;
    		}
    	} else {
    		result.setCode("-1000");
            result.setMessage("通告不存在。");
            return result;
    	}
		return result;
	}
	
	/**
	 * 标记通知反馈已读
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/mark", method = RequestMethod.POST)
	@ApiOperation(value = "标记通知反馈已读", notes = "对发起点之前时间的通知反馈标记为已读")
	public JsonResult<Boolean> markConferenceMark(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") NotificationMarkRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User user = userService.findUserById(form.getLoginId());
		
		Notification notification = notificationService.findById(form.getNotificationId());
    	
    	if (null != notification) {
    		List<User> participants = notification.getParticipants();
    		if (null != participants && !participants.isEmpty()) {
    			boolean flag = false;
    			for (User u : participants) {
    				if (u.getId().equals(user.getId())) {
    					flag = true;
    					break;
    				}
    			}
    			
    			if (!flag) {
    				result.setCode("-1000");
	                result.setMessage("你不是参会人之一，不允许通告已读标记。");
	                return result;
    			}
				if (notificationFeedbackApiService.markRead(form, user)) {
					result.setMessage("标记通告已读标记成功。");
				} else {
					result.setCode("-1000");
					result.setMessage("标记通告已读标记失败。");
					return result;
				}
    		} else {
    			result.setCode("-1000");
                result.setMessage("没有任何参会人，不允许通告已读标记。");
                return result;
    		}
    	} else {
    		result.setCode("-1000");
            result.setMessage("通告不存在。");
            return result;
    	}		
		return result;
	}
	
	/**
	 * 删除通知
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ApiOperation(value = "删除通知", notes = "删除指定IDS的通知")
	public JsonResult<Boolean> deleteConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseIdsRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		if (notificationApiService.batchDelete(form.getIds())) {
			result.setMessage("删除通知成功。");
		} else {
			result.setCode("-1000");
			result.setMessage("删除通知失败。");
			return result;
		}
		return result;
	}	
	
	/**
	 * 取消通知　：　修改会议状态为CANCEL状态　，并且给所有参会人发“取消会议”了的通知
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	@ApiOperation(value = "取消通知", notes = "取消指定IDS的通知")
	public JsonResult<Boolean> cancelConference(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseIdsRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		try{
			
			if (notificationApiService.batchCancel(form.getIds())) {
				result.setMessage("取消通知成功。");
			} else {
				result.setCode("-1000");
				result.setMessage("取消通知失败。");
				return result;
			}
			return result;
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
	}
}
