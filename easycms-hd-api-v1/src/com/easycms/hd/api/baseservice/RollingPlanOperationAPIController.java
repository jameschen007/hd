package com.easycms.hd.api.baseservice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.hd.api.request.underlying.WitingPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.hd.api.enums.RollingPlanMethodEnum;
import com.easycms.hd.api.request.RollingPlanBackFillRequestForm;
import com.easycms.hd.api.request.RollingPlanOperationRequestForm;
import com.easycms.hd.api.request.RollingplanWitnessItemsForm;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.WorkStepWitnessItemsResult;
import com.easycms.hd.api.service.WorkStepApiService;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api/rollingplan_op")
@Api(value = "RollingPlanOperationAPIController", description = "六级滚动计划操作相关的api")
@Slf4j
@Transactional
public class RollingPlanOperationAPIController {
	@Autowired
	private UserService userService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private WorkStepApiService workStepApiService;
	@Autowired
	private JPushService jPushService;
	
	@Autowired
	private RoleService roleService;
	/**
	 * 所有滚动计划的分页信息，可按条件获取。
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	
	@InitBinder  
	public void initBinder(WebDataBinder binder) {  
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    dateFormat.setLenient(false);  
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false)); 
	}
	
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(value = "六级滚动计划操作", notes = "用于六级滚动计划的分派，改派和解除")
	public JsonResult<Boolean> rollingPlanOperation(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") RollingPlanOperationRequestForm rollingPlanRequestForm) throws Exception {
		log.info("对六级计划进行操作。");
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		User loginUser = userService.findUserById(rollingPlanRequestForm.getLoginId());
		List<Role> roles = loginUser.getRoles();
		Integer usingRoleId = rollingPlanRequestForm.getRoleId() ;
		if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
			roles = roleService.findListById(roles,usingRoleId);
		}
		
		try{

			if (null != rollingPlanRequestForm && null != rollingPlanRequestForm.getIds() && 0 < rollingPlanRequestForm.getIds().size()) {
				if (rollingPlanRequestForm.getMethod().equals(RollingPlanMethodEnum.ASSIGN)) {
					if (null != rollingPlanRequestForm.getUserId()) {
						User user = userService.findUserById(rollingPlanRequestForm.getUserId());
						if (null == user) {
							result.setCode("-2001");
							result.setMessage("用户信息为空。");
							return result;
						}
						if (rollingPlanService.assignTeam(rollingPlanRequestForm.getIds().toArray(new Integer[] {}), rollingPlanRequestForm.getUserId(), 0, new Date(), rollingPlanRequestForm.getConsDate(), loginUser.getId())) {
							result.setMessage("分派成功");
							
				            String message = loginUser.getRealname() + " 将[" + rollingPlanRequestForm.getIds().size() + "]条滚动计划分派给了您。";
				            JPushExtra extra = new JPushExtra();
				    		extra.setCategory(JPushCategoryEnums.CONS_ASSIGN.name());
				    		jPushService.pushByUserId(extra, message, JPushCategoryEnums.CONS_ASSIGN.getName(), user.getId());
							return result;
						}
					}
				} else if (rollingPlanRequestForm.getMethod().equals(RollingPlanMethodEnum.REASSIGN)) {
					if (null != rollingPlanRequestForm.getUserId()) {
						User user = userService.findUserById(rollingPlanRequestForm.getUserId());
						if (null == user) {
							result.setCode("-2001");
							result.setMessage("用户信息为空。");
							return result;
						}
						
						boolean flag = false;
						if (null == rollingPlanRequestForm.getConsDate()) {
							flag = rollingPlanService.reAssignTeam(rollingPlanRequestForm.getIds().toArray(new Integer[] {}), rollingPlanRequestForm.getUserId(), loginUser.getId());
						} else {
							flag = rollingPlanService.reAssignTeam(rollingPlanRequestForm.getIds().toArray(new Integer[] {}), rollingPlanRequestForm.getUserId(), rollingPlanRequestForm.getConsDate(), loginUser.getId());
						}
						
						if(flag) {
							result.setMessage("改派成功");
							
							String message = loginUser.getRealname() + " 将[" + rollingPlanRequestForm.getIds().size() + "]条滚动计划改派给了您。";
				            JPushExtra extra = new JPushExtra();
				    		extra.setCategory(JPushCategoryEnums.CONS_ASSIGN.name());
				    		jPushService.pushByUserId(extra, message, "滚动计划改派", user.getId());
							return result;
						}else{
							result.setCode("-1002");
							result.setMessage("存在问题未解决，改派失败！");
							return result;
						}
					}
				} else if (rollingPlanRequestForm.getMethod().equals(RollingPlanMethodEnum.RELEASE)) {
					if (null != roles) {
						for (Role role : roles) {
							Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
							if (roleNameList.contains("captain")) {
								if (rollingPlanService.deleteRollingPlan(rollingPlanRequestForm.getIds().toArray(new Integer[] {}))) {
									result.setMessage("解除成功");
									return result;
								}
							} else if (roleNameList.contains("monitor")) {
								if (rollingPlanService.releaseTeam(rollingPlanRequestForm.getIds().toArray(new Integer[] {}), loginUser.getId())) {
									result.setMessage("解除成功");
									return result;
								}
							}
						}
					}
					
				}
			}
		}catch(Exception e){
			result.setCode("-1002");
			result.setMessage("请求异常"+e.getMessage());
			return result;
		}
		
		
		result.setCode("-1001");
		result.setMessage("不合理的操作请求");
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/backfill", method = RequestMethod.POST)
	@ApiOperation(value = "六级滚动计划回填", notes = "用于六级滚动计划的回填")
	public JsonResult<Boolean> rollingPlanBackFill(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") RollingPlanBackFillRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		RollingPlan rollingplan = rollingPlanService.findById(form.getId());
		if (null == rollingplan) {
			result.setCode("-1001");
			result.setMessage("滚动计划不存在");
			return result;
		}
		
		User user = null;
		if (null != form.getWelderId()) {
			user = userService.findUserById(form.getWelderId());
		}
		if (null == user) {
			result.setCode("-1001");
			result.setMessage("指定用户不存在");
			return result;
		}
		
		if (null != rollingplan.getWelder() && null != rollingplan.getWelddate()) {
			result.setCode("-1001");
			result.setMessage("已经完成过回填的滚动计划，本次提交失败。");
			return result;
		}
		
		if (!rollingPlanService.backFill(form.getId(), form.getWelderId(), form.getWeldDate())) {
			result.setCode("-1001");
			result.setMessage("回填失败");
			return result;
		}
		
		String message = "滚动计划：" + rollingplan.getDrawno() + " 已回填完成。";
        JPushExtra extra = new JPushExtra();
		extra.setCategory(JPushCategoryEnums.ROLLINGPLAN_FILLBACK_COMPLETE.name());
		jPushService.pushByUserId(extra, message, JPushCategoryEnums.ROLLINGPLAN_FILLBACK_COMPLETE.getName(), rollingplan.getConsmonitor());
		
		result.setMessage("成功回填滚动计划");
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/witness", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "滚动计划上发起见证", notes = "对指定滚动计划发起见证, ID为一串，其它为一项")
	public JsonResult<List<WorkStepWitnessItemsResult>> launch(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam @ApiParam(value = "登录用户ID",required=true) Integer loginId,
			@ModelAttribute("form") RollingplanWitnessItemsForm rollingplanWitnessItemsForm){
		JsonResult<List<WorkStepWitnessItemsResult>> result = new JsonResult<List<WorkStepWitnessItemsResult>>();
		User user = userService.findUserById(loginId);
		
		if (null == rollingplanWitnessItemsForm.getIds() || rollingplanWitnessItemsForm.getIds().length == 0) {
			result.setCode("-1000");
			result.setMessage("滚动计划ID号不能为空。");
			return result;
		}
		
		Set<WorkStepWitnessItemsForm> workStepWitnessItemsForms = new HashSet<WorkStepWitnessItemsForm>();
		for (Integer id : rollingplanWitnessItemsForm.getIds()) {
			RollingPlan rollingPlan = rollingPlanService.findById(id);
			if (null != rollingPlan && null != rollingPlan.getWorkSteps()) {
				for (WorkStep workStep : rollingPlan.getWorkSteps()) {
					WorkStepWitnessItemsForm wswf = new WorkStepWitnessItemsForm();
					wswf.setId(workStep.getId());
					wswf.setWitnessaddress(rollingplanWitnessItemsForm.getWitnessaddress());
					wswf.setWitnessdate(rollingplanWitnessItemsForm.getWitnessdate());
					workStepWitnessItemsForms.add(wswf);
				}
			}
		}
		
		List<WorkStepWitnessItemsResult> workStepWitnessItemsResults = null;
		try{

			List<WitingPlan> planList = this.rollingPlanService.findByWorkSteps(workStepWitnessItemsForms);

			workStepWitnessItemsResults = workStepApiService.launchWitness(planList, user);
//			workStepWitnessItemsResults = workStepApiService.launchWitness(workStepWitnessItemsForms, user);
			
			if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().startsWith("-");}).count() > 0L) {
				result.setCode("-1000");
				result.setMessage("发起见证出现异常。");
			} else if (workStepWitnessItemsResults.stream().filter(workStepWitnessItemsResult -> {return workStepWitnessItemsResult.getCode().equals("-8002");}).count() > 0L){
				result.setMessage("某些曾发起过的见证点被自动跳过，其余见证点已被成功发起");
			} else {
				result.setMessage("成功发起所有见证");
			}
					
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			return result;
		}
		

		
//TODO 发起PUSH
		result.setResponseResult(workStepWitnessItemsResults);
		return result;
	}
}
