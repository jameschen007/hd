package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.List;

public class TaskStatusStatisticsResult implements Serializable{
	private static final long serialVersionUID = 6232206639104455938L;
	private String category;
	private List<TaskTypeStatisticsResult> retuls;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public List<TaskTypeStatisticsResult> getRetuls() {
		return retuls;
	}
	public void setRetuls(List<TaskTypeStatisticsResult> retuls) {
		this.retuls = retuls;
	}
}
