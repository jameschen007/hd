package com.easycms.hd.api.domain;

import java.util.Set;

import lombok.Data;

@Data
public class TeamResult {
	private Integer id;
	private String name;
	private String realname;
	private Set<AuthRole> roles;
}
