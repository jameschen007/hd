package com.easycms.hd.api.domain;

import java.io.Serializable;
import java.util.List;

public class TeamMap implements Serializable{
	private static final long serialVersionUID = 8484130702603347755L;
	
	private Constendman parent;
	private List<Constendman> children;
	
	public Constendman getParent() {
		return parent;
	}
	public void setParent(Constendman parent) {
		this.parent = parent;
	}
	public List<Constendman> getChildren() {
		return children;
	}
	public void setChildren(List<Constendman> children) {
		this.children = children;
	}
}
