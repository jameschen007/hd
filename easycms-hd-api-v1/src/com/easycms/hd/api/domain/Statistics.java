package com.easycms.hd.api.domain;

import lombok.Data;

@Data
public class Statistics {
	private Integer id;
	private Integer userId;
	private String type;
//都是给见证用的一些额外属性	
	private String qc2Type;
	private String pointType;
	private String witnessType;
}
