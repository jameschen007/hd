package com.easycms.hd.api.domain;

import java.io.Serializable;

public class TaskPrice implements Serializable{
	private static final long serialVersionUID = -104948368307405598L;
	
	private Double cost;
	private String rule;
	
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
}
