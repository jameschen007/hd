package com.easycms.hd.api.exam.response;

import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ExamScoreResult extends PagenationResult{
	
	private List<ExamUserScoreResult> data;

}
