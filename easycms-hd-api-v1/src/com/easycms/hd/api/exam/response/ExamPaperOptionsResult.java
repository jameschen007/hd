package com.easycms.hd.api.exam.response;

import lombok.Data;

/**
 * 试题的选项
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class ExamPaperOptionsResult {
	/**
	 * 选项唯一ID
	 */
	private Integer id;
	/**
	 * 选项类型
	 */
	private String type;
	/**
	 * 选项内容
	 */
	private String content;
	/**
	 * 选项标号
	 */
	private String index;
}
