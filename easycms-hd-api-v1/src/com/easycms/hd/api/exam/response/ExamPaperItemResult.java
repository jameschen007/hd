package com.easycms.hd.api.exam.response;

import java.util.List;

import lombok.Data;
/**
 * 试题列表
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class ExamPaperItemResult {
	/**
	 * 唯一ID,用于回传答案时的标识
	 */
	private Integer id;
	/**
	 * 试题类型
	 */
	private String type;
	/**
	 * 试题标题
	 */
	private String subject;
	/**
	 * 试题分数
	 */
	private Double score;
	/**
	 * 试题选项
	 */
	private List<ExamPaperOptionsResult> items;
}
