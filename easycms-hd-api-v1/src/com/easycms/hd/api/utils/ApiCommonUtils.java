package com.easycms.hd.api.utils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.easycms.hd.api.response.CommonMapResult;

public class ApiCommonUtils {

	public static List<CommonMapResult> mapToCommonMapResult(Map<String, String> map) {
		if (null != map) {
			List<CommonMapResult> commonMapResults = map.entrySet().stream().map(m -> {
				CommonMapResult commonMapResult = new CommonMapResult();
				commonMapResult.setKey(m.getKey());
				commonMapResult.setValue(m.getValue());
				return commonMapResult;
			}).collect(Collectors.toList());
			return commonMapResults;
		}
		return null;
	}
}
