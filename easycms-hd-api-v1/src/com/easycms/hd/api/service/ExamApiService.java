package com.easycms.hd.api.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.hd.api.exam.request.ExamHandleRequestForm;
import com.easycms.hd.api.exam.request.ExamPaperRequestForm;
import com.easycms.hd.api.exam.request.ExamPaperSolution;
import com.easycms.hd.api.exam.response.ExamPaperHandInResult;
import com.easycms.hd.api.exam.response.ExamPaperResult;
import com.easycms.hd.api.exam.response.ExamScoreResult;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;

public interface ExamApiService {
	
	/**
	 * 获取随机试题
	 * @param form
	 * @return
	 * @throws Exception
	 */
	Map<String,Object> generateTestPaper(ExamPaperRequestForm form)  throws Exception;
	
	/**
	 * 获取所有试题
	 * @param section
	 * @param module
	 * @return
	 */
	ExamPaperResult getAllExamPaper(String section,String module) throws Exception;
	
	ExamScoreResult getExamScore(BasePagenationRequestForm form);
	
	ExamPaperHandInResult checkExamResult(ExamHandleRequestForm form,List<ExamPaperSolution> solution);

	/**
	 * 成绩列表导出
	 */
	void statisticalReport(HttpServletRequest request, HttpServletResponse response);
}
