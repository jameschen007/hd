package com.easycms.hd.api.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.request.ConferenceRequestForm;
import com.easycms.hd.api.response.ConferencePageResult;
import com.easycms.hd.api.response.ConferenceResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.management.user.domain.User;

public interface ConferenceApiService {
	ConferenceResult conferenceTransferToConferenceResult(Conference conference, Integer userId, ConferenceApiRequestType type, boolean feedback);
	
	JsonResult<Integer> insert(ConferenceRequestForm conferenceRequestForm, User user, ConferenceSource source);
	
	ConferencePageResult getConferencePageResult(ConferenceApiRequestType type, Integer userId, Page<Conference> page);

	List<ConferenceResult> conferenceTransferToConferenceResult(List<Conference> conferences, Integer userId, ConferenceApiRequestType type);

	boolean batchDelete(Integer[] ids);
	boolean batchCancel(Integer[] ids);
}
