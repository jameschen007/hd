package com.easycms.hd.api.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.easycms.common.poi.excel.ExcelColumn;
import com.easycms.common.poi.excel.ExcelHead;
import com.easycms.common.poi.excel.ExcelHelperV2;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.config.ExamConfig;
import com.easycms.hd.api.exam.request.ExamHandleRequestForm;
import com.easycms.hd.api.exam.request.ExamPaperRequestForm;
import com.easycms.hd.api.exam.request.ExamPaperSolution;
import com.easycms.hd.api.exam.response.ExamPaperHandInResult;
import com.easycms.hd.api.exam.response.ExamPaperItemResult;
import com.easycms.hd.api.exam.response.ExamPaperOptionsResult;
import com.easycms.hd.api.exam.response.ExamPaperResult;
import com.easycms.hd.api.exam.response.ExamScoreResult;
import com.easycms.hd.api.exam.response.ExamUserScoreResult;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.service.ExamApiService;
import com.easycms.hd.exam.dao.ExamPaperDao;
import com.easycms.hd.exam.domain.ExamPaper;
import com.easycms.hd.exam.domain.ExamPaperOptions;
import com.easycms.hd.exam.domain.ExamUserScore;
import com.easycms.hd.exam.enums.ExamPagerItemEnum;
import com.easycms.hd.exam.enums.ExamResultEnum;
import com.easycms.hd.exam.enums.ExamStatus;
import com.easycms.hd.exam.service.ExamPaperService;
import com.easycms.hd.exam.service.ExamUserScoreService;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearningSectionService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("examApiService")
public class ExamApiServiceImpl implements ExamApiService {
	@Autowired
	private ExamPaperService examPaperService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private ExamUserScoreService examUserScoreService;
	@Autowired
	private ExamPaperDao examPaperDao;
	@Autowired
	private ModulesService modulesService;
	@Autowired
	private LearningSectionService learningSectionService;
	@Autowired
	private ExamConfig examConfig;
	private List<ExamPaper> examPapersFromDb = new ArrayList<ExamPaper>();
	
	
	@Override
	public Map<String,Object> generateTestPaper(ExamPaperRequestForm form) throws Exception {
		Map<String,Object> res = new HashMap<>();
		ExamPaperResult examPaperResult = new ExamPaperResult();
		//试题考试时长
		examPaperResult.setDuration(Long.valueOf(examConfig.DURATION));
		//试题总分
		examPaperResult.setScore(Double.valueOf(examConfig.SCORE_TOTAL));
		//试题合格分数
		examPaperResult.setPassScore(Double.valueOf(examConfig.SCORE_PASS));

		List<ExamPaper> examPapers = new ArrayList<ExamPaper>();
		Integer sumPaperScore = 0;
		
		if(CommonUtility.isNonEmpty(form.getModule())){
			sumPaperScore = examPaperDao.sumBySectionModule(form.getSection(), form.getModule());
		}else{
			sumPaperScore = examPaperDao.sumBySection(form.getSection());			
		}
		if(sumPaperScore == null){
			sumPaperScore = 0;
		}
		if(sumPaperScore < Integer.parseInt(examConfig.EXAM_LIB_MIN_SCORE)){
			log.error("试题库试题总分少于最低限制,抽取试题失败！");
			res.put("data", null);
			res.put("msg", "试题库试题数量不足,无法完成出题！当前设置试题满分"+examConfig.SCORE_TOTAL+"，题库试题总分不得低于"+examConfig.EXAM_LIB_MIN_SCORE);
			return res;
//			examPapers = examPaperDao.findAll();
		}else{
			//分试题类型、各类试题总分数抽取试题，当前共抽取单选，多选，判断题三类，总分数100分
			//试题总分必须是单个试题分数整倍数，不然出问题。。。
			List<ExamPaper> radioExamPapers2 = new ArrayList<ExamPaper>();
			List<ExamPaper> multiExamPapers2 = new ArrayList<ExamPaper>();
			List<ExamPaper> judgeExamPapers2 = new ArrayList<ExamPaper>();
			examPapersFromDb = examPaperDao.findAll();//获取所有试题
			 Map<String,Object> radioExamPapers = generateRandomExamPaper(Integer.parseInt(examConfig.SCORE_TOTAL_RADIO),Integer.parseInt(examConfig.SCORE_SINGLE_RADIO),ExamPagerItemEnum.SINGLE,form.getSection(),form.getModule());
			 Map<String,Object> multiExamPapers = generateRandomExamPaper(Integer.parseInt(examConfig.SCORE_TOTAL_MULTIPLE),Integer.parseInt(examConfig.SCORE_SINGLE_MULTIPLE),ExamPagerItemEnum.MULTIPLE,form.getSection(),form.getModule());
			 Map<String,Object> judgeExamPapers = generateRandomExamPaper(Integer.parseInt(examConfig.SCORE_TOTAL_JUDGMENT),Integer.parseInt(examConfig.SCORE_SINGLE_JUDGMENT),ExamPagerItemEnum.YES_OR_NO,form.getSection(),form.getModule());
			if(radioExamPapers==null||multiExamPapers==null||judgeExamPapers==null){
				log.error("抽取试题数据异常！");
				res.put("data", null);
				res.put("msg", "服务器忙碌，稍后再试！");
				return res;
			}
			
			double selectedScorer = (double) radioExamPapers.get("selectScore");
			double selectedScorem = (double) multiExamPapers.get("selectScore");
			double selectedScorej = (double) judgeExamPapers.get("selectScore");
			log.error(selectedScorer+"");
			log.error(selectedScorem+"");
			log.error(selectedScorej+"");
			List<ExamPaper> rExamPapers=  (List<ExamPaper>) radioExamPapers.get("examPapers");
			List<ExamPaper> mExamPapers=  (List<ExamPaper>) multiExamPapers.get("examPapers");
			List<ExamPaper> jExamPapers=  (List<ExamPaper>) judgeExamPapers.get("examPapers");
			
			List<ExamPaper> xExamPapers = new ArrayList<ExamPaper>();
			final double score = examPaperResult.getScore() ;
			String module = form.getModule();
			boolean existMod = StringUtils.isNoneBlank(module);

			//所有总分比较单独取题
			double currentScore = selectedScorer+selectedScorem+selectedScorej;
			log.error(currentScore+"");
			int maxtime = 0;
			for(double i=(examPaperResult.getScore()-currentScore);i>0;i--){
				//找一次 差多少分的题
				List<ExamPaper> examPapersDb = null;
				if(existMod){
					examPapersDb = examPaperDao.findBySectionAndModuleAndScore(form.getSection(),module,i);
				}else{
					examPapersDb = examPaperDao.findBySectionAndScore(form.getSection(),i);
				}
				if(examPapersDb!=null && examPapersDb.size()>0){

					ExamPaper examPaper = examPapersDb.get(0);
					if(examPaper!=null && (currentScore+examPaper.getScore()) <= score  ){
						xExamPapers.add(examPaper);
						currentScore+=examPaper.getScore();
						if((currentScore+examPaper.getScore()) == score){
							log.info("成功获取试题1！！");
							break;
						}
					}
					
					//找二次　 差多少分的题
					if(examPapersDb.size()>1){
						ExamPaper examPaper2 = examPapersDb.get(1);
						if(examPaper2!=null && (currentScore+examPaper.getScore()) <= score  ){
							xExamPapers.add(examPaper2);
							currentScore+=examPaper.getScore();
							if((currentScore+examPaper.getScore()) == score){
								log.info("成功获取试题2！！");
								break;
							}
						}
					}
					if(examPapersDb.size()>2){
						ExamPaper examPaper2 = examPapersDb.get(2);
						if(examPaper2!=null && (currentScore+examPaper.getScore()) <= score  ){
							xExamPapers.add(examPaper2);
							currentScore+=examPaper.getScore();
							if((currentScore+examPaper.getScore()) == score){
								log.info("成功获取试题3！！");
								break;
							}
						}
					}
				}

				log.error("当前总分："+currentScore+"");
				if(i==1){
					maxtime++;
					if(maxtime>5){
						log.info("获取试题困难！！");
						break;
					}
				}
			}
			
			examPapers.addAll(rExamPapers);
			examPapers.addAll(mExamPapers);
			examPapers.addAll(jExamPapers);	
			examPapers.addAll(xExamPapers);		
		}
		
		for(ExamPaper e : examPapers){
			log.debug(e.getId() + " - " + e.getSubject());
		}
		
		List<ExamPaperItemResult> examPaperItemResults = examPapers.stream().map(mapper ->{
			return transferForResult(mapper);
		}).collect(Collectors.toList());
		
		//已抽取试题集合
		examPaperResult.setResults(examPaperItemResults);
		res.put("data", examPaperResult);
		return res;
	}

	@Override
	public ExamScoreResult getExamScore(BasePagenationRequestForm form) {
		ExamScoreResult examScoreResult = new ExamScoreResult();
		Page<ExamUserScore> page = new Page<ExamUserScore>();
		if (null != form.getPagenum() && null != form.getPagesize()) {
			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
		}
		
		page = examUserScoreService.findByUserAndExamStatus(form.getLoginId(), ExamStatus.COMPLETED.toString(), page);
		
		examScoreResult.setPageCounts(page.getPageCount());
		examScoreResult.setPageNum(page.getPageNum());
		examScoreResult.setPageSize(page.getPagesize());
		examScoreResult.setTotalCounts(page.getTotalCounts());
		
		if (null != page.getDatas()) {
			examScoreResult.setData(page.getDatas().stream().map(exam -> {
				ExamUserScoreResult result = new ExamUserScoreResult();
				result.setExamDate(exam.getExamStartDate());
				
				//返回考试类型
				String type = null;
				Modules module = null;
				LearningSection learningSection = null;
				if(exam.getLearningFold()!=null){
					if(exam.getLearningFold().getModule()!=null){
						module = modulesService.findByType(exam.getLearningFold().getModule());
						type = module.getName();
					}else if(exam.getLearningFold().getSection()!=null){
						learningSection = learningSectionService.findByType(exam.getLearningFold().getSection());
					}
				}else if(exam.getExamType() != null){
					learningSection = learningSectionService.findByType(exam.getExamType());
				}
				if(learningSection != null){
					type = learningSection.getName();
				}else if(module != null){
					type = module.getName();
				}
				result.setExamType(type);					

				result.setExamScore(String.valueOf(exam.getExamScore()));
				result.setExamResult(exam.getExamResult());
				return result;
			}).collect(Collectors.toList()));
		}
		
		return examScoreResult;
	}

	@Override
	public ExamPaperHandInResult checkExamResult(ExamHandleRequestForm form, List<ExamPaperSolution> solution) {
		ExamPaperHandInResult examUserScoreRs = new ExamPaperHandInResult();
		double userScore = 0d;
		for(ExamPaperSolution eps : solution){
			ExamPaper examPaper = examPaperService.findById(eps.getSubjectId());
			List<ExamPaperOptions> examItems = examPaper.getExamPaperOptions();
			Set<Integer> standardAnswer = new HashSet<Integer>();
			Set<Integer> userAnswer = eps.getItemId();
			for(ExamPaperOptions epo : examItems){
				if("Y".equals(epo.getCorrect())){
					standardAnswer.add(epo.getId());
				}
			}
			userScore += generateScores(userAnswer,standardAnswer,examPaper.getScore());
		}
		
		if(userScore < Integer.parseInt(examConfig.SCORE_PASS)){
			examUserScoreRs.setResult(ExamResultEnum.FAIL.getName());
		}else{
			examUserScoreRs.setResult(ExamResultEnum.PASS.getName());
		}
		log.debug("用户得分：" + userScore);
		examUserScoreRs.setScore(userScore);
		return examUserScoreRs;
	}
	
	
	/**
	 * 按试题类型、各类试题总分数抽取试题，试题总分必须是单个试题分数整倍数
	 * @param totalScore 抽取总分 
	 * @param singleScore 单个试题分数
	 * @param paperType 试题类型
	 * @return
	 */
	private Map<String,Object> generateRandomExamPaper(Integer totalScore,Integer singleScore, ExamPagerItemEnum paperType,String section,String module){
		Map<String,Object> result = new HashMap<String,Object>();
		
		
		
		List<ExamPaper> examPapers = new ArrayList<ExamPaper>();
		
//		examPapersFromDb = examPaperDao.findAll();//获取所有试题
		
		Integer paperNum = examPaperDao.maxIdBySection(section);
		Integer maxPaperId = 0;
		if(paperNum==null || paperNum==0){
			log.error("试题库"+section+"板块试题数量为空！");
			return null;
		}
		maxPaperId = examPaperDao.maxIdByType(paperType.toString());//最大试题编号
		int maxPaperNum = 0;//最多试题数量
		maxPaperNum = totalScore / singleScore;
		Integer[] arrSelected = new Integer[maxPaperNum];//抽取试题编号数组
		
		double selectedScore = 0d;//已抽取试题总分
		Long timeout = System.currentTimeMillis();
		
		log.debug("##########################抽取试题开始！##########################");
		A:for(int i=0; i<maxPaperNum; i++){
			arrSelected[i] = (int)(Math.random() * maxPaperId) + 1;
			for(int j=0;j<i;j++){
				if(arrSelected[j].compareTo(arrSelected[i]) == 0 ){
					i--;
					continue A;
				}
			}
			ExamPaper examPaper = null;
			//传入module不为空，并且与试题module不匹配，不抽取该题
//			if(CommonUtility.isNonEmpty(module)){
//				examPaper = examPaperDao.findByIdAndSectionAndModule(arrSelected[i], section,module,paperType.toString());
//			}else{
//				examPaper = examPaperDao.findByIdAndSection(arrSelected[i], section,paperType.toString());
//			}
			
			//优化过滤试题
			examPaper = examPaperfilter(examPapersFromDb,arrSelected[i], section,module,paperType.toString());
			

			if(examPaper == null){
				i--;
			}else{
				selectedScore += examPaper.getScore();
				log.debug("随机"+paperType.getName()+"题编号：" + arrSelected[i]+" - 当前已抽取试题总分数：" + selectedScore);
				if(selectedScore == totalScore){
					log.debug("##########################抽取试题结束！##########################");
					break;
				}else if(selectedScore > totalScore-(singleScore-1) ){
					log.debug("##########################抽取试题结束！##########################");
					break;
				}else if(selectedScore > totalScore-singleScore ){
					log.debug("##########################抽取试题结束！##########################");
					break;
				}
			}
			if(System.currentTimeMillis()-timeout>5000){//超时5秒，返回空
				log.error("抽取试题超时！！！");
				return null;
			}
		}
		//去除数组NULL数据
		List<Integer> strListNew= new ArrayList<Integer>();
		List<Integer> strList = Arrays.asList(arrSelected);
		String paperId = "";
        for (int i = 0; i <strList.size(); i++) {
            if (strList.get(i)!=null&&!strList.get(i).equals("")){
            	strListNew.add(strList.get(i));
            }
            paperId += strList.get(i) + ",";
        }
//        Integer[] strNewArray = strListNew.toArray(new Integer[strListNew.size()]);
//		examPapers = examPaperDao.findByIds(strNewArray);
		log.debug("抽取"+paperType.getName()+"题完成,编号："+paperId);

		examPapers = examPaperfilters(examPapersFromDb,strList);
		result.put("examPapers", examPapers);
		result.put("selectScore", selectedScore);
		return result;
	}
	
	/**
	 * 过滤试题
	 * @param papers
	 * @param id
	 * @param section
	 * @param module
	 * @param type
	 * @return
	 */
	public static ExamPaper examPaperfilter(List<ExamPaper> papers, Integer id,String section,String module,String type){
//		ExamPaper exampaper = new ExamPaper();
		List<ExamPaper> pas = new ArrayList<>();
		pas = papers.stream().filter(paper -> {
			if(paper.getId().equals(id)){
				if(CommonUtility.isNonEmpty(module)){
					return paper.getSection().equals(section)&&paper.getModule().equals(module)&&paper.getType().equals(type);
				}else{
					return paper.getSection().equals(section)&&paper.getType().equals(type);
				}
			}else{
				return false;
			}
		}).collect(Collectors.toList());
		if(!pas.isEmpty()&&pas.size()>0){
			return pas.get(0);
		}else{
			return null;
		}
	}
	
	/**
	 * 获取最终试题
	 * @param papers
	 * @param ids
	 * @param section
	 * @param module
	 * @param type
	 * @return
	 */
	public static List<ExamPaper> examPaperfilters(List<ExamPaper> papers, List<Integer> ids){
		List<ExamPaper> selectPaper = new ArrayList<>();
		selectPaper = papers.stream().filter(paper -> {
			return ids.contains(paper.getId());
		}).collect(Collectors.toList());
		
		return selectPaper;
	}
	
	public static void main(String[] args) {
		Integer a[] = new Integer[10];
		C:for(int i=0;i<10;i++){
			a[i] = (int)(Math.random()*100)+1;
			System.out.println(a[i]);
			for(int j=0;j<i;j++){
				if(a[i]==a[j]){
					System.out.println("                  "+a[i]+" : "+a[j]);
					i--;
					continue C;
				}
			}
			
		}
		System.out.println("长度"+a.length+" ");
		for(int c : a){
			System.out.print(c+" ");
		}
	}
	
	
	/**
	 * 试题转换为返回数据
	 * @param examPaper
	 * @return
	 */
	private ExamPaperItemResult transferForResult(ExamPaper examPaper){
		ExamPaperItemResult epiResult = new ExamPaperItemResult();
		List<ExamPaperOptionsResult> items = new ArrayList<ExamPaperOptionsResult>();
		epiResult.setId(examPaper.getId());
		epiResult.setScore(examPaper.getScore());
		epiResult.setSubject(examPaper.getSubject());
		epiResult.setType(examPaper.getType());
		if(!examPaper.getExamPaperOptions().isEmpty()){
			for(ExamPaperOptions epo : examPaper.getExamPaperOptions()){
				ExamPaperOptionsResult epor = new ExamPaperOptionsResult();
				epor.setId(epo.getId());
				epor.setContent(epo.getContent());
				epor.setIndex(epo.getNumber());
				epor.setType(epo.getType());
				items.add(epor);
			}
		}
		epiResult.setItems(items);
		return epiResult;
	}
	
	/**
	 * 计算考试得分
	 * @param userAnswer
	 * @param standardAnswer
	 * @param systemScore
	 * @return
	 */
	private double generateScores(Set<Integer> userAnswer,Set<Integer> standardAnswer,double systemScore){
		double userScore = 0d;
		if(userAnswer.isEmpty()){
			log.debug("用户答案为空！");
			return userScore;
		}else if(standardAnswer.isEmpty()){
			log.error("###########试题标准答案数据为空！###########");
			return userScore;
		}
		if(standardAnswer.size() == 1){//正确答案只有一个，单选题或判断题否则为多选题
			if(userAnswer.size() == standardAnswer.size() && userAnswer.equals(standardAnswer)){
				userScore = systemScore;
			}
		}else{
			if(userAnswer.size() < standardAnswer.size()){//用户提交的答案不完整
//				boolean flag = true;
//				for(Integer ua : userAnswer){
//					if(!standardAnswer.contains(ua)){//如果用户的答案中有一个不是标准答案，则得0分
//						flag = false;
//					}
//				}
//				if(flag){
//					userScore = systemScore/2;//答案不全，得试题分数的一半
//				}
			}else if(userAnswer.size() == standardAnswer.size()){
				boolean flag = true;
				for(Integer ua : userAnswer){
					if(!standardAnswer.contains(ua)){//如果用户的答案中有一个不是标准答案，则得0分
						flag = false;
					}
				}
				if(flag){
					userScore = systemScore;
				}
			}
		}
		log.debug("本题得分：" + userScore);
		return userScore;
	}

	/**
	 * 成绩列表导出
	 */
	@Override
	public void statisticalReport(HttpServletRequest request, HttpServletResponse response) {

		try{
	        List<ExamUserScore> list= examUserScoreService.findByUserAndExamStatus( ExamStatus.COMPLETED.toString());
			for(int i=0;i<list.size();i++){
				ExamUserScore b = list.get(i);
				b.setId(i+1);
			}
			List<ExamUserScoreResult> bList = new ArrayList<ExamUserScoreResult>();

			bList = list.stream().map(exam -> {
				ExamUserScoreResult result = new ExamUserScoreResult();
				result.setExamDate(exam.getExamStartDate());
				
				//返回考试类型
				String type = null;
				Modules module = null;
				LearningSection learningSection = null;
				if(exam.getLearningFold()!=null){
					if(exam.getLearningFold().getModule()!=null){
						module = modulesService.findByType(exam.getLearningFold().getModule());
						type = module.getName();
					}else if(exam.getLearningFold().getSection()!=null){
						learningSection = learningSectionService.findByType(exam.getLearningFold().getSection());
					}
				}else if(exam.getExamType() != null){
					learningSection = learningSectionService.findByType(exam.getExamType());
				}
				if(learningSection != null){
					type = learningSection.getName();
				}else if(module != null){
					type = module.getName();
				}
				result.setExamType(type);		
				result.setRealname(userService.findRealnameById(exam.getUserId()));

				result.setExamScore(String.valueOf(exam.getExamScore()));
				result.setExamResult(exam.getExamResult());
				result.setId(exam.getId());
				return result;
			}).collect(Collectors.toList());
		
			//获取模板文件
			String tempFlieName = "scoreTemplate.xls";
	    	InputStream in = this.getClass().getClassLoader().getResourceAsStream(tempFlieName);
			
			//设置Excel每列显示内容
			List<ExcelColumn> columns = new ArrayList<ExcelColumn>();
			

			columns.add(new ExcelColumn(0,"id","序号"));
			columns.add(new ExcelColumn(1,"realname","姓名"));
			columns.add(new ExcelColumn(2,"examDate","考试日期"));
			columns.add(new ExcelColumn(3,"examType","考试类别"));
			columns.add(new ExcelColumn(4,"examScore","考试得分"));
			columns.add(new ExcelColumn(5,"examResult","及格情况"));
			
			ExcelHead head = new ExcelHead();
			head.setColumns(columns);
			head.setColumnCount(columns.size());
			head.setRowCount(2);

			//写入数据
			ExcelHelperV2.getInstanse().exportExcelFile(head, in, response.getOutputStream(), bList,"yyyy-MM-dd HH:mm:ss");
		}catch(Exception e){
			log.error("数据写入文件出错！"+e.getMessage());
			e.printStackTrace();
		}
	
		
	}

	@Override
	public ExamPaperResult getAllExamPaper(String section, String module) throws Exception{
		log.debug("开始获取"+section+"版块"+module+"板块下所有试题");
		ExamPaperResult examPaperResult = new ExamPaperResult();
		Specification<ExamPaper> spec = new Specification<ExamPaper>() {
			@Override
			public Predicate toPredicate(Root<ExamPaper> arg0, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				Path<String> psection = arg0.get("section");
				Path<String> pmodule = arg0.get("module");
				Predicate pre = cb.equal(psection, section);
				if(CommonUtility.isNonEmpty(module)){
					return cb.and(pre, cb.equal(pmodule, module));					
				}
				return pre;
			}
		};
		List<ExamPaper> examPapers = examPaperDao.findAll(spec);
		List<ExamPaperItemResult> examPaperItemResults = examPapers.stream().map(mapper ->{
			return transferForResult(mapper);
		}).collect(Collectors.toList());
		
		Double score = 0d;
		for(ExamPaper e : examPapers){
//			log.debug("试题id："+e.getId());
			score += e.getScore();
		}
		
		examPaperResult.setScore(score);
		examPaperResult.setPassScore(Double.valueOf(examConfig.SCORE_PASS));
		examPaperResult.setDuration(Long.valueOf(examConfig.DURATION));
		examPaperResult.setResults(examPaperItemResults);
		return examPaperResult;
	}
}
