package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.hd.api.domain.AuthDept;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.domain.Statistics;
import com.easycms.hd.api.enums.ExtraTypeEnum;
import com.easycms.hd.api.enums.QC2MemberTypeEnum;
import com.easycms.hd.api.response.DepartmentResult;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsMainResult;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsResult;
import com.easycms.hd.api.response.statistics.ExtraResult;
import com.easycms.hd.api.response.statistics.MaterialDepartmentStatisticsResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsMainResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsResult;
import com.easycms.hd.api.response.statistics.ProblemStatisticsResult;
import com.easycms.hd.api.response.statistics.RollingPlanStatisticsResult;
import com.easycms.hd.api.response.statistics.StatisticsMainResult;
import com.easycms.hd.api.response.statistics.StatisticsReportResult;
import com.easycms.hd.api.response.statistics.StatisticsUserResult;
import com.easycms.hd.api.response.statistics.WitnessQC2StatisticsResult;
import com.easycms.hd.api.response.statistics.WitnessStatisticsResult;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.api.service.StatisticsApiService;
import com.easycms.hd.material.dao.MaterialExtractDao;
import com.easycms.hd.plan.mybatis.dao.ConferenceStatisticsMybatisDao;
import com.easycms.hd.plan.mybatis.dao.RollingPlanStatisticsMybatisDao;
import com.easycms.hd.plan.mybatis.dao.RollingQuestionMybatisDao;
import com.easycms.hd.plan.mybatis.dao.WitnessStatisticsMybatisDao;
import com.easycms.hd.plan.mybatis.dao.bean.StatisticsBean;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.DepartmentService;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserService;

@Service("statisticsApiService")
public class StatisticsApiServiceImpl implements StatisticsApiService {
	
	public static Logger logger = Logger.getLogger(StatisticsApiServiceImpl.class);

	@Autowired
	private UserService userService;
	@Autowired
	private RoleApiService roleApiService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private DepartmentApiService departmentApiService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private RollingQuestionMybatisDao rollingQuestionMybatisDao;
	@Autowired
	private RollingPlanStatisticsMybatisDao rollingPlanStatisticsMybatisDao;
	@Autowired
	private WitnessStatisticsMybatisDao witnessStatisticsMybatisDao;
	@Autowired
	private ConferenceStatisticsMybatisDao conferenceStatisticsMybatisDao;
	@Autowired
	private MaterialExtractDao materialExtractDao;
	@Autowired
	private RoleService roleService;
	
	@SuppressWarnings("unchecked")
	@Override
	public StatisticsMainResult<RollingPlanStatisticsResult> getRollingPlanStatisticsResult(User loginUser,String moduleType,Integer usingRoleId) {
		return (StatisticsMainResult<RollingPlanStatisticsResult>)this.getStatistic(loginUser, "RollingPlan",moduleType, null, usingRoleId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public StatisticsMainResult<ProblemStatisticsResult> getProblemStatisticsResult(User loginUser,String moduleType,Integer usingRoleId) {
		return (StatisticsMainResult<ProblemStatisticsResult>)this.getStatistic(loginUser, "Problem",moduleType, null, usingRoleId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public StatisticsMainResult<ProblemStatisticsResult> getProblemStatisticsResultTwo(User loginUser,String moduleType,Integer usingRoleId) {
		return (StatisticsMainResult<ProblemStatisticsResult>)this.getStatisticTwo(loginUser, "Problem",moduleType, usingRoleId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public StatisticsMainResult<WitnessStatisticsResult> getWitnessStatisticsResult(User loginUser,String moduleType, QC2MemberTypeEnum memberType,Integer usingRoleId) {
		return (StatisticsMainResult<WitnessStatisticsResult>)this.getStatistic(loginUser, "Witness",moduleType, memberType, usingRoleId);
	}
	
	@Override
	public ConferenceStatisticsMainResult<ConferenceStatisticsResult> getConferenceStatisticsResult(Integer userId) {
		Statistics statistics = new Statistics();
		statistics.setUserId(userId);
		
		ConferenceStatisticsMainResult<ConferenceStatisticsResult> conferenceStatisticsMainResult = new ConferenceStatisticsMainResult<ConferenceStatisticsResult>();
		ConferenceStatisticsResult conference = new ConferenceStatisticsResult();
		conference.setDraft(conferenceStatisticsMybatisDao.conferenceDraftCount(statistics));
		conference.setReceive(conferenceStatisticsMybatisDao.conferenceReceiveCount(statistics));
		conference.setSend(conferenceStatisticsMybatisDao.conferenceSendCount(statistics));
		conference.setSendExpired(conferenceStatisticsMybatisDao.conferenceSendExpiredCount(statistics));
		conference.setSendProcessing(conferenceStatisticsMybatisDao.conferenceSendProcessingCount(statistics));
		conference.setSendUnStarted(conferenceStatisticsMybatisDao.conferenceSendUnStartedCount(statistics));
		conference.setReceiveExpired(conferenceStatisticsMybatisDao.conferenceReceiveExpiredCount(statistics));
		conference.setReceiveProcessing(conferenceStatisticsMybatisDao.conferenceReceiveProcessingCount(statistics));
		conference.setReceiveUnStarted(conferenceStatisticsMybatisDao.conferenceReceiveUnStartedCount(statistics));
		conference.setReceiveUnread(conferenceStatisticsMybatisDao.conferenceTotalReceiveUnReadFeedback(statistics));
		conference.setSendUnread(conferenceStatisticsMybatisDao.conferenceTotalSendUnReadFeedback(statistics));
		
		ConferenceStatisticsResult notification = new ConferenceStatisticsResult();
		notification.setDraft(conferenceStatisticsMybatisDao.notificationDraftCount(statistics));
		notification.setReceive(conferenceStatisticsMybatisDao.notificationReceiveCount(statistics));
		notification.setSend(conferenceStatisticsMybatisDao.notificationSendCount(statistics));
		notification.setSendExpired(conferenceStatisticsMybatisDao.notificationSendExpiredCount(statistics));
		notification.setSendProcessing(conferenceStatisticsMybatisDao.notificationSendProcessingCount(statistics));
		notification.setSendUnStarted(conferenceStatisticsMybatisDao.notificationSendUnStartedCount(statistics));
		notification.setReceiveExpired(conferenceStatisticsMybatisDao.notificationReceiveExpiredCount(statistics));
		notification.setReceiveProcessing(conferenceStatisticsMybatisDao.notificationReceiveProcessingCount(statistics));
		notification.setReceiveUnStarted(conferenceStatisticsMybatisDao.notificationReceiveUnStartedCount(statistics));
		notification.setReceiveUnread(conferenceStatisticsMybatisDao.notificationTotalReceiveUnReadFeedback(statistics));
		notification.setSendUnread(conferenceStatisticsMybatisDao.notificationTotalSendUnReadFeedback(statistics));
		
		conferenceStatisticsMainResult.setConference(conference);
		conferenceStatisticsMainResult.setNotification(notification);
		
		return conferenceStatisticsMainResult;
	}
	
	@Override
	public MaterialStatisticsResult getMaterialStatisticsResult(Integer loginId) {
		MaterialStatisticsResult materialStatisticsResult = new MaterialStatisticsResult();
		materialStatisticsResult.setInput(143L);
		materialStatisticsResult.setOutput(54L);
		return materialStatisticsResult;
	}
	
	@Override
	public MaterialStatisticsMainResult getMaterialStatisticsMainResult(Integer loginId) {
		MaterialStatisticsMainResult materialStatisticsMainResult = new MaterialStatisticsMainResult();
		List<Integer> departmentIds = materialExtractDao.findAllDepartmentId();
		
		List<Department> departments = departmentIds.stream().map(d -> {
			return departmentService.findById(d);
		}).collect(Collectors.toList());
		List<DepartmentResult> departmentResults = departmentApiService.departmentTransferToDepartmentResult(departments);
		
		List<MaterialDepartmentStatisticsResult> departmentStatisticsResults = departmentResults.stream().map(dep -> {
			MaterialDepartmentStatisticsResult materialDepartmentStatisticsResult = new MaterialDepartmentStatisticsResult();
			materialDepartmentStatisticsResult.setDepartment(dep);
			materialDepartmentStatisticsResult.setOutput((long)(new Random().nextInt(500) % 173));
			return materialDepartmentStatisticsResult;
		}).collect(Collectors.toList());
		
		Long total = departmentStatisticsResults.stream().map(d -> {return d.getOutput();}).reduce((a, b) -> a + b).get();
		
		materialStatisticsMainResult.setDepartments(departmentStatisticsResults);
		materialStatisticsMainResult.setTotal(total);
		
		return materialStatisticsMainResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private StatisticsMainResult<?> getStatistic(User loginUser, String type, String moduleType, QC2MemberTypeEnum memberType,Integer usingRoleId) {
		List<Role> roles = loginUser.getRoles();
		if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
			roles = roleService.findListById(roles,usingRoleId);
		}
		logger.info(loginUser.getId()+" "+roles.size());
		// 修改查询下级人员的逻辑，根据部门去查询
//		List<User> childrenUsers = userService.findUserByParentId(loginUser.getId());
		List<User> childrenUsers = null;
		
		if (null != roles) {
			StatisticsMainResult<RollingPlanStatisticsResult> rollingPlanMainResult = new StatisticsMainResult<RollingPlanStatisticsResult>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> captainResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> monitorResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> witnessTeamResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> witnessQC2Result = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> supervisorResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			//[2017-12-27]
			Set<String> roleNameList = new HashSet<String>();
			for (Role role : roles) {
				logger.info(role.getName());
				roleNameList.addAll(variableSetService.findKeyByValue(role.getName(), "roleType"));
				
			}
				String loginis = null;
				if (roleNameList.contains("captain")) {
					loginis ="captain" ;
					//当前登录的人是队长，这里就查下面的班长
					childrenUsers = userService.findUserByDeptParentIdAndRoleName(loginUser.getId(),ConstantVar.MONITOR);
				}else if (roleNameList.contains(ConstantVar.MONITOR)) { 
					loginis =ConstantVar.MONITOR ;
					//当前登录的人是班长，这里就查下面的组长
					childrenUsers = userService.findUserByDeptParentIdAndRoleName(loginUser.getId(),ConstantVar.TEAM);
				} else {
//					childrenUsers = userService.findUserByDeptParentId(loginUser.getId());
					//qc2组员查询其它三个部门角色的人员

					logger.info("qc2组员查询其它三个部门角色的人员 ====如果不是，那王志需要再看原因");
					String qcDepartment = variableSetService.findValueByKey("departmentQc", "departmentId");
					Integer qcDepartmentId= Integer.valueOf(qcDepartment);
					
					if (roleNameList.contains("witness_team_qc1")) { 
						//当前登录的人是QC1组长
						childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_qc1"});
					} else if (roleNameList.contains("witness_team_qc2")) { 
						//当前登录的人是QC2组长
						childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_qc2"});
					} else {
						childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(qcDepartmentId).getChildren(), new String[]{"witness_member_czecqc","witness_member_czecqa","witness_member_paec"});
					}
				}

				if (roleNameList.contains("supervisor")) {//管道主管  2 作业流程表中是第４类处理人
					loginis =ConstantVar.supervisor ;
					//[2017-12-27]注释部分为查固定部门下的技术人员
//					childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(358).getChildren(), new String[]{"solver"});
					//[2017-12-27]分专业查技术人员，如管道计划则查找管道技术员
					childrenUsers = userService.findUserByDepartmentVariable("roleType",ConstantVar.solver,moduleType);
				}
				if (roleNameList.contains(ConstantVar.solver3)) {//项目副经理３
					loginis =ConstantVar.solver3 ;
//					childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(358).getChildren(), new String[]{ConstantVar.supervisor});
					childrenUsers = userService.findUserByDepartmentVariable("roleType",ConstantVar.supervisor,moduleType);
				}
				if (roleNameList.contains(ConstantVar.solver4)) {//项目总经理４
					loginis =ConstantVar.solver4 ;
					childrenUsers = userService.findUserByDepartmentVariable("roleType",ConstantVar.solver3,moduleType);
				}
				if (roleNameList.contains(ConstantVar.solver5)) {//项目副经理５预留
					loginis =ConstantVar.solver5 ;
					childrenUsers = userService.findUserByDepartmentAndRoleNames(departmentService.findById(281).getChildren(), new String[]{ConstantVar.solver4});
				}
				if(roleNameList.contains(ConstantVar.COORDINATOR)) {//协调人员查询
					return getStatisticTwo(loginUser,type,moduleType, usingRoleId);
				}
				if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
					type = "WitnessQC2";//标识需要统计czecqc
					
					List<ExtraResult> extras = new ArrayList<ExtraResult>();
					ExtraResult extra = new ExtraResult();
					extra.setType(ExtraTypeEnum.QC2_UNASSIGN);
					
					if (null == memberType) {
						return null;
					}
					
					Statistics statistics = new Statistics();
					statistics.setType(moduleType);
					statistics.setUserId(loginUser.getId());
					statistics.setQc2Type(memberType.name());
					
					extra.setResult(witnessStatisticsMybatisDao.qc2MemberUnAssignCount(statistics));
					extras.add(extra);
					rollingPlanMainResult.setExtra(extras);
					if (null != childrenUsers) {
						childrenUsers = childrenUsers.stream().filter(children -> {
							Set<AuthRole> authRoles = roleApiService.getAuthRole(children,false);//rollingPlan等需要验证角色时，设置false
							Set<String> authRolesSet = this.convertAuthRoleToList(authRoles);
							if (null != memberType) {
								if (memberType.equals(QC2MemberTypeEnum.CZEC_QA)) {
									if (authRolesSet.contains("witness_member_czecqa")) {
										return true;
									}
								} else if (memberType.equals(QC2MemberTypeEnum.CZEC_QC)) {
									if (authRolesSet.contains("witness_member_czecqc")) {
										return true;
									}
								} else if (memberType.equals(QC2MemberTypeEnum.PAEC)) {
									if (authRolesSet.contains("witness_member_paec")) {
										return true;
									}
								}
							}
							return false;
						}).collect(Collectors.toList());
					}
					
				}
				final String whoLogin = loginis; 
				if (null != childrenUsers) {
						for (User user : childrenUsers) {
							Set<AuthRole> authRoles = roleApiService.getAuthRole(user,false);//rollingPlan等需要验证角色时，设置false
							Set<String> authRolesSet = this.convertAuthRoleToList(authRoles);
							if (null != authRolesSet) {
//								for (String authRole : authRolesSet) {
									StatisticsUserResult userResult = new StatisticsUserResult();
									userResult.setId(user.getId());
									userResult.setName(user.getName());
									userResult.setRealname(user.getRealname());
									userResult.setRoles(authRoles);
									if (null != user.getDepartment()) {
										AuthDept authDept = new AuthDept();
										authDept.setId(user.getDepartment().getId());
										authDept.setName(user.getDepartment().getName());
										userResult.setDept(authDept);
									}
									StatisticsReportResult rollingPlanReportResult = new StatisticsReportResult();
									if (type.equals("RollingPlan")) {
										RollingPlanStatisticsResult rollingPlanStatisticsResult = this.getRollingPlanStatistic(user.getId(), authRolesSet, moduleType);
										rollingPlanReportResult.setStatistics(rollingPlanStatisticsResult);
									} else if (type.equals("Problem")) {
										if(whoLogin == null){
											throw new RuntimeException("当前登录人员的权限未知！"+ type+" "+moduleType+" "+memberType);
										}
										ProblemStatisticsResult rollingPlanStatisticsResult = this.getProblemStatisticsResult(user.getId(),whoLogin,moduleType,null);
										rollingPlanReportResult.setStatistics(rollingPlanStatisticsResult);
									} else if (type.equals("Witness")) {
										WitnessStatisticsResult witnessStatisticsResult = this.getWitnessStatisticsResult(user.getId(), authRolesSet, moduleType);
										rollingPlanReportResult.setStatistics(witnessStatisticsResult);
									} else if (type.equals("WitnessQC2")) {
										WitnessQC2StatisticsResult witnessQC2StatisticsResult = this.getWitnessQC2StatisticsResult(user.getId(), authRolesSet, moduleType, memberType);
										rollingPlanReportResult.setStatistics(witnessQC2StatisticsResult);
									}
									rollingPlanReportResult.setUser(userResult);
									
									if (roleNameList.contains("captain")) {
										captainResult.add(rollingPlanReportResult);
									}
									if (roleNameList.contains("monitor")) {
										monitorResult.add(rollingPlanReportResult);
									}
									if (roleNameList.contains("witness_team_qc1") || roleNameList.contains("witness_team_qc2")) {
										witnessTeamResult.add(rollingPlanReportResult);
									}
									if ( ComputedConstantVar.qc2OrFqQc1(roleNameList) ) {
										witnessQC2Result.add(rollingPlanReportResult);
									}
									if (roleNameList.contains(ConstantVar.supervisor)
											|| roleNameList.contains(ConstantVar.solver3)
											|| roleNameList.contains(ConstantVar.solver4)
											|| roleNameList.contains(ConstantVar.solver5)
											|| roleNameList.contains(ConstantVar.solver6)
											|| roleNameList.contains(ConstantVar.solver7)) {//管道主管
										supervisorResult.add(rollingPlanReportResult);
									}
//								}
							}
						}
				}
			
			rollingPlanMainResult.setCaptain(captainResult);
			rollingPlanMainResult.setMonitor(monitorResult);
			rollingPlanMainResult.setWitness_team(witnessTeamResult);
			rollingPlanMainResult.setWitness_qc2(witnessQC2Result);
			rollingPlanMainResult.setSupervisor(supervisorResult);
			return rollingPlanMainResult;
		}
		return null;
	}
	
	private RollingPlanStatisticsResult getRollingPlanStatistic(Integer userId, Set<String> role, String type) {
		RollingPlanStatisticsResult rollingPlanStatisticsResult = new RollingPlanStatisticsResult();
		
		Statistics statistics = new Statistics();
		statistics.setType(type);
		statistics.setUserId(userId);
		
		//TODO 返回的数据为假数据
		if (role.contains("monitor")) {
			rollingPlanStatisticsResult.setCompleted(rollingPlanStatisticsMybatisDao.monitorCompletedCount(statistics));
			rollingPlanStatisticsResult.setProgressing(rollingPlanStatisticsMybatisDao.monitorProgressingCount(statistics));
			rollingPlanStatisticsResult.setPause(rollingPlanStatisticsMybatisDao.monitorPauseCount(statistics));
			rollingPlanStatisticsResult.setUnassign(rollingPlanStatisticsMybatisDao.monitorUnAssignCount(statistics));
			//队长看班长的未施工
			rollingPlanStatisticsResult.setUnProgressing(rollingPlanStatisticsMybatisDao.monitorUnProgressing(statistics));
			rollingPlanStatisticsResult.setTotal(rollingPlanStatisticsResult.getCompleted() + 
					rollingPlanStatisticsResult.getProgressing() + 
					rollingPlanStatisticsResult.getPause() + rollingPlanStatisticsResult.getUnProgressing()+
					rollingPlanStatisticsResult.getUnassign());
			rollingPlanStatisticsResult.setMark("monitor");
		} else if (role.contains("team")) {
			rollingPlanStatisticsResult.setCompleted(rollingPlanStatisticsMybatisDao.teamCompletedCount(statistics));
			rollingPlanStatisticsResult.setProgressing(rollingPlanStatisticsMybatisDao.teamProgressingCount(statistics));
			rollingPlanStatisticsResult.setPause(rollingPlanStatisticsMybatisDao.teamPauseCount(statistics));
			rollingPlanStatisticsResult.setUnProgressing(rollingPlanStatisticsMybatisDao.teamUnProgressingCount(statistics));
			rollingPlanStatisticsResult.setUnassign(0L);
			rollingPlanStatisticsResult.setTotal(rollingPlanStatisticsResult.getCompleted() + 
					rollingPlanStatisticsResult.getProgressing() + 
					rollingPlanStatisticsResult.getPause() + rollingPlanStatisticsResult.getUnProgressing()+
					rollingPlanStatisticsResult.getUnassign());
			rollingPlanStatisticsResult.setMark("team");
		} else {
			return rollingPlanStatisticsResult;
		}
//		rollingPlanStatisticsResult.setMark(role);
		return rollingPlanStatisticsResult;
	}
	private ProblemStatisticsResult getProblemStatisticsResultForCoordinate(StatisticsBean problem) {
		ProblemStatisticsResult problemStatisticsResult = new ProblemStatisticsResult();
		if(problem!=null){
			int so = problem.getSolved()==null?0:problem.getSolved();// 已指派统计量
			int unso = problem.getUnsolved()==null?0:problem.getUnsolved();// 待指派统计量
			problemStatisticsResult.setSolved(so);// 当协调人员查看班长的统计数量时。已指派统计量
			problemStatisticsResult.setNeedAssign(unso);// 当协调人员查看班长的统计数量时。待指派统计量
			problemStatisticsResult.setTotal(so+unso);// 总数量
		}
		if(problemStatisticsResult.getTotal()==null){
			problemStatisticsResult.setSolved(0);// 已指派统计量
			problemStatisticsResult.setNeedAssign(0);// 待指派统计量
			problemStatisticsResult.setTotal(0);// 总数量
		}
		return problemStatisticsResult;
	}
	/**
	 * 根据统计输出 设计result
	 * @param userId
	 * @param role
	 * @param moduleType
	 * @return
	 */
	private ProblemStatisticsResult getProblemStatisticsResult(Integer userId, String role,String moduleType, List<StatisticsBean> problemxx) {
		ProblemStatisticsResult problemStatisticsResult = new ProblemStatisticsResult();
		
//		if (role.equals("monitor")) {//班长登录看组长的统计
			StatisticsBean problem =  problemStatistics(role,userId, moduleType);
			if(problem!=null){
				int so = problem.getSolved()==null?0:problem.getSolved();// 已解决的问题数量
				int unso = problem.getUnsolved()==null?0:problem.getUnsolved();// 未解决的问题数量
				int done = problem.getDone()==null?0:problem.getDone();//已处理
				int pre = problem.getPre()==null?0:problem.getPre();//待处理
				int reply = problem.getReply()==null?0:problem.getReply();//已回执
				problemStatisticsResult.setSolved(so);// 已解决的问题数量
				problemStatisticsResult.setUnsolved(unso);// 未解决的问题数量
				problemStatisticsResult.setPre(pre);
				
				if(role.contains("supervisor")){//技术领导统计已回执
					problemStatisticsResult.setDone(reply);
					problemStatisticsResult.setTotal(so+unso+reply+pre);// 总数量
				}else{
					problemStatisticsResult.setDone(done);
					problemStatisticsResult.setTotal(so+unso+done+pre);// 总数量					
				}
				
			}
			if(problemStatisticsResult.getTotal()==null){
				problemStatisticsResult.setSolved(0);// 已解决的问题数量
				problemStatisticsResult.setUnsolved(0);// 未解决的问题数量
				problemStatisticsResult.setPre(0);
				problemStatisticsResult.setDone(0);
				problemStatisticsResult.setTotal(0);// 总数量
			}
//		} else if (role.equals("captain")) {//队长登录看班长的统计
//			if(problemStream!=null && problemStream.size()>0){
//				problemStream.forEach(problem->{
//					if (problem.getId().intValue() == userId.intValue()) {
//						int so = problem.getSolved()==null?0:problem.getSolved();// 已解决的问题数量
//						int unso = problem.getUnsolved()==null?0:problem.getUnsolved();// 未解决的问题数量
//						int ass = problem.getAssign()==null?0:problem.getAssign();// 已指派的问题数量
//						problemStatisticsResult.setSolved(so);// 已解决的问题数量
//						problemStatisticsResult.setUnsolved(unso-ass);// 未解决的问题数量（包括了待指派的数量）
//						problemStatisticsResult.setNeetAssign(ass);//待指派
//						problemStatisticsResult.setTotal(so+unso);// 总数量
//					}
//				});
//			}
//			if(problemStatisticsResult.getTotal()==null){
//				problemStatisticsResult.setSolved(0);// 已解决的问题数量
//				problemStatisticsResult.setUnsolved(0);// 未解决的问题数量（包括了待指派的数量）
//				problemStatisticsResult.setNeetAssign(0);//待指派
//				problemStatisticsResult.setTotal(0);// 总数量
//			}
//		}else if (role.equals(ConstantVar.supervisor)) {//技术直接主管
////			技术状态：pre待处理、done已处理、unsolved仍未解决、solved已解决'
//			if(problemStream!=null && problemStream.size()>0){
//				problemStream.forEach(problem->{
//					if (problem.getId().intValue() == userId.intValue()) {
//						int so = problem.getSolved()==null?0:problem.getSolved();// 已解决的问题数量
//						int unso = problem.getUnsolved()==null?0:problem.getUnsolved();// 未解决的问题数量
//						int done = problem.getDone()==null?0:problem.getDone();//已处理
//						int pre = problem.getPre()==null?0:problem.getPre();//待处理
//						problemStatisticsResult.setSolved(so);// 已解决的问题数量
//						problemStatisticsResult.setUnsolved(unso);// 未解决的问题数量（包括了待指派的数量）
//						problemStatisticsResult.setPre(pre);
//						problemStatisticsResult.setDone(done);
//						problemStatisticsResult.setTotal(so+unso+done+pre);// 总数量
//					}
//				});
//			}
//			if(problemStatisticsResult.getTotal()==null){
//				problemStatisticsResult.setSolved(0);// 已解决的问题数量
//				problemStatisticsResult.setUnsolved(0);// 未解决的问题数量（包括了待指派的数量）
//				problemStatisticsResult.setPre(0);
//				problemStatisticsResult.setDone(0);
//				problemStatisticsResult.setTotal(0);// 总数量
//			}
//		}else if (role.equals("team")) {//TODO 返回的数据为假数据
//			problemStatisticsResult.setSolved(new Random().nextInt(10) % 13);
//			problemStatisticsResult.setUnsolved(new Random().nextInt(10) % 13);
//			problemStatisticsResult.setTotal(problemStatisticsResult.getUnsolved() + 
//					problemStatisticsResult.getUnsolved());
//			
//		} else {
//			return null;
//		}
		
		problemStatisticsResult.setMark(role);
		return problemStatisticsResult;
	}
	
	private WitnessStatisticsResult getWitnessStatisticsResult(Integer userId, Set<String> role, String type) {
		WitnessStatisticsResult witnessStatisticsResult = new WitnessStatisticsResult();
		
		Statistics statistics = new Statistics();
		statistics.setType(type);
		statistics.setUserId(userId);
		
		//TODO 返回的数据为假数据
		if (role.contains("monitor")) {
			witnessStatisticsResult.setUncomplete(witnessStatisticsMybatisDao.monitorUnCompletedCount(statistics));
			witnessStatisticsResult.setLaunched(0L);
			witnessStatisticsResult.setCompleted(witnessStatisticsMybatisDao.monitorCompletedCount(statistics));
			witnessStatisticsResult.setTotal(witnessStatisticsResult.getUncomplete() + 
					witnessStatisticsResult.getCompleted());
			witnessStatisticsResult.setMark("monitor");
		} else if (role.contains("team")) {
			witnessStatisticsResult.setUncomplete(witnessStatisticsMybatisDao.teamUnCompletedCount(statistics));
			witnessStatisticsResult.setLaunched(witnessStatisticsMybatisDao.teamLaunchedCount(statistics));
			witnessStatisticsResult.setCompleted(witnessStatisticsMybatisDao.teamCompletedCount(statistics));
			witnessStatisticsResult.setTotal(witnessStatisticsResult.getUncomplete() + 
					witnessStatisticsResult.getCompleted());
			witnessStatisticsResult.setMark("team");
		} else if (role.contains("witness_member_qc1")) {
			witnessStatisticsResult.setUncomplete(witnessStatisticsMybatisDao.qcMemberUnCompletedCount(statistics));
			witnessStatisticsResult.setLaunched(0L);
			witnessStatisticsResult.setCompleted(witnessStatisticsMybatisDao.qcMemberCompletedCount(statistics));
			witnessStatisticsResult.setTotal(witnessStatisticsResult.getUncomplete() + 
					witnessStatisticsResult.getCompleted());
			witnessStatisticsResult.setMark("witness_member_qc1");
		} else if (role.contains("witness_member_qc2")) {
				witnessStatisticsResult.setUncomplete(witnessStatisticsMybatisDao.qcMemberUnCompletedCount(statistics));
				witnessStatisticsResult.setLaunched(0L);
				witnessStatisticsResult.setCompleted(witnessStatisticsMybatisDao.qcMemberCompletedCount(statistics));
				witnessStatisticsResult.setTotal(witnessStatisticsResult.getUncomplete() + 
						witnessStatisticsResult.getCompleted());
				witnessStatisticsResult.setMark("witness_member_qc2");
		} else {
			return null;
		}
		return witnessStatisticsResult;
	}
	
	private WitnessQC2StatisticsResult getWitnessQC2StatisticsResult(Integer userId, Set<String> role, String type, QC2MemberTypeEnum memberType) {
		WitnessQC2StatisticsResult witnessQC2StatisticsResult = new WitnessQC2StatisticsResult();
		
		Statistics statistics = new Statistics();
		statistics.setType(type);
		statistics.setUserId(userId);
		statistics.setQc2Type(memberType.name());
		
		//TODO 返回的数据为假数据
		if (role.contains("witness_member_czecqc")) {
			witnessQC2StatisticsResult.setUncomplete(witnessStatisticsMybatisDao.qc2MemberUnCompletedCount(statistics));
			witnessQC2StatisticsResult.setCompleted(witnessStatisticsMybatisDao.qc2MemberCompletedCount(statistics));
			witnessQC2StatisticsResult.setTotal(witnessQC2StatisticsResult.getUncomplete() + 
					witnessQC2StatisticsResult.getCompleted());
			witnessQC2StatisticsResult.setMark("witness_member_czecqc");
		} else if (role.contains("witness_member_czecqa")) {
			witnessQC2StatisticsResult.setUncomplete(witnessStatisticsMybatisDao.qc2MemberUnCompletedCount(statistics));
			witnessQC2StatisticsResult.setCompleted(witnessStatisticsMybatisDao.qc2MemberCompletedCount(statistics));
			witnessQC2StatisticsResult.setTotal(witnessQC2StatisticsResult.getUncomplete() + 
					witnessQC2StatisticsResult.getCompleted());
			witnessQC2StatisticsResult.setMark("witness_member_czecqa");
		} else if (role.contains("witness_member_paec")) {
			witnessQC2StatisticsResult.setUncomplete(witnessStatisticsMybatisDao.qc2MemberUnCompletedCount(statistics));
			witnessQC2StatisticsResult.setCompleted(witnessStatisticsMybatisDao.qc2MemberCompletedCount(statistics));
			witnessQC2StatisticsResult.setTotal(witnessQC2StatisticsResult.getUncomplete() + 
					witnessQC2StatisticsResult.getCompleted());
			witnessQC2StatisticsResult.setMark("witness_member_paec");
		} else {
			return null;
		}
//		witnessQC2StatisticsResult.setMark(role);
		return witnessQC2StatisticsResult;
	}
	
	@Override
	public Set<String> convertAuthRoleToList(Set<AuthRole> authRoles) {
		if (null != authRoles) {
			Set<String> roles = new HashSet<String>();
			for (AuthRole authRole : authRoles) {
				roles.addAll(authRole.getRoleType());
			}
			return roles;
		}
		return null;
	}

//	public List<StatisticsBean> problemStatisticsForCaptain(Set<String> LonginUserroleNameList,Integer userId, String role,String moduleType){
//		//当前登录人是队长，查看班长下的问题统计
//		if (LonginUserroleNameList.contains("captain")) {
//			Map<String,Object> inPara = new HashMap<String,Object>();
//			inPara.put("type", moduleType);
//			inPara.put("captainId", userId);
//			List<StatisticsBean> questionList = rollingQuestionMybatisDao.problemStatisticsForCaptain(inPara);
//			return questionList ;
//		}
//		return null;
//	}
	/**
	 * 
					//当前登录人是队长，查看班长下的问题统计 :已解决的问题数量、未解决的问题数量
					problemList = this.problemStatisticsForCaptain(roleNameList,loginUser.getId(), null,moduleType);
					//如果当前登录人是班长：则统计已解决的问题数、未解决的问题数
	 * @param LonginUserroleNameList
	 * @param userId
	 * @param role
	 * @param moduleType
	 * @return
	 */
	public List<StatisticsBean> problemStatistics(Set<String> LonginUserroleNameList,Integer userId, String role,String moduleType){
		//当前登录人是队长，查看班长下的问题统计
		if (LonginUserroleNameList.contains("captain")) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "captain");
			inPara.put("type", moduleType);
			inPara.put("captainId", userId);
			List<StatisticsBean> questionList = rollingQuestionMybatisDao.problemStatisticsForCaptain(inPara);
			return questionList ;
		}else if (LonginUserroleNameList.contains("monitor")) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "monitor");
			inPara.put("type", moduleType);
			inPara.put("monitorId", userId);
			List<StatisticsBean> questionList = rollingQuestionMybatisDao.problemStatisticsForCaptain(inPara);
			return questionList ;
		}else if (LonginUserroleNameList.contains(ConstantVar.supervisor)) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", ConstantVar.supervisor);
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			List<StatisticsBean> questionList = rollingQuestionMybatisDao.problemStatisticsForCaptain(inPara);
			return questionList ;
		}else if (LonginUserroleNameList.contains(ConstantVar.COORDINATOR)) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", ConstantVar.COORDINATOR);
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			List<StatisticsBean> questionList = rollingQuestionMybatisDao.problemStatisticsForCaptain(inPara);
			return questionList ;
		}
		return null;
	}

	public StatisticsBean problemStatistics(String LonginUserroleNameList,Integer userId,String moduleType){
		if(LonginUserroleNameList==null){
			return null;
		}
		//当前登录人是队长，查看班长下的问题统计
		if (LonginUserroleNameList.contains("captain")) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "monitor");
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			StatisticsBean question = rollingQuestionMybatisDao.problemStatisticsForTeam(inPara);
			return question ;
		}else if (LonginUserroleNameList.contains("monitor")) {
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "team");
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			StatisticsBean question = rollingQuestionMybatisDao.problemStatisticsForTeam(inPara);
			return question ;
		}else if (LonginUserroleNameList.contains(ConstantVar.supervisor)) {//solver4
			//当前是技术领导，主任工程师，查询第个技术人员的统计。
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "solver");
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			inPara.put("solverRole", 3);
			StatisticsBean question = rollingQuestionMybatisDao.problemStatisticsForTeam(inPara);
			return question ;
		}else if (LonginUserroleNameList.contains(ConstantVar.solver3) || LonginUserroleNameList.contains(ConstantVar.solver4)
				|| LonginUserroleNameList.contains(ConstantVar.solver5)
				|| LonginUserroleNameList.contains(ConstantVar.solver6)
				|| LonginUserroleNameList.contains(ConstantVar.solver7)
				|| LonginUserroleNameList.contains(ConstantVar.solver8)) {
			int solverRole = Integer.valueOf(LonginUserroleNameList.substring(ConstantVar.solver.length()))+1;
			//当前是技术领导，主任工程师，查询第个技术人员的统计。
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "commonsolver");
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			inPara.put("solverRole", solverRole);
			StatisticsBean question = rollingQuestionMybatisDao.problemStatisticsForTeam(inPara);
			return question ;
		}else{
			Map<String,Object> inPara = new HashMap<String,Object>();
			inPara.put("who", "team");
			inPara.put("type", moduleType);
			inPara.put("userId", userId);
			StatisticsBean question = rollingQuestionMybatisDao.problemStatisticsForTeam(inPara);
			return question ;
		}
//		return null;
	}
/**
 * 直接使用mybatis查询出统计结果，再进行封装
 * */
	private StatisticsMainResult<?> getStatisticTwo(User loginUser, String type, String moduleType,Integer usingRoleId) {
		List<Role> roles = loginUser.getRoles();
		logger.info(loginUser.getId()+" "+roles.size());
		if(usingRoleId!=null){//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景
			roles = roleService.findListById(roles,usingRoleId);
		}
		
		if (null != roles) {
			StatisticsMainResult<RollingPlanStatisticsResult> rollingPlanMainResult = new StatisticsMainResult<RollingPlanStatisticsResult>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> captainResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> monitorResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> witnessTeamResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> witnessQC2Result = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			List<StatisticsReportResult<RollingPlanStatisticsResult>> supervisorResult = new ArrayList<StatisticsReportResult<RollingPlanStatisticsResult>>();
			
			for (Role role : roles) {
				logger.info(role.getName());
				Set<String> roleNameList = variableSetService.findKeyByValue(role.getName(), "roleType");
				final List<StatisticsBean> problemStream = (type.equals("Problem"))?this.problemStatistics(roleNameList,loginUser.getId(), null,moduleType):null;
				for(StatisticsBean bean:problemStream){
					if(bean==null){
						continue ;
					}
					
					User user = userService.findUserById(bean.getId());

					Set<AuthRole> authRoles = roleApiService.getAuthRole(user,false);//rollingPlan等需要验证角色时，设置false
					Set<String> authRolesSet = this.convertAuthRoleToList(authRoles);
					if (null != authRolesSet) {
						for (String authRole : authRolesSet) {
							StatisticsUserResult userResult = new StatisticsUserResult();
							userResult.setId(user.getId());
							userResult.setName(user.getName());
							userResult.setRealname(user.getRealname());
							userResult.setRoles(authRoles);
							if (null != user.getDepartment()) {
								AuthDept authDept = new AuthDept();
								authDept.setId(user.getDepartment().getId());
								authDept.setName(user.getDepartment().getName());
								userResult.setDept(authDept);
							}

							StatisticsReportResult rollingPlanReportResult = new StatisticsReportResult();
							rollingPlanReportResult.setUser(userResult);
							if (type.equals("Problem")) {
								ProblemStatisticsResult rollingPlanStatisticsResult = getProblemStatisticsResultForCoordinate(bean);
								rollingPlanReportResult.setStatistics(rollingPlanStatisticsResult);
							}
							if (roleNameList.contains(ConstantVar.COORDINATOR)) {//作业问题 协调人员 看班长的统计
								monitorResult.add(rollingPlanReportResult);
							}
						}
					}
				}
			}
			rollingPlanMainResult.setCaptain(captainResult);
			rollingPlanMainResult.setMonitor(monitorResult);
			rollingPlanMainResult.setWitness_team(witnessTeamResult);
			rollingPlanMainResult.setWitness_qc2(witnessQC2Result);
			rollingPlanMainResult.setSupervisor(supervisorResult);
			return rollingPlanMainResult;
		}
		return null;
	}
}
