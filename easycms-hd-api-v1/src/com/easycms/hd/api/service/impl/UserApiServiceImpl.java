package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.enpower.domain.EnpowerUserEntity;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

@Service("userApiService")
public class UserApiServiceImpl implements UserApiService {
	@Autowired
	private RoleApiService roleApiService;
	@Autowired
	private UserService userService;
	
	@Override
	public UserResult userTransferToUserResult(User user,boolean showOriginRole) {//boolean showOriginRole
		if (user != null) {
			UserResult userResult = new UserResult();

			userResult.setId(user.getId());
			userResult.setRealname(user.getRealname());
			userResult.setUsername(user.getName());
			userResult.setDel(user.isDel());
			
			if (user.getRoles() != null && user.getRoles().size() > 0) {
				Set<AuthRole> roles = roleApiService.getAuthRole(user,showOriginRole);
				userResult.setRoles(roles);
			}
	
			return userResult;
		}
		return null;
	}

	@Override
	public List<UserResult> userTransferToUserResult(List<User> users,boolean showOriginRole) {//boolean showOriginRole
		if (null != users && !users.isEmpty() ) {
			List<UserResult> userResults = new ArrayList<UserResult>();
			for (User user : users) {
				userResults.add(userTransferToUserResult(user,showOriginRole));
			}
			return userResults;
		}
		return null;
	}

	@Override
	public boolean insertBatch(List<EnpowerUserEntity> enpowerUsers) {
		for(EnpowerUserEntity enpowerUser : enpowerUsers) {
			insertEnpowerUser(enpowerUser, null);
		}
		return true;
	}

	private void insertEnpowerUser(EnpowerUserEntity enpowerUser, String parentId){
		User user = new User();
		user.setEmail(enpowerUser.getEmail());
		user.setName(enpowerUser.getLoginId());
		user.setRealname(enpowerUser.getUserName());
		
		user.setParent(userService.findUser(parentId));
		//递归插入用户信息
//		userService.add(user);
		
		Role role = new Role();
		role.setName(enpowerUser.getRoleName());
		//插入用户角色
//		roleService.add(role);
		
		if (null != enpowerUser.getUnderling() && enpowerUser.getUnderling().size() > 0){
			for (EnpowerUserEntity eu : enpowerUser.getUnderling()){
				insertEnpowerUser(eu, user.getName());
			}
		}
	}
}
