package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.constant.EnpConstant;
import com.easycms.hd.api.enpower.dao.EnpowerLogDao;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enpower.response.EnpowerResponseGetChange;
import com.easycms.hd.api.response.GetChangeResResult;
import com.easycms.hd.api.service.EnpowerApiService;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.hseproblem.service.HseProblemSolveService;
import com.easycms.hd.plan.dao.RollingPlanDao;
import com.easycms.hd.plan.service.EnpowerMaterialInfoService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.variable.service.VariableSetService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 执行推送接口数据至Enpower系统
 * 变更查询
 * @author ul-webdev
 *
 */
@Service("executeSearchPushToEnpowerServiceImpl")
public class ExecuteSearchPushToEnpowerServiceImpl {
	private Logger logger = Logger.getLogger(ExecuteSearchPushToEnpowerServiceImpl.class);

	@Autowired
	private ComputedConstantVar constantVar ;
	@Value("#{APP_SETTING['enpower_api_updateData']}")
	private String enpowerupdateData;
	Gson gson = new Gson();
	
	@Autowired
	private WorkStepService workStepService ;
	@Autowired
	private RollingPlanApiService rollingPlanApiService ;
	@Autowired
	private RollingPlanService rollingPlanService ;
	@Autowired
	private RollingPlanDao rollingPlanDao ;
	@Autowired
	private EnpowerMaterialInfoService enpowerMaterialInfoService ;
	@Autowired
	private EnpowerApiService enpowerApiService ;
	@Autowired
	private VariableSetService variableSetService ;
	@Autowired
	private HseProblemSolveService hseProblemSolveService ;

	@Autowired
	private EnpowerLogDao enpowerLogDao;

	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	
	
	

    /**
     * 变更查询　接口
     * @return
     * @throws Exception 
     */
    public List<GetChangeResResult> getChangeResult(String fileCode,String cnTitle,String remark,Integer pagenum,Integer pagesize) throws Exception{
    	
    	/**
    	 * 一个搜索“中文标题”，一个搜索“备注”，原搜索框搜索“文件编码”
    	 */
    	List<GetChangeResResult> list = new ArrayList<GetChangeResResult>();
    	Map<String,Object> map = new HashMap<String,Object>();
    	StringBuffer condition = new StringBuffer();
    	if(StringUtils.isNoneBlank(fileCode) || StringUtils.isNoneBlank(cnTitle) || StringUtils.isNoneBlank(remark) ){
    		
    		condition.append(" and (");

    		if(StringUtils.isNoneBlank(fileCode)){
        		condition.append("文件编码 like '%");
        		condition.append(fileCode.toUpperCase());
        		condition.append("%'");
    		}
    		if(StringUtils.isNoneBlank(cnTitle)){
    			if(StringUtils.isNoneBlank(fileCode)){
    				condition.append(" or ");
    			}

        		condition.append("中文标题 like '%");
        		condition.append(cnTitle.toUpperCase());
        		condition.append("%'");
    		}
    		if(StringUtils.isNoneBlank(remark)){
    			if(StringUtils.isNoneBlank(fileCode)||StringUtils.isNoneBlank(cnTitle)){
    				condition.append(" or ");
    			}

        		condition.append("备注 like '%");
        		condition.append(remark.toUpperCase());
        		condition.append("%'");
    		}
    		
    		condition.append(" ) ");
    	}else{
//    		return list;
    	}
		//pageIndex 页码
		//pageSize 条数
        
		try {
			String xmlName = "文档变更查询接口";
			String conditionKey = "项目代号";
			map.put(conditionKey, EnpConstant.PROJ_CODE);//条数
			String requestResult = executePushToEnpowerServiceImpl.getEnpowerDBinfo( xmlName,map,condition,pagenum,pagesize);

			if (null != requestResult && !"".equals(requestResult)) {
				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {
				}.getType());
				if (baseResponse.getIsSuccess()) {
					String dataInfo = baseResponse.getDataInfo();
					logger.info(dataInfo);
					List<EnpowerResponseGetChange> enpList = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseGetChange>>() {
					}.getType());
					
					if(enpList.isEmpty()==false){

						enpList.stream().forEach(ret->{
    						GetChangeResResult result = null;
							result = new GetChangeResResult();
							/**文档类型*/
							result.setDocType(ret.getDocType());
							/**文件类型*/
							result.setFileType(ret.getFileType());
							/**版本*/
							result.setVersion(ret.getVersion());
							/**内部文件编号*/
							result.setInnerFileCode(ret.getInnerFileCode());
							/**文件编码*/
							result.setFileCode(ret.getFileCode());
							/**中文标题*/
							result.setCnTitle(ret.getCnTitle());
							/**项目代号*/
							result.setProjCode(ret.getProjCode());
							/**公司代号*/
							result.setCompCode(ret.getCompCode());
							/**"状态":"INV"*/
							result.setStatus(ret.getStatus());
							
							/**"备注":"remark"*/
							result.setRemark(ret.getRemark());

    						list.add(result);
    						
    					});
					}

				} else {
					logger.error(baseResponse.getDataInfo());
				}
			}

		}catch(Exception e){
            e.printStackTrace();
            throw e;
        }

        return list;
    }
    
}
