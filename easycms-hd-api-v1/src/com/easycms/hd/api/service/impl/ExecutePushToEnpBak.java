package com.easycms.hd.api.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.easycms.common.util.CommonUtility;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreSearch;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialOutSearch;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialStoreSearch;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.MaterialResult;
import com.google.gson.reflect.TypeToken;

public class ExecutePushToEnpBak {
//
//    /**
//     * 物项入库(已核实)查询接口  推送接口名: 物项入库(已核实)查询接口 
//     * @return
//     * @throws Exception 
//     */
//    public List<MaterialResult> materialStoreSearchExamined(String warrantyNo) throws Exception{
//    	List<MaterialResult> retList = new ArrayList<MaterialResult>();
//    	MaterialResult ret = null;
//        try{
//            List<EnpowerRequestMaterialStoreSearch> disaList = null;
//            	String xmlName="物项入库(已核实)查询接口";//"物项入库查询接口";
//            	String conditionKey="质保编号";
//
//    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,warrantyNo);
//
//    			if (null != requestResult && !"".equals(requestResult)){
//    				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {}.getType());
//    				//TODO 待Enpower数据过来解析
//    				EnpowerResponseMaterialStoreSearch enp = new EnpowerResponseMaterialStoreSearch();
//				if (baseResponse.getIsSuccess()) {
//					String dataInfo = baseResponse.getDataInfo();
//					logger.info(dataInfo);
//					logger.debug("有了数据，就需要继续往下分析了。。。。");
//
//					ret = new MaterialResult();
//					// 物项编码
//					// ret.setEnpowerhavebutWeNone(enp.getMate_code());
//					// 物项名称
//					ret.setName(enp.getMate_descr());
//					// 质保编号
//					ret.setWarrantyNo(enp.getGuaranteeno());
//					// 数量
//					if (CommonUtility.isNonEmpty(enp.getStk_qty())) {
//						ret.setNumber(Integer.valueOf(enp.getStk_qty().trim()));
//					} else {
//						ret.setNumber(0);
//					}
//					// 材质
//					ret.setMaterial(enp.getTexture());
//					// 规格型号
//					ret.setSpecificationNo(enp.getSpec());
//					// 炉批号
//					ret.setFurnaceNo(enp.getHeat_No());
//					// 批号
//					// ret.setEnpowerhavebutWeNone (enp.getLotno());
//					// 核安全等级
//					ret.setSecurityLevel(enp.getNs_class());
//					// 质保等级
//					ret.setWarrantyLevel(enp.getQa_class());
//					// 船次件号
//					ret.setShipNo(enp.getDeliv_Lot());
//					// 项目代号
//					// ret.setEnpowerhavebutWeNone (enp.getProj_code());
//					// 公司代号
//					// ret.setEnpowerhavebutWeNone (enp.getComp_code());
//
//					retList.add(ret);
//					
//					return retList;
//				} else {
//    					logger.error(baseResponse.getDataInfo());
//    				}
//    			}
//                logger.info("物项入库(已核实)查询接口 ="+CommonUtility.toJson(disaList));
//
//        }catch(Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//        return retList;
//    }
//    
//
//    /**
//     * 物项出库(已核实)查询接口
//     * @return
//     * @throws Exception 
//     */
//    public MaterialResult materialOutSearchExamined  (String outNo) throws Exception{
//    	EnpowerResponseMaterialOutSearch ret = null;
//    	MaterialResult result = null;
//        try{
//            List<EnpowerRequestMaterialOutSearch  > disaList = null;
//            	
//
//            	String xmlName="物项出库(已核实)查询接口";
//            	String conditionKey="ISSNO";//ISSNO：出库单号
//
//    			String requestResult = getEnpowerDBinfo(xmlName, conditionKey,outNo);
//
//    			if (null != requestResult && !"".equals(requestResult)){
//    				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {}.getType());
//    				if (baseResponse.getIsSuccess()){
//    					String dataInfo = baseResponse.getDataInfo();
//    					logger.info(dataInfo);
//    					logger.debug("有了数据，就需要继续往下分析了。。。。");
//    					result = new MaterialResult();
//    					
//    					ret = new EnpowerResponseMaterialOutSearch();
//						// 出库ID
////						result.setEnpHavAppNone(ret.getID());
//						// 入库ID
////						result.setEnpHavAppNone(ret.getINSTK_ID());
//						// 库存ID
////						result.setEnpHavAppNone(ret.getSTK_ID());
//						// 物项编码
////						result.setEnpHavAppNone(ret.getMATE_CODE());
//						// 物项名称
//						result.setName(ret.getMATE_DESCR());
//						// 出库单号
//						result.setIssNo(ret.getISSNO());
//						// 出库类型
////						result.setEnpHavAppNone(ret.getISSTYPE());
//						// 开票日期
//						result.setIssDate(ret.getISSDATE());
//						// 出库单位
//						result.setIssDept(ret.getIss_Dept());
//						// 领料员
//						result.setWhoGet(ret.getWHO_GET());
//						// 出库数量
//						if(CommonUtility.isNonEmpty(ret.getISSQTY())){
//							
//							result.setNumber(Integer.valueOf(ret.getISSQTY()));
//						}else{
//							result.setNumber(0);
//						}
//						// 核实数量
////						result.setEnpHavAppNone(ret.getQTY_RELEASED());
//						// 单价
//						if(ret.getPRICE()!=null){
//							
//							result.setPrice(new BigDecimal(ret.getPRICE()));
//						}else{
//							result.setPrice(new BigDecimal("0"));
//						}
//						// 出库金额
//						result.setCkJe(ret.getCK_JE());
//						// 核实金额
//						result.setCkJeHs(ret.getCK_JE_HS());
//						// 保管员
//						result.setKeeper(ret.getKEEPER());
//						// 核实日期
//						result.setWhenCnfmed(ret.getWHEN_CNFMED());
//						// 需求计划号
//						result.setMrpNo(ret.getMrp_no());
//						// 质保号
//						result.setWarrantyNo(ret.getGUARANTEENO());
//						// 会计科目
//						result.setCstCode(ret.getCst_Code());
//						// 规格型号
//						result.setSpecificationNo(ret.getSpec());
//						// 材质
//						result.setMaterial(ret.getTexture());
//						// 标准
//						result.setStandard(ret.getStand_No());
//						//核安全等级
//						result.setSecurityLevel(ret.getNs_class());
//						// 质保等级
//						result.setWarrantyLevel(ret.getQa_class());
//						// 位号
//						result.setPositionNo(ret.getFUNC_NO());
//						// 炉批号
//						result.setFurnaceNo(ret.getLOTNO());
//						// 批号
//						// result.setEnpowerhavebutWeNone(ret.getHEAT_NO());
//						// 备注
//						result.setRemark(ret.getISSUSEREMARK());
//
//    					return result;
//    				} else {
//    					logger.error(baseResponse.getDataInfo());
//    				}
//    			}
//    			
//                disaList = enpowerApiService.findEnpowerMaterialOutSearchById(outNo);
//                logger.info("物项出库(已核实)查询接口="+CommonUtility.toJson(disaList));
//
//        }catch(Exception e){
//            e.printStackTrace();
//            throw e;
//        }
//
//        return result;
//    }
//    
//
//    /**
//     * 物项退库(已核实)查询接口
//     * @return
//     * @throws Exception 
//     */
//    public JsonResult<Boolean> materialCancelSearchExamined(String cancelNo) throws Exception{
//        
//        JsonResult<Boolean> result = new JsonResult<Boolean>();
//        try{
//            List<EnpowerRequestMaterialCancelSearch   > disaList = null;
//            	String xmlName="物项退库(已核实)查询接口";
//            	String conditionKey="ISSNO";//ISSNO：退库单号
//
//    			String requestResult = getEnpowerDBinfo( xmlName, conditionKey,cancelNo);
//    			
//    			
//    			if (null != requestResult && !"".equals(requestResult)){
//    				BaseResponse baseResponse = gson.fromJson(requestResult, new TypeToken<BaseResponse>() {}.getType());
//    				if (baseResponse.getIsSuccess()){
//    					String dataInfo = baseResponse.getDataInfo();
//    					logger.info(dataInfo);
//    					logger.debug("有了数据，就需要继续往下分析了。。。。");
//    				} else {
//    					logger.error(baseResponse.getDataInfo());
//    				}
//    			}
//    			
//    			
//                disaList = enpowerApiService.findEnpowerMaterialCancelSearchById(cancelNo);
//                logger.info("物项退库(已核实)查询接口 ="+CommonUtility.toJson(disaList));
//
//        }catch(Exception e){
//            e.printStackTrace();
//            result.setCode("-2010");
//            result.setMessage(e.getMessage());
//            throw e;
//        }
//
//        return result;
//    }
}
