package com.easycms.hd.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.core.util.Page;
import com.easycms.hd.api.domain.Statistics;
import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.enums.ConferenceApiRequestType;
import com.easycms.hd.api.enums.ConferenceStatusEnum;
import com.easycms.hd.api.request.ConferenceRequestForm;
import com.easycms.hd.api.response.ConferencePageResult;
import com.easycms.hd.api.response.ConferenceResult;
import com.easycms.hd.api.response.FileResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.ConferenceApiService;
import com.easycms.hd.api.service.ConferenceFeedbackApiService;
import com.easycms.hd.api.service.UserApiService;
import com.easycms.hd.conference.domain.Conference;
import com.easycms.hd.conference.domain.ConferenceFeedback;
import com.easycms.hd.conference.domain.ConferenceFile;
import com.easycms.hd.conference.domain.Notification;
import com.easycms.hd.conference.enums.ConferenceSource;
import com.easycms.hd.conference.enums.ConferenceType;
import com.easycms.hd.conference.service.ConferenceFeedbackMarkService;
import com.easycms.hd.conference.service.ConferenceFeedbackService;
import com.easycms.hd.conference.service.ConferenceFileService;
import com.easycms.hd.conference.service.ConferenceService;
import com.easycms.hd.plan.mybatis.dao.ConferenceStatisticsMybatisDao;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.service.SchedulerService;

@Service("conferenceApiService")
public class ConferenceApiServiceImpl implements ConferenceApiService {
	@Autowired
	private ConferenceService conferenceService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserApiService userApiService;
	@Autowired
	private ConferenceFileService conferenceFileService;
	@Autowired
	private ConferenceFeedbackService conferenceFeedbackService;
	@Autowired
	private ConferenceFeedbackMarkService conferenceFeedbackMarkService;
	@Autowired
	private ConferenceFeedbackApiService conferenceFeedbackApiService;
	@Autowired
	private ConferenceStatisticsMybatisDao conferenceStatisticsMybatisDao;	
	@Autowired
	private SchedulerService schedulerService;
	
	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_conference_path']}")
	private String fileConferencePath;

	@Override
	public ConferenceResult conferenceTransferToConferenceResult(Conference conference, Integer userId, ConferenceApiRequestType type, boolean feedback) {
		if (null != conference) {
			ConferenceResult conferenceResult = new ConferenceResult();
			conferenceResult.setId(conference.getId());
			conferenceResult.setSubject(conference.getSubject());
			conferenceResult.setContent(conference.getContent());
			conferenceResult.setCategory(conference.getCategory());
			conferenceResult.setProject(conference.getProject());
			conferenceResult.setHost(conference.getHost());
			conferenceResult.setAlarmTime(conference.getAlarmTime());
			conferenceResult.setRecorder(conference.getRecorder());
			conferenceResult.setSupplies(conference.getSupplies());
			conferenceResult.setRemark(conference.getRemark());
			conferenceResult.setStartTime(conference.getStartTime());
			conferenceResult.setEndTime(conference.getEndTime());
			conferenceResult.setAddress(conference.getAddress());
			conferenceResult.setCreateTime(conference.getCreateOn());
			conferenceResult.setSource(conference.getSource());
			
			if (null != userId) {
				Statistics statistics = new Statistics();
				statistics.setUserId(userId);
				statistics.setId(conference.getId());
				if (type.equals(ConferenceApiRequestType.SEND)) {
					conferenceResult.setUnread(conferenceStatisticsMybatisDao.conferenceEachSendUnReadFeedback(statistics));
				} else if (type.equals(ConferenceApiRequestType.RECEIVE)) {
					conferenceResult.setUnread(conferenceStatisticsMybatisDao.conferenceEachReceiveUnReadFeedback(statistics));
				}
			}
			
			if (conference.getType().equals(ConferenceType.DRAFT.name())) {
				conferenceResult.setStatus(ConferenceStatusEnum.DRAFT.name());
			} else {
				if (null != conference.getStartTime() && null != conference.getEndTime()) {
					Long currentTime = System.currentTimeMillis();
					Long startTime = conference.getStartTime().getTime();
					Long endTime = conference.getEndTime().getTime();
					
					if (currentTime < startTime) {
						conferenceResult.setStatus(ConferenceStatusEnum.UNSTARTED.name());
					} else if (currentTime >= startTime && currentTime <= endTime) {
						conferenceResult.setStatus(ConferenceStatusEnum.PROGRESSING.name());
					} else if (currentTime > endTime) {
						conferenceResult.setStatus(ConferenceStatusEnum.EXPIRED.name());
					}
				} else {
					conferenceResult.setStatus(ConferenceStatusEnum.UNKNOWN.name());
				}
			}
			
			if (null != conference.getParticipants() && !conference.getParticipants().isEmpty()) {
				conferenceResult.setParticipants(new HashSet<>(userApiService.userTransferToUserResult(conference.getParticipants(),true)));//显示原始的角色，没有roleType显示
			}
			
			List<ConferenceFile> conferenceFiles = conferenceFileService.findFilesByConferenceId(conference.getId());
			if (null != conferenceFiles) {
				conference.setConferenceFiles(conferenceFiles);
			}
			if (null != conference.getConferenceFiles() && !conference.getConferenceFiles().isEmpty()) {
				List<FileResult> fileResults = conference.getConferenceFiles().stream().map(file -> {
					FileResult fileResult = new FileResult();
					fileResult.setId(file.getId());
					fileResult.setUrl("/hdxt/api/files/conference/" + file.getPath());
					fileResult.setFileName(file.getFileName());
					return fileResult;
				}).collect(Collectors.toList());
				conferenceResult.setFiles(fileResults);
			}
			
			if (feedback) {
				List<ConferenceFeedback> conferenceFeedbacks = conferenceFeedbackService.findByConferenceId(conference.getId());
				conferenceResult.setFeedback(conferenceFeedbackApiService.conferenceFeedbackTransferToConferenceFeedbackResult(conferenceFeedbacks));
			}
			
			return conferenceResult;
		}
		return null;
	}
	
	@Override
	public List<ConferenceResult> conferenceTransferToConferenceResult(List<Conference> conferences, Integer userId, ConferenceApiRequestType type) {
		if (null != conferences && !conferences.isEmpty()) {
			List<ConferenceResult> conferenceResults = conferences.stream().map(conference -> {
				return conferenceTransferToConferenceResult(conference, userId, type, false);
			}).collect(Collectors.toList());
			
			return conferenceResults;
		}
		return null;
	}

	@Override
	public JsonResult<Integer> insert(ConferenceRequestForm form, User user, ConferenceSource source) {
		if (null != form) {
			JsonResult<Integer> result = new JsonResult<Integer>();
			
			if (form.getType().equals(ConferenceType.SEND)) {
				List<String> message = new ArrayList<String>();
				
				if (!CommonUtility.isNonEmpty(form.getContent())) {
					message.add("* 正文不能为空.");
				}
				if (!CommonUtility.isNonEmpty(form.getSubject())) {
					message.add("* 主题不能为空.");
				}
				if (null == form.getStartTime()) {
					message.add("* 开始时间不能为空.");
				}
				if (null == form.getEndTime()) {
					message.add("* 结束时间不能为空.");
				}
				if (null == form.getAlarmTime()) {
					message.add("* 提醒类型不能为空.");
				}
				if (null != form.getStartTime() && null != form.getEndTime()) {
					if (form.getStartTime().getTime() > form.getEndTime().getTime()) {
						message.add("* 开始时间不能大于结束时间.");
					}
				}
				
				if (!message.isEmpty()) {
					result.setCode("-1000");
					result.setMessage(message.stream().reduce((a, b) -> a + b).get());
					result.setResponseResult(null);
					return result;
				}
			}
			
			Conference conference = new Conference();
			if (null != form.getId()) {
				conference = conferenceService.findById(form.getId());
				if (null == conference) {
					conference = new Conference();
				}
			} 
			conference.setSubject(form.getSubject());
			conference.setContent(form.getContent());
			conference.setCategory(form.getCategory());
			conference.setProject(form.getProject());
			conference.setHost(form.getHost());
			conference.setAlarmTime(form.getAlarmTime().name());
			conference.setRecorder(form.getRecorder());
			conference.setSupplies(form.getSupplies());
			conference.setRemark(form.getRemark());
			conference.setStartTime(form.getStartTime());
			conference.setEndTime(form.getEndTime());
			conference.setStatus("ACTIVE");
			conference.setAddress(form.getAddress());
			conference.setSource(source.name());
			
			if (null != form.getParticipants() && form.getParticipants().length > 0) {
				Set<User> users = new HashSet<User>();
				users.add(user);
				for (Integer id : form.getParticipants()) {
					User u = userService.findUserById(id);
					if (null != u) {
						users.add(u);
					}
				}
				conference.setParticipants(new ArrayList<>(users));
			}
			
			conference.setCreateBy(user.getId());
			conference.setCreateOn(new Date());
			if (null == form.getType()) {
				conference.setType(ConferenceType.SEND.name());
			} else {
				conference.setType(form.getType().name());
			}

			if (null != conferenceService.add(conference)) {
				//所有的操作都完成之后，发起push
	            if (null == form.getType() || form.getType().equals(ConferenceType.SEND)) {
	            	if (null != form.getAlarmTime() && null != form.getStartTime() && !form.getAlarmTime().equals(ConferenceAlarmTypsEnums.NONE)) {
	            		Map<String, Object> dataMap = new HashMap<String, Object>();
	            		dataMap.put("userId", user.getId());
	            		dataMap.put("conferenceId", conference.getId());
	            		
	            		Date startTime = DateUtility.dateMinus(conference.getStartTime(), form.getAlarmTime().getMillis());
	            		
		            	Map<String, Object> jobDataMap = new HashMap<String, Object>();
		            	jobDataMap.put("jobService", "conferenceAlarmService");
		            	jobDataMap.put("jobDataMap", dataMap);
		            	String name = schedulerService.schedule(startTime, jobDataMap);
		            	
		            	conference.setTriggerName(name);
		            	conferenceService.add(conference);
	            	}
	            	
	            	
	            } else if (form.getType().equals(ConferenceType.DRAFT)) {
	            	if (CommonUtility.isNonEmpty(conference.getTriggerName())) {
	            		schedulerService.removeTrigger(conference.getTriggerName());
	            		conference.setTriggerName(null);
		            	conferenceService.add(conference);
	            	}
	            }
	            result.setResponseResult(conference.getId());
	            return result;
			}
		}
		return null;
	}

	@Override
	public ConferencePageResult getConferencePageResult(ConferenceApiRequestType type, Integer userId, Page<Conference> page) {
		if (null != type && null != userId) {
			ConferencePageResult conferencePageResult = new ConferencePageResult();
			
			if (type.equals(ConferenceApiRequestType.DRAFT)) {
				page = conferenceService.findByTypeAndCreater(type.name(), userId, page);
			} else if (type.equals(ConferenceApiRequestType.SEND)) {
				page = conferenceService.findConferenceSend(userId, page);
			} else if (type.equals(ConferenceApiRequestType.RECEIVE)) {
				//通过mybatis的方式去获取分页列表
				page = conferenceService.findConferenceReceive(userId, page);
			}
			
			if (null != page.getDatas()) {
				List<ConferenceResult> conferenceResults = page.getDatas().stream().map(conference -> {
					return this.conferenceTransferToConferenceResult(conference, userId, type, false);
				}).collect(Collectors.toList());
				
				conferencePageResult.setPageCounts(page.getPageCount());
				conferencePageResult.setPageNum(page.getPageNum());
				conferencePageResult.setPageSize(page.getPagesize());
				conferencePageResult.setTotalCounts(page.getTotalCounts());
				conferencePageResult.setData(conferenceResults);
			}
			
			return conferencePageResult;
		}
		return null;
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (null != ids && ids.length > 0) {
			String filePath = basePath + fileConferencePath;
			List<ConferenceFile>  conferenceFiles = conferenceFileService.findFilesByConferenceIds(ids);
			for (ConferenceFile conferenceFile : conferenceFiles) {
				FileUtils.remove(filePath + conferenceFile.getPath());
			}
			conferenceService.batchDelete(ids);
			conferenceFileService.batchRemoveConference(ids);
			conferenceFeedbackService.batchRemoveConference(ids);
			conferenceFeedbackMarkService.batchRemoveConference(ids);
			return true;
		}
		return false;
	}
	/**批量取消会议，并能参会人员发送推送消息*/
	@Override
	public boolean batchCancel(Integer[] ids){
		if (null != ids && ids.length > 0) {

			return conferenceService.batchCancel(ids);
			
		}
		
		return false ;
	}
}
