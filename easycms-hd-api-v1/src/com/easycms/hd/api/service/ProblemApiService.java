package com.easycms.hd.api.service;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.ProblemTypeEnum;
import com.easycms.hd.api.response.ProblemPageResult;
import com.easycms.hd.problem.domain.Problem;

public interface ProblemApiService {
	ProblemPageResult getProblemPageDataResult(Page<Problem> page, Integer userId, ProblemTypeEnum problemType);
}
