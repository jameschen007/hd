package com.easycms.hd.api.service;

import java.util.List;
import java.util.Map;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enpower.request.EnpowerRequestDisappearPoint;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialBackfill;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanInsertWeld;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanStatusSyn;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanUpdateWeld;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendEquipment;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendMain;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendNotice;
import com.easycms.hd.api.enums.RollingPlanTypeEnum;
import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.api.request.underlying.WitingPlan;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanDataResult;
import com.easycms.hd.api.response.rollingplan.RollingPlanPageDataResult;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.SixLevelRollingPlan;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.management.user.domain.User;

public interface RollingPlanApiService {
	RollingPlanPageDataResult getRollingPlanPageDataResult(Page<RollingPlan> page, User user,
			RollingPlanTypeEnum status, String type, String keyword,Integer usingRoleId);

	RollingPlanDataResult rollingPlanTransferToSixLevelRollingPlan(RollingPlan rollingPlan);

	/**
	 * 进度计划同步
	 * 
	 * @param sixLevelRollingPlan
	 * @return
	 */
	JsonResult<Boolean> save(List<SixLevelRollingPlan> sixLevelRollingPlan) throws Exception;

	/**
	 * 发点子接口 推送接口名: 1插入发点通知单信息 所需数据
	 * 
	 * @param rollingPlanId
	 * @return
	 */
	public List<EnpowerRequestSendMain> findEnpowerSendPointMainByRollingPlanId(Integer rollingPlanId);

	/**
	 * 发点子接口 推送接口名: 2 插入通知单明细 所需数据
	 *
	 * 20181116 增加见证日期。
	 * 20190708　优化数据结构
	 * @return
	 */
//	public List<EnpowerRequestSendNotice> findEnpowerSendPointNoticeDetailByWorkStepIdsx (List<WorkStep> workStepList,String consteamName,RollingPlan plan,Map<Integer,WorkStepWitnessItemsForm> stepWitnessDateMap);

	public List<EnpowerRequestSendNotice> findEnpowerSendPointNoticeDetailByWorkStepIds(WitingPlan wplan, String consteamName);

	/**
	 * 发点子接口 推送接口名: 3插入设备清单 所需数据
	 * 
	 * @param rollingPlanId
	 * @return
	 */
	public List<EnpowerRequestSendEquipment> findEnpowerSendPointEquipByRollingPlanId(Integer rollingPlanId);

	/**
	 * 返回 通知单编号 注：编号格式为F-X-CPN-X-XXXX。其中F—CNF，X—机组号，CPN——Control Points
	 * Notification，X—专业代码，XXXX—流水编号。 各专业代码：1.管道（P）2. 焊接W 3. 机械(M) 4.电气（E）
	 * 5.仪控（I）6.保温（H）7. 通风（V）8. 防腐(A)
	 */
	public String getNoticeOrderNo(RollingPlan plan);

	/**
	 * 滚动计划状态同步接口： 推送接口名: 修改作业条目信息 所需数据
	 * 
	 * @param rollingPlanId
	 * @return
	 */
	public EnpowerRequestPlanStatusSyn findEnpowerStatusSynByRollingPlanId(RollingPlan plan,Integer rollingPlanId);

	/**
	 * 管道　插入焊口条目信息
	 * 
	 * @param rollingPlanId
	 * @return
	 */
	public EnpowerRequestPlanInsertWeld findEnpowerStatusInsertWeldRollingPlanId(RollingPlan plan,Integer rollingPlanId);
	/**
	 * 管道　更新焊口条目信息
	 * 
	 * @param rollingPlanId
	 * @return
	 */
	public EnpowerRequestPlanUpdateWeld findEnpowerStatusUpdateWeldRollingPlanId(RollingPlan plan,Integer rollingPlanId);

	/**
	 * 推送接口名: 修改材料清单 所需数据
	 */
	List<EnpowerRequestMaterialBackfill> findEnpowerMaterialInfoByRollingPlanId(Integer rollingPlanId);
	

///**
// * 消点接口 推送接口名: 		 所需数据
// * @param rollingPlanId
// * @return
// */
//	public List<EnpowerRequestDisappearPoint> findEnpowerDisappearPointByRollingPlanId (Integer rollingPlanId,Integer workStepId);
	

/**
 * 消点接口 推送接口名: 		 所需数据
 * @param rollingPlanId
 * @return
 */

	public List<EnpowerRequestDisappearPoint> findEnpowerDisappearPointByWitness(List<WorkStepWitness> wits,Integer[] witness);
	/**滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长*/
	JsonResult<Boolean> initRollingPlan(Integer userId, String username);	
	JsonResult<Boolean> initRollingPlan(Integer userId,Integer rollingPlanId, String username);	
	/**临时前绩效考核的考核数据全部清理*/
	JsonResult<Boolean> clearAllKpiScore();
	

}
