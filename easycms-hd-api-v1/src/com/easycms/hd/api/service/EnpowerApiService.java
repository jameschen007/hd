package com.easycms.hd.api.service;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.easycms.hd.api.enpower.request.EnpowerRequestFileInvalidNotice;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionComplete;
import com.easycms.hd.api.enpower.request.EnpowerRequestHSEQuestionSend;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelCheck;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialCancelSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutCheck;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialOutSearch;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialTodayOutList;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialTodayStoreList;
import com.easycms.hd.api.enpower.request.EnpowerRequestQualityQuestionSending;
import com.easycms.hd.hseproblem.domain.HseProblem;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.qualityctrl.domain.QualityControl;

public interface EnpowerApiService {
    
/**
 * 文件失效通知单接口 推送接口名: 文件失效通知单接口   
 * 
 */
public List<EnpowerRequestFileInvalidNotice> findEnpowerFileInvalidNoticeById(RollingPlan plan,Integer rollingPlanId);
/**
 * 责任单位整改完成接口 推送接口名: 责任单位整改完成接口
 */
public List<EnpowerRequestHSEQuestionComplete> findEnpowerHSEQuestionCompleteBy(Map<String,Object> map,String enpFlag,Integer solveUser) ;
/**
 * 上报安全整改问题接口 推送接口名: 上报安全整改问题接口 
 * 
 */
public List<EnpowerRequestHSEQuestionSend> findEnpowerHSEQuestionSend(HseProblem hp) ;

/**
 * 质量　上传质量问题整改接口
 */
public List<EnpowerRequestQualityQuestionSending> findEnpowerQualityQuestionSend(QualityControl qc,Integer zgr,Date zgDate,Date yzDate);
/**
 * 物项退库核实接口  推送接口名: 物项退库核实接口    
 * @param MATE_CODE	物项编码
 * @param ISSNO	退库单号
 * @param ISSQTY	退库量
 * @param QTY_RELEASED	核实量
 */
public List<EnpowerRequestMaterialCancelCheck> findEnpowerMaterialCancelCheckById(String MATE_CODE,String ISSNO,String ISSQTY,String QTY_RELEASED);
/**
 * 物项退库查询接口 推送接口名: 物项退库查询接口     
 * 
 */
public List<EnpowerRequestMaterialCancelSearch> findEnpowerMaterialCancelSearchById(String cancelNo);
/**
 * 物项出库核实接口 推送接口名: 物项出库核实接口     
 * @param MATE_CODE	物项编码
 * @param ISSNO	出库单号
 * @param ISSQTY	出库量
 * @param QTY_RELEASED	核实量
 */
public List<EnpowerRequestMaterialOutCheck> findEnpowerMaterialOutCheckById(String MATE_CODE,String ISSNO,String ISSQTY,String QTY_RELEASED);
/**
 * 物项出库查询接口 推送接口名: 物项出库查询接口     
 * 
 */
public List<EnpowerRequestMaterialOutSearch> findEnpowerMaterialOutSearchById(String outNo);
/**
 * 物项入库查询接口 推送接口名: 物项入库查询接口     
 * 
 */
//public List<EnpowerRequestMaterialStoreSearch> findEnpowerMaterialStoreSearchBywarrantyNo(String warrantyNo);
/**
 * 查询今日出库量明细接口 推送接口名: 查询今日出库量明细接口 
 */

public List<EnpowerRequestMaterialTodayOutList> findEnpowerMaterialTodayOutListById(RollingPlan plan,Integer rollingPlanId);
/**
 * 查询今日入库量明细接口  推送接口名:  查询今日入库量明细接口     
 * 
 */
public List<EnpowerRequestMaterialTodayStoreList > findEnpowerMaterialTodayStoreList();
/***
 * 修改安全整改完成情况
 * @param hp
 * @return
 */
public List<EnpowerRequestHSEQuestionSend> findEnpowerHSEQuestionSendUpdate(HseProblem hp,String RE_ZG,String END_DATE,String RQ_YZ,String enpFlag) ;
	

}
