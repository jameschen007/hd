package com.easycms.hd.api.service;

import java.util.Set;

import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.enums.QC2MemberTypeEnum;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsMainResult;
import com.easycms.hd.api.response.statistics.ConferenceStatisticsResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsMainResult;
import com.easycms.hd.api.response.statistics.MaterialStatisticsResult;
import com.easycms.hd.api.response.statistics.ProblemStatisticsResult;
import com.easycms.hd.api.response.statistics.RollingPlanStatisticsResult;
import com.easycms.hd.api.response.statistics.StatisticsMainResult;
import com.easycms.hd.api.response.statistics.WitnessStatisticsResult;
import com.easycms.management.user.domain.User;

public interface StatisticsApiService {
	StatisticsMainResult<RollingPlanStatisticsResult> getRollingPlanStatisticsResult(User user,String moduleType,Integer usingRoleId);

	StatisticsMainResult<ProblemStatisticsResult> getProblemStatisticsResult(User loginUser,String moduleType,Integer usingRoleId);
	StatisticsMainResult<ProblemStatisticsResult> getProblemStatisticsResultTwo(User loginUser,String moduleType,Integer usingRoleId);

	StatisticsMainResult<WitnessStatisticsResult> getWitnessStatisticsResult(User loginUser,String moduleType, QC2MemberTypeEnum memberType,Integer usingRoleId);

	Set<String> convertAuthRoleToList(Set<AuthRole> authRoles);

	//会议和通知的统计信息
	ConferenceStatisticsMainResult<ConferenceStatisticsResult> getConferenceStatisticsResult(Integer loginId);

	//物资日报统计信息
	MaterialStatisticsResult getMaterialStatisticsResult(Integer loginId);

	//用于物料日志部门出库的统计
	MaterialStatisticsMainResult getMaterialStatisticsMainResult(Integer loginId);
}
