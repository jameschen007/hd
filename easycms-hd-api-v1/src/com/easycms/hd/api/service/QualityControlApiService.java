package com.easycms.hd.api.service;

import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.core.util.Page;
import com.easycms.hd.api.request.qualityctrl.QcAssignRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcNoteRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcRenovateResultRequestForm;
import com.easycms.hd.api.request.qualityctrl.QcResultCheckEnum;
import com.easycms.hd.api.request.qualityctrl.QualityControlPageRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityControlRequestForm;
import com.easycms.hd.api.request.qualityctrl.QualityTypePageRequestForm;
import com.easycms.hd.api.response.qualityctrl.QualityControlPageDataResult;
import com.easycms.hd.api.response.qualityctrl.QualityControlResult;
import com.easycms.hd.qualityctrl.domain.QualityControl;

public interface QualityControlApiService {
	
	/**
	 * 创建质量问题
	 * @param qcRequestForm
	 * @param files
	 * @return
	 * @throws Exception 
	 */
	public boolean createQualityProblem(QualityControlRequestForm qcRequestForm, CommonsMultipartFile files[]) throws Exception;

	/**
	 * 获取质量问题分页列表
	 * @param qcRequestForm
	 * @return
	 */
	public QualityControlPageDataResult getQualityProblemPageResult(Page<QualityControl> page,QualityControlPageRequestForm qcRequestForm);
	
	public QualityControlPageDataResult getQualityProblemPageResultByType(Page<QualityControl> page,QualityTypePageRequestForm qcRequestForm);

	
	/**
	 * 数据转换为返回结果
	 * @param qc
	 * @return
	 */
	public QualityControlResult transferToResultList(QualityControl qc);

	/**
	 * QC专业室主任分派QC人员
	 * @return
	 */
	public boolean qcLeaderAssign(QcAssignRequestForm qcRequestForm);
	/**
	 * 责任部门分派责任班组人员
	 * @return
	 */
	public boolean responsibilityLeaderAssign(QcAssignRequestForm qcRequestForm);
	
	/**
	 * QC人员指定整改队伍，增加　可修改责任部门和责任班组
	 * @param qcRequestForm
	 * @return
	 */
	public Map<String,Object> qcAssignRenovateTeam(QcAssignRequestForm qcRequestForm,Long timeLimit,Integer deptId,Integer teamId) throws Exception ;
	
	/**
	 * 关闭问题
	 * @param qcProblemId
	 * @return
	 * @throws Exception 
	 */
	public boolean qcAssignClose(QcNoteRequestForm qcRequestForm,boolean qualityFlag) throws Exception;
	
	/**
	 * 提交整改结果
	 * @param qcRequestForm
	 * @return
	 */
	public boolean teamRenoveteResult(QcRenovateResultRequestForm qcRequestForm,CommonsMultipartFile files[]);
	
	/**
	 * QC核实整改结果
	 * @param qcRequestForm
	 * @return
	 * @throws Exception 
	 */
	public boolean qcVerifyResult(QcNoteRequestForm qcRequestForm,QcResultCheckEnum checkResult) throws Exception;
}
