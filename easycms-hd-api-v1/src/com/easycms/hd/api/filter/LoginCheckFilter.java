package com.easycms.hd.api.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.WebUtility;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;
import com.easycms.quartz.util.SpringContextsUtil;

public class LoginCheckFilter implements Filter {

	private Logger logger = Logger.getLogger(LoginCheckFilter.class);
	
	private static boolean readQc1ReplaceQc2 = false ;
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//麻烦你帮个忙初始化一下访问上下文
		HttpServletRequest req = (HttpServletRequest)request;
		if(com.easycms.common.logic.context.ContextPath.selfProjectContextPath==null){
//			getRequestURL()-getRequestURI()+getContextPath()
			com.easycms.common.logic.context.ContextPath.selfProjectContextPath = req.getRequestURL().substring(0,req.getRequestURL().indexOf(req.getContextPath())+req.getContextPath().length())+"/";
			if( req.getContextPath()==null || "".equals( req.getContextPath().trim())){
				String u = req.getRequestURL().toString();
				com.easycms.common.logic.context.ContextPath.selfProjectContextPath = u.substring(0,u.indexOf("/", 9)+1);
				logger.info("selfProjectContextPath="+com.easycms.common.logic.context.ContextPath.selfProjectContextPath);
			}
		}
		//将文件路径加载
		if(com.easycms.common.logic.context.ContextPath.fileBasePath==null){
	        InputStream in = this.getClass().getResourceAsStream("/api.properties");  
	        Properties p = new Properties();
	        try {  
	            p.load(in);
	            com.easycms.common.logic.context.ContextPath.fileBasePath = p.getProperty("file_base_path");
	            com.easycms.common.logic.context.ContextPath.winfileBasePath = p.getProperty("windows_file_base_path");
				if( false == com.easycms.common.logic.context.ContextPath.Linux.equals(com.easycms.common.logic.context.ContextPath.os_name)){
					com.easycms.common.logic.context.ContextPath.fileBasePath = com.easycms.common.logic.context.ContextPath.winfileBasePath ;
				}
				logger.info("fileBasePath="+com.easycms.common.logic.context.ContextPath.fileBasePath);
	        } catch (IOException e) {  
	            // TODO Auto-generated catch block  
	            e.printStackTrace();
	        }  
	        
		}
		

		//TODO 测试阶段使用以下代码，正式使用阶段放开if的判断。
		if(readQc1ReplaceQc2 == false){
	        //王志，关于福清二级QC修改一级QC问题，APP修改为一级QC，使用现有的QC1流程，把QC2中的见证员后面的监理，工程公司，业主选点加载到QC1流程后面，QC2流程全部取消

	        //默认根据ip设置　了是否　qc1ReplaceQc2　，为了便于本机开发测试，在此读取数据库中的配置，如果读取取，使用数据库的配置
			VariableSetService variableSetService = (VariableSetService) SpringContextsUtil.getBean("variableSetService");
			String seting = variableSetService.findValueByKey("qc1ReplaceQc2", "system");
			if("ON".equals(seting)){
				ContextPath.qc1ReplaceQc2 = true ;
				logger.debug("qc1ReplaceQc2标示使用数据库中的配置!");
			}else{
				ContextPath.qc1ReplaceQc2 = false ;
			}
//	 		String show = "qc1ReplaceQc2标示："+ContextPath.qc1ReplaceQc2 + (ContextPath.qc1ReplaceQc2?"无二级QC，一级QC见证并分派监理，工程公司，业主选点":"使用通用QC2流程（非福清QC1接外部单位流程）！");
//	 		logger.info(show);
//	 		readQc1ReplaceQc2=true ;
		}

		
		HttpServletResponse res = (HttpServletResponse) response;
		String path = req.getContextPath();
		String uri = req.getRequestURI();
		uri = uri.replace(path, "");
		logger.info("Filter path [" + uri + "]");
		logParameters(req);
		JsonResult<String> result = new JsonResult<String>();
		if(uri.equalsIgnoreCase("/hdxt/api/authenticate") || 
				uri.equalsIgnoreCase("/hdxt/api/getUserId") || 
				"/hdxt/api/getUserList".equalsIgnoreCase(uri)||
				uri.startsWith("/www/")||
				uri.startsWith("/hd/learning")||
				uri.startsWith("/hdxt/api/learning_op")||
				uri.startsWith("/hd/exam")||
				uri.equalsIgnoreCase("/hdxt/api/logout") || 
				uri.equalsIgnoreCase("/hdxt/api/mineProject") || 
				"/hdxt/api/banner".equalsIgnoreCase(uri) || 
				"/hdxt/api/test".equalsIgnoreCase(uri) || 
				"/hdxt/api/menubuild".equalsIgnoreCase(uri) || 
				"/hdxt/api/enpowerUserProcess".equalsIgnoreCase(uri) || 
				"/hdxt/api/problem/add".equalsIgnoreCase(uri)|| 
				"/hdxt/api/question/create".equalsIgnoreCase(uri)||
				"/hdxt/api/hse/create".equalsIgnoreCase(uri)||
				"/hdxt/api/hse/submitRenovateResult".equalsIgnoreCase(uri)||
				"/hdxt/api/qualityControl/create".equalsIgnoreCase(uri)||
				"/hdxt/api/question/answer".equalsIgnoreCase(uri)||
				"/hdxt/api/qualityControl/teamRenovete".equalsIgnoreCase(uri)||
				uri.startsWith("/hdxt/api/files/")||
				uri.startsWith("/hdxt/api/download/")||uri.startsWith("/hdxt/api/files/")||//附件图片查看*/ "/hdxt/api/files/problem/".equalsIgnoreCase(uri)|| //http://localhost:8080/easycms-website/hdxt/api/files/problem/_201709271719371506503977493.jpg
				uri.startsWith("/hdxt/api/enpower/material/material")||
				"/hdxt/api/question/feedback".equalsIgnoreCase(uri)) {
			chain.doFilter(request, response);
			return;
		} else {
			/**
             * 如果是来自于api调用，需要进行安全验证。
             * 
             * 验证规则为：
             * header中必须要：
             * 1. 包含timestamp字段，
             * 2. domain: 固定为enpower
             * 3. 包含一个signature字段
             *         其为：timestamp+domain+security_key的MD5值
             */
/*            Map<String, String> headers = getHeadersInfo(req);
            String timestamp = headers.get("timestamp");
            String domain = headers.get("domain");
            String signature = headers.get("signature");
            if (null != domain && null != timestamp && null != signature ) {
                if (domain.equals("enpower")) {
                    if (CommonUtility.MD5Digest(timestamp + domain + SecurityEnum.API_ENPOWER_SECURITY_KEY.getKey()).equalsIgnoreCase(signature)) {
                        chain.doFilter(req, res);
                    } else {
                        res.setStatus(HttpStatus.SC_UNAUTHORIZED);
                        result.setCode("-4000");
                        result.setMessage("错误的signature");
                        WebUtility.writeToClient(res, WebUtility.toJson(result));
                        return;
                    }
                } else if (domain.equals("mobile")) {
                	if (CommonUtility.MD5Digest(timestamp + domain + SecurityEnum.API_BASE_SECURITY_KEY.getKey()).equalsIgnoreCase(signature)) {
                        chain.doFilter(req, res);
                    } else {
                        res.setStatus(HttpStatus.SC_UNAUTHORIZED);
                        result.setCode("-4000");
                        result.setMessage("错误的signature");
                        WebUtility.writeToClient(res, WebUtility.toJson(result));
                        return;
                    }
                } else {
                    res.setStatus(HttpStatus.SC_BAD_REQUEST);
                    result.setCode("-4001");
                    result.setMessage("错误的domain");
                    WebUtility.writeToClient(res, WebUtility.toJson(result));
                    return;
                }
            } else {
                res.setStatus(HttpStatus.SC_UNAUTHORIZED);
                result.setCode("-4002");
                result.setMessage("timestamp，domain，signature均为必填参数。");
                WebUtility.writeToClient(res, WebUtility.toJson(result));
                return;
            }
			*/
			if (!uri.startsWith("/hdxt/api/enpower")) {
				//用户的loginId信息是否在在，是否能找到具体的用户
				UserService userService = (UserService) SpringContextsUtil.getBean("userService");
				
				String loginuserId = request.getParameter("loginId");
			  
				Integer userId = 0;
				if (!CommonUtility.isNonEmpty(loginuserId)){
					result.setCode("-1002");
					result.setMessage("loginId 是必须参数");
					WebUtility.writeToClient(res, WebUtility.toJson(result));
					return;
				} else {
					try {
						userId = Integer.parseInt(loginuserId);
					} catch (NumberFormatException e) {
						e.printStackTrace();
						result.setCode("-1002");
						result.setMessage("loginId 是不合法参数");
						WebUtility.writeToClient(res, WebUtility.toJson(result));
						return;
					}
				}
				
				User user = userService.findUserById(userId);
				if (null == user){
					result.setCode("-1002");
					result.setMessage("user 不存在");
					WebUtility.writeToClient(res, WebUtility.toJson(result));
					return;
				}else if (user.isDel()){
					result.setCode("-1001");
					result.setMessage("用户" + user.getRealname() + "被冻结");
					WebUtility.writeToClient(res, WebUtility.toJson(result));
					return;
				}
			}
			chain.doFilter(req, res);
		}
	}


	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
	
	private void logParameters(HttpServletRequest request){
	Enumeration<String> keys =	 request.getParameterNames();
		while (keys.hasMoreElements()) {
			String key  = keys.nextElement();
			logger.debug("request input parameters : [" + key + "=" + request.getParameter(key)+ " ]" );
		}
	}
	
	@SuppressWarnings("unused")
	private Map<String, String> getHeadersInfo(HttpServletRequest request) {
	    Map<String, String> map = new HashMap<String, String>();
	    Enumeration<String> headerNames = request.getHeaderNames();
	    while (headerNames.hasMoreElements()) {
	        String key = (String) headerNames.nextElement();
	        String value = request.getHeader(key);
	        map.put(key, value);
	    }
	    return map;
	  }
}
