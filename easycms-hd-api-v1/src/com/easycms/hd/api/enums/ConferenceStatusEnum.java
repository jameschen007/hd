package com.easycms.hd.api.enums;

public enum ConferenceStatusEnum {
	/**
	 * 未开始
	 */
	UNSTARTED,
	/**
	 * 进行中
	 */
	PROGRESSING,
	/**
	 * 已结束
	 */
	EXPIRED,
	/**
	 * 草稿
	 */
	DRAFT,
	/**
	 * 未知
	 */
	UNKNOWN,
	/**
	 * 取消
	 */
	CANCEL
}
