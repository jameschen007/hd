package com.easycms.hd.api.enums.question;

/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>用于队长 Form表单使用<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
public enum QuestionStatusEnum_DZ { 
	/**
	 * 待解决的问题
	 */
	NEED_SOLVED, 
	/**
	 * 已解决的问题
	 */
	SOLVED,
}
