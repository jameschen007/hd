package com.easycms.hd.api.enums.question;

/**
 * 这里的状态是用于存到数据库中的
 * @author ul-webdev
 *
 */
public enum QuestionStatusDbEnum { 
	/**
	 * 待解决
	 */
	pre,
	/**
	 * 待确认
	 */
	undo, 
	/**
	 * 已处理
	 */
	done,
	/**
	 * 已指派
	 */
	assign,
	/**
	 * 已回复（提供解决方案）
	 */
	reply,
	/**
	 * 仍未解决
	 */
	unsolved
	,
	/**
	 * 已解决
	 */
	solved
;
	public static void main(String[] args) {
		for(QuestionStatusDbEnum b:QuestionStatusDbEnum.values()){
			System.out.println(b);
		}
	}
}
