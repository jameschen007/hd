package com.easycms.hd.api.enums.question;

/**
 * 用于组长 Form表单使用
 * @author ul-webdev
 *
 */
public enum QuestionStatusEnum_ZZ { 
	/**
	 * 待解决的问题
	 */
	NEED_SOLVE, 
	/**
	 * 待确认的问题
	 */
	NEED_CONFIRM, 
	/**
	 * 已经解决
	 */
	SOLVED,
}
