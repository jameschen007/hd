package com.easycms.hd.api.enums.question;

import java.util.HashMap;
import java.util.Map;

public class QuestionStatusDbMap {
	public static Map<Object,String> map = new HashMap<Object,String>();
	static {
		if(map.size()<1){
			map.put(QuestionStatusDbEnum.pre, "待解决");
			map.put(QuestionStatusDbEnum.undo, "待确认");
			map.put(QuestionStatusDbEnum.done, "已处理");
			map.put(QuestionStatusDbEnum.assign, "已指派");
			map.put(QuestionStatusDbEnum.reply, "已回复");//（提供解决方案）
			map.put(QuestionStatusDbEnum.unsolved, "仍未解决");
			map.put(QuestionStatusDbEnum.solved, "已解决");
//			map.put(QuestionStatusDbEnum.closed, "已关闭");//暂无
			
			map.put("pre", "待解决");
			map.put("undo", "待确认");
			map.put("done", "已处理");
			map.put("assign", "已指派");
			map.put("reply", "已回复");//（提供解决方案）
			map.put("unsolved", "仍未解决");
			map.put("solved", "已解决");
			map.put("closed", "已关闭");//暂无
			
		}
	}
	
	public static void main(String[] args) {
			for(QuestionStatusDbEnum b:QuestionStatusDbEnum.values()){
				System.out.println(b+" "+ map.get(b));
				System.out.println(b.toString()+" == "+ map.get(b.toString()));
			}
		}
}

