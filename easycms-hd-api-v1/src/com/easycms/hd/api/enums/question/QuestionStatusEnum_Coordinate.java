package com.easycms.hd.api.enums.question;

/**
 * 用于作业问题协调人员列表 Form表单使用
 * 创建人：</b>王志<br>
 *
 */
public enum QuestionStatusEnum_Coordinate { 
	/**
	 * 协调待指派
	 */
	NEED_ASSIGN, 
	/**
	 * 协调已指派
	 */
	ASSIGNED, 
}
