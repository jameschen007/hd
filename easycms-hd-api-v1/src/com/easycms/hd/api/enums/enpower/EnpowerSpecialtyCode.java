package com.easycms.hd.api.enums.enpower;
/**
 * Enpower专业代码
 * @author ul-webdev
 *
 */
public enum EnpowerSpecialtyCode {
//	各专业代码：1.管道（P）2. 焊接W 3. 机械(M) 4.电气（E） 5.仪控（I）6.保温（H）7. 通风（V）8. 防腐(A) 
	P("管道"),
	W("焊接"),
	M("机械"),
	E("电气"),
	I("仪控"),
	H("保温"),
	V("通风"),
	A("防腐");
	
	EnpowerSpecialtyCode(String msg){
		this.msg = msg;
	}
	
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public static void main(String[] args) {

System.out.println(EnpowerSpecialtyCode.P.toString());
System.out.println(EnpowerSpecialtyCode.P.getMsg());

	for(EnpowerSpecialtyCode x:EnpowerSpecialtyCode.values()){
		if(x.toString().equals("P")){
			System.out.println("是这母P");
		}
		if(x.getMsg().equals("通风")){
			System.out.println("是这通风");
		}
	}
	}
	
}
