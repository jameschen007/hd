package com.easycms.hd.api.enums.enpower;

/**
 * Enpower中安全文明施工状态 枚举
 * @author ul-webdev
 *
 */
public class EnpowerHSEStatus {
	//状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
	public static String WRITING="0";
	public static String NOT_RECEIVE="1";
	public static String RECEIVED="2";
	public static String FEEDBACK="3";
	public static String COMPLETE="4";
	public static String FAILD="5";

}
