package com.easycms.hd.api.enums.hseproblem.underlying;

/**
 * @author wangzhi
 * @Description: 返回给app的时间筛选下拉框渲染使用的对象
 * @date 2019/7/3 9:59
 */

public class TheFilterItem {
    private String key;
    private String value;
    private Integer order;

    public TheFilterItem(String key, String value) {
        this.key = key;
        this.value = value;
    }
    public TheFilterItem(String key, String value,Integer order) {
        this.key = key;
        this.value = value;
        this.order = order;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}