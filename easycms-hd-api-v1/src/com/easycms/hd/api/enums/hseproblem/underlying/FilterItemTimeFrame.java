package com.easycms.hd.api.enums.hseproblem.underlying;


import com.easycms.common.util.DateUtilReceive;
import com.easycms.hd.api.enums.hseproblem.DynamicFilterItems;
import jodd.madvoc.meta.In;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author wangzhi
 * @Description: 返回给app的时间筛选下拉框渲染使用的对象 子类，内部计算是否显示和时间范围
 * @date 2019/7/3 10:46
 */
public class FilterItemTimeFrame extends TheFilterItem {

    private Date begin;
    private Date end;
    //这个月的早些时候	可能没有	没有的条件：	上周或本周是本月的首周
    //本周早些时候	可能没有	没有的条件：	昨天或今天是周一
    private boolean display;

    public TheFilterItem getSuper(){
        return new TheFilterItem(getKey(),getValue(),getOrder());
    }

    public FilterItemTimeFrame(String key, String value) {
        super(key, value);

    }
    public FilterItemTimeFrame(String key, String value, Integer order) {
        super(key, value,order);

    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public boolean isDisplay() {

        //设置　开始和结束时间，全部除外
        if (getKey().equals(DynamicFilterItems.monthEarly.getCode())) {
            // "这个月的早些时候"),
            setBegin(DateUtilReceive.getBeginDayOfMonth());
            setEnd(DateUtilReceive.getBeginDayOfLastWeek());//上周以前
        }else if(getKey().equals(DynamicFilterItems.lastWeek.getCode())){

            //"上周"),
            setBegin(DateUtilReceive.getBeginDayOfLastWeek());
//            setEnd(DateUtilReceive.getEndDayOfLastWeek());
            setEnd(DateUtilReceive.getBeginDayOfWeek());
        }else if(getKey().equals(DynamicFilterItems.weekEarly.getCode())){

            //"本周早些时候"),
            //
            setBegin(DateUtilReceive.getBeginDayOfWeek());
            setEnd(DateUtilReceive.getBeginDayOfYesterday());
        }else if(getKey().equals(DynamicFilterItems.yesterday.getCode())){

            setBegin(DateUtilReceive.getBeginDayOfYesterday());
//            setEnd(DateUtilReceive.getEndDayOfYesterDay());
            setEnd(DateUtilReceive.getDayBegin());
            //"昨天"),
        }else if(getKey().equals(DynamicFilterItems.today.getCode())){

            setBegin(DateUtilReceive.getDayBegin());
            setEnd(DateUtilReceive.getDayEnd());
            //"今天"),
        }

        //这个月的早些时候	可能没有	没有的条件：	上周或本周是本月的首周
        //本周早些时候	可能没有	没有的条件：	昨天或今天是周一
        if (this.getKey().equals(DynamicFilterItems.monthEarly.getCode()) || this.getKey().equals(DynamicFilterItems.weekEarly.getCode())) {

            //这个月的早些时候	可能没有	没有的条件：	上周或本周是本月的首周
            if (this.getKey().equals(DynamicFilterItems.monthEarly.getCode())) {
                Calendar c = Calendar.getInstance();
                Date today = c.getTime();

                //上周
                Date lastWeek = DateUtilReceive.getBeginDayOfLastWeek();
                //本周
                Date thisWeek = DateUtilReceive.getBeginDayOfWeek();
                //首周

                Date thisMonth = DateUtilReceive.getBeginDayOfMonth();

                if (thisMonth.compareTo(lastWeek) == 0) {
                    display = false ;
                }else if(thisMonth.compareTo(thisWeek) == 0){
                    display = false ;
                }else {
                    display = true ;
                }

            }
            //本周早些时候	可能没有	没有的条件：	昨天或今天是周一
            else if (this.getKey().equals(DynamicFilterItems.weekEarly.getCode())) {
                //昨天
                Date yestoday = DateUtilReceive.getBeginDayOfYesterday();
                //今天
                Date today = DateUtilReceive.getDayBegin();
                //周一
                Date week = DateUtilReceive.getBeginDayOfWeek();
                if (week.compareTo(yestoday) == 0 || week.compareTo(today) == 0 ) {
                    display = false;
                } else {
                    display = true ;
                }
            }

        } else {
            display = true;
        }


        return display;
    }

    public void print() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("key="+getKey()+" value="+ getValue()+" order="+getOrder()+" display="+(display?"显示":"不显示")+ (getBegin()!=null?sdf.format(getBegin()):"")+" "+ (getEnd()!=null?sdf.format(getEnd()):""));
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public void setOrder(Integer order) {
        super.setOrder(order);
//        print();
    }

}
