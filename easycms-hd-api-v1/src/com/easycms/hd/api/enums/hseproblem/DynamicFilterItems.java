package com.easycms.hd.api.enums.hseproblem;

/**
 * @author wangzhi
 * @Description: 标题下拉菜单进行筛选项,全部
 * @date 2019/7/1 15:58
 */
public enum DynamicFilterItems {

    /**
     * 以当前时间动态决定范围
     这个月的早些时候
     上周
     本周早些时候
     昨天
     今天

     */

    createdAll("createdAll", "提出时间-全部"),
    monthEarly("monthEarly", "这个月的早些时候"),
    lastWeek("lastWeek", "上周"),
    weekEarly("weekEarly", "本周早些时候"),
    yesterday("yesterday", "昨天"),
    today("today", "今天"),

    cutOffAll("cutOffAll", "截止时间-全部"),

    problemStatusAll("problemStatusAll", "状态-全部"),
    Need_Handle("Need_Handle", "待处理"),
    Renovating("Renovating", "整改中"),
    Need_Check("Need_Check", "待审查"),
    Finish("Finish", "已完成"),
    None("None", "不需要处理");

    private String code ;

    private String msg ;

    DynamicFilterItems(String code, String msg) {
        this.code = code ;
        this.msg = msg ;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
