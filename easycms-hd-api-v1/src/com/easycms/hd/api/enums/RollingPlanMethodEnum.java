package com.easycms.hd.api.enums;

public enum RollingPlanMethodEnum { 
	/**
	 * 分派操作
	 */
	ASSIGN, 
	/**
	 * 改派
	 */
	REASSIGN,
	/**
	 * 解除
	 */
	RELEASE
}
