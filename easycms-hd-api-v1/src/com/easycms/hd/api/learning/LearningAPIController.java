package com.easycms.hd.api.learning;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.learning.response.LearningResult;
import com.easycms.hd.api.learning.response.SectionDetailResult;
import com.easycms.hd.api.learning.response.SectionResult;
import com.easycms.hd.api.learning.service.LearningApiService;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearningSectionService;
import com.easycms.hd.response.JsonResult;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/learning")
@Api(value = "LearningAPIController", description = "培训管理相关的api")
public class LearningAPIController {
	
	@Autowired
	private LearningSectionService learningSectionService;
	
	@Autowired
	private LearningApiService learningApiService;
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/section", method = RequestMethod.GET)
	@ApiOperation(value = "培训页面主模块", notes = "培训管理页面主模块的获取")
	public JsonResult<List<SectionResult>> module(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<List<SectionResult>> result = new JsonResult<List<SectionResult>>();
		
		List<SectionResult> sectionResults = new ArrayList<SectionResult>();

		List<LearningSection> learningSections = learningSectionService.findByStatus("ACTIVE");

		sectionResults = learningSections.stream().map(sections -> {
			SectionResult sectionResult = new SectionResult();
			sectionResult.setName(sections.getName());
			sectionResult.setId(sections.getId());
			sectionResult.setType(sections.getType());
			return sectionResult;
		}).collect(Collectors.toList());

		result.setResponseResult(sectionResults);
		return result;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/section/{section}", method = RequestMethod.GET)
	@ApiOperation(value = "培训页面主模块下的分支", notes = "培训管理页面主模块分支的获取")
	public JsonResult<SectionDetailResult> section(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "例如基础培训则传入JCPX",required=true) @PathVariable("section") String section,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<SectionDetailResult> result = new JsonResult<SectionDetailResult>();
		
		result.setResponseResult(learningApiService.getSection(section, form.getLoginId()));
		return result;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "培训详细信息", notes = "培训管理详细页面的获取")
	public JsonResult<LearningResult> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "培训课程的ID",required=true) @PathVariable("id") Integer id,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<LearningResult> result = new JsonResult<LearningResult>();
		
		result.setResponseResult(learningApiService.getDetail(id));
		return result;
	}
}
