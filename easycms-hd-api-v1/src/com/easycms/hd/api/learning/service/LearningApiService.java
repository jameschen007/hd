package com.easycms.hd.api.learning.service;

import java.util.List;

import com.easycms.hd.api.learning.response.FoldResult;
import com.easycms.hd.api.learning.response.LearnedHistoryPageResult;
import com.easycms.hd.api.learning.response.LearningBeginResult;
import com.easycms.hd.api.learning.response.LearningResult;
import com.easycms.hd.api.learning.response.SectionDetailResult;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.domain.LearningFold;

public interface LearningApiService {
	/**
	 * 根据用户传入的section类型去获取其分支下的所有内容
	 * @param section
	 * @param userId
	 * @return
	 */
	SectionDetailResult getSection(String section, Integer userId);
	
	/**
	 * 转换fold
	 * @param fold
	 * @return
	 */
	FoldResult transferFold(LearningFold fold, boolean simple);
	List<FoldResult> transferFold(List<LearningFold> folds, boolean simple);
	/**
	 * 转换Learning
	 * @param learning
	 * @return
	 */
	LearningResult transferLearning(Learning learning, boolean simple);
	List<LearningResult> transferLearning(List<Learning> learnings, boolean simple);
	
	/**
	 * 获取培训课程的详情
	 * @param id
	 * @return
	 */
	LearningResult getDetail(Integer id);
	
	/**
	 * 记录学习开始
	 * @param loginId
	 * @param learningId
	 * @return
	 */
	public LearningBeginResult learnBegin(Integer userId,Integer learningId) throws Exception;
	
	/**
	 * 记录学习结束
	 * @param loginId
	 * @param learningId
	 * @param uniqueCode
	 * @return
	 */
	public boolean learnEnd(Integer userId,Integer learningId,String uniqueCode) throws Exception;
	
	/**
	 * 
	 * 获取培训记录
	 * @return
	 */
	public LearnedHistoryPageResult getLearnedHistory(BasePagenationRequestForm form);
}
