package com.easycms.hd.api.learning.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.api.learning.response.FoldResult;
import com.easycms.hd.api.learning.response.LearnedHistoryPageResult;
import com.easycms.hd.api.learning.response.LearnedHistoryResult;
import com.easycms.hd.api.learning.response.LearningBeginResult;
import com.easycms.hd.api.learning.response.LearningResult;
import com.easycms.hd.api.learning.response.ModuleFoldResult;
import com.easycms.hd.api.learning.response.SectionDetailResult;
import com.easycms.hd.api.learning.service.LearningApiService;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.response.FileResult;
import com.easycms.hd.learning.domain.LearnedHistory;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.learning.domain.LearningFold;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearnedHistoryService;
import com.easycms.hd.learning.service.LearningFileService;
import com.easycms.hd.learning.service.LearningFoldService;
import com.easycms.hd.learning.service.LearningSectionService;
import com.easycms.hd.learning.service.LearningService;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("learningApiService")
public class LearningApiServiceImpl implements LearningApiService {
	@Autowired
	private LearningService learningService;
	@Autowired
	private LearningFileService learningFileService;
	@Autowired
	private LearningFoldService learningFoldService;
	@Autowired
	private ModulesService modulesService;
	@Autowired
	private LearnedHistoryService learnedHistoryService;
	@Autowired
	private LearningSectionService learningSectionService;
	
	@Value("#{APP_SETTING['file_learning_path']}")
	private String fileLearningPath;
	
	@Value("#{APP_SETTING['file_learning_convert_path']}")
	private String fileLearningConvertPath;
	
	@Value("#{APP_SETTING['file_learning_cover_path']}")
	private String fileLearningCoverPath;

	@Override
	public SectionDetailResult getSection(String section, Integer userId) {
		SectionDetailResult sectionDetailResult = new SectionDetailResult();
		sectionDetailResult.setType(section);
		
		List<LearningFold> learningFolds = learningFoldService.findBySection(section);
		if (null != learningFolds && !learningFolds.isEmpty()) {
			List<LearningFold> withModule = learningFolds.stream().filter(a -> {return CommonUtility.isNonEmpty(a.getModule());}).collect(Collectors.toList());
			List<LearningFold> withoutModule = learningFolds.stream().filter(a -> {return !CommonUtility.isNonEmpty(a.getModule());}).collect(Collectors.toList());
			sectionDetailResult.setFolds(transferFold(withoutModule, true));
			if (null != withModule && !withModule.isEmpty()) {
				Map<String, List<LearningFold>> mapperFolds = withModule.stream().collect(Collectors.groupingBy(LearningFold::getModule));
				
				List<ModuleFoldResult> modules = mapperFolds.entrySet().stream().map(mapper -> {
					List<LearningFold> values = mapper.getValue();
					Modules module = modulesService.findByType(mapper.getKey());
					if (null != module) {
						ModuleFoldResult result = new ModuleFoldResult();
						result.setId(module.getId());
						result.setType(module.getType());
						result.setName(module.getName());
						
						List<FoldResult> foldResults = transferFold(values, true);
						result.setFolds(foldResults);
						return result;
					}
					return null;
				}).collect(Collectors.toList());
				
				sectionDetailResult.setModules(modules);
			}
		}
		return sectionDetailResult;
	}

	@Override
	public FoldResult transferFold(LearningFold fold, boolean simple) {
		if (null != fold) {
			FoldResult foldResult = new FoldResult();
			foldResult.setId(fold.getId());
			foldResult.setName(fold.getName());
			List<Learning> learnings = learningService.findByFold(fold.getId());
			if (null != learnings && !learnings.isEmpty()) {
				List<LearningResult> learningResults = transferLearning(learnings, true);
				foldResult.setLearnings(learningResults);
			}
			return foldResult;
		}
		return null;
	}

	@Override
	public List<FoldResult> transferFold(List<LearningFold> folds, boolean simple) {
		if (null != folds && !folds.isEmpty()) {
			List<FoldResult> foldResults = new ArrayList<FoldResult>();
			for (LearningFold fold : folds) {
				foldResults.add(this.transferFold(fold, simple));
			}
			return foldResults;
		}
		return null;
	}

	@Override
	public LearningResult transferLearning(Learning learning, boolean simple) {
		if (null != learning) {
			LearningResult learningResult = new LearningResult();
			learningResult.setId(learning.getId());
			learningResult.setTitle(learning.getTitle());
			learningResult.setType(learning.getType());
			if (!simple) {
				learningResult.setDesctiption(learning.getDescription());
				List<LearningFile> files = learningFileService.findFilesByLearningId(learning.getId());
				if (null != files && !files.isEmpty()) {
					List<FileResult> fileResults = files.stream().map(file -> {
						FileResult fileResult = new FileResult();
						fileResult.setId(file.getId());
						if (CommonUtility.isNonEmpty(file.getConventerPath())) {
//							fileResult.setUrl("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningConvertPath.replace("/", "*") + file.getConventerPath());
							fileResult.setUrl("/video/" + fileLearningConvertPath + file.getConventerPath());
						} else {
//							fileResult.setUrl("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + file.getPath());
							fileResult.setUrl("/video/"+ file.getPath());
						}
						if (CommonUtility.isNonEmpty(file.getCoverPath())) {
//							fileResult.setCover("/hdxt/api/files/" + fileLearningPath.replace("/", "*") + fileLearningCoverPath.replace("/", "*") + file.getCoverPath());
							fileResult.setCover("/video/" + fileLearningCoverPath + file.getCoverPath());
						}
						if (CommonUtility.isNonEmpty(file.getConventerFlag())) {
							fileResult.setConvertStatus(file.getConventerFlag());
						}
						fileResult.setFileName(file.getFileName() + (CommonUtility.isNonEmpty(file.getSuffix())?("." + file.getSuffix()):""));
						return fileResult;
					}).collect(Collectors.toList());
					learningResult.setFiles(fileResults);
				}
			}
			return learningResult;
		}
		return null;
	}

	@Override
	public List<LearningResult> transferLearning(List<Learning> learnings, boolean simple) {
		if (null != learnings && !learnings.isEmpty()) {
			List<LearningResult> learningResults = new ArrayList<LearningResult>();
			for (Learning learning : learnings) {
				learningResults.add(this.transferLearning(learning, simple));
			}
			return learningResults;
		}
		return null;
	}

	@Override
	public LearningResult getDetail(Integer id) {
		Learning learning = learningService.findById(id);
		return transferLearning(learning, false);
	}

	@Override
	public LearningBeginResult learnBegin(Integer userId, Integer learningId) throws Exception{
		LearningBeginResult learningBeginResult = new LearningBeginResult();
		String uniqueCode = CommonUtility.MD5Digest(learningId +""+ System.currentTimeMillis());
		LearnedHistory learnedHistory = new LearnedHistory();
		Learning learning = learningService.findById(learningId);
		if(learning == null){
			log.error("学习记录失败！未查询到课程ID为："+learningId+"的相关课程信息。");
			return null;
		}
		learnedHistory.setLearning(learning);			
		learnedHistory.setUserId(userId);
		learnedHistory.setStartDate(new Date());
		learnedHistory.setUniqueCode(uniqueCode);
		learnedHistory.setStatus("LEARNING");
		learnedHistory.setCreateBy(userId);
		learnedHistory.setCreateOn(learnedHistory.getStartDate());
		learnedHistory.setEndDate(learnedHistory.getStartDate());
		learnedHistory.setDuration("1");
		
		learnedHistoryService.add(learnedHistory);
		if(learnedHistory.getId()==null || learnedHistory.getId()==0){
			log.error("学习记录失败！");
			return null;
		}
		learningBeginResult.setUniqueCode(uniqueCode);
		return learningBeginResult;
	}

	@Override
	public boolean learnEnd(Integer userId, Integer learningId,String uniqueCode) throws Exception{
		LearnedHistory learnedHistory = learnedHistoryService.findByUniqueCode(uniqueCode);
		if(learnedHistory == null){
			log.error("未查询到用户"+userId+"唯一编码为："+uniqueCode+"的学习记录！");
			return false;			
		}
		learnedHistory.setEndDate(new Date());
		if(learnedHistory.getStartDate() != null){
			Long duration = new Date().getTime()-learnedHistory.getStartDate().getTime();
			learnedHistory.setDuration(String.valueOf(duration));			
		}
		learnedHistory.setStatus("LEARNED");
		learnedHistoryService.add(learnedHistory);
		return true;
	}

	@Override
	public LearnedHistoryPageResult getLearnedHistory(BasePagenationRequestForm form) {
		LearnedHistoryPageResult learnedHistoryPageResult = new LearnedHistoryPageResult();
		Page<LearnedHistory> page = new Page<LearnedHistory>();
		if (null != form.getPagenum() && null != form.getPagesize()) {
			page.setPageNum(form.getPagenum());
			page.setPagesize(form.getPagesize());
		}
		page = learnedHistoryService.findByUser(form.getLoginId(), page);
		
		learnedHistoryPageResult.setPageCounts(page.getPageCount());
		learnedHistoryPageResult.setPageNum(page.getPageNum());
		learnedHistoryPageResult.setPageSize(page.getPagesize());
		learnedHistoryPageResult.setTotalCounts(page.getTotalCounts());
				
		learnedHistoryPageResult.setData(
				page.getDatas().stream().map(mapper -> {
					return transferLearnedHistory(mapper);
					}).collect(Collectors.toList()));
		
		return learnedHistoryPageResult;
	}
	
	private LearnedHistoryResult transferLearnedHistory(LearnedHistory learnedHistory){
		LearnedHistoryResult learnedHistoryResult = new LearnedHistoryResult();
		learnedHistoryResult.setLearnedDate(learnedHistory.getStartDate());
		
		Learning learing = learnedHistory.getLearning();
		LearningFold learningFold = null;
		try {
			learningFold = learningFoldService.findById(learing.getFold());			
		} catch (Exception e) {
			log.error("查询课程数据异常："+e.getMessage());
			return null;
		}
		
		//返回培训种类
		String type = null;
		Modules module = null;
		LearningSection learningSection = null;
		if(learningFold != null){
			if(learningFold.getModule() != null){
				module = modulesService.findByType(learningFold.getModule());
				if(module!=null)
				type = module.getName();
			}else if(learningFold.getSection() != null){
				learningSection = learningSectionService.findByType(learningFold.getSection());
			}
		}
		
		if(learningSection != null){
			type = learningSection.getName();
		}else if(module != null){
			type = module.getName();
		}
		learnedHistoryResult.setLearnedType(type);
		
		learnedHistoryResult.setLearnedDuration(learnedHistory.getDuration());
		return learnedHistoryResult;
	}
	

}
