package com.easycms.hd.api.learning;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.hd.api.learning.response.LearnedHistoryPageResult;
import com.easycms.hd.api.learning.response.LearningBeginResult;
import com.easycms.hd.api.learning.service.LearningApiService;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.response.JsonResult;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/hdxt/api/learning_op")
@Api(value = "LearningOperationAPIController", description = "培训管理相关的api")
public class LearningOperationAPIController {
	
	@Autowired
	private LearningApiService learningApiService;
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	@ApiOperation(value = "获取历史记录", notes = "指定用户的历史记录。")
	public JsonResult<LearnedHistoryPageResult> getHistory(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") BasePagenationRequestForm form) throws Exception {
		JsonResult<LearnedHistoryPageResult> result = new JsonResult<LearnedHistoryPageResult>();
		LearnedHistoryPageResult responseResult = learningApiService.getLearnedHistory(form);
		result.setResponseResult(responseResult);
		return result;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
//	@ResponseBody
//	@RequestMapping(value = "/history", method = RequestMethod.POST)
//	@ApiOperation(value = "写入历史记录", notes = "指定用户的历史记录。")
//	public JsonResult<Boolean> history(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute("form") BaseRequestForm form) throws Exception {
//		JsonResult<Boolean> result = new JsonResult<Boolean>();
//		
//		return result;
//	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/learnBegin", method = RequestMethod.POST)
	@ApiOperation(value = "学习开始", notes = "学习开始")
	public JsonResult<LearningBeginResult> learnBegin(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "learningId", value = "learningId")@RequestParam(value="learningId",required=true) Integer learningId,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<LearningBeginResult> result = new JsonResult<LearningBeginResult>();
		//学习开始，传入learn_id,生成一个唯一时长序列返回，并将这个唯一值存入
		LearningBeginResult uniqueCode = learningApiService.learnBegin(form.getLoginId(), learningId);
		if(uniqueCode == null){
			result.setCode("-1001");
			result.setMessage("学习记录失败！");
		}
		result.setResponseResult(uniqueCode);
		return result;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/learnEnd", method = RequestMethod.POST)
	@ApiOperation(value = "学习结束", notes = "学习结束")
	public JsonResult<Boolean> learnEnd(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "learningId", value = "learningId")@RequestParam(value="learningId",required=true) Integer learningId,
			@ApiParam(required = true, name = "uniqueCode", value = "uniqueCode")@RequestParam(value="uniqueCode",required=true) String uniqueCode,
			@ModelAttribute("form") BaseRequestForm form) throws Exception {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		boolean status = false;
		status = learningApiService.learnEnd(form.getLoginId(), learningId, uniqueCode);
		if(!status){
			result.setCode("-1000");
			result.setMessage("操作失败！");
		}
		result.setResponseResult(status);
		return result;
	}
	
}
