package com.easycms.hd.api.learning.response;

import java.util.List;

import lombok.Data;

@Data
public class ModuleFoldResult {
	private Integer id;
	private String type;
	private String name;
	private List<FoldResult> folds;
}
