package com.easycms.hd.api.learning.response;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 培训历史记录返回数据
 * @author Mao.Zeng@MG
 *
 */
@Data
public class LearnedHistoryResult implements Serializable{
	private static final long serialVersionUID = -6830276288184670824L;
	
	/**
	 * 培训日期
	 */
	private Date learnedDate;
	
	/**
	 * 培训种类
	 */
	private String learnedType;
	
	/**
	 * 培训时长
	 */
	private String learnedDuration;

}
