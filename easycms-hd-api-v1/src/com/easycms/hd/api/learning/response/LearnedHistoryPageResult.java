package com.easycms.hd.api.learning.response;

import java.util.List;

import com.easycms.hd.api.response.PagenationResult;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分页培训记录返回数据
 * @author Mao.Zeng@MG
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LearnedHistoryPageResult extends PagenationResult {
	
	private List<LearnedHistoryResult> data;

}
