package com.easycms.hd.api.learning.response;

import java.util.List;

import com.easycms.hd.api.response.FileResult;

import lombok.Data;

@Data
public class LearningResult {
	private Integer id;
	private Integer seq;
	private String title;
	private String type;
	private String desctiption;
	private List<FileResult> files;
}
