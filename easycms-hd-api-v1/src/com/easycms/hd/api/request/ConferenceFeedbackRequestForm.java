package com.easycms.hd.api.request;

import java.io.Serializable;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ConferenceFeedbackRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 5723214498321521210L;
	@ApiParam(value = "会议Id", required = true)
	private Integer conferenceId;
	@ApiParam(value = "回复内容", required = false)
	private String message;
}
