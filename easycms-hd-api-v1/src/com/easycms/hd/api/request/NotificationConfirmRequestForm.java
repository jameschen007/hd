package com.easycms.hd.api.request;

import java.io.Serializable;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotificationConfirmRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -6374057777999517361L;
	@ApiParam(value = "通告Id", required = true)
    private Integer notificationId;
}
