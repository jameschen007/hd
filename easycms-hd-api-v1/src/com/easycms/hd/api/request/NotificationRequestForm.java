package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.easycms.hd.api.enums.ConferenceAlarmTypsEnums;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.conference.enums.ConferenceType;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NotificationRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -4243628993031692764L;
	
	@ApiParam(value = "通知ID", required = false)
	private Integer id;
	@ApiParam(value = "通知主题", required = false)
	private String subject;
	@ApiParam(value = "通知正文", required = false)
	private String content;
	@ApiParam(value = "通知类别", required = false)
	private String category;
	@ApiParam(value = "部门名称", required = false)
	private String department;
	@ApiParam(value = "开始时间", required = false)
	private Date startTime;
	@ApiParam(value = "结束时间", required = false)
	private Date endTime;
	@ApiParam(value = "提醒时间", required = false)
	private ConferenceAlarmTypsEnums alarmTime = ConferenceAlarmTypsEnums.NONE;
	@ApiParam(value = "通知的存档类型", required = true)
	private ConferenceType type;
	@ApiParam(value = "参会人", required = false)
	private Integer[] participants;
	@ApiParam(value = "需要保留的文件ID", required = false)
	private List<Integer> fileIds;
}
