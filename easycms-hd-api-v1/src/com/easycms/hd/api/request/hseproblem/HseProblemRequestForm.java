package com.easycms.hd.api.request.hseproblem;

import java.io.Serializable;

import com.easycms.hd.api.request.LoginIdRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemRequestForm extends LoginIdRequestForm implements Serializable {
	private static final long serialVersionUID = -1621034821910095395L;

	@ApiParam(value="检查类型",required=false)
	String checkType;
	@ApiParam(value="隐患严重性类别",required=false)
	String criticalLevel;
	@ApiParam(value="问题名称",required=false)
	String problemTitle;
	@ApiParam(value="问题名称new",required=false)
	String code1Id;
	@ApiParam(value="二级编码",required=false)
	String code2Id;
	@ApiParam(value="隐患描述",required=false)
	String code3Id;
	
	@ApiParam(value="机组",required=true)
	String unit;
	
	@ApiParam(value="厂房",required=true)
	String wrokshop;
	
	@ApiParam(value="标高",required=false)
	String eleration;
	
	@ApiParam(value="房间号",required=false)
	String roomno;
	
	@ApiParam(value="问题描述",required=true)
	String description;
	
	@ApiParam(value="责任部门",required=true)
	Integer responsibleDeptId;
	
	@ApiParam(value="责任班组",required=false)
	Integer responsibleTeamId;
	
	@ApiParam(value="整改要求",required=false)
	String requirement;

}
