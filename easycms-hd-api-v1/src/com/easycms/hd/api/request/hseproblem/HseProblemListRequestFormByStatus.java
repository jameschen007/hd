package com.easycms.hd.api.request.hseproblem;

import java.io.Serializable;

import com.easycms.hd.api.enums.hseproblem.HseProbelmTypeEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemSolveStatusEnum;
import com.easycms.hd.api.enums.hseproblem.HseProblemStatusEnum;
import com.easycms.hd.api.request.base.BasePagenationRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class HseProblemListRequestFormByStatus extends BasePagenationRequestForm implements Serializable {
	private static final long serialVersionUID = 608356712874057613L;
	
	@ApiParam(value = "问题进度状态", required = false)
	private HseProblemStatusEnum problemStatus;
	@ApiParam(value = "问题处理情况,pre:待处;done:已处理", required = false)
	private HseProblemSolveStatusEnum problemSolveStatus;
	@ApiParam(value = "问题类型,all:所有已创建的问题;mine：登录用户创建的问题", required = false)
	private HseProbelmTypeEnum probelmType;

}
