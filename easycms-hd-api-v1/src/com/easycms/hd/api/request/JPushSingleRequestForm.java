package com.easycms.hd.api.request;

import java.io.Serializable;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class JPushSingleRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = -4726664214972235846L;
	@ApiParam(value = "用户登录的ID号", required = false)
	Integer userId;
	@ApiParam(value = "推送的标题", required = false)
	String title;
	@ApiParam(value = "推送的消息内容", required = true)
	String message;
}
