package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.Set;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WitnessSubmitForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 8270628938150008073L;
	@ApiParam(value = "需要提交的见证信息ID号", required = true)
	Set<Integer> ids;
	@ApiParam(value = "见证组组长ID号, 即QC1或者QC2组长的ID号.", required = true)
	Integer teamId;
}
