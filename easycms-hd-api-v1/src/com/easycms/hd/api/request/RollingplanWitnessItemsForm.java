package com.easycms.hd.api.request;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class RollingplanWitnessItemsForm implements Serializable {
	private static final long serialVersionUID = 1366214780857809168L;
	@ApiParam(value = "滚动计划 ID号s", required = true)
	private Integer[] ids;
	@ApiParam(value = "见证地址", required = true)
	private String witnessaddress;
	@ApiParam(value = "见证日期", required = true)
	private String witnessdate;
	
	public RollingplanWitnessItemsForm() {}
}
