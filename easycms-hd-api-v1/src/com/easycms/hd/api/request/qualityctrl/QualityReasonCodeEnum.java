package com.easycms.hd.api.request.qualityctrl;

public enum QualityReasonCodeEnum {
	
	/**
	 * 成品保护
	 */
	PRODUCTP_ROTECTION(1,"成品保护"),
	/**
	 * 清洁度
	 */
	CLEANLINESS(2,"清洁度"),
	/**
	 * 标识
	 */
	SIGN(3,"标识"),
	/**
	 * 防异物
	 */
	ANTI_FOREIGN_BODY(4,"防异物"),
	/**
	 * 预制
	 */
	PRECUT(6,"预制"),
	
	/**
	 * 安装
	 */
	INSTALL(5,"安装");
	
	private int index;  
    private String name;  
    
    private QualityReasonCodeEnum(int index,String name) {
    	this.index = index;
        this.name = name;
    }
    
    public static String getName(int index) {
        for (QualityReasonCodeEnum c : QualityReasonCodeEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;  
            }
        }
        return null;
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}

}
