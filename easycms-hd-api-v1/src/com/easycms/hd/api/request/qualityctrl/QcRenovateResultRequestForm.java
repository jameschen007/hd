package com.easycms.hd.api.request.qualityctrl;

import java.io.Serializable;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.hd.api.request.base.BaseRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class QcRenovateResultRequestForm extends BaseRequestForm implements Serializable{
	private static final long serialVersionUID = 4269954336002214887L;

	@ApiParam(required = true, name = "qcProblrmId", value = "质量管理问题ID")
	private Integer qcProblrmId;
	@ApiParam(required = true, name = "renovateDescription", value = "整改描述")
	private String renovateDescription;
	
}
