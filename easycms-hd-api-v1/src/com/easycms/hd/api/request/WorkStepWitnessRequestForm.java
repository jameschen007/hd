package com.easycms.hd.api.request;

import java.io.Serializable;
import java.util.List;

import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;

@Data
public class WorkStepWitnessRequestForm implements Serializable {
	private static final long serialVersionUID = 6687792075930773234L;
	
	@ApiParam(value = "工序上批量发起见证时的见证信息", required = false)
	List<WorkStepWitnessItemsForm> workStepItem;
	
/**	
  	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private Integer qcsign;
	@ApiParam(value = "如果是在工序的最后一步，此为必填项-将写入滚动计划中", required = false)
	private String qcman;
	**/
}
