package com.easycms.hd.api.request.underlying;

import com.easycms.hd.api.request.WorkStepWitnessItemsForm;
import com.easycms.hd.plan.domain.RollingPlan;

import java.util.List;
import java.util.Set;

/**
 * @author wangzhi
 * @Description: 批量见证时，其中的一个滚动计划的原始见证数据
 * @date 2019/7/5 19:01
 */
public class WitingPlan {
    private Integer planId;
    private RollingPlan plan;
    private Set<WorkStepWitnessItemsForm> step;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Set<WorkStepWitnessItemsForm> getStep() {
        return step;
    }

    public void setStep(Set<WorkStepWitnessItemsForm> step) {
        this.step = step;
    }

    public RollingPlan getPlan() {
        return plan;
    }

    public void setPlan(RollingPlan plan) {
        this.plan = plan;
    }
}
