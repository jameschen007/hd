package com.easycms.hd.api.request.question;

import java.io.Serializable;

import com.easycms.hd.api.enums.QuestionTypeEnum;
import com.easycms.hd.api.request.LoginIdRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>     <br>
 * <b>创建时间：</b>2017-09-15 下午05:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class QuestionRequestForm extends LoginIdRequestForm implements Serializable{

	private static final long serialVersionUID = -6892963500085316925L;
	/**
	 * 问题创建需要的参数：
	 * 作业条目编号
	 * 问题描述
	 * 问题类型
	 * 附件
	 */
	@ApiParam(value="问题创建操作的rollingplan ID",required=true)
	Integer rollingPlanId;
	/**
	 * 问题类型
	 */
	@ApiParam(value="问题类型",required=true)
	QuestionTypeEnum questionType;
	
	@ApiParam(value="问题描述",required=true)
	String describe;
//	
//	@ApiParam(value = "问题状态", required = true)
//	QuestionStatusEnum_ZZ questionStatus;
}
