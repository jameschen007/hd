package com.easycms.hd.api.request.question;

import java.io.Serializable;

import com.easycms.hd.api.enums.QuestionTypeEnum;
import com.easycms.hd.api.enums.question.QuestionStatusEnum_ZZ;
import com.easycms.hd.api.request.LoginIdRequestForm;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * <b>创建人：</b>王志<br>
 * <b>类描述：</b>     <br>
 * <b>创建时间：</b>2017-09-15 下午05:06:20<br>
 * <b>@Copyright:</b>2017-爱特联科技
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class QuestionFeedbackRequestForm extends LoginIdRequestForm implements Serializable{
	private static final long serialVersionUID = -3843225729688347406L;

	@ApiParam(value="问题id",required=true)
	Integer questionId;

	@ApiParam(value="问题反馈",required=true)
	 String describe;
	
}
