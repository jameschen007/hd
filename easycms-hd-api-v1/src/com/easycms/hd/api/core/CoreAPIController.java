package com.easycms.hd.api.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.ConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.ValidateUtility;
import com.easycms.core.form.FormHelper;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.domain.AuthModule;
import com.easycms.hd.api.domain.AuthRole;
import com.easycms.hd.api.enpower.domain.EnpowerUser;
import com.easycms.hd.api.enpower.service.EnpowerUserService;
import com.easycms.hd.api.request.LoginRequestForm;
import com.easycms.hd.api.request.ResetPasswordRequestForm;
import com.easycms.hd.api.request.UserRequestForm;
import com.easycms.hd.api.response.AuthorizationResult;
import com.easycms.hd.api.response.BannerResult;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.response.ModulesResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.service.DepartmentApiService;
import com.easycms.hd.api.service.RoleApiService;
import com.easycms.hd.mobile.domain.Banner;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.BannerService;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.domain.Department;
import com.easycms.management.user.domain.Role;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.domain.UserDevice;
import com.easycms.management.user.form.validator.AppPersonalPwdFormValidator;
import com.easycms.management.user.service.RoleService;
import com.easycms.management.user.service.UserDeviceService;
import com.easycms.management.user.service.UserService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hdxt/api")
@Api(value = "CoreAPIController", description = "登录与主页相关api")
@Slf4j
@Transactional
public class CoreAPIController {
	private static Logger logger = Logger.getLogger(CoreAPIController.class);

	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserDeviceService userDeviceService;
	@Autowired
	private BannerService bannerService;
	@Autowired
	private ModulesService modulesService;
	@Autowired
	private RoleApiService roleApiService;
	@Autowired
	private DepartmentApiService departmentApiService;
	@Autowired
	private EnpowerUserService enpowerUserService;

	@Value("#{APP_SETTING['banner_path']}")
	private String bannerPath;

	@Value("#{APP_SETTING['banner_url_prefix']}")
	private String bannerURLPrefix;
	// 五公司内网ip port
	@Value("#{APP_SETTING['banner_url_iport_in']}")
	private String banner_url_iport_in;
	// 五公司外网ip port
	@Value("#{APP_SETTING['banner_url_iport_out']}")
	private String banner_url_iport_out;

	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;

	@Value("#{APP_SETTING['windows_file_base_path']}")
	private String winBasePath;

	@Value("#{APP_SETTING['file_witness_path']}")
	private String fileWitnessPath;

	@Value("#{APP_SETTING['file_problem_path']}")
	private String fileProblemPath;

	@Value("#{APP_SETTING['file_conference_path']}")
	private String fileConferencePath;

	@Value("#{APP_SETTING['file_notification_path']}")
	private String fileNotificationPath;

	/**
	 * 认证用户
	 * 
	 * @param request
	 * @param response
	 * @param loginForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	@ApiOperation(value = "Mobile端登录", notes = "用于移动端登录。")
	public JsonResult<AuthorizationResult> authenticate(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("requestForm") @Valid LoginRequestForm requestForm) {
		JsonResult<AuthorizationResult> result = new JsonResult<AuthorizationResult>();
		AuthorizationResult auth = new AuthorizationResult();
		String username = requestForm.getUsername();
		String password = requestForm.getPassword();
		String uuid = requestForm.getUuid();
		log.info("==> username ：" + username);
		log.info("==> password ：" + password);
		log.info("==> device ：" + uuid);

		logger.debug("qc1ReplaceQc2标示使用数据库中的配置!" + ContextPath.qc1ReplaceQc2);
		// LOGIN ID非空判断
		if (!CommonUtility.isNonEmpty(username)) {
			result.setCode("-1001");
			result.setMessage("用户名不能为空");
			return result;
		}

		// PASSWORD 非空判断
		if (!CommonUtility.isNonEmpty(password)) {
			result.setCode("-1001");
			result.setMessage("密码不能为空");
			return result;
		}

		// LOGIN ID有效判断
		User user = userService.findUser(username);
		if (user == null) {
			user = userService.findByNameIsDel1(username);
			if (user != null) {

				result.setCode("-1001");
				result.setMessage("用户" + username + "被冻结");
				return result;
			} else {

				result.setCode("-1001");
				result.setMessage("用户" + username + "不存在");
				return result;
			}
		}
		if (ContextPath.WZ) {
			logger.debug("王志本机测试，不验证密码！！！");
		} else {

			// 登录认证
			user = userService.userLogin(username, password, false);
		}

		if (user == null) {
			result.setCode("-1002");
			result.setMessage("用户名或密码不正确！");
			return result;
		}

		if (CommonUtility.isNonEmpty(uuid)) {
			if (null == userDeviceService.getDeviceByDeviceIdAndUserId(user.getId(), uuid)) {
				UserDevice userDevice = new UserDevice();
				userDevice.setUser(user);
				userDevice.setDeviceid(uuid);
				userDevice.setCreatedOn(new Date());
				userDevice.setCreatedBy("Core_API");
				userDeviceService.add(userDevice);
			}
		}
		auth.setId(user.getId());
		auth.setRealname(user.getRealname());
		auth.setUsername(user.getName());

		if (user.getRoles() != null && user.getRoles().size() > 0) {
			Set<AuthRole> roles = roleApiService.getAuthRole(user, false);// rollingPlan等需要验证角色时，设置false
			auth.setRoles(roles);
		}

		// 设置模块属性
		List<Modules> modules = user.getModules();
		if (modules != null && modules.size() > 0) {
			Set<AuthModule> m = roleApiService.getAuthModule(user);
			auth.setModules(m);
		}

		if (null != user.getDepartment()) {
			auth.setDepartment(departmentApiService.departmentTransferToDepartmentResult(user.getDepartment()));
		}
		auth.setQc1ReplaceQc2(ContextPath.qc1ReplaceQc2);

		result.setResponseResult(auth);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/getUserId", method = RequestMethod.POST)
	@ApiOperation(value = "Mobile端获取用户id", notes = "用于移动端获取指定用户的ID用户信息。")
	public JsonResult<UserResult> getUserId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "username", value = "username") @RequestParam(value = "username", required = true) String username,
			@ApiParam(required = false, name = "uuid", value = "uuid") @RequestParam(value = "uuid", required = false) String uuid,
			@ApiParam(required = false, name = "androidOrIOS", value = "androidOrIOS") @RequestParam(value = "androidOrIOS", required = false) String androidOrIOS) {

		JsonResult<UserResult> result = new JsonResult<UserResult>();
		UserResult auth = new UserResult();
		// LOGIN username非空判断
		if (null == username || "".equals(username.trim())) {
			result.setCode("-1001");
			result.setMessage("用户ID不能为空");
			return result;
		}

		// 获取用户信息
		User user = userService.findUser(username.trim());

		if (user == null) {
			result.setCode("-1002");
			result.setMessage("指定用户ID[" + username + "]的用户不存在。");
			return result;
		}

		if (CommonUtility.isNonEmpty(androidOrIOS) && CommonUtility.isNonEmpty(uuid)) {
			List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(user.getId());
			if (deviceList == null || deviceList.size() == 0) {
				UserDevice userDevice = new UserDevice();
				userDevice.setUser(user);
				userDevice.setDeviceid(uuid);
				// userDevice.setAndroidOrIOS("android");
				userDevice.setCreatedOn(new Date());
				userDevice.setCreatedBy("Core_API");
				userDeviceService.add(userDevice);
			} else {
				if (deviceList.size() == 1) {
					UserDevice userDevice = deviceList.get(0);
					userDevice.setDeviceid(uuid);
					// userDevice.setAndroidOrIOS(androidOrIOS);
					userDevice.setUpdatedOn(Calendar.getInstance().getTime());
					userDeviceService.add(userDevice);
				} else if (deviceList.size() > 1) {
					// 首记录更新使用，其它记录删除掉
					UserDevice userDevice = deviceList.get(0);
					deviceList.remove(0);
					userDevice.setDeviceid(uuid);
					// userDevice.setAndroidOrIOS(androidOrIOS);
					userDevice.setUpdatedOn(Calendar.getInstance().getTime());
					userDeviceService.add(userDevice);

					deviceList.stream().forEach(d -> userDeviceService.delete(d.getId()));
				}
			}
		}

		// if (user == null) {
		// result.setCode("-1003");
		// result.setMessage("提定用户ID[" + username + "]的用户不存在。");
		// return result;
		// }

		auth.setId(user.getId());
		auth.setRealname(user.getRealname());
		auth.setUsername(user.getName());

		if (user.getRoles() != null && user.getRoles().size() > 0) {
			Set<AuthRole> roles = roleApiService.getAuthRole(user, false);
			auth.setRoles(roles);
		}

		if (null != user.getDepartment()) {
			auth.setDepartment(departmentApiService.departmentTransferToDepartmentResult(user.getDepartment()));
		}

		result.setResponseResult(auth);
		return result;

	}

	/**
	 * 根据账号获取 所在项目
	 * 
	 * @param request
	 * @param response
	 * @param loginForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/mineProject", method = RequestMethod.POST)
	@ApiOperation(value = "根据账号获取　所在项目", notes = "根据账号获取　所在项目")
	public JsonResult<Map<String, String>> mineProject(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "username", value = "username") @RequestParam(value = "username", required = true) String username) {
		JsonResult<Map<String, String>> result = new JsonResult<Map<String, String>>();
		Map<String, String> ret = new HashMap<String, String>();

		// LOGIN ID非空判断
		if (!CommonUtility.isNonEmpty(username)) {
			result.setCode("-1001");
			result.setMessage("用户名不能为空");
			return result;
		}

		// LOGIN ID有效判断
		User user = userService.findUser(username);
		if (user == null) {
			user = userService.findByNameIsDel1(username);
			if (user != null) {

				result.setCode("-1001");
				result.setMessage("用户" + username + "被冻结");
				return result;
			} else {

				result.setCode("-1001");
				result.setMessage("用户" + username + "不存在");
				return result;
			}
		}

		ret.put("id", user.getId() + "");
		ret.put("realname", user.getRealname());
		ret.put("name", user.getName());

		logger.info("所在项目:" + user.getMineProject());

		ret.put(ConstantVar.PROJ_CODE, user.getMineProject());

		ret.put(ConstantVar.appWwUrl, SysConstant.appWwUrlMap.get(user.getMineProject()));
		ret.put(ConstantVar.projectName, SysConstant.projectNameMap.get(user.getMineProject()));

		result.setResponseResult(ret);

		return result;
	}

	/**
	 * 保存修改密码
	 * 
	 * @param request
	 * @param response
	 * @param personalForm
	 * @param errors
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	@ApiOperation(value = "Mobile端resetPwd", notes = "用于移动端resetPwd。")
	public JsonResult<AuthorizationResult> resetPwd(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") ResetPasswordRequestForm form,
			// @ModelAttribute("personalPasswordForm") @Valid PersonalForm
			// personalFormc,
			BindingResult errors) {
		JsonResult<AuthorizationResult> result = new JsonResult<AuthorizationResult>();
		logger.debug("==> Start change password.");
		logger.debug(CommonUtility.toJson(form));

		// 校验表单
		AppPersonalPwdFormValidator validator = new AppPersonalPwdFormValidator();
		validator.validate(form, errors);
		if (errors.hasErrors()) {
			FormHelper.printErros(errors, logger);
			HttpUtility.writeToClient(response, "false");
			logger.debug("==> Change password failed.");
			logger.debug("==> End change password.");
			result.setCode(ExceptionCode.E1);
			result.setMessage("修改失败！");
			return result;
		}

		// 校验通过,更新用户密码
		// 密码长度最长16位
		String newp = form.getNewPassword();
		if (newp == null || "".equals(newp)) {
			result.setCode(ExceptionCode.E1);
			result.setMessage("修改失败！");
			return result;
		}
		if (newp.length() < 8 || newp.length() > 16) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码长度需8-16,修改失败！");
			return result;
		}

		List<String> ret = ValidateUtility.matchePattern(newp, SysConstant.bigPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含大写字母,修改失败！");
			return result;
		}
		ret = ValidateUtility.matchePattern(newp, SysConstant.smallPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含小写字母,修改失败！");
			return result;
		}
		ret = ValidateUtility.matchePattern(newp, SysConstant.digitPattern);
		if (ret.size() == 0) {
			result.setCode(ExceptionCode.E2);
			result.setMessage("密码不包含数字,修改失败！");
			return result;
		}

		try {
			boolean resultb = userService.updatePassword(form.getLoginId(), form.getPassword(), form.getNewPassword());
			if (resultb) {
				logger.debug("==> Change password success.");
				result.setMessage("修改成功！");
				return result;
			} else {
				logger.debug("==> Change password failed.");
				result.setCode(ExceptionCode.E3);
				result.setMessage("修改失败！");
				return result;
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:" + e.getMessage());
			return result;
		}

	}

	@ResponseBody
	@RequestMapping(value = "/banner", method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端Banner列表", notes = "在移动端登录之后主界面最上方的banner列表")
	public JsonResult<List<BannerResult>> getBanner(HttpServletRequest request, HttpServletResponse response) {
		JsonResult<List<BannerResult>> result = new JsonResult<List<BannerResult>>();

		String ipFromHead = HttpUtility.getIpFromHead(request);
		logger.info("ipFromHead=" + ipFromHead);

		if (ipFromHead != null && ipFromHead.indexOf(SysConstant.hd39Prefix) != -1) {// 测试环境
			// 目前过度阶段常用
			bannerURLPrefix = banner_url_iport_out + "/banner";
		} else if (SysConstant.ipFromHead0.equals(ipFromHead)) {// 本机测试
			bannerURLPrefix = banner_url_iport_in + "/banner";
		} else if (ipFromHead != null && ipFromHead.indexOf(SysConstant.hd5InPrefix) != -1) {// 内网的
			bannerURLPrefix = banner_url_iport_in + "/banner";
		} else {
			// 外网的不准确，实际 ipFromHead =125.70.176.140
			bannerURLPrefix = banner_url_iport_out + "/banner";
		}

		logger.info("bannerURLPrefix=" + bannerURLPrefix);

		String selfbannerURLPrefix = "/hdxt/api/download/banner";

		List<BannerResult> bannerResults = new ArrayList<BannerResult>();

		List<Banner> banners = bannerService.findByStatus("ACTIVE");

		bannerResults = banners.stream().map(banner -> {
			BannerResult bannerResult = new BannerResult();
			bannerResult.setAlt(banner.getAlt());
			bannerResult.setDescription(banner.getDescription());
			bannerResult.setId(banner.getId());
			bannerResult.setOrderNumber(banner.getOrderNumber());
			bannerResult.setImage(bannerURLPrefix + banner.getUrl());
			bannerResult.setSelfImage(selfbannerURLPrefix + banner.getUrl());
			return bannerResult;
		}).collect(Collectors.toList());

		result.setResponseResult(bannerResults);
		return result;

	}

	/**
	 * 登出，清除clientid
	 * 
	 * @param request
	 * @param response
	 * @param username
	 * @param uuid
	 * @param androidOrIOS
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ApiOperation(value = "Mobile端获取用户id", notes = "用于移动端获取指定用户的ID用户信息。")
	public JsonResult<UserResult> logout(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(required = true, name = "username", value = "username") @RequestParam(value = "username", required = true) String username) {

		JsonResult<UserResult> result = new JsonResult<UserResult>();
		UserResult auth = new UserResult();
		if (null == username || "".equals(username.trim())) {
			result.setCode("-1001");
			result.setMessage("用户ID不能为空");
			return result;
		}

		// 获取用户信息
		User user = userService.findUser(username.trim());

		if (user == null) {
			result.setCode("-1002");
			result.setMessage("指定用户ID[" + username + "]的用户不存在。");
			return result;
		}

		List<UserDevice> deviceList = userDeviceService.getDeviceByUserId(user.getId());
		if (deviceList == null || deviceList.size() == 0) {

		} else {
			if (deviceList.size() == 1) {
				UserDevice userDevice = deviceList.get(0);
				userDevice.setDeviceid("");
				// userDevice.setAndroidOrIOS("");
				userDevice.setUpdatedOn(Calendar.getInstance().getTime());
				userDeviceService.add(userDevice);
			} else if (deviceList.size() > 1) {
				// 首记录更新使用，其它记录删除掉
				UserDevice userDevice = deviceList.get(0);
				deviceList.remove(0);
				userDevice.setDeviceid("");
				// userDevice.setAndroidOrIOS("");
				userDevice.setUpdatedOn(Calendar.getInstance().getTime());
				userDeviceService.add(userDevice);

				deviceList.stream().forEach(d -> userDeviceService.delete(d.getId()));
			}
		}

		auth.setId(user.getId());
		auth.setRealname(user.getRealname());
		auth.setUsername(user.getName());

		if (user.getRoles() != null && user.getRoles().size() > 0) {
			Set<AuthRole> roles = roleApiService.getAuthRole(user, false);
			auth.setRoles(roles);
		}

		if (null != user.getDepartment()) {
			auth.setDepartment(departmentApiService.departmentTransferToDepartmentResult(user.getDepartment()));
		}

		result.setResponseResult(auth);
		return result;

	}

	@ResponseBody
	@RequestMapping(value = "/module", method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端module列表", notes = "在移动端登录之后主界面中间module列表")
	public JsonResult<List<ModulesResult>> getMenu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "loginId", required = true) @ApiParam(value = "用户登录之后的用户ID", required = true) Long loginId) {
		JsonResult<List<ModulesResult>> result = new JsonResult<List<ModulesResult>>();

		List<ModulesResult> modulesResults = new ArrayList<ModulesResult>();

		List<Modules> modules = modulesService.findByStatus("ACTIVE");

		modulesResults = modules.stream().map(module -> {
			ModulesResult modulesResult = new ModulesResult();
			modulesResult.setName(module.getName());
			modulesResult.setId(module.getId());
			modulesResult.setOrderNumber(module.getOrderNumber());
			modulesResult.setAlert(new Random().nextInt(50) % 2 == 0 ? true : false);
			modulesResult.setType(module.getType());
			return modulesResult;
		}).collect(Collectors.toList());

		result.setResponseResult(modulesResults);
		return result;

	}

	@RequestMapping(value = { "/files/witness/{filePath:.+}" }, method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到witness文件", notes = "将uri跟在id上则可得到相应流文件")
	public void getWitnessFile(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filePath") @ApiParam(value = "用户登录之后的用户ID", required = true) String filePath)
			throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {

			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}

			in = new FileInputStream(basePath + fileWitnessPath + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = { "/files/conference/{filePath:.+}" }, method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到conference文件", notes = "将uri跟在id上则可得到相应流文件")
	public void getConferenceFile(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filePath") @ApiParam(value = "用户登录之后的用户ID", required = true) String filePath)
			throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {

			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}

			in = new FileInputStream(basePath + fileConferencePath + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = { "/download/{apk}/{filePath:.+}" }, method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到conference文件", notes = "将uri跟在id上则可得到相应流文件")
	public void download(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filePath") @ApiParam(value = "用户登录之后的用户ID", required = true) String filePath,
			@PathVariable("apk") @ApiParam(value = "用户登录之后的用户ID", required = true) String apk) throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {

			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}
			if (StringUtils.isNotBlank(apk)) {
				filePath = apk + "/" + filePath;
			}
			in = new FileInputStream(basePath + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = { "/files/notification/{filePath:.+}" }, method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到notification文件", notes = "将uri跟在id上则可得到相应流文件")
	public void getNotificationFile(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filePath") @ApiParam(value = "用户登录之后的用户ID", required = true) String filePath)
			throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {

			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}

			in = new FileInputStream(basePath + fileNotificationPath + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = "/files/problem/{filePath:.+}", method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到problem文件", notes = "将uri跟在id上则可得到相应流文件")
	public void getProblemFile(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("filePath") @ApiParam(value = "文件名", required = true) String filePath) throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {
			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}
			in = new FileInputStream(basePath + fileProblemPath + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value = "/files/problem/{today}/{filePath:.+}", method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端得到problem文件", notes = "将uri跟在id上则可得到相应流文件")
	public void getProblemFiletoday(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("today") @ApiParam(value = "today", required = true) String today,
			@PathVariable("filePath") @ApiParam(value = "文件名", required = true) String filePath) throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");

		InputStream in = null;
		OutputStream out = null;
		try {
			if (false == com.easycms.common.logic.context.ContextPath.Linux
					.equals(com.easycms.common.logic.context.ContextPath.os_name)) {
				basePath = winBasePath;
			}
			in = new FileInputStream(basePath + fileProblemPath + today + "/" + filePath); // 获取文件的流
			int len = 0;
			byte buf[] = new byte[1024];// 缓存作用
			out = response.getOutputStream();// 输出流
			while ((len = in.read(buf)) > 0) {// 切忌这后面不能加 分号 ”;“
				out.write(buf, 0, len);// 向客户端输出，实际是把数据存放在response中，然后web服务器再去response中读取
			}
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@ResponseBody
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	@ApiOperation(value = "Mobile端获取用户信息", notes = "用于移动端获取指定用户ID的用户信息。")
	public JsonResult<UserResult> getUsersByDepartmentId(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") UserRequestForm form) {
		JsonResult<UserResult> result = new JsonResult<UserResult>();
		UserResult auth = new UserResult();
		// LOGIN ID非空判断
		if (null == form.getUserId()) {
			result.setCode("-1001");
			result.setMessage("用户ID不能为空");
			return result;
		}

		// 获取用户信息
		User user = userService.findUserById(form.getUserId());

		if (user == null) {
			result.setCode("-1002");
			result.setMessage("提定用户ID[" + form.getUserId() + "]的用户不存在。");
			return result;
		}

		auth.setId(user.getId());
		auth.setRealname(user.getRealname());
		auth.setUsername(user.getName());

		if (user.getRoles() != null && user.getRoles().size() > 0) {
			Set<AuthRole> roles = roleApiService.getAuthRole(user, false);// rollingPlan等需要验证角色时，设置false
			auth.setRoles(roles);
		}

		result.setResponseResult(auth);
		return result;

	}

	@ResponseBody
	@RequestMapping(value = "/enpowerUserProcess", method = RequestMethod.GET)
	@ApiOperation(value = "用于内部接口处理上下级关系", notes = "用于内部接口处理上下级关系，仅内部调用，外部调用将报错")
	public JsonResult<UserResult> enpowerUserProcess(HttpServletRequest request, HttpServletResponse response) {
		JsonResult<UserResult> result = new JsonResult<UserResult>();

		// String referer = request.getHeader("Referer");
		// if (referer == null || !referer.contains(request.getServerName())) {
		// result.setCode("-1002");
		// result.setMessage("不符合的请求。");
		// return result;
		// }

		String validataCode = request.getParameter("code");
		if (!CommonUtility.isNonEmpty(validataCode) || !validataCode.equals("e00d0104-ac91-45b1-9ac2-21ea1cc9e4fe")) {
			result.setCode("-1002");
			result.setMessage("不符合的请求。");
			return result;
		}

		List<User> users = userService.findAll();
		for (User user : users) {
			List<Role> userRolesList = user.getRoles();

			if (null != userRolesList && !userRolesList.isEmpty()) {
				Set<Role> userRoles = new HashSet<>(userRolesList);

				for (Role role : userRoles) {
					log.error("Refiny:" + role.getName() + "(" + role.getId() + " : " + role.getEnpowerId() + ")"
							+ " : " + ((role.getUsers() == null || role.getUsers().isEmpty()) ? 0 + ""
									: role.getUsers().size() + ""));
				}

				/**
				 * 根据用户的角色，反查用户的上级人员
				 */
				Role parentRole = userRolesList.get(0).getParent();
				if (null != parentRole) {
					Role role = roleService.findById(parentRole.getId());
					List<User> parentRoleUsers = role.getUsers();
					if (null != parentRoleUsers && !parentRoleUsers.isEmpty()) {
						user.setParent(parentRoleUsers.get(0));
						userService.update(user);
					}
				} else {
					Department department = user.getDepartment();
					if (null != department) {
						Department parentDepartment = department.getParent();
						if (null != parentDepartment) {
							Role topRole = parentDepartment.getTopRole();
							if (null != topRole) {
								List<User> parentRoleUsers = topRole.getUsers();
								if (null != parentRoleUsers && !parentRoleUsers.isEmpty()) {
									user.setParent(parentRoleUsers.get(0));
									userService.update(user);
								}
							}
						}
					}
				}
			}
		}
		return result;

	}

	@ResponseBody
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ApiOperation(value = "测试类", notes = "仅测试报错信息")
	public JsonResult<Boolean> test(HttpServletRequest request, HttpServletResponse response) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();

		EnpowerUser enpowerUser = new EnpowerUser();
		enpowerUser.setCreatedate(new Date());
		enpowerUser.setData("测试数据，来自于HD Test 接口。");
		result.setResponseResult(enpowerUserService.insert(enpowerUser));

		int i = 5 / 0;
		log.info(i + "");

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/menubuild", method = RequestMethod.GET)
	@ApiOperation(value = "菜单重建", notes = "用于重建系统中各角色的菜单")
	public JsonResult<Boolean> menubuild(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "code", required = true) @ApiParam(value = "校验码", required = true) String code) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();

		if (!CommonUtility.isNonEmpty(code) || !code.equals("1dd6333eda2d40c6ad1f1a86c11bb2ce")) {
			result.setCode("-1002");
			result.setMessage("不符合的请求。");
			return result;
		}

		enpowerUserService.buildEnpowerRoleMenu();

		return result;
	}

	public static void main(String[] args) {

		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -2);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.S");

		System.out.println(sdf.format(c.getTime()));

		List<String> kList = new ArrayList<String>();
		List<String> vList = new ArrayList<String>();

		kList.add("a44bb8DD");
		vList.add("[A-Z]+");

		kList.add("a44bb8DD");
		vList.add("[a-z]+");

		kList.add("a44bb8DD");
		vList.add("\\d+");

		System.out.println("------------------------------------------------");
		for (int i = 0; i < kList.size(); i++) {
			System.out.println(kList.get(i));
			List<String> ret = ValidateUtility.matchePattern(kList.get(i), vList.get(i));
			System.out.println(kList.get(i) + " " + vList.get(i) + " size:" + ret.size() + " " + ret);
			System.out.println("------------------------------------------------");
		}
	}
}
