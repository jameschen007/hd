package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 用于会议和通知的统计
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class ConferenceStatisticsResult implements Serializable{
	private static final long serialVersionUID = -3137797022089736611L;
	private Long send = 0L;
	private Long sendUnread = 0L;
	private Long receiveUnread = 0L;
	private Long receive = 0L;
	private Long sendUnStarted = 0L;
	private Long receiveUnStarted = 0L;
	private Long sendProcessing = 0L;
	private Long receiveProcessing = 0L;
	private Long sendExpired = 0L;
	private Long receiveExpired = 0L;
	private Long draft = 0L;
	private String mark;
}
