package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 用于物料日志的统计
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class MaterialStatisticsResult implements Serializable{
	private static final long serialVersionUID = -3352092908794063586L;
	private Long input = 0L;
	private Long output = 0L;
}
