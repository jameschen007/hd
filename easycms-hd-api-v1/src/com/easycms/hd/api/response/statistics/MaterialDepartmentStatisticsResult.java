package com.easycms.hd.api.response.statistics;

import java.io.Serializable;

import com.easycms.hd.api.response.DepartmentResult;

import lombok.Data;
/**
 * 用于物料日志部门出库的统计
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class MaterialDepartmentStatisticsResult implements Serializable{
	private static final long serialVersionUID = -4813915624542703496L;
	private DepartmentResult department;
	private Long output = 0L;
}
