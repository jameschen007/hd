package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import lombok.Data;
/**
 * 每种不同角色下的统计信息
 * 班长和队长有这一项
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class ProblemStatisticsResult implements Serializable{
	private static final long serialVersionUID = -5313072274633196255L;
	private Integer total;
	private Integer pre;//待解决
	private Integer done;//已处理待确认
	private Integer solved;//已解决
	private Integer unsolved;//未解决
	private Integer needAssign;//需要指派
	private String mark;
}
