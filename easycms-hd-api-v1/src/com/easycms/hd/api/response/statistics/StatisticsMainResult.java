package com.easycms.hd.api.response.statistics;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Rolling plan返回的主类
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class StatisticsMainResult<T> implements Serializable{
	private static final long serialVersionUID = -8580262895350954259L;
	private List<StatisticsReportResult<T>> captain;
	private List<StatisticsReportResult<T>> monitor;
	private List<StatisticsReportResult<T>> witness_team;
	private List<StatisticsReportResult<T>> witness_qc2;
	private List<ExtraResult> extra;
	private List<StatisticsReportResult<T>> supervisor;
}
