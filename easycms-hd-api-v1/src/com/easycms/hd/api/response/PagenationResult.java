package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class PagenationResult {
	private Integer pageSize = 10;
	private Integer totalCounts = 0;
	private Integer pageCounts = 0; 
	private Integer pageNum = 1;
}
