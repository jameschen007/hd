package com.easycms.hd.api.response.rollingplan;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import com.easycms.hd.api.response.RollingMaterialResult;
import com.easycms.hd.api.response.UserResult;
import com.easycms.hd.api.response.WitnessResult;

import lombok.Data;

/**
 * 最终的Rolling plan数据
 * @author ChenYuMei-Refiny
 *
 */
@Data
public class RollingPlanDataResult implements Serializable{
	private static final long serialVersionUID = 3219756048063025845L;
//---------------------------------[首页列表展示所需要字段]--------------------------------
	/**
	 * 序号
	 */
	private Integer id;
	/**
	 * 作业条目编号
	 */
	private String workListNo;
	/**
	 * 质量计划号
	 */
	private String qualityplanno;
	/**
	 * 工程量编号
	 */
	private String projectNo;
	/**
	 * 计划施工日期Start
	 */
	private Date planStartDate;
	/**
	 * 计划施工日期End
	 */
	private Date planEndDate;
	/**
	 * 班长分派时指定的施工日期
	 */
	private Date planBeginProgressDate;
	/**
	 * 焊口支架号
	 */
	private String weldno;
	/**
	 * 工程量类型
	 */
	private String projectType;
	/**
	 * 图纸号版本
	 */
	private String drawingVersion;
	
//---------------------------------[单个详细信息展示所需要字段及额外字段]--------------------------------	
	/**
	 * 房间号
	 */
	private String roomNo;
	/**
	 * 系统代号
	 */
	private String systemNo;
	/**
	 * 机组号
	 */
	private String unitNo;
	/**
	 * 图纸号
	 */
	private String drawingNo;
	/**
	 * 作业包名称
	 */
	private String workPackageNo;
	/**
	 * 管线号
	 */
	private String lineNo;
	/**
	 * 工程 量
	 */
	private String projectCost;
	/**
	 * 工程 量
	 */
	private String projectCost2;
	/**
	 * 规格
	 */
	private String speification;
	/**
	 * 材制
	 */
	private String matelial;
	/**
	 * 核级
	 */
	private String croeLevel;
	/**
	 * 工程量单位
	 */
	private String projectUnit;
	/**
	 * 子项
	 */
	private String subItem;
	/**
	 * 点数
	 */
	private BigDecimal points;
	/**
	 * 点值
	 */
	private String spot;
	/**
	 * Itp编号
	 */
	private String itpNo;
	/**
	 * 施工班组
	 */
//	private String consTeam;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 类型
	 */
	private String type;
	/**
	 * 实际完成工程量
	 */
	private String dosage;
	
//--------------------------------[2017-11-01新增额外属性]-----------------------------------
	/**
	 * 设备名称
	 */
	private String deviceName;
	/**
	 * 设备编号
	 */
	private String deviceNo;
	/**
	 * 设备类型
	 */
	private String deviceType;
	/**
	 * 部件号/标识号
	 */
	private String markNo;
	/**
	 * 单元件总数
	 */
	private String unitTotalCount;
	/**
	 * 单元件未完成数
	 */
	private String unitUncompleteCount;
	/**
	 * 回路总数
	 */
	private String loopTotalCount;
	/**
	 * 回路未完成数
	 */
	private String loopUncompleteCount;
	/**
	 * 冲洗/试压名称
	 */
	private String rinseName;
	/**
	 * 冲洗/试压类型
	 */
	private String rinseType;
//---------------------------------[见证员所需的额外字段]--------------------------------
	/**
	 * 计划用量
	 */
	private String planAmount;
	/**
	 * 物项名称
	 */
	private String itemName;
	
	/**
	 * 物项编号
	 */
	private String itemNo;
	/**
	 * 工程量名称
	 */
	private String projectName;
	/**
	 * 通知单名称（ITP模板名称(中文)）２、itp名称

	 */
	private String itpName;
	
//---------------------------------[任务完成之后所需的额外字段]--------------------------------
	/**
	 * 是否存在未解决的问题
	 */
	private boolean problemFlag = false;	
	/**
	 * 是否需要回填
	 */
	private boolean backFill = false;
	/**
	 * 焊工号
	 */
	private UserResult welder;
	/**
	 * 焊接时间
	 */
	private Date welddate;
	/**
	 * 见证信息
	 */
	private List<WitnessResult> witnessInfo;
	
	/**
	 * 作业班长
	 */
	private UserResult consmonitor;
	
	/**
	 * 作业组长
	 */
	private UserResult consteam;
	
	/**
	 * 见证人信息
	 */
	private Set<UserResult> witnessers;
	
	/**
	 * 见证人信息
	 */
	private UserResult QC1Witnesser;
	
	/**
	 * 见证人信息
	 */
	private Date QC1WitnessDate;
	/**
	 * 材料信息
	 */
	private List<RollingMaterialResult> materialList;
}
