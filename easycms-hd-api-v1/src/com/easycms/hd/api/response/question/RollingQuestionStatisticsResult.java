package com.easycms.hd.api.response.question;

import java.io.Serializable;

import lombok.Data;

@Data
public class RollingQuestionStatisticsResult implements Serializable {
	private static final long serialVersionUID = -1683510944844743906L;

	/**
	 * 已反馈统计数据
	 */
	private Integer reply;
	
	/**
	 * 未解决统计数据
	 */
	private Integer unsolved;
	
	/**
	 * 已解决统计数据
	 */
	private Integer solved;

}
