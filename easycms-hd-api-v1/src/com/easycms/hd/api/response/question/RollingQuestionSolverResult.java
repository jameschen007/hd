package com.easycms.hd.api.response.question;

import java.io.Serializable;
import java.util.Date;

public class RollingQuestionSolverResult implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;

	private int id;
	/**
	 * '班长状态:pre待指派、done已指派，技术状态：pre待处理、done已处理、unsolved仍未解决、solved已解决',
	 */
	private String status;
	private String statusName;
	/**
	 * 子表_不通过理由-20171025添加
	 */
	private String reason ;
	private String realname ;

	private Date createTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}



}
