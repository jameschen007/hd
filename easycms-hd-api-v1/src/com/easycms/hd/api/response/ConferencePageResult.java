package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ConferencePageResult extends PagenationResult implements Serializable {
	private static final long serialVersionUID = -5041617839940329871L;
	private List<ConferenceResult> data;
}
