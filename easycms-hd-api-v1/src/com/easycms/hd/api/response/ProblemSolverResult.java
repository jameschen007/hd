package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class ProblemSolverResult {
	private String id;
	private String name;
	private String desc;
}
