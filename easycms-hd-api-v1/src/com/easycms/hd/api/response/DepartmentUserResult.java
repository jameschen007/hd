package com.easycms.hd.api.response;

import java.util.List;

import lombok.Data;

@Data
public class DepartmentUserResult {
	private Integer id;
	private String name;
	private List<UserResult> users;
	private List<DepartmentUserResult> children;
}
