package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Set;

import com.easycms.hd.api.domain.AuthModule;
import com.easycms.hd.api.domain.AuthRole;

import lombok.Data;

@Data
public class AuthorizationResult implements Serializable{
	private static final long serialVersionUID = -6496559982163840671L;
	private Integer id;
	private String username;
	private String realname;
	private Set<AuthRole> roles;
	private String device;
	private Set<AuthModule> modules;
	private DepartmentResult department;	
	public Boolean qc1ReplaceQc2=false ;
}
