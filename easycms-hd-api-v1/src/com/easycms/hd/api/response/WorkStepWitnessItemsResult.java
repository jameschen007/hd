package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class WorkStepWitnessItemsResult {
	private Integer id;
	private String code = "1000";
	private String message = "success";
}
