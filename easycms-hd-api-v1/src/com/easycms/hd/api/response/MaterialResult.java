package com.easycms.hd.api.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class MaterialResult implements Serializable {
	private static final long serialVersionUID = 7136267007264102616L;
	
	/**
	 * 序号
	 */
	private Integer no;
	/**
	 * 唯一ID
	 */
//	private Integer id;
	private String ID;
	/**
	 * MATE_CODE	物项编码
	 */
	private String MATE_CODE;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 仓库
	 */
	private String warehouse;
	/**
	 * 货位　货位号
	 */
	private String location;
	//具体数量是有可能带小数的，这里查了一例，钢管的数量是"QTY":14.5460
	/**
	 * 数量
	 * "QTY":14.5460
	 */
	private String number;
	/**
	 * 质保编号
	 */
	private String warrantyNo;
	/**
	 * 英文名称
	 */
	private String nameEn;
	/**
	 * 规格型号
	 */
	private String specificationNo;
	/**
	 * 材质
	 */
	private String material;
	/**
	 * 安全等级
	 */
	private String securityLevel;
	/**
	 * 质保等级
	 */
	private String warrantyLevel;
	/**
	 * 标准
	 */
	private String standard;
	/**
	 * 位号
	 */
	private String positionNo;
	/**
	 * 炉批号
	 */
	private String furnaceNo;
	/**
	 * 质保书数
	 */
	private Integer warrantyCount;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 船次件号
	 */
	private String shipNo;
	/**
	 * 单价
	 */
	private BigDecimal price;
	/**
	 * Cst_Code		会计科目
	 */
	private String cstCode;
	/**
	 * mrp_no		需求计划号
	 */
	private String mrpNo;
	/**
	 * WHEN_CNFMED		核实日期
	 */
	private Date whenCnfmed;
	/**
	 * KEEPER		保管员
	 */
	private String keeper;
	/**
	 * ISSNO		出库单号
	 */
	private String issNo;
	/**
	 * ISSDATE		开票日期/ 退库时用做退库日期
	 */
	private Date issDate;
	/**
	 * Iss_Dept		出库单位
	 */
	private String issDept;
	/**
	 * WHO_GET		领料员
	 */
	private String whoGet;
	/**
	 * CK_JE		出库金额
	 */
	private String ckJe;
	/**
	 * CK_JE_HS		核实金额
	 */
	private String ckJeHs;
	/**
	 * CPO_NO	合同号
	 */
	private String cpoNo;
	/**
	 * UNIT	单位

	 */
	private String unit;
	
	private String total_count;
	private String row_num;

}
