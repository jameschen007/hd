package com.easycms.hd.api.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class FileResult implements Serializable {
	private static final long serialVersionUID = -5009981411671329766L;
	/**
	 * 唯一ID号
	 */
	private Integer id;
	/**
	 * 文件路径
	 */
	private String url;
	/**
	 * 文件封面
	 */
	private String cover;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件转换状态
	 */
	private String convertStatus;
}
