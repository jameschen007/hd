package com.easycms.hd.api.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class MaterialExtractResult implements Serializable {
	private static final long serialVersionUID = 7136267007264102616L;
	/**
	 * 序号
	 */
	private Integer no;
	/**
	 * 唯一ID
	 */
	private Integer id;
	/**
	 * 质保编号
	 */
	private String warrantyNo;
	/**
	 * 出库量
	 */
	private Integer extractCount;
	/**
	 * 核实量
	 */
	private Integer confirmCount;
	/**
	 * 总价
	 */
	private BigDecimal totalPrice;
	/**
	 * 保管员
	 */
	private String keeper;
	/**
	 * 开票日期
	 */
	private Date invoiceDate;
	/**
	 * 签发日期
	 */
	private Date signDate;
	/**
	 * 领用部门
	 */
	private DepartmentResult department;
	/**
	 * 领用人
	 */
	private String receiver;
	/**
	 * 出库单号
	 */
	private String extractNo;
	/**
	 * 需要计划号
	 */
	private String planNo;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 会计科目
	 */
	private String accounting;
	
	/**
	 * 物料原数据
	 */
	private MaterialResult material;
}
