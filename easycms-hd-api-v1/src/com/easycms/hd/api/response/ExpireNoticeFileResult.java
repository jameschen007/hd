package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
/**
 * 最终版本　失效文件通知单　详情页
 * @author ul-webdev
 *
 */
@Data
public class ExpireNoticeFileResult implements Serializable{
	private static final long serialVersionUID = 478246806320043284L;
	private Integer id;
	private String cancCode;// 文件失效通知单编号 主表 通知单ID
	private String cancId;// enpower 本表 主键 文件id
	private String intercode;// 内部文件编号
	private String cTitle;// 中文名称
	private String revsion;// 版本
	private String status;// 状态
	private List<ExpireNoticeReaderResult> userList;
	
	

//	/** 文件失效通知单编号 */
//	private String cancCode;// 1514
//	/** 内部文件编号 */
//	private String intercode;// 07122A02BZS02-FM
//	/** 中文名称 */
//	private String cTitle;// 2A02区管道支吊架组装图(一)封面
//	/** 版本 */
//	private String revsion;// A
//	/** 状态 */
//	private String status;// INV
//	/** 文件使用人 */
//	private String borrower;// null
//	/** 份数 */
//	private String borNum;// null
//	/** 发布时间 */
//	private String signDate;// 42970
//	/** 编制人 */
//	private String drafter;// 王琳琳
//	/** 编制日期 */
//	private String draftDate;// 2017-07-15 19
//	/** 审批人 */
//	private String signer;// 陈官喜
//	/** 项目代号 */
//	private String projCode;// K2K3
//	/** 公司代码 */
//	private String compCode;// 50812
//	/** 文件借阅人 */
//	private String loginname;// 孙明光
//	/** 借阅人所属分发组 */
//	private String orgName;// 管焊队

}
