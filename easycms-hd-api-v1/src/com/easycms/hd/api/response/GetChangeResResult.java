package com.easycms.hd.api.response;

import java.io.Serializable;

import lombok.Data;

/**
 * 变更查询
 * @author wz
 *
 */
@Data
public class GetChangeResResult implements Serializable {
	private static final long serialVersionUID = 7136267007264102616L;
	
	/**
	 * 序号
	 */
//	private Integer no;
	/**文档类型: "正式工程图纸",*/
	private String docType ;
	/**文件类型: "安装文件",*/
	private String fileType ;
	/**版本: "A",*/
	private String version ;
	/**内部文件编号: "07123B54BZS08-892",*/
	private String innerFileCode ;
	/**文件编码: "KK3B5416892B40743SD",*/
	private String fileCode ;
	/**中文标题: "支吊架组装图",*/
	private String cnTitle ;
	/**项目代号: "K2K3",*/
	private String projCode ;
	/**公司代号: "050812"*/
	private String compCode ;

	/**
	 *   
  "文档类型":"正式工程图纸",
  "文件类型":"安装文件",
  "版本":"A",
  "内部文件编号":"07123B54BZS08-889",
  "文件编码":"KK3B5416889B40743SD",
  "中文标题":"支吊架组装图",
  "项目代号":"K2K3",
  "公司代号":"050812",
  "状态":"INV"
	 */

	/**"状态":"INV"*/
	private String status ;
	/**"备注":"remark"*/
	private String remark ;
}
