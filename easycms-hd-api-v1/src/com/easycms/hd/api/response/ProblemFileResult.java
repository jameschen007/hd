package com.easycms.hd.api.response;

import lombok.Data;

@Data
public class ProblemFileResult {
	private String path;
	private String fileName;
}
