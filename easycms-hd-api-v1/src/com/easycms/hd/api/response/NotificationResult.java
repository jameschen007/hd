package com.easycms.hd.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class NotificationResult implements Serializable{
	private static final long serialVersionUID = -6250884143124652664L;
	/**
	 * 通知唯一标识ID
	 */
	private Integer id;
	/**
	 * 通知主题
	 */
	private String subject;
	/**
	 * 通知正文
	 */
	private String content;
	/**
	 * 通知类型
	 */
	private String category;
	/**
	 * 部门名称
	 */
	private String department;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 提醒时间
	 */
	private String alarmTime;
	/**
	 * 通知状态
	 */
	private String status;
	/**
	 * 通知创建来源 
	 */
	private String source;
	/**
	 * 未读反馈
	 */
	private Long unread = 0L;
	/**
	 * 通知附件
	 */
	private List<FileResult> files;
	/**
	 * 通知人信息
	 */
	private Set<UserResult> participants;
	/**
	 * 反馈信息
	 */
	private List<NotificationFeedbackResult> feedback;
	/**
	 * 确认信息
	 */
	private boolean confirm = false;
}
