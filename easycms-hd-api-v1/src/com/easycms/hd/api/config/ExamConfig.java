package com.easycms.hd.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@Component
@Configuration
@PropertySource(value="classpath:exam.properties")
public class ExamConfig {
	
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
	
	/**
	 * 考试时长
	 */
	@Value("${exam.duration}")
	public String DURATION;
	
	/**
	 * 试题总分数
	 */
	@Value("${exam.score.total}")
	public String SCORE_TOTAL;
	
	/**
	 * 试题合格分数
	 */
	@Value("${exam.score.pass}")
	public String SCORE_PASS;
	
	/**
	 * 单选题总分
	 */
	@Value("${exam.score.total.radio}")
	public String SCORE_TOTAL_RADIO;
	
	/**
	 * 单个单选题分数
	 */
	@Value("${exam.score.single.radio}")
	public String SCORE_SINGLE_RADIO;
	
	/**
	 * 多选题总分
	 */
	@Value("${exam.score.total.multiple}")
	public String SCORE_TOTAL_MULTIPLE;
	
	/**
	 * 单个多选题分数
	 */
	@Value("${exam.score.single.multiple}")
	public String SCORE_SINGLE_MULTIPLE;
	
	/**
	 * 判断题总分
	 */
	@Value("${exam.score.total.judgment}")
	public String SCORE_TOTAL_JUDGMENT;
	
	/**
	 * 单个判断题分数
	 */
	@Value("${exam.score.single.judgment}")
	public String SCORE_SINGLE_JUDGMENT;

	/**
	 * 试题库最小总分数,抽取试题时作为抽题前提条件
	 */
	@Value("${exam.lib.min.score}")
	public String EXAM_LIB_MIN_SCORE;
}
