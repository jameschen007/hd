package com.easycms.hd.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
@EnableSwagger  
public class SwaggerConfig extends WebMvcConfigurerAdapter {  

	@Autowired
	@Qualifier("springSwaggerConfig")
    private SpringSwaggerConfig springSwaggerConfig;  
  
    /** 
     * Required to autowire SpringSwaggerConfig 
     */  
	@Autowired
	@Qualifier("springSwaggerConfig")
    public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig)  
    {  
        this.springSwaggerConfig = springSwaggerConfig;  
    }  
  
    /** 
     * Every SwaggerSpringMvcPlugin bean is picked up by the swagger-mvc 
     * framework - allowing for multiple swagger groups i.e. same code base 
     * multiple swagger resource listings. 
     */  
    @Bean  
    public SwaggerSpringMvcPlugin customImplementation()  
    {  
        return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)  
                .apiInfo(apiInfo())
                .includePatterns("/hdxt/api/enpower.*?", "/hdxt/api/.*?") 
                .apiVersion("1.0.0");
    }
    
    @Override  
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {  
      configurer.enable();  
    }
  
    private ApiInfo apiInfo()  
    {  
        ApiInfo apiInfo = new ApiInfo(  
                "Swagger测试环境",  
                "用于测试HD中所暴露出来的所有API",  
                "My Apps API terms of service",  
                "",  
                "HD",  
                "My Apps API License URL");  
        
        return apiInfo;  
    }  
}  