package com.easycms.hd.api.enpower;

import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.HttpRequest;
import com.easycms.hd.api.enpower.domain.EnpowerProcessDomain;
import com.easycms.hd.api.enpower.domain.EnpowerUser;
import com.easycms.hd.api.enpower.request.EnpowerRequest;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enpower.response.EnpowerResponseDepartment;
import com.easycms.hd.api.enpower.response.EnpowerResponseUser;
import com.easycms.hd.api.enpower.service.EnpowerUserService;
import com.easycms.hd.api.response.JsonResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
		MediaType.APPLICATION_JSON_VALUE })
@Api(value = "EnpowerUserApiController", description = "EnpowerUserApiController相关api")
@Transactional
public class EnpowerUserApiController {

	private static Logger logger = Logger.getLogger(EnpowerUserApiController.class);

	@Autowired
	private ThreadPoolTaskExecutor poolTaskExecutor;

	@Autowired
	private EnpowerUserService enpowerUserService;

	@Autowired
	private ComputedConstantVar constantVar;

	@Value("#{APP_SETTING['enpower_api_getdbinfo']}")
	private String enpowerApiInfo;

	Queue<EnpowerProcessDomain> queue = new LinkedBlockingQueue<EnpowerProcessDomain>();

	/**
	 * 用于载入用户信息到系统中，在POST的body体里传入json数据，示例如下：
	 * 
	 * @param request
	 * @param response
	 * @param domain
	 * @param timestamp
	 * @param signature
	 * @param userJson
	 * @return
	 */
	@ResponseBody
	@ApiOperation(value = "根据Enpower用户数据同步写入系统组织架构", notes = "enpower远程调用接口，传入一个json数据，最终解析为适配当前系统的组织架构。")
	@RequestMapping(value = "/user", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public JsonResult<Boolean> enposerUser(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = true, defaultValue = "enpower") String domain,
			@RequestHeader(required = true, defaultValue = "123") String timestamp,
			@RequestHeader(required = true, defaultValue = "251402f3307cb8f7e7bf53671e210144") String signature,
			@RequestBody(required = true) String data) {

		JsonResult<Boolean> result = new JsonResult<Boolean>();

		log.info(data);
		Gson gson = new Gson();
		EnpowerUser enpowerUser = new EnpowerUser();
		enpowerUser.setCreatedate(new Date());
		enpowerUser.setData(data);
		if (!enpowerUserService.insert(enpowerUser)) {
			result.setCode("-1001");
			result.setMessage("数据存储失败.");
			return result;
		} else {
			try {

				List<EnpowerResponseUser> enpowerResponseUsers = null;
				List<EnpowerResponseDepartment> enpowerResponseDepartments = null;

				// 处理存储的数据，现在无任何数据，以下处理是以假数据方式进行
				EnpowerRequest enpowerRequest = new EnpowerRequest();
				enpowerRequest.setCondition("");
				enpowerRequest.setXmlname("用户信息查询");
				enpowerRequest.setIsPage(1);
				enpowerRequest.setPageIndex(1);
				enpowerRequest.setPageSize(20000);

				String parms = enpowerRequest.toString();
				log.info("[EnpowerUserRequest] Info: " + constantVar.getEnpowerHost() + enpowerApiInfo + " : " + parms);

				String userRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerApiInfo, parms,
						"application/x-www-form-urlencoded", "GBK");
				if (null != userRequestResult && !"".equals(userRequestResult)) {
					BaseResponse baseResponse = gson.fromJson(userRequestResult, new TypeToken<BaseResponse>() {
					}.getType());
					if (baseResponse.getIsSuccess()) {
						String dataInfo = baseResponse.getDataInfo();
						enpowerResponseUsers = gson.fromJson(dataInfo, new TypeToken<List<EnpowerResponseUser>>() {
						}.getType());
						log.info(CommonUtility.toJson(enpowerResponseUsers));
						log.info("获取到用户 (包含重复)数量： " + enpowerResponseUsers.size());
					} else {
						log.error(baseResponse.getDataInfo());
					}
				}

				enpowerRequest = new EnpowerRequest();
				enpowerRequest.setXmlname("组织机构信息查询");
				enpowerRequest.setIsPage(1);
				enpowerRequest.setPageIndex(1);
				enpowerRequest.setPageSize(20000);

				parms = enpowerRequest.toString();
				log.info("[EnpowerDepartmentRequest] Info: " + constantVar.getEnpowerHost() + enpowerApiInfo + " : "
						+ parms);

				String departmentRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerApiInfo,
						parms, "application/x-www-form-urlencoded", "GBK");
				if (null != departmentRequestResult && !"".equals(departmentRequestResult)) {
					BaseResponse baseResponse = gson.fromJson(departmentRequestResult, new TypeToken<BaseResponse>() {
					}.getType());
					if (baseResponse.getIsSuccess()) {
						String dataInfo = baseResponse.getDataInfo();
						enpowerResponseDepartments = gson.fromJson(dataInfo,
								new TypeToken<List<EnpowerResponseDepartment>>() {
								}.getType());
						// log.info(CommonUtility.toJson(enpowerResponseDepartments));
						log.info("获取到部门数量： " + enpowerResponseDepartments.size());
					} else {
						log.error(baseResponse.getDataInfo());
					}
				}

				/**
				 * 使用线程来处理整个过程
				 */
				/*
				 * if (enpowerUserService.insertBatchDepartmentAndUser(
				 * enpowerResponseDepartments, enpowerResponseUsers)) {
				 * log.info("批量更新部门和用户成功。");
				 * 
				 * //[2017-12-14]当前用户上下级对系统不再有影响,取消下面一行的执行
				 * //HttpRequest.sendGet("http://127.0.0.1" + ":" +
				 * request.getLocalPort() +
				 * request.getSession().getServletContext().getContextPath() +
				 * "/hdxt/api/enpowerUserProcess?code=e00d0104-ac91-45b1-9ac2-21ea1cc9e4fe",
				 * null, null); }
				 */

				EnpowerProcessDomain enpowerProcessDomain = new EnpowerProcessDomain();
				enpowerProcessDomain.setEnpowerResponseUsers(enpowerResponseUsers);
				enpowerProcessDomain.setEnpowerResponseDepartments(enpowerResponseDepartments);
				boolean offerResult = queue.offer(enpowerProcessDomain);
				if (poolTaskExecutor.getPoolSize() < poolTaskExecutor.getMaxPoolSize()) {
					Thread insertBatchDepartmentAndUserThread = new Thread(new InsertBatchDepartmentAndUserThread());
					poolTaskExecutor.execute(insertBatchDepartmentAndUserThread);
				}
				// log.info("当前待处理任务加入线程的结果 [" + offerResult + "]");
			} catch (Exception e) {
				e.printStackTrace();
				result.setMessage("操作异常:" + e.getMessage());
				result.setResponseResult(false);
				return result;
			}
		}

		return result;
	}

	/**
	 * 每天同步enpower机构和人员
	 */
	@Scheduled(cron = "0 0 2 * * ?")
	public void updateRollingUsering() {
		logger.info("updateRollingUsering begin 同步enpower机构和人员...");

		this.enposerUser(null, null, null, null, null, null);

		logger.info("updateRollingUsering end 同步enpower机构和人员over。");

	}

	private class InsertBatchDepartmentAndUserThread implements Runnable {
		public void run() {
			while (!queue.isEmpty()) {
				EnpowerProcessDomain enpowerProcessDomain = queue.poll();
				if (null != enpowerProcessDomain) {
					List<EnpowerResponseUser> enpowerResponseUsers = enpowerProcessDomain.getEnpowerResponseUsers();
					List<EnpowerResponseDepartment> enpowerResponseDepartments = enpowerProcessDomain
							.getEnpowerResponseDepartments();
					try {
						if (enpowerUserService.insertBatchDepartmentAndUser(enpowerResponseDepartments,
								enpowerResponseUsers)) {
							log.info("独立线程中批量更新部门和用户成功。");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
