package com.easycms.hd.api.enpower.request;

import lombok.Data;

/**
 * 责任单位整改完成接口		需要接收的数据bean
 * 推送接口名: 责任单位整改完成接口	
 */
@Data
public class EnpowerRequestHSEQuestionComplete {
//	：APP主键
	private String APPID;
//	: 状态值（'0' -'编制中' , '1' -'未接收' ,'2' - '已接收' ,'3' - '整改反馈','4' -'执行完成' ,'5' -'整改不合格' ）
	private String FLAG;
//	：整改人
	private String MODI_MAN;
//	：整改后照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）
	private String ZG_PHOTO;
//	：整改完成日期
	private String MODI_DATE ;
////	TROU_CODE：整改单的编号
//	private String TROU_CODE;

	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb.append("{");
		
		//
		sb.append("'APPID':'");
		if(APPID!=null)
		sb.append(APPID);
		sb.append("',");
//
		sb.append("'FLAG':'");
		if(FLAG!=null)
		sb.append(FLAG);
		sb.append("',");
//  
		sb.append("'MODI_MAN':'");
		if(MODI_MAN!=null)
		sb.append(MODI_MAN);
		sb.append("',");
//
		sb.append("'ZG_PHOTO':'");
		if(ZG_PHOTO!=null)
		sb.append(ZG_PHOTO);
		sb.append("',");
		
		sb.append("'MODI_DATE':'");
		if (MODI_DATE != null)
			sb.append(MODI_DATE);
		sb.append("'");
		

		sb.append("}");
		
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		EnpowerRequestHSEQuestionComplete t = new EnpowerRequestHSEQuestionComplete();
		System.out.println(t.toString());
	}
}
