package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.wordnik.swagger.annotations.ApiModel;

import lombok.Data;

/**
 * 插入库存信息		需要接收的数据bean	
 * 推送接口名: 插入库存信息
 */
@Data
@ApiModel(value="enpowerRequestMaterialStoreCheck")
public class EnpowerRequestMaterialStoreCheckStock  implements Serializable{
	private static final long serialVersionUID = 1L;
	/**入库信息ID*/
	private String INSTK_ID ;
	/**物权属性(S/O)*/
	private String MATE_HOLDER ;
	/**入库类型*/
	private String INSTK_TYPE ;
	/**合同/订单编号*/
	private String CPO_NO ;
	/**PKG_NO：箱件编号*/
	private String PKG_NO;
	/**物项编码*/
	private String MATE_CODE ;
	/**生产日期*/
	private String YIELD_DATE ;
	/**有效期*/
	private String WARRANT_START ;
	/**保质期_月*/
	private String SHELIFE_MONTHS ;
	/**入库数量*/
	private String QTY_ARRIVED ;
	/**库存数量*/
	private String STK_QTY ;
	/**单价*/
	private String PRICE ;
	/**存储仓库*/
	private String WAREH_CODE ;
	/**货位号*/
	private String WAREH_PLACE ;
	/**储存区域*/
	private String WAREH_AREA ;
	/**数据创建日期*/
	private String ROWDATE ;
	/**质保号*/
	private String GUARANTEENO ;
	/**船次*/
	private String DELIV_LOT ;
	/**是否设备工机具*/
	private String GJJSBFL ;
	/**合同明细ID*/
	private String CPO_DETAILID ;

	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
