package com.easycms.hd.api.enpower.request;

import org.apache.log4j.Logger;

import com.easycms.common.util.CommonUtility;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 滚动计划状态同步Enpower的数据对象
 * @author wangzhi
 *
 */
@Data
public class EnpowerRequestPlanStatusSyn {
	
	public static Logger logger = Logger.getLogger(EnpowerRequestPlanStatusSyn.class);

	//作业条目id
		@SerializedName(value = "ID")
		//作业条目id
		private String workListId ;
		
	    //'ZY_DEPT':'管道一班一组（作业组名称）',
		@SerializedName(value = "ZY_DEPT")
		private String ZY_DEPT ;
	   // 'ZY_MAN':'张三（作业组负责人）',
		@SerializedName(value = "ZY_MAN")
		private String ZY_MAN ;
		
	//状态
		@SerializedName(value = "STATUS")
		//状态(状态编码未确定）
		private String status ;
	//实际开始时间
		@SerializedName(value = "START_DATE")
		//施工组长拿到的时间
		private String consteamDate ;
	//实际完成时间
		@SerializedName(value = "END_DATE")
		//完成时间(最后一个消点的时间)
		private String qcdate ;
	//实际完成工程量
		@SerializedName(value = "REAL_QTY")
		//QC1见证回填“实际完成工程量”，两位小数的数字
		private String realProjectcost ;
	//是否报量
		@SerializedName(value = "FLAG")
		//是否报量FLAG
		private String isReport ;

		public static void main(String[] args) {
			EnpowerRequestPlanStatusSyn test = new EnpowerRequestPlanStatusSyn();
			test.setQcdate("qcDate");
			logger.info(CommonUtility.toJson(test));
		}
}
