package com.easycms.hd.api.enpower.request.build;

import java.util.Calendar;

import com.easycms.common.util.DateUtility;
import com.easycms.hd.api.enpower.domain.EnpowerConstantVar;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckForm;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckInfo;
import com.easycms.hd.api.enpower.request.EnpowerRequestMaterialStoreCheckStock;
import com.easycms.hd.api.enpower.response.EnpowerResponseMaterialStoreSearch;
import com.easycms.management.user.domain.User;

/**
 * 生成　EnpowerRequestMaterialStoreCheckInfo　类
 * @author wz
 *
 */
public class BuildEnpowerRequestMaterialStoreCheckInfo {
	
	public EnpowerRequestMaterialStoreCheckStock setMaterialStoreCheckStock(
			EnpowerRequestMaterialStoreCheckStock stock,EnpowerResponseMaterialStoreSearch enp,
			EnpowerRequestMaterialStoreCheckForm form){
		// 入库信息ID
		stock.setINSTK_ID(enp.getID());
		// 物权属性(S/O)
		stock.setMATE_HOLDER(EnpowerConstantVar.MATE_HOLDER);
		// 入库类型
		stock.setINSTK_TYPE(enp.getINSTK_TYPE());
		// 合同/订单编号
		stock.setCPO_NO(enp.getCPO_NO());
		// 物项编码
		stock.setMATE_CODE(enp.getMATE_CODE());
		// 生产日期
		stock.setYIELD_DATE(form.getYIELD_DATE());
		// 有效期
		stock.setWARRANT_START(form.getWARRANT_START_DATE());
		// 保质期_月
		stock.setSHELIFE_MONTHS(form.getSHELIFE_MONTHS());
		// 入库数量
		stock.setQTY_ARRIVED(form.getCHECK_QTY());
		// 库存数量
		stock.setSTK_QTY(form.getCHECK_QTY());
		// 单价
		stock.setPRICE(enp.getPRICE());
		// 存储仓库
		stock.setWAREH_CODE(form.getSTORAGES_CODE());
		// 货位号
		stock.setWAREH_PLACE(form.getVESSEL_NO());
		// 储存区域
		stock.setWAREH_AREA(form.getWAREH_AREA());
		// 数据创建日期
		stock.setROWDATE(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));
		// 质保号
		stock.setGUARANTEENO(enp.getGUARANTEENO());
		// 船次
		stock.setDELIV_LOT(form.getDELIV_LOT());
		// 是否设备工机具
		stock.setGJJSBFL(form.getGJJSBFL());
		// 合同明细ID
		stock.setCPO_DETAILID(enp.getCPO_DETAIL_ID());

		return stock;
	}

	public EnpowerRequestMaterialStoreCheckInfo setMaterialStoreCheckInfo(
			EnpowerRequestMaterialStoreCheckInfo toInfo,EnpowerResponseMaterialStoreSearch enp
			,EnpowerRequestMaterialStoreCheckForm form,User loginUser){
		//enp标识
		toInfo.setID(enp.getID());
		//制造商
		toInfo.setMANUFACTURER(form.getMANUFACTURER());
		//单价
		toInfo.setPRICE(enp.getPRICE());
		//生产日期
		toInfo.setYIELD_DATE(form.getYIELD_DATE());//+" 00:00:01"
		//有效期
		toInfo.setWARRANT_START_DATE(form.getWARRANT_START_DATE());
		//保质期/月
		toInfo.setSHELIFE_MONTHS(form.getSHELIFE_MONTHS());
		//保修期/月
		toInfo.setMONTHS_WARRANT(form.getMONTHS_WARRANT());
		//船次
		toInfo.setDELIV_LOT(enp.getDELIV_LOT());
//		//换算率
//		toInfo.setFACTRATE(());
//		//不合格数量
//		toInfo.setNOTPASSQTY(());
		//存储仓库
		toInfo.setSTORAGES_CODE(form.getSTORAGES_CODE());
		//存储级别
		toInfo.setSTRG_CLASS(form.getSTRG_CLASS());
		//货位
		toInfo.setVESSEL_NO(form.getVESSEL_NO());
		//验收时间
		toInfo.setCNFMED_DATE(DateUtility.sdfyMdHms.format(Calendar.getInstance().getTime()));
		//仓储验收
		toInfo.setKEEPER(loginUser.getRealname());
		//文件编号
		toInfo.setFILE_NO(form.getFILE_NO());
		//状态
		toInfo.setSTATUS(EnpowerConstantVar.alreadyStore);
		
		String MATE_CODE = enp.getMATE_CODE();
		if(MATE_CODE==null){
			MATE_CODE="";
		}
		
		String FUNC_NO = enp.getFUNC_NO();
		if(FUNC_NO==null){
			FUNC_NO="";
		}
		//质保编号
		toInfo.setGUARANTEENO(form.getGUARANTEENO()+MATE_CODE+FUNC_NO);
		
		//{"xml": "修改入库信息(按质保号)","date_col":"","_value":"[{'STATUS':'已入库','GUARANTEENO||MATE_CODE||FUNC_NO':'CS-APP-0020103040010030'}]"}
		
		return toInfo;
	}
	
}
