package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 * 上报质量整改问题接口	需要接收的数据bean		
推送接口名: 
 */
@Data
public class EnpowerRequestQualityQuestionSend {
	/**责任班组*/
	private String MODI_POSI ;
	/**问题描述*/
	private String MODI_REQU ;
	/**责任部门*/
	private String DUTY_COMP ;
	/**机组*/
	private String UNIT ;
	/**子项*/
	private String BUILDING ;
	/**区域*/
	private String AREA ;
	/**楼层*/
	private String LEVELS ;
	/**房间号*/
	private String ROOM ;
	/**系统*/
	private String STSYEM ;
	/**问题照片（Oracle存储为BLOB类型，APP推送前转换为二进制数据）*/
	private String ZGQ_PHOTO ;

	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
