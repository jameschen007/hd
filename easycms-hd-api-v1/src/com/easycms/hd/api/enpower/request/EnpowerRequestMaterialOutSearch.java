package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 物项出库查询接口		需要接收的数据bean	
 * 推送接口名: 物项出库查询接口
 */
@Data
public class EnpowerRequestMaterialOutSearch {
	@SerializedName(value = "出库单号")
	/**
	 * 出库单号
	 */
	private String outNo="KK-WXCK-201710-00612";
//	质保编号
//	项目代号（默认值“K2K3”）
//	公司代码（K项默认值：“050812”)
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;


	
	
//	出库单号
//	项目代号（默认值“K2K3”）
//	公司代码（K项默认值：
}
