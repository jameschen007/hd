package com.easycms.hd.api.enpower.request;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 插入焊口条目信息
 * @author wangz
 *
 */
@Data
public class EnpowerRequestPlanInsertWeld {

	public static Logger logger = Logger.getLogger(EnpowerRequestPlanStatusSyn.class);

	// WORKLIST_ID,--作业条目ID，我们推送给app里有
	@SerializedName(value = "WORKLIST_ID")
	// 作业条目id
	private String workListId;

	// FLAG,--施工是否完成标志，默认N，完成则为Y

	// 施工是否完成标志，默认N，完成则为Y
	@SerializedName(value = "FLAG")
	private String flag = "N";
	// WORK_STATE,--施工状态，如未分配
	@SerializedName(value = "WORK_STATE")
	private String WORK_STATE;
	// START_DATE,--实际开始时间
	@SerializedName(value = "START_DATE")
	private String START_DATE;

	// END_DATE,--实际完成时间
	@SerializedName(value = "END_DATE")
	private String END_DATE;
	// WORK_TEAM_NAME,--作业组名称
	@SerializedName(value = "WORK_TEAM_NAME")
	private String WORK_TEAM_NAME;

	// WORK_TEAM_MAN,--作业组负责人
	@SerializedName(value = "WORK_TEAM_MAN")
	private String WORK_TEAM_MAN;
	// REAL_WORK_GCL,--实际完成工程量
	@SerializedName(value = "REAL_WORK_GCL")
	private String realProjectcost;
	// WELD_CODE,--焊口号，如A01
	//
	@SerializedName(value = "ELD_CODE")
	private String ELD_CODE;

	// PROJ_CODE,--固定代码：K2K3
	@SerializedName(value = "PROJ_CODE")
	private String PROJ_CODE = EnpConstant.PROJ_CODE;

	// COMP_CODE --固定代码：050812
	@SerializedName(value = "COMP_CODE")
	private String COMP_CODE= EnpConstant.COMP_CODE;

}

