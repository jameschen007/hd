package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 * 在APP关闭时，将质量问题推送至enpower
 */
@Data
public class EnpowerRequestQualityQuestionCloseSend {
	
	/**ZRDEPT--责任单位*/
	private String ZRDEPT ;
	/**STATUS--状态*/
	private String STATUS ;
	/**ORIGIN--来源*/
	private String ORIGIN ;
	/**REASION_CODE--原因编码*/
	private String REASION_CODE ;
	/**DESCRIPTION--不符合描述*/
	private String DESCRIPTION ;
	/**EDITER--编制人*/
	private String EDITER ;
	/**EDIT_DATE--编制日期*/
	private String EDIT_DATE ;
	/**ZGR--整改人*/
	private String ZGR ;
	/**ZG_DATE--整改日期*/
	private String ZG_DATE ;
	/**YZR--验证人*/
	private String YZR ;
	/**YZ_DATE--验证日期*/
	private String YZ_DATE ;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
