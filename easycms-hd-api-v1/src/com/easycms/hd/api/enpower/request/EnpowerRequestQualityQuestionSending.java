package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 * 在APP关闭时，将质量问题推送至enpower
 */
@Data
public class EnpowerRequestQualityQuestionSending {

	/**STATUS--状态*/
	private String STATUS ;
	/**ZRDEPT--责任单位*/
	private String ZRDEPT ;	
	/**ORIGIN--来源*/
	private String ORIGIN ;
	/**REASION_CODE--原因编码*/
	private String REASION_CODE ;
	/**DESCRIPTION--不符合描述*/
	private String DESCRIPTION ;
	/**EDITER--编制人*/
	private String EDITER ;
	/**EDIT_DATE--编制日期*/
	private String EDIT_DATE ;
	/**ZGR--整改人*/
	private String ZGR ;
	/**ZG_DATE--整改日期*/
	private String ZG_DATE ;
	/**YZR--验证人*/
	private String YZR ;
	/**YZ_DATE--验证日期*/
	private String YZ_DATE ;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	
	private String AREA;//区域'
	private String SYSTEMS;//系统'
	private String ROOM;//房间'
	private String UNITS;//机组'
	private String REMARK;//备注'
	private String TITLE;//问题单标题'
	private String TYPE;//问题单类型'

}
