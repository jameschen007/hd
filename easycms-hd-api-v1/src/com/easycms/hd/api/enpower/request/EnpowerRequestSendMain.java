package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 滚动计划发点接口主信息
 * 一、	管道发点接口：
作业组长发起见证APP主动调用ENPOWER提供的接口推送发点信息。华辉公司使用的是已有的Services。

1.	步骤1：ENPOWER需接收推送的数据：

 * @author wangzhi
 *
 */
@Data
public class EnpowerRequestSendMain {
//  质量计划号
	@SerializedName(value = "ITP_NO")
	//质量计划号
	private String qualityplanno	;	

//  技术员
	@SerializedName(value = "JS_FZR")
	//技术员（Enpower是存在技术员那个字段的，APP传组长名称）
	private String consteamName	;	
//  机组
	@SerializedName(value = "UNITS")
	//机组号
	private String unitNo	;	
//  子项
	@SerializedName(value = "SUB")
	//子项
	private String subItem	;	
//  系统
	@SerializedName(value = "SYS")
	//系统号
	private String systemNo	;
//  通知单名称
	@SerializedName(value = "TZ_NAME")
	//通知单名称
	private String MQP_NAME	;	//TODO 1

	 //  ITP类型（类别）
		@SerializedName(value = "ITP_TYPES")
		private String ITP_TYPES	;	
	//  状态
		@SerializedName(value = "FLAG")
		private String state	;
	//  主专业
		@SerializedName(value = "SPEC")
		//(质量计划的)专业
		private String SPECIALTY	;	
	//  版本
		@SerializedName(value = "VER")
		//版本（质量计划版本）
		private String T_REV	;
	//  区域
		@SerializedName(value = "AREA")
		//区域
		private String AREA	;	
	//  ITP模板类型
		@SerializedName(value = "ITP_TYPE")
		//ITP模板类型（模板类型）
		private String ITP_TYPE	;	
	//  施工班组
		@SerializedName(value = "SG_BZ")
		//施工班组代码
		private String consmonitorCode	;	
	//  电话
		@SerializedName(value = "TEL")
		//电话
		private String tel	;	//TODO 7
	//  作业组长
		@SerializedName(value = "SGZY_ZZ")
		//技术员（Enpower是存在技术员那个字段的，APP传组长名称）
		private String consteamName2	;
	//  施工队
		@SerializedName(value = "SG_DEPT")
		//施工队
		private String conscaptainName	;	
	//  系统通知单编码 通知单号
		@SerializedName(value = "SEND_NO")
		//系统通知单编码
		private String SEND_NO	;	
	//  编制人
		@SerializedName(value = "EDIT_MAN")
		//APP里面是组长
		private String consteamName3	;
//	20180302　沙道磊反映需要去掉
////  编制日期
//	@SerializedName(value = "EDIT_DATE")
//	//取系统当前日期
//	private String sysDate	;	//TODO 9

//20180509 福清测试时，发布消点信息推不到enpower,才从沙道磊处理获知，需要增加以下几个数据。
		@SerializedName(value = "B_SELECT")
		private String bSelect	;

		@SerializedName(value = "C_SELECT")
		private String cSelect	;

		@SerializedName(value = "D_SELECT")
		private String dSelect	;

		@SerializedName(value = "H_SELECT")
		private String hSelect	;

		/**项目代号（默认值“K2K3”） */
		private String PROJ_CODE = EnpConstant.PROJ_CODE;
		/**项目代码（K项默认值    “050812”) */
		private String COMP_CODE = EnpConstant.COMP_CODE ;
	//  用于Enpower关联信息使用的APPｉｄ
		@SerializedName(value = "APP_ID")
	private String appId;
}
