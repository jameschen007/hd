package com.easycms.hd.api.enpower.request;

import lombok.Data;

/**
 *   查询今日入库量明细接口 		需要接收的数据bean	
 *  推送接口名:  查询今日入库量明细接口
 */
@Data
public class EnpowerRequestMaterialTodayStoreList {
	
//	当天日期
	private String toDay=null;
//	项目代号（默认值“K2K3”）
//	公司代码（K项默认值：“050812”)
 
}
