package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.MyURLEncoder;

import lombok.Data;

@Data
public class EnpowerRequest {
	private String xmlname = ""; // 用户信息查询
	private String condition = ""; // 中文名='张三'
	private String order = ""; // 
	private Integer isPage; // 1
	private Integer pageIndex; // 2
	private Integer pageSize; // 4
	
	@Override
	public String toString() {
		return "xmlname=" + MyURLEncoder.encode(xmlname) + "&condition=" + MyURLEncoder.encode(condition) + "&order=" + order + "&isPage=" + isPage + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize;
	}
}
