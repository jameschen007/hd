package com.easycms.hd.api.enpower.request;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 推送接口名: 插入通知单明细					
 * @author ul-webdev
 *
 */
@Data
public class EnpowerRequestSendNotice {
//  外键（来源于发点通知单主信息的主键ID）
	@SerializedName(value = "MAIN_ID")
	//外键（来源于发点通知单主信息的主键ID）
	private String mainId;	

//  通知单编号（app生成）
	@SerializedName(value = "NOTE_CODE")
	//通知单编号（app生成）
	private String noticeOrderNo;	

//  作业名称
	@SerializedName(value = "TASK_NAME")
	//作业名称（中文）
	private String stepname;	

//  作业序号(工序号)
	@SerializedName(value = "TASK_SEQU")
	//工序号
	private String stepIdentifier;	

//  质量计划编号
	@SerializedName(value = "QUAL_PLAN_CODE")
	//质量计划号
	private String qualityplanno;	

//  A单位设点(QC1)
	@SerializedName(value = "A_POINT")
	//QC1
	private String noticeaqc1;	

//  B单位设点(CZEC QC)
	@SerializedName(value = "B_POINT")
	//CZEC QC
	private String noticeczecqc;	

//  C单位设点(CZEC QA)
	@SerializedName(value = "C_POINT")
	//CZEC QA
	private String noticeczecqa;	

//  D单位设点(PAEC)
	@SerializedName(value = "D_POINT")
	//PAEC
	private String noticepaec;	

//  H单位设点（QC2）
	@SerializedName(value = "H_POINT")
	//QC2
	private String noticeaqc2;	

//  通知人
	@SerializedName(value = "NOTE_MAN")
	//技术员（Enpower是存在技术员那个字段的，APP传组长名称）
	private String consteamName;	

////  通知日期
//	@SerializedName(value = "NOTE_DATE")
//	//系统当前日期
//	private String currentDate;	

//  子专业
	@SerializedName(value = "SUB_SPEC")
	//子专业
	private String subSpecialty;	

//  见证地点(数据来自发点通知单主信息的 区域字段)
	@SerializedName(value = "WITN_ADDR")
	//见证地点(数据来自发点通知单主信息的 区域字段)
	private String witnessaddress;
	
//  见证日期
	@SerializedName(value = "WITN_DATE")
	private String witnessDate;
	
	@SerializedName(value = "APP_ID")
	private String appId;	
	@SerializedName(value = "APP_MAIN_ID")
	private String appMainId;
	//'GCL_ID':'作业条目id' ，    ------新加字段------（备注：将对应的作业条目的ID（worklistId）插入该字段）
	@SerializedName(value = "GCL_ID")
	private String worklistId
;	
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

////  见证日期（值为当前时间加一天）
//	@SerializedName(value = "WITN_DATE")
//	//见证日期（值为当前时间加一天）
//	private String witnessDate;	

}
