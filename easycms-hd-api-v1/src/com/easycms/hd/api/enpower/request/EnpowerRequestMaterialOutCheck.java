package com.easycms.hd.api.enpower.request;

import java.io.Serializable;

import com.easycms.common.logic.context.constant.EnpConstant;

import lombok.Data;

/**
 *  物项出库核实接口		需要接收的数据bean	
 * 推送接口名: 物项出库核实接口
 */
@Data
public class EnpowerRequestMaterialOutCheck  implements Serializable{
	private static final long serialVersionUID = 1L;
	//物项编码   
    private String MATE_CODE;
//出库单号   
    private String ISSNO;
//出库量    
    private String ISSQTY;
//核实量    
    private String QTY_RELEASED;
	
//	MATE_CODE：物项编码
//	ISSNO：出库单号
//	ISSQTY：出库量
//	QTY_RELEASED：核实量
//	PROJ_CODE：项目代号（默认值“K2K3”）
//	COMP_CODE：公司代码（K项默认值：“050812”)
	/**项目代号（默认值“K2K3”） */
	private String PROJ_CODE = EnpConstant.PROJ_CODE;
	/**项目代码（K项默认值    “050812”) */
	private String COMP_CODE = EnpConstant.COMP_CODE ;

}
