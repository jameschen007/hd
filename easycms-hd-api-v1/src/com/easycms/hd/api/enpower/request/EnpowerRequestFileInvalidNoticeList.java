package com.easycms.hd.api.enpower.request;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 *   文件失效通知单列表接口		需要接收的数据bean	
 *  推送接口名: 文件失效通知单列表接口
 */
@Data
public class EnpowerRequestFileInvalidNoticeList {
//  通知单ID(来源于app创建的发点通知单主信息的id)
	@SerializedName(value = "TZD_ID")
	//外键（来源于发点通知单主信息的主键ID）
	private String mainId ;
	
//	回收单编号
//	项目代号（默认值“K2K3”）
//	公司代码（K项默认值：“050812”)
}
