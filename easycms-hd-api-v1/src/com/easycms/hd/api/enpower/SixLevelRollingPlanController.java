package com.easycms.hd.api.enpower;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.exception.ExceptionCode;
import com.easycms.common.logic.context.ComputedConstantVar;
import com.easycms.common.logic.context.ContextPath;
import com.easycms.common.logic.context.constant.SysConstant;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtility;
import com.easycms.common.util.HttpRequest;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.api.enpower.request.EnpowerRequestPlanStatusSyn;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendEquipment;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendMain;
import com.easycms.hd.api.enpower.request.EnpowerRequestSendNotice;
import com.easycms.hd.api.enpower.request.EnpowerRequestXmlName;
import com.easycms.hd.api.enpower.response.BaseResponse;
import com.easycms.hd.api.enums.enpower.EnpowerStatus;
import com.easycms.hd.api.response.JsonResult;
import com.easycms.hd.api.service.RollingPlanApiService;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.RollingPlan;
import com.easycms.hd.plan.domain.SixLevelRollingPlan;
import com.easycms.hd.plan.service.EnpowerJobOrderService;
import com.easycms.hd.plan.service.RollingPlanService;
import com.easycms.hd.plan.service.RollingPlanTansferService;
import com.easycms.hd.variable.service.VariableSetService;
import com.easycms.management.user.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping(value = "/hdxt/api/enpower",
	consumes = {MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(value = "SixLevelRollingPlanController", description = "SixLevelRollingPlanController相关api")
public class SixLevelRollingPlanController {
	
	private static Logger logger = Logger.getLogger(SixLevelRollingPlanController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private RollingPlanService rollingPlanService;
	@Autowired
	private RollingPlanApiService rollingPlanApiService;
	@Autowired
	private RollingPlanTansferService rollingPlanTansferService;
	@Autowired
	private ComputedConstantVar constantVar ;
	@Value("#{APP_SETTING['enpower_api_updateData']}")
	private String enpowerupdateData;
	@Autowired
	private EnpowerJobOrderService jobOrderService;
	@Autowired
	private VariableSetService variableSetService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
	
	Gson gson = new Gson();
	
	private String getCheckStr(){
    	Calendar c = Calendar.getInstance();

    	int x = c.get(Calendar.YEAR);
    	int y = c.get(Calendar.MONTH)+1;
    	int z = c.get(Calendar.DATE);
    	
    	int check = x*y*z;
    	String checkStr = check+"";
    	if(checkStr.length()>5){
    		checkStr = checkStr.substring(0, 5);
    	}

    	logger.info("今日临时效验码："+checkStr);
    	return checkStr;
	}

	@ResponseBody
	@ApiOperation(value = "绩效考核数据清除", notes = "绩效考核数据清除")
	@RequestMapping(value = "/clearAllKpiScore", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> clearAllKpiScore(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="123") String timestamp,
			@RequestHeader(required=true, defaultValue="ca50b9b8596de27d7080f9ead13d0c84") String signature,
			@ApiParam(required = false, name = "userId", value = "userId，已选班长首选")@RequestParam(value="userId",required=false) Integer userId,
			@ApiParam(required = false, name = "username", value = "用户名，已选班长其次")@RequestParam(value="username",required=false) String username
			) {

		JsonResult<Boolean> result = new JsonResult<Boolean>();
		String check = CommonUtility.MD5Digest(timestamp + domain + "hd5kpiclearallscore");

		if (check.equalsIgnoreCase(signature)) {
			logger.info("绩效考核数据清除！！！");
			try{
				result = rollingPlanApiService.clearAllKpiScore();
				result.setMessage("所有评分数据已清除！！");
			}catch(Exception e){
				e.printStackTrace();
				result.setResponseResult(false);
				result.setCode("-1001");
				result.setMessage("操作异常"+e.getMessage());
			}
		}
		
		return result;
		
	}
	@ResponseBody
	@ApiOperation(value = "滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长", notes = "滚动计划状态回退至最初队长已选班长状态 ，（可选）修改已选班长")
	@RequestMapping(value = "/initRollingPlanById", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> initRollingPlanById(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="123") String timestamp,
			@RequestHeader(required=true, defaultValue="ca50b9b8596de27d7080f9ead13d0c84") String signature,
			@ApiParam(required = true, name = "rollingPlanId", value = "rollingPlanId")@RequestParam(value="rollingPlanId",required=true) Integer rollingPlanId,
			@ApiParam(required = false, name = "userId", value = "userId，已选班长首选")@RequestParam(value="userId",required=false) Integer userId,
			@ApiParam(required = false, name = "username", value = "用户名，已选班长其次")@RequestParam(value="username",required=false) String username
			) {
		
		logger.info("通过计算输出 访问Enpower接口的地址="+constantVar.getEnpowerHost());
		StringBuffer URL = request.getRequestURL();
 		logger.info("获得本机URL="+URL);
 		logger.info("获得本机ip="+ContextPath.hostAddress);

		JsonResult<Boolean> result = new JsonResult<Boolean>();
		String chk = getCheckStr();
		if(chk.equals(signature)){

			try{
				String seting = variableSetService.findValueByKey("isDevelopDB", "system");
				 
				if("YES".equals(seting)){
					String ipFromHead = HttpUtility.getIpFromHead(request);
					logger.info("ipFromHead="+ipFromHead);
					if(SysConstant.ipFromHead0.equals(ipFromHead)){//本机测试
						result = rollingPlanApiService.initRollingPlan(userId,username);
					}
				}else{
					
					result = rollingPlanApiService.initRollingPlan(userId,rollingPlanId,username);
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result.setResponseResult(false);
				result.setCode("-1001");
				result.setMessage("操作异常"+e.getMessage());
			}
		}
		
		return result;
		
	}

	@ResponseBody
	@ApiOperation(value = "进度计划同步写入APP", notes = "enpower远程调用接口，用于创建RollingPlan")
	@RequestMapping(value = "/batchRollingPlan", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> rollingplan(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="123") String timestamp,
			@RequestHeader(required=true, defaultValue="ca50b9b8596de27d7080f9ead13d0c84") String signature,
			@RequestBody String form
			) {
		Gson gson = new Gson();
		

		List<SixLevelRollingPlan> sixLevelRollingPlans = gson.fromJson(form, new TypeToken<List<SixLevelRollingPlan>>() {}.getType());
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		boolean f = false;
		
		try{
			result = rollingPlanApiService.save(sixLevelRollingPlans);

			//正常记录原始内容
			FileUtility ut = new FileUtility();

			SimpleDateFormat sdf =new SimpleDateFormat("MM_dd_HHmmssS");
			String rollingfile = sdf.format(Calendar.getInstance().getTime())+"_OK.txt";
			String filePath_ = com.easycms.common.logic.context.ContextPath.fileBasePath+com.easycms.common.util.FileUtils.rollingPlanFilePath;
	        String filenameTemp = filePath_+"/"+rollingfile;//文件路径+名称+文件类型
	        
			ut.createFile(filenameTemp, form);
			
		}catch(Exception e){

			//异常记录原始内容
			FileUtility ut = new FileUtility();
			ut.createFile(null, form);
			
			e.printStackTrace();
			f = false ;
			result.setResponseResult(f);
			result.setCode("-1001");
			result.setMessage("操作异常"+e.getMessage()+form);
		}
		
//		log.info(JsonUtils.objectToJson(sixLevelRollingPlans));
		
		return result;
	}
	
	/**
	 * 一、Enpower提供的状态同步接口				QC1消点时填入
计划主键id	状态	实际开始时间	完成时间	实际完成工程量
	 */
	/**
	 * 根据滚动计划，查出 bean，取出其中的参数调用接口
	 */
	/**
	 * 任务状态接口 （包含任务施工中，停滞，未完成，已完成等状态）
	 */
	@ResponseBody
	@ApiOperation(value = "任务状态接口:修改作业条目信息 （包含任务施工中，停滞，未完成，已完成等状态）手工同步", notes = "任务状态接口:修改作业条目信息 （包含任务施工中，停滞，未完成，已完成等状态）手工同步")
	@RequestMapping(value = "/stateSynRollingPlan", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> stateSynRollingPlan(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "rollingPlanId", value = "计划id") @RequestParam(value="rollingPlanId",required=true) Integer rollingPlanId) {
		
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		log.info(rollingPlanId+"");
		Gson gson = new Gson();
			
//		一、Enpower提供的状态同步接口				QC1消点时填入
//		计划主键id	状态	实际开始时间	完成时间	实际完成工程量
		RollingPlan plan = rollingPlanService.findById(rollingPlanId);
		
		System.out.println("计划主键 id="+plan.getEnpowerPlanId());
		System.out.println("状态="+plan.getIsend());//0初始、1施工、2完成、3退回,4计划，5处理中 

		System.out.println("完成时间="+plan.getRealEndDate());
		System.out.println("实际完成工程量="+plan.getRealProjectcost());

		List<EnpowerRequestPlanStatusSyn> list = new ArrayList<EnpowerRequestPlanStatusSyn>();
		try{
			EnpowerRequestPlanStatusSyn planStatus = rollingPlanApiService.findEnpowerStatusSynByRollingPlanId(plan,null);
			list.add(planStatus);
			logger.info("修改作业条目信息:"+CommonUtility.toJson(planStatus));
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
		bn.setXml("修改作业条目信息");
		bn.setDate_col("START_DATE,END_DATE");
		String data = CommonUtility.toJson(list);
		bn.set_value(data);
		logger.info(bn.toString());
		
		log.info("[EnpowerRequestPlanStatusSyn] Info: " + constantVar.getEnpowerHost() + enpowerupdateData + " : " +  bn.toString());

		try{

			String userRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerupdateData, bn.toString(), "application/x-www-form-urlencoded", "GBK");
			//{"IsSuccess":true,"ErrorMsg":null,"DataInfo":"[{\"status\":\"保存数据操作成功\"}]"}
			if (null != userRequestResult && !"".equals(userRequestResult)){
				BaseResponse baseResponse = gson.fromJson(userRequestResult, new TypeToken<BaseResponse>() {}.getType());
				if (baseResponse.getIsSuccess()){
					String returnInfo = baseResponse.getDataInfo();
					result.setMessage(returnInfo);
					log.info(CommonUtility.toJson(returnInfo));
				} else {
					log.error(baseResponse.getErrorMsg());
					log.error(baseResponse.getDataInfo());
				}
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}

		
			
		return result;
	}

	/**
	 *发点主信息接口:推送接口名: 插入发点通知单信息					
	 */
	@ResponseBody
	@ApiOperation(value = "发点信息接口手工同步,整合发点的3个子接口,最后再调用状态接口", notes = "发点信息接口手工同步,整合发点的3个子接口")
	@RequestMapping(value = "/sendPointToEnpower", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> sendPointToEnpower(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@RequestHeader(required=true, defaultValue="158434") Integer workStepIds) {

		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{
			Integer[] workStepIdss = new Integer[1];
			workStepIdss[0]=workStepIds;
//			result = executePushToEnpowerServiceImpl.insertPointToEnpower(null,workStepIdss);
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
		return result;
	}
	
	/**
	 *发点主信息接口:推送接口名: 插入发点通知单信息					
	 */
	@ResponseBody
	@ApiOperation(value = "发点信息接口手工同步4 管道发点接口 :插入发点通知单信息（作业组长发起见证的时候需要后台发起）", notes = "发点信息接口手工同步4 管道发点接口 :插入发点通知单信息（作业组长发起见证的时候需要后台发起）")
	@RequestMapping(value = "/sendPointMain", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> sendPointMain(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "rollingPlanId", value = "计划id") @RequestParam(value="rollingPlanId",required=true) Integer rollingPlanId) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		
		log.info(rollingPlanId+"");
		List<EnpowerRequestSendMain> mainList = null;
		try{
			mainList = rollingPlanApiService.findEnpowerSendPointMainByRollingPlanId(rollingPlanId);
			logger.info("发点主信息接口:推送接口名: 插入发点通知单信息		="+CommonUtility.toJson(mainList));
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
		bn.setXml("插入发点通知单信息");
//		bn.setDate_col("EDIT_DATE");
		String data = CommonUtility.toJson(mainList);
		bn.set_value(data);
		logger.info(bn.toString());
		log.info("[EnpowerRequestSendMain] Info: " + constantVar.getEnpowerHost() + enpowerupdateData + " : " +  bn.toString());

		try{

			updateEnpowerAndSetResult(result, bn);
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		return result;
	}

	/**
	 * 更新Enpower接口内容,并设置返回值.
	 * 其中会根据变量配置是否调用.
	 * @param result
	 * @param bn
	 * @throws Exception 
	 */
	private void updateEnpowerAndSetResult(JsonResult<Boolean> result, EnpowerRequestXmlName bn) throws Exception {
		String userRequestResult = null;
//		String seting = variableSetService.findValueByKey("tempEnpowerOnOff", "system");
//		
		
//		if("ON".equals(seting)){
			userRequestResult = HttpRequest.sendPost(constantVar.getEnpowerHost() + enpowerupdateData, bn.toString(), "application/x-www-form-urlencoded", "GBK");
//		}
		//
		if (null != userRequestResult && !"".equals(userRequestResult) ){
			BaseResponse baseResponse = gson.fromJson(userRequestResult, new TypeToken<BaseResponse>() {}.getType());
			if (baseResponse.getIsSuccess()){
				String returnInfo = baseResponse.getDataInfo();
				result.setMessage(returnInfo);
				log.info(CommonUtility.toJson(returnInfo));
			} else {
				log.error(baseResponse.getErrorMsg());
				log.error(baseResponse.getDataInfo());
			}

			boolean failure = baseResponse.getIsSuccess()==false || baseResponse.getDataInfo()==null 
					|| baseResponse.getDataInfo().indexOf(EnpowerStatus.SUCCESS)==-1;
			if(failure){
				logger.error("Enpower接口调用异常！！"+bn.toString());
				throw new RuntimeException("Enpower接口调用异常！！"+baseResponse.getErrorMsg()+baseResponse.getDataInfo());
			}
		}else{
			throw new RuntimeException("Enpower接口调用没有返回！！");
		}
	}
	
	/**
	 *发点信息接口:推送接口名: 插入通知单明细		
	 */
	@ResponseBody
	@ApiOperation(value = "发点信息接口手工同步4 管道发点接口 :插入通知单明细（作业组长发起见证的时候需要后台发起）", notes = "发点信息接口手工同步4 管道发点接口 :插入通知单明细（作业组长发起见证的时候需要后台发起）")
	@RequestMapping(value = "/sendPointNoticeDetail", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> sendPointNoticeDetail(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "workstepids", value = "工序id数组") @RequestParam(value="rollingPlanId",required=true) Integer[] workstepids) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		JsonResult<Boolean> result = new JsonResult<Boolean>();

		List<EnpowerRequestSendNotice> noticeList = new ArrayList<EnpowerRequestSendNotice>();
		try{

//			noticeList = rollingPlanApiService.findEnpowerSendPointNoticeDetailByWorkStepIds(workstepids,"TESTconsteamName",null);
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
		bn.setXml("插入通知单明细");
		bn.setDate_col("NOTE_DATE,WITN_DATE");
		String data = CommonUtility.toJson(noticeList);
		bn.set_value(data);
		logger.info(bn.toString());
		log.info("[EnpowerRequestSendNotice] Info: " + constantVar.getEnpowerHost() + enpowerupdateData + " : " +  bn.toString());

		try{
			

			updateEnpowerAndSetResult(result, bn);
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		return result;
	}	
	
	/**
	 *发点信息接口:推送接口名: 插入设备清单
	 */
	@ResponseBody
	@ApiOperation(value = "发点信息接口手工同步4 管道发点接口 :插入设备清单（作业组长发起见证的时候需要后台发起）", notes = "发点信息接口手工同步4 管道发点接口 :插入设备清单（作业组长发起见证的时候需要后台发起）")
	@RequestMapping(value = "/sendPointEquip", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> sendPointEquip(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "rollingPlanId", value = "计划id") @RequestParam(value="rollingPlanId",required=true) Integer rollingPlanId) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		List<EnpowerRequestSendEquipment> equipList = new ArrayList<EnpowerRequestSendEquipment>();
		try{
			equipList = rollingPlanApiService.findEnpowerSendPointEquipByRollingPlanId(rollingPlanId);
			
			logger.info("发点信息接口:推送接口名: 插入设备清单="+CommonUtility.toJson(equipList));
		}catch(Exception e){
			e.printStackTrace();
			result.setCode("-2001");
			result.setMessage(e.getMessage());
		}

		EnpowerRequestXmlName bn = new EnpowerRequestXmlName();
		bn.setXml("插入设备清单");
		bn.setDate_col("FOUND_DATE");
		String data = CommonUtility.toJson(equipList);
		bn.set_value(data);
		logger.info(bn.toString());
		log.info("[EnpowerRequestSendEquipment] Info: " + constantVar.getEnpowerHost() + enpowerupdateData + " : " +  bn.toString());

		try{

			updateEnpowerAndSetResult(result, bn);
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
		return result;
	}


	/**
	 *消点信息接口
	 */
	@ResponseBody
	@ApiOperation(value = "见证员见证结束时调用Enpower消点接口", notes = "见证员见证结束时调用Enpower消点接口")
	@RequestMapping(value = "/disappearPoint", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> disappearPoint(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "witnessIds", value = "见证id数组") @RequestParam(value="witnessIds",required=true) Integer[] witnessIds) {
		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{

			result = executePushToEnpowerServiceImpl.disappearPoint(null,witnessIds);
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
		return result;
	}
	
	
	/**
	 * 材料回填接口，推送接口名: 修改材料清单
	 */
	@ResponseBody
	@ApiOperation(value = "材料回填接口，推送接口名: 修改材料清单", notes = "材料回填接口，推送接口名: 修改材料清单")
	@RequestMapping(value = "/materialBackfill", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public JsonResult<Boolean> materialBackfill(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader(required=true, defaultValue="enpower") String domain,
			@ApiParam(required = true, name = "rollingPlanId", value = "计划id") @RequestParam(value="rollingPlanId",required=true) Integer rollingPlanId) {

		JsonResult<Boolean> result = new JsonResult<Boolean>();
		try{
			RollingPlan plan = this.rollingPlanService.findById(rollingPlanId);
			boolean weldFlag = plan.getWeldno()!=null && !"".equals(plan.getWeldno().trim());
			if(weldFlag==false){
				result = executePushToEnpowerServiceImpl.materialBackfill(null,rollingPlanId);
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			result.setCode(ExceptionCode.E4);
			result.setMessage("操作异常:"+e.getMessage());
			result.setResponseResult(false);
			return result;
		}
		
		return result;
	}
	
	
	public static void main(String[] args) {

		String check = CommonUtility.MD5Digest("domain2017" + "timestamp2017" + "hd5kpiclearallscore");
		System.out.println("check="+check);
	}
	
}
