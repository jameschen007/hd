package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;
@Data
public class EnpowerResponseMaterialCancelSearch  implements Serializable{
	private static final long serialVersionUID = 1L;
	/**退库ID*/
	private String ID ;
	/**入库ID*/
	private String INSTK_ID ;
	/**库存ID*/
	private String STK_ID ;
	/**出库单号*/
	private String ISSNO ;
	/**退库日期*/
	private String RETURNDATE ;
	/**物项编码*/
	private String MATE_CODE ;
	/**物项名称*/
	private String MATE_DESCR ;
	/**退库单位*/
	private String RETURN_DEPT ;
	/**退库员*/
	private String RETURNER ;
	/**退库数量*/
	private String ISSQTY ;
	/**核实数量*/
	private String QTY_RELEASED ;
	/**单价*/
	private String PRICE ;
	/**退库金额*/
	private String CK_JE ;
	/**核实金额*/
	private String CK_JE_HS ;
	/**需求计划号*/
	private String MRP_NO ;
	/**质保号*/
	private String GUARANTEENO ;
	/**会计科目*/
	private String CST_CODE ;
	/**退库类型*/
	private String ISSTYPE ;
	/**状态*/
	private String STATUS ;
	/**规格型号*/
	private String SPEC ;
	/**材质*/
	private String TEXTURE ;
	/**标准*/
	private String STAND_NO ;
	/**核安全等级*/
	private String NS_CLASS ;
	/**质保等级*/
	private String QA_CLASS ;
	/**位号*/
	private String FUNC_NO ;
	/**炉批号*/
	private String LOTNO ;
	/**批号*/
	private String HEAT_NO ;
	/**备注*/
	private String RETURNREMARK ;
	/**项目代号*/
	private String PROJ_CODE ;
	/**公司代号*/
	private String COMP_CODE ;
	/**存储仓库*/
	private String WAREH_CODE ;
	/**货位*/
	private String WAREH_PLACE ;
	/**	单位**/
	private String UNIT;
}
