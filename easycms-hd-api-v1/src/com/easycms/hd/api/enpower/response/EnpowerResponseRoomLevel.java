package com.easycms.hd.api.enpower.response;

import com.easycms.common.logic.context.constant.EnpConstant;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
/**
 * APP拿到的Enpower中的数据 区域房间标高信息查询
 * @author ul-webdev
 */
@Data
public class EnpowerResponseRoomLevel {

	/**
	 * 区域
	 */
	@SerializedName(value = "AREA_CODE")
	private String areaCode ;

	/**
	 * 房间号
	 */
	@SerializedName(value = "CCODE")
	private String room ;

	/**
	 * 标高
	 */
	
	@SerializedName(value = "LEVELES")
	private String level ;
	
	
	@SerializedName(value = "PROJ_CODE")
	private String projCode = EnpConstant.PROJ_CODE;
	@SerializedName(value = "COMP_CODE")
	private String compCode = EnpConstant.COMP_CODE;
	
//	[{"AREA_CODE":"A02",
//		"CCODE":"NA105",
//		"LEVELES":"-8.8",
//		"PROJ_CODE":"K2K3",
//		"COMP_CODE":"050812"},
}
