package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;
@Data
public class EnpowerResponseMaterialTodayStore  implements Serializable {
	
	private static final long serialVersionUID = 7068268681475989131L;

	/**物项编码*/
	private String MATE_CODE ;
	/**物项名称*/
	private String MATE_DESCR ;
	/**质保编号*/
	private String GUARANTEENO ;
	/**入库数量*/
	private String QTY ;
	/**库存数量*/
	private String STK_QTY ;
	/**材质*/
	private String TEXTURE ;
	/**规格型号*/
	private String SPEC ;
	/**炉批号*/
	private String HEAT_NO ;
	/**批号*/
	private String LOTNO ;
	/**核安全等级*/
	private String NS_CLASS ;
	/**质保等级*/
	private String QA_CLASS ;
	/**船次件号*/
	private String DELIV_LOT ;
	/**项目代号*/
	private String PROJ_CODE ;
	/**公司代号*/
	private String COMP_CODE ;
	/**库存ID*/
	private String ID ;
	/**存储仓库*/
	private String STORAGES_CODE ;
	/**货位*/
	private String VESSEL_NO ;
	/**英文名称*/
	private String MATE_DESCR_EN ;
	/**标准*/
	private String STAND_NO ;
	/**位号*/
	private String FUNC_NO ;
	/**备注*/
	private String REMARK ;
	/**核实日期*/
	private String CNFMED_DATE ;
	/**单位*/
	private String UNIT ;

	private String TOTAL_COUNT;
	private String ROW_NUM;
	
	
	

	
	
}
