package com.easycms.hd.api.enpower.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EnpowerResponseUnitWorkshop {
	
	//"CODE":"XX"
	@SerializedName(value = "CODE")
	private String code;
	
	//"PROJ_CODE":"K2K3"
	@SerializedName(value = "PROJ_CODE")
	private String projCode;

}
