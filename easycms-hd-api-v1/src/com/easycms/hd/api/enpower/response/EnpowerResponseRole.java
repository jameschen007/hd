package com.easycms.hd.api.enpower.response;

import java.util.Set;

import lombok.Data;
@Data
public class EnpowerResponseRole {
//  "岗位ID":"c96110c8388f4ba6be5b4b956530a127",
	private String roleId;
//  "岗位编码":"1014",
	private String roleNo;
//  "岗位名称":"管道主任工程师",
	private String roleName;
//"组织机构ID":"11f5d0c7488e442889d6fc0759ea79f5",
	private String departmentId;
	
	//下属的所有角色
	private Set<EnpowerResponseRole> children;
	
	@Override
	public boolean equals(Object obj) {
        if (obj instanceof EnpowerResponseRole) {
        	EnpowerResponseRole role = (EnpowerResponseRole) obj;
            return (roleId.equals(role.getRoleId()));
        }
        return super.equals(obj);
    }
	
	@Override 
    public int hashCode() {
        return roleId.hashCode();
    }
}
