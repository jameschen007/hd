package com.easycms.hd.api.enpower.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EnpowerResponseDepartment implements Serializable {
	private static final long serialVersionUID = -2543063120503403462L;
//    "ID":"6ff7c8f04eb747cca938afecf269ff82",
	@SerializedName(value = "ID")
	private String id;
//    "ORG_CODE":"0502",
	@SerializedName(value = "ORG_CODE")
	private String orgCode;
//    "ORG_NAME":"器材分公司",
	@SerializedName(value = "ORG_NAME")
	private String orgName;
//    "PARENT_ID":"b302cee5542543bf94935701c2b42439",
	@SerializedName(value = "PARENT_ID")
	private String parentId;
//    "REMARK":null,
	@SerializedName(value = "REMARK")
	private String remark;
//    "OTHER1":"COMPANY",
	@SerializedName(value = "OTHER1")
	private String other1;
//    "OTHER2":"NOZLJG",
	@SerializedName(value = "OTHER2")
	private String other2;
//    "OTHER3":null,
	@SerializedName(value = "OTHER3")
	private String other3;
//    "OTHER4":null,
	@SerializedName(value = "OTHER4")
	private String other4;
//    "OTHER5":null,
	@SerializedName(value = "OTHER5")
	private String other5;
//    "OTHER6":null,
	@SerializedName(value = "OTHER6")
	private String other6;
//    "OTHER7":null,
	@SerializedName(value = "OTHER7")
	private String other7;
//    "OTHER8":null,
	@SerializedName(value = "OTHER8")
	private String other8;
//    "OTHER9":null,
	@SerializedName(value = "OTHER9")
	private String other9;
//    "OTHER10":"N",
	@SerializedName(value = "OTHER10")
	private String other10;
//    "ORG_APPCODE":null,
	@SerializedName(value = "ORG_APPCODE")
	private String orgAppcode;
//    "END_DATE":null,
//	@SerializedName(value = "END_DATE")
//	private Date endDate;
//    "DELETE_YN":null,
	@SerializedName(value = "DELETE_YN")
	private String deleteYn;
//    "TOTAL_COUNT":159,
	@SerializedName(value = "TOTAL_COUNT")
	private Integer totalCount;
//    "ROW_NUM":1
	@SerializedName(value = "ROW_NUM")
	private Integer rowNum;
	
//下属部门
	private List<EnpowerResponseDepartment> children;
	
//部门下的所有角色
	private Set<EnpowerResponseRole> roles;
}
