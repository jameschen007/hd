package com.easycms.hd.api.enpower.response;

import java.io.Serializable;

import lombok.Data;
/**
 *物项入库(未核实)查询(汇总)接口 入库查询　enpower属性返回
 * @author ul-webdev
 *
 */
@Data
public class EnpowerResponseMaterialStoreSearch implements Serializable {
	
	private static final long serialVersionUID = 7068268681475989131L;
	
//	mate_code：物项编码
//	mate_descr：物项名称
//	Guaranteeno：质保编号
//	stk_qty：数量
//	Texture：材质
//	Spec：规格型号
//	Heat_No：炉批号
//	Lotno：批号
//	ns_class：核安全等级
//	qa_class：质保等级
//	Deliv_Lot：船次件号
//	proj_code：项目代号
//	comp_code：公司代号

	//物项编码    
	private String  MATE_CODE   ;
	//物项名称    
	private String  MATE_DESCR  ;
	//质保编号    
	private String  GUARANTEENO ;
	//数量  
	private String  QTY ;
	//材质  
	private String  TEXTURE ;
	//规格型号    
	private String  SPEC    ;
	//炉批号 
	private String  HEAT_NO ;
	//批号  
	private String  LOTNO   ;
	//核安全等级   
	private String  NS_CLASS    ;
	//质保等级    
	private String  QA_CLASS    ;
	//船次件号    
	private String  DELIV_LOT   ;
	//项目代号    
	private String  PROJ_CODE   ;
	//公司代号    
	private String  COMP_CODE   ;
	private String  ID   ;
	private String  STAND_NO   ;
	//入库类型
	private String  INSTK_TYPE   ;
	/**CPO_NO	合同号*/
	private String  CPO_NO   ;
	/**PRICE	价格*/
	private String  PRICE   ;
	/**	合同明细ID**/
	private String CPO_DETAIL_ID;
	/**	位号**/
	private String FUNC_NO;
	/**	单位**/
	private String UNIT;
}
