package com.easycms.hd.api.enpower.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.hd.api.enpower.dao.EnpowerLogDao;
import com.easycms.hd.api.enpower.domain.EnpowerLog;

@Service("enpowerLogServiceImpl")
@Transactional
public class EnpowerLogServiceImpl {
	@Autowired
	private EnpowerLogDao enpowerLogDao;

	public boolean insert(EnpowerLog enpowerUser) {
		return null != enpowerLogDao.save(enpowerUser);
	}

}
