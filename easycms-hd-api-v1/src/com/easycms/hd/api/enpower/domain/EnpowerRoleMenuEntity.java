package com.easycms.hd.api.enpower.domain;

import java.util.List;

import lombok.Data;

@Data
public class EnpowerRoleMenuEntity {
	private List<String> id;
	private List<String> name;
	private List<String> menus;
}
