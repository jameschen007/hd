package com.easycms.hd.api.enpower.domain;

public class EnpowerConstantVar {

	//enp接口　修改入库信息(按质保号)
	public static String alreadyStore = "已入库";
	/**MATE_HOLDER	物权属性(S/O)	S*/
	public static String MATE_HOLDER = "S";
	/**修改物项出库信息:已核实*/
	public static String alreadyCheck="已核实";
	/**STATUS--状态（已关闭）*/
	public static String CLOSED="已关闭";
	
	/**ORIGIN--来源（APP）*/
	public static String ENP_ORIGIN="APP";


	/**
	 * 默认调用　enpower接口常量1
	 */
	public final static int enpowerPageIndex=1;
	/**
	 * 默认调用　enpower接口常量10000
	 */
	public final static int enpowerPageSize=10000;
}
