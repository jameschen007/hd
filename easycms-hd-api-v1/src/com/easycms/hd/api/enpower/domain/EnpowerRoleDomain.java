package com.easycms.hd.api.enpower.domain;

import java.util.List;

import lombok.Data;

@Data
public class EnpowerRoleDomain {
	private EnpowerRoleEntity GDJH;	//管道计划
	private EnpowerRoleEntity TFJH;	//通风计划
	private EnpowerRoleEntity JXJH;	//机械计划
	private EnpowerRoleEntity DQJH;	//电气计划
	private EnpowerRoleEntity YBJH;	//仪表计划
	private EnpowerRoleEntity ZXT;	//主系统
	private EnpowerRoleEntity BWJH;	//保温计划
	private EnpowerRoleEntity WMSG;	//文明施工
	private EnpowerRoleEntity ZLGL;	//质量管理
	private EnpowerRoleEntity WZGL;	//物资管理
	private EnpowerRoleEntity TSJH;	//调试计划
	private EnpowerRoleEntity QC;	//QC组织架构
	private EnpowerRoleEntity HG;	//焊工组织架构
	private List<EnpowerRoleEntity> ALL;	//所有组织架构
}

