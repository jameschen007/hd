package com.easycms.hd.api.enpower.domain;

import java.util.List;

import lombok.Data;

@Data
public class EnpowerRoleEntity {
	private String id;
	private String name;
	private List<EnpowerRoleEntity> subRole;
}
