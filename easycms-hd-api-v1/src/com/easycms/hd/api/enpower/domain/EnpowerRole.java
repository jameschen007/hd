package com.easycms.hd.api.enpower.domain;

import lombok.Data;

@Data
public class EnpowerRole {
//  "岗位ID":"c96110c8388f4ba6be5b4b956530a127",
	private String roleId;
//    "岗位编码":"1014",
	private String roleNo;
//    "岗位名称":"管道主任工程师",
	private String roleName;
}
