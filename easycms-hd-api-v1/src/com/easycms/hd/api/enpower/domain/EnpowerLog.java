package com.easycms.hd.api.enpower.domain;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "enpower_log")
@Data
public class EnpowerLog {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "with_time")
	private BigDecimal withTime;
	
	@Column(name = "xml_name")
	private String xmlName;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "is_success")
	private String isSuccess;
	
	@Column(name = "error_msg")
	private String errorMsg;
	
	@Column(name = "return_result")
	private String returnResult;
	@Column(name = "data_info")
	private String dataInfo;
	@Column(name = "on_off")
	private String onOff;
	@Column(name = "update_state")
	private boolean updateState;
	@Transient
	public static List<String>  withoutSaving= Arrays.asList(new String[]{"安全编码信息查询"});
	
}
