package com.easycms.hd.api.enpower.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class EnpowerUserEntity {
	/**
	 * 用户的登录ID号，也是用户的唯一标识符，需要提前验证，重要数据
	 */
	private String loginId;
	/**
	 * 用户的显示名
	 */
	private String userName;
	/**
	 * 系统中用户的默认登录密码
	 */
	private String defaultPassword;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 用户的角色名，重要数据
	 */
	private String roleName;
	/**
	 * 用户角色代号，用于用户角色的识别，重要数据
	 */
	private String roleType;
	/**
	 * 生日
	 */
	private Date birthday;
	/**
	 * 直接下属的信息
	 */
	private List<EnpowerUserEntity> underling = new ArrayList<EnpowerUserEntity>();;

}
