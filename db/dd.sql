/*
Navicat MariaDB Data Transfer

Source Server         : 192.168.99.166
Source Server Version : 50550
Source Host           : 192.168.99.166:3306
Source Database       : easycms_v2

Target Server Type    : MariaDB
Target Server Version : 50550
File Encoding         : 65001

Date: 2016-12-23 11:13:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dd
-- ----------------------------
DROP TABLE IF EXISTS `dd`;
CREATE TABLE `dd` (
  `ss` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS=1;
