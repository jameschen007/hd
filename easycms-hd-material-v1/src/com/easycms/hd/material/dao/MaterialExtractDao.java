package com.easycms.hd.material.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.material.domain.MaterialExtract;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface MaterialExtractDao extends Repository<MaterialExtract,Integer>,BasicDao<MaterialExtract>{
	@Query("FROM MaterialExtract me WHERE me.departmentId = ?1 ORDER BY me.createOn desc")
	Page<MaterialExtract> findByPage(Integer departmentId, Pageable pageable);
	
	@Modifying
	@Query("DELETE FROM MaterialExtract me WHERE me.id IN (?1)")
	int deleteByIds(Integer[] ids);

	@Query("FROM MaterialExtract me WHERE me.warrantyNo = ?1")
	List<MaterialExtract> findByWarrantyNo(String warrantyNo);
	
	@Query("SELECT DISTINCT me.departmentId FROM MaterialExtract me")
	List<Integer> findAllDepartmentId();
}
