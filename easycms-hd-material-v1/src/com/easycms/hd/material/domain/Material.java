package com.easycms.hd.material.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "material")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class Material extends BasicForm implements Serializable {
	private static final long serialVersionUID = -6182873855311689901L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "warehouse")
	private String warehouse;
	@Column(name = "location")
	private String location;
	@Column(name = "number")
	private Integer number;
	@Column(name = "warranty_no")
	private String warrantyNo;
	@Column(name = "name_en")
	private String nameEn;
	@Column(name = "specification_no")
	private String specificationNo;
	@Column(name = "material")
	private String material;
	@Column(name = "security_level")
	private String securityLevel;
	@Column(name = "warranty_level")
	private String warrantyLevel;
	@Column(name = "standard")
	private String standard;
	@Column(name = "position_no")
	private String positionNo;
	@Column(name = "furnace_no")
	private String furnaceNo;
	@Column(name = "warranty_count")
	private Integer warrantyCount;
	@Column(name = "remark")
	private String remark;
	@Column(name = "ship_no")
	private String shipNo;
	@Column(name = "exit")
	private Integer exit;
	@Column(name = "price")
	private BigDecimal price;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
}
