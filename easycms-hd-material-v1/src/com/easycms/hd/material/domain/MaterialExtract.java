package com.easycms.hd.material.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "material_extract")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class MaterialExtract extends BasicForm implements Serializable {
	private static final long serialVersionUID = 2790642427180764859L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "material_id")
	private Integer materialId;
	@Column(name = "warranty_no")
	private String warrantyNo;
	@Column(name = "extract_count")
	private Integer extractCount;	
	@Column(name = "confirm_count")
	private Integer confirmCount;
	@Column(name = "keeper")
	private String keeper;
	@Column(name = "invoice_date")
	private Date invoiceDate;
	@Column(name = "sign_date")
	private Date signDate;
	@Column(name = "department_id")
	private Integer departmentId;
	@Column(name = "receiver")
	private String receiver;
	@Column(name = "extract_no")
	private String extractNo;
	@Column(name = "plan_no")
	private String planNo;
	@Column(name = "remark")
	private String remark;
	@Column(name = "accounting")
	private String accounting;
	
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
}
