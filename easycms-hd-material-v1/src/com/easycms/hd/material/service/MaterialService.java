package com.easycms.hd.material.service;

import com.easycms.core.util.Page;
import com.easycms.hd.material.domain.Material;

public interface MaterialService {
	Material add(Material material);

	boolean batchRemove(Integer[] ids);
	
	boolean batchMarkRemove(Integer[] ids);

	Page<Material> findByPage(Page<Material> page);

	Material findById(Integer id);
	
	Material findByWarrantyNo(String warrantyNo);
}
