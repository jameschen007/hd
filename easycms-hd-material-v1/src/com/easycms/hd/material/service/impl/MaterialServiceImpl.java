package com.easycms.hd.material.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.material.dao.MaterialDao;
import com.easycms.hd.material.domain.Material;
import com.easycms.hd.material.service.MaterialService;

@Service("materialService")
public class MaterialServiceImpl implements MaterialService {

	@Autowired
	private MaterialDao materialDao;

	@Override
	public Material add(Material material) {
		return materialDao.save(material);
	}

	@Override
	public Page<Material> findByPage(Page<Material> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Material> springPage = materialDao.findByPage(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}


	@Override
	public Material findById(Integer id) {
		return materialDao.findById(id);
	}

	@Override
	public boolean batchRemove(Integer[] ids) {
		if (0 != materialDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public boolean batchMarkRemove(Integer[] ids) {
		if (0 != materialDao.markDeleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public Material findByWarrantyNo(String warrantyNo) {
		if (CommonUtility.isNonEmpty(warrantyNo)) {
			return materialDao.findByWarrantyNo(warrantyNo);
		}
		return null;
	}
}
