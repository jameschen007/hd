package com.easycms.hd.learning.directive;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.basic.BaseDirective;
import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearningFold;
import com.easycms.hd.learning.service.LearningFoldService;
import lombok.extern.slf4j.Slf4j;

/**
 * 获取列表指令 
 * 
 * @author fangwei: 
 * @areate Date:2017-12-22
 */
@Slf4j
public class LearningFoldDirective extends BaseDirective<LearningFold>  {
	
	@Autowired
	private LearningFoldService learningFoldService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected LearningFold field(Map params, Map<String, Object> envParams) {
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
		log.debug("[id] ==> " + id);
		// 查询ID信息
		if (id != null) {
			LearningFold learningFold = learningFoldService.findById(id);
			return learningFold;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearningFold> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<LearningFold> page,
			Map<String, Object> envParams) {
		String section = FreemarkerTemplateUtility.getStringValueFromParams(params, "section");
		String module = FreemarkerTemplateUtility.getStringValueFromParams(params, "module");
		
		log.debug("Section : " + section + " ,Module: " + module);
		
		if (CommonUtility.isNonEmpty(module)) {
			page = learningFoldService.findByModuleAndSection(module, section, page);
		} else {
			page = learningFoldService.findBySection(section, page);
		}
		return page.getDatas();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List<LearningFold> tree(Map params, Map<String, Object> envParams) {
		return null;
	}

}
