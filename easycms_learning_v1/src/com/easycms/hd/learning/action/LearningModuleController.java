package com.easycms.hd.learning.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.learning.domain.LearningFold;
import com.easycms.hd.mobile.domain.Modules;
import com.easycms.hd.mobile.service.ModulesService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/learning/module")
@Slf4j
public class LearningModuleController {

	@Autowired
	private ModulesService modulesService;
	
	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") BasicForm form) {
		log.debug("==> Show learning module list.");
		String returnString = ForwardUtility.forwardAdminView("/learning/list_learning_module");
		
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") BasicForm form) {
		log.debug("==> Show learning module data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_json_learning_module");
		return returnString;
	}
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") LearningFold form) {
		if (log.isDebugEnabled()) {
			log.debug("==> Show add new Modules UI.");
		}
		return ForwardUtility.forwardAdminView("/learning/modal_learning_module_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid Modules form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start add Modules.");
			log.debug("[Modules] ==> " + CommonUtility.toJson(form));
		}
		if(!CommonUtility.isNonEmpty(form.getType())){
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		//验证type是否已经存在
		Modules module = modulesService.findByType(form.getType());
		if (null != module) {
			log.error("==> 模块类型[ "+form.getType()+" ]已经存在,不可重复添加！ ");
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		form.setCreateBy(user.getName());
		form.setCreateOn(new Date());
		form.setStatus("ACTIVE");
		if (modulesService.add(form) != null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
		}
		log.debug("==> End add Modules.");
		return;
	}
	
	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") Modules form) {
		log.debug("==> Start delete Modules.");

		Integer[] ids = form.getIds();
		log.debug("==> To delete Modules ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			modulesService.batchDelete(ids);
		}
		
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete Modules.");
	}
	
	/**
	 * 修改模块
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") Modules form) {
		return ForwardUtility.forwardAdminView("/learning/modal_learning_module_edit");
	}

	/**
	 * 保存模块修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid Modules form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start edit Modules.");
			log.debug("[Modules] ==> " + CommonUtility.toJson(form));
		}
		
		Modules module = modulesService.findById(form.getId());
		if (null == module) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		module.setName(form.getName());
//		module.setType(form.getType());
		module.setDescription(form.getDescription());
		module.setUpdateBy(user.getName());
		module.setUpdateOn(new Date());
		
		if (modulesService.add(module) != null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		log.debug("==> End edit Modules.");
		return;
	}
	
	@RequestMapping(value = "", method = RequestMethod.PATCH)
	public void queryType(@RequestBody String typeBody,HttpServletRequest request,HttpServletResponse response){
		String type = typeBody.replaceAll("type=", "");
		log.debug("==> Start query Modules by Type[ " + type+" ]");
		if(!CommonUtility.isNonEmpty(type)){
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
			return;
		}
		Modules module = modulesService.findByType(type);
		if (null == module) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		log.debug("==> End query Modules by Type.");
		return;
	}
}
