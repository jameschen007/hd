package com.easycms.hd.learning.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycms.common.jod.Office2PdfUtil;
import com.easycms.common.jod.PDF2ImageUtil;
import com.easycms.common.jpush.JPushCategoryEnums;
import com.easycms.common.jpush.JPushExtra;
import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.DateUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.common.video.ConvertVideo;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.learning.enums.ConventerFlagEnum;
import com.easycms.hd.learning.enums.LearningTypeEnum;
import com.easycms.hd.learning.form.ConvertForm;
import com.easycms.hd.learning.service.LearningFileService;
import com.easycms.hd.learning.service.LearningService;
//import com.easycms.hd.push.service.JPushService;
import com.easycms.hd.response.JsonResult;
import com.easycms.management.user.domain.User;
import com.easycms.management.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/learning/management")
@Slf4j
public class LearningManagementController {

	@Autowired
	private LearningService learningService;
	@Autowired
	private UserService userService;
//	@Autowired
//	private JPushService jPushService;
	
	@Autowired
	private LearningFileService learningFileService;
	
	@Autowired
	private ThreadPoolTaskExecutor poolTaskExecutor; 

	@Value("#{APP_SETTING['file_base_path']}")
	private String basePath;
	
	@Value("#{APP_SETTING['file_learning_path']}")
	private String fileLearningPath;
	
	@Value("#{APP_SETTING['office_home']}")
	private String officeHome;
	
	@Value("#{APP_SETTING['office_suffix']}")
	private String officeSuffix;
	
	@Value("#{APP_SETTING['ffmpeg_path']}")
	private String ffmpegPath;
	
	@Value("#{APP_SETTING['video_suffix']}")
	private String videoSuffix;
	
	@Value("#{APP_SETTING['file_learning_convert_path']}")
	private String fileLearningConvertPath;
	
	@Value("#{APP_SETTING['file_learning_cover_path']}")
	private String fileLearningCoverPath;
	
	Queue<ConvertForm> convertFormQueue = new LinkedBlockingQueue<ConvertForm>();
	
	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		log.debug("==> Show learning list.");
		String returnString = ForwardUtility.forwardAdminView("/learning/list_learning");
		
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		log.debug("==> Show learning data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_json_learning");
		return returnString;
	}
	
	/**
	 * 添加课程UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		if (log.isDebugEnabled()) {
			log.debug("==> Show add new learning UI.");
		}
		
		request.setAttribute("type", LearningTypeEnum.getList());
		return ForwardUtility.forwardAdminView("/learning/modal_learning_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<String> add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid Learning form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start add learning.");
			log.debug("[Learning] ==> " + CommonUtility.toJson(form));
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		JsonResult<String> result = new JsonResult<String>();
		log.info(CommonUtility.toJson(form));

		if (null != form) {
			Learning learning = new Learning();
			learning.setFold(form.getFold());
			learning.setTitle(form.getTitle());
			learning.setStatus("ACTIVE");
			learning.setDescription(form.getDescription());
			learning.setCreateBy(user.getId());
			learning.setCreateOn(new Date());
			learning.setType(form.getType());
			learning.setSeq(form.getSeq());
			if (learningService.add(learning) != null) {
				if (null != form.getFileIds() && !form.getFileIds().isEmpty()) {
					learningFileService.batchUpdateLearning(form.getFileIds().toArray(new Integer[] {}), learning.getId());
				}
				//向系统所有人发送推送消息

				List<User> targetUsers = userService.findAll();//.findUserVariableRole("roleType","an_quan_guan_li");
				//给所有人员发送一条推送
				String message = user.getRealname() + " 上传了新课件";
//				JPushExtra extra = new JPushExtra();
//				extra.setCategory(JPushCategoryEnums.LEARN_NEW_FILE.name());
//				extra.setPayload(JPushCategoryEnums.LEARN_NEW_FILE.name());
//				jPushService.pushByUser(extra, message, "有新课件上传", targetUsers);
				
				
			} else {
				result.setCode("-1000");
		    	result.setMessage("保存课程失败");
		    	return result;
			}
			log.debug("==> End save new learning.");
			result.setCode("1000");
	    	result.setMessage("保存课程成功。");
	    } else {
	    	result.setCode("-1000");
	    	result.setMessage("保存课程失败。");
	    }
		return result;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		log.debug("==> Start delete learning.");

		Integer[] ids = form.getIds();
		log.debug("==> To delete learning ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			learningService.batchDelete(ids);
		}
		
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete learning.");
	}
	
	/**
	 * 修改课程
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		request.setAttribute("type", LearningTypeEnum.getList());
		return ForwardUtility.forwardAdminView("/learning/modal_learning_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<String> edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid Learning form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start edit learning.");
			log.debug("[Learning] ==> " + CommonUtility.toJson(form));
		}
		
		JsonResult<String> result = new JsonResult<String>();
		
		Learning learning = learningService.findById(form.getId());
		if (null == learning) {
			result.setCode("-1000");
	    	result.setMessage("修改课程失败");
	    	return result;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		learning.setUpdateBy(user.getId());
		learning.setUpdateOn(new Date());
		learning.setTitle(form.getTitle());
		learning.setDescription(form.getDescription());
		learning.setType(form.getType());
		learning.setSeq(form.getSeq());
		
		if (learningService.add(learning) != null) {
			if (null != form.getFileIds() && !form.getFileIds().isEmpty()) {
				learningFileService.batchUpdateLearning(form.getFileIds().toArray(new Integer[] {}), learning.getId());
			}
			result.setCode("1000");
	    	result.setMessage("修改课程成功。");
		} else {
			result.setCode("-1000");
	    	result.setMessage("修改课程失败");
		}
		log.debug("==> End edit learning.");
		return result;
	}

	
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<String> learningFileUpload(HttpServletRequest request, HttpServletResponse response){
		String filePath = basePath + fileLearningPath;
		JsonResult<String> result = new JsonResult<String>();
		
		List<FileInfo> fileInfos = UploadUtil.upload(filePath, "utf-8", true, request);
        if (fileInfos != null && fileInfos.size() > 0) {
        	List<Integer> fileIds = new ArrayList<Integer>();
            for (FileInfo fileInfo : fileInfos) {
            	LearningFile file = new LearningFile();
                file.setPath(fileInfo.getNewFilename());
                file.setTime(new Date());
                String fileName = fileInfo.getFilename();
                if (fileName.length() > 90) {
                	fileName = fileName.substring(0, 90);
                }
                
                String suffix = null;
                if (fileInfo.getNewFilename().contains(".")) {
                	suffix = fileInfo.getNewFilename().substring(fileInfo.getNewFilename().lastIndexOf(".") + 1, fileInfo.getNewFilename().length());
                }
                if (CommonUtility.isNonEmpty(suffix)) {
                	suffix = suffix.toLowerCase();
                }
                file.setSuffix(suffix);
                file.setFileName(fileName);
                
                List<String> officeSuffixList = Arrays.asList(officeSuffix.split(","));
                List<String> videoSuffixList = Arrays.asList(videoSuffix.split(","));
                
                if ("mp4".equals(suffix) || "pdf".equals(suffix)) {
                	String coverName = CommonUtility.generateUUID() + ".jpg";
                	if ("mp4".equals(suffix)) {
	                	if (ConvertVideo.processImg(ffmpegPath, fileInfo.getAbsolutePath(), filePath + fileLearningCoverPath + coverName)) {
	                		file.setCoverPath(coverName);
	                	}
                	} else {
                		if (PDF2ImageUtil.previewFirstImage(fileInfo.getAbsolutePath(), filePath + fileLearningCoverPath + coverName)) {
                			file.setCoverPath(coverName);
                		}
                	}
                } else {
                	if (officeSuffixList.contains(suffix) || videoSuffixList.contains(suffix)) {
	                	file.setConventerFlag(ConventerFlagEnum.START.name());
	                }
                }
                log.debug("upload file success : " + file.toString());
                learningFileService.add(file);
                if (ConventerFlagEnum.START.name().equals(file.getConventerFlag())) {
                	ConvertForm convertForm = new ConvertForm();
                	convertForm.setId(file.getId());
                	convertForm.setSuffix(file.getSuffix());
                	convertForm.setSourceFilePath(fileInfo.getAbsolutePath());
                	convertForm.setBasePath(filePath);
                	
                	boolean offerResult = convertFormQueue.offer(convertForm);
    				if (offerResult && (poolTaskExecutor.getPoolSize() < poolTaskExecutor.getMaxPoolSize())){
    					Thread convertFormThread = new Thread(new ConvertFormThread());
    					poolTaskExecutor.execute(convertFormThread);
    				}
                }
                fileIds.add(file.getId());
            }
            result.setResponseResult(fileIds.stream().map(s -> s+"").reduce((s1, s2) -> s1 + "," + s2).get());
            return result;
        }
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/file/{id}", method = RequestMethod.POST)
	public JsonResult<String> learningFileRemove(HttpServletRequest request, HttpServletResponse response
			, @PathVariable("id") Integer id){
		String filePath = basePath + fileLearningPath;
		JsonResult<String> result = new JsonResult<String>();
		
		if (id != null) {
			LearningFile learningFile = learningFileService.findById(id);
			if (null != learningFile) {
				FileUtils.remove(filePath + learningFile.getPath());
				if (CommonUtility.isNonEmpty(learningFile.getConventerPath())) {
					FileUtils.remove(filePath + fileLearningConvertPath + learningFile.getConventerPath());
				}
				if (CommonUtility.isNonEmpty(learningFile.getCoverPath())) {
					FileUtils.remove(filePath + fileLearningCoverPath + learningFile.getCoverPath());
				}
				Integer[] ids = new Integer[] {id};
				learningFileService.batchRemove(ids);
				result.setMessage("文件删除成功");
			} else {
				result.setCode("-1000");
				result.setMessage("文件没有找到，请重新刷新页面。");
			}
		} else {
			result.setCode("-1000");
			result.setMessage("ID不能为空，请重新刷新页面。");
		}
		
		return result;
	}
	
	private class ConvertFormThread implements Runnable {
        public void run() {
			while(!convertFormQueue.isEmpty()){
				ConvertForm convertForm = convertFormQueue.poll();
				if (null != convertForm){
					LearningFile file = learningFileService.findById(convertForm.getId());
					
					if (null != file) {
						StopWatch stopWatch = new StopWatch();
						if (officeSuffix.contains(convertForm.getSuffix())) {
		                	log.info("当前上传的文件为office文件，即将进行PDF转换");
		                	String convertName = CommonUtility.generateUUID() + ".pdf";
		                	String coverName = CommonUtility.generateUUID() + ".jpg";
		                	
		                	stopWatch.start();
		                	boolean officeConventerResult = Office2PdfUtil.office2Pdf(officeHome, convertForm.getSourceFilePath(), convertForm.getBasePath() + fileLearningConvertPath + convertName);
		                	stopWatch.stop();
		            		file.setConventerTime(stopWatch.getTime());
		            		file.setConventerDescription(DateUtility.sumTime(stopWatch.getTime()));
		                	if (officeConventerResult) {
		                		file.setConventerPath(convertName);
		                		if (PDF2ImageUtil.previewFirstImage(convertForm.getBasePath() + fileLearningConvertPath + convertName, convertForm.getBasePath() + fileLearningCoverPath + coverName)) {
		                			file.setCoverPath(coverName);
		                		}
		                		file.setConventerFlag(ConventerFlagEnum.COMPLETED.name());
		                	} else {
		                		file.setConventerFlag(ConventerFlagEnum.FAILED.name());
		                	}
		                }
		                if (videoSuffix.contains(convertForm.getSuffix())) {
		                	log.info("当前上传的文件为视频文件，即将进行MP4转换");
		                	String convertName = CommonUtility.generateUUID() + ".mp4";
		                	String coverName = CommonUtility.generateUUID() + ".jpg";
		                	stopWatch.start();
		                	boolean videoConventerResult = ConvertVideo.executeCodecs(ffmpegPath, convertForm.getSourceFilePath(), convertForm.getBasePath() + fileLearningConvertPath + convertName, convertForm.getBasePath() + fileLearningCoverPath + coverName);
		                	stopWatch.stop();
		            		file.setConventerTime(stopWatch.getTime());
		            		file.setConventerDescription(DateUtility.sumTime(stopWatch.getTime()));
		                	if (videoConventerResult) {
		                		file.setConventerPath(convertName);
		                		file.setCoverPath(coverName);
		                		file.setConventerFlag(ConventerFlagEnum.COMPLETED.name());
		                	} else {
		                		file.setConventerFlag(ConventerFlagEnum.FAILED.name());
		                	}
		                }
		                //如果转换完成之后，发现原文件已经被删除，将删除所有转换了的文件。
		                LearningFile fileCheck = learningFileService.findById(convertForm.getId());
		                if (null != fileCheck) {
		                	fileCheck.setConventerDescription(file.getConventerDescription());
		                	fileCheck.setConventerFlag(file.getConventerFlag());
		                	fileCheck.setConventerPath(file.getConventerPath());
		                	fileCheck.setConventerTime(file.getConventerTime());
		                	fileCheck.setCoverPath(file.getCoverPath());
		                	
		                	learningFileService.add(fileCheck);
		                } else {
		                	if (CommonUtility.isNonEmpty(file.getConventerPath())) {
		    					FileUtils.remove(convertForm.getBasePath() + fileLearningConvertPath + file.getConventerPath());
		    				}
		    				if (CommonUtility.isNonEmpty(file.getCoverPath())) {
		    					FileUtils.remove(convertForm.getBasePath() + fileLearningCoverPath + file.getCoverPath());
		    				}
		                }
					}
				}
	    	}
		}
	}
}
