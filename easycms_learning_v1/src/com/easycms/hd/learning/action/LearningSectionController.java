package com.easycms.hd.learning.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearningSectionService;
import com.easycms.management.user.domain.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/hd/learning/section")
@Slf4j
public class LearningSectionController {
	
	@Autowired
	private LearningSectionService learningSectionService;

	/**
	 * 数据列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response) {
		log.debug("==> Show learning section list.");
		String returnString = ForwardUtility.forwardAdminView("/learning/list_learning_section");
		
		return returnString;
	}
	
	/**
	 * 数据片段
	 * @param request
	 * @param response
	 * @param pageNum
	 * @return
	 */
	@RequestMapping(value="",method=RequestMethod.POST)
	public String dataList(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") Learning form) {
		log.debug("==> Show learning data list.");
		form.setFilter(CommonUtility.toJson(form));
		log.debug("[easyuiForm] ==> " + CommonUtility.toJson(form));
		String returnString = ForwardUtility.forwardAdminView("/learning/data/data_json_learning_section");
		return returnString;
	}
	
	/**
	 * 添加课程UI
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUI(HttpServletRequest request, HttpServletResponse response
			,@ModelAttribute("form") LearningSection form) {
		if (log.isDebugEnabled()) {
			log.debug("==> Show add new learning UI.");
		}
		return ForwardUtility.forwardAdminView("/learning/modal_learning_section_add");
	}

	/**
	 * 保存新增
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void add(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("form") @Valid LearningSection form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start add learning.");
			log.debug("[Learning] ==> " + CommonUtility.toJson(form));
		}
		LearningSection learningSection = learningSectionService.findByType(form.getType());
		if (null != learningSection) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		form.setCreateBy(user.getId());
		form.setCreateOn(new Date());
		if (learningSectionService.add(form) != null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
		}
		log.debug("==> End add learning.");
		return;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public void batchDelete(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") LearningSection form) {
		log.debug("==> Start delete learning.");

		Integer[] ids = form.getIds();
		log.debug("==> To delete learning ["+ CommonUtility.toJson(ids) +"]");
		if (ids != null && ids.length > 0) {
			learningSectionService.batchDelete(ids);
		}
		
		HttpUtility.writeToClient(response, "true");
		log.debug("==> End delete learning.");
	}
	
	/**
	 * 修改课程
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public String editUI(HttpServletRequest request,
			HttpServletResponse response
			,@ModelAttribute("form") LearningSection form) {
		return ForwardUtility.forwardAdminView("/learning/modal_learning_section_edit");
	}

	/**
	 * 保存修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public void edit(@RequestParam Integer id, HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("form") @Valid LearningSection form,
			BindingResult errors) {
		if (log.isDebugEnabled()) {
			log.debug("==> Start edit learning.");
			log.debug("[Learning] ==> " + CommonUtility.toJson(form));
		}
		
		LearningSection learningSection = learningSectionService.findById(form.getId());
		if (null == learningSection) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
			return;
		}
		
		User user =  ContextUtil.getCurrentLoginUser();
		
		learningSection.setName(form.getName());
		learningSection.setUpdateBy(user.getId());
		learningSection.setUpdateOn(new Date());
		
		if (learningSectionService.add(learningSection) != null) {
			HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		} else {
			HttpUtility.writeToClient(response, CommonUtility.toJson(false));
		}
		HttpUtility.writeToClient(response, CommonUtility.toJson(true));
		log.debug("==> End edit learning.");
		return;
	}

}
