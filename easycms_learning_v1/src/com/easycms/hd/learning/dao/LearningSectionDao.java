package com.easycms.hd.learning.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.learning.domain.LearningSection;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface LearningSectionDao extends BasicDao<LearningSection> ,Repository<LearningSection,Integer>{
	List<LearningSection> findByStatus(String status);
	LearningSection findByType(String type);
	
	@Query("FROM LearningSection l")
	Page<LearningSection> findByPage(Pageable pageable);
	
	@Modifying
	@Query("DELETE FROM LearningSection l WHERE l.id IN (?1)")
	int deleteByIds(Integer[] ids);
}
