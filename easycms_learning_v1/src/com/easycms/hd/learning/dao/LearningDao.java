package com.easycms.hd.learning.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.learning.domain.Learning;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface LearningDao extends Repository<Learning,Integer>,BasicDao<Learning>{
	@Query("FROM Learning l WHERE l.createBy = ?1 order by l.createOn desc")
	Page<Learning> findByCreater(Integer creater, Pageable pageable);
	
	List<Learning> findByFold(Integer fold);
	
	@Modifying
	@Query("DELETE FROM Learning l WHERE l.id IN (?1)")
	int deleteByIds(Integer[] ids);

	@Query("FROM Learning l WHERE l.fold = ?1 order by l.createOn desc")
	Page<Learning> findByFold(Integer fold, Pageable pageable);
}
