package com.easycms.hd.learning.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.learning.domain.LearningFold;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface LearningFoldDao extends BasicDao<LearningFold> ,Repository<LearningFold,Integer>{
	List<LearningFold> findByStatus(String status);
	LearningFold findByType(String type);
	List<LearningFold> findByModule(String module);
	List<LearningFold> findBySection(String section);
	List<LearningFold> findByModuleAndSection(String module, String section);
	
	@Query("FROM LearningFold l where l.module = ?1 and l.section = ?2")
	Page<LearningFold> findByModuleAndSection(String module, String section, Pageable pageable);
	
	@Query("FROM LearningFold l")
	Page<LearningFold> findByPage(Pageable pageable);
	
	@Modifying
	@Query("DELETE FROM LearningFold l WHERE l.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Query("FROM LearningFold l where l.section = ?1")
	Page<LearningFold> findBySection(String section, Pageable pageable);
}
