package com.easycms.hd.learning.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.learning.domain.LearnedHistory;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface LearnedHistoryDao extends BasicDao<LearnedHistory> ,Repository<LearnedHistory,Integer>{
	List<LearnedHistory> findByStatus(String status);
	
	LearnedHistory findByUniqueCode(String uniqueCode);
	
	@Query("FROM LearnedHistory l WHERE l.userId = ?1 order by l.createOn desc")
	Page<LearnedHistory> findByUser(Integer userId,Pageable pageable);
}
