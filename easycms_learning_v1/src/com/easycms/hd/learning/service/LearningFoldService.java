package com.easycms.hd.learning.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearningFold;

public interface LearningFoldService {
	List<LearningFold> findByAll();
	List<LearningFold> findByStatus(String status);
	List<LearningFold> findByModule(String module);
	List<LearningFold> findBySection(String section);
	List<LearningFold> findByModuleAndSection(String module, String section);
	LearningFold findByType(String type);
	LearningFold findById(Integer id);
	
	Page<LearningFold> findByPage(Page<LearningFold> page);
	LearningFold add(LearningFold learningFold);
	boolean batchDelete(Integer[] ids);
	Page<LearningFold> findByModuleAndSection(String module, String section, Page<LearningFold> page);
	Page<LearningFold> findBySection(String section, Page<LearningFold> page);
}
