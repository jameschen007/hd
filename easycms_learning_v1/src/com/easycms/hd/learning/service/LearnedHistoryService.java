package com.easycms.hd.learning.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.LearnedHistory;

public interface LearnedHistoryService {
	List<LearnedHistory> findByAll();
	List<LearnedHistory> findByStatus(String status);
	LearnedHistory findById(Integer id);
	LearnedHistory add(LearnedHistory learnedHistory);
	LearnedHistory findByUniqueCode(String uniqueCode);
	Page<LearnedHistory> findByUser(Integer userId,Page<LearnedHistory> page);

	Page<LearnedHistory> findPage(LearnedHistory condition,Page<LearnedHistory> page);
	/**
	 * 在线学习列表导出
	 * */
	void statisticalReport(HttpServletRequest request, HttpServletResponse response);
	
}
