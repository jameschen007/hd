package com.easycms.hd.learning.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.domain.Learning;

public interface LearningService {
	Learning add(Learning learning);
	
	Learning findById(Integer id);
	
	Page<Learning> findByCreater(Integer creater, Page<Learning> page);
	
	Page<Learning> findByFold(Integer fold, Page<Learning> page);
	
	List<Learning> findByFold(Integer fold);

	boolean batchDelete(Integer[] ids);
}
