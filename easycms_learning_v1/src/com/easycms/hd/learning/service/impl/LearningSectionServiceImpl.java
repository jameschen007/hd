package com.easycms.hd.learning.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.dao.LearningSectionDao;
import com.easycms.hd.learning.domain.LearningSection;
import com.easycms.hd.learning.service.LearningSectionService;

@Service("learningSectionService")
public class LearningSectionServiceImpl implements LearningSectionService {

	@Autowired
	private LearningSectionDao learningSectionDao;

	@Override
	public List<LearningSection> findByAll() {
		return learningSectionDao.findAll();
	}

	@Override
	public List<LearningSection> findByStatus(String status) {
		return learningSectionDao.findByStatus("ACTIVE");
	}

	@Override
	public LearningSection findByType(String type) {
		return learningSectionDao.findByType(type);
	}

	@Override
	public LearningSection findById(Integer id) {
		return learningSectionDao.findById(id);
	}

	@Override
	public Page<LearningSection> findByPage(Page<LearningSection> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<LearningSection> springPage = learningSectionDao.findByPage(pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(),
				springPage.getContent());
		return page;
	}

	@Override
	public LearningSection add(LearningSection learningSection) {
		return learningSectionDao.save(learningSection);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != learningSectionDao.deleteByIds(ids)) {
			return true;
		}
		return false;
	}
}
