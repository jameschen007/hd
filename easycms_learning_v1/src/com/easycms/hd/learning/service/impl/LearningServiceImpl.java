package com.easycms.hd.learning.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.learning.dao.LearningDao;
import com.easycms.hd.learning.domain.Learning;
import com.easycms.hd.learning.service.LearningService;

@Service("learningService")
public class LearningServiceImpl implements LearningService {

	@Autowired
	private LearningDao learningDao;

	@Override
	public Learning add(Learning learning) {
		return learningDao.save(learning);
	}

	@Override
	public Page<Learning> findByCreater(Integer creater, Page<Learning> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Learning> springPage = learningDao.findByCreater(creater, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Learning findById(Integer id) {
		return learningDao.findById(id);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		if (0 != learningDao.deleteByIds(ids)){
			return true;
		}
		return false;
	}

	@Override
	public List<Learning> findByFold(Integer fold) {
		return learningDao.findByFold(fold);
	}

	@Override
	public Page<Learning> findByFold(Integer fold, Page<Learning> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<Learning> springPage = learningDao.findByFold(fold, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
}
