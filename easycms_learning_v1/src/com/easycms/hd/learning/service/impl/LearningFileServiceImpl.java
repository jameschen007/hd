package com.easycms.hd.learning.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.learning.dao.LearningFileDao;
import com.easycms.hd.learning.domain.LearningFile;
import com.easycms.hd.learning.service.LearningFileService;

@Service("learningFileService")
public class LearningFileServiceImpl implements LearningFileService{

	@Autowired
	private LearningFileDao learningFileDao;
	@Override
	public LearningFile add(LearningFile pfile) {
		return learningFileDao.save(pfile);
	}
	@Override
	public List<LearningFile> findFilesByLearningId(Integer id) {
	
		return learningFileDao.findByLearningId(id);
	}
	
	@Override
	public boolean batchRemove(Integer[] ids) {
		return learningFileDao.batchRemove(ids) > 0;
	}
	@Override
	public boolean batchRemoveLearning(Integer[] ids) {
		return learningFileDao.batchRemoveLearning(ids) > 0;
	}
	@Override
	public List<LearningFile> findFilesByLearningIds(Integer[] ids) {
		return learningFileDao.findFilesByLearningIds(ids);
	}
	@Override
	public boolean batchUpdateLearning(Integer[] ids, Integer learningId) {
		return learningFileDao.batchUpdateLearning(ids, learningId) > 0;
	}
	@Override
	public LearningFile findById(Integer id) {
		return learningFileDao.findById(id);
	}

}
