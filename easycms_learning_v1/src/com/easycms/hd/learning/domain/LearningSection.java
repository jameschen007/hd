package com.easycms.hd.learning.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 培训管理主页中的多个主版块，又如基础培训，技能培训等
 * @author fangwei.ren
 *
 */
@Entity
@Table(name = "learning_section")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class LearningSection extends BasicForm implements Serializable {
	private static final long serialVersionUID = -3753029595185768801L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "description", length = 500)
	private String description;
	@Column(name = "type", length = 50)
	private String type;
	@Column(name = "name", length = 50)
	private String name;
	@Column(name = "status", length = 10)
	private String status;
	@Column(name = "needs_sub")
	private boolean needsSub;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by")
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by")
	private Integer updateBy;
}