package com.easycms.hd.learning.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 培训的辅助类，用于保存培训所需要的资料文件路径
 * @author fangwei.ren
 *
 */
@Entity
@Table(name = "learning_file")
@Data
public class LearningFile {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@Column(name = "file_path")
	private String path;
	@Column(name = "upload_time")
	private Date time;
	@Column(name = "learning_id")
	private Integer learningId;
	@Column(name = "filename")
	private String fileName;
	@Column(name = "suffix")
	private String suffix;
	@Column(name = "conventer_path")
	private String conventerPath;
	@Column(name = "cover_path")
	private String coverPath;
	@Column(name = "conventer_flag")
	private String conventerFlag;
	@Column(name = "conventer_time")
	private Long conventerTime;
	@Column(name = "conventer_description")
	private String conventerDescription;
}
