package com.easycms.hd.learning.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.easycms.core.form.BasicForm;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 培训管理的主类，用于保存主要的学习条目
 * @author fangwei.ren
 *
 */
@Entity
@Table(name = "learning")
@Cacheable
@Data
@EqualsAndHashCode(callSuper=false)
public class Learning extends BasicForm implements Serializable {
	private static final long serialVersionUID = -5380007417883159529L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	@Column(name = "title")
	private String title;
	@Column(name = "description")
	private String description;
	@Column(name = "type")
	private String type;
	@Column(name = "fold")
	private Integer fold;
	@Column(name = "status")
	private String status;
	@Column(name = "seq")
	private Integer seq;
	@JsonIgnore
	@Column(name = "created_on", length = 0)
	private Date createOn;
	@JsonIgnore
	@Column(name = "created_by", length = 50)
	private Integer createBy;
	@JsonIgnore
	@Column(name = "updated_on", length = 0)
	private Date updateOn;
	@JsonIgnore
	@Column(name = "updated_by", length = 50)
	private Integer updateBy;
	
	@Transient
	private List<LearningFile> learningFiles;
	@Transient
	private List<Integer> fileIds;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "autoidup", nullable = false)
//	private LearnedHistory history;
}