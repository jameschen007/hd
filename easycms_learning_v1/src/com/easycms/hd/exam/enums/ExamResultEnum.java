package com.easycms.hd.exam.enums;

public enum ExamResultEnum {
	PASS("及格"),
	FAIL("不及格");
	
	private String name;

	private ExamResultEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
