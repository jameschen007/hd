package com.easycms.hd.exam.enums;

public enum ExamStatus {
	
	/**
	 * 正在进行
	 */
	DOING,
	
	/**
	 * 已完成
	 */
	COMPLETED

}
