package com.easycms.hd.exam.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycms.hd.response.CommonMapResult;

public enum ExamPagerOptionEnum {
	TEXT("文本内容"),
	IMAGE("图像内容");
	
	private String name;

	private ExamPagerOptionEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	
    /**
     * enum lookup map
     */
    private static final Map<String, String> lookup = new HashMap<String, String>();
    /**
     * enum lookup list
     */
    private static final List<CommonMapResult> mapResult = new ArrayList<CommonMapResult>();

    static {
        for (ExamPagerOptionEnum s : EnumSet.allOf(ExamPagerOptionEnum.class)) {
            lookup.put(s.name(), s.getName());
            CommonMapResult commonMapResult = new CommonMapResult();
            commonMapResult.setKey(s.name());
            commonMapResult.setValue(s.getName());
            mapResult.add(commonMapResult);
        }
    }

    public static  Map<String, String> getMap(){
        return lookup;
    }
    
    public static  List<CommonMapResult> getList(){
    	return mapResult;
    }
}
