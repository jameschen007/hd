package com.easycms.hd.exam.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.JsonFormatTool;
import com.easycms.hd.exam.domain.ExamPaper;
import com.easycms.hd.exam.domain.ExamPaperOptions;
import com.easycms.hd.exam.enums.ExamPagerItemEnum;
import com.easycms.hd.exam.enums.ExamPagerOptionEnum;
import com.easycms.hd.exam.form.ExamExcelHandleForm;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExamPaperExcelUtil {
	public static void main(String[] args) throws FileNotFoundException {
		String filePath = "F:\\考试样例表格.xls";
		ExamExcelHandleForm examExcelHandleForm = new ExamExcelHandleForm();
		examExcelHandleForm.setSourceFilePath(filePath);
		List<ExamPaper> examPapers = readExcelToObj(examExcelHandleForm);
		System.out.println(JsonFormatTool.formatJson(CommonUtility.toJson(examPapers),"\t"));
	}

	/**
	 * 读取excel数据
	 * 
	 * @param path
	 */
	public static List<ExamPaper> readExcelToObj(ExamExcelHandleForm examExcelHandleForm) {
		Workbook wb = null;
		try {
			if (null != examExcelHandleForm && CommonUtility.isNonEmpty(examExcelHandleForm.getSourceFilePath())) {
				wb = WorkbookFactory.create(new File(examExcelHandleForm.getSourceFilePath()));
				return readExcel(wb, 0, 1, 0, examExcelHandleForm);
			}
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 读取excel文件
	 * 
	 * @param wb
	 * @param sheetIndex
	 *            sheet页下标：从0开始
	 * @param startReadLine
	 *            开始读取的行:从0开始
	 * @param tailLine
	 *            去除最后读取的行
	 */
	private static List<ExamPaper> readExcel(Workbook wb, int sheetIndex, int startReadLine, int tailLine, ExamExcelHandleForm examExcelHandleForm) {
		Sheet sheet = wb.getSheetAt(sheetIndex);
		Row row = null;
		List<ExamPaper> examPapers = new ArrayList<>();
		ExamPaper oldExamPaper = null;
		ExamPaper newExamPaper = null;
		List<ExamPaperOptions> oldExamPaperOptionsList = null;
		List<ExamPaperOptions> newExamPaperOptionsList = null;
		for (int i = startReadLine; i < sheet.getLastRowNum() - tailLine + 1; i++) {
			row = sheet.getRow(i);
			ExamPaperOptions examPaperOptions = new ExamPaperOptions();
			examPaperOptions.setCreateOn(Calendar.getInstance().getTime());
			examPaperOptions.setUpdateOn(Calendar.getInstance().getTime());
			
			boolean titleFlag = false;
			
			if(row.getCell(0)!=null){
				if(CommonUtility.isNonEmpty(getCellValue(row.getCell(0)))){
					oldExamPaper = newExamPaper;
					newExamPaper = new ExamPaper();
					newExamPaper.setFold(examExcelHandleForm.getFold());
					newExamPaper.setModule(examExcelHandleForm.getModule());
					newExamPaper.setSection(examExcelHandleForm.getSection());
					newExamPaper.setCreateOn(Calendar.getInstance().getTime());
					newExamPaper.setUpdateOn(Calendar.getInstance().getTime());
					oldExamPaperOptionsList = newExamPaperOptionsList;
					newExamPaperOptionsList = new ArrayList<ExamPaperOptions>();
					titleFlag = true;
				}
			}
			for (int j = 0; j < row.getLastCellNum(); j++) {
				Cell cell = row.getCell(j);
				if (null != cell && null != newExamPaper) {
					boolean isMerge = isMergedRegion(sheet, i, cell.getColumnIndex());
					// 判断是否具有合并单元格
					if (isMerge) {
						String rs = getMergedRegionValue(sheet, row.getRowNum(), cell.getColumnIndex());
						if (titleFlag) {
							if (1 == cell.getColumnIndex() && StringUtils.isNoneBlank(rs)) {
								newExamPaper.setSubject(rs.trim());
							}
						}
					} else {
						if (titleFlag) {
							if (0 == cell.getColumnIndex()) {
								String num = getCellValue(cell);
								if(num!=null && num.endsWith(".0")){
									num = num.substring(0, num.length()-2);
								}
								newExamPaper.setNumber(num);
							} else if (3 == cell.getColumnIndex()) {
								String type = getCellValue(cell);
								if ("单选".equals(type)) {
									newExamPaper.setType(ExamPagerItemEnum.SINGLE.name());
								} else if ("多选".equals(type)) {
									newExamPaper.setType(ExamPagerItemEnum.MULTIPLE.name());
								} else if ("判断".equals(type)) {
									newExamPaper.setType(ExamPagerItemEnum.YES_OR_NO.name());
								}
							} else if (4 == cell.getColumnIndex()) {
								try {
									newExamPaper.setScore(Double.valueOf(getCellValue(cell)));
								} catch (Exception e) {
									log.error(e.getMessage());
								}
							}
						} else {
							if (1 == cell.getColumnIndex()) {
								examPaperOptions.setNumber(getCellValue(cell));
							} else if (2 == cell.getColumnIndex()) {
								examPaperOptions.setType(ExamPagerOptionEnum.TEXT.name());
								examPaperOptions.setContent(getCellValue(cell));
							} else if (3 == cell.getColumnIndex()) {
								examPaperOptions.setCorrect(getCellValue(cell));
							}
						}
					}
				}
			}
			if (!titleFlag) {
				examPaperOptions.setExamPaper(newExamPaper);
				newExamPaperOptionsList.add(examPaperOptions);
			} else {
				if (null != oldExamPaper) {
					if (null != oldExamPaperOptionsList) {
						oldExamPaper.setExamPaperOptions(oldExamPaperOptionsList);
					}
					examPapers.add(oldExamPaper);
					newExamPaperOptionsList = new ArrayList<ExamPaperOptions>();
				}
			}
			if (i == sheet.getLastRowNum() - tailLine) {
				newExamPaper.setExamPaperOptions(newExamPaperOptionsList);
				examPapers.add(newExamPaper);
			}
		}
		return examPapers;
	}

	/**
	 * 获取合并单元格的值
	 * 
	 * @param sheet
	 * @param row
	 * @param column
	 * @return
	 */
	private static String getMergedRegionValue(Sheet sheet, int row, int column) {
		int sheetMergeCount = sheet.getNumMergedRegions();

		for (int i = 0; i < sheetMergeCount; i++) {
			CellRangeAddress ca = sheet.getMergedRegion(i);
			int firstColumn = ca.getFirstColumn();
			int lastColumn = ca.getLastColumn();
			int firstRow = ca.getFirstRow();
			int lastRow = ca.getLastRow();

			if (row >= firstRow && row <= lastRow) {

				if (column >= firstColumn && column <= lastColumn) {
					Row fRow = sheet.getRow(firstRow);
					Cell fCell = fRow.getCell(firstColumn);
					return getCellValue(fCell);
				}
			}
		}

		return null;
	}

	/**
	 * 判断合并了行
	 * 
	 * @param sheet
	 * @param row
	 * @param column
	 * @return
	 */
	@SuppressWarnings("unused")
	private static boolean isMergedRow(Sheet sheet, int row, int column) {
		int sheetMergeCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergeCount; i++) {
			CellRangeAddress range = sheet.getMergedRegion(i);
			int firstColumn = range.getFirstColumn();
			int lastColumn = range.getLastColumn();
			int firstRow = range.getFirstRow();
			int lastRow = range.getLastRow();
			if (row == firstRow && row == lastRow) {
				if (column >= firstColumn && column <= lastColumn) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断指定的单元格是否是合并单元格
	 * 
	 * @param sheet
	 * @param row
	 *            行下标
	 * @param column
	 *            列下标
	 * @return
	 */
	private static boolean isMergedRegion(Sheet sheet, int row, int column) {
		int sheetMergeCount = sheet.getNumMergedRegions();
		for (int i = 0; i < sheetMergeCount; i++) {
			CellRangeAddress range = sheet.getMergedRegion(i);
			int firstColumn = range.getFirstColumn();
			int lastColumn = range.getLastColumn();
			int firstRow = range.getFirstRow();
			int lastRow = range.getLastRow();
			if (row >= firstRow && row <= lastRow) {
				if (column >= firstColumn && column <= lastColumn) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断sheet页中是否含有合并单元格
	 * 
	 * @param sheet
	 * @return
	 */
	@SuppressWarnings("unused")
	private static boolean hasMerged(Sheet sheet) {
		return sheet.getNumMergedRegions() > 0 ? true : false;
	}

	/**
	 * 合并单元格
	 * 
	 * @param sheet
	 * @param firstRow
	 *            开始行
	 * @param lastRow
	 *            结束行
	 * @param firstCol
	 *            开始列
	 * @param lastCol
	 *            结束列
	 */
	@SuppressWarnings("unused")
	private static void mergeRegion(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {
		sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
	}

	/**
	 * 获取单元格的值
	 * 
	 * @param cell
	 * @return
	 */
	private static String getCellValue(Cell cell) {
		if (cell == null)
			return "";
		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			return cell.getStringCellValue();
		} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			return String.valueOf(cell.getBooleanCellValue());
		} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			return cell.getCellFormula();
		} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			return String.valueOf(cell.getNumericCellValue());
		}else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return null;
		}
		return "";
	}
}
