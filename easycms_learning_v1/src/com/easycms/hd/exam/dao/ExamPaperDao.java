package com.easycms.hd.exam.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.exam.domain.ExamPaper;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ExamPaperDao extends Repository<ExamPaper,Integer>,BasicDao<ExamPaper>{
	@Query("FROM ExamPaper e where e.module = ?1 and e.section = ?2")
	Page<ExamPaper> findByModuleAndSection(String module, String section, Pageable pageable);
	
	@Modifying
	@Query("DELETE FROM ExamPaper e WHERE e.id IN (?1)")
	int deleteByIds(Integer[] ids);
	
	@Query("FROM ExamPaper e where e.section = ?1")
	Page<ExamPaper> findBySection(String section, Pageable pageable);
	
	@Query("FROM ExamPaper e WHERE e.id IN (?1)")
	List<ExamPaper> findByIds(Integer[] ids);
	
	@Query("FROM ExamPaper e WHERE e.id = ?1 AND e.section = ?2 AND e.type=?3")
	ExamPaper findByIdAndSection(Integer id,String section,String type);
	
	@Query("FROM ExamPaper e WHERE e.id = ?1 AND e.section = ?2 AND e.module = ?3 AND e.type=?4")
	ExamPaper findByIdAndSectionAndModule(Integer id,String section,String module,String type);
	
	@Query("SELECT MAX(e.id) FROM ExamPaper e WHERE e.type = ?1")
	Integer maxIdByType(String type);
	
	@Query("SELECT MAX(e.id) FROM ExamPaper e WHERE e.section = ?1")
	Integer maxIdBySection(String section);
	
	@Query("FROM ExamPaper e WHERE e.section = ?1 AND e.module=?2 AND e.score=?3")
	List<ExamPaper> findBySectionAndModuleAndScore(String section,String module,Double score);
	
	@Query("FROM ExamPaper e WHERE e.section = ?1 AND e.score=?2")
	List<ExamPaper> findBySectionAndScore(String section,Double score);
	
	@Query("SELECT SUM(e.score) FROM ExamPaper e WHERE e.section = ?1")
	Integer sumBySection(String section);
	@Query("SELECT SUM(e.score) FROM ExamPaper e WHERE e.section = ?1 AND e.module=?2")
	Integer sumBySectionModule(String section,String module);
	
	@Query("FROM ExamPaper")
	List<ExamPaper> findAllExamPaper();
}
