package com.easycms.hd.exam.service;

import java.util.List;

import com.easycms.core.util.Page;
import com.easycms.hd.exam.domain.ExamUserScore;

public interface ExamUserScoreService {
	
	ExamUserScore add(ExamUserScore examUserScore);

	ExamUserScore findById(Integer id);
	
	Page<ExamUserScore> findByUser(Integer userId,Page<ExamUserScore> page);
	
	List<ExamUserScore> findByUserAndExamStatus(Integer userId,String examStatus);
	
	Page<ExamUserScore> findByUserAndExamStatus(Integer userId,String examStatus,Page<ExamUserScore> page);

	public Page<ExamUserScore> findByExamStatus(String examStatus, Page<ExamUserScore> page);
	List<ExamUserScore> findByUserAndExamStatus(String examStatus);

}
