package com.easycms.hd.exam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.exam.dao.ExamUserScoreDao;
import com.easycms.hd.exam.domain.ExamUserScore;
import com.easycms.hd.exam.service.ExamUserScoreService;

@Service("examUserScoreService")
public class ExamUserScoreServiceImpl implements ExamUserScoreService {
	@Autowired
	private  ExamUserScoreDao  examUserScoreDao;

	@Override
	public ExamUserScore add(ExamUserScore examUserScore) {
		return examUserScoreDao.save(examUserScore);
	}

	@Override
	public ExamUserScore findById(Integer id) {
		return examUserScoreDao.findById(id);
	}

	@Override
	public Page<ExamUserScore> findByUser(Integer userId,Page<ExamUserScore> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<ExamUserScore> springPage = examUserScoreDao.findByUser(userId, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public List<ExamUserScore> findByUserAndExamStatus(Integer userId, String examStatus) {
		return examUserScoreDao.findByUserAndExamStatus(userId, examStatus);
	}

	@Override
	public Page<ExamUserScore> findByUserAndExamStatus(Integer userId,String examStatus, Page<ExamUserScore> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<ExamUserScore> springPage = examUserScoreDao.findByUserAndExamStatus(userId, examStatus, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<ExamUserScore> findByExamStatus(String examStatus, Page<ExamUserScore> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());
		org.springframework.data.domain.Page<ExamUserScore> springPage = examUserScoreDao.findByExamStatus(examStatus, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public
	List<ExamUserScore> findByUserAndExamStatus(String examStatus){
		return examUserScoreDao.findByUserAndExamStatus(examStatus);
	}
}
