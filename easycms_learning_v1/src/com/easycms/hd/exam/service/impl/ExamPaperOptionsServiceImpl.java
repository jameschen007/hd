package com.easycms.hd.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.exam.dao.ExamPaperOptionsDao;
import com.easycms.hd.exam.domain.ExamPaperOptions;
import com.easycms.hd.exam.service.ExamPaperOptionsService;

@Service("examPaperOptionsService")
public class ExamPaperOptionsServiceImpl implements ExamPaperOptionsService{

	@Autowired
	private ExamPaperOptionsDao examPaperOptionsDao;

	@Override
	public ExamPaperOptions add(ExamPaperOptions examPaperOptions) {
		return examPaperOptionsDao.save(examPaperOptions);
	}

	@Override
	public ExamPaperOptions findById(Integer id) {
		return examPaperOptionsDao.findById(id);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		return examPaperOptionsDao.deleteByIds(ids) > 0;
	}
}
