package com.easycms.hd.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.easycms.core.util.Page;
import com.easycms.hd.exam.dao.ExamPaperDao;
import com.easycms.hd.exam.domain.ExamPaper;
import com.easycms.hd.exam.service.ExamPaperService;

@Service("examPaperService")
public class ExamPaperServiceImpl implements ExamPaperService{

	@Autowired
	private ExamPaperDao examPaperDao;

	@Override
	public ExamPaper add(ExamPaper examPaper) {
		return examPaperDao.save(examPaper);
	}

	@Override
	public ExamPaper findById(Integer id) {
		return examPaperDao.findById(id);
	}

	@Override
	public boolean batchDelete(Integer[] ids) {
		return examPaperDao.deleteByIds(ids) > 0;
	}

	@Override
	public Page<ExamPaper> findBySection(String section, Page<ExamPaper> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<ExamPaper> springPage = examPaperDao.findBySection(section, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}

	@Override
	public Page<ExamPaper> findByModuleAndSection(String module, String section, Page<ExamPaper> page) {
		Pageable pageable = new PageRequest(page.getPageNum() - 1, page.getPagesize());

		org.springframework.data.domain.Page<ExamPaper> springPage = examPaperDao.findByModuleAndSection(module, section, pageable);
		page.execute(Integer.valueOf(Long.toString(springPage.getTotalElements())), page.getPageNum(), springPage.getContent());
		return page;
	}
}
