package com.easycms.hd.exam.service;

import com.easycms.hd.exam.domain.ExamPaperOptions;

public interface ExamPaperOptionsService {
	ExamPaperOptions add(ExamPaperOptions examPaperOptions);

	ExamPaperOptions findById(Integer id);

	boolean batchDelete(Integer[] ids);
}
