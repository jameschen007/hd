package com.easycms.hd.exam.service;

import com.easycms.core.util.Page;
import com.easycms.hd.exam.domain.ExamPaper;

public interface ExamPaperService {
	ExamPaper add(ExamPaper examPaper);

	ExamPaper findById(Integer id);

	boolean batchDelete(Integer[] ids);

	Page<ExamPaper> findByModuleAndSection(String module, String section, Page<ExamPaper> page);

	Page<ExamPaper> findBySection(String section, Page<ExamPaper> page);
}
