package com.easycms.hd.exam.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.easycms.hd.learning.domain.LearningFold;

import lombok.Data;

/**
 * 
 * 考试成绩
 *
 */
@Entity
@Table(name = "exam_user_score")
@Data
public class ExamUserScore implements Serializable{
	private static final long serialVersionUID = 3309184181832351635L;
	
	/**
	 * 唯一ID
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	/**
	 * 用户ID
	 */
	@Column(name = "user_id")
	private Integer userId;
	
	/**
	 * 考试得分
	 */
	@Column(name = "exam_score")
	private double examScore;
	
	/**
	 * 考试类型
	 */
	@Column(name = "exam_type")
	private String examType;
	
	/**
	 * 考试结果：是否合格
	 */
	@Column(name = "exam_result")
	private String examResult;
	
	/**
	 * 考试开始时间
	 */
	@Column(name = "exam_start_date")
	private Date examStartDate;
	
	/**
	 * 考试结束时间
	 */
	@Column(name = "exam_end_date")
	private Date examEndDate;
	
	@Column(name = "exam_status")
	private String examStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="learn_fold_id",nullable = false)
	private LearningFold learningFold;

}
