package com.easycms.hd.mobile.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.mobile.dao.BannerDao;
import com.easycms.hd.mobile.domain.Banner;
import com.easycms.hd.mobile.service.BannerService;


@Service("bannerService")
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerDao bannerDao;

	@Override
	public List<Banner> findByAll() {
		return bannerDao.findAll();
	}

	@Override
	public List<Banner> findByStatus(String status) {
		return bannerDao.findByStatus("ACTIVE");
	}


	
}
