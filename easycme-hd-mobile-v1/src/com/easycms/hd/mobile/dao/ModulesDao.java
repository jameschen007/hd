package com.easycms.hd.mobile.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.mobile.domain.Modules;

@Transactional(readOnly = true,propagation=Propagation.REQUIRED)
public interface ModulesDao extends BasicDao<Modules> ,Repository<Modules,Integer>{
	List<Modules> findByStatus(String status);
	Modules findByType(String type);
	Modules findByEnpowerName(String enpowerName);
	

	@Query("FROM Modules m WHERE m.status = ?1")
	Page<Modules> findByStatus(String status, Pageable pageable);
	
	@Query("FROM Modules m WHERE m.status = ?1 AND m.section = ?2")
	Page<Modules> findByStatusAndSection(String status,String section, Pageable pageable);
	
	@Modifying
	@Query("DELETE FROM Modules m WHERE m.id IN (?1)")
	int deleteByIds(Integer[] ids);
}
