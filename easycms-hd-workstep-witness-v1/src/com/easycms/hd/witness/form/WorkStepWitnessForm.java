package com.easycms.hd.witness.form;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.easycms.core.form.BasicForm;
import com.easycms.hd.plan.domain.FakeMap;
import com.easycms.hd.variable.domain.VariableSet;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class WorkStepWitnessForm extends BasicForm implements Serializable {
	private static final long serialVersionUID = 725092056657825631L;
	private Integer id;
	private Integer stepno;
	private String witness;
	private String witnesser;
	private String witnessdes;
	private String witnessaddress;
	private Date witnessdate;
	private Integer rollingplanid;
	
	private String witnessType;
	private String witnessW;
	private String witnessH;
	private String witnessR;
	
	private String operater;
	private Date operatedate;
	private String operatedesc;
	
	private Integer status;
	private Integer isok;
	private String remark;
	private Map<String, String> map;
	private List<FakeMap> fakeMap;
	private List<VariableSet> variableSet;
	
	private Integer qcsign;
	private String qcman;
	private Date qcdate;
	
	private String noticeresultdesc;
	
	private String customRole;
	
	private String dosage;
	private String failType;
	private String type;
	
	private String substitute;
}
