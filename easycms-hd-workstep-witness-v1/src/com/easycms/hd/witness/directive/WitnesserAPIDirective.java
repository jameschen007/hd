package com.easycms.hd.witness.directive;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycms.common.util.CommonUtility;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.jpa.QueryUtil;
import com.easycms.core.util.ContextUtil;
import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.WitnessTypeEnum;
import com.easycms.hd.api.response.WitnessPageResult;
import com.easycms.hd.api.service.WitnessApiService;
import com.easycms.hd.plan.domain.WorkStepWitness;
import com.easycms.management.user.domain.User;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class WitnesserAPIDirective implements TemplateDirectiveModel {
	private static final Logger logger = Logger.getLogger(WitnesserAPIDirective.class);
	
	private static final ObjectWrapper DEFAULT_WRAPER = WrappingTemplateModel.getDefaultObjectWrapper();
	protected static final Integer DEFAULT_PAGE_NUMBER = 1;
	protected static final Integer DEFAULT_PAGE_SIZE = 10;
	
	@Autowired
	private WitnessApiService witnessApiService;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if(loopVars.length < 1) {
			throw new RuntimeException("Loop variable is required.");
		}
		
		// 查询类型
		String queryType = FreemarkerTemplateUtility.getStringValueFromParams(params, "queryType");
		queryType = CommonUtility.isNonEmpty(queryType) ? queryType : QueryUtil.TYPE_FIELD;
		
		User loginUser =  ContextUtil.getCurrentLoginUser();
		String type =  (String) ContextUtil.getSession().getAttribute("type");
		
		if (queryType.equals(QueryUtil.TYPE_FIELD)) {
			Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params, "id");
			loopVars[0] = DEFAULT_WRAPER.wrap(witnessApiService.findByWitnessIdAndRollingPlanType(loginUser, id, type, true));
		} else if (queryType.equals(QueryUtil.TYPE_LIST)) {
			String custom = FreemarkerTemplateUtility.getStringValueFromParams(params, "custom");
			
			//若分页则封装分页数据
			Page<WorkStepWitness> pager = new Page<WorkStepWitness>();
			// 获取页数
			Integer pageNum = FreemarkerTemplateUtility.getIntValueFromParams(
					params, "pageNum");
			pageNum = pageNum == null ? DEFAULT_PAGE_NUMBER : pageNum;
			logger.debug("[pageNum] ==> " + pageNum);
			
			// 每页大小
			Integer pagesize = FreemarkerTemplateUtility.getIntValueFromParams(
					params, "pagesize");
			pagesize = pagesize == null ? DEFAULT_PAGE_SIZE : pagesize;
			logger.debug("[pagesize] ==> " + pagesize);
			
			pager.setPageNum(pageNum);
			pager.setPagesize(pagesize);
			
			WitnessPageResult witnessPageDataResult = null;
			Integer usingRoleId = null;//20180927 添加由APP传到接口的当前使用角色，以区分具体业务场景,至于web端，暂时不做打上记号
			if (null != custom && custom.equals("myevent")){
				witnessPageDataResult = witnessApiService.getWitnessPageResult(pager, loginUser, WitnessTypeEnum.UNCOMPLETED, type , null, null,loginUser, usingRoleId);
			} else if (null != custom && custom.equals("myeventAlready")){
				witnessPageDataResult = witnessApiService.getWitnessPageResult(pager, loginUser, WitnessTypeEnum.COMPLETED, type , null, null,loginUser, usingRoleId);
			}
			pageVariable(env, pager, DEFAULT_WRAPER);
			
			loopVars[0] = DEFAULT_WRAPER.wrap(witnessPageDataResult.getData());
		}
		body.render(env.getOut());
	}
	
	/**
	 * 设置分页变量到输出环境
	 * @param env
	 * @param page
	 * @param DEFAULT_WRAPER
	 * @throws TemplateModelException
	 */
	private static <T> void pageVariable(Environment env, Page<T> page,ObjectWrapper DEFAULT_WRAPER)
			throws TemplateModelException {
		env.setVariable("count", DEFAULT_WRAPER.wrap(page.getTotalCounts())); // 总记录数
		env.setVariable("pagesize", DEFAULT_WRAPER.wrap(page.getPagesize())); // 页面大小
		env.setVariable("start", DEFAULT_WRAPER.wrap(page.getStartPage())); // 开始页面
		env.setVariable("end", DEFAULT_WRAPER.wrap(page.getEndPage())); // 结束页面
		env.setVariable("current", DEFAULT_WRAPER.wrap(page.getCurrentPage())); // 当前页面
		env.setVariable("pagecount", DEFAULT_WRAPER.wrap(page.getPageCount())); // 页面总数
	}
}
