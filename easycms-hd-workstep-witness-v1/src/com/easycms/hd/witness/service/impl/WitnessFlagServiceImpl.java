package com.easycms.hd.witness.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.easycms.common.jpush.JPushCategoryEnums;
//import com.easycms.common.jpush.JPushExtra;
import com.easycms.hd.api.enums.WitnessFlagEnum;
import com.easycms.hd.api.service.impl.ExecutePushToEnpowerServiceImpl;
import com.easycms.hd.plan.domain.StepFlag;
import com.easycms.hd.plan.domain.WorkStep;
import com.easycms.hd.plan.domain.WorkStepWitness;
//import com.easycms.hd.plan.service.JPushService;
import com.easycms.hd.plan.service.WitnessService;
import com.easycms.hd.plan.service.WorkStepFlagService;
import com.easycms.hd.plan.service.WorkStepService;
import com.easycms.hd.witness.service.WitnessFlagService;

import lombok.extern.slf4j.Slf4j;

@Service("witnessFlagService")
@Slf4j
public class WitnessFlagServiceImpl implements WitnessFlagService {
	@Autowired
	private WitnessService witnessService;
	@Autowired
	private WorkStepService workStepService;
	@Autowired
	private WorkStepFlagService workStepFlagService;
	@Autowired
	private ExecutePushToEnpowerServiceImpl executePushToEnpowerServiceImpl;
//	@Autowired
//	private JPushService jPushService;
	
	@Override
	public void changeWitnessFlag(Integer parentId,String dosage) throws Exception {
		log.info("----------------------------判断是否全部结果[开始]");
		
		WorkStepWitness parent = witnessService.findById(parentId);
		if (null != parent){
			List<WorkStepWitness> children = parent.getChildren();
	
			if (null != children && children.size() > 0){
				int number = 0;
				
				for (int i = 0; i < children.size(); i++){
					WorkStepWitness wsw = children.get(i);
					
					if (wsw.getIsok() > 1){
						number++;
					}
				}
				
				log.info("----------------------------判断是否全部结果[开始]--children" + number + " : " + children.size());
				
				if (number == children.size()){
					WorkStep workStep = parent.getWorkStep();
					
					if (null != workStep){
						if (null != workStep.getNoticeresult()){
							log.info(workStep.getStepname() + " already have Noticeresult [" + workStep.getNoticeresult() + "].");
							return;
						}
						
						workStep.setNoticeresult(2+"");
						workStep.setNoticeresultdesc("--各通知点见证人见证通过--");
						workStep.setUpdatedBy("system");
						workStep.setUpdatedOn(new Date());
						workStep.setStepflag(StepFlag.DONE);
						
						workStep = workStepService.add(workStep);
						
						workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),dosage);
					}
				}
			}
		}
	}
	
	@Override
	public void changeWitnessFlagSingle(Integer selfId,String dosage) throws Exception {
		log.info("----------------------------单独判断一个是否全部结果[开始]");
		try{

			WorkStepWitness self = witnessService.findById(selfId);
			if (null != self){
				log.info("----------------------------判断其它人是否已经结果[开始]");
				WorkStep workStep = self.getWorkStep();
					if (null != workStep){
					List<WorkStepWitness> others = witnessService.getActiveByWorkStepId(workStep.getId());
					if (null != others && others.size() > 0){
						int number = 0;
						
						for (int i = 0; i < others.size(); i++){
							WorkStepWitness wsw = others.get(i);
							
							if (wsw.getIsok() > 1){
								number++;
							}
						}
						if (number >= others.size()){
							if (null != workStep.getNoticeresult()){
								log.info(workStep.getStepname() + " already have Noticeresult [" + workStep.getNoticeresult() + "].");
								return;
							}
							
							workStep.setNoticeresult(2+"");
							workStep.setNoticeresultdesc("--各通知点见证人见证通过--");
							workStep.setUpdatedBy("system");
							workStep.setUpdatedOn(new Date());
							workStep.setWitnessflag(WitnessFlagEnum.COMPLETED.name());
							workStep.setStepflag(StepFlag.DONE);
							
							workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),dosage);

							//处理工序状态前，调用Enpower接口推送消点
							ArrayList<WorkStepWitness> enpParam = new ArrayList<WorkStepWitness>();
							enpParam.add(self);
							executePushToEnpowerServiceImpl.disappearPoint(enpParam,null);
							
							workStep = workStepService.add(workStep);
//							String message = "滚动计划：" + workStep.getRollingPlan().getDrawno() + " 工序： " + workStep.getStepno() + " - " + workStep.getStepname() + " 已完成。";
//					        JPushExtra extra = new JPushExtra();
//							extra.setCategory(JPushCategoryEnums.WORKSTEP_COMPLETE.name());
							//只有见证结果不合格时，才给组长推送消息
//							jPushService.pushByUserId(extra, message, JPushCategoryEnums.WORKSTEP_COMPLETE.getName(), workStep.getRollingPlan().getConsteam());
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e ;
		}
		
	}
	
//都消点了，但管焊队队长点进去看，还在未完成界面
	//查询出这些是待办的见证记录
	/**
	 * 修改应该是工序完成了的，但在待　未见证的见证列表中的数据。
	 * @throws Exception
	 */
	@Override
	public void repairChangeWitnessFlagSingle() throws Exception {
		
		List<WorkStep> allStepList = workStepService.findByPageUnCompleted();
		
		Integer selfId=0;
		String dosage=null;
		
		log.info("----------------------------单独判断一个是否全部结果[开始]");
		try{
			for(WorkStep step:allStepList){
				
				List<WorkStepWitness> witnessList = step.getWorkStepWitnesses();
				CopyOnWriteArrayList<WorkStepWitness>  ee = new CopyOnWriteArrayList<WorkStepWitness>();
				ee.addAll(witnessList);
				
				for(WorkStepWitness self:ee){
					
//					if(11353!=self.getId()){
//						continue ;
//					}

//					WorkStepWitness self = witnessService.findById(selfId);
					if (null != self){
						log.info("----------------------------判断其它人是否已经结果[开始]");
						WorkStep workStep = self.getWorkStep();
							if (null != workStep){
							List<WorkStepWitness> others = witnessService.getActiveByWorkStepId(workStep.getId());
							if (null != others && others.size() > 0){
								int number = 0;
								
								for (int i = 0; i < others.size(); i++){
									WorkStepWitness wsw = others.get(i);
									
									if (wsw.getIsok() > 1){
										number++;
									}
								}
								if (number >= others.size()){
									if (null != workStep.getNoticeresult()){
										log.info(workStep.getStepname() + " already have Noticeresult [" + workStep.getNoticeresult() + "].");
										continue;
									}
									
									workStep.setNoticeresult(2+"");
									workStep.setNoticeresultdesc("--各通知点见证人见证通过--");
									workStep.setUpdatedBy("system");
									workStep.setUpdatedOn(new Date());
									workStep.setWitnessflag(WitnessFlagEnum.COMPLETED.name());
									workStep.setStepflag(StepFlag.DONE);
									
									workStepFlagService.changeStepFlag(workStep.getRollingPlan().getId(),dosage);

									//处理工序状态前，调用Enpower接口推送消点
									ArrayList<WorkStepWitness> enpParam = new ArrayList<WorkStepWitness>();
									enpParam.add(self);
									executePushToEnpowerServiceImpl.disappearPoint(enpParam,null);
									
									workStep = workStepService.add(workStep);
//									String message = "滚动计划：" + workStep.getRollingPlan().getDrawno() + " 工序： " + workStep.getStepno() + " - " + workStep.getStepname() + " 已完成。";
//							        JPushExtra extra = new JPushExtra();
//									extra.setCategory(JPushCategoryEnums.WORKSTEP_COMPLETE.name());
									//只有见证结果不合格时，才给组长推送消息
//									jPushService.pushByUserId(extra, message, JPushCategoryEnums.WORKSTEP_COMPLETE.getName(), workStep.getRollingPlan().getConsteam());
								}
							}
						}
					}
				}

			}

		}catch(Exception e){
			e.printStackTrace();
			throw e ;
		}
		
	}
}
