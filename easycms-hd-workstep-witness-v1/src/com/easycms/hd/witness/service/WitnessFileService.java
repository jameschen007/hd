package com.easycms.hd.witness.service;

import java.util.List;

import com.easycms.hd.witness.domain.WitnessFile;

public interface WitnessFileService {
	WitnessFile add(WitnessFile pfile);
	
	List<WitnessFile> findFilesByWitnessId(Integer id);
}
