package com.easycms.hd.question.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.question.dao.RollingQuestionProcessDao;
import com.easycms.hd.question.domain.RollingQuestionProcess;
import com.easycms.hd.question.service.RollingQuestionProcessService;

@Service
public class RollingQuestionProcessImp implements RollingQuestionProcessService{
	public static final Logger logger = Logger.getLogger(RollingQuestionProcessImp.class);

//	@Autowired
//	private RollingQuestionDao rollingQuestionDao;
//	@Autowired
//	private UserDao userDao;
	@Autowired
	private RollingQuestionProcessDao RollingQuestionProcessDao;

//	@Autowired
//	private VariableSetDao variableSetDao;

	@Override
	public List<RollingQuestionProcess> findByQuestionId(Integer id) {
		return RollingQuestionProcessDao.findByQuestionId(id);
	}

	
	@Override
	public RollingQuestionProcess add(RollingQuestionProcess p) {
		return RollingQuestionProcessDao.save(p);
	}

	@Override
	public RollingQuestionProcess update(RollingQuestionProcess p) {
		return RollingQuestionProcessDao.save(p);
	}

	@Override
	public int updateStatusByQuestionIdAndSolv(Integer qId,String s,Integer solv){

		return RollingQuestionProcessDao.updateStatusByQuestionIdAndSolv(qId,s,solv);
	}
}
