package com.easycms.hd.question.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.easycms.core.util.Page;
import com.easycms.hd.api.enums.question.QuestionAnswerStatusDbEnum;
import com.easycms.hd.api.enums.question.QuestionStatusDbEnum;
import com.easycms.hd.api.request.base.BaseRequestForm;
import com.easycms.hd.question.domain.RollingQuestion;
import com.easycms.hd.question.domain.RollingQuestionSolver;

public interface RollingQuestionService {
	public RollingQuestion add(RollingQuestion p);
	public RollingQuestion update(RollingQuestion p);
	public List<RollingQuestion> findByIds(Integer[] aids);

	public RollingQuestion findById(Integer id);
	public List<RollingQuestion> findByWorkstepId(int worstepid);
	public List<RollingQuestion> findByRollingPlanIdAndIsNotOk(int rolingPlanId);
	public List<RollingQuestion> findByUserId(String userid);
	public List<RollingQuestion> findByIdsAndStatus(Integer[] ids, String status);
	public List<RollingQuestion> findByUserIdAndConfirm(String userid, String confirm);
    
	//分页
	public Page<RollingQuestion> findByWorkstepId(int worstepid,Page<RollingQuestion> page);
	public Page<RollingQuestion> findByUserId(String userid,Page<RollingQuestion> page);
	public Page<RollingQuestion> findByIdsAndStatus(Integer[] ids, QuestionStatusDbEnum status,Page<RollingQuestion> page);
	public Page<RollingQuestion> findByUserIdAndConfirm(String userid, String confirm,Page<RollingQuestion> page);
    public Page<RollingQuestion> findByIds(Integer[] ids,Page<RollingQuestion> page);
	public boolean delete(RollingQuestion p);
    public int countByUserId(String userid);
    public int findCountByUserIdAndConfirm(String userid, String i);
    public int findCountConcernedNotSolved(Integer[] ids);
    public int findByNeedToSolveCount(Integer[] ids);
	public List<RollingQuestion> findByRollingPlanId(int rid);
	public Page<RollingQuestion> findByRollingPlanId(int parseInt, Page<RollingQuestion> page);
    public int findCountConcerned(Integer[] ids);
    public List<RollingQuestion> findConfirmByIdsAndStatus(Integer[] ids, String i);
    public Page<RollingQuestion> findConfirmRollingQuestionsByIds(Integer[] ids, String i, Page<RollingQuestion> page);
    
	Page<RollingQuestion> findByIdsAndStatus(Integer[] ids, String status, String keyword,
			Page<RollingQuestion> page);
	Page<RollingQuestion> findByUserId(String userid, String keyword, Page<RollingQuestion> page);
	Page<RollingQuestion> findConfirmRollingQuestionsByIds(Integer[] ids, String i,
			String keyword, Page<RollingQuestion> page);
	Page<RollingQuestion> findByIds(Integer[] ids, String keyword, Page<RollingQuestion> page);
	Page<RollingQuestion> findAll(Page<RollingQuestion> page);
	/**
	 * 根据问题表和问题解决人表查询列表。
	 * @param condition
	 * @param solver
	 * @param page
	 * @param type 滚动计划类型
	 * @param userId 查询谁的问题
	 * @param keyword 问题列表搜索关键字
	 * @return
	 */
    public Page<RollingQuestion> findPage(final RollingQuestion condition,final RollingQuestionSolver solver,Page<RollingQuestion> page,String type,Integer userId,String keyword);

    /**
     * 根据父记录查子记录
     * @param id
     * @return
     */
	public List<RollingQuestion> findByUpId(Integer id);
	/**
	 * 组长回答问题解决情况
	 * @param questionId
	 * @param answer
	 * @param reason 不通过理由
	 */
	public boolean answer(Integer questionId, QuestionAnswerStatusDbEnum answer,String reason,CommonsMultipartFile[] files,HttpServletRequest request) throws Exception;
	/**
	 * 对作业问题的超时状态变更
	 * 1 班长提出的问题，超过（3天+延时小时）时间，技术工程师的状态还是处于待处理状态的，
	 * 2 将技术工程师的记录状态标识为超时，且将作业问题主状态标识为unsolved,主记录的超时状态标识加上
	 */

	public boolean timeoutCompute();
	
	/**
	 * 解决人无法解决当前问题，问题升级至当前解决人的上级人员
	 * @param questionId
	 * @param reason
	 * @param baseRequestForm
	 * @return
	 */
	public boolean unable(Integer questionId,String reason,BaseRequestForm baseRequestForm)  throws Exception;
	
	public Integer getStatistics(String type,Integer userId,String... status);
}
