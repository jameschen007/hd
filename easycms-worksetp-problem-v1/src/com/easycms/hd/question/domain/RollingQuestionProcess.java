package com.easycms.hd.question.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "rolling_question_process")
public class RollingQuestionProcess implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;
	/**
	 * 问题id
	 */
	@JsonIgnore
	@ManyToOne(targetEntity = RollingQuestion.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "question_id")
	private RollingQuestion question;
	/**
	 * 解决人id：即班长或技术
	 */
    @Column(name = "solver")
    private Integer solver;

	/**
	 * 1班长，2协调，3技术
	 */
	@Column(name = "solver_role")
	private Integer solverRole;
	/**
	 * '班长状态:pre待指派、done已指派，技术状态：pre待处理、done已处理、unsolved仍未解决、solved已解决',
	 */
	@Column(name = "status")
	private String status;
	/** 解决时间 */
	@Column(name = "solver_time")
	private Date solverTime;
	/** 创建时间 */
	@Column(name = "create_time")
	private Date createTime;

	/** 延时时间,延时结果时间,即到哪个时间就到要求时间了。要求的年月日时 */
	@Column(name = "delay_time")
	private Date delayTime;

	@Column(name="timeout")
	private Boolean timeout;

	/** 子表_不通过理由-20171025添加*/
	/**
	 * 组长回答不合格原因
	 */
	@Column(name="reason")
	private String reason;
	
	/**
	 * 无法解决的原因
	 */
	@Column(name="unable_reason")
	private String unableReason;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public RollingQuestion getQuestion() {
		return question;
	}
	public void setQuestion(RollingQuestion question) {
		this.question = question;
	}
	/**
	 * 1班长，2协调，3技术
	 */
	public Integer getSolverRole() {
		return solverRole;
	}
	/**
	 * 1班长，2协调，3技术
	 */
	public void setSolverRole(Integer solverRole) {
		this.solverRole = solverRole;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getSolverTime() {
		return solverTime;
	}
	public void setSolverTime(Date solverTime) {
		this.solverTime = solverTime;
	}
	public Integer getSolver() {
		return solver;
	}
	public void setSolver(Integer solver) {
		this.solver = solver;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getDelayTime() {
		return delayTime;
	}
	public void setDelayTime(Date delayTime) {
		this.delayTime = delayTime;
	}
	public Boolean getTimeout() {
		return timeout;
	}
	public void setTimeout(Boolean timeout) {
		this.timeout = timeout;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * 无法解决原因
	 * @return
	 */
	public String getUnableReason() {
		return unableReason;
	}
	/**
	 * 无法解决原因
	 * @param unableReason
	 */
	public void setUnableReason(String unableReason) {
		this.unableReason = unableReason;
	}
	
	
}
