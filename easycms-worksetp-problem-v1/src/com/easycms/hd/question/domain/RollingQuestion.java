package com.easycms.hd.question.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.easycms.core.form.BasicForm;
import com.easycms.management.user.domain.User;

@Entity
@Table(name = "rolling_question")
public class RollingQuestion extends BasicForm implements Serializable {

	private static final long serialVersionUID = -2221658589708419399L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;
	/**
	 * 父id，回执时，回执内容放在这个表子记录里面',
	 */
	@JsonIgnore
	@ManyToOne(targetEntity = RollingQuestion.class,cascade={CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@JoinColumn(name = "up_id")
//	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private RollingQuestion up;
	
	@JsonIgnore
	@OneToMany(targetEntity=RollingQuestion.class,cascade={CascadeType.REMOVE},fetch=FetchType.LAZY,mappedBy="up")
	private List<RollingQuestion> children;
	
//	/**
//	 * 作业条目号
//	 */ 
//	@Column(name = "drawno")
//	private String drawno;

//	/**
//	 * 焊口/支架 保留以前的，如果没有用，就删除
//	 */
//	@Column(name = "weldno")
//	private String weldno;
	
	
	@Column(name = "rolling_plan_id")
	private Integer rollingPlanId;
	/**
	 * 问题类型：tecknicalMatters技术问题,coordinationProblem协调问题
	 */
	@Column(name = "question_type")
	private String questionType;
	/**
	 * '状态:pre待解决、undo待确认、unsolved仍未解决、solved已解决'
	 */
	@Column(name = "status")
	private String status;
	/**
	 * 描述
	 */
	@Column(name = "describes")
	private String describe;
	/**
	 * 创建人id
	 */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	@NotFound(action=NotFoundAction.IGNORE)
	private User owner;
	/**
	 * 被指派解决人id（协调指派的工程师），
	 */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "assign_id")
	@NotFound(action=NotFoundAction.IGNORE)
	private User assign;
	/**
	 * 被指派协调人id（班长指派的谁）
	 */
	@JsonIgnore
	@ManyToOne(targetEntity = com.easycms.management.user.domain.User.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "coordinate_id")
	@NotFound(action=NotFoundAction.IGNORE)
	private User coordinate;
	
	/** 创建时间 */
	@Column(name = "question_time")
	private Date questionTime;
	
	/*
	 * 不通过理由
	 */
	@Column(name="reason")
	private String reason ;
	@Column(name="timeout")
	private Boolean timeout;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getRollingPlanId() {
		return rollingPlanId;
	}
	public void setRollingPlanId(Integer rollingPlanId) {
		this.rollingPlanId = rollingPlanId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Date getQuestionTime() {
		return questionTime;
	}
	public void setQuestionTime(Date questionTime) {
		this.questionTime = questionTime;
	}
	public RollingQuestion getUp() {
		return up;
	}
	public void setUp(RollingQuestion up) {
		this.up = up;
	}
	public List<RollingQuestion> getChildren() {
		return children;
	}
	public void setChildren(List<RollingQuestion> children) {
		this.children = children;
	}
	public User getAssign() {
		return assign;
	}
	public void setAssign(User assign) {
		this.assign = assign;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Boolean getTimeout() {
		return timeout;
	}
	public void setTimeout(Boolean timeout) {
		this.timeout = timeout;
	}
	public User getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(User coordinate) {
		this.coordinate = coordinate;
	}
	
}
