package com.easycms.hd.problem.service;


import com.easycms.hd.problem.domain.ProblemChangeTrigger;

public interface ProblemChangeTriggerService{
	ProblemChangeTrigger findByName(String name);

	ProblemChangeTrigger findById(int id);

	ProblemChangeTrigger update(ProblemChangeTrigger pct);
}
