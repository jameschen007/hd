package com.easycms.hd.problem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycms.hd.problem.dao.ProblemSolverDao;
import com.easycms.hd.problem.domain.ProblemSolver;
import com.easycms.hd.problem.service.ProblemSolverService;


@Service
public class ProblemSolverServiceImp implements ProblemSolverService{

	@Autowired
	private ProblemSolverDao problemSolverDao;
	@Override
	public ProblemSolver add(ProblemSolver p) {
		return problemSolverDao.save(p);
	}

	@Override
	public ProblemSolver update(ProblemSolver p) {
		return problemSolverDao.save(p);
	}

	@Override
	public ProblemSolver findById(Integer id) {
		return problemSolverDao.findById(id);
	}

	@Override
	public List<ProblemSolver> findByProblemId(Integer id) {
		return problemSolverDao.findByProblemId(id);
	}

	@Override
	public List<ProblemSolver> findByProblemIdAndUserId(int problemId,
			String userid) {
		return problemSolverDao.findByProblemIdAndUserId(problemId,userid);
	}

	@Override
	public List<ProblemSolver> findByUserId(String id) {
		return problemSolverDao.findByUserId(id);
	}

	@Override
	public List<ProblemSolver> findByUserIdAndStatus(String userid, int iS_OK) {
		return problemSolverDao.findByUserIdAndStatus(userid,iS_OK);
	}


}
