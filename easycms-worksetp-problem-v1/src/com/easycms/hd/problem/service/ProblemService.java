package com.easycms.hd.problem.service;

import java.util.List;















import com.easycms.core.util.Page;
import com.easycms.hd.problem.domain.Problem;

public interface ProblemService {
	public Problem add(Problem p);
	public Problem update(Problem p);
	public List<Problem> findByIds(Integer[] aids);

	public Problem findById(Integer id);
	public List<Problem> findByWorkstepId(int worstepid);
	public List<Problem> findByRollingPlanIdAndIsNotOk(int rolingPlanId);
	public List<Problem> findByUserId(String userid);
	public List<Problem> findByIdsAndStatus(Integer[] ids, int status);
	public List<Problem> findByUserIdAndConfirm(String userid, int confirm);
    
	//分页
	public Page<Problem> findByWorkstepId(int worstepid,Page<Problem> page);
	public Page<Problem> findByUserId(String userid,Page<Problem> page);
	public Page<Problem> findByIdsAndStatus(Integer[] ids, int status,Page<Problem> page);
	public Page<Problem> findByUserIdAndConfirm(String userid, int confirm,Page<Problem> page);
    public Page<Problem> findByIds(Integer[] ids,Page<Problem> page);
	public boolean delete(Problem p);
    public int countByUserId(String userid);
    public int findCountByUserIdAndConfirm(String userid, int i);
    public int findCountConcernedNotSolved(Integer[] ids);
    public int findByNeedToSolveCount(Integer[] ids);
	public List<Problem> findByRollingPlanId(int rid);
	public Page<Problem> findByRollingPlanId(int parseInt, Page<Problem> page);
    public int findCountConcerned(Integer[] ids);
    public List<Problem> findConfirmByIdsAndStatus(Integer[] ids, int i);
    public Page<Problem> findConfirmProblemsByIds(Integer[] ids, int i, Page<Problem> page);
    
	Page<Problem> findByIdsAndStatus(Integer[] ids, int status, String keyword,
			Page<Problem> page);
	Page<Problem> findByUserId(String userid, String keyword, Page<Problem> page);
	Page<Problem> findConfirmProblemsByIds(Integer[] ids, int i,
			String keyword, Page<Problem> page);
	Page<Problem> findByIds(Integer[] ids, String keyword, Page<Problem> page);
	Page<Problem> findAll(Page<Problem> page);
}
