package com.easycms.hd.problem.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.basic.BasicDao;
import com.easycms.hd.problem.domain.Problem;

@Transactional
public interface ProblemDao extends Repository<Problem,Integer>,BasicDao<Problem>{

	@Query("FROM Problem p where p.id in ?1 order by p.createOn desc")
	List<Problem> findByIdS(Integer[] ids);

	@Query("FROM Problem p where p.worstepid = ?1 order by p.createOn desc")
	List<Problem> findByWorkstepId(int worstepid);

	@Query("FROM Problem p where p.createdBy = ?1 order by p.createOn desc")
	List<Problem> findByUserId(String userid);

	@Query("FROM Problem p where p.id in ?1 and p.isOk = ?2 order by p.createOn desc")
	List<Problem> findByIdsAndStatus(Integer[] ids, int status);

	@Query("FROM Problem p where p.createdBy = ?1 and p.confirm = ?2 order by p.createOn desc")
   List<Problem> findByIdAndStatus(String userid, int con);
	
	@Query("FROM Problem p where p.id in ?1 and p.confirm = ?2 order by p.createOn desc")
	 List<Problem> findConfirmByIdsAndStatus(Integer[] ids, int i);
	
	@Query("FROM Problem p where p.rollingPlanId = ?1 and isok = 0 order by p.createOn desc")
	List<Problem> findByRollingPlanIdAndIsOk(int rolingPlanId);
	
	@Query("FROM Problem p where p.rollingPlanId = ?1 order by p.createOn desc")
	List<Problem> findByRollingPlanId(int rid);
	
	Page<Problem> findByWorstepidOrderByCreateOnDesc(int worstepid,Pageable page);
	Page<Problem> findByCreatedByOrderByCreateOnDesc(String userid,Pageable page);
	
	@Query("FROM Problem p where p.createdBy = ?1 and (p.drawno like ?2 or p.weldno like ?2) order by p.createOn desc")
	Page<Problem> findByCreatedByOrderByCreateOnDesc(String userid, String keyword, Pageable page);
	
	Page<Problem> findByIdInAndIsOkOrderByCreateOnDesc(Integer[] ids, int status,Pageable page);
	
	@Query("FROM Problem p where p.id in ?1 and p.isOk = ?2 and (p.drawno like ?3 or p.weldno like ?3) order by p.createOn desc")
	Page<Problem> findByIdInAndIsOkOrderByCreateOnDesc(Integer[] ids, int status, String keyword, Pageable page);
	Page<Problem> findByCreatedByAndConfirmOrderByCreateOnDesc(String userid, int con,Pageable page);
	Page<Problem> findByIdInAndConfirmOrderByCreateOnDesc(Integer[] ids, int con,Pageable page);
	
	@Query("FROM Problem p where p.id in ?1 and p.confirm = ?2 and (p.drawno like ?3 or p.weldno like ?3) order by p.createOn desc")
	Page<Problem> findByIdInAndConfirmOrderByCreateOnDesc(Integer[] ids, int con, String keyword, Pageable page);
	Page<Problem> findByIdInOrderByCreateOnDesc(Integer[] ids,Pageable page);
	
	@Query("FROM Problem p where p.id in ?1 and (p.drawno like ?2 or p.weldno like ?2) order by p.createOn desc")
	Page<Problem> findByIdInOrderByCreateOnDesc(Integer[] ids, String keyword, Pageable page);

	@Query("SELECT count(1) FROM Problem p where p.createdBy = ?1 order by p.createOn desc")
    int countByUserId(String userid);

	@Query("SELECT count(1) FROM Problem p where p.createdBy = ?1 and p.confirm = ?2 order by p.createOn desc")
    int findCountByUserIdAndConfirm(String userid, int i);

	@Query("SELECT count(1) FROM Problem p where p.id in ?1 and isOk = 0 order by p.createOn desc")
    int findCountConcernedNotSolved(Integer[] ids);

	@Query("SELECT count(1) FROM Problem p where p.id in ?1 and isOk = 0 order by p.createOn desc")
    int findByNeedToSolveCount(Integer[] ids);

	Page<Problem> findByRollingPlanIdOrderByCreateOnDesc(int id,
			Pageable pageable);

	@Query("SELECT count(1) FROM Problem p where p.id in ?1 order by p.createOn desc")
    int findCountConcerned(Integer[] ids);


	
}
