package com.easycms.hd.problem.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.easycms.basic.BasicDao;
import com.easycms.hd.problem.domain.ProblemFile;

@Transactional
public interface ProblemFileDao extends Repository<ProblemFile,Integer>,BasicDao<ProblemFile>{

	@Query("From ProblemFile pf where pf.problemid = ?1")
	List<ProblemFile> findByProblemId(Integer id);
	@Query("SELECT count(1) From ProblemFile pf where pf.problemid = ?1")
	int findCountByProblemId(Integer id);

}
