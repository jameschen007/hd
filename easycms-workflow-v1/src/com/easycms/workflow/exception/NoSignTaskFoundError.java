package com.easycms.workflow.exception;


public class NoSignTaskFoundError extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7043472125107236501L;

	public NoSignTaskFoundError() {
		super("No sign tasks found");
	}
	
	public NoSignTaskFoundError(String msg) {
		super(msg);
	}
	
}
