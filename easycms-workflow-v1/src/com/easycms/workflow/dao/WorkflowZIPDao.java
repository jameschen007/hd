package com.easycms.workflow.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.workflow.domain.WorkflowZIP;

@Transactional(readOnly=true)
public interface WorkflowZIPDao extends Repository<WorkflowZIP,Integer> {
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	WorkflowZIP findById(Integer id);
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	int countByFilename(String filename);
	
	@Query("FROM WorkflowZIP f WHERE f.id IN (?1)")
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<WorkflowZIP> findAll(Integer[] ids);

	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	List<WorkflowZIP> findAll();
	
	@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	Page<WorkflowZIP> findAll(Specification<WorkflowZIP> spec,Pageable pageable);
	
	int deleteById(int id);
	/**
	 * 删除所有
	 * @return
	 */
	@Modifying
	@Query("DELETE FROM WorkflowZIP")
	int deleteAll();
	/**
	 * 删除指定
	 */
	@Modifying
	@Query("DELETE From WorkflowZIP f WHERE f.id IN (?1)")
	int deleteAll(Integer[] ids);
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED)
	WorkflowZIP save(WorkflowZIP flowZip);
}
