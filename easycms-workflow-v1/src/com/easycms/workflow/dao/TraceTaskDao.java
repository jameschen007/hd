package com.easycms.workflow.dao;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.easycms.workflow.domain.TraceTask;

@Transactional(readOnly=true)
public interface TraceTaskDao extends Repository<TraceTask,Integer> {
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED)
	TraceTask save(TraceTask traceTask);
	@Query("From TraceTask")
	TraceTask findByTaskId(String taskId);
	@Query("From TraceTask")
	List<TraceTask> findByCurrentHandleId(String currentHandleId);
	@Query("From TraceTask")
	List<TraceTask> findByCurrentHandleIdAndTaskStatus(String currentHandleId,String taskStatus);
	@Query("From TraceTask")
	List<TraceTask> findByDepolyId(String deployId);
	@Query("From TraceTask")
	List<TraceTask> findByDepolyIdAndTaskStatus(String deployId,String taskStatus);
	@Query("From TraceTask")
	Page<TraceTask> findByPage(Specification<TraceTask> sepc,Pageable pageable);
	/**
	 * 删除指定
	 */
	@Modifying
	@Query("DELETE From TraceTask t WHERE t.traceId IN (?1)")
	int deleteAll(String[] ids);
	
	@Modifying
	@Query("DELETE From TraceTask t WHERE t.pid IN (?1)")
	int deleteAllByProcessInstanceIds(String[] processInstanceIds);
}
