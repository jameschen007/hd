package com.easycms.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rd_workflow_flow_zip")
@Cacheable
public class WorkflowZIP implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1677689130385357159L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;	//ID
	
	private String filename;	//文件名称
	
	private String realname;	//文件真实名字
	
	private String path;	//相对路径
	
	@Column(name="real_path")
	private String realPath;	//真实相对路径
	
	@Column(name="upload_date")
	private Date uploadDate;	//创建时间

	private Integer version;	//文件版本
	
	private Integer status=0;		//文件状态，0未发布，1已发布
	
	@Column(name="deployment_id")
	private String deploymentId;	//发布ID

	@Column(name="deployment_name")
	private String deploymentName;	//发布名称
	
	@Column(name="deployment_date")
	private Date deploymentDate;	//发布日期
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRealPath() {
		return realPath;
	}

	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getDeploymentName() {
		return deploymentName;
	}

	public void setDeploymentName(String deploymentName) {
		this.deploymentName = deploymentName;
	}

	public Date getDeploymentDate() {
		return deploymentDate;
	}

	public void setDeploymentDate(Date deploymentDate) {
		this.deploymentDate = deploymentDate;
	}
	
	
	
}
