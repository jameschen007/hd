package com.easycms.workflow.directive;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.core.freemarker.FreemarkerTemplateUtility;
import com.easycms.core.util.Page;
import com.easycms.workflow.domain.WorkflowZIP;
import com.easycms.workflow.service.RDWorkflowService;

import freemarker.template.TemplateDirectiveModel;

/**
 * 获取来源信息标签
 * 
 * @author jiepeng
 * 
 */
@Component
public class WorkflowZIPDirective extends BaseDirective<WorkflowZIP> implements
		TemplateDirectiveModel {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(WorkflowZIPDirective.class);
	@Autowired
	private RDWorkflowService workflowService;

	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected WorkflowZIP field(Map params, Map<String, Object> envParams) {
		// #########################################################
		// 若有ID信息
		// #########################################################获取用户Id
		Integer id = FreemarkerTemplateUtility.getIntValueFromParams(params,
				"id");
		if (id != null) {
			logger.debug("[id] ==> " + id);
			WorkflowZIP flowZip = workflowService.findFlowZip(id);
			return flowZip;
		}
		return null;
	}

	@Override
	protected List<WorkflowZIP> list(Map params, String filter, String order,
			String sort, boolean pageable, Page<WorkflowZIP> pager,
			Map<String, Object> envParams) {
		// #########################################################
		// 若没有ID信息
		// #########################################################
		List<WorkflowZIP> list = workflowService.findFlowZip();
		return list;
	}

	@Override
	protected List<WorkflowZIP> tree(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
