package com.easycms.workflow.directive;

import java.util.List;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycms.basic.BaseDirective;
import com.easycms.core.util.Page;

import freemarker.template.TemplateDirectiveModel;

/**
 * 获取来源信息标签
 * 
 * @author jiepeng
 * 
 */
@Component
public class WorkflowProcessDefDirective extends BaseDirective<ProcessDefinition> implements TemplateDirectiveModel {
	@Autowired
	private RepositoryService repositoryService;

	@Override
	protected Integer count(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ProcessDefinition field(Map params, Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<ProcessDefinition> list(Map params, String filter,
			String order, String sort, boolean pageable,
			Page<ProcessDefinition> pager, Map<String, Object> envParams) {
		ProcessDefinitionQuery query = repositoryService
				.createProcessDefinitionQuery();
		List<ProcessDefinition> list = query.active().list();
		return list;
	}

	@Override
	protected List<ProcessDefinition> tree(Map params,
			Map<String, Object> envParams) {
		// TODO Auto-generated method stub
		return null;
	}

}
