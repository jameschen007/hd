package com.easycms.workflow.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.workflow.form.RDForm;
import com.easycms.workflow.service.TraceTaskService;
import com.easycms.workflow.util.WorkflowHelper;

@Controller
@RequestMapping("/workflow/prodef")
public class WorkflowProcessDefController {

	private static final Logger logger = Logger
			.getLogger(WorkflowProcessDefController.class);

	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private TraceTaskService traceTaskService;
	@Autowired
	private WorkflowHelper bpmnUtil;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility
				.forwardAdminView("/workflow/list_process_def");
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String data(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility
				.forwardAdminView("/workflow/data/data_json_process_def");
	}

	@RequestMapping(value = "/delete")
	public void delete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RDForm form)
			throws Exception {
		Boolean val = false;
		String[] ids = form.getDeploymentId();
		if (ids == null || ids.length == 0) {
			HttpUtility.writeToClient(response, val.toString());
			return;
		}

		try {
			for (String id : ids) {
				repositoryService.deleteDeployment(id);
			}
			val = true;
			HttpUtility.writeToClient(response, val.toString());
		} catch (Exception e) {
			HttpUtility.writeToClient(response, e.getMessage());
		}
	}

	@RequestMapping(value = "/delete/demand")
	public void demandDelete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RDForm form)
			throws Exception {
		Boolean val = false;
		String[] ids = form.getDeploymentId();
		if (ids == null || ids.length == 0) {
			HttpUtility.writeToClient(response, val.toString());
			return;
		}

		try {
			for (String id : ids) {
				ProcessDefinition pd = repositoryService
						.createProcessDefinitionQuery().deploymentId(id)
						.singleResult();
//				 if(pd == null) {
//				 HttpUtility.writeToClient(response, val.toString());
//				 return;
//				 }
				List<ProcessInstance> piList = runtimeService
						.createProcessInstanceQuery()
						.processDefinitionId(pd.getId()).list();
				List<String> piIdList = new ArrayList<String>();
				for (ProcessInstance pi : piList) {
					piIdList.add(pi.getId());
				}
				String[] array = new String[piIdList.size()];
				if(array != null && array.length > 0) {
					traceTaskService.delete(piIdList.toArray(array));
				}
				repositoryService.deleteDeployment(id, true);
			}
			val = true;
			HttpUtility.writeToClient(response, val.toString());
		} catch (Exception e) {
			// HttpUtility.writeToClient(response, e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 显示流程图
	 * 
	 * @param request
	 * @param response
	 * @param form
	 * @throws Exception
	 */
	@RequestMapping(value = "/def")
	public String defImg(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") RDForm form)
			throws Exception {
		if (form.getDeploymentId() == null
				|| form.getDeploymentId().length == 0) {
			return ForwardUtility
					.forwardAdminView("/workflow/modal_workflow_process_img");
		}

		if ("img".equals(form.getType())) {
			String deploymentId = form.getDeploymentId()[0];
			bpmnUtil.createImg(response, deploymentId);
		} else {
			return ForwardUtility
					.forwardAdminView("/workflow/modal_workflow_process_img");
		}
		return null;
	}


}
