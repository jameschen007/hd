package com.easycms.workflow.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycms.common.upload.FileInfo;
import com.easycms.common.upload.UploadUtil;
import com.easycms.common.util.CommonUtility;
import com.easycms.common.util.FileUtils;
import com.easycms.core.form.BasicForm;
import com.easycms.core.util.ForwardUtility;
import com.easycms.core.util.HttpUtility;
import com.easycms.workflow.domain.WorkflowZIP;
import com.easycms.workflow.service.RDWorkflowService;

@Controller
@RequestMapping("/workflow/zips")
public class WorkflowZIPController {

	private static final Logger logger = Logger
			.getLogger(WorkflowZIPController.class);

	@Autowired
	private RDWorkflowService workflowService;
	@Autowired
	private RepositoryService repositoryService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility
				.forwardAdminView("/workflow/list_flowzip");
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String data(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return ForwardUtility
				.forwardAdminView("/workflow/data/data_json_zips");
	}

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String uploadUI(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return ForwardUtility
				.forwardAdminView("/workflow/modal_workflow_upload");
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void upload(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "type", required = false) String type)
			throws Exception {

		// ####################################################
		// 检查文件是否存在
		// #########################################################
		if ("check".equals(type)) {
			HttpUtility.writeToClient(response, "1");
			logger.debug("Check file is exist or not");
			return;
		}

		// ####################################################
		// 不存在则添加
		// #########################################################

		List<FileInfo> fileList = UploadUtil.upload("/WEB-INF/workflow",
				"utf-8", request);
		WorkflowZIP flowZip = new WorkflowZIP();
		if (fileList != null && fileList.size() > 0) {
			FileInfo f = fileList.get(0);
			flowZip.setFilename(f.getFilename());
			flowZip.setRealname(f.getNewFilename());
			flowZip.setPath(f.getRelativePath());
			flowZip.setRealPath(f.getRealRelativePath());
			flowZip.setUploadDate(new Date());

			workflowService.addFlowZip(flowZip);
		}

		HttpUtility.writeToClient(response,
				CommonUtility.toJson(flowZip.getId() != null));
	}

	@RequestMapping(value = "/delete")
	public void delete(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") BasicForm form)
			throws Exception {
		Boolean val = false;
		Integer[] ids = form.getIds();

		if (ids == null || ids.length == 0) {
			HttpUtility.writeToClient(response, val.toString());
			return;
		}

		List<WorkflowZIP> list = workflowService.findFlowZip(ids);
		for (WorkflowZIP flow : list) {
			FileUtils.remove(flow.getRealPath());
		}
		val = workflowService.deleteFlowZip(ids);

		HttpUtility.writeToClient(response, val.toString());
	}

	@RequestMapping(value = "/deploy")
	public void deploy(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("form") BasicForm form)
			throws Exception {
		
		Integer[] ids = form.getIds();
		if(ids == null || ids.length == 0) {
			HttpUtility.writeToClient(response, "false");
			return;
		}
		
		WorkflowZIP flowZip = workflowService.findFlowZip(ids[0]);
		
		if(flowZip == null) {
			HttpUtility.writeToClient(response, "false");
		}
		
		// 获取在classpath下的流程文件
		String zipPath = HttpUtility.getServletContext().getRealPath(flowZip.getRealPath());
		File file = null;
		try {
			file = new File(zipPath);
			InputStream in = new FileInputStream(file);
			ZipInputStream zipInputStream = new ZipInputStream(in);
			// 使用deploy方法发布流程
			Deployment deployment = repositoryService.createDeployment()
					.addZipInputStream(zipInputStream)
					.name(flowZip.getFilename())
					.deploy();
			if (CommonUtility.isNonEmpty(deployment.getId())) {
				flowZip.setStatus(1);
				flowZip.setDeploymentId(deployment.getId());
				flowZip.setDeploymentName(deployment.getName());
				flowZip.setDeploymentDate(deployment.getDeploymentTime());
				workflowService.updateFlowZip(flowZip);
				HttpUtility.writeToClient(response, "true");
			} else {
				HttpUtility.writeToClient(response, "false");
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
			HttpUtility.writeToClient(response, "false");
			e.printStackTrace();
		}
		
	}

}
