package com.easycms.workflow.form;

import java.io.Serializable;

public class WorkflowForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1677689130385357159L;
	private String id;	//ID
	private String processName;	//流程名字
	private String queryType;	//流程类型
	private String username;	//用户名
	private String status;	//状态
	private String assignee;	//代理人
	private String[] handle;	//处理信息
	
	private String leaveType;
	private String leaveDescription;
	private Integer userId;	//userId
	private Integer roleId;	//userId
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getLeaveDescription() {
		return leaveDescription;
	}
	public void setLeaveDescription(String leaveDescription) {
		this.leaveDescription = leaveDescription;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String[] getHandle() {
		return handle;
	}
	public void setHandle(String[] handle) {
		this.handle = handle;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
}
