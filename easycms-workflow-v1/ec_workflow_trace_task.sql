/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : easycms_workflow

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-30 00:07:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ec_workflow_trace_task`
-- ----------------------------
DROP TABLE IF EXISTS `ec_workflow_trace_task`;
CREATE TABLE `ec_workflow_trace_task` (
  `trace_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '跟踪ID',
  `pid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '流程实例ID',
  `pdid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '流程定义ID',
  `deploy_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发布人ID',
  `current_handle_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '当前处理人id',
  `pre_handle_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '前处理人ID',
  `task_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节点ID',
  `pre_task_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '前节点ID',
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `handle_date` date DEFAULT NULL COMMENT '处理时间',
  `duration` int(11) DEFAULT NULL COMMENT '持续时间',
  `task_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务状态',
  PRIMARY KEY (`trace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ec_workflow_trace_task
-- ----------------------------
