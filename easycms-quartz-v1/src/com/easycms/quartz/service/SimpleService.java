package com.easycms.quartz.service;

import java.lang.reflect.Method;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import com.easycms.quartz.util.SpringContextsUtil;

@Service("simpleService")
public class SimpleService {
	
	private static final Logger logger = LoggerFactory.getLogger(SimpleService.class);

	public void doExecute(JobDataMap jobDataMap) {
		logger.info("Get into SimpleService");
		
		Method  mh = ReflectionUtils.findMethod(SpringContextsUtil.getBean(jobDataMap.getString("jobService")).getClass(), "doJob",new Class[]{Object.class} );
        Object obj = ReflectionUtils.invokeMethod(mh,  SpringContextsUtil.getBean(jobDataMap.getString("jobService")), jobDataMap.get("jobDataMap"));
        logger.info("obj = " + obj);
	}
}
