package com.easycms.common.poi.excel;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.easycms.common.util.CommonUtility;

import jodd.bean.BeanUtil;
import jodd.datetime.JDateTime;
import jodd.util.StringUtil;

/**
 * Excel帮助类
 *,String dateFormat
 */
public class ExcelHelperV2 {
    private static ExcelHelperV2 helper = null;
     
    private ExcelHelperV2() {
         
    }
     
    public static synchronized ExcelHelperV2 getInstanse() {
        if(helper == null) {
            helper = new ExcelHelperV2();
        }
        return helper;
    }
     
    /**
     * 将Excel文件导入到list对象
     * @param head  文件头信息
     * @param file  导入的数据源
     * @param cls   保存当前数据的对象
     * @return
     * @throws Exception 
     */
    @SuppressWarnings("rawtypes")
	public List importToObjectList(ExcelHead head, InputStream is, Class cls, String sheetName) throws Exception {
        List contents = null;
        // 根据excel生成list类型的数据
        List<List> rows;
        rows = excelFileConvertToList(is, sheetName);
         
        // 删除头信息
        for (int i = 0; i < head.getRowCount(); i++) {
            rows.remove(0);
        }
         
        // 将表结构转换成Map
        Map<Integer, String> excelHeadMap = convertExcelHeadToMap(head.getColumns());
        
        // 构建为对象
        contents = buildDataObject(excelHeadMap, head.getColumnsConvertMap(), rows, cls);
        return contents;
    }
     
    /**
     * 导出数据至Excel文件
     * @param excelColumns  报表头信息
     * @param excelHeadConvertMap   需要对数据进行特殊转换的列
     * @param modelFile  模板Excel文件
     * @param outputFile 导出文件
     * @param dataList  导入excel报表的数据来源
     * @throws IOException 
     * @throws InvalidFormatException 
     */
    public void exportExcelFile(ExcelHead head, File modelFile, File outputFile, List<?> dataList,String dateFormat) throws IOException, InvalidFormatException {
        // 读取导出excel模板
        InputStream inp = null;
        Workbook wb = null;
        inp = new FileInputStream(modelFile);
        wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);
        // 生成导出数据
        buildExcelData(sheet, head, dataList,dateFormat);
         
        // 导出到文件中
        FileOutputStream fileOut = new FileOutputStream(outputFile);
        wb.write(fileOut);
        fileOut.close();
    }
    
    public void exportExcelFile(ExcelHead head, ServletOutputStream outputStream, List<?> dataList,String dateFormat) throws IOException, InvalidFormatException {
        // 读取导出excel模板
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("1");
        // 生成导出数据
        buildExcelData(sheet, head, dataList,dateFormat);
        // 导出到文件中
        
        wb.write(outputStream);
        outputStream.close();
    }
    public void exportExcelFile(ExcelHead head,InputStream modelFileIs, ServletOutputStream outputStream, List<?> dataList,String dateFormat) throws IOException, InvalidFormatException {
        // 读取导出excel模板
        Workbook wb = null;
        wb = WorkbookFactory.create(modelFileIs);
        Sheet sheet = wb.getSheetAt(0);
        // 生成导出数据
        buildExcelData(sheet, head, dataList,dateFormat);
        // 导出到文件中
        
        wb.write(outputStream);
        outputStream.close();
    }
    public void exportExcelFile(ExcelHead head,File modelFile, ServletOutputStream outputStream, List<?> dataList,String dateFormat) throws IOException, InvalidFormatException {
        // 读取导出excel模板
    	InputStream inp = null;
        Workbook wb = null;
        inp = new FileInputStream(modelFile);
        wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);
        // 生成导出数据
        buildExcelData(sheet, head, dataList,dateFormat);
        // 导出到文件中
        
        wb.write(outputStream);
        outputStream.close();
    }
     
    /**
     * 将报表结构转换成Map
     * @param excelColumns
     */
    private Map<Integer, String> convertExcelHeadToMap(List<ExcelColumn> excelColumns) {
        Map<Integer, String> excelHeadMap = new HashMap<Integer, String>();
        for (ExcelColumn excelColumn : excelColumns) {
            if(StringUtil.isEmpty(excelColumn.getFieldName())) {
                continue;
            } else {
                excelHeadMap.put(excelColumn.getIndex(), excelColumn.getFieldName());
            }
        }
        return excelHeadMap;
    }
     
    /**
     * 生成导出至Excel文件的数据
     * @param sheet 工作区间
     * @param excelColumns  excel表头
     * @param excelHeadMap  excel表头对应实体属性
     * @param excelHeadConvertMap   需要对数据进行特殊转换的列
     * @param dataList      导入excel报表的数据来源
     */
    @SuppressWarnings("rawtypes")
	private void buildExcelData(Sheet sheet, ExcelHead head, List<?> dataList,String dateFormat) {
        List<ExcelColumn> excelColumns = head.getColumns(); 
        Map<String, Map> excelHeadConvertMap = head.getColumnsConvertMap();
         
        // 将表结构转换成Map
        Map<Integer, String> excelHeadMap = convertExcelHeadToMap(excelColumns);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
         
        // 从第几行开始插入数据
        int startRow = head.getRowCount();
        int order = 1;
        for (Object obj : dataList) {
            Row row = sheet.createRow(startRow++);
            for (int j = 0; j < excelColumns.size(); j++) {
                Cell cell = row.createCell(j);
                cell.setCellType(excelColumns.get(j).getType());
                String fieldName = excelHeadMap.get(j);
                if(fieldName != null) {
                    Object valueObject = BeanUtil.getProperty(obj, fieldName);
                     
                    // 如果存在需要转换的字段信息，则进行转换
                    if(excelHeadConvertMap != null && excelHeadConvertMap.get(fieldName) != null) {
                        valueObject = excelHeadConvertMap.get(fieldName).get(valueObject);
                    }
                     
                    if(valueObject == null) {
                        cell.setCellValue("");
                    } else if (valueObject instanceof Integer) {
                        cell.setCellValue((Integer)valueObject);
                    } else if (valueObject instanceof String) {
                        cell.setCellValue((String)valueObject);
                    } else if (valueObject instanceof Date) {
                    	String dat = sdf.format(valueObject);
                        cell.setCellValue(dat);
                    } else {
                        cell.setCellValue(valueObject.toString());
                    }
                } else {
                    cell.setCellValue(order++);
                }
            }
        }
    }
     
    /**
     * 将Excel文件内容转换为List对象
     * @param fis   excel文件
     * @return  List<List> list存放形式的内容
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unused" })
	public List<List> excelFileConvertToList(InputStream is, String sheetName) throws Exception {
        Workbook wb = WorkbookFactory.create(is);
        
        Sheet sheet;
        if (null == sheetName || "".equals(sheetName)){
        	sheet = wb.getSheetAt(0);
        } else {
        	sheet = wb.getSheet(sheetName);
        }
         
        List<List> rows = new ArrayList<List>();
        
        for (Row row : sheet) {
            List<Object> cells = new ArrayList<Object>();
            
            for (int i = 0; i < row.getLastCellNum(); i++) {
            	 Cell cell = row.getCell(i);
            	
                Object obj = null;
                
                if (cell == null){
                	cells.add(obj);
                	continue;
                }
                 
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
 
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        obj = cell.getRichStringCellValue().getString();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            obj = CommonUtility.formateDate(cell.getDateCellValue(), "yyyy-MM-dd");
                        } else {
                            obj = cell.getNumericCellValue();
                        }
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        obj = cell.getBooleanCellValue();
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        obj = cell.getNumericCellValue();
                        break;
                    default:
                        obj = null;
                }
                cells.add(obj);
            }
            rows.add(cells);
        }
        return rows;
    }
     
    /**
     * 根据Excel生成数据对象
     * @param excelHeadMap 表头信息
     * @param excelHeadConvertMap 需要特殊转换的单元
     * @param rows
     * @param cls 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private List buildDataObject(Map<Integer, String> excelHeadMap, Map<String, Map> excelHeadConvertMap, List<List> rows, Class cls) throws InstantiationException, IllegalAccessException {
        List contents = new ArrayList();
        for (List list : rows) {
            // 如果当前第一列中无数据,则忽略当前行的数据
            if(list == null || list.get(0) == null) {
                break;
            }
            // 当前行的数据放入map中,生成<fieldName, value>的形式
            Map<String, Object> rowMap = rowListToMap(excelHeadMap, excelHeadConvertMap, list);
            
            // 将当前行转换成对应的对象
            Object obj = null;
            obj = cls.newInstance();
            BeanUtil.populateBean(obj, rowMap);
             
            contents.add(obj);
        }
        return contents;
    }
     
    /**
     * 将行转行成map,生成<fieldName, value>的形式
     * @param excelHeadMap 表头信息
     * @param excelHeadConvertMap excelHeadConvertMap
     * @param list
     * @return
     */
    @SuppressWarnings("rawtypes")
	private Map<String, Object> rowListToMap(Map<Integer, String> excelHeadMap, Map<String, Map> excelHeadConvertMap, List list) {
        Map<String, Object> rowMap = new HashMap<String, Object>();
        for(int i = 0; i < list.size(); i++) {
            String fieldName =  excelHeadMap.get(i);
            // 存在所定义的列
            if(fieldName != null) {
                Object value = list.get(i);
                if(excelHeadConvertMap != null && excelHeadConvertMap.get(fieldName) != null) {
                    value = excelHeadConvertMap.get(fieldName).get(value);
                }
                rowMap.put(fieldName, value);
            }
        }
        return rowMap;
    }
}