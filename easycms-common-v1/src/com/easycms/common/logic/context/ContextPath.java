package com.easycms.common.logic.context;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.easycms.common.logic.context.constant.EnpConstant;

/**
 * 需求：问题上传的图片需要放在单独的硬盘空间管理，
 * 这里将图片的路径做一定的逻辑处理。
 * @author wangzhi
 *
 */
public class ContextPath {
	
	private static Logger logger = Logger.getLogger(ContextPath.class);

	public static String os_name = null ;
	/**
	 * 当前工程运行的项目：是福清吗？
	 * 用于个性化　20180727王志，关于福清二级QC修改一级QC问题，APP修改为一级QC，使用现有的QC1流程，把QC2中的见证员后面的监理，工程公司，业主选点加载到QC1流程后面，QC2流程全部取消
	 * 首先根据ip判断是福清ip时，会设置为true .如果　为false时，会在数据库中查询一下配置　，如果配置了，使用配置的值。
	 */
	public static Boolean qc1ReplaceQc2=false ;

	/**
	 * 是否是王志本机，如果是，为了测试方便，登录不需要输入密码
	 */
	public static Boolean WZ=false ;
	/**
	 * 是否是王志本机，如果是，为了测试方便，登录不需要输入密码
	 */
	public static Boolean FQ=false ;
	/**
	 * 本机IP，用于根据不同机器判断不同逻辑
	 */
	public static String hostAddress = null ;
	static {
		Properties props=System.getProperties(); //获得系统属性集 
		os_name = props.getProperty("os.name");//Linux  TTs.java
		
		
	    StringBuilder IFCONFIG=new StringBuilder();  
	       try {  
	           for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {  
	               NetworkInterface intf = en.nextElement();  
	               for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {  
	                   InetAddress inetAddress = enumIpAddr.nextElement();  
	                   if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {  
	                   IFCONFIG.append(inetAddress.getHostAddress().toString()+"\n");  
	                   }  
	      
	               }  
	           }  
	       } catch (SocketException ex) {  
	       }  
	       
	       hostAddress = IFCONFIG.toString().trim();
	       
	       logger.info("hostAddress="+hostAddress);
	       System.out.println("hostAddress="+hostAddress);

	 		logger.info("初始化Enpower内网地址="+ContextPath.hostAddress);

	 		List<String> wzIpList = new ArrayList<String>();
	 		wzIpList.add("192.168.99.160");
	 		wzIpList.add("192.168.99.159");
	 		wzIpList.add("192.168.56.1");

			String wzCatalina = props.getProperty("catalina.home");
			logger.info(wzCatalina);
			logger.info("C:\\Program Files\\apache-tomcat-7.0.39".equals(wzCatalina));
	 		if(wzIpList.contains(hostAddress)||"C:\\Program Files\\apache-tomcat-7.0.39".equals(wzCatalina)){//本机不修改Enpower接口地址
				logger.info("本机不修改Enpower接口地址");
				WZ=true ;
//
//	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE;
//	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE;
	 			
//	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_FQ;
//	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_FQ;
	 			
	 		}else if(ContextPath.hostAddress.contains(ConstantVar.HOST_ADDRESS_K2K3) ){//卡项
	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_K;
	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_K;
	 		}else if(ContextPath.hostAddress.contains(ConstantVar.HOST_ADDRESS_HY) ){//海阳
	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_HY;
	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_HY;
	 		}else if(ContextPath.hostAddress.contains(ConstantVar.HOST_ADDRESS_SM) ){//三门
	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_SM;
	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_SM;
	 		}else if(ContextPath.hostAddress.contains(ConstantVar.HOST_ADDRESS_XP) ){//霞浦
	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_XP;
	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_XP;
	 		}else if(ContextPath.hostAddress.contains(ConstantVar.HOST_ADDRESS_FQ)){//福清
	 			EnpConstant.PROJ_CODE =EnpConstant.PROJ_CODE_FQ;
	 			EnpConstant.COMP_CODE =EnpConstant.COMP_CODE_FQ;
	 			qc1ReplaceQc2 =true ;
	 			FQ=true ;
	 		}
	 		String show = "访问Enpower接口使用项目代号"+EnpConstant.PROJ_CODE+" "+EnpConstant.COMP_CODE +"   qc1ReplaceQc2标示："+qc1ReplaceQc2;
	 		
	 		logger.info(show);
	}
	public static String Linux="Linux";
	
	
	/**
	 * 本工程的项目访问上下文地址，如：http://39.108.165.171/或 http://localhost:8080/easycms-website/
	 * 
	 * 这个如果外网映射，或通过了代理，可能存在问题
	 */
	public static String selfProjectContextPath = null ;
	public static String selfProjectContextPath_test = null ;
	
	/**
	 * 附件路径，从api.properties读取到。请统一使用这个变量，这个是在初始化时，根据操作系统类型进行过判断的。
	 */
	public static String fileBasePath = null ;
	public static String fileBasePath_test = null ;
	public static String winfileBasePath = null ;
	public static String winfileBasePath_test = null ;
	
	//com.easycms.common.util.FileUtils.questionFilePath
}
