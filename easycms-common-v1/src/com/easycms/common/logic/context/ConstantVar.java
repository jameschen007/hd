package com.easycms.common.logic.context;

public class ConstantVar {

/**程序中需要使用的键 PROJ_CODE**/
	public final static String PROJ_CODE="PROJ_CODE";
	/**程序中需要使用的键 COMP_CODE**/
	public final static String COMP_CODE="COMP_CODE";
	/**程序中需要使用的键  appWwUrl**/
	public final static String appWwUrl="appWwUrl";
	/**程序中需要使用的键  projectName**/
	public final static String projectName="projectName";
	
	public final static String okCode="1000";
	
/**作业问题模块,当前登录人的角色,去variable_set是按roleType查询出的value值,对应的key值*/
	public final static String solver="solver";
	public final static String supervisor="supervisor";//技术主管　相当于是　solver4
	public final static String solver3="solver3";//作业问题处理人　在变量表中临时配置的转换key

	public final static String solver4="solver4";
	public final static String solver5="solver5";
	public final static String solver6="solver6";
	public final static String solver7="solver7";
	public final static String solver8="solver8";
/**作业问题模块,当前登录人的角色,去variable_set是按roleType查询出的value值,对应的key值*/
	public final static String COORDINATOR="coordinator";
	public final static String TEAM="team";
	public final static String MONITOR="monitor";


	/**
	 * 
	 * Enpower地址：http://10.1.1.81:8018
	 * 
	 * 五公司正式机地址App外网地址 http://116.236.114.61:9200
	 * 五公司正式机地址App内网地址 http://10.1.1.104:9201
	 * 
	 * */
	public static String HOST_ADDRESS10 = "10.1.1.104" ;
	/**
	 * 福清api机器地址
	 */
	public static String HOST_ADDRESS_FQ = "10.2.1.54" ;
	/**
	 * 海阳api机器地址192.168.201.20
	 */
	public static String HOST_ADDRESS_HY = "192.168.201.20" ;
	/**
	 *  霞浦api机器地址10.8.1.12
	 */
	public static String HOST_ADDRESS_XP = "10.8.1.12" ;
	/**
	 * 三门api机器地址
	 */
	public static String HOST_ADDRESS_SM = "172.23.0.211";
	/**
	 * k2k3 国外　api机器地址
	 */
	public static String HOST_ADDRESS_K2K3 ="10.7.1.6";// "10.7.1.30" ;
	

	public static String before = "before" ;
	public static String after = "after" ;

	public static String projBegin="中核五公司";
	public static String projEnd="项目部";

	/**
	 * 质量管理功能中的QC　和　滚动计划中消点的QC是相同的操作人员
	 * */
	public static String witness_team_qc1="witness_team_qc1";
	public static String witness_team_qc2="witness_team_qc2";
	public static String witness_member_qc1="witness_member_qc1";
	public static String witness_member_qc2="witness_member_qc2";
	public static String qc_member="qc_member";
	
	//质量管理的常量
	public static String workshop = "workshop";
	public static String room = "room";
	

	/**
	 * 项目总经理
	 */
	public static String project_general_manager = "project_general_manager";
	
	/**
	 * 项目部副总经理
	 */
	public static String project_vice_manager = "project_vice_manager";
	
	/**
	 * 技术经理
	 */
	public static String technical_manager = "solver4";//注意已经存在

	/**
	 * 技术部副经理
	 */
	public static String technical_dept_vice_manager = "technical_dept_vice_manager";
	/**
	 * 技术部经理
	 */
	public static String technical_dept_manager = "technical_dept_manager";
	/**
	 * 工程经理
	 */
	public static String engineering_manager = "engineering_manager";
	/**
	 * 工程部经理
	 */
	public static String engineering_dept_manager = "engineering_dept_manager";
	/**
	 * 工程部副经理
	 */
	public static String engineering_dept_vice_manager = "engineering_dept_vice_manager";
	/**
	 * 商务经理
	 */
	public static String commerce_manager = "commerce_manager";
	/**
	 * 商务部经理
	 */
	public static String commerce_dept_manager = "commerce_dept_manager";
	/**
	 * 商务部副经理
	 */
	public static String commerce_dept_vice_manager = "commerce_dept_vice_manager";
	/**
	 * QHSE经理
	 */
	public static String QHSE_manager = "QHSE_manager";
	/**
	 * QC部经理
	 */
	public static String QC_dept_manager = "QC_dept_manager";
	/**
	 * QC部副经理
	 */
	public static String QC_dept_vice_manager = "QC_dept_vice_manager";
	/**
	 * QA部经理
	 */
	public static String QA_dept_manager = "QA_dept_manager";
	/**
	 * QA部副经理
	 */
	public static String QA_dept_vice_manager = "QA_dept_vice_manager";

	/**
	 * HSE部经理
	 */
	public static String HSE_dept_manager = "HSE_dept_manager";
	/**
	 * HSE部副经理
	 */
	public static String HSE_dept_vice_manager = "HSE_dept_vice_manager";
}
