package com.easycms.common.upload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.easycms.common.util.CommonUtility;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class UploadUtilToday {

	private static final Logger logger = Logger.getLogger(UploadUtilToday.class);
	private static final String defaultEncode = "utf-8";

	// public static final int RELATIVE_PATH = 0;
	// public static final int ABSOLUTE_PATH = 1;

	/**
	 * 上传文件
	 * 
	 * @param targetDir
	 *            绝对路径
	 * @param encode
	 *            编码
	 * @param rename
	 *            是否重命名
	 * @param request
	 *            HttpServletRequest对象
	 * @param pathType
	 *            上传文件路径是相对路径还是绝对路径,RELATIVE_PATH/ABSOLUTE_PATH
	 * 
	 * @return Map<String,String> 包含文件原始完整名称和路径的集合
	 */
	public static List<FileInfo> upload(String targetDir, String encode,
			boolean rename, HttpServletRequest request,boolean today) {
		List<FileInfo> fileInfoList = null;

		fileInfoList = executeUpload(targetDir, encode, rename, request,
				fileInfoList,today);

		return fileInfoList;
	}
	/**
	 * 一次上传多个文件
	 * @param targetDir
	 * @param encode
	 * @param rename
	 * @param request
	 * @return
	 */
	public static List<FileInfo> moreUpload(String targetDir, String encode,
			boolean rename,CommonsMultipartFile files[],boolean today) {
		List<FileInfo> fileInfoList = new ArrayList<FileInfo>();

		fileInfoList = executeMoreUpload(targetDir, encode, rename,files,
				fileInfoList,today);

		return fileInfoList;
	}
	/**
	 * 上传文件 不会重命名文件
	 * 
	 * @param targetDir
	 *            相对路径
	 * @param encode
	 *            编码
	 * @param request
	 *            HttpServletRequest对象
	 * 
	 * @return Map<String,String> 包含文件原始完整名称和路径的集合
	 */
	public static List<FileInfo> upload(String targetDir, String encode,
			HttpServletRequest request,boolean today) {
		List<FileInfo> fileInfoList = null;

		fileInfoList = executeUpload(targetDir, encode, false, request,
				fileInfoList,today);

		return fileInfoList;
	}

	/**
	 * 上传文件并返回一个MAP包含文件名和流信息
	 * @param encode
	 * @param request
	 * @return
	 */
	public static Map<String, InputStream> upload(String encode,
			HttpServletRequest request) {
		// #########################################################
		// 执行上传
		// #########################################################
		Map<String,InputStream> map = new HashMap<String, InputStream>();
		try {
			CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver(
					request.getSession().getServletContext());

			// 若不是一个包含上传文件的request，则返回错误
			if (!commonsMultipartResolver.isMultipart(request)) {
				logger.error("==> Not a multi request,upload file is not in this request");
				return null;
			}

			// 开始上传

			if (!CommonUtility.isNonEmpty(encode))
				encode = defaultEncode;
			commonsMultipartResolver.setDefaultEncoding(encode);

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> iter = multipartRequest.getFileNames();
			while (iter.hasNext()) {
				MultipartFile multiFile = multipartRequest
						.getFile((String) iter.next());
				if (multiFile != null) {

					String orignFilename = multiFile.getOriginalFilename();
					map.put(orignFilename, multiFile.getInputStream());
				}
			}
			return map;
		} catch (UnsupportedEncodingException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);

			// e.printStackTrace();
		} catch (IllegalStateException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);

			// e.printStackTrace();
		} catch (IOException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);
			if ("Stream ended unexpectedly".equals(e.getCause())) {
				logger.error("File upload is canceled");
			} else {
				// e.printStackTrace();
			}
		}
		return null;
	}
/**一次上传多附件
 * */
	private static List<FileInfo> executeMoreUpload(String targetDir,
			String encode, boolean rename, CommonsMultipartFile files[],
			List<FileInfo> fileInfoList,boolean today) {
		      
		        // 上传位置  
		        String path = targetDir; // 设定文件保存的目录  
				if(today){
					path=path+"/"+DateFormatUtils.format(new Date(), "yyyyMMdd");
					logger.info("目录："+path);
				}
		        File f = new File(path);  
		        if (!f.exists())  
		            f.mkdirs();  
		      
		        for (int i = 0; i < files.length; i++) {  
		            // 获得原始文件名  
		            String fileName = files[i].getOriginalFilename();  

		      

					FileInfo fileInfo = new FileInfo();

					String orignFilename =files[i].getOriginalFilename();
					if(CommonUtility.isNonEmpty(orignFilename)){
						logger.debug("[orignFilename] = " + orignFilename);

						// 获取相对站点路径
						String ralativePath = CommonUtility.assemblePath(com.easycms.common.util.FileUtils.questionFilePath,
								orignFilename);
						logger.debug("[ralativePath] = " + ralativePath);

						// 重命名文件
						String ext = "";
						String filename = "";
						if (orignFilename.lastIndexOf(".") != -1) {
							filename = orignFilename.substring(0,
									orignFilename.lastIndexOf("."));
							ext = orignFilename.substring(
									orignFilename.lastIndexOf(".") + 1,
									orignFilename.length());
						} else {
							filename = orignFilename;
						}
						logger.debug("[filename] = " + filename);

						String newFilename = "";
						if (rename) {
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyyMMddHHmmss");
							String suffix = sdf.format(new Date()) + System.currentTimeMillis();
							if (CommonUtility.isNonEmpty(ext)) {
								newFilename += "_" + suffix + "." + ext;
							} else {
								newFilename += "_" + suffix;
							}
						} else {
							newFilename = orignFilename;
						}
						newFilename = CommonUtility.removeBlank(newFilename, "_");
						logger.debug("[new filename] = " + newFilename);

						String realRelativePath = CommonUtility.assemblePath(
								targetDir, newFilename);
						logger.debug("[realRelativePath] = " + realRelativePath);

						String absolutPath = CommonUtility.assemblePath(
								f.getAbsolutePath(), newFilename);
						logger.debug("[absolutPath] = " + absolutPath);

						if(today){
							newFilename=fileInfo.getToday()+"/"+newFilename;
						}
						// 写入文件
						File file = new File(absolutPath);
						if (file.exists()) {
							logger.debug("==> File[" + file.getName()
									+ "] is exist and delete it.");
							logger.debug("==> File[" + file.getName() + "] delete "
									+ file.delete());
							
						}
			            System.out.println("原始文件名:" + fileName);  
			            // 新文件名  
//			            String newFileName = UUID.randomUUID() + fileName;  
//			            if (!files[i].isEmpty()) {  
//			                try {  
//			                    FileOutputStream fos = new FileOutputStream(path  
//			                            + newFilename);  
//			                    InputStream in = files[i].getInputStream();  
//			                    int b = 0;  
//			                    while ((b = in.read()) != -1) {  
//			                        fos.write(b);  
//			                    }  
//			                    fos.close();  
//			                    in.close();  
//			                } catch (Exception e) {  
//			                    e.printStackTrace();  
//			                }  
//			            }  
//			            System.out.println("上传图片到:" + path + newFilename);  
						
						
						try {
							files[i].transferTo(file);
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						// InputStream in = multiFile.getInputStream();
						// OutputStream out = new BufferedOutputStream(new
						// FileOutputStream(file));
						// IOUtils.copy(in, out);
						// IOUtils.closeQuietly(in);
						// IOUtils.closeQuietly(out);

						// 设置返回信息
						fileInfo.setFilename(filename);
						fileInfo.setNewFilename(newFilename);
						fileInfo.setRelativePath(ralativePath);
						fileInfo.setRealRelativePath(realRelativePath);
						fileInfo.setAbsolutePath(absolutPath);
						fileInfo.setSize(String.valueOf(file.length()));
						fileInfo.setType(ext);

						fileInfoList.add(fileInfo);
					}

				
		        }  
		
		return fileInfoList;
	}
	/**
	 * @param targetDir
	 * @param encode
	 * @param request
	 * @param pathType
	 * @param fileInfoList
	 * @return
	 */
	private static List<FileInfo> executeUpload(String targetDir,
			String encode, boolean rename, HttpServletRequest request,
			List<FileInfo> fileInfoList,boolean today) {
		// #########################################################
		// 检查目标目录是否为空
		// #########################################################
		if (!CommonUtility.isNonEmpty(targetDir)) {
			logger.error("==> Target dir can not null,targetDir =" + targetDir);
			return fileInfoList;
		}

		File dir = null;
		if(today){
			targetDir=targetDir+"/"+DateFormatUtils.format(new Date(), "yyyyMMdd");
			logger.info("目录："+targetDir);
		}
		dir = new File(targetDir);
		// if(RELATIVE_PATH == pathType) {
		// }else if(ABSOLUTE_PATH == pathType) {
		// dir = new File(targetDir);
		// }else{
		// dir = new File(targetDir);
		// }
		// #########################################################
		// 检查目标目录是否存在，如不存在则建立
		// #########################################################
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// #########################################################
		// 检查目标目录是否是一个目录
		// #########################################################
		if (!dir.isDirectory()) {
			logger.error("==> Target dir must be a direct,targetDir ="
					+ targetDir);
			return fileInfoList;
		}

		// #########################################################
		// 执行上传
		// #########################################################
		try {
			CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver(
					request.getSession().getServletContext());

			// 若不是一个包含上传文件的request，则返回错误
			if (!commonsMultipartResolver.isMultipart(request)) {
				logger.error("==> Not a multi request,upload file is not in this request");
				return fileInfoList;
			}

			fileInfoList = new ArrayList<FileInfo>();

			// 开始上传

			if (!CommonUtility.isNonEmpty(encode))
				encode = defaultEncode;
			commonsMultipartResolver.setDefaultEncoding(encode);

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> iter = multipartRequest.getFileNames();
			while (iter.hasNext()) {
				MultipartFile multiFile = multipartRequest
						.getFile((String) iter.next());
				if (multiFile != null) {
					FileInfo fileInfo = new FileInfo();

					String orignFilename = multiFile.getOriginalFilename();

					logger.debug("[orignFilename] = " + orignFilename);

					// 获取相对站点路径
					String ralativePath = CommonUtility.assemblePath(targetDir,
							orignFilename);
					logger.debug("[ralativePath] = " + ralativePath);

					// 重命名文件
					String ext = "";
					String filename = "";
					if (orignFilename.lastIndexOf(".") != -1) {
						filename = orignFilename.substring(0,
								orignFilename.lastIndexOf("."));
						ext = orignFilename.substring(
								orignFilename.lastIndexOf(".") + 1,
								orignFilename.length());
					} else {
						filename = orignFilename;
					}
					logger.debug("[filename] = " + filename);

					String newFilename = "";
					if (rename) {
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyyMMddHHmmss");
						String suffix = sdf.format(new Date()) + System.currentTimeMillis();
						if (CommonUtility.isNonEmpty(ext)) {
							newFilename += "_" + suffix + "." + ext;
						} else {
							newFilename += "_" + suffix;
						}
					} else {
						newFilename = orignFilename;
					}
					newFilename = CommonUtility.removeBlank(newFilename, "_");
					logger.debug("[new filename] = " + newFilename);

					String realRelativePath = CommonUtility.assemblePath(
							targetDir, newFilename);
					logger.debug("[realRelativePath] = " + realRelativePath);

					String absolutPath = CommonUtility.assemblePath(
							dir.getAbsolutePath(), newFilename);
					logger.debug("[absolutPath] = " + absolutPath);
					if(today){
						newFilename=fileInfo.getToday()+"/"+newFilename;
					}

					// 写入文件
					File file = new File(absolutPath);
					if (file.exists()) {
						logger.debug("==> File[" + file.getName()
								+ "] is exist and delete it.");
						logger.debug("==> File[" + file.getName() + "] delete "
								+ file.delete());
					}
					multiFile.transferTo(file);
					// InputStream in = multiFile.getInputStream();
					// OutputStream out = new BufferedOutputStream(new
					// FileOutputStream(file));
					// IOUtils.copy(in, out);
					// IOUtils.closeQuietly(in);
					// IOUtils.closeQuietly(out);

					// 设置返回信息
					fileInfo.setFilename(filename);
					fileInfo.setNewFilename(newFilename);
					fileInfo.setRelativePath(ralativePath);
					fileInfo.setRealRelativePath(realRelativePath);
					fileInfo.setAbsolutePath(absolutPath);
					fileInfo.setSize(String.valueOf(file.length()));
					fileInfo.setType(ext);

					fileInfoList.add(fileInfo);
				}
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);

			// e.printStackTrace();
		} catch (IllegalStateException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);

			// e.printStackTrace();
		} catch (IOException e) {
			logger.error("upload(HttpServletRequest, HttpServletResponse)", e);
			if ("Stream ended unexpectedly".equals(e.getCause())) {
				logger.error("File upload is canceled");
			} else {
				// e.printStackTrace();
			}
		}
		return fileInfoList;
	}

	/**
	 * 文件下载
	 * 
	 * @param path
	 * @param encode
	 * @param response
	 */
	public static boolean download(String absolutePath, String encode,
			HttpServletResponse response) {
		// #########################################################
		// 检查下载地址是否为空
		// #########################################################
		if (!CommonUtility.isNonEmpty(absolutePath)) {
			logger.error("==> Download path can not be null,path ="
					+ absolutePath);
			return false;
		}

		File file = new File(absolutePath);
		// #########################################################
		// 检查目标目录是否存在，如不存在则建立
		// #########################################################
		if (!file.exists()) {
			logger.error("==> Download path is not exist,path =" + absolutePath);
			return false;
		}

		// #########################################################
		// 检查下载地址是否是一个文件
		// #########################################################
		if (!file.isFile()) {
			logger.error("==> Download path is not a file type,path ="
					+ absolutePath);
			return false;
		}

		// #########################################################
		// 检查编码
		// #########################################################
		if (!CommonUtility.isNonEmpty(encode)) {
			encode = defaultEncode;
		}

		// #########################################################
		// 执行下载
		// #########################################################
		InputStream in = null;
		OutputStream out = null;
		Writer writer = null;
		try {
			// 写入头信息
			response.setContentType("application/x-msdownload");
			// response.setContentType("application/vnd.ms-excel;charset=" +
			// encode);
			String filenameSrc = file.getName();
			// String filename = MyURLEncoder.encode(filenameSrc,"UTF-8");
			// if(filename.length()>150)//解决IE 6.0 bug
			String filename = new String(filenameSrc.getBytes("utf-8"),
					"ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment;filename="
					+ filename);
			// response.setHeader("Content-Disposition", "attachment; filename="
			// + new String(.getBytes("iso-8859-1"),encode));
			response.addHeader("Content-Length", "" + file.length());

			// 读流
			FileInputStream fin = new FileInputStream(file);
			// BufferedReader reader = new BufferedReader(new
			// InputStreamReader(fin, encode));
			in = new BufferedInputStream(fin);

			// 写流
			// writer = response.getWriter();
			out = response.getOutputStream();

			IOUtils.copy(in, out);

			// byte[] buff = new byte[2048];
			// int bytesRead;
			//
			// while(-1 != (bytesRead = reader.read(buff, 0, buff.length))) {
			// out.write(buff,0,bytesRead);
			// }
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(writer);
		}
		return true;
	}
}
