package com.easycms.common.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Messenger implements Serializable {
	private static final long serialVersionUID = -7044921187605122610L;
	
	private Map<MessageType, List<String>> messenger = new HashMap<MessageType, List<String>>();

	public Map<MessageType, List<String>> getMessenger() {
		return messenger;
	}
	
	public void addMessenger(MessageType mt, String message) {
		if (null == this.messenger.get(mt)){
			List<String> messageList = new ArrayList<String>();
			messageList.add(message);
			this.messenger.put(mt, messageList);
		} else {
			this.messenger.get(mt).add(message);
		}
	}
	
	public boolean isContainError(){
		boolean flag = false;
		Iterator<MessageType> it = messenger.keySet().iterator();  
        while(it.hasNext()){
        	MessageType key = (MessageType)it.next();  
        	if (key.equals(MessageType.ERROR)){
        		flag = true;
        		break;
        	}
        }
		return flag;
	}
}