package com.easycms.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author dengjiepeng:
 * @areate Date:2012-3-13
 * 
 */
public class WebUtility {
	private static Logger log = Logger.getLogger(WebUtility.class);
	private static GsonBuilder builder = new GsonBuilder();
	// private static HttpServletRequest request;
	private static final String EMAIL_PATTERN = "^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$";
	private static final String USERNAME_PATTERN = "^[\\w\\u4e00-\\u9fa5]*$";
	private static final String NUMBER_PATTERN = "^([0-9]+)$";

	public static final String EMAIL = "EMAIL";
	public static final String THIRDPARTY = "THIRDPARTY";
	public static final String MOBILE = "MOBILE";
	public static final String BIND = "BIND";
	public static final String CHANGE = "CHANGE";
	public static final String ADD = "ADD";
	public static final String REGISTER = "REGISTER";
	public static final String SMS = "SMS";
	public static final String ID = "ID";
	public static final String VOICE = "VOICE";
	public static final String EN = "EN";
	public static final String CN = "CN";
	public static final String ZH = "zh";
	public static final String REG = "REG";
	public static final String REF = "REF";
	public static final String NUMBER = "NUMBER";
	public static final String NAME = "NAME";
	public static final String USERID = "USERID";
	public static final String GOLD = "GOLD";
	public static final String SILVER = "SILVER";
	public static final String BRONZE = "BRONZE";
	public static final String USER_IN_SESSION = "user";

	private static String TOKEN_FOR_REPEAT_SUBMIT = "TOKEN_FOR_REPEAT_SUBMIT";

	public static final TimeZone TIMEZONE_CHINA_SHANGHAI = TimeZone
			.getTimeZone("Asia/Shanghai");
	public static final TimeZone TIMEZONE_AMERICAN_NEWYORK = TimeZone
			.getTimeZone("America/New_York");
	public static final String TIMEZONE_GMT_TWO = "GMT+2";
	public static final String TIMEZONE_GMT_THREE = "GMT+3";
	public static final String TIMEZONE_GMT_FOUR = "GMT+4";
	public static final String TIMEZONE_GMT_FIVE = "GMT+5";
	public static final String TIMEZONE_GMT_SIX = "GMT+6";
	public static final String TIMEZONE_GMT_SEVEN = "GMT+7";
	public static final String TIMEZONE_GMT_ENGHT = "GMT+8";
	public static final String TIMEZONE_GMT_NINE = "GMT+9";

	public static Locale getI18nLocal(HttpServletRequest request) {
		Locale locale = (Locale) request.getSession().getAttribute(
				SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		return locale;
	}

	public static String getI18nLang(HttpServletRequest request) {
		Locale locale = (Locale) request.getSession().getAttribute(
				SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		String lang = locale == null ? WebUtility.ZH : locale.getLanguage();
		return lang;
	}

	public static String getI18nIsoCode(HttpServletRequest request) {
		Locale locale = (Locale) request.getSession().getAttribute(
				SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		String isocode = null;
		if (locale != null)
			isocode = locale.getCountry();
		return isocode;
	}

	/**
	 * * 通配符比较
	 */
	public static boolean simpleWildcardMatch(String pattern, String str) {
		return wildcardMatch(pattern, str, "*");
	}

	/**
	 * 通配符比较
	 * 
	 * @param pattern
	 *            匹配规则
	 * @param str
	 *            待匹配字符串
	 * @param wildcard
	 *            通配符
	 * @return
	 */
	public static boolean wildcardMatch(String pattern, String str,
			String wildcard) {
		if (StringUtils.isEmpty(pattern) || StringUtils.isEmpty(str)) {
			return false;
		}

		pattern = pattern.replace("*", ".*");
		return Pattern.matches(pattern, str);

	}

	/**
	 * 获取带参数的请求URL
	 * 
	 * @param request
	 * @return URL字符串
	 */
	public static String getRequestURLWithQueryParrams(
			HttpServletRequest request) {
		StringBuffer sb = request.getRequestURL();
		Map<String, String[]> map = request.getParameterMap();
		if (map.size() > 0)
			sb.append("?");
		Iterator<Entry<String, String[]>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String[]> entry = it.next();
			String key = entry.getKey();
			String[] value = entry.getValue();
			String parram = key + "=" + value[0] + "&";
			sb.append(parram);
		}
		return sb.toString();
	}

	public static String getHostURL(HttpServletRequest request) {
		StringBuffer url = request.getRequestURL();
		String tempContextUrl = url.delete(
				url.length() - request.getRequestURI().length(), url.length())
				.toString();
		return tempContextUrl;
	}

	public static boolean isMobileNumber(String mobileNo) {
		boolean result = false;
		if (isVerify(mobileNo)) {
			String regex = "^(\\+)*([0-9])+$";
			result = Pattern.compile(regex).matcher(mobileNo).matches();
		}
		return result;
	}

	public static String toJson(Object obj) {
		Gson gson = builder.create();
		return gson.toJson(obj);
	}

	public static <T> T toObject(Class<T> clazz, String json) {
		Gson gson = builder.create();
		return gson.fromJson(json, clazz);
	}

	public static <T> T toObject(Type type, String json) {
		Gson gson = builder.create();
		return gson.fromJson(json, type);
	}

	public static void writeToClient(HttpServletResponse response, String msg,
			String code) {
		String defaultCode = "UTF-8";
		if (WebUtility.isVerify(code))
			defaultCode = code;
		resp(response, msg, defaultCode);
	}

	public static void writeToClient(HttpServletResponse response, String msg) {
		String defaultCode = "UTF-8";
		if (null == msg){
			msg = "";
		}
		resp(response, msg, defaultCode);
	}

	private static void resp(HttpServletResponse response, String msg,
			String code) {
		try {
			response.setCharacterEncoding(code);
			response.setContentType("text/html;charset=" + code);
			response.getWriter().write(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get browser language.
	 */
	public static String getBrowserLanuage(HttpServletRequest request) {
		Locale locale = (Locale) request.getSession().getAttribute(
				SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		String lanuage = null;
		if (locale != null) {
			log.debug("Get lanuage from session");
			lanuage = locale.getLanguage();
		} else {
			log.debug("Get lanuage from browser");
			lanuage = request.getLocale().getLanguage();
		}
		if (lanuage == null || lanuage == "") {
			log.debug("Get lanuage from browser is null,set the server lanuage");
			lanuage = Locale.getDefault().getLanguage();
		}
		log.debug("lanuage:" + lanuage);
		return lanuage;
	}

	/**
	 * set userId in session
	 * 
	 * @param userId
	 * @param request
	 */
	public static void setUserIdInSession(String userId,
			HttpServletRequest request) {
		if (isVerify(userId)) {
			request.getSession().setAttribute("userId", userId);
		}
	}

	/**
	 * get userId from session
	 * 
	 * @param request
	 * @return
	 */
	public static String getUserIdFromSession(HttpServletRequest request) {
		return (String)request.getSession().getAttribute("userId");
	}

	/**
	 * to prevent repeat submit
	 */
	public static void makeToken(HttpServletRequest request) {
		String token = UUID.randomUUID().toString();
		log.info("makeToken:" + token);
		resetToken(request);
		request.getSession().setAttribute(TOKEN_FOR_REPEAT_SUBMIT, token);
	}

	public static String getToken(HttpServletRequest request) {
		return (String) request.getSession().getAttribute(
				TOKEN_FOR_REPEAT_SUBMIT);
	}

	public static boolean isValidatedToken(HttpServletRequest request) {
		boolean result = false;
		String tokenFromRequest = request.getParameter("token");
		log.info("tokenFromRequest:" + tokenFromRequest);
		String tokenFromSession = (String) request.getSession().getAttribute(
				TOKEN_FOR_REPEAT_SUBMIT);
		log.info("tokenFromSession:" + tokenFromSession);
		if (isVerify(tokenFromSession) && isVerify(tokenFromRequest)) {
			if (tokenFromRequest.equals(tokenFromSession)) {
				result = true;
			}
		}
		return result;
	}

	public static void resetToken(HttpServletRequest request) {
		request.getSession().removeAttribute(TOKEN_FOR_REPEAT_SUBMIT);
	}

	public static String removePlus(String prefix) {
		if (isVerify(prefix)) {
			if (prefix.contains("+")) {
				prefix = prefix.substring(prefix.indexOf("+") + 1,
						prefix.length());
			}
		} else {
			prefix = "";
		}
		return prefix.trim();
	}

	/**
	 * md5
	 * 
	 * @param str
	 * @return
	 */
	public static String MD5Digest(String str) {
		// use md5
		String value = null;
		if (isVerify(str)) {
			value = DigestUtils.md5Hex(str);
		}
		return value;
	}

	/**
	 * Verify Null
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isVerify(String str) {
		if (str == null || "".equals(str)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * get identified code
	 * 
	 * @return
	 */
	public static String getVerifyCodeByCreated(HttpServletRequest request) {
		if (request != null) {
			return (String) request.getSession().getAttribute(
					com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		} else {
			return null;
		}
	}

	/**
	 * convert GMT
	 * 
	 * @param GMT
	 * @return
	 * @throws ParseException
	 */
	public static long convertCalendarToTimestamp(String calendar,
			TimeZone timezone) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format.setTimeZone(timezone);
		// log.info("timezone:"+timezone.getID());
		long time = format.parse(calendar).getTime();
		return time;
	}

	public static String convertCurrentTimestampToCalendar(TimeZone timezone) {
		try {
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			format.setTimeZone(timezone);
			Date date = new Date();
			String calendarStr = format.format(date);
			return calendarStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * is email pattern
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
		if (isVerify(email) && emailPattern.matcher(email).matches()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isUsername(String username) {
		Pattern usernamePattern = Pattern.compile(USERNAME_PATTERN);
		if (isVerify(username) && usernamePattern.matcher(username).matches()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNumber(String number) {
		Pattern numberPattern = Pattern.compile(NUMBER_PATTERN);
		if (isVerify(number) && numberPattern.matcher(number).matches()) {
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings({ "unused" })
	public static String getIpFromHead(HttpServletRequest request) {

		Enumeration<String> headerNames = request.getHeaderNames();

		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
		}

		String ip = null;
		ip = request.getHeader("X-Forwarded-For");
		log.info("Get header ip:" + ip);
		if (!isVerify(ip))
			ip = request.getRemoteAddr();
		return ip;
	}

	public static void disableBrowserCache(HttpServletResponse response) {
		response.setHeader("pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", -1);
	}

	/**
	 * 得到request
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request;
	}

	/**
	 * 得到session
	 * 
	 * @return
	 */
	public static HttpSession getSession() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request.getSession();
	}

	/**
	 * 得到context
	 * 
	 * @return
	 */
	public static ServletContext getServletContext() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request.getSession().getServletContext();
	}

	/**
	 * 去除字符串中的所有空格
	 * 
	 * @param str
	 * @return
	 */
	public static String removeBlank(String str) {
		StringBuffer sb = new StringBuffer();
		if (isVerify(str)) {
			int length = str.length();
			for (int i = 0; i < length; i++) {
				char s = str.charAt(i);
				if (0 == s || 32 == s) {
					continue;
				} else {
					sb.append(s);
				}
			}
			str = sb.toString();
		}
		return str;
	}

	public static String formatTime(String seconds) {
		int time = Integer.parseInt(seconds);
		DecimalFormat df = new DecimalFormat("00");
		int sec = time%60;
		int min = (time%3600)/60;
		int hour = time/3600;
		
		String sec_str = df.format(sec);
		String min_str = df.format(min);
		String hour_str = df.format(hour);
		
		String time_str = "";
		if(hour == 0) {
			if(min == 0) {
				time_str = sec_str +" secs";
			} else {
				time_str = min_str + " mins " + sec_str +" secs";
			}
		} else {
			time_str = hour_str + " hous " + min_str + " mins " + sec_str +" secs";
		}
		//01 hous 00 mins 00 secs
		return time_str;
	}
	
	public static long convertCurrentDateToLong() {
		try {
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			return convertStringToCalendar(sdf.format(d), "yyyy-MM-dd").getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
    public static Calendar convertStringToCalendar (String dateTime, String dateFormat) {
        Calendar calendar = Calendar.getInstance () ;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat (dateFormat) ;
            Date date ;
            date = sdf.parse (dateTime) ;
            calendar.setTime ( date ) ;
        } catch ( ParseException e ) {
            e.printStackTrace () ;
        }
        return calendar ;
    }
    
	public static String convertLongToDateString(Long millSec, String format){
	     SimpleDateFormat sdf = new SimpleDateFormat(format);
	     Date date= new Date(millSec);
	     return sdf.format(date);
	}
	
	public static Date convertLongToDate(Long millSec){
	     Date date= new Date(millSec);
	     return date;
	}
	
	/**
	 * 生成随机字符串码
	 * @param length 串码长度
	 * @return
	 */
	public static String getRandCNString(int length) {
		String charList = "0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		String rev = " ";
		java.util.Random f = new Random();
		for (int i = 0; i < length; i++) {
			rev += charList.charAt(Math.abs(f.nextInt()) % charList.length());
		}
		return rev.trim();
	}
	
	public static String chinaToUnicode(Object obj) {
    	char[] hex = "0123456789ABCDEF".toCharArray();
    	StringBuffer buf = new StringBuffer();
    	
        CharacterIterator it = new StringCharacterIterator(obj.toString());  
        for (char c = it.first(); c != CharacterIterator.DONE; c = it.next()) {  
            if (c == '\\') {  
            	buf.append("\\\\");  
            } else if (c == '/') {  
            	buf.append("\\/");  
            } else if (c == '\b') {  
            	buf.append("\\b");  
            } else if (c == '\f') {  
            	buf.append("\\f");  
            } else if (c == '\n') {  
            	buf.append("\\n");  
            } else if (c == '\r') {  
            	buf.append("\\r");  
            } else if (c == '\t') {  
            	buf.append("\\t");  
            } else if (c >= 19968 && c <= 171941) { 
            	buf.append("\\u");
            	int n = c;  
                for (int i = 0; i < 4; ++i) {  
                    int digit = (n & 0xf000) >> 12;  
                	buf.append(hex[digit]);  
                    n <<= 4;  
                } 
            } else {  
            	buf.append(c);  
            }  
        }  
        
        return buf.toString();
    }
	
	public static String emailFade(String email) {
		if(!CommonUtility.isNonEmpty(email) || !email.contains("@")) {
			return email;
		}
		
		String emailPrfix = email.split("@")[0];
		StringBuffer sb = new StringBuffer();
		char[] c = emailPrfix.toCharArray();
		
		for (int i = 0; i < c.length; i++){
			if (i == 0 || i == c.length - 1){
				sb.append(c[i]);
			} else {
				sb.append("*");
			}
		}
		
		return sb.toString() + "@" + email.split("@")[1];
	}
	
	public static String changeDate(String date){
		Date tDate;
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			tDate = sdf.parse(date);
			calendar.setTime(tDate);
		} catch (ParseException e) {
			throw new RuntimeException("date add error");
		}
		tDate = calendar.getTime();
		return sdf.format(tDate);
	}
	
	public static String getDateString(int monthNo){
		if (0 == monthNo){
			Calendar cal = Calendar.getInstance();
			System.out.println(cal.toString());
			return "_of_" + cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1);
		} else if (1 == monthNo){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			return "_of_" + cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1);
		} else if (2 == monthNo){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -2);
			return "_of_" + cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1);
		} else if (-1 == monthNo){
			Calendar cal = Calendar.getInstance();
			String to = cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1);
			cal.add(Calendar.MONTH, -2);
			String from = cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1);
			return "_from_" + from + "_to_" + to;
		}
		
		return null;
	}
	
	public static String setContentType(String returnFileName){  
        String contentType = "application/octet-stream";  
        if (returnFileName.lastIndexOf(".") < 0)  
            return contentType;  
        returnFileName = returnFileName.toLowerCase();  
        returnFileName = returnFileName.substring(returnFileName.lastIndexOf(".")+1);  
          
        if (returnFileName.equals("html") || returnFileName.equals("htm") || returnFileName.equals("shtml")){  
            contentType = "text/html";  
        } else if (returnFileName.equals("css")){  
            contentType = "text/css";  
        } else if (returnFileName.equals("xml")){  
            contentType = "text/xml";  
        } else if (returnFileName.equals("gif")){  
            contentType = "image/gif";  
        } else if (returnFileName.equals("jpeg") || returnFileName.equals("jpg")){  
            contentType = "image/jpeg";  
        } else if (returnFileName.equals("js")){  
            contentType = "application/x-javascript";  
        } else if (returnFileName.equals("atom")){  
            contentType = "application/atom+xml";  
        } else if (returnFileName.equals("rss")){  
            contentType = "application/rss+xml";  
        } else if (returnFileName.equals("mml")){  
            contentType = "text/mathml";   
        } else if (returnFileName.equals("txt")){  
            contentType = "text/plain";  
        } else if (returnFileName.equals("jad")){  
            contentType = "text/vnd.sun.j2me.app-descriptor";  
        } else if (returnFileName.equals("wml")){  
            contentType = "text/vnd.wap.wml";  
        } else if (returnFileName.equals("htc")){  
            contentType = "text/x-component";  
        } else if (returnFileName.equals("png")){  
            contentType = "image/png";  
        } else if (returnFileName.equals("tif") || returnFileName.equals("tiff")){  
            contentType = "image/tiff";  
        } else if (returnFileName.equals("wbmp")){  
            contentType = "image/vnd.wap.wbmp";  
        } else if (returnFileName.equals("ico")){  
            contentType = "image/x-icon";  
        } else if (returnFileName.equals("jng")){  
            contentType = "image/x-jng";  
        } else if (returnFileName.equals("bmp")){  
            contentType = "image/x-ms-bmp";  
        } else if (returnFileName.equals("svg")){  
            contentType = "image/svg+xml";  
        } else if (returnFileName.equals("jar") || returnFileName.equals("var") || returnFileName.equals("ear")){  
            contentType = "application/java-archive";  
        } else if (returnFileName.equals("doc")){  
            contentType = "application/msword";  
        } else if (returnFileName.equals("pdf")){  
            contentType = "application/pdf";  
        } else if (returnFileName.equals("rtf")){  
            contentType = "application/rtf";  
        } else if (returnFileName.equals("xls")){  
            contentType = "application/vnd.ms-excel";   
        } else if (returnFileName.equals("ppt")){  
            contentType = "application/vnd.ms-powerpoint";  
        } else if (returnFileName.equals("7z")){  
            contentType = "application/x-7z-compressed";  
        } else if (returnFileName.equals("rar")){  
            contentType = "application/x-rar-compressed";  
        } else if (returnFileName.equals("swf")){  
            contentType = "application/x-shockwave-flash";  
        } else if (returnFileName.equals("rpm")){  
            contentType = "application/x-redhat-package-manager";  
        } else if (returnFileName.equals("der") || returnFileName.equals("pem") || returnFileName.equals("crt")){  
            contentType = "application/x-x509-ca-cert";  
        } else if (returnFileName.equals("xhtml")){  
            contentType = "application/xhtml+xml";  
        } else if (returnFileName.equals("zip")){  
            contentType = "application/zip";  
        } else if (returnFileName.equals("mid") || returnFileName.equals("midi") || returnFileName.equals("kar")){  
            contentType = "audio/midi";  
        } else if (returnFileName.equals("mp3")){  
            contentType = "audio/mpeg";  
        } else if (returnFileName.equals("ogg")){  
            contentType = "audio/ogg";  
        } else if (returnFileName.equals("m4a")){  
            contentType = "audio/x-m4a";  
        } else if (returnFileName.equals("ra")){  
            contentType = "audio/x-realaudio";  
        } else if (returnFileName.equals("3gpp") || returnFileName.equals("3gp")){  
            contentType = "video/3gpp";  
        } else if (returnFileName.equals("mp4") ){  
            contentType = "video/mp4";  
        } else if (returnFileName.equals("mpeg") || returnFileName.equals("mpg") ){  
            contentType = "video/mpeg";  
        } else if (returnFileName.equals("mov")){  
            contentType = "video/quicktime";  
        } else if (returnFileName.equals("flv")){  
            contentType = "video/x-flv";  
        } else if (returnFileName.equals("m4v")){  
            contentType = "video/x-m4v";  
        } else if (returnFileName.equals("mng")){  
            contentType = "video/x-mng";  
        } else if (returnFileName.equals("asx") || returnFileName.equals("asf")){  
            contentType = "video/x-ms-asf";  
        } else if (returnFileName.equals("wmv")){  
            contentType = "video/x-ms-wmv";  
        } else if (returnFileName.equals("avi")){  
            contentType = "video/x-msvideo";  
        }
          
        return contentType;  
    }

	public static Date convertStringToDate(String strDate, String format) {
		  SimpleDateFormat formatter = new SimpleDateFormat(format);
		  ParsePosition pos = new ParsePosition(0);
		  Date strtodate = formatter.parse(strDate, pos);
		  return strtodate;
	}
	
	public static String convertCurrentTimeToString() {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String timeStr = format.format(date);
			return timeStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String convertTimestampToString(Timestamp ts, String format) {
		try {
			SimpleDateFormat df = new SimpleDateFormat(format);
			String str = df.format(ts);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	    
    /**
     * 符合 RFC 3986 标准的“百分号URL编码”
     * 在这个方法里，空格会被编码成%20，而不是+
     * 和浏览器的encodeURIComponent行为一致
     * @param value
     * @return
     */
	public static String encodeURIComponent(String value) {
		try {
			return URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String decodeURIComponent(String value) {
		try {
			return URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
