package com.easycms.common.util;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class AppPropertiesManager {
	
    private static AppPropertiesManager _instance;

    private static String PROFILENAME = "";

    private static Properties properties = null;

    private AppPropertiesManager() {
        _instance = null;
    }

    public static synchronized AppPropertiesManager getInstance() {
        if (_instance == null) {
            _instance = new AppPropertiesManager();
            getInstance();
        }
        return _instance;
    }

    private static void getPropFile() {
        InputStream inputStream = null;

        try {
            properties = new Properties();
            inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROFILENAME);
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new NullPointerException(PROFILENAME + " cannot be found!");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static List<String> getAllPropertyKeyAsList(String filePro) {
    	PROFILENAME = filePro;
    	AppPropertiesManager.getInstance();
		Object[] objs = AppPropertiesManager.properties.keySet().toArray();
    	List<String> propList = new ArrayList<String>();
    	for(int i = 0; i < objs.length; i++){
    		propList.add(objs[i].toString());
    	}

        return propList;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, String> getAllPropertyMapAsList(String filePro) {
    	PROFILENAME = filePro;
    	AppPropertiesManager.getInstance();
    	try {
    		properties = new Properties();
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROFILENAME);
			if (inputStream != null) {  
				properties.load(inputStream);
				inputStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	Map<String, String> map = new HashMap<String, String>((Map)properties);
    	
    	return map;
    }
    
    public static boolean setMapToProperty(String filePro, Map<String, String> map) {
    	PROFILENAME = filePro;
    	AppPropertiesManager.getInstance();
    	try {
    		properties = new Properties();
    		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROFILENAME);
    		if (inputStream != null) {  
    			properties.load(inputStream);
    			
    			OutputStream fos = new FileOutputStream(Thread.currentThread().getContextClassLoader().getResource("").getPath() + PROFILENAME);
    			
    			Iterator<Entry<String, String>> iter = map.entrySet().iterator();
    			while(iter.hasNext()) {
    				Entry<String, String> entry = (Entry<String, String>)iter.next();
    				properties.setProperty(entry.getKey(), entry.getValue());
    			}
    			
    			properties.store(fos, "Update '" + PROFILENAME + "' value");
    			inputStream.close();
    			
    			return true;
    		}
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	
    	return false;
    }

    public static String getPropertyAsString(String filePro, String key) {
    	PROFILENAME = filePro;
    	getPropFile();
        String propValue = "";
        AppPropertiesManager.getInstance();
		propValue = AppPropertiesManager.properties.getProperty(key);
        if (propValue == null)
            propValue = "";

        return propValue;
    }

    public String getPropertyAsString(String filePro, String key, String DefaultValue) {
    	PROFILENAME = filePro;
        String propValue = "";

        AppPropertiesManager.getInstance();
		propValue = AppPropertiesManager.properties.getProperty(key, DefaultValue);
        if (propValue == null)
            propValue = "";

        return propValue;
    }
}
