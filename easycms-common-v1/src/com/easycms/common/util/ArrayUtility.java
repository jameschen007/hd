package com.easycms.common.util;

public class ArrayUtility {

	/**
	 * 转换为以逗号,分隔的字符串
	 * @param array
	 * @return
	 */
	public static String toCommaString(Object[] array){
		StringBuffer sb = new StringBuffer(1024);
		if(array!=null && array.length>0){
			for(int i=0;i<array.length;i++){
				if(i==0){
					sb.append(array[0]);
				}else{
					sb.append(",");
					sb.append(array[i]);
				}
			}
		}
		
		return sb.toString();
	}
}
