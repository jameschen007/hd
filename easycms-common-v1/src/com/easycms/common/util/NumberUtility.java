package com.easycms.common.util;

public class NumberUtility {
	
	public static void main(String[] args) {
		NumberUtility t = new NumberUtility();
		
		t.test();
	}

	  public static boolean isNumber(String s)  
	  
	    {
		  if(s==null){
			  return false;
		  }
	          
	        String str = s.trim();  
	        if(str.isEmpty())  
	        {  
	            return false;  
	        }  
	        //正则表达式的运用：  
	        /*第一位：+-（一次或者一次也没有） 
	         * 第二位开始：可以使若干数字加一个点或者一个点加若干数字（数字出现一次或多次） 
	         * \d:表示[0-9],而+表示一个或多个 
	         * ?:一次或一次也没有 
	         * 指数部分：出现一次或者不出现 
	         */  
	        String regex = "[+-]?(\\d+\\.?|\\.\\d+)\\d*(e[+-]?\\d+)?";  
	          
	        if(str.matches(regex))  
	        {  
	            return true;  
	        }else  
	        {  
	            return false;  
	        }  
	          
	          
	    }  
	    public void test()  
	    {  
	        System.out.println(isNumber("0"));  
	        System.out.println(isNumber(" 0.1 "));  
	        System.out.println(isNumber("abc"));  
	        System.out.println(isNumber("1 a"));  
	        System.out.println(isNumber("2e10"));  
	        System.out.println(isNumber(".1"));  
	        System.out.println(isNumber("3."));  
	        System.out.println(isNumber("."));  
	        System.out.println(isNumber(".."));  
	        System.out.println(isNumber("-1."));  
	        System.out.println(isNumber("+.8"));  
	        System.out.println(isNumber("-."));  
	        System.out.println(isNumber(".e1"));  
	        System.out.println(isNumber("1e."));  
	        System.out.println(isNumber("2e0"));  
	        System.out.println(isNumber("46.e3"));  
	        System.out.println(isNumber("-e58"));  
	        System.out.println(isNumber("005047e+6"));  
	        System.out.println(isNumber("6+1"));  
	        System.out.println(isNumber("+53"));  
	          
	    }  
}
