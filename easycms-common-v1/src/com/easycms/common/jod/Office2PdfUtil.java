package com.easycms.common.jod;

import java.io.File;

import org.artofsolving.jodconverter.OfficeDocumentConverter;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeManager;

import com.easycms.common.util.CommonUtility;

public class Office2PdfUtil {
	private static OfficeManager officeManager;
	// 服务端口
	private static int OPEN_OFFICE_PORT[] = { 8100, 8101, 8102, 8103 };

	/**
	 * 
	 * office2Pdf 方法
	 * 
	 * @descript：TODO
	 * @param inputFile
	 *            文件全路径
	 * @param pdfFilePath
	 *            pdf文件全路径
	 * @return void
	 */
	public static boolean office2Pdf(String officeHome, String inputPath, String pdfPath) {
		if (CommonUtility.isNonEmpty(officeHome) && CommonUtility.isNonEmpty(inputPath) && CommonUtility.isNonEmpty(pdfPath)) {
			File inputFile = new File(inputPath);
			File pdfFile = new File(pdfPath);
			if (!pdfFile.exists()) {
				File file = new File(pdfPath.substring(0, pdfPath.lastIndexOf(File.separator)));
	    		if (!file.exists()) {
	    			file.mkdirs();
	    		}
			}

			try {
				// 打开服务
				startService(officeHome);
				OfficeDocumentConverter converter = new OfficeDocumentConverter(officeManager);
				// 开始转换
				converter.convert(inputFile, pdfFile);
				// 关闭
				stopService();
				System.out.println("运行结束");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public static void stopService() {
		if (officeManager != null) {
			officeManager.stop();
		}
	}

	public static void startService(String officeHome) {
		DefaultOfficeManagerConfiguration configuration = new DefaultOfficeManagerConfiguration();
		try {
			configuration.setOfficeHome(officeHome);// 设置安装目录
			configuration.setPortNumbers(OPEN_OFFICE_PORT); // 设置端口
			configuration.setTaskExecutionTimeout(1000 * 60 * 5L);
			configuration.setTaskQueueTimeout(1000 * 60 * 60 * 24L);
			officeManager = configuration.buildOfficeManager();
			officeManager.start(); // 启动服务
		} catch (Exception ce) {
			System.out.println("office转换服务启动失败!详细信息:" + ce);
		}
	}
}