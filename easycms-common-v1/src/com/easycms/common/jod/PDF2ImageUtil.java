package com.easycms.common.jod;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

public class PDF2ImageUtil {

	@SuppressWarnings("resource")
	public static boolean previewFirstImage(String pdfPath, String firstImagePath) {
		long start = System.currentTimeMillis();

		try {
			PDDocument document = new PDDocument();
			File pdfFile = new File(pdfPath);
			document = PDDocument.load(pdfFile, (String) null);
			int size = document.getNumberOfPages();
			if (size > 0) {
				BufferedImage image = new PDFRenderer(document).renderImageWithDPI(0, 130, ImageType.RGB);

				File outFile = new File(firstImagePath);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				ImageIO.write(image, "jpg", out);// 写图片
				byte[] b = out.toByteArray();
				FileOutputStream output = new FileOutputStream(outFile);
				output.write(b);
				out.close();
				output.close();
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		long end = System.currentTimeMillis();
		System.out.println(end - start);

		return true;
	}
}