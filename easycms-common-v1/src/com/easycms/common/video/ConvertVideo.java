package com.easycms.common.video;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.easycms.common.util.CommonUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConvertVideo {

	/**
	 * 视频转码
	 * 
	 * @param ffmpegPath
	 *            转码工具的存放路径
	 * @param upFilePath
	 *            用于指定要转换格式的文件,要截图的视频源文件
	 * @param codcFilePath
	 *            格式转换后的的文件保存路径
	 * @param mediaPicPath
	 *            截图保存路径
	 * @return
	 * @throws Exception
	 */
	public static boolean executeCodecs(String ffmpegPath, String upFilePath, String codcFilePath,
			String mediaPicPath) {
		if (CommonUtility.isNonEmpty(codcFilePath) && CommonUtility.isNonEmpty(ffmpegPath)
				&& CommonUtility.isNonEmpty(upFilePath) && checkContentType(upFilePath) == 0) {
			File file = new File(codcFilePath.substring(0, codcFilePath.lastIndexOf(File.separator)));
			if (!file.exists()) {
				file.mkdirs();
			}
		} else {
			return false;
		}

		// 创建一个List集合来保存转换视频文件为flv格式的命令
		// 原始命令： C:\ffmpeg\bin>ffmpeg.exe -i d:/video/old/test.wmv -vcodec libx264 -b
		// 300k -strict -2 d:/video/other/target_video.mp4
		List<String> convert = new ArrayList<String>();
		convert.add(ffmpegPath); // 添加转换工具路径
		convert.add("-i"); // 添加参数＂-i＂，该参数指定要转换的文件
		convert.add(upFilePath); // 添加要转换格式的视频文件的路径
		convert.add("-qscale"); // 指定转换的质量
		convert.add("6");
		convert.add("-ab"); // 设置音频码率
		convert.add("64");
		convert.add("-ac"); // 设置声道数
		convert.add("2");
		convert.add("-ar"); // 设置声音的采样频率
		convert.add("22050");
		convert.add("-r"); // 设置帧频
		convert.add("24");
		convert.add("-y"); // 添加参数＂-y＂，该参数指定将覆盖已存在的文件
		convert.add(codcFilePath);

		boolean mark = true;
		ProcessBuilder builder = new ProcessBuilder();
		try {
			builder.command(convert);
			builder.redirectErrorStream(true);
			builder.start();
			Process p = builder.start();
			doWaitFor(p);
			p.destroy();

			if (CommonUtility.isNonEmpty(mediaPicPath)) {
				processImg(ffmpegPath, upFilePath, mediaPicPath);
			}
		} catch (Exception e) {
			mark = false;
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		return mark;
	}

	public static boolean processImg(String ffmpegPath, String upFilePath, String mediaPicPath) {
		if (CommonUtility.isNonEmpty(mediaPicPath) && CommonUtility.isNonEmpty(ffmpegPath)
				&& CommonUtility.isNonEmpty(upFilePath) && checkContentType(upFilePath) == 0) {
			File file = new File(mediaPicPath.substring(0, mediaPicPath.lastIndexOf(File.separator)));
			if (!file.exists()) {
				file.mkdirs();
			}
		} else {
			return false;
		}

		// 创建一个List集合来保存从视频中截取图片的命令
		List<String> cutpic = new ArrayList<String>();
		cutpic.add(ffmpegPath);
		cutpic.add("-i");
		cutpic.add(upFilePath); // 同上（指定的文件即可以是转换为flv格式之前的文件，也可以是转换的flv文件）
		cutpic.add("-y");
		cutpic.add("-f");
		cutpic.add("image2");
		cutpic.add("-ss"); // 添加参数＂-ss＂，该参数指定截取的起始时间
		cutpic.add("17"); // 添加起始时间为第17秒
		cutpic.add("-t"); // 添加参数＂-t＂，该参数指定持续时间
		cutpic.add("0.001"); // 添加持续时间为1毫秒
		cutpic.add("-s"); // 添加参数＂-s＂，该参数指定截取的图片大小
		cutpic.add("800*600"); // 添加截取的图片大小为350*240
		cutpic.add(mediaPicPath); // 添加截取的图片的保存路径

		boolean mark = true;
		ProcessBuilder builder = new ProcessBuilder();
		try {
			builder.command(cutpic);
			builder.redirectErrorStream(true);
			// 如果此属性为 true，则任何由通过此对象的 start() 方法启动的后续子进程生成的错误输出都将与标准输出合并，
			// 因此两者均可使用 Process.getInputStream() 方法读取。这使得关联错误消息和相应的输出变得更容易
			builder.start();
			Process p = builder.start();
			doWaitFor(p);
			p.destroy();
		} catch (Exception e) {
			mark = false;
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return mark;
	}

	@SuppressWarnings("static-access")
	public static int doWaitFor(Process p) {
		InputStream in = null;
		InputStream err = null;
		int exitValue = -1; // returned to caller when p is finished
		try {
			log.debug("comeing");
			in = p.getInputStream();
			err = p.getErrorStream();
			boolean finished = false; // Set to true when p is finished

			while (!finished) {
				try {
					while (in.available() > 0) {
						Character c = new Character((char) in.read());
						System.out.print(c);
					}
					while (err.available() > 0) {
						Character c = new Character((char) err.read());
						System.out.print(c);
					}

					exitValue = p.exitValue();
					finished = true;

				} catch (IllegalThreadStateException e) {
					Thread.currentThread().sleep(500);
				}
			}
		} catch (Exception e) {
			System.err.println("doWaitFor();: unexpected exception - " + e.getMessage());
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				log.debug(e.getMessage());
			}
			if (err != null) {
				try {
					err.close();
				} catch (IOException e) {
					log.debug(e.getMessage());
				}
			}
		}
		return exitValue;
	}

	private static int checkContentType(String upFilePath) {
		String type = upFilePath.substring(upFilePath.lastIndexOf(".") + 1, upFilePath.length()).toLowerCase();
		// ffmpeg能解析的格式：（asx，asf，mpg，wmv，3gp，mp4，mov，avi，flv等）
		if (type.equals("avi")) {
			return 0;
		} else if (type.equals("mpg")) {
			return 0;
		} else if (type.equals("wmv")) {
			return 0;
		} else if (type.equals("3gp")) {
			return 0;
		} else if (type.equals("mov")) {
			return 0;
		} else if (type.equals("mp4")) {
			return 0;
		} else if (type.equals("asf")) {
			return 0;
		} else if (type.equals("asx")) {
			return 0;
		} else if (type.equals("flv")) {
			return 0;
		}
		// 对ffmpeg无法解析的文件格式(wmv9，rm，rmvb等),
		// 可以先用别的工具（mencoder）转换为avi(ffmpeg能解析的)格式.
		else if (type.equals("wmv9")) {
			return 1;
		} else if (type.equals("rm")) {
			return 1;
		} else if (type.equals("rmvb")) {
			return 1;
		}
		return 9;
	}
}
