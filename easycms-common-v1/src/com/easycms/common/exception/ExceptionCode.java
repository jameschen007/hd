package com.easycms.common.exception;

public class ExceptionCode {

	/**
	 * -1004 程序异常抛出后，在controller捕获
	 */
	public static String E4="-1004";
	/**
	 * -1001 入参有误
	 */
	public static String E1="-1001";
	/**
	 * -1002 入参格式有识
	 */
	public static String E2="-1002";
	/**
	 * -1003 操作失败
	 */
	public static String E3="-1003";
}
