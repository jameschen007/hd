package com.easycms.common.jpush;

import com.easycms.common.util.AppPropertiesManager;

public class JPushAliasPrefix {
    private static final String JPUSH = "jpush.properties";
    public static final String ALIAS_PREFIX = AppPropertiesManager.getPropertyAsString(JPUSH, "aliasPrefix");
    public static final String USER_PREFIX = AppPropertiesManager.getPropertyAsString(JPUSH, "userPrefix");
}
