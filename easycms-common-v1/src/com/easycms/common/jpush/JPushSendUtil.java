package com.easycms.common.jpush;

import java.util.Map;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.audience.AudienceTarget;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import lombok.extern.slf4j.Slf4j;

import com.easycms.common.util.AppPropertiesManager;
import com.google.gson.JsonObject;

@Slf4j
public class JPushSendUtil {
    private static final String JPUSH = "jpush.properties";
    
    private static final String appKey = AppPropertiesManager.getPropertyAsString(JPUSH, "appKey");
	private static final String masterSecret = AppPropertiesManager.getPropertyAsString(JPUSH, "masterSecret");
	
	public static void sendPushWithAlias(String alert, String alias) {
        JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
        
        PushPayload payload = null;
        //判断平台
        payload = buildPushObject_all_alias_alert(alert, alias);
        log.info("--------------------------------------Start to Send Push------------------------------------------");
        log.info("Push properties : " + alias);
        log.info("Alias ==> " + alias);
        try {
            PushResult result = jpushClient.sendPush(payload);
            log.info("Got result - " + result);
        } catch (APIConnectionException e) {
            log.error("Connection error. Should retry later. ", e);
        } catch (APIRequestException e) {
            log.error("Error response from JPush server. Should review and fix it. ", e);
            log.info("HTTP Status: " + e.getStatus());
            log.info("Error Code: " + e.getErrorCode());
            log.info("Error Message: " + e.getErrorMessage());
            log.info("Msg ID: " + e.getMsgId());
        }
        log.info("--------------------------------------End to Send Push------------------------------------------");
	}
	
	public static void sendPushWithAliasAndExtra(String alert, String alias, String title, JsonObject extra) {
		JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
		
		PushPayload payload = null;
		//判断平台
		payload = buildPushObject_android(alert, alias, title, extra);
		log.info("--------------------------------------Start to Send Push [Notification]------------------------------------------");
		log.info("Alias ==> " + alias);
		try {
			PushResult result = jpushClient.sendPush(payload);
			log.info("Got result - " + result);
		} catch (APIConnectionException e) {
			log.error("Connection error. Should retry later. ", e);
		} catch (APIRequestException e) {
			log.error("Error response from JPush server. Should review and fix it. ", e);
			log.info("HTTP Status: " + e.getStatus());
			log.info("Error Code: " + e.getErrorCode());
			log.info("Error Message: " + e.getErrorMessage());
			log.info("Msg ID: " + e.getMsgId());
		}
		log.info("--------------------------------------End to Send Push [Notification]------------------------------------------");
	}
	
	public static void sendPushWithAliasAndExtra(String alert, String alias, String title, Map<String, String> extra) {
		class JPushSendUtilThread implements Runnable {
			String alert;
			String alias;
			String title;
			Map<String, String> extra;
			
			public JPushSendUtilThread(String alert, String alias, String title, Map<String, String> extra){
				this.alert = alert;
				this.alias = alias;
				this.title = title;
				this.extra = extra;
			}
			@Override
			public void run() {
				log.info("[JPushSendUtilThread] run");
				JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
				
				PushPayload payload = null;
				//判断平台
				payload = buildPushObject_android(alert, alias, title, extra);
				log.info("--------------------------------------Start to Send Push [Message]------------------------------------------");
				log.info("Alias ==> " + alias);
				log.info("Alert ==> " + alert);
					try {
						PushResult result = jpushClient.sendPush(payload);
						log.info("Got result - " + result);
					} catch (APIConnectionException e) {
						log.error("Connection error. Should retry later. ", e);
					} catch (APIRequestException e) {
						log.error("Error response from JPush server. Should review and fix it. ", e);
						log.info("HTTP Status: " + e.getStatus());
						log.info("Error Code: " + e.getErrorCode());
						log.info("Error Message: " + e.getErrorMessage());
						log.info("Msg ID: " + e.getMsgId());
					}
				log.info("--------------------------------------End to Send Push [Message]------------------------------------------");
				log.info("[JPushSendUtilThread] end");
			}
		}
		
		JPushSendUtilThread jsut = new JPushSendUtilThread(alert, alias, title, extra);
		Thread thread = new Thread(jsut);
		thread.start();
	}
	
	public static void sendPushToAll(String alert) {
		JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
		
		PushPayload payload = null;
		//判断平台
		payload = buildPushObject_all_all_alert(alert);
		
		try {
			PushResult result = jpushClient.sendPush(payload);
			log.info("Got result - " + result);
		} catch (APIConnectionException e) {
			log.error("Connection error. Should retry later. ", e);
		} catch (APIRequestException e) {
			log.error("Error response from JPush server. Should review and fix it. ", e);
			log.info("HTTP Status: " + e.getStatus());
			log.info("Error Code: " + e.getErrorCode());
			log.info("Error Message: " + e.getErrorMessage());
			log.info("Msg ID: " + e.getMsgId());
		}
	}
	private static PushPayload buildPushObject_all_all_alert(String alert) {
	    return PushPayload.alertAll(alert);
	}
	
	private static PushPayload buildPushObject_all_alias_alert(String alert, String alias) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.alias(alias))
                .setNotification(Notification.alert(alert))
                .build();
    }
    
	@SuppressWarnings("unused")
	private static PushPayload buildPushObject_android_tag_alertWithTitle(String alert, String title, String tag) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.android())
                .setAudience(Audience.tag(tag))
                .setNotification(Notification.android(alert, title, null))
                .build();
    }
    
	@SuppressWarnings("unused")
	private static PushPayload buildPushObject_android_and_ios() {
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                .setAudience(Audience.tag("tag1"))
                .setNotification(Notification.newBuilder()
                		.setAlert("alert content")
                		.addPlatformNotification(AndroidNotification.newBuilder()
                				.setTitle("Android Title").build())
                		.addPlatformNotification(IosNotification.newBuilder()
                				.incrBadge(1)
                				.addExtra("extra_key", "extra_value").build())
                		.build())
                .build();
    }
	
	private static PushPayload buildPushObject_android(String alert, String alias, String title, JsonObject extra) {
		return PushPayload.newBuilder()
				.setPlatform(Platform.android())
				.setAudience(Audience.alias(alias))
				.setNotification(Notification.newBuilder()
						.setAlert(alert)
						.addPlatformNotification(AndroidNotification.newBuilder()
								.setTitle(title)
								.addExtra("extra", extra)
								.build())
								.build())
				 .setMessage(Message.newBuilder()
                        .setMsgContent(alert)
                        .setTitle(title)
                        .build())
								.build();
	}
	
	private static PushPayload buildPushObject_android(String alert, String alias, String title, Map<String, String> extra) {
		return PushPayload.newBuilder()
				.setPlatform(Platform.android())
				.setAudience(Audience.alias(alias))
					.setMessage(Message.newBuilder()
							.setMsgContent(alert)
							.setTitle(title)
							.addExtras(extra)
							.build())
							.build();
	}
    
	@SuppressWarnings("unused")
	private static PushPayload buildPushObject_ios_tagAnd_alertWithExtrasAndMessage(String alert, String messageContent) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.ios())
                .setAudience(Audience.tag_and("tag1", "tag_all"))
                .setNotification(Notification.newBuilder()
                        .addPlatformNotification(IosNotification.newBuilder()
                                .setAlert(alert)
                                .setBadge(5)
                                .setSound("happy")
                                .addExtra("from", "JPush")
                                .build())
                        .build())
                 .setMessage(Message.content(messageContent))
                 .setOptions(Options.newBuilder()
                         .setApnsProduction(true)
                         .build())
                 .build();
    }
    
	@SuppressWarnings("unused")
	private static PushPayload buildPushObject_ios_audienceMore_messageWithExtras(String messageContent) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                .setAudience(Audience.newBuilder()
                        .addAudienceTarget(AudienceTarget.tag("tag1", "tag2"))
                        .addAudienceTarget(AudienceTarget.alias("alias1", "alias2"))
                        .build())
                .setMessage(Message.newBuilder()
                        .setMsgContent(messageContent)
                        .addExtra("from", "JPush")
                        .build())
                .build();
    }
}

